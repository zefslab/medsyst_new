﻿using System;
using Medsyst.Class;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;

namespace Medsyst.Dao.Repositories
{
    /// <summary>Класс коллекция: Требование на склад
    /// Автор: Заплатин Е.Ф.
    /// Дата создания: 06.08.2011
    ///</summary>
    public class DemandList : DBOList<Demand>
    {
        /// <summary>Загружаются требования согасно установленных критериев фильтра
        /// Заплатин Е.Ф.
        /// 25.07.2011
        /// </summary>
        public new bool Load(bool chkDateBegin, DateTime dateBegin, bool chkDateEnd, DateTime dateEnd,
                             bool chkProviders, int employeeID, int doctype_id)
        {
            dbWhere = "Document.DocTypeID = " + doctype_id.ToString();
            if (!showdeleted)
                dbWhere += " AND Document.Deleted = 0";//Кроме тех, на которые поставлен признак удаленного
            if (chkDateBegin)
                dbWhere += " AND RegisterD >= '" + dateBegin.Year.ToString() + "-" + dateBegin.Month.ToString("00") + "-" + dateBegin.Day.ToString("00") + "'";
            if (chkDateEnd)
                dbWhere += " AND RegisterD <= '" + dateEnd.Year.ToString() + "-" + dateEnd.Month.ToString("00") + "-" + dateEnd.Day.ToString("00") + "'";
            if (chkProviders)
                dbWhere += " AND EmployeeID = " + employeeID.ToString();
            //if (dbWhere != "")
            //     dbWhere = dbWhere.Remove(0, 4);

            dbOrder = "RegisterD DESC, Nbr DESC";
            return base.Load();
        }
    }

    ///<summary>Класс: коллекция требований с кратким перечнем свойств 
    ///используется в форме выдачи медикаментов со склада
    ///</summary>
    public class DemandShortList : DBOList<DemandShort>
    {
        public bool Load(int season, int member)// по сезону пока не удается фильтровать требование. 
        //Нужно будет вычислять диапазон времени от начала до окончания сезона, а затем эти временные рамки 
        //добавлять в критерий
        {
            dbSelectUser = "SELECT DocDemand.*, " +
                           " Document.RegisterD, Document.Nbr, " +
                           " p.LastName, p.Name,p.SurName " +
                    "FROM DocDemand " +
                    "LEFT JOIN Document ON DocDemand.DocumentID = Document.DocumentID " +
                    "LEFT JOIN Person AS p ON DocDemand.EmployeeID = p.PersonID ";
            //проверять параметры на ненулевые значения
            dbWhere = "";
            if (season > 0)
            {
                //выяснить начальную и конечную дату сезона 
                Season ss = new Season();//Загрузка сведений о сезоне по идентификатору
                ss.SeasonID = season;//Указывается идентификатор через передаваемый параметр
                ss.Load();//Загружаются данные по указонному идентификатору сезона в свойства класса
                DateTime begin = ss.SeasonOpen;
                DateTime end = ss.SeasonClose;
                //dbWhere += " AND Document.RegisterD >= '" + begin.ToString("dd.MM.yyyy") + "' AND Document.RegisterD <= '" + end.ToString("dd.MM.yyyy") + "' ";//Критерий по полю сезона с указанием его граничных дат
            }
            if (member > 0)
                dbWhere += " AND DocDemand.EmployeeID = " + member;//Критерий по полю сотрудника оформившего требование
            if (dbWhere != "")
                dbWhere = dbWhere.Remove(0, 4);//Удаляется лишний AND

            //Логичней было бы подсчитывать стоимость при оформлении требования и записывать в отдельное поле таблицы
            //так как это делается в истории болезни

            //System.Windows.Forms.MessageBox.Show(dbSelectCustom, "Запрос", System.Windows.Forms.MessageBoxButtons.OK);

            /*оригинальный 
             *             dbSelectCustom = "SELECT dd.DocumentID,dd.EmployeeID, d.RegisterD, d.Nbr, COUNT(*) AS CountMed, p.LastName, p.Name,p.SurName, SUM(o.Price*o.CountOutput) AS Sum FROM DocDemand AS dd " +
                    "LEFT JOIN Document AS d ON dd.DocumentID = d.DocumentID " +
                    "LEFT JOIN Person AS p ON dd.EmployeeID = p.PersonID " +
                    "RIGHT JOIN MedicamentOutput AS o ON dd.DocumentID = o.OutputDocumentID " +
                    "WHERE o.OutputDocumentTypeID = 3 " +
                    "GROUP BY dd.DocumentID, dd.EmployeeID, d.RegisterD, d.Nbr, p.LastName,p.Name,p.SurName";

             */
            return base.Load();
        }
    }
}
