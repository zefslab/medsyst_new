﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;

namespace Medsyst.Dao.Repositories
{
    /// <summary>Класс: коллекция требований по назначенным медикаментам
    /// Заплатин Е.Ф.
    /// Дата создания: 09.08.2011
    /// </summary>
    public class DemandMaterialList : DBOList<DemandMaterial>
    {
        /// <summary>Загружаются требования согасно установленных критериев фильтра
        /// Заплатин Е.Ф.
        /// 09.08.2011
        /// </summary>
        public new bool Load(bool chkDateBegin, DateTime dateBegin, bool chkDateEnd, DateTime dateEnd,
                             bool chkProviders, int employeeID, bool chkSeason, int seasonID)
        {
            dbWhere = "";
            if (!showdeleted)
                dbWhere += " AND Document.Deleted = 0";//Кроме тех, на которые поставлен признак удаленного
            if (chkDateBegin)
                dbWhere += " AND RegisterD >= '" + dateBegin.Year.ToString() + "-" + dateBegin.Month.ToString("00") + "-" + dateBegin.Day.ToString("00") + "'";
            if (chkDateEnd)
                dbWhere += " AND RegisterD <= '" + dateEnd.Year.ToString() + "-" + dateEnd.Month.ToString("00") + "-" + dateEnd.Day.ToString("00") + "'";
            if (chkProviders)
                dbWhere += " AND EmployeeID = " + employeeID.ToString();
            if (chkSeason)
                dbWhere += " AND SeasonID = " + seasonID.ToString();
            if (dbWhere != "")
                dbWhere = dbWhere.Remove(0, 4);

            dbOrder = "Nbr DESC";//Сортировка начиная с болеее поздних номеров
            return base.Load();
        }
    }
}
