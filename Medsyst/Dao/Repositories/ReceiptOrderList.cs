﻿using System;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;

namespace Medsyst.Dao.Repositories
{
    /// <summary>
    /// Класс: Коллекция приходных ордеров
    /// </summary>   
    public class ReceiptOrderList : DBOList<ReceiptOrder>
    {
        /// <summary>Загрузка приходных ордеров согласно установленных фильтров
        /// Запалтин Е.Ф.
        /// 27.11.2011
        /// </summary>
        /// <param name="chkShowDeleted"></param>
        /// <param name="chkDate"></param>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <param name="chkProvider"></param>
        /// <param name="providerId"></param>
        /// <param name="chkBudget"></param>
        /// <param name="isBudget"></param>
        /// <returns></returns>
        public DBOList<ReceiptOrder> Load(bool chkShowDeleted, bool chkDate, DateTime date1, DateTime date2,
                             bool chkProvider, int providerId, bool chkBudget, bool isBudget)
        {
            dbWhere = "";
            dbWhere += chkShowDeleted ? "" : " AND Deleted = 0";//Установлен признак показа удаленных приходных ордеров
            if (chkDate)
            {//Учитывается период прихода
                string d1 = "'" + date1.Year + "-" + date1.Month.ToString("00") + "-" + date1.Day.ToString("00") + "'";
                string d2 = "'" + date2.Year + "-" + date2.Month.ToString("00") + "-" + date2.Day.ToString("00") + "'";
                dbWhere += " AND AccountD >= " + d1 + " AND AccountD <= " + d2;
            }
            if (chkProvider)
                dbWhere += " AND ProviderID = " + providerId;
            if (chkBudget)
                dbWhere += " AND IsBudget = " + (isBudget ? "1" : "0");

            if (dbWhere != "")
                dbWhere = dbWhere.Remove(0, 4);

            dbOrder = "RegisterD DESC, Nbr DESC";

            return Get();
        }
    }
}
