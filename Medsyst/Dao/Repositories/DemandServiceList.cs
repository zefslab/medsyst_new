﻿using System;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;

namespace Medsyst.Dao.Repositories
{
    /// <summary>Класс: Коллекция требований по услугам
    /// Заплатин Е.Ф.
    /// Дата создания: 08.11.09
    /// </summary>
    public class DemandServiceList : DBOList<DemandService>
    {
        /// <summary>Загружаются требования согасно установленных критериев фильтра
        /// Заплатин Е.Ф.
        /// 25.07.2011
        /// </summary>
        public bool Load(bool chkDateBegin, DateTime dateBegin, bool chkDateEnd, DateTime dateEnd,
                             bool chkProviders, int employeeID, bool chkServices, int serviceID,
                             bool chkSeason, int seasonID, int docType)
        {
            dbWhere = "Document.DocTypeID = " + docType.ToString();
            if (!showdeleted)
                dbWhere += " AND Document.Deleted = 0";//Кроме тех, на которые поставлен признак удаленного
            if (chkDateBegin)
                dbWhere += " AND RegisterD >= '" + dateBegin.Year.ToString() + "-" + dateBegin.Month.ToString("00") + "-" + dateBegin.Day.ToString("00") + "'";
            if (chkDateEnd)
                dbWhere += " AND RegisterD <= '" + dateEnd.Year.ToString() + "-" + dateEnd.Month.ToString("00") + "-" + dateEnd.Day.ToString("00") + "'";
            if (chkProviders)
                dbWhere += " AND EmployeeID = " + employeeID.ToString();
            if (chkServices)
                dbWhere += " AND ServiceID = " + serviceID.ToString();
            if (chkSeason)
                dbWhere += " AND SeasonID = " + seasonID.ToString();

            dbOrder = "RegisterD DESC, Nbr DESC";//Сортировка начиная с болеее поздних номеров
            return base.Load();
        }

        /// <summary>Загружает несколько последних требований в обратном хронологическом порядке
        /// Заплатин Е.Ф
        /// 11.07.2011
        /// </summary>
        /// <param name="serviceID">услуга для которой загружаются требования</param>
        /// <param name="count">количество загружаемых требований</param>
        public bool Load(int serviceID, int count)
        {
            //string Select = "SELECT TOP " + count + " Document.*, DocDemand.* FROM Document " +
            //                "RHIGT JOIN DocDemand ON DocDemand.DocumentID = Document.DocumentID";
            //string Where = " WHERE Document.Deleted = 0 AND DocDemand.ServiceID   = " + serviceID.ToString();
            //string Order = " ORDER BY RegisterD DESC";
            //dbSelectCustom = Select + Where + Order;
            dbWhere = "Document.Deleted = 0 AND DocDemand.ServiceID   = " + serviceID.ToString();
            dbOrder = "RegisterD DESC";
            return base.Load();
        }
    }
 
}
