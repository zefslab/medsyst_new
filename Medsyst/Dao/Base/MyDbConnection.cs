﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;

namespace Medsyst.Dao.Base
{
    /// <summary>
    /// Singletone для подключения к БД
    /// </summary>
    public sealed  class MyDbConnection
    {
        private static SqlConnection _sqlConnection;

        private MyDbConnection(){}

        public static  SqlConnection Instance(string conectionString)
        {
            return _sqlConnection ?? (_sqlConnection = new SqlConnection(conectionString));
        }

        /// <summary>
        /// Используется при старте приложения
        /// </summary>
        /// <param name="conectionString"></param>
        public static void Open(string conectionString)
        {
            if (_sqlConnection.State != ConnectionState.Open)
                Instance(conectionString).Open();
        }

        /// <summary>
        /// Используется при обычном или аварийном завершении программы
        /// </summary>
        public static void Close()
        {
            if (_sqlConnection.State != ConnectionState.Closed)
                _sqlConnection.Close();
        }
    }
}
