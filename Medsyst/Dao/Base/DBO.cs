﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Data;
using rsdev.MCV.View;
using System.Windows.Forms;
using Medsyst.Class.Core;

/// <summary>Описание таблицы БД</summary>
public class DBTableDesc
{
    public string Table = "";// Название таблицы

    public string PrimaryKey = ""; // Главный ключ
    ///<summary>Первичные ключи таблицы</summary>
    public List<string> PrimaryKeys = new List<string>();


    public string ForeignKey = ""; // Поле, по которому привязывается наследник к базовому классу
    public string ForeignKeyBase = ""; // Поле, внешний ключ, базаового класса, к которому привязывается ForeignKey

    public List<string> ForeignKeys = new List<string>(); // Поле, по которому привязывается наследник к базовому классу
    public List<string> ForeignKeyBases  = new List<string>(); // Поле, внешний ключ, базаового класса, к которому привязывается ForeignKey


    public string dbSelect = ""; // SQL-операция выборки
    public string dbInsert = ""; // SQL-операция вставки
    public string dbInsertFull = ""; // SQL-операция вставки всех полей
    public string dbUpdate = ""; // SQL-операция обновления
    public string dbDelete = ""; // SQL-операция удаления
    public string _insValues = "";
    public string _insValuesFull = "";

    public bool Right = true; // означает что эта таблица будет правой при соединении с таблицами базового класса
    public bool IsMain = true; // 

    public int Identity = 0; // Возвращенное значение первичный ключа при вставке
}
public class DBOInit
{
    public bool IsInit = false;

    public List<DBTableDesc> _Tables = new List<DBTableDesc>();
    public string dbSelect = "";
    public string dbKeyField = "";
    public List<string> dbKeyFields = new List<string>();
    public string dbKeyTable = "";
    public string dbSelectList = "";
}
public static class DBOFabric
{
    static Hashtable list = new Hashtable();

    public static DBOInit GetInit(string classname)
    {
        DBOInit o = (DBOInit)list[classname];
        if (o == null)
        {
            o = new DBOInit();
            list.Add(classname, o);
        }
        return o;
    }
}

/// <summary>Интерфейс для категории. Используется для формы шаблона представления сатегорий в виде списка
/// Заплатин Е.Ф.
/// 12.05.2013
/// </summary>
public interface IDBO
{
    int ID { get; set; } //идентификатор элемента
    string Name { get; set; } //строковое представление элемента
    int list_order { get; set; } // порядок сортировки
    bool deleted { get; set; } //признак того, что элемент отмечен как удаленный. Добавил ЗЕФ 08.07.2011
    string comment { get; set; } //коментарий

    bool Load(); // Загрузка категории
    bool Insert(); // Вставка категории
    bool Update(); // Обновление категории
    bool Delete(); // Удаление категории
}
public class DBO:BindingForm
{
    public DBOInit ini = null;
    public string Error = "";

    public DBO()
    {
        Type type = this.GetType();
        if (ini == null)
        {
            ini = DBOFabric.GetInit(type.Name);
            if(!ini.IsInit)
                Init();
        }
    }
    public DBO(bool init)
    {
        Type type = this.GetType();
        if (ini == null)
        {
            ini = DBOFabric.GetInit(type.Name);
            if (!ini.IsInit)
                Init();
        }
    }
    /// <summary>Инициализация и формирование запросов</summary>
    /// <returns></returns>
    public bool Init()
    {
        Type type = this.GetType();
        PropertyInfo[] props = type.GetProperties();
        if (props.Length == 0)
        {
            Error = "Not found attributes in class " + type.Name;
            return false;
        }
        List<DBTableDesc> t = ini._Tables;
        string dbSelectFull = "";
        // перебор всех свойств, имеющих атрибут [db]
        // начинается с конца т.к. свойства базовых класов находятся в конце, а наследников в начале
        for(int j = props.Length - 1; j >= 0; j--)
        {
            PropertyInfo p = props[j];

            object[] attribs = p.GetCustomAttributes(typeof(dbAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                dbAttribute attrib = attribs[0] as dbAttribute;
                if (attrib.ReadOnly) continue;
                string DBField = attrib.Name == "" ? p.Name : attrib.Name;
                // поиск описания таблицы текущего свойства в списке или создание нового
                string tt = attrib.Table;
                DBTableDesc b = null;
                foreach(DBTableDesc i in t)
                    if (i.Table == tt)
                    {
                        b = i;
                        break;
                    }
                if (b == null)
                {
                    t.Add(new DBTableDesc() { Table = tt });
                    b = t[t.Count-1];
                }
                b.IsMain = attrib.IsMain; // является ли данная таблица главной
                b.Right = attrib.Right; // является ли данная таблица правой по отношению к базовой
                // определяем является ли данное поле ключевым
                object[] pka = p.GetCustomAttributes(typeof(pkAttribute), false);
                if (pka != null && pka.Length > 0)
                {
                    b.PrimaryKey = DBField;
                    // если больше одно первичного ключа, то остальные во вставке добавляем добляем в  параметры
                    if (b.PrimaryKeys.Count > 0)
                    {
                        b.dbInsert += "[" + DBField + "],";
                        b._insValues += "@" + b.Table + DBField + ",";
                    }

                    b.PrimaryKeys.Add(DBField);
                }
                else
                {
                    b.dbUpdate += "[" + DBField + "] = @" + b.Table + DBField + ",";
                    b.dbInsert += "[" + DBField + "],";
                    b._insValues += "@" + b.Table + DBField + ",";
                }
                b.dbInsertFull += "[" + DBField + "],";
                b._insValuesFull += "@" + b.Table + DBField + ",";

                dbSelectFull += ", [" + b.Table + "].[" + DBField + "] AS  [" + DBField + "]";
                // определяем является ли данное поле внешним ключом
                object[] fka = p.GetCustomAttributes(typeof(fkAttribute), false);
                if (fka != null && fka.Length > 0)
                {
                    fkAttribute fk = fka[0] as fkAttribute;
                    b.ForeignKey = DBField;
                    b.ForeignKeyBase = fk.Table + "." + fk.Field;
                    // добавление внешнего ключа для таблицы, если их нескокльо
                    b.ForeignKeys.Add(DBField);
                    b.ForeignKeyBases.Add(fk.Table + "." + fk.Field);
                }
            }
        }
        int n = t.Count;
        if (n <= 0) return false;
        // формирование части запроса - объединение таблиц и по каким полям 
        string table = "";
        for (int i = 0; i <n ; i++)
        {
            if (i > 0)
            {
                table += " " + (t[i].Right ? " RIGHT JOIN " : " LEFT JOIN ") + t[i].Table;
                // условие соедиенения дочерней и родительской таблиц для одного внешнего ключа
                //table += " ON " + t[i].Table + "." + t[i].ForeignKey + " = " + t[i].ForeignKeyBase;

                // сборка условия соедиенения дочерней и родительской таблиц с несколькими внешними ключами
                string cond = "";
                for(int j=0; j < t[i].ForeignKeys.Count; j++)
                {
                    cond += t[i].Table + "." + t[i].ForeignKeys[j] + " = " + t[i].ForeignKeyBases[j];
                    if (j < t[i].ForeignKeys.Count - 1)
                        cond += " AND ";
                }

                table += " ON " + cond;
            }
            else
                table = t[i].Table;
        }
        // определение, какое поле и из какой таблицы считать ключевым для SELECT запроса
        ini.dbKeyField = t[n - 1].PrimaryKey;
        ini.dbKeyFields = t[n - 1].PrimaryKeys;
        ini.dbKeyTable = t[n - 1].Table;
        for (int i = n - 1; i >= 0; i--)
        {
            if (t[i].IsMain)
            {
                ini.dbKeyField = t[i].PrimaryKey;
                ini.dbKeyFields = t[i].PrimaryKeys;
                ini.dbKeyTable = t[i].Table;
                break;
            }
        }

        dbSelectFull = dbSelectFull.Substring(1);


        ini.dbSelectList = "SELECT * FROM " + table + " ";
        // формирование запроса с одним ключевым полем
        //ini.dbSelect = "SELECT * FROM " + table + " WHERE " + ini.dbKeyTable + "." + ini.dbKeyField + "= @" + ini.dbKeyTable + ini.dbKeyField;
        
        // формирование главного запроса SELECT с несколькими ключевыми полями
        string sel_cond = "";// условие выборки по первичным ключам
        for (int j = 0; j < ini.dbKeyFields.Count; j++)
        {
            sel_cond += ini.dbKeyTable + "." + ini.dbKeyFields[j] + "= @" + ini.dbKeyTable + ini.dbKeyFields[j];
            if (j < ini.dbKeyFields.Count - 1)
                sel_cond += " AND ";
        }
        ini.dbSelect = "SELECT * FROM " + table + " WHERE " + sel_cond;
        
        // формирование запросов для каждой таблицы при несокльких соединенных
        foreach (DBTableDesc i in t)
        {
            string cond = "";// условие выборки по первичным ключам
            for (int j = 0; j < i.PrimaryKeys.Count; j++)
            {
                cond += "[" + i.PrimaryKeys[j] + "] = @" + i.Table + i.PrimaryKeys[j];
                if (j < i.PrimaryKeys.Count - 1)
                    cond += " AND ";
            }
            // Запрос выборки
            i.dbSelect = "SELECT * FROM [" + i.Table + "] WHERE " + cond;
            // Запрос удаления
            i.dbDelete = "DELETE FROM [" + i.Table + "] WHERE " + cond;
            
            // вставка
            i._insValues = i._insValues.Substring(0, i._insValues.Length - 1);
            i.dbInsert = i.dbInsert.Substring(0, i.dbInsert.Length - 1);
            i.dbInsert = "INSERT INTO [" + i.Table + "] (" + i.dbInsert + ") VALUES (" + i._insValues + ")";
            
            // полная вставка вместе с ключевым полем
            i._insValuesFull = i._insValuesFull.Substring(0, i._insValuesFull.Length - 1);
            i.dbInsertFull = i.dbInsertFull.Substring(0, i.dbInsertFull.Length - 1);
            i.dbInsertFull = "INSERT INTO [" + i.Table + "] (" + i.dbInsertFull + ") VALUES (" + i._insValuesFull + ")";

            // Запрос обновления
            i.dbUpdate = i.dbUpdate.Substring(0, i.dbUpdate.Length - 1);
            i.dbUpdate = "UPDATE [" + i.Table + "] SET " + i.dbUpdate + " WHERE " + cond;
        }
        ini.IsInit = true;
        return true;
    }
    /// <summary>Добавление к команде текущих параметров</summary>
    /// <returns>Успешность выполнения</returns>
    private void AddParams(DB cmd, string table, string noadd)
    {
        Type type = this.GetType();
        PropertyInfo[] fields = type.GetProperties();
        //Непонятно какие действия производит каждый оператор этого метода?
        foreach (PropertyInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(dbAttribute), false);
            if (attribs != null && attribs.Length > 0 && field.Name != noadd)
            {
                dbAttribute attrib = attribs[0] as dbAttribute;
                if (attrib.ReadOnly)continue;
                if (attrib.Table != table)continue;
                string DBField = attrib.Name == "" ? field.Name : attrib.Name;
                cmd.ParamAdd(table + DBField, field.GetValue(this, null));
            }
        }

    }
    /// <summary>Загрузка данных из БД</summary>
    /// <returns>Успешность выполнения</returns>
    public bool Load()
    {
        DB cmd = new DB(ini.dbSelect);
        // Передача параметров в запрос для одного ключевого поля
        //object value = this.GetType().GetProperty(ini.dbKeyField).GetValue(this, null);
        //cmd.ParamAdd(ini.dbKeyTable + ini.dbKeyField, value);

        // Передача параметров в запрос для нескольких ключевых полей
        for (int j = 0; j < ini.dbKeyFields.Count; j++)
        {
            object value = this.GetType().GetProperty(ini.dbKeyFields[j]).GetValue(this, null);
            cmd.ParamAdd(ini.dbKeyTable + ini.dbKeyFields[j], value);
        }

        cmd.ExecuteReader();
        if (cmd.Read())
        {
            Type type = this.GetType();
            PropertyInfo[] fields = type.GetProperties();
            foreach (PropertyInfo field in fields)
            {
                object[] attribs = field.GetCustomAttributes(typeof(dbAttribute), false);
                if (attribs != null && attribs.Length > 0)
                {
                    dbAttribute attrib = attribs[0] as dbAttribute;
                    string DBField;
                    if (attrib.Name == "")
                        DBField = field.Name;
                    else
                        DBField = attrib.Name;
                    try
                    {
                        if (!Convert.IsDBNull(cmd[DBField]))
                            field.SetValue(this, cmd[DBField],null);
                    }
                    catch (IndexOutOfRangeException)
                    {
                    }
                }
            }
            cmd.Close();
            return true;
        }
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool Load()\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
    }
    /// <summary> Удаление записи из таблицы</summary>
    /// <returns>Успешность выполнения</returns>
    public bool Delete()
    {
        string e = "";
        foreach (DBTableDesc i in ini._Tables)
        {
            DB cmd = new DB(i.dbDelete);
            //object value = this.GetType().GetProperty(i.PrimaryKey).GetValue(this,null);
            //cmd.ParamAdd(i.Table + i.PrimaryKey, value);

            // Передача параметров в запрос для нескольких ключевых полей
            for (int j = 0; j < i.PrimaryKeys.Count; j++)
            {
                object value = this.GetType().GetProperty(i.PrimaryKeys[j]).GetValue(this, null);
                cmd.ParamAdd(i.Table + i.PrimaryKeys[j], value);
            }
            if (!cmd.ExecuteNonQuery())
                e+=cmd.Error + "\n";
        }
        if (e != "") Error += e + "\n";
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool DeleteDemandMedicament()\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return e == "";
    }
    /// <summary>Удаление записи из конкретной таблицы</summary>
    /// <param name="table">Название таблицы</param>
    /// <returns>Успешность выполнения</returns>
    public bool Delete(string table)
    {
        string e = "";
        foreach (DBTableDesc i in ini._Tables)
            if (i.Table == table)
            {
                DB cmd = new DB(i.dbDelete);
                //object value = this.GetType().GetProperty(i.PrimaryKey).GetValue(this, null);
                //cmd.ParamAdd(i.Table + i.PrimaryKey, value);
                // Передача параметров в запрос для нескольких ключевых полей
                for (int j = 0; j < i.PrimaryKeys.Count; j++)
                {
                    object value = this.GetType().GetProperty(i.PrimaryKeys[j]).GetValue(this, null);
                    cmd.ParamAdd(i.Table + i.PrimaryKeys[j], value);
                }
                if (!cmd.ExecuteNonQuery()) e += cmd.Error + "\n";
                break;
            }
        if (e != "") Error += e + "\n";
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool DeleteDemandMedicament(string table)\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return e == "";
    }
    /// <summary>Обновление записи в таблице</summary>
    /// <returns>Успешность выполнения</returns>
    public bool Update()
    {
        string e = "";
        foreach (DBTableDesc i in ini._Tables)
        {
            DB cmd = new DB(i.dbUpdate);
            AddParams(cmd, i.Table, "");//
            if (!cmd.ExecuteNonQuery()) e += cmd.Error + "\n";
        }
        //Обработка ошибочного результата при выполнении запроса
        if (e != "") Error += e + "\n";
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool Update()\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return e == "";
    }
    /// <summary>Обновление записи в конкретной таблице</summary>
    /// <param name="table">Название таблицы</param>
    /// <returns>Успешность выполнения</returns>
    public bool Update(string table)
    {
        string e = "";
        foreach (DBTableDesc i in ini._Tables)
            if (i.Table == table)
            {
                DB cmd = new DB(i.dbUpdate);
                AddParams(cmd, i.Table, "");
                if (!cmd.ExecuteNonQuery()) e += cmd.Error + "\n";
                break;
            }
        if (e != "") Error += e + "\n";
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: Update(string table)\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return e == "";
    }
    /// <summary>Вставка записи в таблицу.Работает пока только для классов с одной таблицей</summary>
    public bool Insert()
    {
        if (ini._Tables.Count == 1)
        {
            return Insert(ini._Tables[0].Table);
        }
        return false;
    }
    /// <summary>Вставка записи в таблицу</summary>
    /// <param name="table">Название таблицы</param>
    /// <returns>Успешность выполнения</returns>
    public bool Insert(string table)
    {
        return Insert(table, true);
    }
    /// <summary>Выполняется встака значений свойств экземпляра класса в таблицу базы данных исключая значение ключевого поля
    /// Как правило это таблицы с первичным, самогенерируемым ключем. Поэтому его значение не удасться вставить впринцыпе.
    /// Марьенков Е.В.
    /// </summary>
    /// <param name="table"></param>
    /// <param name="identity"></param>
    /// <returns></returns>
    public bool Insert(string table, bool identity)
    {
        foreach (DBTableDesc i in ini._Tables)
            if (i.Table == table)
            {
                if (i.PrimaryKeys.Count <=0) // если у таблицы отсутсвует главный ключ, то вставляем полностью
                    return InsertFull(table);

                DB cmd = new DB(i.dbInsert);
                AddParams(cmd, i.Table, i.PrimaryKeys[0]);
                i.Identity = 0;
                if (cmd.ExecuteNonQuery(identity))
                {
                    // Уставнавливаем значение полученное в результате добавления
                    if (identity) i.Identity = (int)cmd.Identity;
                    // Ищем поле главного ключа и устанавливам ему значение 
                    SetValue(i.PrimaryKeys[0], i.Identity);

                    return true;
                }
                else
                    Error += cmd.Error + "\n";
                break;
            }
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool Insert(string table, bool identity)\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
    }
    /// <summary>Выполняется встака значений свойств экземпляра класса в таблицу базы данных включая значение ключевого поля
    /// Этот метод используется для связанных таблиц не имеющих самогенерируемых ключевых полей. Как правило это таблицы со вторичным
    /// ключевым полем
    /// </summary>
    /// <param name="table"></param>
    /// <returns></returns>
    public bool InsertFull(string table)
    {
        foreach (DBTableDesc i in ini._Tables)
            if (i.Table == table)
            {
                DB cmd = new DB(i.dbInsertFull);
                AddParams(cmd, i.Table, "");
                i.Identity = 0;
                if (cmd.ExecuteNonQuery(false))
                    return true;
                else
                    Error += cmd.Error + "\n";
                break;
            }
        if (DB.IsMessageError && Error != "")
            MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBO\n" + "Функция: public bool InsertFull(string table, bool identity)\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
    }
    public int Identity(string table)
    {
        foreach (DBTableDesc i in ini._Tables)
            if (i.Table == table)
                return i.Identity;
        return 0;
    }
    public int GetCount(string table)
    {
        DB cmd = new DB("SELECT Count(*) FROM ["+table+"]");
        return (int)cmd.ExecuteScalar();
    }
    public void Clear()
    {
        Type type = this.GetType();
        PropertyInfo[] fields = type.GetProperties();
        foreach (PropertyInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(dbAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                if (field.PropertyType == typeof(int)) field.SetValue(this, 0, null);
                if (field.PropertyType == typeof(string)) field.SetValue(this, "", null);
                if (field.PropertyType == typeof(float)) field.SetValue(this, 0f, null);
                if (field.PropertyType == typeof(double)) field.SetValue(this, 0.0, null);
                if (field.PropertyType == typeof(char)) field.SetValue(this, 0, null);
                if (field.PropertyType == typeof(byte)) field.SetValue(this, 0, null);
                if (field.PropertyType == typeof(decimal)) field.SetValue(this, (decimal)0, null);
                if (field.PropertyType == typeof(long)) field.SetValue(this, 0, null);
                if (field.PropertyType == typeof(short)) field.SetValue(this, 0, null);
                if (field.PropertyType == typeof(object)) field.SetValue(this, null, null);
                if (field.PropertyType == typeof(bool)) field.SetValue(this, false, null);
                if (field.PropertyType == typeof(DateTime)) field.SetValue(this, new DateTime(1900, 1, 1), null);
            }
        }
    }
    /// <summary>Установка значения для определенного свойства</summary>
    /// <param name="field">Имя свойства</param>
    /// <param name="value">Значение свойства</param>
    public void SetValue(string field, object value)
    {
        this.GetType().GetProperty(field).SetValue(this, value, null);
    }
}



