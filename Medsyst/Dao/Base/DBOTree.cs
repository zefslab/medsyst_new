﻿//3.07.09
//добавлено описание работы с классом DBOTree и интерфейсом ITreeItem
//7.07.09
//изменена роль свойства TreeNode.Tag при формировании дерева: теперь хранит ID-ключ вместо порядкового номера во временном списке
//21.07.09
//удаление узла и всех его подузлов из дерева и БД

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

//   Использование метода:
//1. Унаследовать класс элемента от класса DBO и интерфейса ITreeItem.
//2. Реализовать интерфейс ITreeItem в классе элемента.
//3. Описать класс коллекций, наследуемый от класса DBOTree (вместо DBOList) с указанием типа элемента.
//4. В событии Load формы загрузить данные из таблицы (метод DBOTree.Load).
//5. Вызвать метод DBOTree.FillTree с указанием экземпляра TreeView, предварительно размещенного на форме.
using Medsyst.Dao.Base;


/// <summary>Интерфейс, позволяющий использовать данные из БД
    /// для формирования иерархической структуры
    /// Марьенков Е.В.
    /// </summary>
    public interface ITreeItem
    {
        int ID { get; set; } //идентификатор элемента
        int ParentID { get; set; } //родительский идентификатор
        string Text { get; set; } //строковое представление элемента
        int list_order { get; set; } // порядок сортировки
        bool visible { get; set; } //условие отображения элемента коллекции в визуальном элементе 
                                   //TreeView, установливаемое на основе какого либо критеия. Добавил ЗЕФ. 12.05.2011
        bool deleted { get; set; } //признак того, что элемент дерева отмечен как удаленный. Добавил ЗЕФ 08.07.2011

        bool Load(); // Загрузка категории
        bool Insert(); // Вставка категории
        bool Update(); // Обновление категории
        bool Delete(); // Удаление категории
    }
    /// <summary>Делегат для перебора узлов дерева (Tree - дерево, Node - очередной узел, 
    /// автор: Чирков Е.О.
    /// дата создания 18.09.09<summary>
    /// <param name="Tree"></param>
    /// <param name="Node"></param>
    /// <param name="Param">произвольный параметр</param>
    /// <returns> возвращение false - конец перебора</returns>
    public delegate bool TreeIterationProc(TreeView Tree, TreeNode Node, ref object Param);
    ///<summary>Интерфейс для дерева категорий</summary>
    public interface IDBOTree
    {
        bool ShowDeleted { get; set; }//Добавил Заплатин Е.Ф. 07.07.2011
        bool Load();
        /// <summary>Заполнение элемента управления TreeView значениями из древовидной коллекции. Марьенков Е.В. 01.09.09 </summary>
        void FillTree(TreeView tree);
        /// <summary>Проверка на наличие вложений в указанном узле. Возвращает - true если вложения имеются. ЗЕФ 07.07.2011</summary>
        bool _checkAttachments(TreeNode node, bool deleted);
        /// <summary>Удаление узла из дерева категорий. При успешном выполнении удаления возвращает - true. ЗЕФ 07.07.2011</summary>
        bool DeleteFromTable(TreeNode node);
        /// <summary>Восстановление узла отмеченного как удаленный. При успешном восстановлении возвращает - true. ЗЕФ 07.07.2011</summary>
        bool Restore(TreeNode node);
        TreeNode AddNode(TreeView tree, TreeNode node, int key, string Text);
       
    }
    public class DBOTree<T> : DBOList<T>, IDBOTree 
        where T : DBO, ITreeItem, new()
    {
        //Подготовка интерфейса
        public bool ShowDeleted { get { return showdeleted; } set { showdeleted = value; } } //Признак отображения удаленных элементов в дереве. Добавил Заплатин Е.Ф. 07.07.2011
        /// <summary>Добавление нового узла в качестве дочернего к указанному узлу или в корень дерева
        /// Заплатин Е.Ф.
        /// 08.07.2011
        /// </summary>
        /// <param name="tree">дерево</param>
        /// <param name="node">узел в который нужно добавить новый узел как дочерний</param>
        /// <param name="key">идентификатор объекта, который хранится в узле</param>
        /// <param name="Text">Наименование объекта</param>
        /// <returns>возращает ссылку на добавленный узел</returns>
        public TreeNode AddNode(TreeView tree, TreeNode node, int key, string Text)
        {
            TreeNode nodeNew = new TreeNode();
            if (node != null) 
                nodeNew = node.Nodes.Add(key.ToString(),Text); //добавление призводится в переданный узел
            else
                nodeNew = tree.Nodes.Add(key.ToString(),Text);//добавление производится в корень дерева
            nodeNew.Tag = key;//Свойство Tag  должно содержать идентификатор указывающий на объект
            return nodeNew;
        }
        /// <summary>Поиск номера позиции элемента в коллекции с указанным идентификатором ID
        /// Марьенков Е.В.
        /// </summary>
        /// <param name="ID">идентификатор объекта в коллекции</param>
        /// <returns>позиция искомого объекта</returns>
        public int GetIndexFromID(int ID)
        {
            for (int k = 0; k < this.Count; k++)
            {
                if (this[k].ID == ID) return k;
            }
            return -1;
        }
        /// <summary>Переопределяемый потомками метод для поиска вложений в указанном узле 
        /// Заплатин Е.Ф.
        /// 08.07.2011
        /// </summary>
        /// <param name="node">узел в котором проверяется наличие вложенных узлов или элементов</param>
        /// <param name="deleted">учитывать или нет удаленные вложенные узлы</param>
        /// <returns></returns>
        public virtual bool _checkAttachments(TreeNode node, bool deleted)
        {
            return false;//Полная реализация в переопределяемых методах классов-потомков
        }
        /// <summary>Удаление узла из таблицы БД.
        /// Заплатин Е.Ф.
        /// 07.07.2011
        /// </summary>
        /// <param name="node">удаляемый узел</param>
        public bool DeleteFromTable(TreeNode node)
        {
            int id = (int)node.Tag;
            int i = GetIndexFromID(id);
            if (this[i].deleted)
            {//Узел уже был отмечен ранее как удаленный, поэтому требуется его полное удаление из базы данных
                if (_checkAttachments(node, true))//проверяется наличие вложений count учетом удаленных узлов
                {
                    MessageBox.Show("Не возможно удалить без возможности последующего восстановления категорию, в которой содержатся вложенные, удаленные категории." +
                                    "Необходимо сначала удалить все вложенные категории.",
                                    "Удаление категории", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
                if (DialogResult.No == MessageBox.Show("Удалить без возможности восстановления?",
                                        "Подтверждение удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                    return false;
                //Если имеются вложенные категории то 
                this[i].Delete();
            }
            else
            {//Узел небыл отмечен как удаленный
                //Проверка на наличие вложенных категорий и медикаментов 
                if (_checkAttachments(node, false))//проверяется наличие вложений без учета удаленных узлов
                {
                    MessageBox.Show("Не возможно удалить категорию, в которой содержатся вложенные категории или элементы. " +
                                    "Необходимо предварительно удалить или перенести все вложенные категории и элементы.",
                                    "Удаление категории", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }

                if (DialogResult.No == MessageBox.Show("Произвести удаление?",
                                                        "Подтверждение удаления", 
                                                        MessageBoxButtons.YesNo, 
                                                        MessageBoxIcon.Question))
                    return false;
                this[i].deleted = true;//Установка отметки об удалении
                this[i].Update();
            }
            return true;
        }
        /// <summary>Восстановление удаленного узла в таблице БД.
        /// Заплатин Е.Ф.
        /// 08.07.2011
        /// </summary>
        /// <param name="node">восстанавливаемый узел</param>
        public bool Restore(TreeNode node)
        {
            int id = (int)node.Tag;
            int i = GetIndexFromID(id);
            if (this[i].deleted)
            {//Узел уже был отмечен ранее как удаленный
                this[i].deleted = false;//Снятие отметки об удалении
                this[i].Update();
                return true;
            }
            else
            {
                //Узел не был ранее отмечен как удаленный, поэтому нечего и восстанавливать 
                MessageBox.Show("Выберите категорию, отмеченную как удаленная, прежде чем приступать к восстановлению.",
                                "Восстановление удаленной категории", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
        /// <summary>Заполнение элемента управления TreeView значениями из древовидной коллекции
        /// Марьенков Е.В.
        /// 01.09.09
        /// </summary>
        public void FillTree(TreeView tree)
        {
            //очистка дерева
            tree.Nodes.Clear();
            _FillTree(tree, null);
        }
        /// <summary>Рекурсивное заполнение дерева
        /// автор: Марьенков Е.В.
        /// дата создания 01.09.09
        /// </summary>
        private void _FillTree(TreeView tree, TreeNode treenode)
        {
            int parent = treenode == null ? 0 : int.Parse(treenode.Name);
            for (int i = 0; i < this.Count; i++)
                if (this[i].ParentID == parent)
                {
                    TreeNode node = AddNode(tree, treenode, this[i].ID, this[i].Text);
                    if (this[i].deleted) node.ForeColor = Color.Plum; //Изменение цвета шрифта для того, чтобы отличать удаленные элементы от неудаленных
                    _FillTree(tree, node);//вызов заполнения для только что добавленого узла
                }
        }
        /// <summary>Заполнение визуального элемента TreeView только теми элементами коллекции, у которых установлено
        /// свойство visible в true. Этот метод используется для группировки древовидных структур. Например категории медикаментов 
        /// распределяются по группам. Этот метод создан для того чтобы визуально можно было отслеживать перемещение отдельных ветвей деревьев по группам
        /// Заплатин Е.Ф.
        /// 12.05.2011
        /// </summary>
        /// <param name="tree">визуальный элемент дерева</param>
        public void FillTreeVisibleNodes(TreeView tree)
        {
            //очистка дерева
            tree.Nodes.Clear();
            _FillTreeVisibleNodes(tree, null, null);//анализ и добавление начинается с корневых элементов коллекции
        }
        /// <summary> рекурсия для заполнения дерева только теми элементами коллекции которые отмечены в свойстве visible значением true
        /// Заплатин Е.Ф.
        /// 12.05.2011
        /// </summary>
        /// <param name="tree">Дерево, наполняемое узлами</param>
        /// <param name="carrentnode">анализируемый узел дерева</param>
        /// <param name="visible_parent_node">последний предок анализируемого узла, который отмечен как отображаемый</param>
        private void _FillTreeVisibleNodes(TreeView tree, int? parent , TreeNode visible_parent_node)//TreeNode carrentnode
        {
            
            //int parent = carrentnode == null ? 0 : int.Parse(carrentnode.Name);// ноль присваивается только узлам корневого уровня
            if (parent == null)
                parent = 0;//анализируются корневые элементы 
            for (int i = 0; i < this.Count; i++)//Обрабатываются все элементы коллекции
            {
                if (this[i].ParentID == parent)
                {//Найден элемент, являющийся дочерним для переданного узла
                    if (this[i].visible == true)
                    {//Элемент коллекции отмечен как отображаемый, поэтому его следует добавить в TreeView
                        TreeNode node = AddNode(tree, visible_parent_node, this[i].ID, this[i].Text);//Добавляется новый узел в дерево
                        node.Tag = this[i].ID; //свойство Tag хранит идентификатор элемента
                        //Передается идентификатор this[i].ID элемента коллекции для поиска и анализа дочерних ему элементов
                        //В качестве родительского узла быдет использоваться только что добавленный узел
                        _FillTreeVisibleNodes(tree, this[i].ID, node);
                    }
                    else
                    {//Элемент коллекции отмечен как неотображаемый, поэтому новый узел не добавляется в дерево
                        //В качестве родительского узла быдет использоваться переданный в качестве параметра узел
                        _FillTreeVisibleNodes(tree, this[i].ID, visible_parent_node);
                    }

                }
            }
        }
    }

  
