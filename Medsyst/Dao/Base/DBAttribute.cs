﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/// <summary>
/// ЗЕФ. Описание атрибутов используемых при описании классов
/// </summary>
/// <summary>
/// Пользовательский аттрибут, обозначающий что поле сохраняется в БД
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
public class dbAttribute : System.Attribute
{
    public string Name = "";
    public string Table = "";
    private bool _IsMain = true;
    private bool _Right = true;

    public dbAttribute() { }
    public dbAttribute(string table, string name)
    {
        Name = name;
        Table = table;
    }
    public dbAttribute(string table)
    {
        Table = table;
    }
    /// <summary>
    /// Только чтение поля из БД, без возможности записи и обновления
    /// </summary>
    public bool ReadOnly { get; set; }
    /// <summary>
    /// Явлеется ли наследуемая таблица главной, т.е. операции выборки данных осуществляются по ключевому полю этой таблицы
    /// </summary>
    public bool IsMain { get { return _IsMain; } set { _IsMain = value; } }
    /// <summary>
    /// Является ли наследуемая таблица правой по отношению к базовой в запросе JOIN
    /// </summary>
    public bool Right { get { return _Right; } set { _Right = value; } }
}
/// <summary>
/// Атрибут указывающий, что данное свойство является ключевым полем
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
public class pkAttribute : System.Attribute
{
    public pkAttribute() { }
}

// Атрибут, указывающий что данное свойство ключом к внешнему ключу указанному в парметрах
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
public class fkAttribute : System.Attribute
{
    public string Table = "";
    public string Field = "";

    public fkAttribute(string table, string field)
    {
        Field = field;
        Table = table;
    }
}

/// <summary>
/// Пользовательский аттрибут, обозначающий что поле сохраняется в БД
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
public class DBFieldAttribute : System.Attribute
{
    public string name = "";
    private bool readOnly = false;

    public DBFieldAttribute() { }
    public DBFieldAttribute(string name)
    {
        this.name = name;
    }
    /// <summary>
    /// Только чтение поля из БД, без возможности записи и обновления
    /// </summary>
    public bool ReadOnly
    {
        get { return readOnly; }
        set { readOnly = value; }
    }
}
