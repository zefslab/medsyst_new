﻿using System;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Common;
using System.Windows.Forms;
using rsdev.MCV.View;
using System.Collections.Generic;
using System.Xml;
using Medsyst.Dao.Base;

/// <summary>
/// Методы работы с БД
/// </summary>
public class DB: IDisposable
{
    //Коннектор к базе данных сделан публичным и статическим, 
    //чтобы можно было получать доступ открытию транзакций. ЗЕФ 19.05.2015
    private  SqlConnection _connection = null;
    
    SqlCommand sqlCommand = null;
    
    public decimal Identity = 0;//...

    string _Error = "";//описание ошибки
    
    public static string _cs = "";//строка подключения

    public static bool IsMessageError = false;//...

    public string Error
    {
        get { return _Error; }
    }
    
    public CommandType Type
    {
        get { return sqlCommand.CommandType; }
        set { sqlCommand.CommandType = value; }
    }
    
    public SqlParameterCollection Parameters = null;
    
    public DbDataReader Reader = null;

	public DB()	{}
    public DB(CommandType type, string command)
    {
        Create(type, command);
    }
    public DB(string command)
    {
        Create(CommandType.Text, command);
    }
    ~DB()
    {
        Close();
    }
    public void Dispose()
    {
        Close();
    }

    public bool IsColumn(string column)
    {
        return Reader.GetSchemaTable().Columns.Contains(column);
    }

    public object this[string name]
    {
        get {return Reader[name]; }
    }
    
    public object this[int ordinal]
    {
        get { return Reader[ordinal]; }
    }

    public bool Read()
    {
        if (Reader == null) return false;
        return Reader.Read();
    }

    /// <summary>
    /// Создание команды
    /// </summary>
    /// <param name="type">Тип команды</param>
    /// <param name="command">команда SQL</param>
    public void Create(CommandType type,string command)
    {
        Close();
        /// Используется синглтон для работы через одно единственное подключение
        /// Это необходимо для использования транзакций на несколько выполняемых
        /// команд в рамках одного подключения
        /// 
        _connection = MyDbConnection.Instance(CS);
 
        sqlCommand = new SqlCommand(command, _connection) {CommandType = type};

        Parameters = sqlCommand.Parameters;
    }
    public void Create(string command)
    {
        Create(CommandType.Text,command);
    }
    
    // Закрытие команды
    public void Close()
    {
        if (Reader != null && !Reader.IsClosed)
        {
            Reader.Close();
            Reader = null;
        }
    }


    public bool ExecuteNonQuery()
    {
        return ExecuteNonQuery(false);
    }
    /// <summary>
    /// Выполнить без возврата результат
    /// </summary>
    /// <returns><count>true</count> - если успешно, иначе <count>false</count></returns>
    public bool ExecuteNonQuery(bool isInsert)
    {
        _Error = "";
        try{
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            sqlCommand.ExecuteNonQuery();

            if (isInsert)
            {
                SqlCommand ident = new SqlCommand("SELECT @@Identity AS [IDENT]", _connection);
                SqlDataReader dr = ident.ExecuteReader();
                if (dr.Read())
                    Identity = (decimal)dr["IDENT"];
                dr.Close();
            }
        }
        catch (SqlException ex){
            _Error = ex.Message;
        }
        finally{
            Close();}
        if (_Error.Length > 0) return false;
        return true;
    }
    /// <summary>
    /// Выполнить c возвратом результата
    /// Разработана в основном для выполнения инструкции DELETE и возврата количества удаленных строк
    /// для информирования пользователей об эфективности проведенной операции
    /// Заплатин Е.Ф.
    /// 20.01.2013
    /// </summary>
    /// <returns>Количество строк примененых в команде</returns>
 
    public int ExecuteQuery()
    {
        _Error = "";
        int _i = 0; //Количество строк примененных в выполненной операции
        try
        {
            if(_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            _i = sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            _Error = ex.Message;
        }
        finally
        {
            Close();
        }
        
        return _i;
    }
    /// <summary>
    /// Выполнить с возвратом одиночного результата
    /// </summary>
    /// <returns>Требуемый объект если удачно, иначе <count>null</count></returns>
    public object ExecuteScalar()
    {
        _Error = "";
        object o=null;
        try
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            o = sqlCommand.ExecuteScalar();
            
        }
        catch (SqlException ex)
        {
            _Error = ex.Message;
        }
        finally { Close(); }
        if (_Error.Length > 0) return null;
        return o;
    }
    /// <summary>
    /// Выполнить с возвратом DbDataReader для чтения выборки
    /// </summary>
    /// <returns>Требуемый объект DbDataReader, иначе <count>null</count></returns>
    public DbDataReader ExecuteReader()
    {
        _Error = "";
        try
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            Reader = sqlCommand.ExecuteReader();
        }
        catch (SqlException ex)
        {
            _Error = ex.Message;
            Close();
        }
        if (_Error.Length > 0) return null;
        return Reader;
    }

    public DbDataReader ExecuteInDataTable(DataTable dt)
    {
        _Error = "";
        try
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
                
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);     
        }
        catch (SqlException ex)
        {
            _Error = ex.Message;
            Close();
        }
        if (_Error.Length > 0) return null;
        return Reader;
    }

    /// <summary>
    /// Добавление параметров со значением
    /// </summary>
    /// <param name="parameterName">Имя параметра</param>
    /// <param name="value">Значение параметра</param>
    /// <returns>Параметр</returns>
    public SqlParameter ParamAdd(string parameterName, object value)
    {
        return sqlCommand.Parameters.AddWithValue(parameterName, value);
    }

    /// <summary>
    /// Текущая строка подключения
    /// </summary>
    public static string CS
    {
        get { return _cs; }
    }

    /// <summary>
    /// Считывание строк подключения из файла web.config
    /// </summary>
    /// <returns>Строка подключения</returns>
    public static string ConnectionString()
    {
        _cs = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;

        // Проверка соединения с БД, занимает двольно большое время
        DB cmd = new DB("SELECT COUNT(*) FROM Measure");

        cmd.ExecuteScalar();

        if (cmd.Error != "")
        {
            MessageBox.Show("Настройте список подключений к базе данных, " +
                            "действующее подключение находится не в первой строке," +
                            " и поэтому его определение занимет длительное время!",
                "Подключение к базе данных",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return String.Empty;
        }
        else
        {
            return _cs;
        }



    }
}


/// <summary>
/// Базовый класс для объектов сохраняемых в БД
/// </summary>
public class DBObject : BindingForm
{
    public string dbTable = "";       // Имя таблицы в БД соответствующей классу
    public string dbKey = "Id";       // Имя первичного ключа

    public string dbSelect;      // Запрос выборка
    public string dbInsert;      // Вставка записи
    public string dbUpdate;      // Обновление записи
    public string dbDelete;      // Удаление записи
    public int Identity = 0;

    public string Error;

    /// <summary>
    /// Инициализация команд SQL
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    protected bool Init()
    {
        Type type = this.GetType();
        FieldInfo[] fields = type.GetFields();
        if (fields.Length == 0)
        {
            Error = "Not found attributes in class " + type.Name;
            return false;
        }
        dbSelect = "SELECT * FROM [" + dbTable + "] WHERE [" + dbKey + "]= @" + dbKey;
        dbDelete = "DELETE FROM [" + dbTable + "] WHERE [" + dbKey + "]= @" + dbKey;
        dbInsert = "INSERT INTO [" + dbTable + "] ";
        dbUpdate = "UPDATE [" + dbTable + "] SET";
        StringBuilder insFields = new StringBuilder();
        StringBuilder insValues = new StringBuilder();
        StringBuilder update = new StringBuilder();

        foreach (FieldInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(DBFieldAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                DBFieldAttribute attrib = attribs[0] as DBFieldAttribute;
                if (attrib.ReadOnly)
                    continue;
                string DBField;
                if (attrib.name == "")
                    DBField = field.Name;
                else
                    DBField = attrib.name;
                if (field.Name != dbKey)
                {
                    insFields.Append(",[" + DBField + "]");
                    insValues.Append(",@" + DBField);
                    update.Append(",[" + DBField + "]=@" + DBField);
                }
            }
        }
        insFields[0] = '(';
        insValues[0] = '(';
        dbInsert += insFields + ") VALUES " + insValues + ")";

        update[0] = ' ';
        dbUpdate += update + " WHERE [" + dbKey + "]= @" + dbKey;
        return true;
    }

    /// <summary>
    /// Добавление к команде текущих параметров
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    private void AddParams(DB cmd, string noadd)
    {
        Type type = this.GetType();
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(DBFieldAttribute), false);
            if (attribs != null && attribs.Length > 0 && field.Name != noadd)
            {
                DBFieldAttribute attrib = attribs[0] as DBFieldAttribute;
                if (attrib.ReadOnly) 
                    continue;
                string DBField;
                if (attrib.name == "")
                    DBField = field.Name;
                else
                    DBField = attrib.name;
                cmd.ParamAdd(DBField, field.GetValue(this));
            }
        }

    }

    /// <summary>
    /// Загрузка записи из таблицы
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    public bool Load()
    {
        DB cmd = new DB(dbSelect);
        object value = this.GetType().GetField(dbKey).GetValue(this);
        cmd.ParamAdd(dbKey, value);
        cmd.ExecuteReader();
        if (cmd.Read())
        {
            Type type = this.GetType();
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                object[] attribs = field.GetCustomAttributes(typeof(DBFieldAttribute), false);
                if (attribs != null && attribs.Length > 0)
                {
                    DBFieldAttribute attrib = attribs[0] as DBFieldAttribute;
                    string DBField;
                    if (attrib.name == "")
                        DBField = field.Name;
                    else
                        DBField = attrib.name;
                    try
                    {
                        if (!Convert.IsDBNull(cmd[DBField]))
                            field.SetValue(this, cmd[DBField]);
                    }
                    catch (IndexOutOfRangeException)
                    {
                    }
                }
            }
            cmd.Close();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Удаление записи из таблицы
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    public bool Delete()
    {
        DB cmd = new DB(dbDelete);
        object value = this.GetType().GetField(dbKey).GetValue(this);
        cmd.ParamAdd(dbKey, value);
        if (cmd.ExecuteNonQuery()) return true;
        Error = cmd.Error;
        return false;
    }

    /// <summary>
    /// Вставка записи в таблицу
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    public bool Insert()
    {
        return Insert(true);
    }
    public bool Insert(bool identity)
    {
        DB cmd = new DB(dbInsert);
        AddParams(cmd, dbKey);
        Identity = 0;
        if (cmd.ExecuteNonQuery(identity))
        {
            if(identity)Identity = (int)cmd.Identity;
            return true;
        }
        Error = cmd.Error;
        return false;
    }

    /// <summary>
    /// Обновление записи в таблице
    /// </summary>
    /// <returns>Успешность выполнения</returns>
    public bool Update()
    {
        DB cmd = new DB(dbUpdate);
        AddParams(cmd, "");
        if (cmd.ExecuteNonQuery()) return true;
        Error = cmd.Error;
        return false;
    }
    
    /// <summary>
    /// Очистка полей, приведение их к значению по умолчанию
    /// </summary>
    public void Release()
    {
        Type type = this.GetType();
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(DBFieldAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                if(field.FieldType == typeof(string))
                    field.SetValue(this, "");
                else if (field.FieldType == typeof(int) 
                        || field.FieldType == typeof(long) 
                        || field.FieldType == typeof(byte) 
                        || field.FieldType == typeof(int)
                        || field.FieldType == typeof(float)
                        || field.FieldType == typeof(double)
                        || field.FieldType == typeof(char)
                        || field.FieldType == typeof(decimal))
                    field.SetValue(this, 0);
                else if (field.FieldType == typeof(bool))
                    field.SetValue(this, false);
                else if (field.FieldType == typeof(DateTime))
                    field.SetValue(this, new DateTime(1,1,1));
                else
                    field.SetValue(this, null);
            }
        }
    }
}

/// <summary>
/// Базовый класс для таблиц БД
/// </summary>
public class DBTable<T> where T: new()
{
    /// <summary>
    /// Класс ассоцирующий имя столбца из таблицы БД с полем в классе
    /// </summary>
    class FieldAssoc
    {
        public FieldInfo Info;
        public string Name;
    }
    class PropertyAssoc
    {
        public PropertyInfo Info;
        public string Name;
    }

    public string dbTable = ""; // Имя таблицы в БД соответствующей классу
    public string dbKey = "Id"; // Имя первичного ключа

    public string dbSelect = "";// Запрос выборка
    public string dbWhere = ""; // Условие выборки
    public string dbOrder = ""; // Сортировка

    public Type Type = null;    // класс используемый как основа

    public List<T> Items;     // Список записей

    public DataTable _table;

    public string Error;

    public Control ToDataSource = null;//...

    // Список полей загружаемых из таблицы БД
    private ArrayList FieldsDB;
    private List<PropertyAssoc> PropertiesDB;

    private BindingSource _bs = new BindingSource();

    public DBTable()
    {
        Items = new List<T>();
        FieldsDB = new ArrayList();
        PropertiesDB = new List<PropertyAssoc>();
        Type type = typeof(T);

        // поиск полей объекта
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo field in fields)
        {
            object[] attribs = field.GetCustomAttributes(typeof(DBFieldAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                DBFieldAttribute attrib = attribs[0] as DBFieldAttribute;
                FieldAssoc fa = new FieldAssoc();
                fa.Info = field;
                if (attrib.name == "")
                    fa.Name = field.Name;
                else
                    fa.Name = attrib.name;
                FieldsDB.Add(fa);
            }
        }
        // поиск свойств объекта
        PropertyInfo[] properties = type.GetProperties();
        foreach (PropertyInfo property in properties)
        {
            object[] attribs = property.GetCustomAttributes(typeof(DBFieldAttribute), false);
            if (attribs != null && attribs.Length > 0)
            {
                DBFieldAttribute attrib = attribs[0] as DBFieldAttribute;
                PropertyAssoc fa = new PropertyAssoc();
                fa.Info = property;
                if (attrib.name == "")
                    fa.Name = property.Name;
                else
                    fa.Name = attrib.name;
                PropertiesDB.Add(fa);
            }
        }
        _table = new DataTable(dbTable);
    }

    public int Count
    {
        get { return Items.Count; }
    }

    public T this[int index]
    {
        get { return (T)Items[index]; }
    }

    public bool Load()
    {
        DB cmd = new DB(dbSelect);
        //cmd.ExecuteReader();
        //while (cmd.Read())
        //{
        //    T elem = new T();
        //    foreach (FieldAssoc f in FieldsDB)
        //        if (!(cmd[f.Name] is DBNull))
        //            f.Info.SetValue(elem, cmd[f.Name]);
        //    foreach (PropertyAssoc f in PropertiesDB)
        //        if (!(cmd[f.Name] is DBNull))
        //            f.Info.SetValue(elem, cmd[f.Name],null);
        //    Items.Add(elem);
        //}
        //if (ToDataSource != null)
        //{
        //    //_bs.DataSource = Items;
        //    if (ToDataSource is DataGridView) ((DataGridView)ToDataSource).DataSource = Items;
        //    if (ToDataSource is ComboBox) ((ComboBox)ToDataSource).DataSource = Items;
        //    if (ToDataSource is ListBox) ((ListBox)ToDataSource).DataSource = Items;
        //}
        if (ToDataSource == null)
        {
            cmd.ExecuteReader();
            while (cmd.Read())
            {
                T elem = new T();
                foreach (FieldAssoc f in FieldsDB)
                    if (!(cmd[f.Name] is DBNull))
                        f.Info.SetValue(elem, cmd[f.Name]);
                Items.Add(elem);
            }
        }
        else
        {
            _bs.DataSource = Items;
            cmd.ExecuteInDataTable(_table);
            if (ToDataSource is DataGridView) ((DataGridView)ToDataSource).DataSource = _table;
            if (ToDataSource is ComboBox) ((ComboBox)ToDataSource).DataSource = _table;
            if (ToDataSource is ListBox) ((ListBox)ToDataSource).DataSource = _table;
        }
        Error = cmd.Error;
        cmd.Close();
        return true;
    }

    public void Init()
    {
        dbSelect = "SELECT * FROM [" + dbTable + "]";
        if (dbWhere != "") dbSelect += " WHERE " + dbWhere;
        if (dbOrder != "") dbSelect += " ORDER BY " + dbOrder;
    }
}