﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Medsyst.Dao.Base
{
    ///<summary>Интерфейс для списка категорий
    ///Заплатин Е.Ф.
    ///12.05.2013</summary>
    public interface IDBOList
    {
        /// <summary>Флаг для отображения категорий отмеченных как удаленные.</summary>
        bool ShowDeleted { get; set; }

        /// <summary>Загрузка списка категорий из базы данных. Уже реализован в классе DBOList </summary>
        bool Load();
        /// <summary>Удаление элемента из списка категорий. При успешном выполнении удаления возвращает - true. </summary>
        bool Delete(int id);
        /// <summary>Восстановление элемента списка категорий отмеченного как удаленный. При успешном восстановлении возвращает - true. </summary>
        bool Restore(int id);
        /// <summary>Добавление элемента в список категорий. При успешном добавлении возвращает - true. </summary>
        //    bool Add(string Text);
        /// <summary>Переименование элемента в список категорий. При успешном переименовании возвращает - true. </summary>
        bool Rename(string Text);
    }

    /// <summary>Класс базовый для таблиц БД</summary>
    public class DBOList<T> : List<T>
        where T : new()
    {
        /// <summary>Класс ассоцирующий имя столбца из таблицы БД с полем в классе</summary>
        class FieldAssoc
        {
            public FieldInfo Info;
            public string Name;
        }
        class PropertyAssoc
        {
            public PropertyInfo Info;
            public string Name;
        }
        public string dbSelect = "";// Запрос выборка
        public string dbWhere = ""; // Условие выборки
        public string dbOrder = ""; // Сортировка. Позволяет указать поле для сортировки выводимых данных
        public string dbKeyField = ""; //Ключевое поле 
        public string dbKeyTable = ""; //Название таблицы с ключевым полем
        /// <summary> Пользовательская SQL директива Select.ЗЕФ, 27.07.2011</summary>
        public string dbSelectUser = "";
        /// <summary> Запрос произвольный, который выполняется целиком без конструирования</summary>
        public string dbSelectCustom = "";
        /// <summary> Дополнительняа часть SELECT в запросе.ЗЕФ, 11.05.2012
        /// Если строка не пустая, то сформированый запрос по технологии Марьенкова следует дополнить
        /// выражениями dbSelectAdd и dbFromAdd</summary>
        public string dbSelectAdd = "";
        /// <summary> Дополнительняа часть FROM в запросе.ЗЕФ, 11.05.2012</summary>
        public string dbFromAdd = "";
        public bool showdeleted = false; //Указывает на то, показывать (true) или нет (false) элементы отмеченные как удаленные. ЗЕФ.
        public string addnondefined = ""; //Используется для добавление в список неопределенного значения с нулевым идентификатором. Например, "не определено" или "не указано" и пр. ЗЕФ. 13.05.2013
        public Type Type = null;    // класс используемый как основа
        public string Error;
        /// <summary>Список полей загружаемых из таблицы БД</summary>
        private List<FieldAssoc> FieldsDB;
        private List<PropertyAssoc> PropertiesDB;
        public int PageCount; //Количество страниц
        public int Page = 0; //Номер текущей страницы
        public int PageSize = 50; //Количество строк на странице
        public bool Pagination = false; //Разбиение выборки на страницы
        public DBOList()
            : base()
        {
            //Items = new List<T>();
            FieldsDB = new List<FieldAssoc>();
            PropertiesDB = new List<PropertyAssoc>();
            Type type = typeof(T);

            // поиск полей объекта
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                object[] attribs = field.GetCustomAttributes(typeof(dbAttribute), false);
                if (attribs != null && attribs.Length > 0)
                {
                    dbAttribute attrib = attribs[0] as dbAttribute;
                    FieldAssoc fa = new FieldAssoc();
                    fa.Info = field;
                    if (attrib.Name == "")
                        fa.Name = field.Name;
                    else
                        fa.Name = attrib.Name;
                    FieldsDB.Add(fa);
                }
            }
            // поиск свойств объекта
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object[] attribs = property.GetCustomAttributes(typeof(dbAttribute), false);
                if (attribs != null && attribs.Length > 0)
                {
                    dbAttribute attrib = attribs[0] as dbAttribute;
                    PropertyAssoc fa = new PropertyAssoc();
                    fa.Info = property;
                    if (attrib.Name == "")
                        fa.Name = property.Name;
                    else
                        fa.Name = attrib.Name;
                    PropertiesDB.Add(fa);
                }
            }
            T t = new T();
            if (t is DBO)
            {
                DBO tt = ((DBO)(object)t);
                //tt.Init();
                dbSelect = tt.ini.dbSelectList;
                dbKeyField = tt.ini.dbKeyField;
                dbKeyTable = tt.ini.dbKeyTable;
            }
        }

        /// <summary>Загрузка данных из таблиц</summary>
        /// <returns>Успешность выполнения</returns>
        public virtual bool Load()
        {
            string dbselect, dbselect1 = "";
            //Условие добавлено Заплатиным Е. 28.01.2011
            if (dbSelectUser == "")
                dbselect = dbselect1 = dbSelect;
            else
                dbselect = dbselect1 = dbSelectUser;//SQL директива SELECT формируется в бизнес-классе

            //Если программист расширил запрос, то следует добавить это расширение к уже сформированному по технологии запросу
            if (dbSelectAdd != "" || dbFromAdd != "")
            {
                //dbSelect разбивается на две части SELECT и FROM вставкой строки dbSelectAdd и дополняется табличными связями dbFromAdd 
                dbselect = dbselect.Insert(dbselect.IndexOf("FROM", 0) - 1, dbSelectAdd) + " " + dbFromAdd;
            }

            if (dbWhere != "")
            {

                if (Pagination)
                {//Требуется разбиение на таблицы
                    dbselect += " WHERE ( " + dbWhere + ") AND " + dbKeyField + " in(#in#) ";
                    dbselect1 += " WHERE " + dbWhere;
                }
                else
                {
                    dbselect += " WHERE " + dbWhere;
                    dbselect1 += " WHERE " + dbWhere;
                }
            }
            else if (Pagination)
            {//Требуется разбиение на страницы
                dbselect += " WHERE " + dbKeyField + " in(#in#) ";
            }

            if (dbOrder != "")
            {//Задан критерий сртироваки
                dbselect += " ORDER BY " + dbOrder;
                dbselect1 += " ORDER BY " + dbOrder;
            }


            if (dbOrder != "" && dbKeyField != "")
            {
                dbselect += ", [" + dbKeyTable + "].[" + dbKeyField + "]";
                dbselect1 += ", [" + dbKeyTable + "].[" + dbKeyField + "]";
            }
            else if (dbKeyField != "")
            {
                dbselect += " ORDER BY [" + dbKeyTable + "].[" + dbKeyField + "]";
                dbselect1 += " ORDER BY [" + dbKeyTable + "].[" + dbKeyField + "]";
            }

            if (Pagination)
            {//Требуется разбиение на страницы
                int pc = (Page + 1) * PageSize;
                string key = dbKeyField;
                string dbs =
                    " SELECT TOP " + PageSize + " " + dbKeyField + " FROM " +
                    " (" + dbselect1.Replace("*", " TOP " + pc.ToString() + " " + dbKeyField + (dbOrder != "" ? ", " + dbOrder : "")) +
                    ") x ORDER BY " + (dbOrder != "" ? (dbOrder + ",").Replace(",", " DESC,") : "") + key + " DESC ";//+
                dbselect = dbselect.Replace("#in#", dbs);
            }
            //MessageBox.Show(dbselect, "");
            Clear();

            DB cmd = new DB(dbSelectCustom.Trim() != "" ? dbSelectCustom : dbselect);
            cmd.ExecuteReader();//Выполнение сформированного SQL запроса для получение данных из базы в виде набора записей
            //Программный блок заполнения коллекции данными, полученными из базы
            while (cmd.Read())
            {//Последовательное считываение каждой записи из набора
                T elem = new T();//Формирование нового экземляра элемента коллекции
                foreach (FieldAssoc f in FieldsDB)
                    if (!(cmd[f.Name] is DBNull))
                        f.Info.SetValue(elem, cmd[f.Name]);
                foreach (PropertyAssoc f in PropertiesDB)
                    //Последовательная обрабртка каждого поля записи полученной из набора записей
                    if (!(cmd[f.Name] is DBNull))
                        //Значение в поле записи базы данных существует, т.е. не Null
                        f.Info.SetValue(elem, cmd[f.Name], null);//Установка значения свойства элемента коллекции значением из поля базы данных
                //TODO Есть проблема состоящая в том, что если в записи окажется два одноименных поля из разных таблиц,
                //то непонятно значение которого из них будет присвоено свойству экземпляра класса
                Add(elem);//Добавление заполненного элемента в коллекцию
            }
            Error += cmd.Error;
            cmd.Close();
            if (DB.IsMessageError && cmd.Error != "")
                MessageBox.Show("Файл: DBO.cs\n" + "Класс: DBOList\n" + "Функция: public bool Load()\n" + Error, "Ошибка DBO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            if ((cmd.Error != "") || (Count == 0)) return false; //Не найдено ни одной записи ЗЕФ. 14.08.2011
            return true;
        }

        /// <summary>
        /// Добавлен для более простого использования загружаемых данных
        /// </summary>
        /// <returns></returns>
        public DBOList<T> Get()
        {
            Load();
                
            return this;
        }

        public virtual int GetCount()
        {
            string dbselect = dbSelect;

            if (dbWhere != "") dbselect += " WHERE " + dbWhere;

            dbselect = dbselect.Replace("*", "Count(*)");

            DB cmd = new DB(dbselect);

            int c = (int)cmd.ExecuteScalar();

            PageCount = (int)Math.Ceiling((double)c / (double)PageSize);

            return c;
        }
    }

}
