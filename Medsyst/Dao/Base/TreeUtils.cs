﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Medsyst.Dao.Base
{
    /// <summary>Класс утилит для работы с деревом как элементом управления
    /// автор: Чирков Е.О.
    /// дата создания 23.09.09
    /// </summary>
    public class TreeUtils
    {
        /// <summary>Перебор узлов дерева Tree начиная со StartNode (или если StartNode=null - всего дерева),
        /// применяя к каждому узлу делегат Proc с произвольным параметром Param
        /// автор: Чирков Е.О.
        /// дата создания 18.09.09
        /// дата перемещения в класс TreeUtils 23.09.09summary>
        /// <param name="Tree"></param>
        /// <param name="StartNode"></param>
        /// <param name="Proc"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public static bool IterateTreeItems(TreeView Tree, TreeNode StartNode, TreeIterationProc Proc, ref object Param)
        {
            TreeNodeCollection nodes;
            if (StartNode == null)
            {
                nodes = Tree.Nodes;
            }
            else
            {
                if (!Proc(Tree, StartNode, ref Param)) return false;
                nodes = StartNode.Nodes;
            }
            foreach (TreeNode node in nodes)
            {
                if (!Proc(Tree, node, ref Param)) return false;
                if (!IterateTreeItems(Tree, node, Proc, ref Param)) return false;
            }
            return true;
        }
        /// <summary>класс состояния дерева, содержит выделенный (Selected), выбранные (Checked) и развернутые (Expanded) узлы
        /// автор: Чирков Е.О.
        /// дата создания 23.09.09
        /// </summary>
        public class TreeState
        {
            public List<int> Checks = new List<int>();
            public List<int> Expands = new List<int>();
            public int SelectedID;
        }
        static bool _gettreecheck(TreeView tree, TreeNode node, ref object Param)
        {
            TreeState state = Param as TreeState;
            int id = (int)node.Tag;
            if (node.Checked) state.Checks.Add(id);
            if (node.IsExpanded) state.Expands.Add(id);
            Param = state;
            return true;
        }
        /// <summary>сохранение состояния дерева
        /// автор: Чирков Е.О.
        /// дата создания 23.09.09
        /// </summary>
        /// <param name="Tree"></param>
        /// <param name="state"></param>
        public static void SaveTreeState(TreeView Tree, out TreeState state)
        {
            object o = new TreeState();
            IterateTreeItems(Tree, null, _gettreecheck, ref o);
            state = o as TreeState;
            if (Tree.SelectedNode != null) state.SelectedID = (int)Tree.SelectedNode.Tag;
            else state.SelectedID = -1;
        }
        static bool _settreecheck(TreeView tree, TreeNode node, ref object Param)
        {
            TreeState state = Param as TreeState;
            int id = (int)node.Tag;
            foreach (int i in state.Checks)
            {
                if (i == id)
                {
                    node.Checked = true;
                    state.Checks.Remove(i);
                    break;
                }
                else node.Checked = false;
            }
            foreach (int e in state.Expands)
            {
                if (e == id)
                {
                    node.Expand();
                    state.Expands.Remove(e);
                    break;
                }
                else node.Collapse();
            }
            Param = state;
            return true;
        }
        /// <summary>восстановление состояния дерева
        /// автор: Чирков Е.О.
        /// дата создания 23.09.09 </summary>
        /// <param name="Tree"></param>
        /// <param name="state"></param>
        public static void RestoreTreeState(TreeView Tree, TreeState state)
        {
            object o = state;
            IterateTreeItems(Tree, null, _settreecheck, ref o);
            if (state.SelectedID >= 0) Tree.SelectedNode = GetNodeFromID(Tree, null, state.SelectedID);
        }
        /// <summary>возвращает экземпляр узла дерева с идентификатором ID, или null - если такого нет
        /// автор: Чирков Е.О.
        /// дата создания 24.09.09</summary>
        /// <param name="Tree"></param>
        /// <param name="Start">Узел в дереве от которого нужно производить поиск. Если не указан, то поиск производится во всем дереве целиком</param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static TreeNode GetNodeFromID(TreeView Tree, TreeNode Start, int ID)
        {
            TreeNode node = null;
            TreeNodeCollection st;
            if (Start == null) st = Tree.Nodes;
            else st = Start.Nodes;
            foreach (TreeNode n in st)
            {
                if (n.Nodes.Count > 0) node = GetNodeFromID(Tree, n, ID);
                if (node != null) return node;
                if ((int)n.Tag == ID) return n;
            }
            return node;
        }
        /// <summary>Возвращает корневой узел для любого узла в дереве, переданного в качестве аргумента
        /// Залатин Е.Ф.
        /// 29.04.2011
        /// </summary>
        /// <param name="Tree"></param>
        /// <param name="node">произвольный узел в дереве для которого нужно найти корневой узел</param>
        /// <returns></returns>
        public static void GetRootNode(ref TreeNode node)
        {
            if (node.Parent != null)
            {//Если у переданного узла найден родитель, то его тоже нужно проверить на предмет наличия родительского узла
                TreeNode perentnode = new TreeNode();
                node = node.Parent; //передается аргументу для возврата результата родительская ТМЦ 
                GetRootNode(ref node);//Проверяется родительское ТМЦ
            }
            //Если родитель не найден, то переданный аргумент остается без изменений
        }
        /// <summary>Выбор узлов отмеченых чекбоксом в дереве целиком или в поддереве от узла
        /// Заплатин Е.Ф. 
        /// 04.05.11</summary>
        /// <param name="tree">Дерево в котором нужно найти отмеченные узлы</param>
        /// <returns>возвращает коллекцию выбранных из дерева узлов</returns>
        public static TreeNodeCollection GetCheckitNodes(TreeView tree)
        {
            TreeView treeCheckitNodes = new TreeView();//Дерево, которое будет содержать только обгалоченые узлы из исходного деоева
            //Анализируются все корневые узлы дерева
            foreach (TreeNode node in tree.Nodes)
            {
                TreeNodeCollection subnodes = _getCheckitNodes(node);//анализируются все вложенные узлы и возвращаются только отмеченные чекбоксами

                //Добавление отмеченных чекитами узлов  корневой ветки в общую коллекцию отмеченных узлов
                if (subnodes.Count != 0) //Возможно лучше проверять (subnodes != null)
                {
                    TreeNode copyNode = new TreeNode();
                    //Найдено некоротое количество подузлов
                    foreach (TreeNode subnode in subnodes)//Добавление всех найденных узлов в возвращаемую коллекцию узлов
                    {
                        copyNode = (TreeNode)subnode.Clone();
                        treeCheckitNodes.Nodes.Add(copyNode);//В новое дерево добавляется копия узла из коллекции отмеченных чекитами
                    }
                }
            }
            return treeCheckitNodes.Nodes;
        }
        /// <summary>Выбор узлов отмеченых чекбоксом в поддереве от узла. Рекурсивный метод.
        /// Заплатин Е.Ф. 
        /// 04.05.11</summary>
        /// <param name="tree">Дерево в котором нужно найти отмеченные узлы</param>
        /// <returns>возвращает коллекцию выбранных из дерева узлов</returns>
        private static TreeNodeCollection _getCheckitNodes(TreeNode treeNode)
        {
            TreeView CheckitNodes = new TreeView();//Новое дерево в которое будут добавляться отмеченные чекитами узлы

            //проверка переданного узла на предмет отметки чекбоксом
            if (treeNode.Checked == true)
                CheckitNodes.Nodes.Add((TreeNode)treeNode.Clone());


            if (treeNode.Nodes.Count != 0)
            {//В переданном узле содержатся подузлы и их нужно так же проверить

                //проверяются все вложенные узлы
                foreach (TreeNode node in treeNode.Nodes)
                {
                    //Проверка на наличие подузлов. Рекурсивный вызов метода.
                    TreeNodeCollection checkitSubnodes = _getCheckitNodes(node);//Добавление отмеченных подузлов в общую коллекцию отмеченных узлов
                    if (checkitSubnodes.Count != 0)//Возвращено некоротое количество обгалоченых подузлов
                        foreach (TreeNode subnode in checkitSubnodes)
                            //Добавление всех найденных узлов в возвращаемую коллекцию узлов
                            CheckitNodes.Nodes.Add((TreeNode)subnode.Clone());
                }
            }
            return CheckitNodes.Nodes;
        }
        /// <summary>Снятие или установка чекбоксов со всех дочерних узлов. Нельзя связывать этот метод с событием AfterCheck.
        /// В противном случае оно будет повторятся для каждого подузла, что приведет к нежелательному эфекту постора запросов
        /// Лучше всего этот метод связывать с событием NodeMouseClick и проверять состояние чекбокса. Если оно изменилось то выполнять этот метод
        /// Метод требует проверки и доработки
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="node">узел в котором изменился статус чекбокса</param>
        public static void CheckAllSubnodes(TreeNode node)
        {
            //Спрашивается подтверждение изменения статус чекбоксов со всех дочерних подузлов
            string SubMsg = "";
            if (node.Checked == false)
                SubMsg = "Снять";
            else
                SubMsg = "Установить";
            string Msg = SubMsg + " чекбоксы у всех дочерних элементов?";
            if (MessageBox.Show(Msg, "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No) return;

            _checkAllSubnodes(node);

        }
        /// <summary>Производится рекурсивное снятие или установка чекбоксов во всех дочерних элементах
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="node"></param>
        public static void _checkAllSubnodes(TreeNode node)
        {
            // Ставим или убираем отметку со всех подузлов.
            foreach (TreeNode treeSubNode in node.Nodes)
            {
                treeSubNode.Checked = node.Checked;     //  изменяем значение чекбокса подузла в соответсвии с родительским узлом
                _checkAllSubnodes(treeSubNode);      // вызываем рекурсивно метод выделения всех подузлов  
            }

        }
    }
}
