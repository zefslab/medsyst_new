﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data; //Добавлено вручную согласно ms-help://MS.MSDNQTR.v90.ru/crystlmn/html/topic358.htm
using System.Data.OleDb; //Добавлено вручную согласно ms-help://MS.MSDNQTR.v90.ru/crystlmn/html/topic358.htm
using Medsyst.MedsystDataSetTableAdapters;

//Клас выполняет функцию заполнения набора данных MedsystDataSet для последующего его использования в отчетах
//Автор: Заплатин Е.Ф
//Дата создания: 05.09.2010
namespace Medsyst.Class.Database
{
    class DataSetConfiguration
    {
        public static DataSet medsystDataSet
        {
            get
            {
                MedsystDataSet dataSet = new MedsystDataSet(); //объявление и создание экземпляра класса MedsystDataSet схемы данных
                //Далее заполняются  таблицы данных DataTable внутри экземпляра DataSet данными, извлеченными из базы данных. 
                //Основной отчет
                PersonTableAdapter personDbDataAdapter = new PersonTableAdapter();
                personDbDataAdapter.Fill(dataSet.Person); 
                SeasonTableAdapter seasonDbDataAdapter = new SeasonTableAdapter();
                seasonDbDataAdapter.Fill(dataSet.Season);
                IllnessHistoryTableAdapter IHDbDataAdapter = new IllnessHistoryTableAdapter();
                IHDbDataAdapter.Fill(dataSet.IllnessHistory);
                //IHDbDataAdapter.Connection.ConnectionString = ""; имеется возможность подставлять строку подключения считываемую при загрузке системы
                //IHDbDataAdapter.Adapter.GetFillParameters();//Получет параметры используемые в запросе адаптера заданные пользователем
                DocAmbulanceCardTableAdapter docAmbulanceCardDataAdapter = new DocAmbulanceCardTableAdapter();
                docAmbulanceCardDataAdapter.Fill(dataSet.DocAmbulanceCard);
                PersonEmployeeTableAdapter personEmployeeDbDataAdapter = new PersonEmployeeTableAdapter();
                personEmployeeDbDataAdapter.Fill(dataSet.PersonEmployee); 
                FirmTableAdapter firmDbDataAdapter = new FirmTableAdapter();
                firmDbDataAdapter.Fill(dataSet.Firm);
                AddressTableAdapter addressDbDataAdapter = new AddressTableAdapter();
                addressDbDataAdapter.Fill(dataSet.Address);
                CityTableAdapter cityDbDataAdapter = new CityTableAdapter();
                cityDbDataAdapter.Fill(dataSet.City);
                PositionTableAdapter positionDbDataAdapter = new PositionTableAdapter();
                positionDbDataAdapter.Fill(dataSet.Position);
                
                //Подотчет назначенных услуг
                ServiceTableAdapter serviceDbDataAdapter = new ServiceTableAdapter();
                serviceDbDataAdapter.Fill(dataSet.Service);
                ServiceOutputTableAdapter serviceOutputDbDataAdapter = new ServiceOutputTableAdapter();
                serviceOutputDbDataAdapter.Fill(dataSet.ServiceOutput);

                //Подотчет назначенных медикаментов
                MedicamentTableAdapter medicamentDbDataAdapter = new MedicamentTableAdapter();
                medicamentDbDataAdapter.Fill(dataSet.Medicament);
                MedicamentInputTableAdapter medicamentInputDbDataAdapter = new MedicamentInputTableAdapter();
                medicamentInputDbDataAdapter.Fill(dataSet.MedicamentInput);
                MedicamentOnScladTableAdapter medicamentOnScladDbDataAdapter = new MedicamentOnScladTableAdapter();
                medicamentOnScladDbDataAdapter.Fill(dataSet.MedicamentOnSclad);
                MedicamentOutputTableAdapter medicamentOutputDbDataAdapter = new MedicamentOutputTableAdapter();
                medicamentOutputDbDataAdapter.Fill(dataSet.MedicamentOutput);

                //Подотчет наблюдений
                IllnessHistoryObservationTableAdapter ihObservationOutputDbDataAdapter = new IllnessHistoryObservationTableAdapter();
                ihObservationOutputDbDataAdapter.Fill(dataSet.IllnessHistoryObservation);
                
                
                return dataSet; //возвращается экземпляр DataSet
            }
        }



    }
}
