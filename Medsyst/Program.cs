﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using Medsyst.Class.Forms;
using Medsyst.Dao.Base;
using NLog;


namespace Medsyst
{
    /// <summary>
    /// Нет оприсания
    /// </summary>
    class p
    {
        ///<summary>Путь директории исполняемого файла приложения</summary>
        public static string AppPath = "";
        /// <summary>включить или выключить режим отладки программы. 
        /// В этом режите отображается различная вспомогательная информация: 
        /// Информационные окна, служебные управляющие элементы и пр. Добавил ЗЕФ 11.01.2011</summary>
        public static bool trace = false;//Лучше всего загружать этот параметр их настроечного файла settings.xml, 
                                         //а так же иметь возможность установки его из меню Сервис/Настройки самой программы

        ///<summary>Входная точка и главная функция приложения</summary>
        [STAThread]
        static void Main()
        {
            Logger logger = LogManager.GetLogger("Medsyst");

            AppPath = Directory.GetCurrentDirectory();
            ///Инициализация подключения к БД
            MyDbConnection.Open(DB.ConnectionString());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Чирков Е.О 19.03.2010
            if (new frmUserAuthorization().ShowDialog() == DialogResult.Cancel) return;

            FormSettings.Path = AppPath + "\\lists.xml";//инициализация пути к файлу со списками значений

            while (true) 
            {
                try
                {
                    //throw new NullReferenceException("It is fixtion error for test logging");

                    Application.Run(new frmMain());

                    break;
                }
                catch (Exception ex)
                {
                    MyDbConnection.Close();

                    var message = Environment.NewLine + Environment.OSVersion + Environment.NewLine
                                  + Environment.UserName + Environment.NewLine;

                    logger.Fatal($"Произошла критическая ошибка в программе Medsyst пользователя {Security.user.Login}: {ex.Message} {message}", ex);

                    MessageBox.Show("Произошла критическая ошибка\r\nПриложение будет перезапущено.\r\nОписание:\r\n" + ex,
                        "Критическая ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
            MyDbConnection.Close();
        }
    }
}
