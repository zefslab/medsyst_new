﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Constant;
using Medsyst.Data.Core;
using Medsyst.Data.Dto.ReceiptOrder;
using Medsyst.Data.Entity;
using Medsyst.Data.Filter;

namespace Medsyst.Services.Bl
{
    public class ReceiptOrderCrudService : BaseCrudService<ReceiptOrder,  ReceiptOrderDto>
    {
        ReceiptOrderList _orders = new ReceiptOrderList();
        ProviderList _providers = new ProviderList();
        InputMedicineCrudService _inputMedicineCrudService = new InputMedicineCrudService();
        
        public override ReceiptOrderDto Update(ReceiptOrderDto dto)
        {
            throw new NotImplementedException();
        }

        public override ReceiptOrderDto Get(int Id)
        {
            throw new NotImplementedException();
        }

        public override IList<ReceiptOrderDto> Get(BaseFilter _filter)
        {
            if (!(_filter is ReceiptOrderFilter)) return null;
           
            var filter = _filter as ReceiptOrderFilter;
           
            var providers = _providers.Get().Select(x => new
            {
                Id = x.FirmID,
                Name = x.FirmN
            }).ToList();

            return _orders.Load(filter.ShowWithDeleted,
                                filter.BeginDate != null, filter.BeginDate ?? DateTime.MinValue, filter.EndDate ?? DateTime.MinValue,
                                filter.ProviderId != null, filter.ProviderId ?? int.MinValue,
                                filter.isBudget != null, filter.isBudget ?? false)
                .Join(providers, x => x.ProviderID, x => x.Id, (x, p) => new ReceiptOrderDto
                       {
                           Id =x.DocumentID,
                           Number = x.Nbr,
                           Date = x.RegisterD,
                           AccountNbr = x.AccountNbr,
                           AccountDate = x.AccountD,
                           isSigned = x.Signed,
                           SourceFinancing = x.IsBudget? "бюджет": "внебюджет",
                           Provider = p.Name,
                           Summ = x.PriceRO,
                           Comment = x.Comment,
                           isDeleted = x.Deleted
                       }).ToList();

        }

        /// <summary>
        /// Прежде чем удалять приходный ордер, нужно удалить все оприходованные по нему медикаменты.
        /// Если хотя бы один из медикаментов удалить не удасться, 
        /// то удаление приходного ордера тоже становится невозможным.
        /// Подписанные приходные ордеры тоже невозможно удалить.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override ProcessingResult<ReceiptOrder> Delete(int id, CommandDirective deleteDirective)
        {
            var result = new ProcessingResult<ReceiptOrder>() {Result = ProcessingResults.Successfull};

            var receiptOrder = new ReceiptOrder(true){DocumentID = id};
            receiptOrder.Load();

            if (receiptOrder.Signed)
            {
                //Не возможно удалить подписанные приходные ордера
                result.Result = ProcessingResults.Error;

                return result;
            }

            #region Удаление всех медикаментов по приходному ордеру
            
            var inputMedicines = new MedicamentInputList();

            inputMedicines.Load(id);

            var deleteMedicinesResult = new ProcessingResult<MedicamentInput>(){Result = ProcessingResults.Successfull};

            //Начать транзакцию
            Wrap(() =>
            {
                inputMedicines.ForEach(x =>
                {
                    deleteMedicinesResult += _inputMedicineCrudService.Delete(x.MedicamentInputID, deleteDirective);
                });

                if (deleteMedicinesResult.Result == ProcessingResults.Successfull)
                {
                   result.Result =  receiptOrder.Delete(deleteDirective)
                                                   ? ProcessingResults.Successfull 
                                                   :ProcessingResults.Error ;
                }
            });
            //Завершить транзакцию

            return result;

            #endregion
        }
    }
}
