﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;

namespace Medsyst.Services.Bl
{
    /// <summary>
    /// Управляет сезонами
    /// </summary>
    public class SeasonService : BaseService<Season, SeasonFullDto>
    {
        private Seasons Seasons = new Seasons();

        public override SeasonFullDto Save(SeasonFullDto dto)
        {
            throw new NotImplementedException();
        }

       
        public IList<SeasonFullDto> Save(int docId, IList<SeasonFullDto> dtoList)
        {
           


            return dtoList;
        }

        public override SeasonFullDto Get(int id)
        {
            throw new NotImplementedException();
        }

       
        public override IList<SeasonFullDto> Get(BaseFilter filter)
        {

            if (filter != null && filter is SeasonFilter)
            {
                var f = filter as SeasonFilter;

                if (f.Year != null)
                {
                    Seasons.Load((int)f.Year);
                }
            }

            return Seasons.Select( s=> new SeasonFullDto
            {
                Id = s.SeasonID,
                Number = s.SeasonNbr,
                Year = s.Year
                
            }).ToList();
        }

        public override bool Delete(int id)
        {
           var result = Wrap(() =>
            {
                //1. Возвращается удаляемый из расхода товар на склад

                //2. Расход отмечается как удаленный 
            });

            return result;
        }

        
    }
}
