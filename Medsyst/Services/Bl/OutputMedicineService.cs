﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;

namespace Medsyst.Services.Bl
{
    /// <summary>
    /// Управляет расходными медикаментами
    /// </summary>
    public class OutputMedicineCrudService : BaseCrudService<MedicamentOutput, OutputMedicineDto>
    {
        public override OutputMedicineDto Update(OutputMedicineDto dto)
        {
            throw new NotImplementedException();
        }

        /// <summary> Сохраняется серия расходуемых медикаментов в указанном расходном документе
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="dtoList"></param>
        /// <returns></returns>
        public IList<OutputMedicineDto> Save(int docId, IList<OutputMedicineDto> dtoList)
        {
            ///Загрузка медикаментов из БД
            var outputMedicines = new OutputMedicamentList();
            outputMedicines.Load((true ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted), docId);
            
            //Сохраняются измененные или добавленные элементы коллекции расходных медикаментов, 
            //а удаленные в текущем сеансе отмечаются как удаленные 14.12.2013
            var deletedOutputMedicines = outputMedicines.Where(x => x.DeletedOut).ToList();

            if (deletedOutputMedicines.Any())
            {
                var isDeleteSucsess = ((OutputMedicamentList)deletedOutputMedicines).Delete(CommandDirective.MarkAsDeleted);

                if (!isDeleteSucsess)
                    MessageBox.Show("Не удалось удалить медикаменты из расхода, поэтому не возможно назначить новую услугу. \n\rЭто нештатная ситуация. Сообщите о ней администратору Медсист.",
                                   "Прерывание операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            outputMedicines.SaveNotSaved();

            outputMedicines.UpdateModified();


            return dtoList;
        }

        public override OutputMedicineDto Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>Возвращает список расходных медикаментов по расходному документу
        ///  </summary>
        /// <returns></returns>
        public override IList<OutputMedicineDto> Get(BaseFilter filter )
        {
            var result = new List<OutputMedicineDto>();

            var _filter = filter as OutputMedicineFilter;

            if (_filter != null)
            {
                var outputMedicaments = new OutputMedicamentList();

                outputMedicaments.Load((_filter.withDeleted ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted),
                    _filter.DocumentId);

                result = outputMedicaments.Select(x => new OutputMedicineDto
                {
                    Id = x.MedicamenOutputID,
                    MedicamenOnScladID = x.MedicamentOnScladID,
                    Name = x.MedicamentN,
                    MeasureID = x.MeasureID,
                    CountOutput = x.CountOutput,
                    Deleted = x.DeletedOut
                }).ToList();
            }

            return result;
        }

        public override ProcessingResult<MedicamentOutput> Delete(int id, CommandDirective deleteDirective)
        {
            throw new NotImplementedException();
        }

        
    }
}
