﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;


namespace Medsyst.Services.Bl
{
    public class IllnessHistoryCrudService : BaseCrudService<IllnessHistory, IllnessHistoryDto>
    {
        IllnessHistoryShortList _histories = new IllnessHistoryShortList();

        public override IllnessHistoryDto Update(IllnessHistoryDto dto)
        {
            throw new NotImplementedException();
        }

        public override IllnessHistoryDto Get(int outputMedicamentId)
        {
            throw new NotImplementedException();
        }

        public override IList<IllnessHistoryDto> Get(BaseFilter filter)
        {
            var result = new List<IllnessHistoryDto>();

            var _filter = filter as IllnessHistoryFilter;

            if (_filter != null)
            {

                if (_filter.SeasonId != null && _filter.CategoryPacientId != null)
                {
                    _histories.Load(_filter.SeasonId, _filter.CategoryPacientId);

                    var season = new Season {SeasonID = _filter.SeasonId };
                    season.Load();

                    result = _histories.Select(ih => new IllnessHistoryDto
                    {
                        DocumentId = ih.DocumentID,
                        Doctor = ih.DoctorName,
                        Number = ih.NbrIH + "/" + season.SeasonNbr,
                        PatientName = ih.FullName,
                        SummMedicament = _filter.PresentationForm == PresentationForms.MedicamentOutput
                                         ? ih.SummMedicament
                                         : ih.SummService
                    }).ToList();
                }
            }

            return  result;

        }

        public override ProcessingResult<IllnessHistory> Delete(int id, CommandDirective deleteDirective)
        {
            throw new NotImplementedException();
        }

       
    }
}
