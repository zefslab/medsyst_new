﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;


namespace Medsyst.Services.Bl
{
    public class OnDemandMedicineCrudService : BaseCrudService<OnDemandMedicine, OnDemandMedicineDto>
    {
        OnScladMedicineCrudService _onScladMedicineCrudService = new OnScladMedicineCrudService();
        
        OutputMedicamentList outputMedicaments = new OutputMedicamentList();

        public override OnDemandMedicineDto Update(OnDemandMedicineDto dto)
        {
            throw new NotImplementedException();
        }

        public override OnDemandMedicineDto Get(int outputMedicamentId)
        {
            throw new NotImplementedException();
        }

        public override IList<OnDemandMedicineDto> Get( BaseFilter filter )
        {
            var result = new List<OnDemandMedicineDto>();

            var _filter = filter as OnDemandMedicineFilter;

            if (_filter != null)
            {
                //Медикаменты по требованию определяются на основании расхода, произведенного по требованию
                outputMedicaments.Load((_filter.withDeleted ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted),
                    _filter.DocumentId);

                result = outputMedicaments.Select(x => new OnDemandMedicineDto
                {
                    Id = x.MedicamenOutputID,
                    MedicamenOnScladID = x.MedicamentOnScladID,
                    Name = x.MedicamentN,
                    MeasureID = x.MeasureID,
                    CountOutput = x.CountOutput,
                    Deleted = x.DeletedOut
                }).ToList();
            }

            return result;
        }

        public override ProcessingResult<OnDemandMedicine> Delete(int id, CommandDirective deleteDirective)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Добавление лекарственного средства в требование с предварительной проверкой его существования в требовании.
        /// Не допускается добавление двух одинаковых складских мдикоментов в одно отребование.
        /// </summary>
        /// <param name="outputMedicamentId"></param>
        /// <returns></returns>
        public bool AddToDemand(int onScladMedicineId, IEnumerable<OnDemandMedicineDto> onDemandMedicines,
                                out OnDemandMedicineDto onDemandMedicine)
        {
            var onScladMedicine = _onScladMedicineCrudService.Get(onScladMedicineId);

            //Проверка существования такого же медикамента в требовании
            if (onDemandMedicines.Any(x => x.MedicamenOnScladID == onScladMedicine.Id))
            {
                //Этого достаточно, чтобы на UI выдать сообщение о невозможности добавления этого медикамента в требование
                onDemandMedicine = new OnDemandMedicineDto {Name = onScladMedicine.Name};

                return false;
            }
            
            onDemandMedicine =  new OnDemandMedicineDto
            {
                Id = 0,
                MedicamenOnScladID = onScladMedicine.Id,
                Name = onScladMedicine.Name,
                MeasureID = onScladMedicine.MeasureId,
                CountOutput = 0,
                Deleted = false
            };

            return true;
        }
    }
}
