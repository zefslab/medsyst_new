﻿using System;
using System.Collections.Generic;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;

namespace Medsyst.Services.Bl
{
    public class OnScladMedicineCrudService : BaseCrudService<MedicamentOnSclad, OnScladMedicineDto>
    {
        public override OnScladMedicineDto Update(OnScladMedicineDto dto)
        {
            throw new NotImplementedException();
        }

        public override OnScladMedicineDto Get(int outputMedicamentId)
        {
            MedicamentOnSclad onScladMedicine = new MedicamentOnSclad(true);
            
            onScladMedicine.MedicamentOnScladID = outputMedicamentId;
            
            onScladMedicine.Load();

            return new OnScladMedicineDto
            {
                Id = onScladMedicine.MedicamentOnScladID,

                Name = onScladMedicine.MedicamentN,

                InputMedicamentId = onScladMedicine.MedicamentInputID,

                Count = onScladMedicine.CountOnSclad,

                MeasureId = onScladMedicine.MeasureID,

                ReservedCount = onScladMedicine.CountReserve
                
            };
        }

        public override IList<OnScladMedicineDto> Get(BaseFilter filter)
        {
            throw new NotImplementedException();
        }

        public override ProcessingResult<MedicamentOnSclad> Delete(int id, CommandDirective deleteDirective)
        {
            throw new NotImplementedException();
        }

        /// <summary>Резервирование медикамента на складе 
        /// </summary>
        /// <param name="id">идентификатор медикамента на складе</param>
        /// <param name="count">если достаточно медикаментов на складе - true">количество, котрое нужно зарезервировать</param>
        /// <returns>если достаточно </returns>
        public bool Reserve(int id, int count)
        {
            return Wrap(() =>
            {

            });
        }

        /// <summary>Снятие с резерва медикамента на складе
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UnReserve(int id)
        {
            return Wrap(() =>
            {

            });
        }

    }
}
