﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;

namespace Medsyst.Services.Bl
{
    public class OnDemandMaterialCrudService : BaseCrudService<OnDemandMedicine, OnDemandMaterialDto>
    {
        OnScladMedicineCrudService _onScladMedicineCrudService = new OnScladMedicineCrudService();

        OutputMedicamentList outputMedicaments = new OutputMedicamentList();

        MedicamentByDemandList _medicamentByDemandList = new MedicamentByDemandList();

        public override OnDemandMaterialDto Update(OnDemandMaterialDto dto)
        {
            throw new NotImplementedException();
        }

        public override OnDemandMaterialDto Get(int Id)
        {
            throw new NotImplementedException();
        }

        public override IList<OnDemandMaterialDto> Get(BaseFilter filter)
        {
            var result = new List<OnDemandMaterialDto>();

            var _filter = filter as OnDemandMedicineFilter;

            if (_filter != null)
            {
                //Медикаменты по требованию определяются на основании расхода, произведенного по требованию
                outputMedicaments.Load((_filter.withDeleted ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted),
                    _filter.DocumentId);

                result = outputMedicaments.Select(x => new OnDemandMaterialDto
                {
                    Id = x.MedicamenOutputID,
                    MedicamenOnScladID = x.MedicamentOnScladID,
                    Name = x.MedicamentN,
                    MeasureID = x.MeasureID,
                    CountOutput = x.CountOutput,
                    Deleted = x.DeletedOut
                }).ToList();
            }

            return result;
        }

        public override ProcessingResult<OnDemandMedicine> Delete(int Id, CommandDirective deleteDirective)
        {
            throw new NotImplementedException();
        }
       
        public override OnDemandMaterialDto Add(OnDemandMaterialDto dto)
        {
            var onScladMedicine = _onScladMedicineCrudService.Get(dto.MedicamenOnScladID);

            OnDemandMedicine material = new OnDemandMedicine();

            //TODO все выполнять в одной транзакции!!!
            //material.Insert();

            //TODO Внести коррективы на склад и в расходные медикаменты
            //...

            dto.Id = material.MedicamentByDemandID;
            dto.MedicamenOnScladID = onScladMedicine.Id;
            dto.Name = onScladMedicine.Name;
            dto.MeasureID = onScladMedicine.MeasureId;
            dto.CountOutput = 0;//TODO это осталось  под вопросом
            dto.Deleted = material.Deleted;
            dto.DemandID = material.DemandID;
       

            return dto;



        }
    }
}
