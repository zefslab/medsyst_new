﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;

namespace Medsyst.Services.Bl
{
    class InputMedicineCrudService : BaseCrudService<MedicamentInput, InputMedicineDto>
    {
        public override InputMedicineDto Update(InputMedicineDto dto)
        {
            throw new NotImplementedException();
        }

        public override InputMedicineDto Get(int Id)
        {
            throw new NotImplementedException();
        }

        public override IList<InputMedicineDto> Get(Data.Filter.BaseFilter _filter)
        {
            throw new NotImplementedException();
        }

        public override ProcessingResult<MedicamentInput> Delete(int id, CommandDirective deleteDirective)
        {
            var result = new ProcessingResult<MedicamentInput>();

            var inputMedicine = new MedicamentInput(true) { MedicamentInputID = id };

            if (!inputMedicine.CheckOutput())
            {
                //TODO обернуть в обертку Warp котрая пока не работает
                if (!inputMedicine.Delete(deleteDirective))
                {
                    //Попытка перманентного удаления приходного медикамента
                    result.Result = ProcessingResults.Error;
                    result.Description = "Не удалось удалить медикамент из приходного ордера.";
                }
                else
                {
                    result.Result = ProcessingResults.Successfull;
                }
            }
            else
            {
                result.Result = ProcessingResults.Error;
                result.Description = "Не возможно удалить медикамент из приходного ордера," +
                    " который уже попал в расход или резерв.";
            }

            return result;
        }
    }
}
