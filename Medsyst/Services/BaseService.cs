﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using Medsyst.Class.Core;
using Medsyst.Data.Core;
using Medsyst.Data.Dto;
using Medsyst.Data.Filter;

namespace Medsyst.Services
{
    public abstract class BaseCrudService<T, TDto>
        where T : DBO
        where TDto : BaseDto
    {

        public abstract TDto Update(TDto dto);
        
        public abstract TDto Get(int Id);

        public abstract IList<TDto> Get(BaseFilter _filter);

        public virtual TDto Add(TDto dto)
        {
            return dto;
        }

        public abstract ProcessingResult<T> Delete(int Id, CommandDirective deleteDirective);

        protected bool Wrap(Action action)
        {
            var connection =  new SqlConnection(DB.CS);
            
            using (var transaction = connection.BeginTransaction())
            {
                try
                {
                    action.Invoke();

                    transaction.Commit();

                }
                catch (Exception exception)
                {
                    ///TODO логировать проблему записи в БД
                    /// Уведомить корректным сообщение пользователя о возникшей проблеме

                    transaction.Rollback();

                    return false;
                }
          
            }
            return true;
        }
    }
}
