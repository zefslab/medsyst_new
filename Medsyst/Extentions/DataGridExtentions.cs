﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace Medsyst.Extentions
{
    internal static class DataGridExtentions
    {
        public static IEnumerable<TResult> Select<TResult>(this DataGridViewSelectedRowCollection  collection, 
                                                                Func<DataGridViewRow, TResult> selector )
        {
            if (collection == null)
            {
                throw new NullReferenceException();
            }

            IList<TResult> result = new  List<TResult>();

            foreach (DataGridViewRow row in collection)
            {
                result.Add(selector(row));
            }

            return result.AsEnumerable();
        }

        /// <summary>
        /// Возврашает значение выделенного элемента списка в грите по указанной колонке
        /// 
        /// </summary>
        /// <typeparam name="T">тип позвращаемого значения</typeparam>
        /// <param name="grid"></param>
        /// <param name="colName"></param>
        /// <returns></returns>
        public static T GetSelectedItemColumnValue<T>(this DataGridView grid, string colName)
        {
            if (grid.SelectedRows.Count <= 0)
            {

                MessageBox.Show("Не выбран элемент списка.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return default(T);
            }
            
           return (T)grid.SelectedRows[0].Cells[colName].Value;

        }

    }
}
