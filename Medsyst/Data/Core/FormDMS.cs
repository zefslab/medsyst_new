﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Medsyst.Class.Core;
using Medsyst.Class.Forms;
using Medsyst;
using System.Windows.Forms;
using System.Reflection;

// Пространство имен специальных обходящих классов для обеспечения документооборота,
// не используемых для прямого программирования

namespace DMS._technical
{
    public partial class Formt : System.Windows.Forms.Form
    {
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Formt
            // 
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Name = "Formt";
            this.Load += new System.EventHandler(this.Formt_Load);
            this.ResumeLayout(false);

        }

        private void Formt_Load(object sender, EventArgs e)
        {

        }
    }
}

/// <summary>
    /// Базовый класс формы, со специфическими функциями системы документооборота
    /// </summary>
    public partial class Form : DMS._technical.Formt
    {
        ///<summary>Интерфейс, передаваемый форме через ствойство Tag</summary>
        public Command cmd;

        /// <summary>Флаг используемый в формах редактирования данных сущности. Если данные в форме изменяются, 
        /// флаг устанавливается в false. После выполнения операции сохранения восстанавливается в true.
        /// Удобно использовать при закрытии формы для контроля сохранения данных
        /// Заплатин Е.Ф.
        /// 16.08.2011
        /// </summary>
        public bool dataSaved = true;


        [EditorBrowsable(EditorBrowsableState.Advanced)]
        protected override void OnLoad(EventArgs e)
        {
            // Обрабатывем Tag на предмет команды формы
            if (Tag == null)
            {
                cmd = new Command(CommandModes.Open);
                Tag = cmd;
            }
            else if (Tag is Command)
                cmd = (Command) Tag;

            // Загрузка значений свойств элементов управления из файла
            var fs = new FormSettings(this, FormSettings.Path);

            SecurityProcess();


            ///////////////////////////////////////////////
            // Вывзов обработчика события загрузки формы
            ///////////////////////////////////////////////
            base.OnLoad(e);

        }

        ///<summary>Вычисление и закрывание элементов управления на которых нет прав доступа</summary>
        private void SecurityProcess()
        {
            FindControls(this.Controls);
        }

        /// <summary>
        /// Поиск контролов на форме привязанных по свойству <code>Tag</code>
        /// </summary>
        /// <param name="controls">Сиписок контролов в котором искать</param>
        /// <param name="objectname">Имя объекта к которому производится привязка</param>
        /// <param name="fields">Описание свойства объекта (рефлексия)</param>
        private void FindControls(Control.ControlCollection controls)
        {
            //foreach (Control count in controls)
            //{
            Type type = this.GetType();


            FieldInfo[] fields = type.GetFields();
            foreach (var field in fields)
            {
                object[] ats = field.GetCustomAttributes(typeof (secAttribute), false);
                if (ats != null && ats.Length > 0)
                {
                    secAttribute a = ats[0] as secAttribute;
                    // поиск разрешенных групп
                    bool b = false;
                    if (a.Items != null && a.Items.Length > 0)
                    {
                        for (int i = 0; i < a.Items.Length; i++)
                        {
                            //Чирков Е.О.
                            //15.03.10
                            ////////////////////////////////////////////
                            foreach (int ugid in Security.user.UGroupIDs)
                                if ((int) a.Items[i] == ugid)
                                    b = true;
                            ////////////////////////////////////////////
                        }
                    }
                    else
                        b = true;

                    if (!b) // если доступ запрещен
                    {
                        object o = null;
                        o = field.GetValue(this);

                        if (o is ToolStripItem) // если тип элемента управления наследуется ToolStripItem
                        {
                            if (a.Hide)
                                ((ToolStripItem) o).Visible = false;
                            else
                                ((ToolStripItem) o).Enabled = false;
                        }
                        else if (o is Control) // иначе если обычный элемент управления
                        {
                            if (a.Hide)
                                ((Control) o).Visible = false;
                            else
                                ((Control) o).Enabled = false;
                        }
                    }
                }
            }

            //FindControls(count.Controls);
            //}
        }
    }


