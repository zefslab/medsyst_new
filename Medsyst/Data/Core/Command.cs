﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Medsyst.Class.Core
{
    ///<summary>Командный интерфейс для обмена информацией между зависимыми формами
    /// вызываемыми каскадно. У таких зависимых форм появляется необходимость 
    /// передавать друг другу информацию.</summary>
    public class Command
    {
        ///<summary>Режим открытия формы</summary>
        public CommandModes Mode = CommandModes.Open;
        
        /// <summary>любой идентификатор передаваемый от одной формы к другой </summary>
        public int Id;

        /// <summary>Ссылка на функцию событие обновления родительской формы </summary>
        public Action OnUpdate;

        /// <summary>Вызывающая форма</summary>
        public Form Form;

        /// <summary>Произвольный объект или коллекция. 
        /// ЗЕФ. 14.01.2012</summary>
        public object obj; 

        /// <summary>Тип класса идентификатор которого передан в интерфейсе. 
        /// Заплатин Е.Ф.26.03.2013 </summary>
        public Type type;//Это свойство позволит передавать идентификаторы объектов различных типов и идентифицировать их на стороне формы

        public Command(CommandModes mode, int id)
        {
            Mode = mode;
            Id = id;
        }
        public Command(CommandModes mode, int id,Form form)
        {
            Mode = mode;
            Id = id;
            Form = form;
        }
        public Command(CommandModes mode, Form form)
        {
            Mode = mode;
            Form = form;
        }
        public Command(CommandModes mode)
        {
            Mode = mode;
        }

        /// <summary>
        /// Функция события - Обновление родительской формы
        /// </summary>
        public void UpdateParent()
        {
            if (OnUpdate != null)
                OnUpdate();
        }
    }
}
