﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst
{
    ///<summary>Атрибут запрещающий видимость элемента управления в зависимости от прав пользователя</summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class secAttribute : System.Attribute
    {
        public Groups[] Items { get; set; }

        ///<summary>Параметр сокрытия видимости, если true то 
        ///элемент управления скрывается, если false - становится не доступным</summary>
        public bool Hide { get; set; }
        public secAttribute() { }
        public secAttribute(Groups[] groups)
        {
            Items = groups;
        }
        /// <summary>Конструктор атрибута запрета доступа к элементу управления</summary>
        /// <param name="groups">Массив групп пользователей имеющих доступ к элементу управления</param>
        /// <param name="hide">Параметр сокрытия видимости, если true то элемент управления скрывается, если false - становится не доступным</param>
        public secAttribute(Groups[] groups, bool hide)
        {
            Items = groups;
            Hide = hide;
        }

    }
}
