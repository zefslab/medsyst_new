﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Medsyst.Class.Core
{        
    ///<summary>Типы фильтра ввода значений для элемента <code>RegularTextBox</code></summary>
    public enum FilterType
    {
        Any = 0,            // Любое значение
        Integer,            // Целое значение
        Double,             // Дробное значение
        Mony,               // Денежные значения
        NotSignedInteger,   // Целое положительное

        [Browsable(false)]//делаем невидимым для редактора свойств
        _count // специальный элемент для подсчета кол-ва элементов енума
    }
    ///<summary>Текстовое поле с фильтрацией вводимого значения</summary>
    public class RegularTextBox: TextBox
    {

        ///<summary>Дробно-числовое значение поля</summary>
        public double Value { get; set; }

        ///<summary>Массив регулярных выражений в зависимости от выбранного фильтра</summary>
        private string[] _filter = new string[(int)FilterType._count+1];

        ///<summary>Фильтр текстового поля</summary>
        [Browsable(true)]// делаем видимым в свойствах контрола
        [Description("Тип вводимого значения")]// описание свойства в редакторе
        public FilterType Filter { get; set; }

        public RegularTextBox(): base()// конструктор с вызовом базового конструктора TextBox
        {
            //Заполняем массив регулярных выражений
            _filter[(int)FilterType.Any] = "";
            _filter[(int)FilterType.Integer] = @"[-+]?\d*";
            _filter[(int)FilterType.Double] = @"[-+]?\d*\,?\d*";
            _filter[(int)FilterType.Mony] = @"\d*\,?\d?\d?";
            _filter[(int)FilterType.NotSignedInteger] = @"\d*";
        }

        //перегружаем событие TextChanged
        protected override void OnTextChanged(EventArgs e)
        {
            string f = _filter[(int)Filter];// выбираем соотвествующее регулярное выражение
            if (f == "") return; // Если регулярное выражение пустое то не фильтруем
            Regex regex = new Regex(f);// задаем реегулярное выражение
            Match match = regex.Match(Text);// обрабатываем введенную строку
            if (match.Success)// если удачно обработалось
            {
                int l = SelectionStart;// запоминаем позицию курсора
                Text = match.Value;// заменяем введенный текст обработанным текстом
                SelectionStart = l;// устанавливаем курсор
                double d = 0;
                if (double.TryParse(match.Value, out d))// проверка на конвертацию в числовой тип
                    Value = d;
                // проверка строки на нулевые значения
                if (match.Value == "" || match.Value == "-" || match.Value == "+" || match.Value == ",")
                    Value = 0;
            }
            base.OnTextChanged(e);//вызываем стандартный обработчик события
        }
    }
}
