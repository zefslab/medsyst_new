﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Medsyst.Class.Core
{
    /// <summary>Класс расширяющий методы стандартного класса TreeView 
    /// Методы расширения определяются как статические методы, но вызываются с помощью синтаксиса 
    /// обращения к методу экземпляра. Их первый параметр определяет, с каким типом оперирует метод, 
    /// и перед параметром идет модификатор this.
    /// Заплатин Е.Ф.
    /// 25.02.2013
    /// </summary> 
    static class TreeViewExtensions //использовал - http://msdn.microsoft.com/ru-ru/library/bb383977
    {

        /// <summary>Формирование списка обгалоченых  узлов в виде списка целочисленных значений, строки идентификаторов и строки наименований узлов
        /// Метод пока обрабатывает только двухуровневую структуру дерева и этого вполне достаточно
        /// Заплатин Е.Ф.
        /// 25.02.2013
        /// <param name="ids">возвращаемый параметр списка обгалоченых узлов в виде строки идентификаторов  с разделетелем "запятая"</param>
        /// <param name="nameNodes">список наименований обгалоченых категорий</param>
        /// <returns>список идентификаторов из обгалоченых узлов </returns>
        /// </summary>
        public static List<int> lstCheckedNodes(this TreeView tv, ref string ids, ref string nameNodes)
        {
            List<int> lstCheckNodes = new List<int>();//Формируемый список обгалоченых узнов

            foreach (TreeNode n in tv.Nodes)
            {
                if (n.Checked)
                {
                    lstCheckNodes.Add(int.Parse(n.Name));//Если категория отмечена
                    ids += ", " + int.Parse(n.Name);//Добавляется в список идентификаторов категорий
                    nameNodes += ", " + n.Text;//Добавляется в список наименований категорий
                }
                foreach (TreeNode cn in n.Nodes)
                {
                    if (cn.Checked)
                    {
                        lstCheckNodes.Add(int.Parse(cn.Name));
                        ids += ", " + int.Parse(cn.Name);//Добавляется в список категорий
                        nameNodes += ", " + cn.Text;//Добавляется в список наименований категорий
                    }
                }
            }
            if (lstCheckNodes.Count <= 0) lstCheckNodes.Add(0);//Если список выделеных категорий оказался пуст то добавляется 0, только зачем?
            if (ids != "")
            {//Обгалочена хотя бы одна категория
                ids = ids.Remove(0, 2); //Удаление лишней запятой  спереди списка
                nameNodes = nameNodes.Remove(0, 2); //Удаление лишней запятой  спереди списка
            }
            else
                ids = "0";//Не выделено ни одной категории

            return lstCheckNodes;
        }
    }
  }
