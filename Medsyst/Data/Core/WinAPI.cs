﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace Medsyst.Class.Core
{
    /// <summary>
    /// Класс использующий системные функции для выполнения некоторых специализированых действий.
    /// Автор: Чирков Е.
    /// </summary>
    public class WinAPI
    {
        private static IntPtr ESNumber = new IntPtr(0x2000);
        private const Int32 GWLStyle = -16;

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowLong(IntPtr Wnd, Int32 Index, IntPtr NewLong);
        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowLong(IntPtr Wnd, Int32 Index);

        /// <summary>
        /// Ограничивает ввод в поле исключительно цифровыми символами
        /// </summary>
        /// <param name="tb">объект текстового поля ввода</param>
        public static void SetTextBoxNumber(TextBox tb)
        {
            IntPtr old = GetWindowLong(tb.Handle, GWLStyle);
            SetWindowLong(tb.Handle, GWLStyle, new IntPtr(WinAPI.ESNumber.ToInt64() | old.ToInt64()));
        }
    }
}
