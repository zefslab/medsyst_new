﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Medsyst
{
    ///<summary>Класс системы безопасности</summary>
    public static class Security
    {
        ///<summary>Текущий пользователь</summary>
        public static User user = new User();

        ///<summary>Аутентификация</summary>
        public static bool Auth(string login, string password)
        {
            Users users = new Users();
            if (users.Load(login, password, loadDeleted:false))
            {
                if (users.Count > 0)
                {
                    user = users[0];

                    //Чирков Е.О.
                    //15.03.10
                    ////////////////////////////////////////////
                    //загрузка групп
                    user.UGroupIDs.Clear();
                    User_UserGroups ugs = new User_UserGroups();
                    ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + user.UserID;
                    ugs.Load();
                    foreach (User_UserGroup ug in ugs) user.UGroupIDs.Add(ug.UGroupID);
                    ///////////////////////////////////////////

                    return true;
                }
                else
                {
                    user = new User();
                    return false;
                }
            }
            else
            {
                user = new User();
                return false;
            }
        }

        ///<summary>Аутентифицирован ли пользователь</summary>
        public static bool IsAuth
        {
            get
            {
                return user != null && user.UserID > 0;
            }
        }
 
        ///<summary>Проверка соответсвия группы текущего пользователя
        ///Автор: Чирков Е.О., 15.03.10
        /// </summary>
        ///<param name="groups">Список групп пользователя</param>
        ///<returns>true - доступ есть, false - доступа нет</returns>
        public static bool Access(params Groups[] groups)
        {
            bool Res = false;
            foreach(Groups group in groups)
                foreach(int ugid in user.UGroupIDs)
                    Res = Res || (ugid == (int)group);
            return Res;
        }
        ///<summary>Проверка соответсвия группы указанного пользователя
        /// Заплатин Е.Ф.
        /// 19.05.2012
        /// </summary>
        /// <param name="uId">указанный пользователь</param>
        ///<param name="groups">Список групп пользователя</param>
        ///<returns>true - доступ есть, false - доступа нет</returns>
        public static bool Access(int uId, params Groups[] groups)
        {
            bool Res = false;
            User u = new User();
            u.UserID = uId;
            u.Load();
            foreach (Groups group in groups)
                foreach (int ugid in u.UGroupIDs)
                    Res = Res || (ugid == (int)group);
            return Res;
        }

        ///<summary>Получение хеша строки по алгоритму md5</summary>
        public static string GetMD5Hash(string input)
        {
            MD5 m = MD5.Create();
            var data = m.ComputeHash(Encoding.Default.GetBytes(input));
            var b = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
                b.Append(data[i].ToString("x2"));
            return b.ToString();
        }
    }
}
