﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto
{
    public class OnDemandMedicineDto : BaseDto
    {
        public string Name { get; set; }
            
        public int MedicamenOnScladID { get; set; }
        
        public int MeasureID { get; set; }
        public int CountOutput { get; set; }
        public bool Deleted { get; set; }

        public bool Modified = false;
    }
}
