﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto
{
    public class OnDemandMaterialDto : BaseDto
    {
        public string Name { get; set; }
        public int MeasureID { get; set; }
        public int CountOutput { get; set; }
        public bool Deleted { get; set; }
        public int DemandID { get; set; }

        /// <summary>
        /// Используется для обратной связи с формой выбора медикаментов на складе
        /// </summary>
        public int MedicamenOnScladID { get; set; }


    }
}
