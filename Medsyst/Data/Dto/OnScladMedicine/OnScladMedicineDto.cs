﻿
namespace Medsyst.Data.Dto
{
    public class OnScladMedicineDto : BaseDto
    {
        public string Name { get; set; }

        ///<summary> Код медикамента из приходного ордера</summary>
        public int InputMedicamentId { get; set; }

        ///<summary>Количество медикаментов на складе </summary>
        public int Count { get; set; }

        public int MeasureId { get; set; }

        ///<summary>Количество зарезервированных медикаментов </summary>
        public int ReservedCount { get; set; } 
        
        ///<summary>Количество медикаментов на складе за вычетом зарезервированных (остаток)</summary>
        public int DividedCount { get { return Count - ReservedCount; } }
    }
}
