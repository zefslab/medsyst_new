﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto
{
    /// <summary>
    /// Класс для отображения списка сезонов в списочных элементах в краткой форме
    /// </summary>
    public class SeasonFullDto : SeasonDto
    {
        public DateTime SeasonOpen { get; set; } // дата открытия сезона
       
        public DateTime SeasonClose { get; set; } //дата закрыттия сезона
    }
}
