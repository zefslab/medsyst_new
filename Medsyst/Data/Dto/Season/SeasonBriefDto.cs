﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto
{
    /// <summary>
    /// Класс для отображения списка сезонов в списочных элементах в краткой форме
    /// </summary>
    public class SeasonBriefDto : BaseDto
    {
        public int Id { get; set; }
        
        public int Number { get; set; } // порядковый номер сезона в году
       
    }
}
