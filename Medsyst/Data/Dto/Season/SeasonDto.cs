﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto
{
    /// <summary>
    /// Класс для отображения списка сезонов в списочных элементах в краткой форме
    /// </summary>
    public class SeasonDto : SeasonBriefDto
    {
        public int Year { get; set; } // год к которому принадлежит сезон

        public string FullNbr
        {
            get
            {
                if (Id != 0)
                    return Number + "/" + Year;
                else
                    return "Не определен";
            }
        }

    }
}
