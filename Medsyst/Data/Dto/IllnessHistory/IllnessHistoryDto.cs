﻿
namespace Medsyst.Data.Dto
{
    public class IllnessHistoryDto : BaseDto
    {
        /// <summary>
        /// Идентификатор истории болезни
        /// </summary>
        public int DocumentId { get; set; }

        /// <summary>
        /// ФИО пациента
        /// </summary>
        public string PatientName { get; set; }

        /// <summary>
        /// Составной номер истории болезни
        /// </summary>
        public string Number { get; set; } 
        
        /// <summary>
        /// ФИО доктора к которому прикреплена история болезни
        /// </summary>
        public string Doctor { get; set; } 
        
        /// <summary>
        /// Сумма стоимости медикаментов назначенных по истории болезни
        /// </summary>
        public decimal SummMedicament { get; set; } 
        
    }
}
