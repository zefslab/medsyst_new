﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Dto.ReceiptOrder
{
    public class ReceiptOrderDto : BaseDto
    {
        public int Number { get; set; }

        public DateTime Date { get; set; }

        public string AccountNbr { get; set; }

        public DateTime AccountDate { get; set; }

        public string SourceFinancing { get; set; }

        public string Provider { get; set; }

        public decimal Summ { get; set; }

        public bool isSigned { get; set; }

        public bool isDeleted { get; set; }

        public string Comment { get; set; }

    }
}
