﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Constant
{
    /// <summary>
    /// Результаты выполнения какого либо сложно-составного процесса
    /// Например, импорт, парсинг, валидация, систематизация и пр.
    /// </summary>
    public enum ProcessingResults
    {
        NonResult = 0,

        Successfull = 1,

        Error = 2,

        NotCritical = 3
        ///В случае добавления нового элемента в список необходимо дополнить метод
        /// SummingResults() класса ProcessingResult
    }
}
