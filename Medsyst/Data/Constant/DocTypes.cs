﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Constant
{
    ///<summary>Список типов документов
    ///Заплатин Е.Ф,
    ///06.08.2011</summary>
    public enum DocTypes
    {
        /// <summary>Амбулаторная карта</summary>
        AmbulaceCard = 1,
        /// <summary>Приходный ордер</summary>
        ReciptOrder = 2,
        /// <summary>Требование по услугам	Медикаменты по требованию выдаются на оказание назначеных услуг</summary>
        DemandService = 3,
        /// <summary>История болезни</summary>
        IllnessHistory = 4,
        /// <summary>Акт на списание</summary>
        ActOffCharge = 5,
        /// <summary>Кассовый чек</summary>
        Check = 6,
        /// <summary>Требование по медикаментам	назначенным непосредственно пациентам</summary>
        DemandMedicament = 7,
        /// <summary>Требование по расходным материалам для процедурных кабинетов без учета расхода</summary>
        DemandMaterial = 8
    }
}
