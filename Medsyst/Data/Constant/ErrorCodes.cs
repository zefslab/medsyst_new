﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Constant
{
    /// <summary>
    /// Всевозможные ошибки, кторые могут возникать исходя из общего функционала системы
    /// Каждый модуль может дополнять этот список, исходя из специфики привносимого им функионала
    /// </summary>
    public enum ErrorCodes
    {
        Success = 0,

        IncorrectData = 1,

        CodeNotExist = 2,

        DatabaseError = 3

    }
}
