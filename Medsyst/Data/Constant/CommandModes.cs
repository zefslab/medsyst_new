﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Class.Core
{
    ///<summary>Список команд-режиомв формы</summary>
    public enum CommandModes
    {
        Open,   // режим открытия формы
        Edit,   // режим редакатирования объекта
        Veiw,   // режим только просмотра содержимого формы
        New,    // режим создания нового объекта
        Select, // режим единственного выбора объекта
        SelectWithUnavaleble, // режим выбора с отметкой недоступных элементов по каким либо признакам
        MultiSelect, // режим множественного выбора объектов не закрывая формы выбора. ЗЕФ. 14.01.2012
        Return,  // этот флаг ставится при возврате формой результата
        Cancel   // этот флаг ставится при закрытии формы и отказе от сохранения данных
    }
}
