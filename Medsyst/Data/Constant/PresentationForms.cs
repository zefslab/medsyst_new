﻿
namespace Medsyst.Data.Constant
{
    /// <summary>
    /// Перечень форм представления. Используется в фильтрах для изменения состава данных
    /// в зависимости от места их представления
    /// </summary>
    public  enum PresentationForms 
    {
        MedicamentOutput =  1,

        ServiceOutput
    }
}
