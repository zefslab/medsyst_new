﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Class.Core
{
    ///<summary>Список команд-директив (указаний харктера действия) Используется в качестве аргументов в некоторых 
    ///функциях для указания характера производимых действий
    ///Автор: Заплатин
    ///Дата: 23.12.2010
    /// </summary>
    public enum CommandDirective
    {
        /// <summary>Удаление без возможности восстановления</summary>
        DeletePermanently,
        /// <summary>Отметить как удаленный но не удалять</summary>
        MarkAsDeleted,
        /// <summary>Добавить</summary>
        Add,
        /// <summary>Ни какой директивы нет</summary>
        Non,
        /// <summary>Добавление в список-коллекцию записи с неопределенным значением. ЗЕФ 08.08.2011</summary>
        AddNonDefined,
        /// <summary>Загружать удаленные записи. ЗЕФ 19.08.2011</summary>
        LoadDeleted,
        /// <summary>Не загружать записи отмеченные как удаленные. ЗЕФ 20.08.2011</summary>
        NotLoadDeleted
    }
}
