﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Class.Core
{
    ///<summary>Источники финансирования
    ///Автор: Заплатин
    ///Дата: 08.09.2011
    /// </summary>
    public enum Finance
    {
        /// <summary>Без принадлежности к источнику финансирования</summary>
        Non = -1,
        /// <summary>Внебюджетный источник финансирования</summary>
        ExtraBudget = 0,
        /// <summary>Бюджетный источник финансирования</summary>
        Budget = 1
    }
}
