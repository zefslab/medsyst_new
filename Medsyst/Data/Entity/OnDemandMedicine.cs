﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;


namespace Medsyst.Class
{
    ///<summary>Класс: Медикамент, выписанный по требованию 
    ///</summary>
    public class OnDemandMedicine : Medicament
    {
        [pk]
        [db("MedicamentByDemand")]
        public int MedicamentByDemandID { get; set; }
        /// <summary>Медикамент в каталоге</summary>
        [fk("Medicament", "MedicamentID")]
        [db("MedicamentByDemand", "MedicamentID")]
        public int _MedicamentID { get; set; }
        //[fk("MedicamentService", "ServiceMedicamentID")][db("MedicamentByDemand", "ServiceMedicamentID")]public int ServiceMedicamentID_ { get; set; }
        [db("MedicamentByDemand")]
        public int DemandID { get; set; }
        /// <summary>Количество  медикаментов выписанных и полученных по требованию</summary>
        [db("MedicamentByDemand")]
        public int CountGet { get; set; }
        /// <summary>Остаток медикаментов еще не списанных с требования, при реальном расходе медикаментов</summary>
        [db("MedicamentByDemand")]
        public int CountResidue { get; set; }
        /// <summary>Признак того, что медикамент по требованию удален. </summary>
        [db("MedicamentByDemand")]
        public bool mbdDeleted { get; set; }

        public OnDemandMedicine() { }
        public OnDemandMedicine(bool init)
            : base(init)
        {
            MedicamentByDemandID = _MedicamentID = DemandID = 0; //ServiceMedicamentID_ =
            CountGet = CountResidue = 0;
            mbdDeleted = false;
        }
        /// <summary>Создание нового экземляра медикамента по требованию
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// </summary>
        /// <param name="medicamentID">идентификатор каталожного медикамента</param>
        /// <param name="docID">идентификатор расходного документа</param>
        public void New(int medicamentID, int docID)
        {
            var m = new Medicament(true);
            m.MedicamentID = medicamentID;
            m.Load();

            MedicamentID = _MedicamentID = m.MedicamentID;
            MedicamentN = m.MedicamentN;
            MeasureID = m.MeasureID;
            DemandID = docID;
            NotSaved = true;
        }
        public new bool Insert()
        {
            _MedicamentID = MedicamentID;
            if (Insert("MedicamentByDemand"))
            {
                MedicamentByDemandID = Identity("MedicamentByDemand");
                return true;
            }
            return false;
        }
        /// <summary>Удаляется из базы или делается отметка об удалении медикамента связанного с требованием 
        /// Заплатин Е.Ф.
        /// 19.08.2011
        /// </summary>
        /// <returns></returns>
        public bool Delete(CommandDirective permanently)
        {
            if (permanently == CommandDirective.DeletePermanently)
            {//Запись о медикаменте в базе данных удаляется полностью
                return base.Delete("MedicamentByDemand");
            }
            else
            {//Медикаменту устанавливается признак удаленного
                mbdDeleted = true;
                return Update();
            }
        }
        public new bool Update()
        {
            _MedicamentID = MedicamentID;
            return base.Update("MedicamentByDemand");
        }
        /// <summary>Уменьшение остатков по требованию в связи со снятием с резерва медикаментов во время выписки дефектной ведомости
        /// Заплатин Е.Ф.
        /// 05.09.2011
        /// </summary>
        public void UnreservateMaterial()
        {
            var d = new DemandMaterial(true);
            d.DocumentID = DemandID;
            d.Load();//Загружается требование для изменения суммы 

            //Материалы списываются целиком
            var mos = new OutputMedicamentList();
            if (mos.Load(d.DocumentID, MedicamentID))//Загрузка медикамента из расхода, соответсвующего медикамету выписанному по требованию. В требованиях на матреиалы 
            //в отличии от других требований это соответсвие однозначное
            {
                if (mos.Count == 1)
                {
                    d.SummMedicament += CountResidue * mos[0].Price; //Рассчет суммы средств на отпущенные медикаменты  по требованию
                    CountResidue = 0;
                    Update();//Обновление остатка медикамента
                    d.Update();//Обновление изменений в требовании
                }
                else
                {
                    MessageBox.Show("Найдено более одного расходного медикамента соответствующего медикаменту по требованию. \n\r" +
                                    "Такая ситуация в принцыпе не возможна. Обратитесь к разработчикам системы Медсист",
                                    "Недопустимая ситуация", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //TODO сделать запись ошибки в базу данных
                }
            }
            else
            {
                MessageBox.Show("Не найдено ни одного расходного медикамента соответствующего медикаменту по требованию. \n\r" +
                                "Такая ситуация в принцыпе не возможна. Обратитесь к разработчикам системы Медсист",
                                "Недопустимая ситуация", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //TODO сделать запись ошибки в базу данных
            }
        }
        /// <summary>Проверка остатка на равенство выписанному по требованию количеству
        /// Заплатин Е.Ф.
        /// 09.09.2011
        /// </summary>
        /// <returns>возвращается наименование медикамента, если он уже выдавался пациентам</returns>
        public string ChekResidue()
        {
            string res = "";
            if (CountResidue != CountGet)
                //Медикамент уже выдавался пациенту
                res = MedicamentN;
            return res;
        }
    }
    /// <summary>Коллекция медикаментов по требованию
    /// </summary>
    public class MedicamentByDemandList : DBOList<OnDemandMedicine>
    {
        /// <summary>Загрузка списка медикаментов по требованию</summary>
        /// <param name="doc_id">идентификатор требования</param>
        public bool Load(int doc_id, CommandDirective loadDeleted)
        {
            dbWhere = " DemandID = " + doc_id.ToString();
            //Не загружать или загружать удаленные медикаменты по требованиям
            dbWhere += (loadDeleted == CommandDirective.LoadDeleted) ? "" : " AND mbdDeleted = 0";
            return base.Load();
        }
        /// <summary>Загрузка списка медикаментов выписанных по требованию по указанному идентификатору услуги, 
        /// идентификатору медикамента и идентификатору одного из трех типов требований.
        /// Заплатин Е.Ф.
        /// 09.09.2011</summary>
        /// <param name="service_id">идентификатор услуги</param>
        /// <param name="m_id">идентификатор медикамента </param>
        /// <param name="docType">идентификатор типа требования</param> ЗЕФ 14.08.2011
        /// <param name="NotZeroResidue">Не нулевой остаток </param>ЗЕФ 22.08.2011
        public bool Load(int service_id, int m_id, int docType, bool NotZeroResidue, Finance f)
        {
            dbSelectUser = " SELECT Medicament.*, MedicamentByDemand.* " +
                            " FROM MedicamentByDemand" +
                //" Left Join MedicamentService on MedicamentByDemand.ServiceMedicamentID=MedicamentService.ServiceMedicamentID " +
                            " Left Join Medicament on Medicament.MedicamentID=MedicamentByDemand.MedicamentID" +
                            " Left Join Document  as d  on d.DocumentID=MedicamentByDemand.DemandID" +
                            " Left Join DocDemand  as dd  on d.DocumentID=dd.DocumentID";
            dbWhere = "mbdDeleted = 0"; //Не загружать удаленные медикаменты по требованиям
            dbWhere += " AND d.DocTypeID = " + docType.ToString(); //Медикаменты выписанные для требований конкретного типа. ЗЕФ 14.08.2011
            dbWhere += " AND d.Deleted = 0"; //Не учитывать удаленные требования
            if (f != Finance.Non)
                dbWhere += " AND dd.IsBudget = " + ((int)f).ToString();//Источник финансов имеет значение
            dbWhere += " AND dd.ServiceID = " + service_id.ToString(); //Выбирать только те медикаменты которые выписаны по требованиям для оказания конкретной услуги ЗЕФ 14.08.2011
            dbWhere += " AND Medicament.MedicamentID = " + m_id.ToString(); //Выбирать только интересующий медикамент
            dbWhere += (NotZeroResidue ? " AND CountResidue > 0" : ""); //Критерий отбора медикаментов для незакрытых требований, т.е. с ненулевым остатком
            dbOrder = "d.RegisterD" +//Такой порядок загрузки требуется для того, чтобы гарантировать списание медикаментов от более старых требований. ЗЕФ, 07.08.2011
                (NotZeroResidue ? "" : " DESC");//Обратный порядок загрузки требуется для того, чтобы гарантировать возвращение медикаментов от более новых требований. ЗЕФ, 22.08.2011
            return base.Load();
        }

        /// <summary>Удаление коллекции медикаментов. Предусматривает удаление из базы данных медикаментов уже ранее сохраненных в ней
        /// Заплатин Е.Ф.
        /// 14.09.2011
        /// </summary>
        /// <param name="permanently"> указывает удалять полностью или устанавливать лишь признак удаления</param>
        public bool Delete(CommandDirective permanently)
        {
            bool res = true;
            foreach (OnDemandMedicine m in this)
            {
                if (m.NotSaved)
                {//медикамент коллекции небыл ранее сохранен в базе, поэтому достаточно его исключить из коллекции
                    //не требуется удаление из базы 
                }
                else
                {
                    res = res && m.Delete(permanently);
                }
            }
            if (res) this.Clear();//Очистка коллекции после успешного удаления всех ее элементов
            return res;
        }
        /// <summary>Сохраняются в базе все экземпляры, отмеченные как не сохраненные
        /// Заплатин Е.Ф.
        /// 17.08.2011
        /// </summary>
        public void SaveNotSaved()
        {
            foreach (OnDemandMedicine m in this)
            {
                if (m.NotSaved)
                {//Медикамент отмечен как не сохраненный
                    m.Insert();//Добавляется в базу
                    m.NotSaved = false;
                    m.Modified = false;
                }
            }
        }
        /// <summary>Обновляются в базе все экземпляры, отмеченные как измененные
        /// Заплатин Е.Ф.
        /// 17.08.2011
        /// </summary>
        public void UpdateModified()
        {
            foreach (OnDemandMedicine m in this)
            {
                if (m.Modified)
                {//Медикамент отмечен как не сохраненный
                    m.Update();//Обновляется информация об экземпляре в базе
                    m.Modified = false;
                }
            }
        }
        /// <summary>Проверка достаточности количества выписанных медикаментов по требованиям для списания медикамента зарезервированного в расход
        /// Заплатин Е.Ф.
        /// 20.08.2011
        /// </summary>
        /// <param name="count">количество медикаментов для списания</param>
        /// <param name="mID">Идентификатор медикамента который предполагается списать</param>
        /// <param name="f">источник финаисрования</param>
        public bool CheckCount(int count, int mID, Finance f)
        {
            //MedicamentByDemandList mbds = new MedicamentByDemandList();

            //mbds.Load(0, mID, (int)DocTypes.DemandMedicament, true, f);//Загружаются  медикаменты с остатком большим ноля (true) по всем требованиям, принадлежащим конкретному медикаменту 
            int md_summ = 0;//Инициализации суммы всех медикаментов по требованиям с ненулевым остатком
            foreach (OnDemandMedicine mbd in this)
            {//Перебираются все медикаменты по требованиям
                md_summ += mbd.CountResidue;//Сумма увеличивается на количество оставшихся по требованию медикаментов

            }
            if (md_summ < count)
            {//выдаваемое количество пациенту медикамента превосходит количество выписанное по требованиям.
                MessageBox.Show("Количество медикамента - " + count.ToString() +
                                " превышает количество выписанного по требованиям - " + md_summ.ToString() +
                                ".\r\nВ связи с этим медикамент невозможно выдать пациенту.", "Предупреждение",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            return true;//Остатков медикамента по требованиям достаточно для выдачи пациенту
        }
        /// <summary>Уменьшение остатка по требованиям при списании медикамента
        /// Заплатин Е.Ф.
        /// 05.09.2011</summary>
        public void UnreservateMaterial()
        {
            foreach (OnDemandMedicine mbd in this)
            {
                mbd.UnreservateMaterial();
            }
        }
    }

}