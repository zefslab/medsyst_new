﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /// <summary> Класс: Медицинская услуга
    /// автор: Заплатин Е.Ф.
    /// создан: 30.11.2009
    /// </summary>
    public class Service : DBO
    {
        /// <summary>Идентификатор услуги</summary>
        [db("Service")][pk]
        public int ServiceID { get; set; }

        /// <summary>Название услуги</summary>
        [db("Service")]
        public string ServiceN { get; set; }

        /// <summary>Шифр услуги</summary>
        [db("Service")]
        public string Code { get; set; }

        /// <summary>FK идентификатор единицы измерения</summary>
        [db("Service")]
        public int MeasureID { get; set; }

        /// <summary>Стоимость услуги</summary>
        [db("Service")]
        public decimal Cost { get; set; }

        /// <summary>ориентировочная продолжительность процедуры по времени</summary>
        [db("Service")]
        public string Duration { get; set; }

        /// <summary>Комментарий к услуге</summary>
        [db("Service")]
        public string Comment { get; set; }
        /// <summary>FK идентификатор категории</summary>
        [db("Service")]
        public int CategoryID { get; set; }

        /// <summary>Стоимость медикаментов услуги</summary>
        [db("Service")]
        public decimal PriceMed { get; set; }

        /// <summary>Норматив объема работ на оказание услуги в условных единицах </summary>
        [db("Service")]
        public int Norm { get; set; }

        /// <summary>Номер комнаты в которой оказывается услуга</summary>
        [db("Service")]
        public string Room { get; set; }

        ///<summary>Признак удаления услуги</summary>
        [db("Service")]
        public bool Deleted { get; set; }

        ///<summary>Источник финансирования к которому нужно привязать услугу. ЗЕФ. 13.01.2012</summary>
        public Finance finance { get; set;  }

        ///<summary>Признак доступности услуги для назначения пациенту в зависимости от источника финансирования. ЗЕФ. 07.01.2012</summary>
        public bool Accessed { get {return DefineAccess(finance);} }
        
        public Service () { }
        /// <summary>Конструктор класса</summary>
        /// <param name="init">Параметр инициализации ORM механизма для данного объекта.</param>
        public Service (bool init) : base(init) 
        {
            //Инициализация свойств класса
            MeasureID = 1;
            Cost = 0;
            ServiceN = "";
            Code = "";
            Comment = "";
            Duration = "0";
            Room = "";
            finance = Finance.Non;//Источник финансирования уточняется позже
            Norm = 0;
            
        }
        /// <summary>Удаление услуги
        /// 
        /// </summary>
        /// <param name="directive">Указание характера выполнения удаления (полное удаление или отметка об удалении)</param>
        public bool Delete(CommandDirective directive)
        {
            if (directive == CommandDirective.DeletePermanently)
            {//Удаляется информация из базы данных
                return base.Delete();
            }
            else
            {//Устанавливается признак удаления записи
                Load();//Загрузка всех свойств необходима перед выполнением метода Update иначе все поля будут проинициализированы значениями по умолчанию.
                Deleted = true;//Назначается признак удаления записи
                return Update();//Обновляется информация в базе данных
            }
        }
        /// <summary>Определение доступности услуги по наличию связанных с ней 
        /// медикаментов на складе и принадлежности их к определенному источнику финансирования
        /// Заплатин Е.Ф.
        /// 12.01.2012
        /// </summary>
        public bool DefineAccess(Finance finance)
        {
            MedicamentServiceList mss = new MedicamentServiceList();
            mss.Load(ServiceID, false);
            foreach (MedicamentService ms in mss)
            {
                //Проверка наличия каждого медикамента на складе
                MedicamentOnScladList moss = new MedicamentOnScladList();
                if (!moss.Load(ms.MedicamentID, true, false, finance))//Загрузка складских медикаментов  которых есть наличие
                {//Если не найден хотя бы один медикамент с ненулевым количеством, то услуга становится недоступной
                    return false;
                }
            }
            return true;
        }
    }

    /// <summary>Класс:  Коллекция медицинских услуг
    ///автор: Заплатин Е.Ф.
    ///создан: 01.12.2009
    /// последнее изменение:01.12.2009
    /// </summary>
    public class ServiceList : DBOList<Service>
    {
        /// <summary>Загрузка списка услуг в алфавитном порядке с возможностью добавления неопределенной записи
        /// Заплатин Е.Ф.
        /// 08.08.2011
        public bool Load(CommandDirective cd)
        {
            if (!showdeleted)
                dbWhere = "Deleted = 0";
            else
                dbWhere = "";
            dbOrder = "ServiceN";
            bool res = base.Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Service s = new Service(true);
                s.ServiceID = 0;
                s.ServiceN = "--Услуга не определена--";
                this.Add(s);
            }
            return res;
        }
        /// <summary>Загрузка списка услуг для указанной категории
        /// Заплатин Е.Ф.
        /// 07.07.2011
        /// </summary>
        /// <param name="CategoryID">категория услуг</param>
        /// <returns></returns>
        public bool Load(int CategoryID)
        {
            if (!showdeleted)
                dbWhere = " AND Deleted = 0";
            else
                dbWhere = "";
            dbWhere += " AND CategoryID = " + CategoryID.ToString();
            if (dbWhere != "")
                dbWhere = dbWhere.Remove(0, 4);

            dbOrder = "ServiceN";
            return base.Load();
        }
         /// <summary>Загрузка списка услуг для указанной категории с учетом принадлежности к источнику финансирования
        /// Заплатин Е.Ф.
        /// 13.01.2012
        /// </summary>
        /// <param name="CategoryID">категория услуг</param>
        /// <returns></returns>
        public bool Load(int CategoryID, Finance f)
        {
            bool res = this.Load(CategoryID);//Загружаются все услуги в указанной категории 
            //Проставляется источник финасирования и определяется доступность услуг
            foreach (Service s in this)
            {
                s.finance = f;//Явное указание источника финансирования приводит к изменению доступности услуги (см. определеине свойства Accessed)
            }
            return res;
        }
         /// <summary>Загрузка списка услуг назначенных, но еще не оказанных
        /// Заплатин Е.Ф.
        /// 13.01.2012
        /// </summary>
        /// <param name="CategoryID">категория услуг</param>
        /// <returns></returns>
        public new bool Load()
        {
            dbSelectCustom = " Select s.* " +
            "  FROM Service as s " +
            "  RIGHT JOIN (" +
                    " SELECT ServiceID, COUNT(ServiceID) as cnt" +
                    " FROM ServiceOutput" +
                    " LEFT JOIN Document as d on d.DocumentID=ServiceOutput.DocumentID" +
                    " Where ServiceOutput.Deleted = 0" + //За исключением удаленных услуг
                    " AND d.Deleted = 0" + //За исключением удаленных документов по котороым была назначена услуга  +  
                    " AND Fulfil = 0" + //Не отмеченные как уже оказанные
                    " AND NOT ServiceID IS NULL" + //Исключая несуществующие услуги
                    " AND ServiceID <> 0" + // 
                    " GROUP BY ServiceID"+ 
                    " ) as so on s.ServiceID = so.ServiceID";
 

            return base.Load();
        }
         
    }
    
    
    
}
