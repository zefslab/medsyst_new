﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;

namespace Medsyst.Data.Entity
{
    ///<summary>Класс: Требование на склад, для выдачи расходных материалов реализуемых в процедурных кабинетах без учета
    ///В отличии от других классов требований этот клас является дополнительно и расходным документом
    ///Заплатин Е.Ф.
    ///06.08.2011
    ///</summary>
    public class DemandMaterial : Demand
    {
        public DemandMaterial() { }
        public DemandMaterial(bool init)
            : base(init)
        {
            DocTypeID = (int)DocTypes.DemandMaterial;// тип документа
        }
        /// <summary>Загрузка расходных медикаментов связанных с требованием
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// <returns>коллекция расходных медикаментов</returns>
        /// </summary>
        public OutputMedicamentList LoadMedsOutput(bool loadDeleted)
        {
            OutputMedicamentList mos = new OutputMedicamentList();
            mos.Load((loadDeleted ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted), DocumentID);
            return mos;
        }

    }
   

}
