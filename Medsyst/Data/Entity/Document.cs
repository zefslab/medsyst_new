﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;


namespace Medsyst.Class
{
   
    /// <summary>Класс: Любой документ. Используется в качестве родительского класса для документов любых типов и видов
    /// </summary>
    public class Document : DBO 
    {
        [pk][db("Document",Name="DocumentID")]public int DocumentID_ {get;set;}
        /// <summary>номер документа	</summary>
        [db("Document")]public int Nbr { get; set; }
        /// <summary>дата регистрации документа	</summary>
        [db("Document")]public DateTime RegisterD { get; set; }
        /// <summary>тип документа</summary>
        [db("Document")]public int DocTypeID { get; set; }
        /// <summary>Коментарий или примечание</summary>
        [db("Document")]public string Comment { get; set; }
        /// <summary>Признак того, что документ считается удаленным</summary>
        [db("Document")]public bool Deleted { get; set; }
        /// <summary>Признак того, что документ подписан и внесение изменений в него невозможно. ЗЕФ. 16.08.2011</summary>
        [db("Document")]public bool Signed { get; set; }
        /// <summary>идентификатор выполняемой задачи над документом. ЗЕФ. 29.06.2012</summary>
        [db("Document")]public int TaskID { get; set; }
        /// <summary>Признак того, что текущая задача завершена, а следующая еще не начата. ЗЕФ. 29.06.2012</summary>
        [db("Document")]public bool TaskComplit { get; set; }
        /// <summary>Дата завершения выполнения задачи. ЗЕФ. 29.06.2012	</summary>
        [db("Document")]public DateTime TaskComplitD { get; set; }

        
        public Document() 
        {
            DocumentID_ = TaskID = 0;
            RegisterD = TaskComplitD = new DateTime(1900, 01, 01);
            Comment = "";
            Signed = TaskComplit = false;
        }
        public Document(bool init) : base(init) 
        {
            DocumentID_ = TaskID = 0;
            RegisterD = TaskComplitD = new DateTime(1900, 01, 01);
            Comment = "";
            DocTypeID = 0; //тип документа указывается в производных классах
            Deleted = false;
            Nbr = 0;
            Signed = TaskComplit = false;
        }
        /// <summary>Наложение или снятие подписи на документ. После того как документ будет подписан, внесение изменений в него станет невозможным.
        /// Заплатин Е.Ф.
        /// 16.08.2011
        /// </summary>
        public void Sign()
        {
            if (Signed)
                Signed = false;
            else
                Signed = true;
        }
        /// <summary>Находит документ указанных типов с последним зарегистрированным номером и вычисляет новый номер на единицу больше
        /// </summary>
        /// <param name="docTypes">список типов документов разделенных запятой </param>список нужен для того чтобы можно было реализовать сквозную нумерацию у документов с разными типами
        /// например у всех требований
        /// <param name="absolut">Указывает на то, что нужно получить абсолютный номер - true, или вычесленный относительно текущего года - false</param>
        /// <Author>Заплатин Е.Ф.</Author>
        /// <date>18.01.2011</date>
        /// <returns>Новый номер документа</returns>
        public int GetNewNumber(string docTypes, bool absolut)
        {
             //Получить отсортированую по номерам коллецию документов по индивидуальному запросу
            DocumentList docList = new DocumentList();
            if (docList.Load(docTypes, !absolut) & docList.Count != 0 )//Запрос возвращает всего одну запись с последним номером документа указанного типа
            {
                return docList[0].Nbr+1;//Новый номер - следующий за последним
            }
            else
            {
                return 1; //Первый документ
            }
        }
    }
    
    public class DocumentList : DBOList<Document>
    {
        /// <summary>Используется и оптимизорован исключительно для определения последнего номера документа
        /// </summary>
        /// <Author>Заплатин Е.Ф.</Author>
        /// <date>18.01.2011</date>
        /// <param name="docTypes">список типов документов, у которых следует организовать сквозную нумерацию</param>
        /// <param name="thisYear">true - если принимаются документы только этого года, false - по всем документам не привязываясь к временным периодам</param>
        /// <returns></returns>
        public bool Load(string docTypes, bool thisYear)
        {

            string where = "Deleted = 0 AND DocTypeID IN (" +docTypes+ ")"; //Выбираются документы определенных типов
            if (thisYear)
            {//Требуется принимать во внимание только документы за текущий год
                where += " AND DATEPART(YEAR, RegisterD) = " + DateTime.Today.Year.ToString();
            }
            string order = "Nbr DESC"; //Сортировка в обратном порядке по номеру, чтобы проще было получить документ с последним номером
            dbSelectCustom = "SELECT TOP 1 *" +//Возвращается только одна запись  DocumentID , Number, DocTypeID, RegisterDate
                             " FROM Document" +
                             " WHERE " + where +
                             " ORDER BY " + order;
            return base.Load();
        }
    }


    
   
      

     
    

}
