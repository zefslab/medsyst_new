﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;

namespace Medsyst.Data.Entity
{
    ///<summary>Класс: Требование на склад, для выдачи медикаментов реализуемых непосредственно пациентам по выписке
    ///Заплатин Е.Ф.
    ///06.08.2011
    ///</summary>
    public class DemandMedicament : Demand
    {
        public DemandMedicament() { }
        public DemandMedicament(bool init)
            : base(init)
        {
            DocTypeID = (int)DocTypes.DemandMedicament;// тип документа
        }
        /// <summary>Загрузка расходных медикаментов для формирования требования
        /// Заплатин Е.Ф.
        /// </summary>
        /// <returns></returns>
        public OutputMedicamentList LoadOutputMeds()
        {
            //Выбираются все неудаленные расходные документы определнных типов
            List<int> dts = new List<int>();//Список типов расходных документов по которым нужно искать медикаменты
            dts.Add((int)DocTypes.IllnessHistory);//Используется пока только один тип расходных документов

            OutputMedicamentList mos = new OutputMedicamentList();
            Finance fin;
            if (IsBudget) fin = Finance.Budget; else fin = Finance.ExtraBudget;
            bool countIsNotZero = true;//Медикаенты с нулевым количеством выбирать нет необходимости 
            mos.Load(fin, CommandDirective.NotLoadDeleted, countIsNotZero, dts);//Загрузка производится в ранжированном порядке по идентификаторам медикаментов (MedicamentID)

            return mos;
        }

        /// <summary>Добавляются коллекция медикаментов к требованию из числа назначенных пациентам.
        /// Для этого из числа назначенных медикаментов остаются лишь те, которые еще не выданы 
        /// по уже выписанным требованиям 
        /// Заплатин Е.Ф.
        /// 14.08.2011
        /// </summary>
        /// <param name="medOnSclad">Коллекция назначеных медикаментов</param>
        /// <param name="f">источник финасирования</param>
        /// <returns>коллекция медикаментов по требованию</returns>
        public MedicamentByDemandList AddMeds(OutputMedicamentList mos, Finance f)
        {
            MedicamentByDemandList mbds_result = new MedicamentByDemandList();//Формируемая коллекция медикаментов по требованию
            foreach (MedicamentOutput mo in mos)
            {//Для каждого назначенного медикамента находятся соответсвующие медикаменты, выписанные по требованиям с остатком больше нуля (true) и с учетом
                //финансового источника
                MedicamentByDemandList mbds = new MedicamentByDemandList();
                if (mbds.Load(0, mo.MedicamentID, DocTypeID, true, f))//Ноль означает, что медикаменты выписаны не по услугам
                {//Найдено некоторое количество медикаментов
                    foreach (OnDemandMedicine mbd in mbds)
                    {//Каждый найденный медикамент уменьшает требуемое количество медикамента 
                        //для выпски по требованию на величину остатка
                        mo.CountOutput = mo.CountOutput - mbd.CountResidue;
                    }
                }
                if (mo.CountOutput > 0)
                {//Если после возможного вычитания остатков по требованиям осталось положительное количество назначенного медикамента
                    //значит нужно выписать по требованию  дополнительное его количество
                    //Формируется новый элемент коллекции медикаментов по требованию
                    OnDemandMedicine mbd_result = new OnDemandMedicine(true);
                    mbd_result.MedicamentID = mo.MedicamentID;
                    mbd_result.MedicamentN = mo.MedicamentN;
                    mbd_result.MeasureID = mo.MeasureID;
                    mbd_result.CountGet = mbd_result.CountResidue = mo.CountOutput;
                    mbd_result.DemandID = DocumentID;
                    mbd_result.NotSaved = true;
                    //Этого количества информации по медикаменту пока достаточно для отображения в таблице. После 
                    //сохранения в базе данных появится полная информация
                    mbds_result.Add(mbd_result);
                }
            }
            if (mbds_result.Count == 0)
            {
                MessageBox.Show("Нет потребности в выдаче медикаментов по требованию.",
                "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return mbds_result;
        }
    }
   
}
