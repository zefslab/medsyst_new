﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;

namespace Medsyst.Data.Entity
{
    ///<summary>Класс: Требование на склад, для выдачи медикаментов реализуемых через услуги
    ///Заплатин Е.Ф.
    ///06.08.2011
    ///</summary>
    public class DemandService : Demand
    {
        /// <summary>Услуга из прейскуранта</summary>
        [db("DocDemand")]
        public int ServiceID { get; set; }

        public DemandService() { }
        public DemandService(bool init)
            : base(init)
        {
            ServiceID = 0;
            DocTypeID = 3;// тип документа - требование на склад
        }
        /// <summary>Формируется коллекция медикаментов  по указанной услуге в таблицу OnDemandMedicine, 
        /// выписываемых по требованию.
        /// Автор: Заплатин Е.Ф.
        /// 05.12.2010
        /// 07.11.2012 Изменил количество параметров и преобразовал функцию в статическую, изменил название (старое AddMeds)
        /// чтобы сделать ее более универсальной (ЗЕФ)
        /// </summary>
        /// <param name="ServiceID">услуга по которой нужно добавить связанные с ней медикаменты</param>
        /// <param name = "isBudget">принадлежность к бюджету</param>
        /// <param name = "documentID">идентификатор требования на склад</param>
        /// <param name="pressmsg">условие подавления сообщений о наличии медикаментов</param>
        /// <returns>Коллекция расходуемых медикаментов</returns>
        public static MedicamentByDemandList Meds(int ServiceID, bool isBudget, int documentID, bool pressmsg = false)
        {
            Finance f;
            if (isBudget) f = Finance.Budget; else f = Finance.ExtraBudget;//Выявляется источник финансирования
            MedicamentByDemandList mbds = new MedicamentByDemandList();//Инициируется экзкмпляр коллекции списка медикаментов по требованию
            // Шаг 1. Формируется коллекция медикаментов по выбранной в требовании услуге
            MedicamentServiceList ms = new MedicamentServiceList();
            ms.Load(ServiceID, false);//Загружаются медикаменты по идентификатору услуги
            // Шаг 2. Обрабатывается каждый медикамент из коллекциимедикаментов связанный с услугой для формирования коллекции MedicamentByDemandList. 
            foreach (MedicamentService m in ms)
            {
                //Шаг 3. Добавляется медикамент в таблицу OnDemandMedicine тем самым формируется коллекция MedicamentByDemandList
                OnDemandMedicine mbd = new OnDemandMedicine(true);
                mbd.DemandID = documentID; //Идентифмкатор, ссылающийся на требование
                mbd.MedicamentID = m.MedicamentID;//Идентификатор ссылающийся на каталожный  медикамент
                mbd.MedicamentN = m.MedicamentN;//Это присвоение требуется для отображения медикамента в таблице до момента его вставки в базу данных
                mbd.MeasureID = m.MeasureID;
                mbd.NotSaved = true;
                //mbd.Insert();//Добавляется запись в таблицу OnDemandMedicine

                //Шаг 4. Подсчитывается требуемое количество медикаментов по назначенным услугам без учета удаленных,
                // как самих услуг, так и документов по которым они были выписаны
                OutputMedicamentList mos = new OutputMedicamentList();
                mos.Load(f, false, true, ServiceID, m.MedicamentID);
                int SummMedService = 0;//Сумма медикаментов назначенных в расход по услугам
                foreach (MedicamentOutput mo in mos)
                {
                    SummMedService += mo.CountOutput;
                }
                //// требуемое для оказания единичной услуги согласно прейскуранта
                if (p.trace == true) MessageBox.Show("Подсчитатна сумма медикамента " + m.MedicamentN + " по назначенным услугам = " +
                                                       SummMedService.ToString(), "Информер");

                //Шаг 5. Подсчитывается количество медикаментов по ранее выписанным требованиям с ненулевым остатком 
                //для той же самой услуги без учета удаленных требований
                MedicamentByDemandList medbds = new MedicamentByDemandList();

                medbds.Load(ServiceID, m.MedicamentID, (int)DocTypes.DemandService, true, f);//Формируется коллекция медикаментов по требованиям с ненулевыми остатками.ЗЕФ, 07.08.2011
                int SummMedDemand = 0;
                //Подсчитывается сумма остатков медикаментов по требованиям
                foreach (OnDemandMedicine medbd in medbds)
                    SummMedDemand += medbd.CountResidue;
                if (p.trace == true) MessageBox.Show("Подсчитатна сумма медикамента " + m.MedicamentN + " с ненулевым остатком на основе уже выписаных требований = " +
                                                          SummMedDemand.ToString(), "Информер");

                //Шаг 6. Вычисляется сумма рекомендуемая для получения по требованию
                mbd.CountGet = SummMedService - SummMedDemand;
                if (mbd.CountGet < 0)
                {//Рекомендуемое количество может оказаться меньше нуля, если выписанного по требованию количества медикаментов достаточно
                    //для закрытия всех назначенных услуг
                    if (!pressmsg)
                    {
                        MessageBox.Show("Количество медикамента по уже выписанным требованиям : " + m.MedicamentN + " - "
                                    + SummMedDemand.ToString() + " достаточно на остатках,\r\n" +
                                     "чтобы оказать выбранную услугу в количестве: " + SummMedService.ToString() + ".\r\n" +
                                     "Выберите другую услугу или укажите требуемое количество медикамента для завершения оформления требования.",
                                     "Выписка медикаментов для требования", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    mbd.CountGet = 0;
                }
                mbd.CountResidue = mbd.CountGet;
                if (p.trace == true) MessageBox.Show("Подсчитатно рекомендуемое количество медикамента " + m.MedicamentN + " для требования - " +
                                                       mbd.CountGet.ToString(), "Информер");


                //Шаг 9.Схранение собранных данных о медикаменте в базу данных
                //mbd.Update();

                //Шаг 8.Добавляется медикамент в коллекцию MedicamentByDemandList
                //mbd.Load();//Загружаются все данные экземпляра OnDemandMedicine перед добавлением в коллекцию
                mbds.Add(mbd);//Добавление медикамента в коллецию
            }
            return mbds;
        }
    }
  
}
