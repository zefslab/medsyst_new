﻿using System.Windows.Forms;

namespace Medsyst.Class
{
    ///<summary>Категория услуг</summary>
    public class ServiceCategory : DBO, ITreeItem
    {
        [db("ServiceCategory")]
        [pk]
        public int ServiceCategoryID { get; set; }

        /// <summary>Наименование категории услуги</summary>
        [db("ServiceCategory")]
        public string CategoryN { get; set; }

        /// <summary>Код родительской категории</summary>
        [db("ServiceCategory")]
        public int ParentID { get; set; }
        
        /// <summary> Порядок сортировки </summary>
        [db("ServiceCategory")]
        public int list_order { get; set; }

        /// <summary>Признак удаления записи</summary>
        [db("ServiceCategory")]
        public bool Deleted { get; set; }

        public int ID { get { return ServiceCategoryID; } set { ServiceCategoryID = value; } } //идентификатор элемента
        public string Text { get { return CategoryN; } set { CategoryN = value; } } //строковое представление элемента
        public bool visible { get; set; } //Признак отображения элемента в дереве
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве

        public ServiceCategory()
            : base()
        {
            CategoryN = "";
        }

    }
    
    ///<summary>Список категорий услуг
    ///Заплатин Е.Ф.
    ///07.07.2011</summary>
    public class ServiceCategoryList : DBOTree<ServiceCategory>
    {
        public override bool Load()
        {
            if (!ShowDeleted)//Проверяется интерфейс, а не поле showdeleted класса DBOList
                //Параметр установлен в  false, значит нужно загружать только элементы не отмеченные как удаленные
                dbWhere = "Deleted = 0";
            else
                dbWhere = "";
            return base.Load();
        }
        ///<summary>Загрузка категорий услуг, которые являются дочерними по отношению к указанной в качестве параметра и не отмечены как удаленные
        ///Заплатин Е.Ф.
        ///08.07.2011</summary>
        ///<param name="catID">Идентификатор родительской категории</param>
        public bool Load(int catID, bool _deleted)
        {
            dbWhere = "ParentID = " + catID.ToString();
            if (!_deleted)
                dbWhere += " AND Deleted = 0";
            return base.Load();
        }
        ///<summary>Проверка на наличие в категории вложенных категорий или услуг с учетом признака их удаления. Переопределенный метод.
        ///Заплатин Е.Ф.
        ///07.07.2011
        /// </summary>
        /// <param name="node">узел в дереве</param>
        /// <returns>true - если содержит вложения, false - если не содержит</returns>
        public override bool _checkAttachments(TreeNode node, bool deleted)
        {
            bool check = false;

            //Проверка на наличие вложенных категорий
            ServiceCategoryList scs = new ServiceCategoryList();
            scs.Load((int)node.Tag, deleted);//Загрузка категорий у которых в качестве родительской указан идентификатор передаваемой категории
            if (scs.Count > 0)//Если количество таких элементов больше нуля, то
                check = true;

            //Проверка на наличие вложенных услуг
            ServiceList ss = new ServiceList();
            ss.Load((int)node.Tag);//Загрузить все услуги у которых узел фигурирует в качестве категории Convert.ToInt32(node.Tag)
            if (ss.Count > 0)    //Если количество таких элементов больше нуля, то
                check = true;

            return check;
        }
    }
}
