﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;


namespace Medsyst.Class
{
    /// <summary>Класс: Категория медикамента
    /// </summary>
    public class MedicamentCategory : DBO, ITreeItem
    {
        [db("MedicamentCategory")]
        [pk]
        public int MedicamentCategoryID { get; set; }
        [db("MedicamentCategory")]
        public string CategoryN { get; set; }// Наименование категории медикамента
        [db("MedicamentCategory")]
        public int GroupID { get; set; } // Принадлежность к группе для формирования отчетам расхода медикаментов по группам
        [db("MedicamentCategory")]
        public int ParentID { get; set; } // Код родительской категории
        [db("MedicamentCategory")]
        public int list_order { get; set; } // Порядок сортировки
        [db("MedicamentCategory")]
        public bool Deleted { get; set; } // Признак того, что звпись удалена


        public int ID { get { return MedicamentCategoryID; } set { MedicamentCategoryID = value; } } //идентификатор элемента
        public string Text { get { return CategoryN; } set { CategoryN = value; } } //строковое представление элемента
        public bool visible { get; set; } //Признак отображения элемента в дереве
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве

        public MedicamentCategory()
            : base()
        {
            CategoryN = "";
            GroupID = 0;
            list_order = 0;
            Deleted = false;
        }
    }
    ///<summary>Коллекция:  категорий медикаментов
    ///</summary>
    public class MedicamentCategoryList : DBOTree<MedicamentCategory>
    {
        /// <summary>Загрузка категорий 
        /// Заплатин Е.Ф.
        /// 03.05.2011</summary>
        /// <returns></returns>
        public override bool Load()
        {
            dbOrder = "list_order, CategoryN";
            if (!ShowDeleted)//Проверяется интерфейс, а не поле showdeleted класса DBOList
                //Параметр установлен в  false, значит нужно загружать только элементы не отмеченные как удаленные
                dbWhere = "Deleted = 0";
            else
                dbWhere = "";
            return base.Load();
        }
        /// <summary>Загрузка категорий вложенных в указанную категорию с учетом признака их удаленности
        /// Заплатин Е.Ф.
        /// 08.07.2011</summary>
        /// <returns></returns>
        public bool Load(int catID, bool _deleted)
        {
            dbWhere = "ParentID = " + catID.ToString();
            if (!_deleted)
                dbWhere += " AND Deleted = 0";
            return base.Load();
        }
        /// <summary>Загрузка категорий относящихся к указанной группе
        /// Заплатин Е.Ф.
        /// 03.05.2011</summary>
        /// <returns></returns>
        public bool Load(int groupID)
        {
            dbOrder = "list_order, CategoryN";
            dbWhere = "GroupID = " + groupID.ToString();
            if (!showdeleted)
                //Параметр установлен в  false, значит нужно загружать только элементы не отмеченные как удаленные
                dbWhere += " AND Deleted = 0";
            return base.Load();
        }
        /// <summary>Отметка категорий принадлежащих к какой либо группе
        /// Заплатин Е.Ф.
        /// 13.05.2011
        /// </summary>
        /// <param name="group">допускается значение null. это означает, что нужно визуализировать в дереве 
        /// категории, не принадлежащие ни к одной из групп</param>
        public void CheckVisible(int? group)
        {
            if (group == null)
                //нужно визуализировать только те категории, которые не принадлежат ни к одной группе
                foreach (MedicamentCategory mc in this)
                    mc.visible = mc.GroupID == 0 ? true : false;
            else
                //визуализировать категории принадлежащие указанной гуппе
                foreach (MedicamentCategory mc in this)
                    mc.visible = mc.GroupID == group ? true : false;
        }

        /// <summary>Коллекция дополняется элементами на основе переданных узлов из визуального элемента дерева категорий
        /// Если потребуется сформировать новую коллекцию из добавляемых узлов, то предварительно нужно ее очитить методом Clear
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="checkitNodes">Узлы, как правило отмеченные чекитом в визуальном элементе TreeView</param>
        public void ConverFromNodes(TreeNodeCollection checkitNodes)
        {
            foreach (TreeNode chn in checkitNodes)
            {
                MedicamentCategory mc = new MedicamentCategory();
                mc.MedicamentCategoryID = (int)chn.Tag;
                mc.Load();
                Add(mc);
            }
        }

        ///<summary>Проверка на наличие в категории вложенных категорий или медикаментов. Переопределенный метод.
        ///Заплатин Е.Ф.
        ///07.07.2011
        /// </summary>
        /// <param name="node">узел в дереве</param>
        /// <returns>true - если содержит вложения, false - если не содержит</returns>
        public override bool _checkAttachments(TreeNode node, bool deleted)
        {
            bool check = false;

            //Проверка на наличие вложенных категорий
            MedicamentCategoryList mcs = new MedicamentCategoryList();
            mcs.Load((int)node.Tag, deleted);//Загрузка категорий у которых в качестве родительской указан идентификатор передаваемой категории
            if (mcs.Count > 0)//Если количество таких элементов больше нуля, то
                check = true;

            //Проверка на наличие вложенных услуг
            MedicamentList ms = new MedicamentList();
            ms.Load((int)node.Tag);//Загрузить все медикаменты у которых узел фигурирует в качестве категории Convert.ToInt32(node.Tag)
            if (ms.Count > 0)    //Если количество таких элементов больше нуля, то
                check = true;

            return check;
        }
    }
}