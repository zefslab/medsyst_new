﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;


namespace Medsyst.Class
{

    ///<summary>Класс: Расходуемый медикамент
    ///</summary>
    public class MedicamentOutput : MedicamentOnSclad
    {
        [pk]
        [db("MedicamentOutput")]
        public int MedicamenOutputID { get; set; }
        
        /// <summary>Вторичный ключ с измененным названием</summary>
        [fk("MedicamentOnSclad", "MedicamentOnScladID")]
        [db("MedicamentOutput", "MedicamentOnScladID")]
        public int MedicamentOnScladID_ { get; set; }
        
        [db("MedicamentOutput")]
        public int OutputDocumentID { get; set; }
        
        [db("MedicamentOutput")]
        public int CountOutput { get; set; }
        
        [db("MedicamentOutput")]
        public new decimal Price { get; set; }
        
        [db("MedicamentOutput")]
        public int EmployeeID { get; set; }
        
        /// <summary>Дата назначения медикамента</summary>
        [db("MedicamentOutput")]
        public DateTime MedicamentD { get; set; }
        
        /// <summary>Дата выдачи назначенного медикамента</summary>
        [db("MedicamentOutput")]
        public DateTime OutputD { get; set; }
        
        [db("MedicamentOutput")]
        public string CommentOutput { get; set; }
        
        /// <summary>Ссылка на назначенную услугу по которой был отпущен медикамент</summary>
        [db("MedicamentOutput")]
        public int ServiceOutputID { get; set; }
        
        /// <summary>Признак того, что некоторое количество медикаментов зарезервировано</summary>
        [db("MedicamentOutput")]
        public bool Reserved { get; set; }
        
        /// <summary>Признак противоположный резервированию, необходим для правильного отображения чекбоксов в форме frmScladOutput</summary>
        public bool UnReserved { get { return !Reserved; } set { Reserved = !value; } }
        
        /// <summary>Рецепт приема выписанного лекарствнного средства</summary>
        [db("MedicamentOutput")]
        public string Prescription { get; set; }
        
        /// <summary>Признак удаления расхода медикаментов</summary>
        [db("MedicamentOutput")]
        public bool DeletedOut { get; set; }
        
        /// <summary>Общая стоимость назначенного стредства</summary>
        public new decimal PriceSum { get { return CountOutput * Price; } }

        public MedicamentOutput() { }
        public MedicamentOutput(bool init)
            : base(init)
        {
            //Инициализация свойств класса
            MedicamenOutputID = MedicamentOnScladID_ = OutputDocumentID = EmployeeID = 0;
            Price = 0;
            CountOutput = 0;
            MedicamentD = new DateTime(1900, 1, 1);
            OutputD = new DateTime(1900, 1, 1);
            ServiceOutputID = 0;
            CommentOutput = "";
            Prescription = "";
            Reserved = true; //резервируется медикамент до момента выдачи
            DeletedOut = false;
        }
        /// <summary>Создание нового экземляра расходного медикамента не сохраненного в базе данных
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// </summary>
        /// <param name="mosID">идентификатор складского медикамента</param>
        /// <param name="docID">идентификатор расходного документа</param>
        public void New(int mosID, int docID)
        {
            OutputDocumentID = docID;//Медикамент связывается с расходным документом
            DemandMaterial d = new DemandMaterial(true);
            d.DocumentID = docID;
            d.Load();
            //OutputDocumentTypeID = d.DocTypeID;//Тип расходного документа. ЗЕФ. 10.08.2011
            
            MedicamentD = DateTime.Now;//Присваивается дата назначения медикамента в расход
            EmployeeID = Security.user.UserID;//Отмечается пользователь назначивший медикамент
            NotSaved = true;

            //base.Load();//TODO Реализовать загрузку всех свойств предков. Почему-то не прогружает. 
            //Если заработает, то нижележащие присвоения не понадобятся.

            MedicamentOnSclad mos = new MedicamentOnSclad(true);
            mos.MedicamentOnScladID = mosID;
            mos.Load();

            Price = mos.Price;
            MedicamentOnScladID = MedicamentOnScladID_ = mos.MedicamentOnScladID;//Указывается  идентификатор складского медикамента
            MedicamentInputID = MedicamentInputID_ = mos.MedicamentInputID;//Требуется для несохраненного в базе расходного медикамента.
            MedicamentID = MedicamentID_ = mos.MedicamentID;//Требуется для несохраненного в базе расходного медикамента.
            MedicamentN = mos.MedicamentN;//Требуется для несохраненного в базе расходного медикамента. После сохранения это свойство будет загружено из унаследованного медикамента
            CountOnSclad = mos.CountOnSclad; //Требуется для несохраненного в базе расходного медикамента. После сохранения это свойство будет загружено из унаследованного медикамента
            CountReserve = mos.CountReserve;//Требуется для несохраненного в базе расходного медикамента. После сохранения это свойство будет загружено из унаследованного медикамента
            MeasureID = mos.MeasureID;//Требуется для несохраненного в базе расходного медикамента. После сохранения это свойство будет загружено из унаследованного медикамента
        }
        /// <summary>Проверка медикамента на возможность его удаления
        /// Заплатин Е.Ф.
        /// 02.09.2011</summary>
        public bool TryDelete()
        {
            bool res = true;
            //Проверка 1. на предмет того, что медикамент уже выдан или еще стоит в резерве. 
            if (Reserved)
            {//Медикамент еще в резерве
                //Проверка 2. на предмет того количество резерва достаточно для его снятия
                if (CountOutput > CountReserve)
                {//Резерв мал по сравнению с откатываемым количеством медикамента. Вообще говоря, это нештатная ситуация. По идее такого быть не должно.
                    //Если такая ситуация возникает, значит возникают ошибки в операциях с количеством на складе или происходили сбои в системе во время выполнения таких операций
                    MessageBox.Show("Количество удаляемого медикамента: " + CountOutput.ToString() +
                                    " превышает количество: " + CountReserve.ToString() + " зарезервированного на складе.\n\r" +
                                    "Не возможно произвести удаление медикамента. Обратитесь за помощью к администратору системы Медсист.",
                                    "Отказ в удалении",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    res = res && false;
                }
            }
            else
            {//медикамент уже реализован, т.е снят с резерва. В этом случае сделать отказ в удалении.
                MessageBox.Show("Медикамент уже выдан пациенту или израсходован и его невозможно удалить.", "Отказ в удалении",
                                 MessageBoxButtons.OK, MessageBoxIcon.Stop);
                res = res && false;
            }

            return res;
        }
        /// <summary>Удаление назначенного медикамента. Прежде чем удалить расход производится восстановление резерва на складе
        /// Заплатин Е.Ф.
        /// 02.09.2011</summary>
        public bool Delete(CommandDirective directive)
        {
            //Проверка на предмет того, что медикамент уже выдан или еще стоит в резерве. 
            //TODO пока можно оставить эту проверку, но вообще нужно пользоваться для этих целей методом TryDelete() см. выше
            if (!Reserved)
            { //медикамент уже реализован. В этом случае сделать отказ в удалении.
                MessageBox.Show("Медикамент ID: " + MedicamenOutputID.ToString() + " " + MedicamentN + " уже выдан пациенту или израсходован и его невозможно удалить.", "Отказ в удалении",
                                 MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            else
            {// зарезрвированно, но не выдано пациенту

                if (directive == CommandDirective.DeletePermanently)
                {//Удалить полностью медикамент из базы
                    return base.Delete("MedicamentOutput");//Удаление записи назначенного медикамента.
                }
                else
                {//Устаннивается признак удаления с предварительным снятием резерва со склада
                    CountReserve -= CountOutput;//Снятие с резрва со склада
                    MedicamentOnScladID_ = MedicamentOnScladID;//Предается складской идентификатор удаляемого медикамента вторичноу ключу
                    Update("MedicamentOnSclad");//Обновление сведений о медикаменте на складе

                    DeletedOut = true;
                    return Update(false);
                }
            }
        }
        /// <summary>Удаление заданного количества медикаментов из расхода и возврашение на склад. 
        /// Вместе с удалением из расхода производится восстановление резерва на складе.
        /// Этот метод необходим когда изменяется количество уже назначенной услуги
        /// Автор: Заплатин Е.Ф.
        /// Дата: 24.12.2010</summary>
        /// <param name="count">количество удаляемых медикаментов из расхода</param>
        /// <returns></returns>
        public bool Delete(ref int countDelete)
        {
            if (CountOutput <= countDelete)
            {//Количества текущего расхода не хватает
                CountReserve -= CountOutput;//Снятие резрва со склада из расхода
                base.Update("MedicamentOnSclad");//Обновление сведений о медикаменте на складе
                countDelete -= CountOutput; //уменьшение удаляемого количества на величину расхода
                return base.Delete("MedicamentOutput");//Удаление записи назначенного в расход медикамента.
            }
            else
            {//Количества текущего расхода достаточно, чтобы вычесть из него удаляемые медикаменты
                CountReserve -= countDelete;//Снятие резрва со склада из  удаляемого количества
                base.Update("MedicamentOnSclad");//Обновление сведений о медикаменте на складе. Возможно этот вызов не нужен
                CountOutput -= countDelete; //Расход уменьшается на величину удаляемых медикаментов
                countDelete = 0; //Все удаляемые медикаменты вычтены из расхода
                return Update(false);//Обновление сведений о медикаменте в расходе
            }
        }
        public new bool Insert()
        {
            MedicamentOnScladID_ = MedicamentOnScladID;
            // если тип докумнта не определен, то ставим как историю болезни (для совместимости со старой версией)
            //if (OutputDocumentTypeID == 0) OutputDocumentTypeID = 1;
            return Insert("MedicamentOutput");
        }
        /// <summary>Обновление позволяющее вносить изменения как только в текущий класс, так и во все классы иерархии
        /// Текущий релиз метода ограничен и делает обновление лишь двух классов в двух таблицах. Для настоящего случая этого достаточно.
        /// Для реализации полнофункциональной версии нужно переопределить Метод Update с параметром Full
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// </summary>
        /// <param name="full">true - вносятся изменения во все классы иерархии, false -  изменения вносятся только в текущий класс</param>
        /// <returns></returns>
        public bool Update(bool full)
        {
            bool res = true;
            MedicamentOnScladID_ = MedicamentOnScladID;
            if (full)
            {
                res = res && Update("MedicamentOutput");
                res = res && Update("MedicamentOnSclad");
                //res = res && base.Update(full);//Вызов родительского метода с передачей ему параметра
            }
            else
            {
                res = res && Update("MedicamentOutput");
            }
            return res;
        }
        /// <summary>Назначение некоторого количества медикамента в расход и корректировка резерва на складе
        /// Заплатин Е.Ф.
        /// 31.08.2011</summary>
        /// <param name="count"> старое количество медикаментов в расходе до назначения нового</param>
        public new bool Output(int oldCount)
        {
            bool res = false;
            string head = "Проблемы со складом";//Заголовок окон сообщений о проблемах со складом
            if (!Reserved)
            {//Медикамент уже выдан, т.е. снят с резерва и поэтому его количество изменить невозможно 
                CountOutput = oldCount; //Восстанавливается прежнее количество назначения
                MessageBox.Show("У выданного медикамента невозможно изменить количество", "Отказ в изменении количества", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {//Медикамент зарезервирован, а значит еще не выдан. Изменение количества возможно
                int delta = CountOutput - oldCount; //Разность между вновь назначеным и ранее установленным количеством 
                if (delta == 0)
                {//Не изменилось количество, а значит ни каких действий не предпринимается
                }
                // --------------------------------------------------------------------------------------------
                // ------ Условие 1. Не должно выбираться медикамента больше, чем имеется на остатках ---------
                // --------------------------------------------------------------------------------------------
                else if (delta > 0 && delta > (CountDivide)) // Разность положительная но остатка на складе недостаточно для постановки в резерв
                // TODO Не верное сравнение. Сравнивать нужно не с medOnSclad.CountDivide, а со всеми приходами этого медикамента
                {//Недостаточно запрошенного количества медикаментов на складе, чтобы зарезервировать, 
                    //поэтому оно должно быть уменьшено
                    if (CountDivide == 0)
                    {//Запрашиваемого медикамента не осталось на складе
                        MessageBox.Show("Медикамента - " + MedicamentN + " на складе не осталось",
                            head, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        CountOutput = oldCount; //Количество назначаемых в расход медикаментов остается прежним
                    }
                    else if (CountDivide > 0)
                    {//На складе есе же есть некоторое, но недостаточное количество медикаментов. Поэтому требуется корректировка вновь введенного количества
                        MessageBox.Show("Количество остатков медикамента - " + MedicamentN +
                            " на складе: " + CountDivide + ".\r\n Этого объема не достаточно для назначения в необходимом количестве." +
                            "\r\n Вновь введенное значение будет скорректировано на допустимо возможное.",
                            head, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        CountOutput = oldCount + CountDivide;//Количество добавляемых в расход медикаментов увеличивается лишь на величину остатка на складе
                        res = true;
                        Modified = true;
                    }
                    else
                    {//Это служебное условие требуется для контроля того, чтобы выявить остаток меньший нуля, чего в принцыпе быть не должно
                        MessageBox.Show("Остаток медикамента - " + MedicamentN + " на складе: " + CountReserve.ToString() + " имеет отрицательное значение: " + CountDivide.ToString()
                                        + ".\r\n Такое значение недопустимо. Сообщите об этом администратору системы Медсист",
                                        head, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (delta < 0 && -delta > CountReserve)//Разность меньше нуля, а значит произошло уменьшние нового количества по отношению к старому значению 
                {// --------------------------------------------------------------------------------------------------------
                    // ------- Условие 2. Нужно проверить чтобы при снятии с резерва резерв не получился отрицательным --------
                    // --------------------------------------------------------------------------------------------------------
                    //Уменьшение резерва в таком объеме недопустимо т.к. приводит к установке отрицательного его значения
                    //поэтому нужно скорректировать новое количество на корректно-допустимое
                    MessageBox.Show("Количество резерва медикамента - " + MedicamentN +
                                     " на складе: " + CountReserve.ToString() + " не может быть уменьшено на величину: " + (-delta).ToString()
                                     + ".\r\n Поэтому новое количество будет скорректировано на допустимо возможное значение.",
                                     head, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    CountOutput = oldCount - CountReserve;//В этом случае уменьшение возможно только на величину резерва
                    res = true;
                    Modified = true;
                }
                else
                {//На складе достаточно остатков, чтобы списать в резерв
                    //Поэтому вновь введеное количество остается актуальным 
                    res = true;
                    Modified = true;
                }
                //Корректировка и сохранение измененного резерва на складе непосредственно в базе данных 
                //производится только при намеренном сохранении внесенных изменений в требование


                //Обновление командой Update() здесь не производится, поэтому измения в базе данных нужно производить отдельно, при подаче пользователем явной директивы сохранения
                //Если такой директивы не поступит то склад не изменит своего состояния
            }

            return res;
        }
        /// <summary>Списание медикамента: снятие резерва со склада и с расхода и уменьшение остатка по требованиям
        /// Выполняется так же обратная операция по возврату медикамента, восстановлению резервов и увеличения остатка по требованиям
        /// Заплатин Е.Ф.
        /// 20.08.2011
        /// 14.09.2011</summary>
        /// <param name="mbds">коллекция медикаментов по требованиям из которых будут списываться остатки</param>
        public void Unreservate(MedicamentByDemandList mbds)
        {
            Demand d = new Demand(true);
            Finance f;//TODO разобраться почему не используется эта переменная. Возможно потому, что  медикаменты по требованию относящиеся 
            //к нужному источнику финансирования уже загружены на предыдущем шаге и переданы в качестве параметра
            MedicamentOnSclad mos = new MedicamentOnSclad();
            if (IsBudget) f = Finance.Budget; else f = Finance.ExtraBudget;//Определяется источник финансирования расходного медикамента

            if (Reserved)
            {//Медикаменты находятся в резерве, поэтому требуется снятие медикамента с резерва

                //Шаг 1.1. Снятие медикамента с резерва в расходе
                Reserved = false;
                OutputD = DateTime.Now;

                //Шаг 1.2. Снятие резерва с медикаментов на складе    
                mos.MedicamentOnScladID = MedicamentOnScladID;
                mos.Load();
                mos.Output(CountOutput); //В данном случае не случайно использован экpемпляр класса медикамента на складе с загрузкой сведений из базы даннных, 
                //для того, чтобы иметь самые актуальные сведения о состоянии склада. В противном случае если использовать обращение посредством  ((MedicamentOnSclad)this).Output(CountOutput)
                //последние изменения на складе становятся недоступными.

                //Шаг 1.3. Уменьшение остатка медикамента выписанного по требованию
                int co = CountOutput;
                foreach (OnDemandMedicine mbd in mbds)
                {
                    d.DocumentID = mbd.DemandID;
                    d.Load();//Загружается требование для изменения суммы 

                    if (co <= mbd.CountResidue)
                    {//В требовании достаточно оставшегося количества медикамента для списания расхода
                        mbd.CountResidue -= co;
                        d.SummMedicament += co * Price; //Увеличение суммы средств на отпущенные медикаменты  по требованию
                        mbd.Update();
                        d.Update();//Обновление изменений в требовании
                        if (p.trace == true) MessageBox.Show("Уменьшен остаток: " + co.ToString() + " шт. медикамента: " + MedicamentN + " в требовании №" + d.Nbr.ToString() + ".",
                                "Корректировка кол-ва медикаментов в требовании",
                                MessageBoxButtons.OK);

                        break; //весь расход списан, цикл прерывается
                    }
                    else
                    {//В требовании не хватает оставшегося количества медикамента для списания расхода
                        co -= mbd.CountResidue;//Расход уменьшается на величину остатка в требовании
                        if (p.trace == true) MessageBox.Show("Уменьшен остаток: " + mbd.CountResidue.ToString() + " шт. медикамента: " + MedicamentN + " в требовании №" + d.Nbr.ToString() + ". " +
                                                            "Остатка медикаманта по текушему требованию недостаточно.",
                                                            "Корректировка кол-ва медикаментов в требовании",
                                                            MessageBoxButtons.OK);
                        d.SummMedicament += mbd.CountResidue * Price; //Увеличение суммы средств на отпущенные медикаменты  по требованию
                        mbd.CountResidue = 0;
                        mbd.Update();
                        d.Update();//Обновление изменений в требовании

                        //Расход списан не полностью, поэтому цикл продолжается
                    }
                }
            }
            else
            {//Медикамент нужно вернуть в резерв
                //Шаг 2.1. Ппостановка медикаментов в резерв в расходе
                Reserved = true;
                OutputD = new DateTime(1900, 1, 1);

                //Шаг 2.2. Постановка резерва медикаментов на склад    
                //((MedicamentOnSclad)this).Output(-CountOutput);
                mos.MedicamentOnScladID = MedicamentOnScladID;
                mos.Load();
                mos.Output(-CountOutput); //medOnSclad. (раньше был вызов из объекта MedicamentOnSclad. ЗЕФ. 05.09.2011)

                //Шаг 2.3. Увеличение остатка медикамента выписанного по требованию
                int co = CountOutput;//Вспомогательная переменная для прохождения цикла и уменьшения расходного количества
                foreach (OnDemandMedicine mbd in mbds)
                {
                    d.DocumentID = mbd.DemandID;
                    d.Load();//Загружается требование для изменения суммы 

                    if (co <= (mbd.CountGet - mbd.CountResidue))
                    {//В требовании разность между выданным количеством и остатком позволяет вернуть расход в требование
                        mbd.CountResidue += co;
                        d.SummMedicament -= co * Price; //Уменьшение суммы средств на отпущенные медикаменты  по требованию
                        mbd.Update();//Обновление медикамента по требованию
                        d.Update();//Обновление изменений в требовании
                        if (p.trace == true) MessageBox.Show("Увеличен остаток: " + co.ToString() + " шт. медикамента: " + MedicamentN + " в требовании №" + d.Nbr.ToString() + ".",
                                                            "Корректировка кол-ва медикаментов в требовании",
                                                            MessageBoxButtons.OK);
                        break; //весь расход списан, цикл прерывается
                    }
                    else
                    {//В требовании не хватает разности между выданным количеством и остатком для возврата расхода
                        co -= (mbd.CountGet - mbd.CountResidue);//Расход уменьшается на величину разности выданного количества и остатка в требовании
                        if (p.trace == true) MessageBox.Show("Увеличен остаток: " + (mbd.CountGet - mbd.CountResidue).ToString() + " шт. медикамента: " + MedicamentN + " в требовании №" + d.Nbr.ToString() + ". " +
                                                           "Остатка медикаманта по текушему требованию недостаточно.",
                                                           "Корректировка кол-ва медикаментов в требовании",
                                                           MessageBoxButtons.OK);
                        d.SummMedicament -= (mbd.CountGet - mbd.CountResidue) * Price; //Уменьшение суммы средств на отпущенные медикаменты  по требованию
                        mbd.CountResidue = mbd.CountGet;//Остаток становится равным выданному количеству
                        mbd.Update();//Обновление медикамента по требованию
                        d.Update();//Обновление изменений в требовании
                        //Расход восстановлен не полностью, поэтому цикл продолжается
                    }
                }
            }
            if (Update(false))//Обновление расхода. Обновляется флаг резервирования.
            {
                mos.Update();//Обновление склада. Обновляется количество резерва и количества на складе.
            }
        }
        /// <summary>Списание расходных материалов: снятие резерва со склада и с расхода
        /// Заплатин Е.Ф.
        /// 20.08.2011</summary>        
        public void UnreservateMaterial()
        {
            MedicamentOnSclad mos = new MedicamentOnSclad();
            if (Reserved)
            {//Медикаменты находятся в резерве, поэтому требуется снятие медикамента с резерва

                //Шаг 1.1. Снятие медикамента с резерва в расходе
                Reserved = false;
                OutputD = DateTime.Now;

                //Шаг 1.2. Снятие резерва с медикамента на складе    
                mos.MedicamentOnScladID = MedicamentOnScladID;
                mos.Load();
                mos.Output(CountOutput);

                //Шаг 1.4 Внесение сделанных изменений в базу данных
                Update("MedicamentOutput");//Обновление расхода. Обновляется флаг резервирования.
                mos.Update();//Обновление склада. Обновляется количество резерва и количества на складе.
            }
            else
                MessageBox.Show("Невозможно вернуть в резерв материалы. Это действие непредусмотрено. Обратитесь к администратору системы за разъяснениями",
                                "Недопустимая операция", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

    }
    /// <summary>Коллекция: расходуемых медикаментов
    /// </summary>
    public class OutputMedicamentList : DBOList<MedicamentOutput>
    {
        /// <summary>Загрузка списка медикаментов назначеных в расходном документе</summary>
        /// TODO Объединить этот метод с последующим методом оставив один универсальный 07.09.2011
        /// <used_by>frmScladOutput.gridOutDocuments_SelectionChanged</used_by>        
        /// <param name="doc_id">идентификатор расходного документа</param>
        public bool Load(int doc_id)
        {
            dbWhere = "MedicamentOutput.DeletedOut = 0 AND ";//Кроме тех, на которые поставлен признак удаленного

            //Похорошему нужно перенести этот критерий в родительский метод DBOList, а в текущий метод добавить 
            //параметр, определяющий показывать или нет удаленные записи. Это в последствии пригодится при просмотре 
            // удаленных записей с административными правами            
            dbWhere += "MedicamentOutput.ServiceOutputID = 0 AND ";//Кроме тех, которые назначены по услуге
            dbWhere += " OutputDocumentID = " + doc_id.ToString(); // Отбор по идентификатору расходного документа

            return base.Load();
        }
        /// <summary>Загрузка списка медикаментов назначеных в расходном документе с учетом признака удаления</summary>
        /// Заплатин Е.Ф.
        /// 07.09.2011
        /// <param name="doc_id">идентификатор расходного документа</param>
        /// <param name="loadDeleted">признак указывает загружать или нет записи отмеченные как удаленные </param>
        public bool Load(CommandDirective loadDeleted, int doc_id)
        {
            dbWhere = ((loadDeleted == CommandDirective.LoadDeleted) ? "" : "MedicamentOutput.DeletedOut = 0 AND "); //Загружать или нет записи отмеченные как удаленные
            //Похорошему нужно перенести этот критерий в родительский метод DBOList, а в текущий метод добавить 
            //параметр, определяющий показывать или нет удаленные записи. Это в последствии пригодится при просмотре 
            // удаленных записей с административными правами            
            dbWhere += "  MedicamentOutput.ServiceOutputID = 0 ";//Кроме тех, которые назначены по услуге
            dbWhere += " AND OutputDocumentID = " + doc_id.ToString(); // Отбор по идентификатору расходного документа

            return base.Load();
        }
        /// <summary>Загрузка списка медикаментов назначеных в расходном документе с учетом критерия по выдаче медикамента</summary>
        /// <used_by>frmScladOutput.gridOutDocuments_SelectionChanged</used_by> 
        /// Заплатин Е.Ф.
        /// 14.04.2011
        /// TODO объединить с предыдущей реализацией метода
        /// <param name="doc_id">идентификатор расходного документа</param>
        /// <param name="reserved">признак того что медикамент зарезервирован</param>
        public bool Load(int doc_id, bool reserved)
        {
            dbWhere = "MedicamentOutput.DeletedOut = 0 AND ";//Кроме тех, на которые поставлен признак удаленного

            //Похорошему нужно перенести этот критерий в родительский метод DBOList, а в текущий метод добавить 
            //параметр, определяющий показывать или нет удаленные записи. Это в последствии пригодится при просмотре 
            // удаленных записей с административными правами            
            dbWhere += " MedicamentOutput.ServiceOutputID = 0 AND ";//Кроме тех, которые назначены по услуге
            dbWhere += " OutputDocumentID = " + doc_id.ToString() + " AND "; // Отбор по идентификатору расходного документа
            dbWhere += " MedicamentOutput.Reserved = " + (reserved ? "1 " : "0 "); // Отбор по признаку резерва медикаментов 
            return base.Load();
        }
        /// <summary>Загрузка списка медикаментов назначеных в расход по идентификатору назначенной и прейскурантной услуг</summary>
        /// <used_by>ServiceOutput.Implement</used_by>
        /// <param name="loadDeleted">признак указывает загружать или нет записи отмеченные как удаленные </param>
        /// <param name="serviceOutput_id">идентификатор назначенной услуги. </param>
        /// <param name="medicament_id">идентификатор прейскурантного медикамента. Параметр сделан опциональным. </param>
        public bool Load(bool loadDeleted, int serviceOutput_id, params int[] medicament_id)
        {
            //TODO внесены изменения в запрос добавкой критерия по удаленной услуге и пользовательского запроса. Нужно протестировать все вызова.ЗЕФ 14.09.2011
            dbSelectUser = "SELECT * FROM Medicament " +
               "RIGHT JOIN MedicamentInput ON MedicamentInput.MedicamentID = Medicament.MedicamentID " +
               "RIGHT JOIN MedicamentOnSclad ON MedicamentOnSclad.MedicamentInputID = MedicamentInput.MedicamentInputID " +
               "RIGHT JOIN MedicamentOutput ON MedicamentOutput.MedicamentOnScladID = MedicamentOnSclad.MedicamentOnScladID " +
               "RIGHT JOIN ServiceOutput as so ON so.ServiceOutputID = MedicamentOutput.ServiceOutputID";
            dbWhere = (loadDeleted ? "" : "MedicamentOutput.DeletedOut = 0 AND so.Deleted = 0  ") + //Загружать или нет записи отмеченные как удаленные исключая удаленные услуги
                        " AND MedicamentOutput.ServiceOutputID = " + serviceOutput_id.ToString() + //Отбор по идентификатору назначенной услуги
                        (medicament_id.Length != 0 ? " AND Medicament.MedicamentID = " + medicament_id[0].ToString() : ""); //Отбор по идентификатору медикамента из прейскуранта
            return base.Load();
        }
        /// <summary>Загрузка списка медикаментов назначеных в расход по идентификатору прейскурантной услуги с учетом
        /// источника финасирования</summary>
        /// <param name="loadDeleted">признак указывает загружать или нет записи отмеченные как удаленные </param>
        /// <param name="loadReserved">загружать зарезервированнвые или незарезервированные медикаменты в расходе </param>
        /// <param name="service_id">идентификатор прейскурантной услуги. </param>
        /// <param name="medicament_id">идентификатор прейскурантного медикамента. Параметр сделан опциональным. </param>
        public bool Load(Finance f, bool loadDeleted, bool loadReserved, int service_id, int medicament_id)
        {
            dbSelectUser = "SELECT * FROM Medicament " +
                           "RIGHT JOIN MedicamentInput ON MedicamentInput.MedicamentID = Medicament.MedicamentID " +
                           "RIGHT JOIN MedicamentOnSclad ON MedicamentOnSclad.MedicamentInputID = MedicamentInput.MedicamentInputID " +
                           "RIGHT JOIN MedicamentOutput ON MedicamentOutput.MedicamentOnScladID = MedicamentOnSclad.MedicamentOnScladID " +
                           "RIGHT JOIN ServiceOutput as so ON so.ServiceOutputID = MedicamentOutput.ServiceOutputID";
            dbWhere = (loadDeleted ? "" : "MedicamentOutput.DeletedOut = 0 AND so.Deleted = 0 "); //Загружать или нет записи отмеченные как удаленные
            dbWhere += " AND Medicament.MedicamentID = " + medicament_id.ToString();
            dbWhere += " AND so.ServiceID = " + service_id.ToString(); //Отбор по идентификатору назначенной услуги
            dbWhere += " AND MedicamentOutput.Reserved = " + (loadReserved ? "1 " : "0 "); // Отбор по признаку резерва медикаментов 
            if (f != Finance.Non)
            {//Источник финансирования имеет значение 
                dbWhere += " AND MedicamentInput.IsBudget = " + ((int)f).ToString();//Критерий по принадлежности к источнику финансирования
            }
            return base.Load();
        }
        /// <summary>Загрузка расходного медикамента, соответсвующего медикаменту выписанному по требованию материалов
        /// Заплатин Е.Ф.
        /// 05.09.2011
        /// </summary>
        /// <param name="docID">идентификатор расходного документа</param>
        /// <param name="mID">идентификатор каталожного медикамента</param>
        /// <returns></returns>
        public bool Load(int docID, int mID)
        {
            dbWhere = "MedicamentOutput.DeletedOut = 0 AND";//Кроме тех, на которые поставлен признак удаленного
            dbWhere += " MedicamentOutput.ServiceOutputID = 0 AND";//Кроме тех, которые назначены по услуге
            dbWhere += " MedicamentOutput.OutputDocumentID = " + docID.ToString() + " AND"; // Отбор по идентификатору расходного документа
            dbWhere += " Medicament.MedicamentID = " + mID.ToString(); // Отбор по идентификатору медикамента

            return Load();
        }
        /// <summary>Загрузка всех назначенных в расход по всем документам определенного типа, 
        /// но еще не выданных пациентам медикаментов с учетом финасового источника</summary>
        /// Заплатин Е.Ф.
        /// 08.09.2011
        /// <param name="f">источник финансирования</param>
        /// <param name="loadDeleted">указывает удалять или нет удаленные медикаменты</param>
        /// <param name="countIsNotZero">загружать или нет медикаменты с нулевым назначением</param>
        /// <param name="dts">типы расходных документов по которым выбираются медикаменты</param>
        public bool Load(Finance f, CommandDirective loadDeleted, bool countIsNotZero, List<int> dts)
        {
            dbSelectUser =
            " SELECT * FROM Medicament  " +
            " RIGHT JOIN MedicamentInput ON MedicamentInput.MedicamentID = Medicament.MedicamentID" +
            " RIGHT JOIN MedicamentOnSclad ON MedicamentOnSclad.MedicamentInputID = MedicamentInput.MedicamentInputID" +
            " RIGHT JOIN MedicamentOutput ON MedicamentOutput.MedicamentOnScladID = MedicamentOnSclad.MedicamentOnScladID" +
            " RIGHT JOIN Document as d ON MedicamentOutput.OutputDocumentID = d.DocumentID";//Пользовательский запрос пришлось делать из-за этой связки

            dbWhere += "MedicamentOutput.ServiceOutputID = 0";//Кроме тех, которые назначены по услуге
            dbWhere += " AND MedicamentOutput.Reserved = 1";//Поставленные в резерв, но еще не выданные пациентам
            dbWhere += loadDeleted == CommandDirective.NotLoadDeleted ? " AND MedicamentOutput.DeletedOut = 0" : "";//Условие для загрузки  медикаментов отмеченых как удаленные
            dbWhere += countIsNotZero ? " AND MedicamentOutput.CountOutput > 0" : "";//Количество, назначенное в расход должно быть больше нуля
            if (f != Finance.Non)
            {//Источник финансирования имеет значение 
                dbWhere += " AND MedicamentInput.IsBudget = " + ((int)f).ToString();//Критерий по принадлежности к источнику финансирования
            }
            foreach (int dt in dts)
                dbWhere += " AND d.DocTypeID = " + dt.ToString();//Тип расходного документа. Пользовательский запрос пришлось делать из-за это критерия
            dbOrder = "Medicament.MedicamentID";
            return base.Load();
        }
        /// <summary>Удаляются все элементы коллекции
        /// Заплатин Е.Ф.
        /// 07.09.2011
        /// </summary>
        /// <returns>true - если все медикаменты удадились успешно, falce - если хотя бы один не удалился</returns>
        public bool Delete(CommandDirective directive)
        {
            if (!this.Any()) return true; //Нет элементов в коллекции отмеченых как удаленные

            //TODO если хоть один медикамент отказался быть удаленным, то производить откат всей групповой операции
            bool res = true;
            
            foreach (MedicamentOutput mo in this)
            {//Удаляется поочередно каждый элемент коллекции
                if (mo.NotSaved)
                {//медикамент коллекции небыл ранее сохранен в базе, поэтому достаточно его исключить лишь из коллекции
                    //не требуется удаление из базы 
                }
                else
                {
                    res = res && mo.Delete(directive);
                }
            }
            if (res) this.Clear();//Очистка коллекции после успешного удаления всех ее элементов
            
            return res;
        }
        /// <summary>
        /// Удаляется заданное количество медикаментов из всей коллекции
        /// </summary>
        /// <param name="count"> количество медикаментов, которое нужно удалить из резерва коллекции</param>
        public void Delete(int count)
        {
            foreach (MedicamentOutput mo in this)
            {//Перебираются поочередно элементы коллекции пока не удалится из расхода требуемое количество.
                mo.Delete(ref count);//Удаляется передаваемое по ссылке количество медикамента
                if (count == 0) break; //Все медикаменты удалены из расхода
                //Если количество не равно нулю значит переходим к следующему расходу и в нем также снимается оставшиеся медикаменты
            }
        }
        /// <summary>Группировка медикаментов по MedicamentID с суммированием количества назначенного в расход.
        /// Временный метод пока не реализована группировка в методе Load
        /// Заплатин Е.Ф.
        /// 13.08.2011
        /// </summary>
        public OutputMedicamentList GroupByMedicament()
        {
            MedicamentOutput mo = new MedicamentOutput(true);//Инициируется новый медикамент по требованию
            OutputMedicamentList mos = new OutputMedicamentList();
            foreach (MedicamentOutput m in this)
            {//формируется коллекция медикаментов, сгруппированных по идентификатору MedicamentID
                //TODO В последствии можно реализовать эту группировку через ручной запрос в методе Load
                if (mo.MedicamentID == m.MedicamentID)
                {//Нашелся идентичный медикамент. Поэтому нужно увеличить количество медикаментов в требовании
                    mo.CountOutput = mo.CountOutput + m.CountOutput;//Увеличивается количество расходных медикаментов
                }
                else
                {//Нашелся неидентичный медикамент в списке расходуемых, поэтому нужно добавить 
                    mos.Add(m);
                    mo = m;
                }
            }//На выходе из цикла имеем сгруппированную  колекцию медикаментов с суммированным расходным количеством
            return mos;
        }
        /// <summary>Сохраняются в базе все экземпляры, отмеченные как не сохраненные
        /// Заплатин Е.Ф.
        /// 01.09.2011
        /// </summary>
        public void SaveNotSaved()
        {
            foreach (MedicamentOutput mo in this)
            {
                if (mo.NotSaved)
                {//Медикамент отмечен как не сохраненный
                    mo.OutputD = DateTime.Now;
                    mo.MedicamentD = DateTime.Now;
                    mo.Insert("MedicamentOutput");//Добавляется в базу запись расходного медикамента
                    //Обновляется информация по резерву складского медикамента
                    MedicamentOnSclad mos = new MedicamentOnSclad(true);
                    mos.MedicamentOnScladID = mo.MedicamentOnScladID;
                    mos.Load();
                    mos.Resrved(mo.CountOutput);//Резервируется количество целиком, т.к. сохраняется вновь добавленный медикамент и прежнее значение у него всегда - 0
                    mos.Update();
                    mo.NotSaved = false;
                    mo.Modified = false;
                }
            }
        }
        /// <summary>Обновляются в базе все экземпляры, отмеченные как измененные
        /// Заплатин Е.Ф.
        /// 01.09.2011
        /// </summary>
        public void UpdateModified()
        {
            foreach (MedicamentOutput mo in this)
            {
                if (mo.Modified)
                {//Медикамент отмечен как не сохраненный
                    //Требуется загрузка из базы старых данных о количестве расхода
                    MedicamentOutput old_mo = new MedicamentOutput();
                    old_mo.MedicamenOutputID = mo.MedicamenOutputID;
                    old_mo.Load();
                    mo.Resrved(mo.CountOutput - old_mo.CountOutput);//Изменение складского резерва
                    mo.Update(true);//Обновляется информация об экземпляре в базе
                    mo.Modified = false;
                }
            }
        }
        /// <summary>Списание всех медикаментов коллекции: снятие резерва со склада и с расхода
        /// Заплатин Е.Ф.
        /// 05.09.2011</summary>
        public void UnreservateMaterial()
        {
            foreach (MedicamentOutput mo in this)
            {
                mo.UnreservateMaterial();//TODO попробовать разбить этот метод на более элементарные действия
            }
        }
        /// <summary>Cнятие резерва со склада
        /// Заплатин Е.Ф.
        /// 07.09.2011</summary>
        public void UnreservateOnSclad()
        {
            foreach (MedicamentOutput mo in this)
            {
                //Знак минус обозначает снятие резерва с медикамента со склада    
                ((MedicamentOnSclad)mo).Resrved(-mo.CountOutput);
                ((MedicamentOnSclad)mo).Update();//Обновляются  изменения внесенные на склад
            }
        }

    }

}
   
    
