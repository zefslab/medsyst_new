﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;


namespace Medsyst.Class
{
    ///<summary>Класс: Медикамент связанный с услугой
    ///</summary>
    public class MedicamentService : Medicament
    {
        ///<summary>Идентификатор связи услуги и медикамента</summary>
        [db("MedicamentService")]
        [pk]
        public int ServiceMedicamentID { get; set; }
        ///<summary>Внешний ключ на медикамент соотнесенный к услуге</summary>
        [fk("Medicament", "MedicamentID")]
        [db("MedicamentService", "MedicamentID")]
        public int MedicamentID_ { get; set; }
        ///<summary>Идентификатор услуги, которой соотнсится медикамент</summary>
        [db("MedicamentService")]
        public int ServiceID { get; set; }
        ///<summary>Количество медикаментов</summary>
        [db("MedicamentService")]
        public int Count { get; set; }
        [db("MedicamentService", Name = "Deleted")]
        public bool Deleted_ { get; set; } //ЗЕФ 11.07.2011
        ///<summary>Общая стоимость медикамента</summary>
        public decimal PriceSum { get { return Cost * Count; } }

        public MedicamentService() { }
        /// <summary>Инициализация экземпляра
        /// Заплатин Е.Ф.
        /// 08.01.2012</summary>
        public MedicamentService(bool init)
            : base(init)
        {
            ServiceID = ServiceMedicamentID = MedicamentID_ = 0;
            Count = 1;//По умолчанию добавляется к услуге медикамент с количеством = 1
            Deleted_ = false;
        }

        public new bool Insert()
        {
            MedicamentID_ = MedicamentID;
            if (Insert("MedicamentService"))
            {
                ServiceMedicamentID = Identity("MedicamentService");
                return true;
            }
            return false;
        }
        /// <summary>Условное удаление медикамента связанного с услугой
        /// Заплатин Е.Ф.
        /// 11.07.2011
        /// </summary>
        public bool Delete(CommandDirective permanently)
        {
            if (permanently == CommandDirective.DeletePermanently)
                return base.Delete("MedicamentService");
            else
            {
                Deleted_ = true;
                return Update();
            }
        }

        public new bool Update()
        {
            MedicamentID_ = MedicamentID;
            return base.Update("MedicamentService");
        }
    }
    ///<summary>Коллекция: медикаментов связанных с услугой
    ///</summary>
    public class MedicamentServiceList : DBOList<MedicamentService>
    {
        public new bool Load()
        {
            dbWhere = "";
            return base.Load();
        }
        /// <summary>Загрузка медикаментов соотнесенных с услугой</summary>
        /// <param name="service_id">Идентификатор услуги</param>
        /// <param name="showdeleted">Показывать или нет удаленные медикаменты</param>
        public bool Load(int service_id, bool showdeleted)
        {
            dbWhere = "ServiceID=" + service_id.ToString();
            if (!showdeleted)
                dbWhere += " AND MedicamentService.Deleted = 0";
            return base.Load();
        }
    }
}