﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;
using Medsyst.Dao.Base;

namespace Medsyst
{
    /// <summary>
    /// Пользователь программы
    /// </summary>
    public class User : Person
    {

        [pk][fk("Person", "PersonID")][db("PersonUser")]public int UserID { get; set; }
        [db("PersonUser")]public string Login { get; set; }
        [db("PersonUser")]public string Password { get; set; }
        [db("PersonUser")]public string Email { get; set; }

        /////////////
        //Чирков Е.О
        //15.03.10
        //[db("PersonUser")]public int UGroupID { get; set; }   // принадлежность к группе пользователей
        public List<int> UGroupIDs { get; set; } //группы, в которые входит пользователь

        [db("PersonUser")]public DateTime RegisterD{ get; set; }          // дата регистрации
        [db("PersonUser")]public bool Block { get; set; }     // заблокирован или нет пользователь

        //Чирков Е.О
        //22.03.10
        [db("PersonUser")]public bool Deleted { get; set; } //был ли пользователь помечен как удаленный
        
        public User () 
        {
            UserID = 0;
            Login = "";
            Password = "";
            Email = "";

            //Чирков Е.О.
            //15.03.10
            ////////////////////////////
            //UGroupID = 0;
            RegisterD = new DateTime(2010,01,01);
            ////////////////////////////

            Block = false;
            UGroupIDs = new List<int>();
            //Чирков Е.О.
            //22.03.10
            Deleted = false;
        }

        //Чирков Е.О.
        //15.03.10
        public new bool Load()
        {
            if (!base.Load()) return false;
            //загрузка групп
            UGroupIDs.Clear();
            User_UserGroups ugs = new User_UserGroups();
            ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + UserID;
            ugs.Load();
            foreach (User_UserGroup ug in ugs) UGroupIDs.Add(ug.UGroupID);

            return true;
        }

        public new bool Insert()
        {
            if (PersonID != 0) UserID = PersonID;
            if (UserID <= 0) return false;
            string s = Password;
            Password = Security.GetMD5Hash(Password);
            bool b = base.InsertFull("PersonUser");
            Password = s;
            return b;
        }

        public new bool Delete()
        {
            return base.Delete("PersonUser");
        }

        //Чирков
        //19.03.2010
        //Вставка данных с шифрованием пароля
        public new bool InsertFull(string Table)
        {
            string p = Password;
            Password = Security.GetMD5Hash(Password);
            bool res = base.InsertFull(Table);
            Password = p;
            return res;
        }

        public new bool Update()
        {
            string s = Password;
            Password = Security.GetMD5Hash(Password);
            bool b = base.Update("PersonUser");
            Password = s;
            return b;
        }
        //Чирков Е.О.
        //19.03.2010
        //Обновление данных БЕЗ шифрования пароля (для того случая, когда обновляются данные, кроме пароля)
        public bool UpdateNoPassword()
        {
            return base.Update("PersonUser");
        }

        //Чирков Е.О.
        //22.03.2010
        //Удаление пользователя из БД. Если Recycle=true, то пользователь только помечается как удаленный, но на самом деле не удаляется
        public void RemoveFromTable(bool Recycle)
        {
            if (!Recycle) //полное удаление
            {
                User_UserGroups ugs = new User_UserGroups();
                ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + UserID;
                ugs.Load();

                foreach (User_UserGroup ug in ugs)
                {
                    ug.Init();
                    ug.Delete();
                }

                Init();
                Load();
                Delete("PersonUser");
            }
            else //удаление "в корзину"
            {
                Deleted = true;
                UpdateNoPassword();
            }
        }

        //Чирков Е.О.
        //22.03.10
        //Существует ли пользователь
        public bool IsExists()
        {
            Users us = new Users();
            us.dbSelect = "SELECT * FROM Person, PersonUser WHERE (PersonID=UserID) AND (UserID=" + UserID + ")";
            us.Load();
            return us.Count > 0;
        }
    }

    public class Users : DBOList<User> //Коллекция пользователей 
    {
        public bool Load(string login, string password)
        {
            string pass = Security.GetMD5Hash(password);
            dbWhere = "Login = '" + login + "' AND Password='" + pass + "'";
            return base.Load();
        }

        public bool Load(string login, string password, bool loadDeleted)
        {
            string pass = Security.GetMD5Hash(password);

            var deleted = loadDeleted ? "1" : "0";

            dbWhere = "Login = '" + login + "' AND Password='" + pass + "'" + " AND Deleted=" + deleted;

            return base.Load();
        }
    }
}