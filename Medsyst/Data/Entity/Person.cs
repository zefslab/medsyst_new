﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Class
{
    /// <summary>Персона
    /// 
    /// </summary>
    public class Person : DBO 
    {
        [db("Person")][pk]public int PersonID { get; set; }
        [db("Person")]public string Name { get; set; } // имя
        [db("Person")]public string LastName { get; set; } // фамилия
        [db("Person")]public string SurName { get; set; } // отчество
        [db("Person")]public DateTime BirthD { get; set; } // дата рождения
        [db("Person")]public string Pasport { get; set; } // паспортные данные
        [db("Person")]public string PhoneHome { get; set; } // Домашний телефон
        [db("Person")]public string PhoneMobile { get; set; } //Мобильный телефон
        [db("Person")]public int HomeAdressID { get;set; }
        [db("Person")]public bool Sex { get; set; } // пол
        [db("Person")]public DateTime RegistD { get; set; } // домашний адрес
        [db("Person")]public string EMail { get; set; }
        ///<summary>ФИО полностью</summary>
        public string Fullname {get{return LastName + " " + Name + " " + SurName;}}
        /// <summary>Краткая запись Фамилия И.О.</summary>
        public string FIOBrif
        {
            get { return LastName + " " + (Name != "" ? Name.Substring(0, 1) + ". " : "") + (SurName != "" ? SurName.Substring(0, 1) + ". " : ""); }
            set { ;}
        }
        public Address HomeAdress { get; set; } // домашний адрес

        public Person(): base() 
        {
            Name = LastName = SurName = "";
            BirthD = RegistD = new DateTime(1900,1,1);
            Pasport = "";
            PhoneHome = "";
            PhoneMobile = "";
            EMail = "";
            HomeAdress = new Address(true);
        }
        public Person(bool init) : base(init) 
        {
            Name = LastName = SurName = "";
            BirthD = RegistD = new DateTime(1900, 1, 1);
            Pasport = "";
            PhoneHome = "";
            PhoneMobile = "";
            EMail = "";
            HomeAdress = new Address(true);
        }

 
    }
}
