﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    //11.04.10
    //Чирков Е.О.
    public class MedicalFirm : Firm // Медицинское заведение	
    {
        [pk]
        [fk("Firm", "FirmID")]
        [db("FirmMedical", IsMain = false)]
        public int MedicalFirmID { get; set; }
        [db("FirmMedical", Name = "Comment")]
        public string MedComment { get; set; }//Можно указывать свойство с другим именем, но тогда нужно обязательно указывать спецификатор Name="" 

        public MedicalFirm() : base() { }
        public MedicalFirm(bool init) : base(init) { MedicalFirmID = 0; MedComment = ""; }

        public new bool Insert()
        {
            if (base.Insert("Firm"))
            {
                MedicalFirmID = Identity("Firm");
                return InsertFull("FirmMedical");
            }
            return false;
        }
    }

    public class MedicalFirmList : DBOList<MedicalFirm>
    {
        /// <summary>Загрузка списка медецинских учреждений
        /// Заплатин Е.Ф.
        /// 08.08.2011
        /// </summary>
        /// <param name="cd"></param>
        /// <returns></returns>
        public bool Load(CommandDirective cd)
        {
            dbOrder = "FirmN";
            dbWhere = "Deleted=0"; //кроме удаленных

            bool res = base.Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                MedicalFirm f = new MedicalFirm(true);
                f.FirmID = 0;
                f.FirmN = "-- Не определено --";
                this.Add(f);
            }
            return res;
        }
    }

}
