﻿using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    //26.04.10
    //Чирков Е.О.
    public class StudentGroup : DBO
    {
        [pk]
        [db("StudentGroup")]
        public int GroupID { get; set; }
        [db("StudentGroup")]
        [fk("Faculty", "FacultyID")]
        public int FacultyID { get; set; }
        [db("StudentGroup")]
        public string Name { get; set; }

        public StudentGroup() : base() { GroupID = 0; FacultyID = 0; Name = ""; }

        public override string ToString()
        {
            return Name;
        }
        /// <summary>Добавление новой студенческой группы в указанный факультет с указанным названием
        /// Заплатин Е.Ф.
        /// 05.06.2013
        /// </summary>
        /// <param name="facultyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int Add(int facultyId, string name)
        {
            StudentGroup sg = new StudentGroup();
            sg.Name = name;
            sg.FacultyID = facultyId;
            sg.Insert();
            sg.GroupID = sg.Identity("StudentGroup");

            return sg.GroupID;
        }
    }
    /// <summary>Коллекция: учебных групп
    /// Заплатин Е.Ф.
    /// 05.06.2013
    /// </summary>
    public class StudentGroupList : DBOList<StudentGroup>
    {
        public new bool Load(int facultyId)
        {
            dbWhere = "FacultyID=" + facultyId;
            dbOrder = "Name";
            return base.Load();
        }
    }
}
