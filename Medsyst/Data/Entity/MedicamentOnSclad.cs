﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;


namespace Medsyst.Class
{
    ///<summary>Класс: Медикамент, оприходованный на складе
    ///</summary>
    public class MedicamentOnSclad : MedicamentInput
    {
        /// <summary>Первичный ключ медикамента на складе</summary>
        [db("MedicamentOnSclad")]
        [pk]
        public int MedicamentOnScladID { get; set; }// Код приходного ордера
        [fk("MedicamentInput", "MedicamentInputID")]
        [db("MedicamentOnSclad", "MedicamentInputID")]
        public int MedicamentInputID_ { get; set; }// Код приходного ордера
        [db("MedicamentOnSclad")]
        public int CountOnSclad { get; set; } // Количество медикаментов на складе
        [db("MedicamentOnSclad")]
        public int CountReserve { get; set; } // Количество зарезервированных медикаментов
        ///<summary>Количество медикаментов на складе за вычетом зарезервированных (остаток)</summary>
        public int CountDivide { get { return CountOnSclad - CountReserve; } }

        public MedicamentOnSclad() { }
        public MedicamentOnSclad(bool init)
            : base(init)
        {
            MedicamentOnScladID = 0;
            MedicamentInputID_ = 0;
            CountOnSclad = 0;
            CountReserve = 0;
        }
        public new bool Insert()
        {
            MedicamentInputID_ = MedicamentInputID;
            if (Insert("MedicamentOnSclad"))
            {
                MedicamentOnScladID = Identity("MedicamentOnSclad");
                return true;
            }
            return false;
        }
        public new bool Delete()
        {
            return base.Delete("MedicamentOnSclad");
        }
        public new bool Update()
        {
            MedicamentInputID_ = MedicamentInputID;
            return base.Update("MedicamentOnSclad");
        }
        /// <summary>Выдача медикаментов со склада или возврат на склад елсли количество со знаком минус.</summary>
        /// <param name="count">Количество выдаваемых медикаментов</param>
        public void Output(int count)
        {
            CountOnSclad -= count;//Уменьшается количество на складе
            CountReserve -= count;//Снимается резерв
        }
        /// <summary>Постановка/снятие резерва медикаментов на складе</summary>
        /// <param name="count">количество медикаментов для постановки в резерв, а если со знаком минус, то - снятия с резерва</param>
        public void Resrved(int count)
        {
            CountReserve += count;
            if (CountReserve < 0)
            {//если count окажется отрицательным числом и превысит по абсолютному значению резерв
                MessageBox.Show("Внимание! Возникала нештатная ситуация при которой количество остатка медикамента: " + MedicamentN
                    + " стало меньше нуля: " + CountReserve.ToString() + ". Такого быть в принцыпе не должно."
                    + "\n\r Обязательно сообщите об этом разработчикам программы Медсист.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //TODO Cделать запись в базе данных о возникшей нештатной ситуации, т.к. нет гарантии, что пользователь сообщит о ней разработчикам
            }
        }
    }
    /// <summary>Класс: Коллекция оприходованных на складе медикаментов 
    /// </summary>
    public class MedicamentOnScladList : DBOList<MedicamentOnSclad> //Коллекция медикаментов на складе
    {
        /// <summary>Удаление складских медикаментов
        /// Заплатин Е.Ф.
        /// 17.11.2011
        /// </summary>
        public bool Delete()
        {
            //TODO если хоть один медикамент отказался быть удаленным, то производить откат всей групповой операции
            bool res = true;
            foreach (MedicamentOnSclad mos in this)
            {//Удаляется поочередно каждый элемент коллекции
                res = res && mos.Delete();
            }
            //if (res) this.Clear();//Очистка коллекции после успешного удаления всех ее элементов
            return res;

        }
        /// <summary>Загрузка складских медикаментов по идентификатору приходного медикамента
        /// Заплатин Е.Ф.
        /// 17.11.2011
        /// </summary>
        /// <param name="mi">приходный медикамент</param>
        public bool Load(MedicamentInput mi)
        {
            dbWhere = "MedicamentOnSclad.MedicamentInputID = " + mi.MedicamentInputID;
            return base.Load();
        }


        public bool Load(int CategoryID, decimal price1, decimal price2, bool zerovisible, bool isbudget, bool isnobudget)
        {
            dbWhere = "Price>=" + price1.ToString() + " AND Price<=" +
                price2.ToString() + " AND CategoryID = " + CategoryID +
                (zerovisible ? "" : " AND CountOnSclad-CountReserve > 0") +
                (isbudget || isnobudget ? " AND (" +
                (isbudget ? " IsBudget = 1" : "") +
                (isnobudget ? (isbudget ? " OR " : "") + " IsBudget = 0" : "") + ")" : " AND IsBudget = 2");
            return base.Load();
        }
        /// <summary>
        /// Формируется коллекция складских медикаментов по указанному идентификатору каталожного медикамента 
        /// с количеством на складе больше указанного
        /// Заплатин Е.Ф.
        /// 16.01.2012
        /// 07.02.2013, ЗЕФ.
        /// </summary>
        /// <param name="MedicamentID">Идентификатор каталожного медикамента</param>
        /// <param name="notzero">Количество должно быть больше нуля</param>
        /// <param name="countOnSclad">Определяет какое количество должно быть больше нуля: на складе или в остатке</param>
        /// <param name="f">указатель источника финансирования</param>
        public bool Load(int MedicamentID, bool notzero, bool countOnSclad, Finance f)
        {
            //Присоединяется дополнительная таблица документа, для добавления критерия отбора по сигнатуре
            dbFromAdd = "LEFT JOIN Document AS d ON d.DocumentID = MedicamentInput.InputDocumentID";//ЗЕФ. 07.02.2013

            dbWhere = "Medicament.MedicamentID = " + MedicamentID;
            if (notzero)
                dbWhere += (countOnSclad ? " AND CountOnSclad > 0" : " AND (CountOnSclad - CountReserve) > 0");
            if (f != Finance.Non)//Источник винасирования имеет значение
                dbWhere += " AND IsBudget = " + (f == Finance.Budget ? "1" : "0") +
                " AND d.Signed = 1"; //Медикаменты только из подписанных документов. ЗЕФ. 07.02.2013
            return base.Load();
        }
        /// <summary>Формируется коллекция складских медикаментов по указанной категории и некоторым фильтрующим параметрам
        /// </summary>
        /// <param name="CategoryID">категория медикаментов в каталоге</param>
        /// <param name="zerovisible"></param>
        /// <param name="isbudget"></param>
        /// <param name="isnobudget"></param>
        /// <returns></returns>
        public bool Load(int CategoryID, bool zerovisible, bool isbudget, bool isnobudget)
        {
            //Присоединяется дополнительная таблица документа, для добавления критерия отбора по сигнатуре
            dbFromAdd = "LEFT JOIN Document AS d ON d.DocumentID = MedicamentInput.InputDocumentID";//ЗЕФ. 11.05.2012

            dbWhere = "CategoryID = " + CategoryID +
                (zerovisible ? "" : " AND CountOnSclad-CountReserve > 0") +
                (isbudget || isnobudget ? " AND (" +
                (isbudget ? " IsBudget = 1" : "") +
                (isnobudget ? (isbudget ? " OR " : "") + " IsBudget = 0" : "") + ")" : " AND IsBudget = 2 ") +
                " AND d.Signed = 1"; //Медикаменты только из подписанных документов. ЗЕФ. 11.05.2012
            return base.Load();
        }
        public bool LoadSclad(int CategoryID, bool zerovisible, bool isbudget, bool isnobudget)
        {
            //Присоединяется дополнительная таблица документа, для добавления критерия отбора по сигнатуре
            dbFromAdd = "LEFT JOIN Document AS d ON d.DocumentID = MedicamentInput.InputDocumentID"; //ЗЕФ. 11.05.2012

            dbWhere = "CategoryID = " + CategoryID.ToString() +
                (zerovisible ? "" : " AND CountOnSclad > 0") +
                (isbudget || isnobudget ? " AND (" +
                (isbudget ? " IsBudget = 1" : "") +
                (isnobudget ? (isbudget ? " OR " : "") + " IsBudget = 0" : "") + ")" : " AND IsBudget = 2 ") +
                " AND d.Signed = 1"; //Медикаменты только из подписанных документов. ЗЕФ. 11.05.2012

            return base.Load();
        }

        public bool LoadSclad(int CategoryID, decimal price1, decimal price2, bool zerovisible, bool isbudget, bool isnobudget)
        {
            //Здесь неверно преобразуются десятичные числа в строковые с запятой вместо точки
            dbWhere = "Price>=" + price1.ToString() + " AND Price<=" +
                price2.ToString() + " AND CategoryID = " + CategoryID.ToString() +
                (zerovisible ? "" : " AND CountOnSclad > 0") +
                (isbudget || isnobudget ? " AND (" +
                (isbudget ? " IsBudget = 1" : "") +
                (isnobudget ? (isbudget ? " OR " : "") + " IsBudget = 0" : "") + ")" : " AND IsBudget = 2");
            return base.Load();
        }

        //Что делает этот метод?
        public bool LoadOfInputID(int medicament_input_id)
        {
            dbWhere = "MedicamentInput.MedicamentInputID = " + medicament_input_id.ToString();
            return base.Load();
        }
    }
}