﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    /// <summary>Класс: Факультет 
    /// Чирков Е.О.
    /// 26.04.10
    /// </summary>
    public class Faculty : DBO
    {
        [pk]
        [db("Faculty")]
        public int FacultyID { get; set; }
        [db("Faculty")]
        public string Name { get; set; }
        [db("Faculty")]
        public string ShortName { get; set; }

        public Faculty() : base() { FacultyID = 0; Name = ""; ShortName = ""; }

        public override string ToString()
        {
            return Name;
        }
        /// <summary>Добавление новой студенческой группы в указанный факультет с указанным названием
        /// Заплатин Е.Ф.
        /// 05.06.2013
        /// </summary>
        /// <param name="facultyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int Add(string name)
        {
            Faculty fac = new Faculty();
            fac.Name = name;
            fac.Insert();
            fac.FacultyID = fac.Identity("Faculty");
            return fac.FacultyID;
        }
    }
    /// <summary>Коллекция: факультетов
    /// Заплатин Е.Ф.
    /// 29.03.12
    /// </summary>
    public class FacultyList : DBOList<Faculty>
    {
    }
}
