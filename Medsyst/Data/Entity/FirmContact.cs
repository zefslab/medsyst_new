﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    //27.03.10
    //Чирков Е.О.
    public class FirmContact : DBO //Контакты фирмы
    {
        [db("FirmContact")]
        [pk]
        public int FirmContactID { get; set; }
        [db("FirmContact")]
        public int FirmID { get; set; }
        [db("FirmContact")]
        public int PersonID { get; set; }
        [db("FirmContact", Name = "Comment")]
        public string fcComment { get; set; }

        public FirmContact() : base() { }
        public FirmContact(bool init)
            : base(init)
        {
            FirmContactID = 0;
            FirmID = 0;
            PersonID = 0;
            fcComment = "";
        }
    }
    public class FirmContacts : DBOList<FirmContact> { }

}
