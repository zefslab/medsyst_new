﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Entity
{
    /// <summary>Класс: дефектные ведомости
    /// Заплатин Е.Ф.
    /// Дата создания: 09.08.2011
    /// </summary>
    public class DefectiveSheet : DemandMaterial
    {
        [pk]
        [fk("DocDemand", "DocumentID")]
        [db("DocDefectiveSheet", Name = "DocumentID", IsMain = true, Right = true)]
        public int _DocumentID { get; set; } //Код для связи с документом
        /// <summary>Сотрудник оформивший дефектную ведомость</summary>
        [db("DocDefectiveSheet")]
        public int EmployeeDefectID { get; set; }
        [db("DocDefectiveSheet")]
        public DateTime DefectiveD { get; set; }

        public DefectiveSheet() { }
        public DefectiveSheet(bool init)
            : base(init)
        {
            EmployeeDefectID = 0;
            DefectiveD = new DateTime(1900, 01, 01);
        }
        /// <summary>Создание новой дефектной ведомости
        /// Заплатин Е.Ф.
        /// 05.09.2011
        /// </summary>
        /// <param name="documentID">идентификатор требования, которому будет соответсвовать дефектная ведомость</param>
        public bool Insert(int documentID)
        {
            ////В родительском поле денлается отметка о создании дефектной ведомости
            //Demand d = new Demand(true);
            //d.DocumentID = documentID;
            //d.Load();
            //d.IsDefected = true;
            //if (d.Update("DocDemand"))//Обновляется значение поля IsDefected
            //{
            EmployeeDefectID = Security.user.PersonID;
            DefectiveD = DateTime.Now;
            _DocumentID = documentID;
            return InsertFull("DocDefectiveSheet");
            //}
            //return false;
        }
    }


}
