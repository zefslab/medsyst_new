﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /// <summary>Категории пациентов
    /// Заплатин Е.Ф.
    /// 03.05.2013
    /// </summary>
    public class PatientCategory : DBO, IDBO
    {
        /// <summary>Категория пациентов</summary>
        [pk]
        [db("PersonPatientCategory")]
        public int CategoryID { get; set; }
        /// <summary>Наименование категории пациентов</summary>
        [db("PersonPatientCategory")]
        public string CategoryN { get; set; }
        /// <summary>Тип направления пациента из лечебного учреждения: путевка или курсовка или платная основа</summary>
        [db("PersonPatientCategory")]
        public int TypeID { get; set; }
        /// <summary>Признак удаления категории</summary>
        [db("PersonPatientCategory")]
        public bool Deleted { get; set; }
        /// <summary>Порядок отображения категорий</summary>
        [db("PersonPatientCategory")]
        public int list_order { get; set; }
        /// <summary>Коменнатий к категории пациентов</summary>
        [db("PersonPatientCategory")]
        public string Comment { get; set; }

        //Элементы интерфейса
        public int ID { get { return CategoryID; } set { CategoryID = value; } } //идентификатор элемента
        public string Name { get { return CategoryN; } set { CategoryN = value; } } //Название элемента
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве
        public string comment { get { return Comment; } set { Comment = value; } }//Коментарий к выделеному элементу



        public PatientCategory() { }
        public PatientCategory(bool init) : base(init)
        {
            CategoryID = TypeID = list_order = 0;
            CategoryN = Comment = "";
            Deleted = false;
        }
    }
    /// <summary>Коллекция категорий пациентов
    /// Заплатин Е.Ф.
    /// 03.05.2013
    /// </summary>
    public class PatientCategoryList : DBOList<PatientCategory>, IDBOList
    {
        public bool ShowDeleted { get { return showdeleted; } set { showdeleted = value; } } //Признак отображения удаленных элементов в дереве. Добавил Заплатин Е.Ф. 07.07.2011

        /// <summary>Загрузка колекции неудаленных категорий
        /// Заплатин Е.Ф.
        /// </summary>
        /// <param name="cd">по умолчанию не нужно загружать неопределенное значение</param>
        /// <returns></returns>
        public new bool Load()
        {
            if (showdeleted == true)
            {
                dbWhere = "Deleted = 0";
            }
            bool res = base.Load();
            if (addnondefined != "")
            {//Требуется добавить запись с непоределенным значением
                PatientCategory patientCategory = new PatientCategory(true);
                patientCategory.CategoryID = 0;
                patientCategory.CategoryN = addnondefined;
                this.Add(patientCategory);
            }

            return res;
        }
        /// <summary>Перевод коллекции в строковый массив содержащий наименования категорий пациентов
        /// Заплатин Е.Ф.
        /// 06.05.2013
        /// </summary>
        /// <returns></returns>
        public new string[] ToArray()
        {
            Load();
            string[] a = new string[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                a[i] = this[i].CategoryN;
            }
            return a;
        }
        /// <summary>Удаление элемента из списка категорий. При успешном выполнении удаления возвращает - true. </summary>
        public bool Delete(int id)
        {
            return true;
        }
        /// <summary>Восстановление элемента списка категорий отмеченного как удаленный. При успешном восстановлении возвращает - true. </summary>
        public bool Restore(int id)
        {
            return true;
        }
        /// <summary>Добавление элемента в список категорий. При успешном добавлении возвращает - true. </summary>
        //public bool Add(string Text)
        //{
        //    return true;
        //}
        /// <summary>Переименование элемента в список категорий. При успешном переименовании возвращает - true. </summary>
        public bool Rename(string Text)
        {
            return true;
        }
    }

}
