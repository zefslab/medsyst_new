﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    ///<summary>Класс: Процесс/задача
    ///Заплатин Е.Ф.
    ///29.06.2012
    ///</summary>
    public class Task : DBO, ITreeItem 
    {
        [db("Task")][pk]public int TaskID { get; set; }
        /// <summary>Идентификатор родительской задчи</summary>
        [db("Task")]public int ParentID { get; set; }
        /// <summary>Порядок очередности выполнения задач</summary>
        [db("Task")]public int list_order { get; set; }
        /// <summary>Наименование задачи</summary>
        [db("Task")]public string TaskN { get; set; }
        /// <summary>Системное имя задачи</summary>
        [db("Task")]public string TaskSystemN { get; set; }
        /// <summary>Подпись кнопки завершения задачи</summary>
        [db("Task")]public string ButtonTaskComplet { get; set; }
        /// <summary>Строка статуса выполнения задачи</summary>
        [db("Task")]public string StatusTask { get; set; }
        /// <summary>Признак удаления</summary>
        [db("Task")]public bool Deleted { get; set; }

        //Реализация интерфейса древовидной структуры
        public int ID { get { return TaskID; } set { TaskID = value; } } //идентификатор элемента
        public string Text { get { return TaskN; } set { TaskN = value; } } //строковое представление элемента
        public bool visible { get; set; } //Признак отображения элемента в дереве
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве


        public Task () { }
        public Task(bool init): base(init) 
        {
            TaskID = ParentID = list_order = 0;
            TaskN = TaskSystemN = ButtonTaskComplet = StatusTask = "";
            Deleted = false;
        }
    }
    /// <summary>Класс: Коллекция задач
    /// Заплатин Е.Ф.
    /// 29.06.2012
    /// </summary>
    public class TaskList : DBOTree<Task>
    {
    }
    ///<summary>Класс: Тип уведомление о выполнении задачи
    ///Заплатин Е.Ф.
    ///29.06.2012
    ///</summary>
    public class Notice : DBO
    {
        [db("Notice")][pk]public int NoticeID { get; set; }
        /// <summary>Наименование типа уведомления</summary>
        [db("Notice")]public string NoticeN { get; set; }
        /// <summary>Системное имя типа уведомления</summary>
        [db("Notice")]public string NoticeSystemN { get; set; }
    
        public Notice () { }
        public Notice(bool init): base(init) 
        {
            NoticeID =  0;
            NoticeN = NoticeSystemN = "";
        }
    }
    /// <summary>Класс: Коллекция уведомлений
    /// Заплатин Е.Ф.
    /// 29.06.2012
    /// </summary>
    public class NoticeList : DBOList<Notice>
    {
        /// <summary>Загрузка списка типов уведомлений с неопределенным значением для тех записей, у которых свойство типа уведомления еще не определено
        /// Заплатин Е.Ф.
        /// 04.07.2012
        /// </summary>
        /// <param name="cd">директива</param>
        public bool Load(CommandDirective cd)
        {
            bool res = base.Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Notice n = new Notice(true);
                n.NoticeID = 0;
                n.NoticeN = "не определен";
                this.Add(n);
            }
            return res;
        }
    }
    ///<summary>Класс: Сотрудника, отвечающего за выполнение задачи
    ///Заплатин Е.Ф.
    ///03.07.2012
    ///</summary>
    public class TaskEmployee : DBO
    {
        [db("TaskEmployee")][pk]public int TaskEmployeeID { get; set; }
        /// <summary>Идентификатор задчи</summary>
        [db("TaskEmployee")]public int TaskID { get; set; }
        /// <summary>Идентификатор работника ответсвенного за выполнение задачи</summary>
        [db("TaskEmployee")]public int EmployeeID { get; set; }
         /// <summary>Идентификатор типа уведомления работника наступлении времени выполнения задачи</summary>
        [db("TaskEmployee")]public int NoticeID { get; set; }
        
        public TaskEmployee() { }
        public TaskEmployee(bool init): base(init)
        {
            TaskID = TaskEmployeeID = EmployeeID = NoticeID = 0;
        }
    }
    /// <summary>Класс: Коллекция сотрудников, отвечающих за выполнение задачи
    /// Заплатин Е.Ф.
    /// 04.07.2012
    /// </summary>
    public class TaskEmployeeList : DBOList<TaskEmployee>
    {
        public bool Load (Task t)
        {
            dbWhere = "TaskID = " + t.TaskID.ToString();
            return base.Load();
        }
    }
}
