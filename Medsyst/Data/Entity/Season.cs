﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /// <summary>Класс: сезон в котором ведется учет лечебных процедур. 
    /// 
    /// </summary>
    public class Season : DBO
    {
        [db("Season")][pk] public int SeasonID { get; set; }
        [db("Season")] public int SeasonNbr { get; set; } // порядковый номер сезона в году
        [db("Season")] public DateTime SeasonOpen { get; set; } // дата открытия сезона
        [db("Season")] public DateTime SeasonClose { get; set; } //дата закрыттия сезона
        [db("Season")] public int Year { get; set; } // год к которому принадлежит сезон
        public bool Chk { get; set; } // Используется в списках с чекбоксами. ЗЕФ. 09.04.2012

        public string FullNbr { 
            get 
            {
                if (SeasonID != 0)
                    return SeasonNbr + "/" + Year;
                else
                    return "Не определен";
            } 
        } //Составной номер сезона


        public Season() { }
        public Season(bool init) : base(init) 
        {
            //Необходимо обязательно инициировать все текстовые поля и поля с датами
            //Иначе при записи в базу данных будет возникать ошибка
            SeasonOpen = new DateTime(DateTime.Now.Year,1,1);
            SeasonClose = new DateTime(DateTime.Now.Year, 1, 1);
            SeasonNbr = 0;
            Year = 1900;
            Chk = false;

        }
            
        /// <summary> Перегрузка необходима для корректной работы ComboBox</summary>
        /// <returns></returns>

        public override string ToString()
        {
            return SeasonNbr.ToString();
        }
    }
    public class Seasons : DBOList<Season> //коллекция сезонов
    {
      /// <summary>Загрузка списка сезонов с неопределенным значением для тех записей, у которых свойство сезона еще не определено
        /// Заплатин Е.Ф.
        /// 08.08.2011
        /// </summary>
        /// <param name="cd">директива</param>
        /// <returns></returns>
        public bool Load(CommandDirective cd)
        {
            dbWhere = "";
            dbOrder = "Year DESC, SeasonNbr DESC"; //Определяется порядок сортировки сначала по годам в обратном порядке, а затем по номерам сезонов.

            bool res = base.Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Season s = new Season(true);
                s.SeasonID = 0;
                this.Add(s);
            }
            return res;
       }
        
        ///<summary>Загрузка сезонов по годам</summary>
        public bool Load( int year)
        {
            dbWhere = "Season.Year = " + year;
            dbOrder = "SeasonNbr";
            return base.Load();
        }

        ///<summary>Загрузка сезонов по годам</summary>
        public bool Load(IEnumerable<int> seasonsId)
        {
            dbWhere = "Season.SeasonID in (" + String.Join(",", seasonsId.Select(id=> id.ToString()).ToArray()) + ")";
            return base.Load();
        }

        ///<summary>Загрузка сезонов входящих целиком в диапазон указанных дат</summary>
        ///<param name="dBegin">Дата начала диапазона</param>
        ///<param name="dEnd">Дата конца диапазона</param>
        public bool Load(DateTime dBegin, DateTime dEnd)
        {
            dbWhere = "Season.SeasonOpen >= '" + dBegin.ToString("dd.MM.yyyy") + "'" +
                      " AND Season.SeasonClose <= '" + dEnd.ToString("dd.MM.yyyy") + "'";
            dbOrder = "Season.SeasonOpen";
            return base.Load();
        }
        
        /// <summary>Подсчет количества историй болезней пациентов определенных категорий в коллекции сезонов
        /// Заплатин Е.Ф.
        /// 30.05.2011
        /// 24.02.2013 - полностью переписан. ЗЕФ
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public int CountIH(int[] categories) //Должен передаваться список категорий пациентов
        {
            if (Count == 0 || categories == null)
            {
                return 0;
            }

            return new IllnessHistoryShort2List().GetCount(categories, this.Select(x => x.SeasonID).ToArray());
        }

    }
}
