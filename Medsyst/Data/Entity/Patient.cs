﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /// <summary>
    /// Пациент профилактория
    /// </summary>
    public class Patient : Employee  
    {
        /// <summary>Код для связи с персоной ("Person", "PersonID")</summary>
        [pk][fk("PersonEmployee", "PersonID")][db("PersonPatient")]public int PatientID { get; set; }
        /// <summary>Категория пациентов</summary>
        [db("PersonPatient")]public int CategoryID { get; set; }
        /// <summary>Группа для студентов</summary>
        [db("PersonPatient")]public int StudentGroupID { get; set; } 
        /// <summary>Состоит на диспансерном учете</summary>
        [db("PersonPatient")]public bool ClinicRegiter { get; set; }
        /// <summary>Состоит на диспансерном учете по заболеванию</summary>
        [db("PersonPatient")]public string txtClinicRegiter { get; set; }
        /// <summary>Инвалидность</summary>
        [db("PersonPatient")]public bool Disabiblity { get; set; }
        /// <summary>Инвалидность по заболеванию</summary>
        [db("PersonPatient")]public string txtDisabiblity { get; set; }
        /// <summary>Сирота</summary>
        [db("PersonPatient")]public bool Orphan { get; set; }
        /// <summary>Донор</summary>
        [db("PersonPatient")]public bool Donor { get; set; }
        /// <summary>Беременность</summary>
        [db("PersonPatient")]public bool Pregnancy { get; set; }
        /// <summary>КОВ</summary>
        [db("PersonPatient")]public bool KOV { get; set; }
        [db("PersonPatient")]public bool Deleted { get; set; }


        public Address Address; 

        public Patient() { }
        public Patient(bool init) : base(init) 
        {
            Clear();
            Address = new Address(true);
            StudentGroupID = 0;
        }

        public new bool Load()
        {
            bool b = base.Load();
            if (HomeAdressID <= 0)
            {
                Address.Insert();
                HomeAdressID = Address.AddressID;
                Update("Person");
            }
            Address.AddressID = HomeAdressID;
            Address.Load();
            if (!IsEmployeeExist())
            {
                return InsertFull("PersonEmployee");
            }
            return b;
        }

        public new bool Insert() //Объяснить суть каскадного вызова
        {
            RegistD = DateTime.Now; //Зачем?
            if (Insert("Person"))
            {
                PersonID_employee = PersonID = PatientID = Identity("Person");
                if (InsertFull("PersonEmployee"))
                {
                    if (InsertFull("PersonPatient"))
                    {
                        if (Address.Insert())
                        {
                            HomeAdressID = Address.AddressID;
                            return Update("Person");
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Обновление записи в таблице
        /// </summary>
        /// <returns>Успешность выполнения</returns>
        public new bool Update()
        {
            if(base.Update())//?
            {
                return Address.Update();//?
            }
            return false;
        }
        /// <summary>Удаление пациента
        /// Заплатин Е.Ф.
        /// 20.10.2011
        /// </summary>
        public new bool Delete()
        {
            if (Deleted)
            {
                if (base.Delete())
                {
                    return Address.Delete();
                }
                return false;
            }
            else
            {
                Deleted = true;
                return Update();
            }
        }

     }
    public class PatientList : DBOList<Patient>
    {
        //ЗЕФ. Используется при выборе пациентов по принадлежности к категории
        public bool Load(int[] categories)
        {
            if (categories.Length > 0)
            {
                dbWhere = "Deleted = 0 AND ";
                for (int i = 0; i < categories.Length; i++)
                    dbWhere += " [CategoryID] = " + categories[i].ToString() + " OR";
                dbWhere = dbWhere.Substring(0, dbWhere.Length - 3);
                return base.Load();
            }
            return false;
        }
        //ЗЕФ. Используется при поиске пациентов через строку поиска
        public bool Load(string name)
        {
            dbWhere = "Deleted = 0 AND ";
            dbWhere += " Lastname LIKE '%" + name + "%'";
            return base.Load();
        }
        //ЗЕФ. Используется для выбора пациентов с учетом элементов фильтра
        public bool Load(int[] categories, bool sex, int year1, int year2, DateTime RegistD1, DateTime RegistD2, bool issex, bool isyear, bool isregistd)
        {
           
            _getDbWhere(categories, sex, year1,year2, RegistD1, RegistD2, issex, isyear, isregistd);

            return base.Load();
        }

        private void _getDbWhere(int[] categories, bool sex, int year1, int year2, DateTime RegistD1, DateTime RegistD2,
                             bool issex, bool isyear, bool isregistd)
        {
            dbWhere = "Deleted = 0 AND ";
            if (categories.Length == 0)//ЗЕФ. 07.05.2013 Если не передано ни одной категории то нужно отфильтровать всех пациентов
            {
                dbWhere += " [CategoryID] = 0";
            }
            else
            {
                for (int i = 0; i < categories.Length; i++)
                    dbWhere += " [CategoryID] = " + categories[i].ToString() + " OR";
                dbWhere = dbWhere.Substring(0, dbWhere.Length - 3);
            }
            dbWhere = "(" + dbWhere + ")";
            dbOrder = " LastName ";


            if (issex) dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " [Sex]=" + (sex ? "1" : "0");
            if (isyear) dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " [BirthD]>='" + new DateTime(year1, 1, 1).ToString("dd.MM.yyyy") + "' AND [BirthD]<='" + new DateTime(year2, 12, 31).ToString("dd.MM.yyyy") + "'";
            if (isregistd) dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " [RegistD]>='" + RegistD1.ToString("dd.MM.yyyy") + "' AND [RegistD]<='" + RegistD2.ToString("dd.MM.yyyy") + "'";
        }
        //Попытка оптимизировать запрос для списка пациентов. Пока запутался. Нужно еще разбираться.
        //public bool LoadShot(int[] categories, bool sex, int year1, int year2, DateTime RegistD1, DateTime RegistD2, bool issex, bool isyear, bool isregistd)
        //{
        //    _getDbWhere(categories, sex, year1, year2, RegistD1, RegistD2, issex, isyear, isregistd);

        //    dbSelectUser = "SELECT pp.PatientID, p.LastName, p.SurName, p.Name, p.BirthD" +
        //                   " FROM PersonPatient AS pp " +
        //                   " LEFT JOIN Person AS p ON p.PersonID = pp.PatientID  ";

        //    return base.Load();
        //}
    }
  
}
