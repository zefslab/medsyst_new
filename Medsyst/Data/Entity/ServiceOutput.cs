﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Constant;

namespace Medsyst.Class
{
    ///<summary>Назначенные услуги</summary>
    public class ServiceOutput : Service
    {
        ///<summary>Идентификатор назначения услуги</summary>
        [db("ServiceOutput")]
        [pk]
        public int ServiceOutputID { get; set; }

        ///<summary>Идентификатор документа по которому назначена услуга</summary>
        [db("ServiceOutput")]
        public int DocumentID { get; set; }

        ///<summary>Внешний ключ на услугу назначеную в истории болезни</summary>
        [fk("Service", "ServiceID")]
        [db("ServiceOutput", "ServiceID")]
        public int ServiceID_ { get; set; }

        ///<summary>Количество услуг</summary>
        [db("ServiceOutput")]
        public int Count { get; set; }

        /// <summary>Переопределено свойство от родительского класса
        /// Это сделано для того чтобы при изменении цен на медикаменты в перйскуранте услуг
        /// не изменялись цены уже назначенных ранее услуг </summary>
        [db("ServiceOutput")]
        public new decimal PriceMed {get; set; }
       
        ///<summary>Суммарная стоимость всех медикаментов услуги</summary>
        public decimal PriceSum { get { return this.PriceMed * Count; } }

        /// <summary>ЗЕФ. Описание характера применения назначенной процедуры</summary>
        [db("ServiceOutput")]
        public string Character { get; set; }

        ///<summary>Дата назначения услуги</summary>
        [db("ServiceOutput")]
        public DateTime Date { get; set; }

        /// <summary>ЗЕФ. Признак того, что  услуга оказана пациенту</summary>
        [db("ServiceOutput")]
        public bool Fulfil { get; set; }

        ///<summary>Дата оказания услуги</summary>
        [db("ServiceOutput")]
        public DateTime DateFulfil { get; set; }

        ///<summary>Признак удаления услуги</summary>
        [db("ServiceOutput")]
        public new bool Deleted { get; set; }


        public ServiceOutput()
            : base(true)
        {
            Date = new DateTime(1900, 1, 1);
            DateFulfil = new DateTime(1900, 1, 1);
            Character = "";
            Fulfil = false;
            Deleted = false;
        }

        public new bool Insert()
        {
            ServiceID_ = ServiceID;
            return Insert("ServiceOutput");
        }
        /// <summary>Удаление услуги из назначения. Если услуга была ранее отмечена как удаленная, то она удаляется перманентно
        /// вместе с медикаментами, назначенными в расход
        /// Заплатин Е.Ф.
        /// 13.09.2011
        /// </summary>
        public bool Delete()
        {
            bool res = false;
            OutputMedicamentList mos = new OutputMedicamentList();
            if (Deleted == true)//Если услуга уже была ранее отмечена как удаленная
            {//Удаляется информация из базы данных
                mos.Load(true, ServiceOutputID);//Загружаются все медикаменты из расхода включая отмеченные как удаленные  для назначеной услуги                
                //Резерв со склада снимать не нужно, т.к. он был уже снят при отметке услуги как удаленной
                if (mos.Delete(CommandDirective.DeletePermanently))//Удаление медикаментов из расхода без возможности восстановления
                    //OutputMedicaments( CommandDirective.DeletePermanently, finance);//Медикаменты удаляются полностью из базы данных
                    res = base.Delete("ServiceOutput");
            }
            else
            {//Устанавливается лишь признак удаления услуги
                mos.Load(false, ServiceOutputID);//Загружаются все не отмеченные как удаленные медикаменты для назначеной услуги
                mos.UnreservateOnSclad();//Возвращаются медикаменты на склад, т.е. снимаются с резерва
                Deleted = true;//Назначается признак удаления записи
                res = Update();//Обновляется информация в базе данных
            }

            return res;
        }
        public new bool Update()
        {
            ServiceID_ = ServiceID;
            return base.Update("ServiceOutput");
        }
        /// <summary>Проверка наличия медикаментов на складе связанных с назначаемой услугой        
        /// Заплатин Е.Ф.
        /// 28.11.10</summary>
        /// <used_by>ServiceOutputList.Apply</used_by>
        /// <param name="count">Количество запрошенных медикаментов для проверки на доступность на складе</param>
        /// <returns>Возвращается строку с перечнем  медикаментов, которых не оказалось на складе</returns>
        public string CheckMedicamentOnSclad(int count, Finance f)
        {
            string str_med = "";
            string str_med_all = "";
            MedicamentServiceList smeds = new MedicamentServiceList();
            MedicamentOnScladList moss = new MedicamentOnScladList();

            //Шаг 1. Составление списка каталожных медикаментов соответсвующих прейскурантной услуге 
            smeds.Load(this.ServiceID_, false);

            //Шаг 2. По каждому каталожному медикаменту формируется коллекция складских медикаментов, соответсвующих каталожным, 
            //количество остатка которых на складе больше нуля, сравнивается затребованное количество с остатками на складе
            foreach (MedicamentService sm in smeds)
            {
                //Извлекаются со склада медикаменты соответсвующие каталожному и относящиеся к приходу по определенному финансовому источнику
                moss.Load(sm.MedicamentID, true, true, f);
                //Подсчитываектся сумма остатков для сравнения с затребуемым количеством при назначении услуги
                int summ = 0;
                ///Это в принцыпе можно было бы сделать одним запросом. Так и нужно будет сделать в дальнейшем
                foreach (MedicamentOnSclad mos in moss)
                {
                    summ += mos.CountOnSclad - mos.CountReserve;//Добавляется сумма остатка к общей накапливаемой сумме 
                }//на выходе из цикла сумма количества медикаментов на складе по указанному сервисному медикаменту

                //Шаг 3. Если количества какого-то медикамента недостаточно, то он попадает в список возврашаемый методом
                str_med_all += sm.MedicamentN + ", ";
                if (p.trace == true) MessageBox.Show("Затребован медикамент - " + sm.MedicamentN + " по услуге в количестве: " + (sm.Count * count).ToString() +
                                    ",\r\nна складе остаток: " + summ.ToString(), "Информер", MessageBoxButtons.OK);

                if (sm.Count * count > summ)
                {
                    str_med += sm.MedicamentN + " требуется: " + (sm.Count * count).ToString() +
                                    ", остаток складе : " + summ.ToString() + "; ";//Медикамент добавляется в список недостающих медикаментов
                }

            }
            if (p.trace == true) MessageBox.Show("Проверены все медикаменты соответствующие назначаемой услуге: " + str_med_all + ".", "Информер", MessageBoxButtons.OK);

            return str_med;
        }

        /// <summary>Постановка/снятие медикаментов в/с резерв(а) на складе (MedicamentOnSclad) и
        /// добавление/удаление медикаментов в/из расход(а) (MedicamentOutput) с установкой/снятием отметки (Reserved) о постановке в резерв
        /// Автор: Заплатин Е.Ф.
        /// Дата: 20.12.2010
        /// </summary>
        /// <param name="directive">указывает на то, добавлять, удалять или маркировать как удаленные медикаменты</param>
        /// <param name="serviceCount">количество добавляемых или удаляемых услуг. Необходимость в этом параметре возникла потому, 
        /// что требуется не только удалять и добавлять услуги целиком, но и изменять их количество в большую или меньшую сторону</param>
        public void OutputMedicaments(CommandDirective directive, Finance finance, params int[] serviceCount)
        {//TODO Разбить метод на два для процессов снятия и для постановки отдельно
            if (directive == CommandDirective.Non)
            {//Нет ни каких директив в отношении медикаментов
                return;
            }
            else if (directive == CommandDirective.Add)
            {//Добавляются медикаменты в расход
                // Шаг 1. Добавляются записи в расход медикаментов связанные с назначенными услугами
                //1.1. Выбираются медикаменты из (ServiceMedicament) в соответсвии с услугой
                var sms = new MedicamentServiceList();
                sms.Load(ServiceID, false);//Загружается коллекция медикаментов по оказанной услуге
                //Перебираются все медикаменты услуги
                foreach (MedicamentService sm in sms)
                {
                    //Вычисляется требуемое количество медикаментов для списания в расход. 
                    int RequestMeds = sm.Count * serviceCount[0];// Умножается количество медикаментов по услуге на количество назначенных или доназначаемых пациетну услуг
                    //Составляется перечень медикаментов на складе по указанному медикаменту с ненулевым количеством
                    var moss = new MedicamentOnScladList();
                    moss.Load(sm.MedicamentID, true, true, finance);//Загружаются  медикаменты со склада количество которых больше или равно единицы
                    //Перебираются все складские медикаменты найденные по критериям отбора введенные на склад по разным приходам
                    foreach (MedicamentOnSclad mos in moss)
                    {
                        int OutputMed = 0; //Количество медикаментов добавляемое в расход
                        //Возможны два случая: кода медикаментов хватает для постановки в резерв с одного прихода и когда
                        //предеется задействовать более одного прихода, чтобы полностью поставить медикамент в резерв
                        if (mos.CountDivide >= RequestMeds)
                        {// медикаментов с одного прихода достаточно
                            //Списываются медикаменты со склада
                            OutputMed = RequestMeds; //Количество медикаментов добавляемое в расход
                            mos.CountReserve += RequestMeds; //Требуемое количество добавляется в резерв
                            RequestMeds = 0;
                            mos.Update();//Обновляются данные в базе
                        }
                        else
                        {//не достаточно медикаментов с одного прихода
                            //Списываются медикаменты со склада
                            OutputMed = mos.CountDivide; //Количество медикаментов добавляемое в расход приравнивается к остатку
                            RequestMeds -= OutputMed; //Пересчитывается списываемое количество медикаментов, уменьшаясь на величину медиаментов направленную в расход
                            mos.CountReserve = mos.CountOnSclad; //Полностью все медикаменты ставятся в резерв
                            mos.Update();//Обновляются данные в базе
                        }
                        if (p.trace == true) MessageBox.Show("Медикамент " + mos.MedicamentN + " поставлен в резер на складе.", "Информер");

                        //Добавляется медикамент в расход
                        MedicamentOutput mo = new MedicamentOutput(true);
                        mo.MedicamentOnScladID = mos.MedicamentOnScladID;
                        mo.CountOutput = OutputMed; //Количество расходуемых медикаментов по текщему приходу.
                        mo.ServiceOutputID = ServiceOutputID; //связь с назначенной услугой
                        mo.EmployeeID = Security.user.PersonID; //присваивается идентифекатор текущего пользователя
                        mo.MedicamentD = DateTime.Now;//Текущая дата добавления медикамента в расход
                        mo.Reserved = true; //медикамент отмечается как зарезервированный в расходе.
                        mo.Price = mos.Price; //Расходная цена приравнивается к приходной
                        mo.OutputDocumentID = DocumentID; //Идентификатор связанного расходного документа (История болезни)
                        //Document d = new Document();
                        //d.DocumentID_ = DocumentID;
                        //d.Load();//Требуется загрузка данных о документе, чтобы установить тип расходного документа. ЗЕФ. 10.08.2011
                        ////mo.OutputDocumentTypeID = d.DocTypeID;
                        mo.Insert();//Записываются данные в расход
                        mo.Load(); //Временно добавлено чтобы выводить сообщение
                        if (p.trace == true) MessageBox.Show("Медикамент " + mo.MedicamentN + " добавлен в расход в количестве: "
                                           + mo.CountOutput.ToString(), "Информер");
                        if (RequestMeds == 0) //Все медикаменты списаны со склада и назначены в расход
                            break; //Завершается цикл списывания со склада и добавления в расход одного из медикаментов услуги
                    }
                }
            }
            else if (directive == CommandDirective.DeletePermanently || directive == CommandDirective.MarkAsDeleted)
            {//Удаляются или отмечаются как удаленные из расхода все медикаменты в случае удаления услуги из назначения до того как ее успели оказать или
                //было уменьшено количество назначенной услуги
                bool loadDeleted;//Признак того, что нужно загружать отмеченные как удаленные записи
                if (directive == CommandDirective.MarkAsDeleted)
                    //Загружать записи отмеченные как удаленные не нужно, если требуется отметить как удаленные
                    loadDeleted = false;
                else //directive == CommandDirective.DeletePermanently
                    //Загружаются все записи, в том числе и отмеченные как удаленные, если требуется полностью удалить
                    loadDeleted = true;
                //Выбираются медикаменты из (ServiceMedicament) в соответсвии с услугой
                var sms = new MedicamentServiceList();
                sms.Load(ServiceID, false);//Загружается коллекция медикаментов по оказанной услуге из прейскуранта
                foreach (MedicamentService sm in sms)
                {//Перебираются все медикаменты услуги
                    //Выбираются медикаменты из (MedicamentOutput) в соответсвии с назначеной услугой
                    OutputMedicamentList mos = new OutputMedicamentList();
                    mos.Load(loadDeleted, ServiceOutputID, sm.MedicamentID);//Загружается коллекция медикаментов назначенных в расход по назначенной услуге, 
                    if (serviceCount.Length == 0)
                        //Удаляемого количества услуги не указано в передаваемом параметре поэтому требуется  удаление всех медикаментов по услуге
                        mos.Delete(directive);//Удаляются или отмечаются как удаленные все медикаменты коллекции в расходе
                    else
                        //удаляется лишь указанное количество медикаментов после изменения количества услуг в назначении
                        mos.Delete(serviceCount[0] * sm.Count);//общее количество равно произведению кол-ва услуг на кол-во медикаментов прейскуранта
                    if (p.trace == true) MessageBox.Show("Медикаменты " +
                           (directive == CommandDirective.MarkAsDeleted ? "отмечены как удаленные в расходе" : "удалены из базы") +
                           ", резерв на складе снят.", "Информер");
                }
            }
        }
        /// <summary>Применение (оказание) назначенной пациенту единичной услуги. Для этого нужно: 
        /// 1) снять признак резервирования с расходуемых медикаментов  
        /// 2) снять резерв со склада,  
        /// 3) уменьшить остаток в требованиях
        /// Автор: Заплатин Е.Ф.
        /// 05.12.2010
        /// 11.07.2011
        /// 22.08.2011 Задействовал метод  mo.Unreservate()
        /// </summary>
        public void Implement()
        {
            Finance finance;
            //Составляется перечень медикаментов в расходе по назначенной услуге
            OutputMedicamentList mos = new OutputMedicamentList();
            mos.Load(false, ServiceOutputID);//Загружаются все медикаменты из расхода за исключением удаленных (False)

            if (!Fulfil)
            {//Услуга не была ранее отмечена как оказанная
                //Прежде чем применять услугу нужно убедиться, что по требованиям достаточно медикаментов для ее оказания  
                if (!CheckCountMedInDemand()) return; //Недостаточно медикаментов по требованиям для оказания услуги

                foreach (MedicamentOutput mo in mos)
                {//Перебираются все медикаменты из расхода по назначенной услуге

                    MedicamentByDemandList mbds = new MedicamentByDemandList();
                    if (mo.IsBudget) finance = Finance.Budget; else finance = Finance.ExtraBudget;
                    mbds.Load(ServiceID, mo.MedicamentID, (int)DocTypes.DemandService, true, finance);// Возможно сдесь ошибка в том что не прогружаются все медикаменты по требованиям!!! Загружаются  медикаменты с остатком большим ноля (true) по всем требованиям, принадлежащим конкретной услуге.ЗЕФ, 07.08.2011
                    mo.Unreservate(mbds);//Снятие резерва со склада и с расхода, уменьшение складского количества и остатков по требованиям
                }
                Fulfil = true;//Признак того что услуга оказана
                DateFulfil = DateTime.Now;//Дата оказания услуги
            }
            else
            {/*услуга была уже назначена ранее и ее назначение отменяется. Необходимо посоветоваться, 
              * возможно такую операцию следует запретить или нужно отслеживать выданы ли медикаменты по этой услуге.
              * Выполняются действия в обратном опрядке назначению услуги
              */
                foreach (MedicamentOutput mo in mos)
                {//Перебираются все медикаменты из расхода по назначенной услуге
                    MedicamentByDemandList mbds = new MedicamentByDemandList();
                    if (mo.IsBudget) finance = Finance.Budget; else finance = Finance.ExtraBudget;
                    mbds.Load(ServiceID, mo.MedicamentID, (int)DocTypes.DemandService, false, finance);//Загружаются  медикаменты по всем требованиям, принадлежащим конкретной услуге.ЗЕФ, 07.08.2011
                    mo.Unreservate(mbds);//Заменяет весь закомментированный код. ЗЕФ. 22.08.2011
                }
                Fulfil = false;//Снятие отметки о назначении
                DateFulfil = new DateTime(1900, 1, 1);
            }
            Update();//Сохранение изменений по назначенной услуге
        }

        /// <summary>Проверяется достаточность медикаментов выписанных по требованию для применения услуги
        /// </summary>
        /// Заплатин Е.Ф.
        /// 28.12.2010
        /// 22.08.2011 - задействован метод medbds.CheckCount(mo.CountOutput, mo.MedicamentID). ЗЕФ
        /// <returns>true - если медикаментов достаточно, false - если медикаменто недостаточно выписано по требованиям</returns>
        public bool CheckCountMedInDemand()
        {
            bool res = true;
            Finance f = Finance.Budget;//Заглушенное значение, котрое будет изменено
            //Проверка проводится по каждому медикаменту прейскурантной услуги в отдельности
            MedicamentServiceList mss = new MedicamentServiceList();
            mss.Load(ServiceID, false);
            foreach (MedicamentService ms in mss)
            {
                //Составляется перечень медикаментов в расходе по назначенной услуге
                OutputMedicamentList mos = new OutputMedicamentList();
                mos.Load(false, ServiceOutputID, ms.MedicamentID);//Загружаются все медикаменты назначенные в расход за исключением удаленных (False)

                int countMOS = 0;//Суммарное количество одноименных медикаментов в расходе. Одноименные медикаменты могут быть отпущены в расход по разным приходам
                foreach (MedicamentOutput mo in mos)
                {//Производится суммирование количества назначеных в расход одноименных медикаментов 
                    if (mo.IsBudget) f = Finance.Budget; else f = Finance.ExtraBudget;//Если не будет ни одного расходного медикамента то и поиска медикаментов по требованию тоже не сотстоиться
                    //Все медикаменты по одной услуге расходуются исключительно по одному источнику финансирования.
                    countMOS += mo.CountOutput;
                }
                if (countMOS > 0)
                {//Набралась сумма отличная от нуля
                    MedicamentByDemandList mbds = new MedicamentByDemandList();
                    mbds.Load(ServiceID, ms.MedicamentID, (int)DocTypes.DemandService, true, f);//Загружаются  медикаменты с остатком большим ноля (true) по всем требованиям, принадлежащим конкретной услуге.ЗЕФ, 07.08.2011

                    res = res && mbds.CheckCount(countMOS, ms.MedicamentID, f);
                }
            }
            return res;
        }

    }

    ///<summary>
    ///Класс: Коллекция назначенных услуг
    ///</summary>
    public class ServiceOutputList : DBOList<ServiceOutput>, IEnumerable<ServiceOutput>
    {
        public new bool Load()
        {
            dbWhere = "ServiceOutput.Deleted = 0";//Кроме тех, на которые поставлен признак удаленного
            //Похорошему нужно перенести этот критерий в родительский метод DBOList, а в текущий метод добавить 
            //параметр, определяющий показывать или нет удаленные записи. Это в последствии пригодится при просмотре 
            // удаленных записей с административными правами            
            return base.Load();
        }

        /// <summary>Загрузка назначенных, но еще не оказаных услуг</summary>
        /// <author>Заплатин Е.Ф.</author>
        /// <date>04.10.2012</date> 
        public bool Load(bool l) //фихтивный параметр
        {
            dbSelectCustom = "SELECT ServiceID, COUNT(ServiceID) as cnt" +
           " FROM ServiceOutput" +
           " LEFT JOIN Document as d on d.DocumentID=ServiceOutput.DocumentID" +
           " Where ServiceOutput.Deleted = 0" + //За исключением удаленных услуг
                   " AND d.Deleted = 0" + //За исключением удаленных документов по котороым была назначена услуга  +  
                   " AND Fulfil = 0" + //Не отмеченные как уже оказанные
                   " AND NOT ServiceID IS NULL" + //Исключая несуществующие услуги
                   " AND ServiceID <> 0" + // 
           " GROUP BY ServiceID";
            return base.Load();
        }
        /// <summary>Загрузка назначенных услуг по указанному расходному документу</summary>
        /// <param name="docid">Идентификатор расходного документа</param>
        public bool Load(int docid)
        {
            dbWhere = "ServiceOutput.Deleted = 0 AND ";
            dbWhere += "DocumentID=" + docid.ToString();
            return base.Load();
        }
        /// <summary>Загрузка назначенных услуг по массиву расходных документов
        /// Заплатин Е.Ф.
        /// 12.07.2012</summary>
        /// <param name="docids">Массив идентификаторов расходного документа</param>
        public bool Load(int[] docids, params bool[] fulfil)
        {
            if (docids.Count() > 0)
            {//Если переданы идентификаторы документов то производится загрузка
                dbWhere = "ServiceOutput.Deleted = 0 AND ";
                dbWhere += "DocumentID in (";
                string docs = "";
                foreach (int i in docids)
                {
                    docs += "," + i.ToString();
                }
                docs = docs.Remove(0, 1);//Удаляется лишняя запитая-разделитель
                dbWhere += docs + ")";
                if (fulfil.Count() != 0)
                {//Передано условие отбора услуг (оказаны/неоказаны)
                    dbWhere += " AND Fulfil = " + (fulfil[0] ? "1 " : "0 ");
                }

                dbOrder = "ServiceN";
                return base.Load();
            }
            return false;
        }


        /// <summary>Применение, а фактически добавление услуги к назначению с предварительной оценкой наличия
        /// достаточного количества медикаментов на складе
        /// </summary>
        /// <author>Заплатин Е.Ф.</author>
        /// <date>04.12.2010</date> 
        /// <used_by>frmIllnessHystory.btnAddService_Click</used_by>
        /// <param name="serviceID"> код назначаемой услуги из прейскуранта</param> 
        /// <param name="counService"> количество назначаемых услуг</param>
        /// <param name="docID">идентификатор документа по которому назначается услуга</param>
        /// <param name="absentmeds">выходной параметр в котором возвращается сторока недостающих медикаментов</param>
        /// <returns>если добавление произошл успешно то - true</returns>
        public static bool Apply(int serviceID, int counService, int docID, Finance finance, ref string absentmeds)
        {
            var serviceOutput = new ServiceOutput();
            serviceOutput.ServiceID_ = serviceID;
            serviceOutput.Load();//Загружаются сведения о назначенной услуге
            serviceOutput.Count = counService;//Принимается количество назначаемых услуг

            absentmeds = serviceOutput.CheckMedicamentOnSclad(counService, finance);//Проверка наличия медикаментов на складе, которые связаны с оказанием услуги
            
            if (absentmeds == "")
            {//Все необходимые медикаменты имеются на складе и можно назначать услугу пациенту
                if (p.trace == true)
                {
                    MessageBox.Show("Проверка на наличие медикаментов на складе прошла успешно."
                        , "Автооповещение"
                        , MessageBoxButtons.OK);
                }
                //Идентификатор услуги из прейскуранта
                serviceOutput.ServiceID = serviceID;
                
                //Идентификатор документа по которому назначается услуга
                serviceOutput.DocumentID = docID; 
                
                serviceOutput.Date = DateTime.Now;

                
                //Добавляется запись об услуге. Может быть заменить на метод Add?
                serviceOutput.Insert(); 
                
                //Добавляются медикаменты в расход и ставятся в резерв
                serviceOutput.OutputMedicaments(CommandDirective.Add, finance, serviceOutput.Count);
                
                return true;
            }
            
            //Некоторых медикаментов на складе не оказалось. Список дефицитных медикаментов
            //возвращается через ссылочный параметр absentmeds
            return false;
            
        }
    }
}
