﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    ///<summary>Класс: Сотрудник профилактория</summary>
    public class Member : Person
    {
        ///<summary>Внешний ключ на персону</summary>
        [pk][fk("Person", "PersonID")][db("PersonMember", Name = "PersonID", IsMain = true, Right = true)]
        public int PersonID_member { get; set; }
        ///<summary>Место работы</summary>
        [db("PersonMember")]public int FirmID { get; set; }
        ///<summary>Подразделение</summary>
        [db("PersonMember")]public int DivisionID { get; set; }
        ///<summary>Должность</summary>
        [db("PersonMember")]public int PositionID { get; set; }
        ///<summary>Рабочий телефон</summary>
        [db("PersonMember")]public string PhoneWork { get; set; }
        /// <summary>Дневной норматив объема работ работника мед. учреждения </summary>
        [db("PersonMember")]public int Norm { get; set; }
        [db("PersonMember")]public bool Deleted { get; set; }

        public Member() : base() 
        {
            FirmID = 0;
            PositionID = 0;
            Deleted = false;
            PhoneWork = "";
            Norm = 0;
        }

        ///<summary>Проверка существования сотрудника с идентификатором <code>PersonID_member</code></summary>
        public bool IsMemberExist()
        {
            DB cmd = new DB("SELECT Count(*) FROM [PersonMember] WHERE [PersonID]="
                + PersonID_member.ToString());
            return (int)cmd.ExecuteScalar() > 0;
        }

        //15.03.10
        //Чирков Е.О.
        public override string ToString()
        {
            return Fullname;
        }

        public new bool Insert()
        {
            if (Insert("Person"))
            {
                PersonID_member = PersonID;
                return InsertFull("PersonMember");
            }
            return false;
        }
    }

    ///<summary>Класс: Список сотрудников профилактория</summary>
    public class MemberList : DBOList<Member>
    {
        /// <summary>Загрузка списка сотрудников с неопределенным  значением
        /// Заплатин Е.Ф.
        /// 08.08.2011
        /// </summary>
        /// <param name="cd"></param>
        public bool Load(CommandDirective cd)
        {
            dbOrder = "LastName";
            dbWhere = "Deleted=0"; //кроме удаленных

            bool res = Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Member m = new Member();
                m.PersonID = 0;
                m.LastName = "-- Не определен --";
                this.Add(m);
            }
            return res;
        }
    }
        ///<summary>Класс: Услуга оказываемая сотрудником профилактория</summary>
    public class MemberService : DBO
    {
        [pk][db("PersonMemberService")] public int MemberServiceID { get; set; }
        ///<summary>Сотрудник профилактория</summary>
        [db("PersonMemberService")]public int MemberID { get; set; }
        ///<summary>Оказываемая услуга</summary>
        [db("PersonMemberService")]public int ServiceID { get; set; }


        public MemberService(): base()
        {
            MemberID = ServiceID = 0;
        }
    }
}
