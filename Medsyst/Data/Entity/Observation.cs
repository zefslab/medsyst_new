﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /*Наблюдение врача за пациентом в процессе всей истории болезни
     * автор: Заплатин Е.Ф.
     * создан: 05.12.09
     * изменен: 05.12.09 */
    public class Observation : DBO
    {
        [db("IllnessHistoryObservation")]
        [pk]
        public int ObservationID { get; set; }//Уникальный идентификатор наблюдения
        
        [db("IllnessHistoryObservation")]
        public int DocumentID { get; set; } //Привязка к истории болезни
        
        [db("IllnessHistoryObservation")]
        public int DoctorID { get; set; } //Лечащий врач, проводивший наблюдение. ЗЕФ. 23.12.2011
        
        [db("IllnessHistoryObservation")]
        public DateTime ReceptionD { get; set; }// дата приема у врача
        
        [db("IllnessHistoryObservation")]
        public bool boolComplaint { get; set; }// жалоба (чекбокс)
        [db("IllnessHistoryObservation")]
        public string txtComplaint { get; set; }// жалоба (коментарий)
        [db("IllnessHistoryObservation")]
        public int lstState { get; set; }// общее состояние (комбобокс)
        [db("IllnessHistoryObservation")]
        public string txtState { get; set; }// общее состояние (коментарий)
        [db("IllnessHistoryObservation")]
        public int lstBreathing { get; set; }    // Дыхание (комбобокс)
        [db("IllnessHistoryObservation")]
        public string txtBreathing { get; set; } // Частота дыхания (коментарий)
        [db("IllnessHistoryObservation")]
        public int lstCrepitation { get; set; }  // Хрипы (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstToneHeart { get; set; }    // Тоны сердца (комбобокс)
        [db("IllnessHistoryObservation")]
        public string txtToneHeart1 { get; set; }// Тоны сердца ЧСС
        [db("IllnessHistoryObservation")]
        public string txtToneHeart2 { get; set; }// Тоны сердца АД
        [db("IllnessHistoryObservation")]
        public int lstNoiseHeart1 { get; set; }  // Шумы сердца (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstNoiseHeart2 { get; set; }  // Шумы сердца (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstNoiseHeart3 { get; set; }  // Шумы сердца (комбобокс)
        [db("IllnessHistoryObservation")]
        public string txtNoiseHeart { get; set; }// Шумы сердца (коментарий)
        [db("IllnessHistoryObservation")]
        public int lstTongue { get; set; }       // Язык (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstStomachNormal { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistoryObservation")]
        public string txtStomachNormal { get; set; }// Живот нормальный (текстовое поле)
        [db("IllnessHistoryObservation")]
        public int lstStomachNormal1 { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstStomachNormal2 { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistoryObservation")]
        public int boolStomachSickly1 { get; set; }// Живот болезненный в эпигостральной области (чекбокс)
        [db("IllnessHistoryObservation")]
        public int boolStomachSickly2 { get; set; }// Живот болезненный в правом подребелье (чекбокс)
        [db("IllnessHistoryObservation")]
        public int boolStomachSickly3 { get; set; }// Живот болезненный в пакриатической зоне (чекбокс)
        [db("IllnessHistoryObservation")]
        public int boolStomachSickly4 { get; set; }// Живот болезненный в проекции желчного пузыря (чекбокс)
        [db("IllnessHistoryObservation")]
        public int boolStomachSickly5 { get; set; }// Живот болезненный в плиолородуденальной зоне (чекбокс)
        [db("IllnessHistoryObservation")]
        public int lstMendalins { get; set; } // Мендалины (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstStool { get; set; }// Стул (комбобокс)
        [db("IllnessHistoryObservation")]
        public int lstUrination { get; set; }// Мочеиспускание (комбобокс)
        [db("IllnessHistoryObservation")]
        public int boolUrination1 { get; set; }// Мочеиспускание учащено (чекбокс)
        [db("IllnessHistoryObservation")]
        public int boolUrination2 { get; set; }// Мочеиспускание болезненное (чекбокс)
        [db("IllnessHistoryObservation")]
        public string txtUrination { get; set; }// Мочеиспускание (коментарий)
        [db("IllnessHistoryObservation")]
        public string txtResult { get; set; }//Результаты наблюдения
        /// <summary>
        /// Используется в списке наблюдений
        /// </summary>
        public string Head
        {
            get
            {
                Person person = new Person(true);
                person.PersonID = DoctorID;
                person.Load();
                return ReceptionD.ToString("d") + " " + person.FIOBrif;
            }
        } 

        public Observation() { }
        public Observation(bool init) : base(init)
        {
            ReceptionD = DateTime.Now;
            boolComplaint = true;
            txtComplaint = "";
            txtState = "";
            txtBreathing = "";
            txtToneHeart1 = "";
            txtToneHeart2 = "";
            txtNoiseHeart = txtResult = "";
            txtUrination = txtStomachNormal = "";
            lstStomachNormal = 0;
            DoctorID = Security.user.UserID;//Назначается текущий доктор, который проводит наблюдение.ЗЕФ. 23.12.2011
        }

        public override string ToString()
        {
            return ReceptionD.ToString();
        }
    }
    /*Множество наблюдений врача за пациентом в процессе всей истории болезни
     * автор: Заплатин Е.Ф.
     * создан: 05.12.09
     * изменен: 05.12.09 */
    public class Observations : DBOList<Observation>
    {
        public new bool Load()
        {
            dbWhere = "";
            return base.Load();
        }

        public bool Load(int history_id)
        {
            dbWhere = "DocumentID = " + history_id;
            return base.Load();
        }

    }

    /// <summary>Класс шаблона истории болезнии, содержащий ссылку на историю болезни, которая будет взята за основу вновь создаваемой
    /// Шаблон призван облегчить заполнение истории болезни. Все поля новой истории болезни созданные на основе шаблона заполняются
    /// "нормальными" значениями
    /// Заплатин Е.Ф.
    /// 21.09.2009
    /// </summary>
    public class IllnessHistoryObservationTemplate : DBO, IObservationDermaTemplate
    {
        [db("IllnessHistoryObservationTemplate")]
        public int ObservationID { get; set; }
        /// <summary>Наименование шаблона</summary>
        [db("IllnessHistoryObservationTemplate")]
        public string Name { get; set; }
        /// <summary>дата создания шаблона</summary>
        [db("IllnessHistoryObservationTemplate")]
        public DateTime Date { get; set; }
        /// <summary>Сотрудник создавший шаблон</summary>
        [db("IllnessHistoryObservationTemplate")]
        public int EmploeeyID { get; set; }

        public IllnessHistoryObservationTemplate() { }
        public IllnessHistoryObservationTemplate(bool init)
            : base(init)
        {
            ObservationID = 0;
            EmploeeyID = 0;
            Name = "";
            Date = new DateTime(1900, 1, 1);
        }
        public new bool Insert()
        {
            return base.InsertFull("IllnessHistoryObservationTemplate");
        }

    }

    /// <summary>Колеекция шаблонов наблюдений.
    /// Заплатин Е.Ф.
    /// 12.10.2012
    /// </summary>
    public class IllnessHistoryObservationTemplateList : DBOList<IllnessHistoryObservationTemplate>
    {
        /// <summary>Загрузка историй болезней объявленных как шаблоны в обратном хронологическом порядке, чтобы выбирался наиболее свежий шаблон
        /// Заплатин 
        /// </summary>
        /// <returns></returns>
        public new bool Load()
        {
            dbOrder = "Date DESC";//Загрузка в обратном хронологическом порядке, чтобы выбирать шаблон по умолчанию самый свежий, созданный последним
            return base.Load();
        }
    }

    
}