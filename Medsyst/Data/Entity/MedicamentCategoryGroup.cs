﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;


namespace Medsyst.Class
{

    /// <summary>Класс: Группа категорий медикаментов. Используется при формировании отчета расхода медикаментов по группам
    /// Заплатин Е.ф.
    /// 03.05.2011
    /// </summary>
    public class MedicamentCategoryGroup : DBO
    {
        [db("MedicamentCategoryGroup")]
        [pk]
        public int GroupID { get; set; }
        [db("MedicamentCategoryGroup")]
        public string GroupN { get; set; }// Наименование группы категории медикамента

        public MedicamentCategoryGroup()
            : base()
        {
            GroupN = "";
        }
        /// <summary>Добавление коллекции категорий в группу.
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="mcsInGpoup">Коллекция категорий</param>
        public void AddCategories(MedicamentCategoryList mcs)
        {
            foreach (MedicamentCategory mc in mcs)
            {
                mc.GroupID = GroupID;
                mc.Update();
            }
        }
        /// <summary>Удаление  коллекции категорий из группы. Для этого достаточно обнулить свойство идентификатора группы в каждой категории коллекции.
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="mcsInGpoup">Коллекция категорий</param>
        public void DeleteCategories(MedicamentCategoryList mcs)
        {
            foreach (MedicamentCategory mc in mcs)
            {
                mc.GroupID = 0;
                mc.Update();
            }
        }
    }
    ///<summary>Коллекция:  групп категорий медикаментов
    ///Заплатин Е.ф.
    ///03.05.2011
    ///</summary>
    public class MedicamentCategoryGroups : DBOList<MedicamentCategoryGroup>
    {
        ///<summary>Загрузка групп с сортировкой по наименованиям категорий медикаментов
        ///Заплатин Е.ф.
        ///03.05.2011
        ///</summary>
        public new bool Load()
        {
            dbOrder = "GroupN";
            return base.Load();
        }
    }
}