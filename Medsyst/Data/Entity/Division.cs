﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;

namespace Medsyst.Data.Entity
{

    public class Division : DBO, ITreeItem //Подразделение
    {
        [db("Division")]
        [pk]
        public int DivisionID { get; set; }
        [db("Division")]
        public string Name { get; set; }
        [db("Division")]
        public int ParentID { get; set; }
        [db("Division")]
        public int FirmID { get; set; }
        [db("Division")]
        public string URL { get; set; }
        [db("Division")]
        public string EMail { get; set; }
        [db("Division")]
        public int leaderID { get; set; }
        [db("Division")]
        public int responsID { get; set; }
        [db("Division")]
        public int phoneID { get; set; }
        [db("Division")]
        public string ShortName { get; set; }
        [db("Division")]
        public bool Deleted { get; set; }
        [db("Division")]
        public int list_order { get; set; } // Порядок сортировки        
        public int ID { get { return DivisionID; } set { DivisionID = value; } } //ITreeItem
        public string Text
        { //для дерева отображается как "ShortName (Name)", если ShortName не пустое
            get
            {
                if (ShortName != "") return ShortName + " (" + Name + ")";
                return Name;
            }
            set { Name = value; }
        } //ITreeItem
        public bool visible { get; set; } //Признак отображения элемента в дереве
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве

        //public int ObjectTypeID { get; set; }

        public Division() { }
        public Division(bool init)
            : base(init)
        {
            Name = "";
            ParentID = 0;
            DivisionID = FirmID = 0;
            list_order = 0;
            URL = "";
            EMail = "";
            leaderID = 0;
            responsID = 0;
            phoneID = 0;
            ShortName = "";
            Deleted = false;
        }
        /// <summary>Инициализируется экземплр загрузкой из базы данных по указанному идентификатору</summary>
        /// Заплатин Е.Ф.
        /// 26.03.2013
        /// <param name="divigionID">идентификатор подразделения</param>
        public Division(int divigionID)
        {
            DivisionID = divigionID;
            Load();
        }
    }

    public class DivisionList : DBOTree<Division>
    {
        /// <summary>Загрузка коллекции с возможностью добавления дополнительной записи с неопределенного подразделения
        /// Заплатин Е.Ф.
        /// 26.03.2013
        /// </summary>
        /// <param name="addNonDefinted">директива уточняющего действия процесса загрузки коллекции</param>
        public bool Load(int FirmId, CommandDirective addNonDefinted, CommandDirective loadDeleted)
        {
            bool res = true;
            if (FirmId != 0)//Не указан идентификатор фирмы,  подразделения которой нужно загрузить
            {
                dbWhere = "FirmID = " + FirmId.ToString(); //Выбираются все подразделения указанной фирмы
                if (loadDeleted != CommandDirective.LoadDeleted)
                    dbWhere += " AND Deleted = 0";
                dbOrder = "ShortName";
                res = base.Load();
            }

            if (addNonDefinted == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Division d = new Division(true);
                d.ShortName = "Не определено";
                d.Name = "Не определено";
                d.DivisionID = 0;
                this.Add(d);
            }
            return res;
        }
    }
}
