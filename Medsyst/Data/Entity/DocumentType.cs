﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
   
    /// <summary>Класс: Тип документа</summary>
    public class DocumentType : DBO
    {
        [pk]
        [db("DocumentType")]
        public int DocTypeID { get; set; }
        [db("DocumentType")]
        public string Name { get; set; } // наименование типа документа	
        [db("DocumentType")]
        public string Comment { get; set; } // Коментарий	
        /// <summary>
        /// вид типов документов
        /// 0 - неопределенный вид
        /// 1 - расходные типы документоа
        /// 2 - приходные типы документов
        /// </summary>        
        [db("DocumentType")]
        public int Type { get; set; }

        public DocumentType() { }
        public DocumentType(bool init) : base(init) { Comment = Name = ""; Type = 0; }
    }
    /// <summary>Класс: Коллекция типов документов</summary>
    public class DocumentTypeList : DBOList<DocumentType>
    {
        public bool Load()
        {
            dbWhere = "";
            dbOrder = "Name";
            return base.Load();
        }
        public bool Load(int type)
        {
            dbWhere = "Type = " + type;
            dbOrder = "Name";
            return base.Load();
        }
    }
   
}
