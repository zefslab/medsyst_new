﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Constant;


namespace Medsyst.Class
{
    /// <summary>Класс: Медикамент, по приходному документу 
    /// </summary>
    public class MedicamentInput : Medicament
    {
        [db("MedicamentInput")]
        [pk]
        public int MedicamentInputID { get; set; }
        [fk("Medicament", "MedicamentID")]
        [db("MedicamentInput", "MedicamentID")]
        public int MedicamentID_ { get; set; }
        [db("MedicamentInput")]
        public int InputDocumentID { get; set; }
        [db("MedicamentInput")]
        public int InputDocumentTypeID { get; set; }
        [db("MedicamentInput")]
        public decimal Price { get; set; }// Стоимость медикамента согласно счет-фактуры
        [db("MedicamentInput")]
        public int Count { get; set; } // Количество медикаментов
        ///<summary>Оприходован ли медикамент на склад</summary>
        [db("MedicamentInput")]
        public bool IsOnSclad { get; set; }
        [db("MedicamentInput")]
        public string CommentInput { get; set; }
        [db("MedicamentInput")]
        public bool IsBudget { get; set; }
        public decimal PriceSum { get { return Price * Count; } }// Стоимость

        public MedicamentInput() { }
        public MedicamentInput(bool init)
            : base(init)
        {
            MedicamentInputID = MedicamentID_ = InputDocumentID = InputDocumentTypeID = 0;
            Price = 0;
            Count = 0;
            IsOnSclad = false;
            CommentInput = "";
            IsBudget = true;
        }

        public new bool Insert()
        {
            MedicamentID_ = MedicamentID;
            if (Insert("MedicamentInput"))
            {
                MedicamentInputID = Identity("MedicamentInput");
                return true;
            }
            return false;
        }
        /// <summary>Создание нового экземляра приходного медикамента не сохраненного в базе данных
        /// Заплатин Е.Ф.
        /// 15.04.2012
        /// </summary>
        /// <param name="mID">идентификатор каталожного медикамента</param>
        /// <param name="docID">идентификатор приходного документа</param>
        /// <param name="docTypeID">идентификатор типа приходного документа</param>
        public void New(int mID, int docID, int docTypeID)
        {
            //Присваивается полю возвращенный идентификатор
            MedicamentID = mID;
            //Загружается каталожный элемент по возвращенному идентификатору
            Medicament m = new Medicament(true);
            m.MedicamentID = MedicamentID;
            m.Load();
            //Указывается тип документа по которому происходило приходование медикамента. Не совсем удачное решение. В последствии нужно переработать.
            //При инициализации формы приходного ордера тип документа должен сам инициализироваться.
            InputDocumentTypeID = docTypeID;
            InputDocumentID = docID;
            //Стоимости медикамента в приходном ордере присваивается значения прайсовой цены из каталога. Это значение далее будет модифицировано пользователем и приведено в соответсвие со счетом.
            Price = m.Cost;
            //Призводится попытка добавления записи в таблицу с оприходованными медикаментами
            if (!Insert())
                MessageBox.Show("Не удалось добавить медикамент в приходный ордер");
            else
            {//Если медикамент добавился в приход , то добавляется и на склад
                if (!Debit())
                    MessageBox.Show("Не удалось оприходовать медикамент на склад");
            }
        }
        /// <summary>Удаление прихода
        /// Заплатин Е.Ф.
        /// 28.11.2011
        /// 17.11.2015 - сначал удаляется медикамент на складе из-за ограничений в БД
        /// </summary>
        /// <returns></returns>
        public bool Delete(CommandDirective directive)
        {
            if (directive == CommandDirective.DeletePermanently)
            {
                //Предварительно удаляются медикаменты и со склада
                //TODO вынести эту оерацию в сервис за пределы InputMedicament
                var moss = new MedicamentOnScladList();
                moss.Load(this);//Загрузка всех складских медикаментов по идентификатору приходного медикамента
                moss.Delete();//Удаление медикаментов со склада

                return base.Delete("MedicamentInput");//Удаление медикаментов из приходных медикаментов
            }
            else
            {//Если удаление условное, то достаточно удалить медикаменты только со склада, а у приходных медикаментов снять атрибут IsOnSclad передачи на склад
                //Примечание. При восстановлении приходного ордера медикаменты снова добавляются на склад
                IsOnSclad = false;
                return Update();
            }
        }

        public new bool Update()
        {
            MedicamentID_ = MedicamentID;
            return base.Update("MedicamentInput");
        }
        /// <summary>Передача оприходованного медикамента на склад
        /// Марьенков Е.В.
        /// </summary>
        /// <returns></returns>
        public bool Debit()
        {
            if (IsOnSclad)
            {//Медикамент уже оприходован на складе
                return false;
            }
            else
            {
                MedicamentOnSclad m = new MedicamentOnSclad(true);
                m.MedicamentInputID = MedicamentInputID;
                m.CountOnSclad = Count;
                if (m.Insert())//Добавление медикамента на склад
                {
                    IsOnSclad = true;
                    return Update();
                    //Зачем нужен последующий код, кода можно просто вызвать метод Update(). ЗЕФ. 28.11.2011
                    //MedicamentInput mmm = new MedicamentInput(true);
                    //mmm.MedicamentInputID = MedicamentInputID;
                    //mmm.Load();
                    //mmm.IsOnSclad = true;
                    //return mmm.Update();
                }
                else
                    return false;
            }
        }
        /// <summary>Проверка медикамента на предмет его расхода или резервирования
        /// Запалтин Е.Ф.
        /// 28.11.2011
        /// </summary>
        /// <returns>возвращает true если расход по медикаментам был и false если - небыло</returns>
        public bool CheckOutput()
        {
            MedicamentOnScladList moss = new MedicamentOnScladList();
            //Выбираются складские медикаменты соответсвующие приходному
            moss.dbWhere += "MedicamentOnSclad.MedicamentInputID = " + MedicamentInputID;
            //Среди них выбираются такие, у которых приход больше чем остаток т.е. медикаменты которые уже частично выданы или зарезервированы
            moss.dbWhere += " AND MedicamentInput.Count > CountOnSclad-CountReserve";

            return moss.Load();
        }
    }
    /// <summary>Коллекция: приходуемых медикаментов 
    /// </summary>
    public class MedicamentInputList : DBOList<MedicamentInput> //Коллекция медикаментов ордера
    {
        public new bool Load()
        {
            dbWhere = "";
            return base.Load();
        }
        /// <summary>Загрузка медикаментов приходного ордера
        /// Марьенков Е.В.
        /// </summary>
        /// <param name="document_id">идентификатор приходного ордера</param>
        public bool Load(int document_id)
        {
            dbOrder = "MedicamentN";
            dbWhere = "InputDocumentID=" + document_id.ToString() + " AND InputDocumentTypeID=" + (int)DocTypes.ReciptOrder;
            return base.Load();
        }
        /// <summary>Передача оприходованных медикаментов на склад
        /// Заплатин Е.Ф.
        /// 28.11.2011
        /// </summary>
        /// <returns></returns>
        public bool Debit()
        {
            bool b = true;
            foreach (MedicamentInput mi in this)
                b = b && mi.Debit();
            return b;
        }
        /// <summary>Удаление всех элементов коллекции
        /// Запалтин Е.Ф.
        /// 17.11.2011
        /// </summary>
        public bool Delete(CommandDirective directive)
        {
            //TODO если хоть один медикамент отказался быть удаленным, то нужно производить откат всей групповой операции
            bool res = true;
            foreach (MedicamentInput mi in this)
            {//Удаляется поочередно каждый элемент коллекции
                res = res && mi.Delete(directive);
            }
            //if (res) this.Clear();//Очистка коллекции после успешного удаления всех ее элементов
            return res;
        }
        /// <summary>Проверка всех оприходованных медикаментов коллекции на предмет их выдачи со склада
        /// Запалтин Е.Ф.
        /// 17.11.2011
        /// </summary>
        public bool CheckOutputMeds()
        {
            MedicamentOnScladList moss = new MedicamentOnScladList();
            moss.dbWhere = "";
            //Выбираются складские медикаменты соответсвующие приходным
            foreach (MedicamentInput mi in this)
            {
                moss.dbWhere += " AND MedicamentOnSclad.MedicamentInputID = " + mi.MedicamentInputID.ToString();
            }
            //Среди оприходованных выбираются такие, у которых приход больше чем остаток т.е. медикаменты которые уже частично выданы или зарезервированы
            moss.dbWhere += " AND MedicamentInput.Count > CountOnSclad-CountReserve";
            if (moss.dbWhere != "")
                moss.dbWhere = moss.dbWhere.Remove(0, 4);
            moss.Load();

            if (moss.Count > 0)//если найден хоть один медикамент, то приходный ордер не может быть удален
                return true;//некторое количество было реализовано со склада
            else
                return false;//Ни один медикамент не был реализован со склада
        }

    }
}