﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst.Data.Entity
{
    ///<summary>Класс: Требование на склад,  базовый класс для трех типов требований
    ///TODO Сделать класс абстрактным и переопределить все методы и своийства в дочерних классах или создать интерфейс для 
    ///этого класса в который включить переопределяемые  в дочерних классах методы
    ///Автор: Заплатин Е.Ф.
    ///Дата создания: 08.11.09
    ///Дата последнего изменения: 05.12.2010
    ///</summary>
    public class Demand : Document
    {
        [pk]
        [fk("Document", "DocumentID")]
        [db("DocDemand", IsMain = true, Right = true)]
        public int DocumentID { get; set; } //Код для связи с документом
        /// <summary>Сотрудник оформивший требование</summary>
        [db("DocDemand")]
        public int EmployeeID { get; set; }
        /// <summary>Соотнесение требования с категорией пациентов для которых выписаны материалы</summary>
        [db("DocDemand")]
        public int CategoryID { get; set; }
        /// <summary>Привязка требования к сезону</summary>
        [db("DocDemand")]
        public int SeasonID { get; set; }
        /// <summary>Сумма стоимости медикаментов</summary>
        [db("DocDemand")]
        public decimal SummMedicament { get; set; }
        /// <summary>Источник финансирования соласно которого назначаются медикаменты</summary>
        [db("DocDemand")]
        public bool IsBudget { get; set; }
        /// <summary>Признак того, что была оформлена дефектная ведомость</summary>
        [db("DocDemand")]
        public bool IsDefected { get; set; }


        public Demand() { }
        public Demand(bool init)
            : base(init)
        {
            DocumentID = 0;
            EmployeeID = 0;
            SeasonID = 0;
            CategoryID = 0;
            SummMedicament = 0;
            IsBudget = true;
            IsDefected = false;
        }
        public new bool Insert()
        {
            if (Insert("Document"))
            {
                DocumentID_ = DocumentID = Identity("Document");
                return InsertFull("DocDemand");
            }
            return false;
        }
        //TODO Переделать на технологическую загрузку. ЗЕФ.09.08 Возможно лучше перенести этот метод в класс Document с передачей ему параметра
        //типа или типов документов, номер которого нужно подсчитать. Вожможна ситуация при которой документы разных типов имеют сквозную нумерацию 
        //(например требования трех типов)
        /// <summary>Вычисление нового номера добавляемого документа</summary>
        //public int GetNewNumber()
        //{
        //    DB cmd = new DB("SELECT MAX(d.Nbr) FROM Document AS d RIGHT JOIN DocDemand as dd ON d.DocumentID=dd.DocumentID WHERE Deleted = 0");
        //    object o = cmd.ExecuteScalar();
        //    int nbr = 0;
        //    try
        //    {
        //        nbr = (int)o;
        //    }
        //    catch(InvalidCastException)
        //    {
        //        nbr = 0;
        //    }
        //    return nbr+1;
        //}
        /// <summary>Загружается коллекция медикаментов, уже ранее связанных с требованием
        /// Автор: Заплатин Е.Ф.
        /// Дата: 05.12.2010
        /// </summary>
        /// <returns>Коллекция расходуемых медикаментов</returns>
        public MedicamentByDemandList LoadMed()
        {
            MedicamentByDemandList mbds = new MedicamentByDemandList();
            mbds.Load(DocumentID, CommandDirective.NotLoadDeleted);//Загрузка медикаментов связанных с идентификатором требования
            return mbds;
        }
        /// <summary>Загружается коллекция медикаментов, уже ранее связанных с требованием
        /// Автор: Заплатин Е.Ф.
        /// Дата: 15.09.2011
        /// </summary>
        /// <param name="loadDeleted">Указывает грузить или нет удаленные записи</param>
        /// <returns>Коллекция расходуемых медикаментов</returns>
        public MedicamentByDemandList LoadMed(bool loadDeleted)
        {
            MedicamentByDemandList mbds = new MedicamentByDemandList();

            mbds.Load(DocumentID, (loadDeleted ? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted));//Загрузка медикаментов связанных с идентификатором требования
            return mbds;
        }
        /// <summary>Удаляются связанные с требованием медикаменты из таблицы OnDemandMedicine
        /// Заплатин Е.Ф.
        /// 09.12.2010
        /// 31.07.2011 Оптимизировал алгоритм ЗЕФ
        /// </summary>
        public void DeleteDemandMedicament()
        {
            if (Deleted)
            {//Требовани  уже было ранее отмечено как удаленное, поэтому производится удаление без возможности восстановления
                if (DialogResult.Yes == MessageBox.Show("Удаляется требование №" + Nbr.ToString() +
                                                        " без возможности его последующего восстановления." +
                                                        " Вы подтверждаете эту операцию? ",
                                                        "Удаление требования", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    if (DeleteMeds()) //Удаляются медикаменты выписанные по требованию. 
                    {
                        base.Delete();
                    }
                }
                else
                    return;
            }
            else
            {//Устанавливается признак удаления у требования. Медикаменты при этом не отмечаются как удаленные
                if (DialogResult.Yes == MessageBox.Show("Удалить требование №" + Nbr.ToString() +
                                                        "?", "Удаление требования", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    Deleted = true;
                    Update();
                }
            }
        }
        /// <summary>Удаляются связанные с требованием медикаменты из таблицы OnDemandMedicine и снимается резерв со склада и удаляется расход расхода
        /// Заплатин Е.Ф.
        /// 06.09.2011
        /// </summary>
        public void DeleteDemandMaterial()
        {
            OutputMedicamentList mos = new OutputMedicamentList();

            if (Deleted)
            {//Требование уже было ранее отмечено как удаленное, поэтому производится удаление без возможности восстановления
                if (DialogResult.Yes == MessageBox.Show("Удаляется требование №" + Nbr.ToString() +
                                                        " без возможности его последующего восстановления." +
                                                        " Вы подтверждаете эту операцию? ",
                                                        "Удаление требования", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {//Перманентное удаление требования без возможности восстановления


                    //Шаг 0. Снятие резерва со склада не требуется, т.к. оно было произведено при отметке требования как удаленного

                    //Шаг 1. Удаление медикаментов с расхода вместе с ранее удаленными
                    mos.Load(CommandDirective.LoadDeleted, DocumentID);//Загрузка медикаментов в расходе по текущему требованию с учетом удаленных так, чтобы не оставалось мусора в базе данных
                    if (!mos.Delete(CommandDirective.DeletePermanently))
                    {
                        MessageBox.Show("Операция удаления медикаментов из расхода завершилась неудачей. Сообщите об этом аминистратору системы",
                                                        "Ошибка удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        //TODO производить запись в журнал ошибок в базе данных
                        return;
                    }

                    //Шаг 2. Удаление медикаментов по требованию
                    if (!DeleteMeds())
                    {
                        MessageBox.Show("Операция удаления медикаментов по требованию завершилась неудачей. Сообщите об этом аминистратору системы",
                                   "Ошибка удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        //TODO производить запись в журнал ошибок в базе данных
                        //TODO нужно откатить удаление расхода используя транзакции
                        return;
                    }
                    //Шаг 3. Удаляется само требование
                    base.Delete();
                }
                else//Отказ от удаления требования
                    return;
            }
            else
            {//Устанавливается признак удаления у требования. Медикаменты при этом не отмечаются как удаленные
                if (DialogResult.Yes == MessageBox.Show("Удалить требование №" + Nbr.ToString() + "?",
                                                        "Удаление требования", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    mos.Load(CommandDirective.NotLoadDeleted, DocumentID);//Загрузка медикаментов в расходе по текущему требованию без учета удаленных

                    //Шаг 1.Снятие резерва со склада уменьшается с осхранением изменений в базе данных
                    mos.UnreservateOnSclad();

                    //Шаг 2. Требование отмечается как удаленное
                    Deleted = true;
                    Update();
                }
            }
        }
        /// <summary>Удаление связанных с требованием медикаментов
        /// </summary>
        public bool DeleteMeds()
        {
            bool res = true;
            //Загрузить  все медикаменты связанные с требованием включая отмеченные как удаленные
            MedicamentByDemandList mbds = new MedicamentByDemandList();
            mbds.Load(DocumentID, CommandDirective.LoadDeleted);//Загружается коллекция медикаментов
            if (mbds.Count > 0)
                res = mbds.Delete(CommandDirective.DeletePermanently);//Удаляются без возможности восстановления медикаменты связанные с требованием
            return res;
        }
        /// <summary>Удаление медикаментов по требованию переданных в качестве параметра
        /// Заплатин Е.Ф.
        /// 14.09.2011
        /// </summary>
        public bool DeleteMeds(ref MedicamentByDemandList mbds, CommandDirective permanently)
        {
            bool res = true;
            if (mbds.Count > 0)
                res = mbds.Delete(permanently);//Удаляются без возможности восстановления медикаменты связанные с требованием
            return res;
        }
        /// <summary>Восстановление требования отмеченного ранее как удаленное
        /// Заплатин Е.Ф.
        /// 25.07.2011
        public void Recover()
        {
            Deleted = false;
            Update();
        }
        /// <summary>Проверка остатков медикаментов на равенство выписанному по требованию количеству. 
        /// Заплатин Е.Ф.
        /// 09.09.2011
        /// </summary>
        /// <returns>возвращается наименования медикаментов, которые уже выдавались пациентам</returns>
        public string ChekDesidue()
        {
            string res = "";
            MedicamentByDemandList mbds = new MedicamentByDemandList();
            mbds.Load(DocumentID, CommandDirective.NotLoadDeleted);//Загружается список медикаментов выписанных требованию
            foreach (OnDemandMedicine mbd in mbds)
            {//Проверяется  каждый медикамент на предмет того, что некоторое его количество уже было списано по оказанным услугам
                string subres = mbd.ChekResidue();
                if (subres != "")
                {//Часть медикамента уже была выписана
                    res += subres + "; ";
                }
            }
            if (p.trace)
            {
                string txtMsg = "";
                if (res == "")
                    txtMsg = "Медикаменты выписанные по требованиям не выдавались пациентам";
                else
                    txtMsg = "Медикаменты " + res + "уже частично выданы пациентам";
                MessageBox.Show(txtMsg, "Проверка остатков медикаментов по требованиям", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return res;
        }

    }

  
    ///<summary>
    ///Класс: Требование на склад, короткая версия для формы выдачи медикаментов
    ///</summary>
    public class DemandShort : DBO
    {
        [pk]
        [db("DocDemand")]
        public int DocumentID { get; set; } //Код для связи с документом
        [db("DocDemand")]
        public int EmployeeID { get; set; } // Сотрудник оформивший требование
        [db("Document")]
        public DateTime RegisterD { get; set; } // Дата выдачи требования
        [db("Document")]
        public int Nbr { get; set; } // Номер требования
        [db("DocDemand")]
        public decimal SummMedicament { get; set; } // Сумма стоимости медикаментов
        [db("Person")]
        public string Name { get; set; } // имя
        [db("Person")]
        public string LastName { get; set; } // фамилия
        [db("Person")]
        public string SurName { get; set; } // отчество
        ///<summary>ФИО полностью</summary>
        public string Fullname { get { return LastName + " " + Name + " " + SurName; } }
        public DemandShort()
            : base()
        {
            RegisterD = new DateTime(1900, 1, 1);
        }
    }
 
    
}
