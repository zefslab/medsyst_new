﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Entity;


namespace Medsyst.Class
{
    public class City: DBO // Город 
     {
        [db("City")][pk]public int CityID { get; set; }
        [db("City")]public string CityN { get; set; }
        [db("City")]public string Comment { get; set; }

        public City() { }
        public City(bool init) : base(init) { CityID = 0; CityN = ""; Comment = ""; }
     }

     public class Cities : DBOList<City> //Коллекция городов
     {
         public new bool Load()
         {
             dbOrder = "CityN";//Сортировака загружаемого списка городов производится в алфавитном порядке по названию
             return base.Load();
         }
     }

     /// <summary>Класс: Адрес  не наследует City, а просто содержит ссылку на него.
     /// 23.03.10
     /// Чирков Е.О.
     /// </summary>
    public class Address: DBO // Адрес места расположения или места проживания
    {
        [db("Address")][pk]public int AddressID { get; set; }	//Код
        [db("Address")]public int CityID { get; set; }	//Код города
        [db("Address")]public string Location { get; set; }	//Улица, дом, офис
        [db("Address")]public string IndexNbr { get; set; }	//Почтовый индекс
        [db("Address")]public string Comment { get; set; }	//Комментарий

        public Address() { }
        public Address(bool init) : base(init) 
        {
            AddressID = 0;
            CityID = 0;
            Location = "";
            IndexNbr = "";
            Comment = "";
        }

        public new bool Insert()
        {
            if (Insert("Address"))
            {
                AddressID = Identity("Address");
                return true;
            }
            return false;
        }
    }

    ///<summary>Класс: Единица измерения</summary>
    public class Measure:DBO
    {
        [db("Measure")][pk]public int MeasureID { get; set; }	//Код
        [db("Measure")]public string ShotName { get; set; }	//Сокращенное обозначение
        [db("Measure")]public string FullName { get; set; }	//Полное наименоание

        public Measure(): base()
        {
            FullName = "";
            ShotName = "";        
        }

        public Measure(bool init): base(init) 
        {
            FullName = "";
            ShotName = "";
        }
    }

    public class Measures : DBOList<Measure>
    {
        //Чирков Е.О.
        //27.06.10
        //Перекрывает базовый метод загрузки одновременно сортируя список по короткому названию
        public new bool Load()
        {
            dbOrder = "ShotName";
            return base.Load();
        }
        //Чирков Е.О.
        //27.06.10
        //Загрузка и сортировка списока по полному названию (исп-ся в форме frmMeasure)
        public bool LoadSortByName()
        {
            dbOrder = "FullName";
            return base.Load();
        }
    }

    ///<summary>Класс настроек приложения</summary>
    public class Settings : DBO
    {
        /// <summary>Идентификатор записи настроек</summary>
        [db("Settings")][pk] public int Id { get; set; }
        /// <summary>Текущий год</summary>
        [db("Settings")]public int CurrentYear { get; set; }
        /// <summary>Идентификатор текущего сезона.</summary>
        [db("Settings")]public int CurrentSeason { get; set; }
        /// <summary>Главный врач. ЗЕФ. 04.05.2012</summary>
        [db("Settings")]public int mainDoctor { get; set; }
        /// <summary>Санаторий- профилакторий. ЗЕФ. 04.05.2012</summary>
        [db("Settings")]public int Sanatorium { get; set; }

        

        public Settings() { }
        public Settings(bool init): base(init)
        {
            Id = 1;
        }

        public new bool Insert()
        {
            return false;
        }
        /// <summary>Возвращает ФИО главного врача
        /// Заплатин Е.Ф.
        /// 04.05.2012
        /// </summary>
        public string MainDoctor()
        {
            if (mainDoctor == 0) return "";

            Person mainDoc = new Person();
            mainDoc.PersonID = mainDoctor;
            mainDoc.Load();
            return mainDoc.FIOBrif;
        }
        /// <summary>Возвращает название санатория профилактория
        /// Заплатин Е.Ф.
        /// 04.05.2012
        /// </summary>
        public string SanatoriumName()
        {
            if (Sanatorium == 0) return "";

            Firm f = new Firm();
            f.FirmID = Sanatorium;
            f.Load();
            return f.FirmN;
        }
    }
    
    ///<summary>Класс кодов МКБ
    ///Заплатин Е.Ф
    ///24.01.2012
    ///</summary>
    public class MKBCode : DBO, ITreeItem 
    {
        /// <summary>Идентификатор кода</summary>
        [db("MKBCode")][pk] public int MKBCodeID { get; set; }	
        /// <summary>Шифр кода</summary>
        //[db("MKBCode")]public string CodeMKB { get; set; }// ЗЕФ. 24.01.2012
        /// <summary>Ссылка на родительский МКБ код</summary>
        [db("MKBCode")]public int ParentID { get; set; } //Добавил  ЗЕФ. 24.01.2012
        /// <summary>Описание заболеваний по коду</summary>
        [db("MKBCode")]public string Name { get; set; }
        [db("MKBCode")]public bool Deleted { get; set; }
        /// <summary>Порядок сортировки</summary>
        [db("MKBCode")]public int list_order { get; set; }
        /// <summary>Комплексное наименование состоящее из шифра и наименования</summary>
        public string ComplexName { get { return Name; } } //Убрать 

        //Реализуются элементы интерфейса
        public int ID { get { return MKBCodeID; } set { MKBCodeID = value; } } //идентификатор элемента
        public string Text { get { return Name; } set { Name = value; } } //строковое представление элемента
        public bool visible { get; set; } //Признак отображения элемента в дереве
        public bool deleted { get { return Deleted; } set { Deleted = value; } } //Признак того, что элемент числится удаленным в дереве

        public MKBCode(): base() {
            //CodeMKB = "";
            Name = "";
        }
        public MKBCode(bool init) : base(init) {
            //CodeMKB = "";
            Name = "";
            ParentID = 0;
        }
    }
    ///<summary>Класс дерево кодов МКБ
    ///Авторы: Заплатин Е.Ф
    ///Дата создания: 17.05.2010
    ///</summary> 
    public class MKBCodeList : DBOTree<MKBCode>
    {
    }
    ///<summary>Класс: календарный месяц
    ///Авторы: Заплатин Е.Ф
    ///Дата создания: 23.03.2012
    ///</summary> 
    public class Month
    {
        public string MnthName = "";
        public string ShotName = "";
        public int Nbr = 0;

        public Month(string name, string sname, int nbr)
        { 
            MnthName = name; 
            ShotName = sname;
            Nbr = nbr; 
        }
    }
    ///<summary>Коллекция: календарных месяцев
    ///Авторы: Заплатин Е.Ф
    ///Дата создания: 23.03.2012
    ///</summary> 
    public class Months : List<Month>
    {
        public Months()
        {
            Add(new Month("январь", "янв.", 1));
            Add(new Month("февраль", "фев.", 2));
            Add(new Month("март", "март", 3));
            Add(new Month("апрель", "апрл.", 4));
            Add(new Month("май", "май", 5));
            Add(new Month("июнь", "июнь", 6));
            Add(new Month("июль", "июль", 7));
            Add(new Month("август", "авг.", 8));
            Add(new Month("сентябрь", "сент.", 9));
            Add(new Month("октябрь", "окт.", 10));
            Add(new Month("ноябрь", "нояб.", 11));
            Add(new Month("декабрь", "дек.", 12));
        }
    }

}
