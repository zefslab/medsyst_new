﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    public class Employee : Person   // Сотрудник учреждения/фирмы/организации 
    {
        [pk][fk("Person", "PersonID")][db("PersonEmployee",Name="PersonID",IsMain=false,Right=false)] 
        public int PersonID_employee {get;set;}
        [db("PersonEmployee")]public int FirmID {get;set;} // место работы
        [db("PersonEmployee")]public int DivisionID { get; set; } // подразделение
        [db("PersonEmployee")]public int PositionID {get;set;} // должность
        [db("PersonEmployee")]public string PhoneWork {get;set;} // Рабочий телефон
        
        public Employee() { }
        public Employee(bool init) : base(init) { FirmID = 0; PositionID = DivisionID = 0;
                                                  PhoneWork = ""; PersonID_employee = 0; }

        public new bool Insert()
        {
            if (Insert("Person"))
            {
                PersonID_employee = PersonID;
                return InsertFull("PersonEmployee");
            }
            return false;
        }

        public bool IsEmployeeExist()
        {
            DB cmd = new DB("SELECT Count(*) FROM [PersonEmployee] WHERE [PersonID]="
                + PersonID_employee.ToString());
            return (int)cmd.ExecuteScalar()>0;
        }
    }
    public class EmployeeList : DBOList<Employee>
    {
    }
   
    

    
}
