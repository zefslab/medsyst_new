﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst
{
    ///<summary>Группы пользователей</summary>
    public enum Groups
    {   // Значения присваиваются в нарастающем порядке начиная с единицы
        ///<summary>Администратор системы</summary>
        Admin = 1,
        ///<summary>Любой зарегистрированный пользователь</summary>
        Guest,
        ///<summary>Главный врач или доверенное лицо</summary>
        Manager,
        ///<summary>Регистратор пациентов</summary>
        Registrar,
        ///<summary>Врач-специалист</summary>
        Doctor,
        ///<summary>Медсестра</summary>
        Nurse,
        ///<summary>Управляющий складом медикаментов</summary>
        Stock,
        ///<summary>Бухгалтер</summary>
        Accountant,
        ///<summary>Программист</summary>
        Programmer
    }

    ///<summary>Группа пользователей</summary>
    public class UserGroup : DBO
    {
        ///<summary>Идентификатор группы</summary>
        [pk][db("UserGroup")]public int UGroupID { get; set; }
        ///<summary>Имя группы</summary>
        [db("UserGroup")]public string UGroupN { get; set; } //
        ///<summary>Комментарий</summary>
        [db("UserGroup")]public string Comment { get; set; } //

        public UserGroup () 
        {
            UGroupN = "";
            Comment = "";
        }

        public override string ToString()
        {
            return UGroupN;
        }
    }

    //Чирков Е.О
    //15.03.10
    //Класс для связи User и UserGroup
    public class User_UserGroup : DBO
    {
        [pk][db("PersonUser_UserGroup")]public int User_UserGroupID { get; set; }
        [db("PersonUser_UserGroup")]public int UserID { get; set; }
        [db("PersonUser_UserGroup")]public int UGroupID { get; set; }

        public User_UserGroup()
        {
            User_UserGroupID = 0;
            UserID = 0;
            UGroupID = 0;
        }
    }

    //Чирков Е.О
    //15.03.10
    public class User_UserGroups : DBOList<User_UserGroup>
    {
    }

    ///<summary>Коллекция групп пользователей </summary>
    public class UserGroups : DBOList<UserGroup>
    {}
}
