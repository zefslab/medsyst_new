﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;

namespace Medsyst.Data.Entity
{
    ///<summary>Класс: Приходный ордер для медикаментов
    ///</summary>
    public class ReceiptOrder : Document
    {
        [pk]
        [fk("Document", "DocumentID")]
        [db("DocReceiptOrder", IsMain = true)]
        public int DocumentID { get; set; }
        /// <summary>Номер счета </summary>
        [db("DocReceiptOrder")]
        public string AccountNbr { get; set; }
        /// <summary> Дата выписки счета</summary>
        [db("DocReceiptOrder")]
        public DateTime AccountD { get; set; }
        /// <summary> Источник финансирования</summary>
        [db("DocReceiptOrder")]
        public string SourceFinancing { get; set; }
        /// <summary> Поставщик медикаментов</summary>
        [db("DocReceiptOrder")]
        public int ProviderID { get; set; }
        /// <summary> Фирма поставщик медикаментов</summary>
        [db("DocReceiptOrder")]
        public int MedicalFirmID { get; set; }
        ///<summary>Медикаменты по гос. бюджету</summary>
        [db("DocReceiptOrder")]
        public bool IsBudget { get; set; }
        /// <summary> </summary>
        [db("DocReceiptOrder")]
        public decimal PriceRO { get; set; }

        public string IsBudgetString
        {
            get
            {
                if (IsBudget)
                    return "Бюджет"; //ЗЕФ: Не очень красивое решение. Эти два значения нужно снести в list.xml 
                else
                    return "Внебюджет";
            }
        }
        public ReceiptOrder() { }
        public ReceiptOrder(bool init)
            : base(init)
        {
            AccountD = new DateTime(1900, 01, 01);
            AccountNbr = SourceFinancing = "";
            DocTypeID = (int)DocTypes.ReciptOrder;
        }

        public new bool Insert()
        {
            if (Insert("Document"))
            {
                DocumentID_ = DocumentID = Identity("Document");
                return InsertFull("DocReceiptOrder");
            }
            return false;
        }
        /// <summary>Вычисляет максимальный номер из всех приходных ордеров для того, что бы увеличить номер вновь создаваемого
        /// ордера на единицу.
        /// </summary>
        public int GetNewNbr()
        {
            string where = " WHERE Deleted = 0 AND DATEPART(YEAR, RegisterD) = " + DateTime.Today.Year.ToString();// 29.02.2012. Исправлена нумерация с учетом года. ЗЕФ
            DB cmd = new DB("SELECT MAX(d.Nbr) FROM Document AS d RIGHT JOIN DocReceiptOrder as ro ON d.DocumentID=ro.DocumentID " + where);
            object o = cmd.ExecuteScalar();
            int nbr = 0;
            try
            {
                nbr = (int)o;
            }
            catch (InvalidCastException)
            {
                nbr = 0;
            }
            return nbr;
            //string sql = ini.dbSelectList.Replace("*", "MAX(Document.Nbr)");
            //DB cmd = new DB(sql);
            ////Прежде чем получать номер приходного ордера нужно проверить, возращено ли хотя бы одно значение. ЗЕФ
            //return (int)cmd.ExecuteScalar(); //возникает ошибка в том случае если не существует ни одного приходного ордера. ЗЕФ
        }

        /// <summary>Удаление приходного ордера
        /// Заплатин Е.Ф.
        /// 09.08.2011
        /// </summary>
        /// <param name="permanently"></param>
        /// <returns></returns>
        public bool Delete(CommandDirective directive)
        {
            bool res = false;
            //Проверка на наличие медикаментов, выписанных про приходному ордеру
            MedicamentInputList mis = new MedicamentInputList();
            mis.Load(this.DocumentID);
            if (mis.Count > 0)
            {//Имеются медикаменты, и нужно проверить на складе на предмет уже израсходованного количества
                if (!mis.CheckOutputMeds())
                {//расхода на основе медикаментов небыло, поэтому нужно сначала удалить медикаменты
                    mis.Delete(directive);
                }
                else
                {//если расход был то выдать предупреждение о невозможности удаления
                    MessageBox.Show("Невозможно удалить  приходный ордер №" + this.Nbr + ", медикаменты которого уже были выданы со склда.",
                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    return false;
                }
            }
            //Удаление приходного ордера
            if (CommandDirective.DeletePermanently == directive)
                res = base.Delete();
            else
            {
                Deleted = true;
                res = base.Update();
            }

            return res;
        }
        /// <summary>Наложение или снятие подписи на документ. После того как документ будет подписан, внесение изменений в него станет невозможным.
        /// Заплатин Е.Ф.
        /// 04.01.2013
        /// </summary>
        public new void Sign()
        {
            if (Signed)
            {//Реализовать проверку на предмет израсходованности медиваментов по приходному ордеру
                //если расход уже был, то подпись снмать нельзя
                if (true)//Здесь должна быть проверка на синкронность прихода с состтоянием склада по количеству медикаментов
                {
                    Signed = false;
                }
            }

            else
            {
                Signed = true;
            }
        }

    }

    
}
