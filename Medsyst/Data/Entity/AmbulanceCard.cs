﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;
using Medsyst.Data.Constant;

namespace Medsyst.Data.Entity
{
    ///<summary>/Класс: Амбулаторная карта
    ///</summary>
    public class AmbulanceCard : Document
    {
        [pk]
        [db("DocAmbulanceCard")]
        public int PatientID { get; set; } // ссылка на пациента
        [fk("Document", "DocumentID")]
        [db("DocAmbulanceCard", IsMain = true)]
        public int DocumentID { get; set; } // ссылка на документ

        public AmbulanceCard() { }
        public AmbulanceCard(bool init)
            : base(init)
        {
            DocTypeID = (int)DocTypes.AmbulaceCard;//Тип документа  - Амбулаторная карта. ЗЕФ. 04.08.2011
        }
        /// <summary>Запись данных амбулаторной карты в таблицу
        /// Заплатин Е.Ф.
        /// 05.08.2011
        /// </summary>
        public new bool Insert()
        {
            if (Insert("Document"))
            {
                DocumentID_ = DocumentID = Identity("Document");
                return InsertFull("DocAmbulanceCard");
            }
            return false;
        }
        /// <summary>Удаление амбулаторной карты
        /// Заплатин Е.Ф.
        /// 20.10.2011
        /// </summary>
        public new bool Delete()
        {
            if (Deleted)
            {
                return base.Delete();
            }
            else
            {
                Deleted = true;
                return Update();
            }
        }


    }
    /// <summary>Класс: дублер амбулаторной карты. используется для загрузки амбулаторной карты по ее идентификатору
    /// В отличии от класса AmbulanceCard который згружает по идентификатору пациента
    /// 
    /// </summary>
    public class AmbulanceCardFromDocumentID : Document
    {
        [pk]
        [fk("Document", "DocumentID")]
        [db("DocAmbulanceCard", IsMain = true)]
        public int DocumentID { get; set; } // ссылка на документ
        [db("DocAmbulanceCard")]
        public int PatientID { get; set; } // ссылка на пациента

        public AmbulanceCardFromDocumentID() { }
        public AmbulanceCardFromDocumentID(bool init) : base(init) { }

        public new bool Load()
        {
            return base.Load();
        }

    }

}
