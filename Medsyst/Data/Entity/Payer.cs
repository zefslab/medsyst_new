﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    public class Payer : Firm // Плательщик
    {
        [db("FirmPayer", Name = "FirmID", IsMain = false)]
        [fk("Firm", "FirmID")]
        public int FirmID_payer { get; set; }
        [db("FirmPayer", Name = "Comment")]
        public string PayerComment { get; set; }

        public Payer() { }
        public Payer(bool init) : base(init) { }

        public new bool Insert()
        {
            if (base.Insert("Firm"))
            {
                FirmID_payer = FirmID;
                return InsertFull("FirmPayer");
            }
            return false;
        }
    }
    public class PayerList : DBOList<Payer>
    {
    }

}
