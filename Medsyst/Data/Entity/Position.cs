﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    public class Position : DBO
    {
        [db("Position")]
        [pk]
        public int PositionID { get; set; }
        [db("Position")]
        public string PositionN { get; set; }
        [db("Position")]
        public string Comment { get; set; }

        public Position() { }
        public Position(bool init)
            : base(init)
        {
            Clear();
            PositionID = 0;
            PositionN = Comment = "";
        }

    }

    public class PositionList : DBOList<Position>
    {
        /// <summary>Загрузка должностей с или без учета неопределенной должности
        /// Заплатин Е.Ф.
        /// 03.01.2012
        /// </summary>
        /// <returns></returns>
        public new bool Load()
        {
            dbOrder = "PositionN";
            bool res = base.Load();
            if (addnondefined != "")
            {
                Position p = new Position(true);
                p.PositionN = addnondefined;
                p.PositionID = 0;
                this.Add(p);
            }
            return res;

        }
    }
}
