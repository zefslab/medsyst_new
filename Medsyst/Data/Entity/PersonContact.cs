﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class;

namespace Medsyst.Data.Entity
{
    //05.04.10
    //Чирков Е.О.
    public class PersonContact : Employee
    {
        [pk]
        [db("PersonContact", IsMain = false)]
        [fk("PersonEmployee", "PersonID")]
        public int PersonContactID { get; set; }
        [db("PersonContact")]
        public string Position { get; set; }

        public PersonContact() : base() { }
        public PersonContact(bool init) : base(init) { PersonContactID = 0; Position = ""; }

        public new bool Insert()
        {
            if (base.Insert())
            {
                PersonContactID = PersonID;
                return base.InsertFull("PersonContact");
            }
            return false;
        }
    }
}
