﻿using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
  
    
    public class  Firm: DBO // Фирма  (абстрактный класс)	
    {
        [db("Firm")][pk]public int FirmID { get; set; }
        [db("Firm")]public string FirmN { get; set; }
        [db("Firm")]public int FormID { get; set; }	//Код формы собственности
        [db("Firm")]public string Phone { get; set; } //Телефон
        [db("Firm")]public int AdressID { get; set; }
        [db("Firm")]public int BankDetailsID { get; set; }
        [db("Firm")]public string Comment { get; set; }
        //27.03.10
        //Чирков Е.О.
        [db("Firm")]public string Fax { get; set; }
        [db("Firm")]public string EMail { get; set; }
        [db("Firm")]public string URL { get; set; }
        //05.04.10
        //Чирков Е.О.
        [db("Firm")]public bool Deleted { get; set; }

        public Address IspAdress; //Физический адрес
        public BankDetails Bank; // банковские реквизиты

        public Firm() { }
        public Firm(bool init) : base(init) 
        {
            FirmID = 0;
            FormID = 0;
            AdressID = 0;
            BankDetailsID = 0;
            Comment = "";
            Phone = "";
            FirmN = "";
            Fax = "";
            EMail = "";
            URL = "";
            Deleted = false;
        }
    }

    public class FirmList : DBOList<Firm>
    {
    }

    public class BankDetails : DBO //Банковские реквизиты
    {
        [pk]
        [db("BankDetails")]
        public int BankDetailsID { get; set; }
        [db("BankDetails")]
        public string Adress { get; set; } //Юридический адрес
        [db("BankDetails")]
        public string RS { get; set; } //Расчетный счет
        [db("BankDetails")]
        public string INN { get; set; } //ИНН
        [db("BankDetails")]
        public string KPP { get; set; } //КПП
        [db("BankDetails")]
        public string OKOHN { get; set; } //ОКОХН
        [db("BankDetails")]
        public string OKPD { get; set; } //ОКПД
        [db("BankDetails")]
        public string OKPO { get; set; } //ОКПО
        [db("BankDetails")]
        public string OKPF { get; set; } //ОКПФ
        [db("BankDetails")]
        public string BankName { get; set; } //Наименование банка
        [db("BankDetails")]
        public string KorS { get; set; } //кор. счет банка
        [db("BankDetails")]
        public string BIK { get; set; } //БИК
    }
    public class FirmForm : DBO  // Форма собственности	
    {
        [pk]
        [db("FirmForm")]
        public int FormID { get; set; }
        [db("FirmForm")]
        public string ShotName { get; set; }
        [db("FirmForm")]
        public string FullName { get; set; }
    }

   
   
 
}
