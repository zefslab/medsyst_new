﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;


namespace Medsyst.Class
{
    ///<summary>Класс: Медикамент в каталоге
    ///</summary>
    public class Medicament : DBO
    {
        [db("Medicament")][pk]public int MedicamentID { get; set; }
        /// <summary>Наименование медикамента</summary>
        [db("Medicament")]public string MedicamentN { get; set; }
        //public Measure MedMeasure = null; // код единицы измерения. Закоментировал ЗЕФ 23.08.2011
        /// <summary>код единицы измерения</summary>
        [db("Medicament")]public int MeasureID { get; set; }
        /// <summary>ориентировочная стоимость медикамента на рынке</summary>
        [db("Medicament")]public decimal Cost { get; set; }
        /// <summary>иповой рецепт к медикаменту (в последствии лучше переименовть поле и произвести рефакторинг)</summary>
        [db("Medicament")]public string Comment { get; set; }
        /// <summary>Категоряи к которой относиться медикамент и по которой группируется в дереве категорий</summary>
        [db("Medicament")]public int CategoryID { get; set; }
        /// TODO: надо продумать механизм хранения картинки, где, в БД или на локалной машине?
        ///<summary>Путь к файлу картинки</summary>
        [db("Medicament")]public string FileImage { get; set; }
        /// <summary>Признак использования медикамента при назначении услуги. ЗЕФ. 07.01.2012</summary>
        [db("Medicament")]public bool Deleted { get; set; }
        /// <summary>Признак того, что ноыый экземпляр добавлен в коллекцию, но еще не сохранен в базе.
        /// Это поле используется всеми наследниками.
        /// Заплатин Е.Ф. 17.08.2011</summary>
        public bool NotSaved;
        /// <summary>Признак того, что в экземпляр внесены изменения, но они еще не сохранены в базе. 
        /// Это поле используется всеми наследниками.
        /// Заплатин Е.Ф. 17.08.2011</summary>
        public bool Modified;
        
        public Medicament () { }
        public Medicament(bool init) : base(init) 
        {
            MedicamentID = 0;
            MeasureID  = 1;
            CategoryID = 0;
            Cost = 0;
            MedicamentN = "";
            Comment = "";
            FileImage = "";
            Deleted = false;
            NotSaved = false;
            Modified = false;
        }
        /// <summary>Внесены изменения в свойства экземпляра
        /// Заплатин Е.Ф.
        /// 17.08.2011
        /// </summary>
        public void Modifie()
        {
            Modified = true;
        }
    }
    /// <summary>Класс: Коллекция медикаментов каталога
    /// </summary>
    public class MedicamentList : DBOList<Medicament> 
    {
        public new bool Load()
        {
            dbWhere = "Deleted=0";
            return base.Load();
        }
        
        public bool Load(int CategoryID)
        {
            dbWhere = "(CategoryID = "+ CategoryID + ") AND (Deleted=0)";
            return base.Load();
        }

        public bool Load(int CategoryID,decimal price1, decimal price2)
        {
            dbWhere = "Cost>=" + price1 + " AND Cost<=" + price2 + " AND CategoryID = "+ CategoryID;
            return base.Load();
        }
    }
}
