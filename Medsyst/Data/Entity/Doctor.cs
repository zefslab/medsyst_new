﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Class 
{
    /// <summary>
    /// Лечащий врач
    /// </summary>
    public class Doctor : Employee 
{
    //в качестве базовой намеренно указана таблица Person, а не PersonEmployee. В противном случае выбираются не врачи, а все персоны.
    //В этом нужно разобраться с Марьенковым. Не помогают даже атрибуты IsMain и  Right
    [pk][fk("Person", "PersonID")][db("PersonDoctor")]public int DoctorID { get; set; }//, IsMain = true, Right = true
    [db("PersonDoctor")]public int SpecializationID { get; set; } // специализация врача
    [db("PersonDoctor")]public string RoomNbr { get; set; } // номер кабинета
        
    public Doctor() { }
    public Doctor(bool init) : base(init) 
    {
        SpecializationID = 1;
    }
    /// <summary>
    /// Выясняет, является текущий пользователь доктором или нет. 
    /// Метод объявлен статическим, чтобы можно было его вызывать без создания экземпляра объекта
    /// Автор: Заплатин Е.Ф.
    /// </summary>
    /// <returns></returns>
        public static bool isdoctor()
    {
        Doctors drs = new Doctors();//TODO докторов выявлять из пользователей групп
        drs.Load(); //загружается список всех терапевтов
        /// перебираются все терапевты и если находится совпадение по идентификатору то 
        /// это значит текущий пользователь является терапевтом
        foreach (Doctor dr in drs)
        {
            //MessageBox.Show("Доктор из списка: " + dr.DoctorID + "Текущий пользователь: " + Security.user.PersonID);
            if (dr.DoctorID == Security.user.PersonID)//Сравнивается идентификатор доктора из списка с идентификатором текущего пользователя
            {
                //MessageBox.Show("Текущий пользователь - доктор.");
                return true;
            }
        }
        //MessageBox.Show("Текущий пользователь не является доктором");
        return false;
    }
}
public class Doctors : DBOList<Doctor>
{
    /// <summary>Загрузка списка докторов с неопределенным значением для тех записей, у которых свойство лечащего доктора еще не определено
    /// Заплатин Е.Ф.
    /// 10.04.2012
    /// </summary>
    /// <param name="cd">директива</param>
    /// <returns></returns>
    public bool Load(CommandDirective cd)
    {
        bool res = base.Load();
        if (cd == CommandDirective.AddNonDefined)
        {//Требуется добавить запись с непоределенным значением
            Doctor d = new Doctor(true);
            d.DoctorID = 0;
            d.LastName = "не определен";
            this.Add(d);
        }
        return res;
    }
}
public class Specialization : DBO   // Специализация лечащего врача
{
    [pk][db("PersonDoctorSpecialization")]public int SpecializationID { get; set; }
    [db("PersonDoctorSpecialization")]public int SpecializationN { get; set; }
    [db("PersonDoctorSpecialization")]public string Comment { get; set; }

    public Specialization() { }
    public Specialization(bool init) : base(init) { }

}   
}
