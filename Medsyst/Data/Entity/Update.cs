﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace aisCore.Class
{
    public class Update
    {
        public Version version { get; set; }
        public List<string> Files { get; set; }
        public string WhatsNew { get; set; }
        public long Size { get; set; }

        private Update()
        {
            version = new Version();
            Files = new List<string>();
            WhatsNew = "";
            Size = 0;
        }

        static void ParseVersionString(string s, ref Update u) //#1.0.0, xxxx.yyy, yyyy.xxx
        {
            int p = s.IndexOf(',');
            if (p < 0) return;
            //версия
            string vs = s.Substring(1, p - 1);
            u.version = new Version(vs);
            //файлы
            while (s.Length > 0)
            {
                s = s.Substring(p + 1);
                s = s.Trim();
                p = s.IndexOf(',');
                if (p < 0)
                {
                    u.Files.Add(s);
                    break;
                }
                else u.Files.Add(s.Substring(0, p));
            }
        }

        public static Update FromTextFile(string FileName)
        {
            if (!File.Exists(FileName)) return null;
            StreamReader rd = new StreamReader(FileName, Encoding.Default);

            Update res = new Update();
            string s = "";
            Update u = null;
            while (!rd.EndOfStream)
            {
                s = rd.ReadLine();
                if ((s == "") || (s[0] == ';')) continue; //комментарий
                if (s[0] == '#') //описатель очередной версии
                {
                    u = new Update();
                    ParseVersionString(s, ref u);
                    if (res.version < u.version) res.version = u.version;
                    foreach (string fn in u.Files)
                    {
                        string lfn = fn.ToLower();
                        if (res.Files.IndexOf(lfn) < 0) res.Files.Add(lfn);
                        string fulln = Path.Combine(Path.GetDirectoryName(FileName), fn);
                        if (File.Exists(fulln))
                        {
                            FileStream fs = File.Open(fulln, FileMode.Open);
                            res.Size += fs.Length;
                            fs.Close();
                        }
                    }
                }
                else
                {
                    res.WhatsNew += s + "\n";
                }
            }
            rd.Close();
            return res;
        }
    }
}
