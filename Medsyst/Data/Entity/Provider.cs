﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;

namespace Medsyst.Data.Entity
{
    /// <summary>
    /// Фирма-поставщик медикаментов
    /// </summary>
    public class Provider : Firm
    {
        [pk]
        [fk("Firm", "FirmID")]
        [db("FirmProvider", Name = "FirmID", IsMain = false)]
        public int FirmID_provider { get; set; }
        [db("FirmProvider")]
        public string CommentProvider { get; set; }

        public Provider() : base() { }
        public Provider(bool init) : base(init) { FirmID_provider = 0; CommentProvider = ""; }

        public new bool Insert()
        {
            if (base.Insert("Firm"))
            {
                FirmID_provider = FirmID;
                return InsertFull("FirmProvider");
            }
            return false;
        }
    }
    public class ProviderList : DBOList<Provider>
    {
        /// <summary>Загрузка коллекции с возможностью добавления дополнительной записи с неопределенного поставщика
        /// Заплатин Е.Ф.
        /// 31.10.2011
        /// </summary>
        /// <param name="cd">директива уточняющего действия процесса загрузки коллекции</param>
        public bool Load(CommandDirective cd)
        {
            dbOrder = "FirmN";
            bool res = base.Load();
            if (cd == CommandDirective.AddNonDefined)
            {//Требуется добавить запись с непоределенным значением
                Provider p = new Provider(true);
                p.FirmN = "Не определен";
                p.FirmID = 0;
                this.Add(p);
            }
            return res;
        }
    }


}
