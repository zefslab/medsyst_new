﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.Data.Constant;

namespace Medsyst.Class
{
    /// <summary>Класс шаблона истории болезнии, содержащий ссылку на историю болезни, которая будет взята за основу вновь создаваемой
    /// Шаблон призван облегчить заполнение истории болезни. Все поля новой истории болезни созданные на основе шаблона заполняются
    /// "нормальными" значениями
    /// Заплатин Е.Ф.
    /// 21.09.2009
    /// </summary>
    public class IllnessHistoryTemplate : DBO
    {
        [db("IllnessHistoryTemplate")]
        public int DocumentID { get; set; }
        /// <summary>Наименование шаблона</summary>
        [db("IllnessHistoryTemplate")]
        public string Name { get; set; }
        /// <summary>дата создания шаблона</summary>
        [db("IllnessHistoryTemplate")]
        public DateTime Date { get; set; }
        /// <summary>Сотрудник создавший шаблон</summary>
        [db("IllnessHistoryTemplate")]
        public int EmploeeyID { get; set; }
        /// <summary>Принадлежность шаблона по половому признаку</summary>
        [db("IllnessHistoryTemplate")]
        public bool Sex { get; set; }

        public IllnessHistoryTemplate() { }
        public IllnessHistoryTemplate(bool init) : base(init)
        {
            DocumentID = 0;
            EmploeeyID = 0;
            Name = "";
            Date = new DateTime(1900, 1, 1);
            Sex = true;
        }
        public new bool Insert()
        {
            return base.InsertFull("IllnessHistoryTemplate");
        }

    }

    /// <summary>Колеекция шаблонов истории болезнней.
    /// Заплатин Е.Ф.
    /// 21.09.2009
    /// </summary>
    public class IllnessHistoryTemplateList : DBOList<IllnessHistoryTemplate>
    {
        /// <summary>Загрузка историй болезней объявленных как шаблоны в обратном хронологическом порядке, чтобы выбирался наиболее свежий шаблон
        /// Заплатин 
        /// </summary>
        /// <returns></returns>
        public new bool Load(bool sex)
        {
            dbWhere = "Sex = " + (sex ? "1" : "0");//Загрузка женских или мужских шаблонов в зависимости от пола пациента
            dbOrder = "Date DESC";//Загрузка в обратном хронологическом порядке, чтобы выбирать шаблон по умолчанию самый свежий, созданный последним
            return base.Load();
        }
    }
    /// <summary>Тип направления из лечебного учреждения в профилакторий: путевка, курсовка, платная основа.
    /// Заплатин Е.Ф.
    /// 12.05.2013
    /// </summary>
    public class DirectionType : DBO
    {
        [pk]
        [db("DirectionType")]
        public int TypeID { get; set; }
        [db("DirectionType")]
        public string TypeN { get; set; }

        public DirectionType() { }
        public DirectionType(bool init) : base(init)
        {
            TypeID = 0;
            TypeN = "";
        }
    }
    public class DirectionTypeList : DBOList<DirectionType>
    {
        public bool Load()
        {
            bool res = base.Load();
            if (addnondefined != "")
            {
                DirectionType dt = new DirectionType(true);
                dt.TypeN = addnondefined;
                dt.TypeID = 0;
                this.Add(dt);
            }
            return res;



        }
    }

    /// <summary>Класс истории болезни
    /// Марьенков Е.В., Заплатин Е.Ф.
    /// </summary>
    public class IllnessHistory : Document
    {
        [db("IllnessHistory")]
        [pk]
        [fk("Document", "DocumentID")]
        public int DocumentID { get; set; }
        [db("IllnessHistory")]
        public int AmbCardID { get; set; }// привязка к амбулаторной карте пациента
                                          //Общие сведения
        [db("IllnessHistory")]
        public int NbrIH { get; set; }// номер истроии болезни
        [db("IllnessHistory")]
        public DateTime BeginD { get; set; } // Дата начала путевки
        [db("IllnessHistory")]
        public DateTime EndD { get; set; }// дата окончания путевки
        [db("IllnessHistory")]
        public int SeasonID { get; set; }// номер сезона
        [db("IllnessHistory")]
        public int TypeIH { get; set; }// тип путевки или курсовки или платной основы
        [db("IllnessHistory")]
        public string TypeNbr { get; set; }// номер путевки или курсовки или платной основы
        [db("IllnessHistory")]
        public int DoctorID { get; set; }// лечащий врач

        //Закладка Общая
        [db("IllnessHistory")]
        public string DiagOut { get; set; }// диагноз с места отбора
        [db("IllnessHistory")]
        public int DiagOutMKB { get; set; }// код диагноза с места отбора
        [db("IllnessHistory")]
        public string DiagAttend { get; set; }// Диагноз профилактория при поступлении
        [db("IllnessHistory")]
        public int DiagAttendMKB { get; set; }// Код диагноза профилактория при поступлении
        [db("IllnessHistory")]
        public string DiagMain { get; set; }//Диагноз профилактория при выписке
        [db("IllnessHistory")]
        public int DiagMainMKB { get; set; }//Код диагноза профилактория при выписке


        //Закладка Жалобы больного
        [db("IllnessHistory")]
        public string Complaints { get; set; }// жалобы больного
                                              //Закладка История заболевания
        [db("IllnessHistory")]
        public string HistiryOffIllness { get; set; }// история заболевания
                                                     //Закладка Антрометрические данные
        [db("IllnessHistory")]
        public string txtHeightIn { get; set; }//Рост при поступлении
        [db("IllnessHistory")]
        public string txtWeightIn { get; set; }//Вес при поступлении
                                               /// <summary>
                                               /// Динамометрию заменили на пульсоксиметрию 1.04.2015 ЗЕФ Issues #6
                                               /// </summary>
        [db("IllnessHistory")]
        public string txtDynamomentyIn { get; set; }
        /// <summary>
        /// Спирометрию заменили на пикфлоуметрию 01.04.2015 ЗЕФ Issues #6
        /// </summary>
        [db("IllnessHistory")]
        public string txtSpirometriaIn { get; set; }
        [db("IllnessHistory")]
        public string txtDynamomentyOut { get; set; }//Динамометрия при выписке
        [db("IllnessHistory")]
        public string txtSpirometriaOut { get; set; }//Спирометрия при поступлении
        [db("IllnessHistory")]
        public string txtPulseIn { get; set; }//Пульс при поступлении
        [db("IllnessHistory")]
        public string txtArterialIn { get; set; }//Артериальное давление при поступлении
        [db("IllnessHistory")]
        public string txtHeightOut { get; set; }//Рост при выписке
        [db("IllnessHistory")]
        public string txtWeightOut { get; set; }//Вес при выписке

        [db("IllnessHistory")]
        public string txtPulseOut { get; set; }//Пульс при выписке
        [db("IllnessHistory")]
        public string txtArterialOut { get; set; }//Артериальное давление при выписке
                                                  //Закладка Общий анамнез
        [db("IllnessHistory")]
        public bool boolInherit { get; set; }//Наследственность
        [db("IllnessHistory")]
        public string txtInherit { get; set; }//Наследственность
        [db("IllnessHistory")]
        public bool boolDisorder { get; set; }//Профессиональные вредности и перенесенные заболевания
        [db("IllnessHistory")]
        public string txtDisorder { get; set; }//Профессиональные вредности и перенесенные заболевания
        [db("IllnessHistory")]
        public bool boolTrauma { get; set; }//Нервнопсихические травмы, контузии
        [db("IllnessHistory")]
        public string txtTrauma { get; set; }//Нервнопсихические травмы, контузии
        [db("IllnessHistory")]
        public string txtBeginWork { get; set; }//Начало трудовой деятельности
        [db("IllnessHistory")]
        public int lstWorkForm { get; set; }//Работа
        [db("IllnessHistory")]
        public int lstMeals { get; set; }//Питание
        [db("IllnessHistory")]
        public int lstMaelsForm { get; set; }//Фон питания
        [db("IllnessHistory")]
        public int lstHousingCondition { get; set; }//Условия проживания
        [db("IllnessHistory")]
        public int lstFamilyStatus { get; set; }//Семейное положение
        [db("IllnessHistory")]
        public int lstUnhialthy { get; set; }//Вредные привычки
        [db("IllnessHistory")]
        public bool boolMenstrual { get; set; }//Менструальные циклы
        [db("IllnessHistory")]
        public int intMenstrualYear { get; set; }//Количество лет
        [db("IllnessHistory")]
        public int intMenstrualDays { get; set; }//Количество дней
        [db("IllnessHistory")]
        public int intMenstrualDaysFrom { get; set; }//Через какое количество дней повторяются
        [db("IllnessHistory")]
        public int lstMenstrualForm { get; set; }//Форма менструаций
        [db("IllnessHistory")]
        public bool boolPregnancy { get; set; }//Беременность
        [db("IllnessHistory")]
        public int intPregnancy { get; set; }//Количество беременностей
        [db("IllnessHistory")]
        public bool boolBirth { get; set; }//Роды
        [db("IllnessHistory")]
        public int intBirth { get; set; }//Количество родов
        [db("IllnessHistory")]
        public bool boolAbortion { get; set; }//Аборты
        [db("IllnessHistory")]
        public int intAbortion { get; set; }//Количество абортов
        [db("IllnessHistory")]
        public bool boolGenecologyHarmfulness { get; set; }//Генекологические заболевания
        [db("IllnessHistory")]
        public string txtGenecologyHarmfulness { get; set; }//Генекологические заболевания
                                                            //Закладка Объективное исследование
        [db("IllnessHistory")]
        public int lstFigure { get; set; }//Телосложение список
        [db("IllnessHistory")]
        public string txtFigure { get; set; }//Телосложение текстовое поле
        [db("IllnessHistory")]
        public int lstMeals2 { get; set; }//Питание список
        [db("IllnessHistory")]
        public string txtMeals2 { get; set; }//Питание текст
        [db("IllnessHistory")]
        public int lstSkin { get; set; }//Кожные покровы
        [db("IllnessHistory")]
        public bool boolSkinDryness { get; set; }//Сухость кожных покровов
        [db("IllnessHistory")]
        public bool boolSkinRash { get; set; }//Высыпание на кожных покровах
        [db("IllnessHistory")]
        public string txtSkin { get; set; }//Кожные покровы
        [db("IllnessHistory")]
        public int lstMucuos { get; set; }//Слизистые
        [db("IllnessHistory")]
        public string txtMucuos { get; set; }//Слизистые текстовые
        [db("IllnessHistory")]
        public int lstGland { get; set; }//Щитовидная железа
        [db("IllnessHistory")]
        public int lstMuscular { get; set; }//Костно-мышечная система
        [db("IllnessHistory")]
        public string txtMuscular { get; set; }//Костно-мышечная система текст
                                               //Закладка Течение болезни со списком наблюдений врача и характеристиками по каждому наблюдению
                                               //Поля реализованы в классе Obcervation
                                               //Закладка Сердечно-сосудистая система
        [db("IllnessHistory")]
        public int lstPulse1IH { get; set; }//Пульс 
        [db("IllnessHistory")]
        public int intPulseRateIH { get; set; }//Частоста пульса
        [db("IllnessHistory")]
        public int lstPulse2IH { get; set; }//Пульс
        [db("IllnessHistory")]
        public int lstHeartToneIH { get; set; }//Тоны сердца
        [db("IllnessHistory")]
        public int lstHeartTone1IH { get; set; }//Тон сердца 1 тон на верхушке
        [db("IllnessHistory")]
        public int lstHeartTone2IH { get; set; }//Тон сердца 2 тон
        [db("IllnessHistory")]
        public string txtHeartToneIH { get; set; }//Тон сердца описние
        [db("IllnessHistory")]
        public int lstHeartNoiseIH { get; set; }//Шум сердца
        [db("IllnessHistory")]
        public int lstHeartNoise1IH { get; set; }//Шум сердца 
        [db("IllnessHistory")]
        public int lstHeartNoise2IH { get; set; }//Шум сердца 
        [db("IllnessHistory")]
        public string txtHeartNoiseIH { get; set; }//Шум сердца описние
        [db("IllnessHistory")]
        public int lstArteryPulseIH { get; set; }//Артерии нижних конечностей: пульсация
        [db("IllnessHistory")]
        public string txtArteryPulseIH { get; set; }//Артерии нижних конечностей: описание 
        [db("IllnessHistory")]
        public string txtArterialPressureLeftIH { get; set; }//Артериальное давление слева
        [db("IllnessHistory")]
        public string txtArterialPressureRightIH { get; set; }//Артериальное давление с права
                                                              //Закладка Органы дыхания
        [db("IllnessHistory")]
        public int lstSoundIH { get; set; }//При сравнительной перекуссии звук 
        [db("IllnessHistory")]
        public string txtSoundIH { get; set; }// При сравнительной перекуссии звук: коментарий
        [db("IllnessHistory")]
        public int lstBreathingIH { get; set; }// Аускультативно дыхание
        [db("IllnessHistory")]
        public string txtBreathingIH { get; set; }// Аускультативно дыхание: коментарий
        [db("IllnessHistory")]
        public int lstCracklingIH { get; set; }// Хрипы
        [db("IllnessHistory")]
        public string txtCracklingIH { get; set; }// Хрипы: коментарий
                                                  //Закладка Органы пищеварения
        [db("IllnessHistory")]
        public int lstTongue { get; set; }//Язык
        [db("IllnessHistory")]
        public string txtTongue { get; set; }// Язык: коментарий
        [db("IllnessHistory")]
        public int lstTooths { get; set; }// Зубы
        [db("IllnessHistory")]
        public string txtTooths { get; set; }// Зубы: коментарий
        [db("IllnessHistory")]
        public int lstMendalins { get; set; }// Мендалины
        [db("IllnessHistory")]
        public string txtMendalins { get; set; }// Мендалины: коментарий
        [db("IllnessHistory")]
        public int lstStomachNormal { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistory")]
        public string txtStomachNormal { get; set; }// Живот нормальный (текстовое поле)
        [db("IllnessHistory")]
        public int lstStomachNormal1 { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistory")]
        public int lstStomachNormal2 { get; set; }// Живот нормальный (комбобокс)
        [db("IllnessHistory")]
        public bool boolStomachSickly1 { get; set; }// Живот болезненный в эпигостральной области (чекбокс)
        [db("IllnessHistory")]
        public bool boolStomachSickly2 { get; set; }// Живот болезненный в правом подребелье (чекбокс)
        [db("IllnessHistory")]
        public bool boolStomachSickly3 { get; set; }// Живот болезненный в пакриатической зоне (чекбокс)
        [db("IllnessHistory")]
        public bool boolStomachSickly4 { get; set; }// Живот болезненный в проекции желчного пузыря (чекбокс)
        [db("IllnessHistory")]
        public bool boolStomachSickly5 { get; set; }// Живот болезненный в плиолородуденальной зоне (чекбокс)
        [db("IllnessHistory")]
        public int lstLiver1 { get; set; }// Печень (комбобокс)
        [db("IllnessHistory")]
        public string txtLiver1 { get; set; }// Печень, см
        [db("IllnessHistory")]
        public int lstLiver2 { get; set; }// Печень (комбобокс)
        [db("IllnessHistory")]
        public int lstLiver3 { get; set; }// Печень (комбобокс)
        [db("IllnessHistory")]
        public int lstLiver4 { get; set; }// Печень (комбобокс)
        [db("IllnessHistory")]
        public string txtLiver2 { get; set; }// Печень: комментарий
        [db("IllnessHistory")]
        public int lstSpleen { get; set; }// Селезенка (комбобокс)
        [db("IllnessHistory")]
        public string txtSplleen { get; set; }// Селезенка: комментарий
        [db("IllnessHistory")]
        public int lstStool1 { get; set; }// Стул (комбобокс)
        [db("IllnessHistory")]
        public int lstStool2 { get; set; }// Стул (комбобокс)

        //Закладка Мочеполовая система
        [db("IllnessHistory")]
        public int lstUrination { get; set; }//Мочеиспускание 
        [db("IllnessHistory")]
        public bool boolMoroFrequent { get; set; }// Учащено
        [db("IllnessHistory")]
        public bool boolPainfully { get; set; }// Болезненное
        [db("IllnessHistory")]
        public string txtUrination { get; set; }// Мочеиспускание: коментарий
                                                //Закладка Органы опоры и движения
        [db("IllnessHistory")]
        public bool boolSupportMotion { get; set; }// без паталогии
        [db("IllnessHistory")]
        public string txtSupportMotion { get; set; }// Органы опоры и движения: описание
                                                    //Закладка Нервная система
        [db("IllnessHistory")]
        public bool boolNoPeculiarity { get; set; }// без особенностей
        [db("IllnessHistory")]
        public string txtNervose { get; set; }// Нервная система: описание
                                              //Закладка Назначения
        [db("IllnessHistory")]
        public decimal SummService { get; set; }//Сумма стоимости услуг
        [db("IllnessHistory")]
        public decimal SummMedicament { get; set; }//Сумма стоимости медикаментов
        public decimal Summ { get { return SummService + SummMedicament; } }//Сумма стоимости услуг и медикаментов
                                                                            //Закладка Заключительный прием
        [db("IllnessHistory")]
        public int lstSensation { get; set; }//Самочувствие 
        [db("IllnessHistory")]
        public string txtSensation { get; set; }// Самочувствие: коментарий
        [db("IllnessHistory")]
        public int lstIntensification { get; set; }// Обострение
        [db("IllnessHistory")]
        public string txtIntensification { get; set; }// Обострение: коментарий
        [db("IllnessHistory")]
        public bool boolOtherDinamic { get; set; }// другой динамики нет - 1, есть - 0
        [db("IllnessHistory")]
        public string txtOtherDinamic { get; set; }// Другая динамика
        [db("IllnessHistory")]
        public string txtObjectiveData { get; set; }// Объективные данные
        [db("IllnessHistory")]
        public int lstResult { get; set; }// Исход
        [db("IllnessHistory")]
        public string txtResult { get; set; }// Исход: коментарий
        [db("IllnessHistory")]
        public string txtDoctorRecomendation { get; set; }//Лечебные и профилактические советы врача при выписке

        public IllnessHistory() { }
        public IllnessHistory(bool init) : base(init)
        {
            BeginD = new DateTime(1900, 1, 1);
            EndD = new DateTime(1900, 1, 1);
            DiagOut = DiagMain = DiagAttend = Complaints = "";
            HistiryOffIllness = "";
            txtHeightIn = txtWeightIn = txtHeightOut = txtWeightOut = "";
            txtPulseIn = txtPulseOut = txtArterialIn = txtArterialOut =
            txtInherit = "";
            txtDynamomentyIn = txtDynamomentyOut = txtSpirometriaIn = txtSpirometriaOut = "";
            txtDisorder = "";
            txtTrauma = "";
            txtBeginWork = "";
            txtGenecologyHarmfulness = "";
            txtFigure = "";
            txtMeals2 = "";
            txtSkin = "";
            txtMucuos = "";
            txtMuscular = "";
            txtHeartToneIH = txtHeartNoiseIH = txtArteryPulseIH = txtArterialPressureLeftIH = txtArterialPressureRightIH = "";
            txtSoundIH = txtBreathingIH = txtCracklingIH = txtTongue = txtTooths = txtMendalins = txtLiver1 = txtLiver2 = "";
            txtSplleen = txtUrination = txtSupportMotion = txtNervose = txtSensation = txtIntensification = "";
            txtOtherDinamic = txtObjectiveData = txtResult = txtDoctorRecomendation = txtStomachNormal = "";
            lstStomachNormal = lstStomachNormal1 = lstStomachNormal2 = 0;
            HistiryOffIllness = "";
            boolOtherDinamic = true;
            TypeNbr = "";
            TypeIH = 0;
            SeasonID = 0;
            NbrIH = 0;
            DoctorID = 0;
            DocTypeID = (int)DocTypes.IllnessHistory;//Тип документа  - История болезни


        }
        public new bool Insert()
        {
            if (Insert("Document"))
            {
                DocumentID = DocumentID_ = Identity("Document");
                return InsertFull("IllnessHistory");
            }
            return false;
        }
        /// <summary>Удаление истории болезни
        /// Заплатин Е.Ф.
        /// 05.07.2011
        /// 14/03/2017 - убрал логику обновления с установкой признака удаления
        /// </summary>
        public bool Delete(CommandDirective directive)
        {
            var document = new Document {DocumentID_ = DocumentID};
            bool result = base.Delete(); 

            return result & document.Delete();
            //if (directive == CommandDirective.DeletePermanently)
            //{//Удаляется информация из базы данных

            //    return base.Delete();
            //}
            //else
            //{//Устанавливается признак удаления записи
            //    Deleted = true;//Назначается признак удаления записи
            //    return base.Update();//Обновляется информация в таблице базы данных
            //}
        }

        /// <summary>
        /// Для удобства использования
        /// </summary>
        /// <param name="id"></param>
        /// <param name="directive"></param>
        /// <returns></returns>
        public static bool Delete(int id, CommandDirective directive)
        {
            var ih = new IllnessHistory();
            ih.DocumentID = id;
            ih.Load();

            return ih.Delete(directive);
        }

        /// <summary>Вычисление нового номера истории болезни
        /// Заплатин Е.Ф.
        /// </summary>
        /// <param name="seasonid">идентификатор сезона</param>
        /// <param name="typeih">идентификатор типа направления в профилакторий</param>
        public int GetNewNbrIH(int seasonid, int typeih)
        {
            //ЗЕФ. запрос на поиск максимального значения номера истории болезни
            //по известным значениям сезона и типа ист. бол.
            DB cmd = new DB(
                "SELECT MAX(NbrIH) " +
                "FROM IllnessHistory as ih " +
                "LEFT JOIN Document as d on d.DocumentID = ih.DocumentID " +
                "WHERE SeasonID = " + seasonid + " AND ih.DocumentID <> " + DocumentID +
                        " AND TypeIH = " + typeih + " AND d.Deleted = 0 ");//Последняя редакция ЗЕФ 20.04.2013. Добавил исключение удаленных историй болезней
                                                                           //Выполнение запроса и преобразование результата в числовое значение
            object o = cmd.ExecuteScalar();
            if (o is int) //Если в результате запроса возвращено числовое значение, 
                          //т.е. существует хотя бы одна история болезни с номером
                return (int)o + 1; //то увеличивается найденое значение на единицу
            else
                return 1; //иначе (т.е. нет историй болезни) возвращается первый номер
        }
        /// <summary>Определение типа направления истории болезни по принадлежности к пациенту
        /// 05.05.2013
        /// Заплатин Е.Ф.
        /// </summary>
        public int GetTypeIH(int categoryId)
        {
            //Определение типа направления по категории пациента
            PatientCategoryList pcs = new PatientCategoryList();
            pcs.Load();
            //В коллекции загруженых категорий ищется та которая совпадает с категорией пациента
            //TODO: решить как поступать если не будет найдено категории 
            PatientCategory pcategory = pcs.Find(delegate (PatientCategory pc) { return (pc.CategoryID == categoryId); });
            this.TypeIH = pcategory.TypeID;

            return this.TypeIH;
        }
        /// <summary>Возвращает суммарную стоимость назначеных медикаментов</summary>
        public decimal SumMedic()
        {
            OutputMedicamentList medicaments = new OutputMedicamentList();//Назначенные медикаменты
            medicaments.Load(this.DocumentID);//Загрузаются все медикаменты по текущей истории болезни
            decimal s_med = 0;
            for (int i = 0; i < medicaments.Count; i++)
                s_med += medicaments[i].CountOutput * medicaments[i].Price;
            return s_med;
        }
        /// <summary>Возвращает суммарную стоимость медикаментов в назначеных услугах</summary>
        public decimal SumService()
        {
            ServiceOutputList services = new ServiceOutputList();//Назначенные услуги
            services.Load(this.DocumentID);//Загрузаются все услуги по текущей истории болезни
                                           //decimal s_service = 0;

            //for (int i = 0; i < services.Count; i++)
            //    s_service += services[i].PriceSum;
            return services.Sum(x => x.PriceSum);  //s_service;
        }

        /// <summary>Проверка на применение назначений к истории болезней. 
        /// Заплатин Е.Ф.
        /// 06.07.2011</summary>
        /// <returns>Если по истории болезней были сделаны назначения возвращает - true.</returns>
        public bool CheckDestination()
        {
            bool check = false;

            //Проверка назначений сделанных по медикаментам
            OutputMedicamentList medicaments = new OutputMedicamentList();//Назначенные медикаменты
            medicaments.Load(this.DocumentID);//Загрузаются все медикаменты по текущей истории болезни
            if (medicaments.Count != 0) check = true;

            //Проверка назначений сделанных по услугам
            ServiceOutputList services = new ServiceOutputList();//Назначенные услуги
            services.Load(this.DocumentID);//Загрузаются все услуги по текущей истории болезни
            if (services.Count != 0) check = true;

            return check;
        }
        /// <summary>Добавление диагноза
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// </summary>
        public void AddMKB(int MKBId, bool main, MKBType type)
        {
            //Добавление диагноза
            IH_MKBCode ih_MKB = new IH_MKBCode(true);
            ih_MKB.MKBCodeID = MKBId;//Указывается идентификатор именно родительского класса
            ih_MKB.IH_ID = DocumentID;
            ih_MKB.Main = main;
            ih_MKB.TypeMKB = (int)type;
            ih_MKB.Insert();
        }
        /// <summary>Проверка наличия уже назначенного основного диагноза истории болезни
        /// Заплатин Е.Ф.
        /// 28.02.2012
        /// </summary>
        public bool CheckMainMKB(MKBType type)
        {
            IH_MKBCodeList list = new IH_MKBCodeList(type);
            list.Load(this.DocumentID, true);
            return list.Count > 0;
        }
        /// <summary>Перенос всех диагнозов при поступлении  в диагнозы при приеме 
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// <param name="typeFrom">Тип диагнозов, которые нужно передать</param>
        /// <param name="typeTo">Тип диагнозов, которые нужно добавить</param>
        /// </summary>
        public void MKBMove(MKBType typeFrom, MKBType typeTo)
        {
            IH_MKBCodeList MKB = new IH_MKBCodeList(typeFrom);
            MKB.Load(DocumentID, false); //Загружаются диагнозы источники
            foreach (IH_MKBCode mkb in MKB)
            {
                //Добавляются диагнозы
                AddMKB(mkb.MKB_ID, mkb.Main, (MKBType)typeTo);
            }
        }

    }

    /// <summary> Класс: Список историй болезни
    /// автор Марьенков Е.В.</summary>
    public class IllnessHistoryList : DBOList<IllnessHistory>
    {
        /// <summary>
        /// Список историй болезней отдельного пациента
        /// </summary>
        /// <param name="ambcard_id">Код амбулаторной карты пациента</param>
        /// <returns></returns>

        public bool Load(int ambcard_id)
        {
            dbWhere = " AmbCardID=" + ambcard_id;
            return base.Load();
        }
    }
    // Класс историй болезни с кратким списком свойств
    // автор Марьенков Е.В.
    public class IllnessHistoryShort : DBO
    {
        [pk]
        [db("IllnessHistory")]
        public int DocumentID { get; set; }
        [db("IllnessHistory")]
        public int AmbCardID { get; set; } // привязка к амбулаторной карте пациента
        //Общие сведения
        [db("IllnessHistory")]
        public int NbrIH { get; set; } // номер путевки или курсовки
        [db("IllnessHistory")]
        public int SeasonID { get; set; }
        [db("IllnessHistory")]
        public int DoctorID { get; set; }
        [db("IllnessHistory")]
        public string DoctorName { get; set; } //В ковычках можно писать что угодно, т.к. запрос сформирован вручную

        /// <summary>
        /// Сумма стоимости назначенных медикаментов
        /// </summary>
        [db("IllnessHistory")]
        public decimal SummMedicament { get; set; }

        ///<summary>Сумма стоимости медикаментов по назначенным услугам</summary>
        [db("IllnessHistory")]
        public decimal SummService { get; set; }
        [db("Person")]
        public string Name { get; set; }
        [db("Person")]
        public string Lastname { get; set; }
        [db("Person")]
        public string SurName { get; set; }

        public string FullName { get { return Lastname + " " + Name + " " + SurName; } } // ФИО

        public IllnessHistoryShort() { }

        public IllnessHistoryShort(bool init) : base(init) { }
    }
    /// <summary>
    /// Класс: списка историй болезни с кратким списком свойств
    /// Используется для отображения списка историй болезней в формах: "Выдача со склада медикаментов"...
    /// автор Марьенков Е.В.
    /// </summary>
    public class IllnessHistoryShortList : DBOList<IllnessHistoryShort>
    {
        /// <summary>Используется в форме "Выдача со склада медикаментов" с учетом установленных фильтров
        /// </summary>
        /// <param name="season">идентификатор истории болезни</param>
        /// <param name="patientCategory">идентификатор категории пациента</param>
        /// <returns></returns>
        public bool Load(int season, int patientCategory)
        {
            //проверять параметры на ненулевые значения
            dbWhere = "";
            if (patientCategory > 0)
                dbWhere += " CategoryID = " + patientCategory + " AND ";//Критерий по полю категории пациента
            if (season > 0)
                dbWhere += " SeasonID = " + season;//Критерий по полю сезона

            dbSelectCustom = "SELECT ih.DocumentID,ih.AmbCardID, ih.SeasonID, ih.NbrIH, ih.SummMedicament, ih.SummService, " +
                                     "ih.DoctorID, pp.CategoryID, p.Name,p.LastName,p.SurName, pd.LastName AS DoctorName " +
                    "FROM IllnessHistory AS ih " +
                    "LEFT JOIN DocAmbulanceCard AS a ON ih.AmbCardID = a.DocumentID " +
                    "LEFT JOIN PersonPatient AS pp ON pp.PatientID = a.PatientID " + // связка нужна ради поля CategoryID
                    "LEFT JOIN Person AS p ON p.PersonID = pp.PatientID  " +          //Вытягивание информации о пациенте
                    "LEFT JOIN Person AS pd ON pd.PersonID = ih.DoctorID " +          //Вытягивание информации о лечащем враче
                    "WHERE " + dbWhere +
                    "ORDER BY p.LastName";//Сортировку добавил ЗЕФ 28.09.10
            return base.Load();
        }
    }

    ///<summary>
    ///Класс: История болезни для формы "Список историй болезней"
    /// автор Марьенков Е.В.
    ///</summary>
    public class IllnessHistoryShort2 : DBO
    {
        ///<summary>Идентификатор истории болезни - идентификатор документа</summary>
        [db("IllnessHistory")]
        [pk]
        public int DocumentID { get; set; }
        ///<summary>Идентификатор амбулаторной карты</summary>
        [db("IllnessHistory")]
        public int AmbCardID { get; set; }
        ///<summary>Категория пациентов</summary>
        [db("IllnessHistory")]
        public int CategoryID { get; set; }
        ///<summary>Идентификатор сезона</summary>
        [db("IllnessHistory")]
        public int SeasonID { get; set; }
        ///<summary>тип путевки или курсовки или платной основы</summary>
        [db("IllnessHistory")]
        public int TypeIH { get; set; }
        ///<summary>номер путевки или курсовки</summary>
        [db("IllnessHistory")]
        public int NbrIH { get; set; }
        ///<summary>Пол</summary>
        [db("IllnessHistory")]
        public bool Sex { get; set; }
        ///<summary>дата рождения</summary>
        [db("IllnessHistory")]
        public DateTime BirthD { get; set; }
        ///<summary>имя</summary>
        [db("IllnessHistory")]
        public string Name { get; set; }
        ///<summary>фамилия</summary>
        [db("IllnessHistory")]
        public string LastName { get; set; }
        ///<summary>отчество</summary>
        [db("IllnessHistory")]
        public string SurName { get; set; }
        /// <summary>лечащий врач</summary>
        [db("IllnessHistory")]
        public int DoctorID { get; set; }// Добавил ЗЕФ 06.10.2010
        [db("IllnessHistory")]
        public string DoctorName { get; set; } //Добавил ЗЕФ 10.04.2012
        ///<summary>диагноз с места отбора</summary>
        [db("IllnessHistory")]
        public string DiagOut { get; set; }
        ///<summary>Диагноз при поступлении</summary>
        [db("IllnessHistory")]
        public string DiagMain { get; set; }
        ///<summary>Диагноз сопутсвующий при поступлении</summary>
        [db("IllnessHistory")]
        public string txtResult { get; set; }
        ///<summary>дата регистрации документа</summary>
        [db("IllnessHistory")]
        public DateTime RegisterD { get; set; }
        ///<summary>Сумма стоимости услуг</summary>
        [db("IllnessHistory")]
        public decimal SummService { get; set; }
        ///<summary>Сумма стоимости медикаментов</summary>
        [db("IllnessHistory")]
        public decimal SummMedicament { get; set; }
        ///<summary>Сумма стоимости услуг и медикаментов</summary>
        public decimal Summ { get { return SummService + SummMedicament; } }

        public IllnessHistoryShort2() : base()
        {
            BirthD = new DateTime(1900, 1, 1);
            Name = "";
            LastName = "";
            SurName = "";
            DiagOut = "";
            DiagMain = "";
            txtResult = "";
            DoctorID = 0;
        }

        ///<summary>ФИО</summary>
        public string Fullname
        {
            get
            {
                return LastName + " " + Name + " " + SurName;
            }
        }
    }

    ///<summary>Класс: Список историй болезни для формы "Список историй болезни"</summary>
    public class IllnessHistoryShort2List : DBOList<IllnessHistoryShort2>
    {
        /// <summary>Загрузка списока историй болезней с учетом установленных фильтров и поисковой строки
        /// </summary>
        /// <param name="categories">категории пациентов</param>
        /// <param name="sex">пол</param>
        /// <param name="year1">год рождение (начало периода)</param>
        /// <param name="year2">год рождения (конец периода)</param>
        /// <param name="issex">нужно ли учитывать пол</param>
        /// <param name="isyear">нужно ли учитывать период годов рождения</param>
        /// <param name="name">Фамилия пациента</param>
        /// <param name="seasons">отмеченные сезоны</param>
        /// <param name="isseasons">учитывать ли сезоны</param>
        /// <param name="isdoctor">учитывать ли фильтр лечащего врача</param>
        /// <param name="doctor">лечащий врач</param>
        public bool Load(int[] categories, bool sex, int year1, int year2, bool issex, bool isyear,
                        string name, int[] seasons, bool isseasons, bool isdoctor, int doctor, 
                        string order_fild, bool showDeleted = false)
        {
            if (order_fild != "")
                dbOrder = order_fild;
            else
                dbOrder = "p.LastName";// Сортировка формируемого списка по фамилиям если не указано иного

            _dbWhereCreate(categories, sex, year1, year2,
                                     issex, isyear, name, seasons, isseasons, isdoctor, doctor);

            //*** Формирование комплексного запроса произвольного содержания
            dbSelectCustom = "SELECT h.DocumentID, h.DoctorID, h.DiagOut, h.DiagMain, h.txtResult, h.SeasonID, h.NbrIH, " +
                    "h.AmbCardID, h.TypeIH, h.SummService, h.SummMedicament, " +
                    "p.Name, p.LastName, p.SurName, p.Sex, p.BirthD, pp.CategoryID, d.RegisterD, doc.LastName AS DoctorName " +
                    "FROM IllnessHistory AS h  " +
                    "LEFT JOIN DocAmbulanceCard AS a ON h.AmbCardID = a.DocumentID " +
                    "JOIN Person AS p ON p.PersonID = a.PatientID " +
                    "LEFT JOIN PersonPatient AS pp ON p.PersonID = pp.PatientID " +
                    "LEFT JOIN Person AS doc ON doc.PersonID = h.DoctorID " +          //Вытягивание информации о лечащем враче
                    "LEFT JOIN Document AS d ON h.DocumentID = d.DocumentID " +
                    (dbWhere != "" ? "WHERE " + dbWhere + " " : "") +
                    (dbOrder != "" ? "ORDER BY " + dbOrder + " " : "");

            return base.Load();
        }

        /// <summary>
        /// Формирует критерий отбора на основе фильтров
        /// </summary>
        private void _dbWhereCreate(int[] categories, bool sex, int year1, int year2, bool issex, bool isyear, string name,
            int[] seasons, bool isseasons, bool isdoctor, int doctor)
        {

            //*** Обработка критерия по категориям пациентов. Требуется переработка постановленых условий ЗЕФ
            if (categories.Length == 0)
            //Если количество отмеченных категорий больше нуля то формируется критерий поиска по категориям
            {
                dbWhere += " pp.CategoryID = 0";
            }
            else
            {
                dbWhere = "";
                for (int i = 0; i < categories.Length; i++)
                    dbWhere += " pp.CategoryID = " + categories[i].ToString() + " OR";
                dbWhere = dbWhere.Substring(0, dbWhere.Length - 3);
                dbWhere = "(" + dbWhere + ")";
            }

            //*** Обработка критерия по сезонам. Требуется переработка постановленых условий ЗЕФ
            if (isseasons && seasons.Length >= 0) //Если установлен критерий отбора по сезонам 
            {
                // выделен хотя бы один сезон
                if (seasons.Length == 0)
                    return; //Если не выделен ни один сезон, то списка 

                string str_seasons = "";

                for (int i = 0; i < seasons.Length; i++)
                    str_seasons += " h.SeasonID = " + seasons[i].ToString() + " OR";

                str_seasons = str_seasons.Substring(0, str_seasons.Length - 3);

                if (dbWhere != "")
                    dbWhere += " AND ";

                dbWhere += "(" + str_seasons + ")";
            }

            //*** Обработка критерия по лечащему врачу. Добавил ЗЕФ.
            if (isdoctor) dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " h.DoctorID =" + "(" + doctor + ")";

            //*** Обработка критерия по принадлежности к полу пациента
            if (issex) dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " p.Sex =" + (sex ? "1" : "0");

            //*** Обработка критерия по году рождения
            if (isyear)
                dbWhere += (dbWhere.Trim() != "" ? " AND " : "") + " p.BirthD >='" +
                           new DateTime(year1, 1, 1).ToString("dd.MM.yyyy") + "' AND p.BirthD <='" +
                           new DateTime(year2, 12, 31).ToString("dd.MM.yyyy") + "'";

            //*** Обработка критерия поисковой строке
            if (name.Trim() != "")
                dbWhere = " p.Lastname LIKE '%" + name + "%'";

        }
        /// <summary>
        /// Формирует критерий отбора на основе фильтров
        /// </summary>
        private void _dbWhereCreate(int[] categories, int[] seasons)
        {
            //*** Обработка критерия по категориям пациентов. Требуется переработка постановленых условий ЗЕФ
            if (categories.Length == 0)
            //Если количество отмеченных категорий больше нуля то формируется критерий поиска по категориям
            {
                dbWhere += " pp.CategoryID = 0";
            }
            else
            {
                dbWhere = "";
                for (int i = 0; i < categories.Length; i++)
                    dbWhere += " pp.CategoryID = " + categories[i].ToString() + " OR";
                dbWhere = dbWhere.Substring(0, dbWhere.Length - 3);
                dbWhere = "(" + dbWhere + ")";
            }

            //*** Обработка критерия по сезонам. Требуется переработка постановленых условий ЗЕФ
            if (seasons.Length >= 0) //Если установлен критерий отбора по сезонам 
            {
                // выделен хотя бы один сезон
                if (seasons.Length == 0)
                    return; //Если не выделен ни один сезон, то списка 

                string str_seasons = "";

                for (int i = 0; i < seasons.Length; i++)
                    str_seasons += " h.SeasonID = " + seasons[i].ToString() + " OR";

                str_seasons = str_seasons.Substring(0, str_seasons.Length - 3);

                if (dbWhere != "")
                    dbWhere += " AND ";

                dbWhere += "(" + str_seasons + ")";
            }

        }

        ///<summary>Получить сумму стоимости медикаментов и услуг всех историй болезни</summary>
        public decimal GetSumm()
        {
            decimal s = 0;
            for (int i = 0; i < Count; i++)
                s += this[i].Summ;
            return s;
        }

        /// <summary>
        /// Вычисляет количество записей в соответсвии со списком фильтров
        /// </summary>
        /// <returns></returns>
        public int GetCount(int[] categories, int[] seasons)
        {
            _dbWhereCreate(categories, seasons);

            dbSelect = "SELECT * " +
                   "FROM IllnessHistory AS h  " +
                   "LEFT JOIN DocAmbulanceCard AS a ON h.AmbCardID = a.DocumentID " +
                   "JOIN Person AS p ON p.PersonID = a.PatientID " +
                   "LEFT JOIN PersonPatient AS pp ON p.PersonID = pp.PatientID ";

            return base.GetCount();
        }
    }

    ///<summary>Перечисление типов кодов МКБ
    ///Заплатин Е.Ф
    ///24.01.2012
    ///</summary>
    public enum MKBType
    {
        /// <summary>Диагноз с места отбора</summary>
        DiagOut = 1,
        /// <summary>Диагноз профилактория при поступлении</summary>
        DiagAttend = 2,
        /// <summary>Диагноз профилактория при выписке</summary>
        DiagMain = 3
    }
    ///<summary>Класс связи истории болезни с кодом МКБ
    ///Заплатин Е.Ф
    ///24.01.2012
    ///</summary>
    public class IH_MKBCode : MKBCode
    {
        /// <summary>Первичный ключ</summary>
        [pk]
        [db("IllnessHistoryMKB")]
        public int IH_MKB_ID { get; set; }
        /// <summary>Вторичный ключ</summary>
        [fk("MKBCode", "MKBCodeID")]
        [db("IllnessHistoryMKB")]
        public int MKB_ID { get; set; }
        /// <summary>Связь с историей болезни</summary>
        [db("IllnessHistoryMKB")]
        public int IH_ID { get; set; }
        /// <summary>Указание на основной - 1, или вспомогательный - 0 диагнозы</summary>
        [db("IllnessHistoryMKB")]
        public bool Main { get; set; }
        /// <summary>Указание на основной - 1, или вспомогательный - 0 диагнозы</summary>
        public string MainText { get { return (Main ? "Основной" : "Сопутсвующий"); } }
        /// <summary>Тип МКБ из энум перечисления</summary>
        [db("IllnessHistoryMKB")]
        public int TypeMKB { get; set; }

        public IH_MKBCode()
            : base() { }
        public IH_MKBCode(bool init)
            : base(init)
        {
            IH_MKB_ID = IH_ID = MKB_ID = TypeMKB = 0;
            Main = false;
        }
        public new bool Insert()
        {
            MKB_ID = MKBCodeID;
            return Insert("IllnessHistoryMKB");
        }
        public new bool Delete()
        {
            return base.Delete("IllnessHistoryMKB");
        }
        public new bool Update()
        {
            MKB_ID = MKBCodeID;
            return base.Update("IllnessHistoryMKB");
        }
    }
    ///<summary>Класс коллекции связей истории болезни с кодами МКБ
    ///Заплатин Е.Ф
    ///24.01.2012
    ///</summary>
    public class IH_MKBCodeList : DBOList<IH_MKBCode>
    {
        MKBType TypeList = new MKBType();

        public IH_MKBCodeList(MKBType type) : base()
        {
            TypeList = type;
        }
        ///<summary>Загрузка диагнозов по конкретной истории болезни
        ///Заплатин Е.Ф
        ///24.01.2012
        ///<param name="ih">Идентификатор истории болезни</param>
        ///<param name="main">Если true, то нужно загружать исключительно основные диагнозы</param>
        ///</summary>
        public bool Load(int ih, bool main)
        {
            dbWhere = "IH_ID = " + ih.ToString() + " AND TypeMKB = " + ((int)TypeList).ToString();
            if (main)
                dbWhere += " AND Main = 1";
            dbOrder = "Main DESC, Name ";//Сортировка нужна для отображения основного диагноза на первом месте а затем сопутсвующих в алфавитном порядке
            return base.Load();
        }
    }
}
