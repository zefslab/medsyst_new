﻿using System;
using Medsyst.Dao.Base;

namespace Medsyst.Class
{
    /// <summary>
    /// Наблюдение дерматоловинеролога за пациентом в процессе всей истории болезни
    /// автор: Заплатин Е.Ф.
    /// создан: 17.11.2013
    /// </summary>
    public class ObservationDerma : DBO
    {
        [db("IllnessHistoryObservationDerma")]
        [pk]
        public int ObservationID { get; set; }//Уникальный идентификатор наблюдения

        [db("IllnessHistoryObservationDerma")]
        public int DocumentID { get; set; } //Привязка к истории болезни

        [db("IllnessHistoryObservationDerma")]
        public int DoctorID { get; set; } //Лечащий врач, проводивший наблюдение. ЗЕФ. 23.12.2011

        [db("IllnessHistoryObservationDerma")]
        public DateTime ReceptionD { get; set; }// дата приема у врача

        [db("IllnessHistoryObservationDerma")]
        public string txtComplaint { get; set; }// жалоба 

        [db("IllnessHistoryObservationDerma")]
        public string txtAnamnes { get; set; }// Анамнез заболевания 

        [db("IllnessHistoryObservationDerma")]
        public int lstSkin { get; set; }// Кожные покровы

        [db("IllnessHistoryObservationDerma")]
        public string txtSkin { get; set; }// Кожные покровы

        [db("IllnessHistoryObservationDerma")]
        public int lstNail { get; set; }    // Ногтевые пластинки

        [db("IllnessHistoryObservationDerma")]
        public string txtNail { get; set; } // Ногтевые пластинки

        [db("IllnessHistoryObservationDerma")]
        public int lstHair { get; set; }    // Волосы

        [db("IllnessHistoryObservationDerma")]
        public string txtHair { get; set; } // Волосы

        [db("IllnessHistoryObservationDerma")]
        public int lstProcessIs { get; set; }    // Процесс носит...

        //[db("IllnessHistoryObservationDerma")]
        //public int lstProcessLocalized { get; set; }    // Процесс локализуется на ...

        [db("IllnessHistoryObservationDerma")]
        public string txtProcessLocalized { get; set; } //  Процесс локализуется на ...

        [db("IllnessHistoryObservationDerma")]
        public string txtRecomendations { get; set; } //  Рекомендации

        [db("IllnessHistoryObservationDerma")]
        public int lstLymphNode { get; set; }    // Перефирические лимфоузлы

        [db("IllnessHistoryObservationDerma")]
        public string txtLymphNode { get; set; } //  Перефирические лимфоузлы

        [db("IllnessHistoryObservationDerma")]
        public string txtDiagnosis { get; set; } //  Диагноз

        /// <summary>
        /// Используется в списке наблюдений
        /// </summary>
        public string Head
        {
            get
            {
                Person person = new Person(true);
                person.PersonID = DoctorID;
                person.Load();
                return ReceptionD.ToString("d") + " " + person.FIOBrif;
            }
        }


        public ObservationDerma() { }
        public ObservationDerma(bool init)
            : base(init)
        {
            ReceptionD = DateTime.Now;
            DoctorID = Security.user.UserID;
            txtComplaint = "";
            txtAnamnes = "";
            txtSkin = "";
            txtNail = "";
            txtHair = "";
            txtProcessLocalized = "";
            txtLymphNode = txtDiagnosis = "";
        }
    }

    /// <summary>
    /// Коллекция наблюдений дерматоловинеролога за пациентом в процессе всей истории болезни
    /// автор: Заплатин Е.Ф.
    /// создан: 17.11.2013
    /// </summary>
    public class ObservationsDerma : DBOList<ObservationDerma>
    {
        public new bool Load()
        {
            dbWhere = "";
            return base.Load();
        }

        public bool Load(int history_id)
        {
            dbWhere = "DocumentID = " + history_id;
            return base.Load();
        }

    }

    /// <summary>Класс шаблона наблюдения, содержащий ссылку на историю болезни, которая будет взята за основу вновь создаваемой
    /// Шаблон призван облегчить заполнение истории болезни. Все поля новой истории болезни созданные на основе шаблона заполняются
    /// "нормальными" значениями
    /// Заплатин Е.Ф.
    /// 17.11.2013
    /// </summary>
    public class IllnessHistoryObservationDermaTemplate : DBO, IObservationDermaTemplate
    {
        [db("IllnessHistoryObservationDermaTemplate")]
        public int ObservationID { get; set; }

        /// <summary>Наименование шаблона</summary>
        [db("IllnessHistoryObservationDermaTemplate")]
        public string Name { get; set; }

        /// <summary>дата создания шаблона</summary>
        [db("IllnessHistoryObservationDermaTemplate")]
        public DateTime Date { get; set; }

        /// <summary>Сотрудник создавший шаблон</summary>
        [db("IllnessHistoryObservationDermaTemplate")]
        public int EmploeeyID { get; set; }

        public IllnessHistoryObservationDermaTemplate() { }
        public IllnessHistoryObservationDermaTemplate(bool init)
            : base(init)
        {
            ObservationID = 0;
            EmploeeyID = 0;
            Name = "";
            Date = new DateTime(1900, 1, 1);
        }
        public new bool Insert()
        {
            return base.InsertFull("IllnessHistoryObservationDermaTemplate");
        }

    }

    /// <summary>Колеекция шаблонов наблюдений дерматологинеколога.
    /// Заплатин Е.Ф.
    /// 17.11.2013
    /// </summary>
    public class IllnessHistoryObservationDermaTemplateList : DBOList<IllnessHistoryObservationDermaTemplate>
    {
        /// <summary>Загрузка наблюдений объявленных как шаблоны в обратном хронологическом порядке, чтобы выбирался наиболее свежий шаблон
        /// Заплатин 
        /// </summary>
        public new bool Load()
        {
            dbOrder = "Date DESC";//Загрузка в обратном хронологическом порядке, чтобы выбирать шаблон по умолчанию самый свежий, созданный последним
            return base.Load();
        }
    }
    /// <summary>
    /// Интерфейс используется в обобщенном методе создания шаблонов
    /// </summary>
    public interface IObservationDermaTemplate
    {

        int ObservationID { get; set; }

        string Name { get; set; }

        DateTime Date { get; set; }

        int EmploeeyID { get; set; }

        bool Insert();
    }
}
