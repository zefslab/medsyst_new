﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medsyst.Data.Filter
{
    public class OnDemandMedicineFilter : BaseFilter
    {
        public int DocumentId { get; set; }

        public bool withDeleted { get; set; }
    }
}
