﻿
using System;

namespace Medsyst.Data.Filter
{
    public class ReceiptOrderFilter : BaseFilter
    {
        public int? ProviderId  { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? isBudget { get; set; }

        public bool ShowWithDeleted { get; set; }
    }
}
