﻿
using Medsyst.Data.Constant;

namespace Medsyst.Data.Filter
{
    /// <summary>
    /// Фильтр для отбора историй болезней
    /// </summary>
    public class IllnessHistoryFilter : BaseFilter 
    {
        public int SeasonId { get; set; }

        public int CategoryPacientId { get; set; }

        /// <summary>
        /// Указывает на принадлежность данных к форме представления
        /// и в зависимости от этого может меняться состав данных
        /// </summary>
        public PresentationForms PresentationForm { get; set; } 
    }
}
