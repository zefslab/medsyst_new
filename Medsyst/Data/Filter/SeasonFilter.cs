﻿
using Medsyst.Data.Constant;

namespace Medsyst.Data.Filter
{
    /// <summary>
    /// Фильтр для отбора сезонов 
    /// </summary>
    public class SeasonFilter : BaseFilter 
    {
        public int? Year  { get; set; }

       
    }
}
