﻿
namespace Medsyst.Data.Filter
{
    public class OutputMedicineFilter : BaseFilter
    {
        public int DocumentId { get; set; }

        public bool withDeleted { get; set; }
    }
}
