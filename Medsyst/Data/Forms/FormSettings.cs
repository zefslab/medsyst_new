﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace Medsyst.Class.Forms
{
    /// <summary>
    /// Инициализация значений элементов управления данными из XML файла
    /// </summary>
    public class FormSettings
    {
        ///<summary>Путь к файлу со свойствами элементов управления</summary>
        public string FileName = "";
        ///<summary>Путь к файлу со свойствами элементов управления, для технической инициализации</summary>
        public static string Path = "";
        XmlNode fs;

        public FormSettings()
        {
        }
        ///<summary>Загружаются узлы со списками значений для указанной в параметре формы.ЗЕФ
        public FormSettings(Form form, string filename)
        {
            Load(form, filename);
        }
        public void Load(Form form, string filename)
        {
            if (!File.Exists(filename)) return;//Проверка на существование файла выполнена неполно.
                //Если файл не найден то должно быть выдано вразумительное объяснение. Иначе непонятно почему метод не отрабатывается.
            FileName = filename;//Непонятно для чего это нужно. ЗЕФ.
            XmlDocument doc = new XmlDocument(); //создание экземпляра обекта XML структуры.ЗЕФ.
            doc.Load(FileName);//Загрузка XML документа.ЗЕФ
            
            XmlNodeList forms = doc.GetElementsByTagName("forms");//Выбираются все узловые структуры с названием "forms"
            fs = forms[0];//В предположении что такая структура всего одна берется первая из них. 
                            //Если таких структур в последствии появится больше, то потребуется оператор foreach для прохода по всем узлам.ЗЕФ.
            foreach (XmlNode f in fs.ChildNodes)//Перебираются все узлы с названием forms и находится соответсвие.ЗЕФ.
            {
                if (f.Attributes["name"].Value == form.Name)//Если найдено соответсвие узла названию формы.ЗЕФ.
                {
                    form.Text = f.Attributes["text"].Value;//Названию формы присваивается значение из поля text
                    FindControls(form.Controls, f.ChildNodes);
                    break;
                }
            }
        }
        /// <summary>Выбираются управляющие элементы из древовидной структуры XLM для установки их списочных значений. ЗЕФ. 
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="nodes"></param>
        private void FindControls(Control.ControlCollection controls, XmlNodeList nodes)
        {
            foreach (Control c in controls)
            {
                if (nodes.Count > 0)
                    for (int i = 0; i < nodes.Count; i++)
                        if (nodes[i].Attributes["name"].Value == c.Name)
                        {
                            if (nodes[i].Attributes["link"] != null && nodes[i].Attributes["link"].Value!="")
                            {
                                string link = nodes[i].Attributes["link"].Value;
                                string form = nodes[i].Attributes["form"].Value;
                                
                                foreach (XmlNode f in fs.ChildNodes)
                                    if (f.Attributes["name"].Value == form)
                                    {
                                        foreach (XmlNode ctrl in f.ChildNodes)
                                            if (ctrl.Attributes["name"].Value == link)
                                            {
                                                SetValue(c, ctrl);//Производится установка значения управляющего элемента с помощью вспомогательного метода этого же класса.ЗЕФ.
                                                break;
                                            }
                                        break;
                                    }
                            }
                            SetValue(c, nodes[i]);
                        }
                FindControls(c.Controls, nodes);
            }
        }
        private void FindControlsName(Control.ControlCollection controls, XmlNodeList nodes)
        {
            foreach (Control c in controls)
            {
                if (nodes.Count > 0)
                    for (int i = 0; i < nodes.Count; i++)
                        if (nodes[i].Attributes["name"].Value == c.Name)
                        {
                            if (nodes[i].Attributes["link"] != null && nodes[i].Attributes["link"].Value != "")
                            {
                                string link = nodes[i].Attributes["link"].Value;
                                string form = nodes[i].Attributes["form"].Value;

                            }
                            SetValue(c, nodes[i]);
                        }
                FindControls(c.Controls, nodes);
            }
        }
        /// <summary>В зависимости от типа управляющего элемента ему присваиваются значения из сопоставленного с ним узла из XML файла.
        /// </summary>
        /// <param name="count"></param>
        /// <param name="node"></param>
        private void SetValue(Control c, XmlNode node)
        {
            if (c is TextBox) ((TextBox)c).Text = node.Attributes["text"].Value;
            if (c is Label) ((Label)c).Text = node.Attributes["text"].Value;
            if (c is LinkLabel) ((LinkLabel)c).Text = node.Attributes["text"].Value;
            if (c is Button) ((Button)c).Text = node.Attributes["text"].Value;
            if (c is GroupBox) ((GroupBox)c).Text = node.Attributes["text"].Value;
            if (c is TabPage) ((TabPage)c).Text = node.Attributes["text"].Value;
            if (c is DateTimePicker) ((DateTimePicker)c).Value = DateTime.Parse(node.Attributes["value"].Value);
            if (c is CheckBox)
            {
                ((CheckBox)c).Text = node.Attributes["text"].Value;
                ((CheckBox)c).Checked = bool.Parse(node.Attributes["value"].Value);
                return;
            }
            if (c is RadioButton)
            {
                ((RadioButton)c).Text = node.Attributes["text"].Value;
                ((RadioButton)c).Checked = bool.Parse(node.Attributes["value"].Value);
            }
            if (c is MonthCalendar)
            {
                ((MonthCalendar)c).SelectionStart = DateTime.Parse(node.Attributes["value"].Value);
                ((MonthCalendar)c).SelectionEnd = ((MonthCalendar)c).SelectionStart;
            }
            if (c is ComboBox)
            {
                if (node.ChildNodes.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(int));
                    dt.Columns.Add("text", typeof(string));
                    for (int i = 0; i < node.ChildNodes.Count; i++)
                        if (node.ChildNodes[i].Name == "item" && node.ChildNodes[i].NodeType == XmlNodeType.Element)
                        dt.Rows.Add(new object[]{int.Parse(node.ChildNodes[i].Attributes["value"].Value), node.ChildNodes[i].Attributes["text"].Value});
                    ((ComboBox)c).ValueMember = "id";
                    ((ComboBox)c).DisplayMember = "text";
                    ((ComboBox)c).DataSource = dt;
                }
            }
            if (c is ListBox)
            {
                if (node.ChildNodes.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(int));
                    dt.Columns.Add("text", typeof(string));
                    for (int i = 0; i < node.ChildNodes.Count; i++)
                        if (node.ChildNodes[i].Name == "item" && node.ChildNodes[i].NodeType == XmlNodeType.Element)
                            dt.Rows.Add(new object[] { int.Parse(node.ChildNodes[i].Attributes["value"].Value), node.ChildNodes[i].Attributes["text"].Value });
                    ((ListBox)c).ValueMember = "id";
                    ((ListBox)c).DisplayMember = "text";
                    ((ListBox)c).DataSource = dt;
                }
            }
            if (c is CheckedListBox)
            {
                if (node.ChildNodes.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(int));
                    dt.Columns.Add("text", typeof(string));
                    for (int i = 0; i < node.ChildNodes.Count; i++)
                        if (node.ChildNodes[i].Name == "item" && node.ChildNodes[i].NodeType == XmlNodeType.Element)
                            dt.Rows.Add(new object[] { int.Parse(node.ChildNodes[i].Attributes["value"].Value), node.ChildNodes[i].Attributes["text"].Value });
                    ((CheckedListBox)c).ValueMember = "id";
                    ((CheckedListBox)c).DisplayMember = "text";
                    ((CheckedListBox)c).DataSource = dt;
                }
            }

            if (c is TreeView)
            {
                TreeViewLoad(((TreeView)c).Nodes, node.ChildNodes);//Используется вспомогательный метод этого класса
            }
            if (c is DataGridView)
            {
                DataGridView dg = c as DataGridView;

                    for (int i = 0; i < node.ChildNodes.Count; i++)
                        if (node.ChildNodes[i].Name == "header" && node.ChildNodes[i].NodeType == XmlNodeType.Element)
                        {
                            DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
                            col.DataPropertyName = node.ChildNodes[i].Attributes["data"].Value;
                            col.HeaderText = node.ChildNodes[i].Attributes["text"].Value;
                            col.Name = node.ChildNodes[i].Attributes["name"].Value;
                            if(node.ChildNodes[i].Attributes["width"]!=null && node.ChildNodes[i].Attributes["width"].Value!="")
                                col.Width = int.Parse(node.ChildNodes[i].Attributes["width"].Value);
                            col.ReadOnly = true;
                            dg.Columns.Add(col);
                        }
            }
        }
        /// <summary>Заполнение управляющего эл-та типа TreeNodeCollection значениями из узла
        /// </summary>
        /// <param name="tnodes"></param>
        /// <param name="nodes"></param>
        private void TreeViewLoad(TreeNodeCollection tnodes, XmlNodeList nodes)
        {
            foreach (XmlNode n in nodes)
                if (n.Name == "node" && n.NodeType == XmlNodeType.Element)
                {
                    TreeNode tn = tnodes.Add(n.Attributes["value"].Value, n.Attributes["text"].Value);
                    if (n.Attributes["checked"] != null && n.Attributes["checked"].Value == "true")
                        tn.Checked = true;
                    //bool.Parse(n.Attributes["text"].Value);
                    TreeViewLoad(tn.Nodes,n.ChildNodes);
                }
        }
    }
}
