﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace rsdev.MCV.View
{
    /// <summary>Класс привязки контролов формы к свойствам объекта наследуемого от этого класса 
    /// </summary>
    public class BindingForm
    {
        /// <summary>Список контролов привязаных к объекту</summary>
        protected List<BindingFormLink> _Controls;
        /// <summary>Префикс для имени контролов, при привязке по имени или номеру свойства объекта</summary>
        public string fbPrefix = "ctrl";

        public BindingForm()
        {
            _Controls = new List<BindingFormLink>();
        }
        /// <summary>Привязка контролов формы и объекта по свойству <code>Tag</code> контрола
        /// </summary>
        /// <param name="objectname">Имя объекта к которому производится привязка</param>
        /// <param name="form">Форма контролы которой будут привязаны</param>
        /// <returns>Успешность привзяки</returns>
        public bool FormBinding(string objectname, Form form)
        {
            Type type = this.GetType();
            PropertyInfo[] fields = type.GetProperties();
            if (fields.Length == 0)
                return false;
            _Controls.Clear();
            FindControls(form.Controls, objectname, fields);
            return _Controls.Count>0;
        }
        /// <summary>Поиск контролов на форме привязанных по свойству <code>Tag</code>
        /// </summary>
        /// <param name="controls">Сиписок контролов в котором искать</param>
        /// <param name="objectname">Имя объекта к которому производится привязка</param>
        /// <param name="fields">Описание свойства объекта (рефлексия)</param>
        private void FindControls(Control.ControlCollection controls, string objectname, PropertyInfo[] fields)
        {
            foreach (Control c in controls)
            {
                if (c.Tag != null)
                {
                    string tag = c.Tag.ToString();
                    if (tag.Contains(objectname + "."))
                    {
                        string FieldName = tag.Replace(objectname + ".", "").Trim();
                        foreach (PropertyInfo field in fields)
                            if (field.Name == FieldName)
                                _Controls.Add(new BindingFormLink(c, field));
                    }
                }
                FindControls(c.Controls, objectname, fields);
            }
        }
        /// <summary>Поиск контролов на форме привязанных по имени. Правило именования контролов: <code>fbPrefix + Object_Name + Object_Field</code>
        /// </summary>
        /// <param name="controls">Сиписок контролов в котором искать</param>
        /// <param name="objectname">Имя объекта к которому производится привязка</param>
        /// <param name="fields">Описание свойства объекта (рефлексия)</param>
        private void FindControls_ForName(Control.ControlCollection controls, string objectname, PropertyInfo[] fields)
        {
            foreach (Control c in controls)
            {
                string name = c.Name;
                if (name.Contains(fbPrefix + objectname + "_"))
                {
                    string FieldName = name.Replace(fbPrefix + objectname + ".", "").Trim();
                    foreach (PropertyInfo field in fields)
                        if (field.Name == FieldName)
                            _Controls.Add(new BindingFormLink(c, field));
                }
                FindControls_ForName(c.Controls, objectname, fields);
            }
        }
        /// <summary> Запись данных в контролы на форме из привязанного объекта
        /// </summary>
        public void WriteToForm()
        {
            foreach(BindingFormLink c in _Controls)
            {
                    if (c.Control is Label) c.Control.Text = c.Field.GetValue(this, null).ToString();
                    else if (c.Control is TextBox) c.Control.Text = c.Field.GetValue(this, null) == null ? "" : c.Field.GetValue(this, null).ToString();
                    else if (c.Control is Button) c.Control.Text = c.Field.GetValue(this, null).ToString();
                    else if (c.Control is CheckBox)
                    {
                        object o = c.Field.GetValue(this, null);
                        bool b = false;
                        if(o==null) 
                            b=false;
                        else
                        {
                            if(o is bool)
                                b = (bool)o;
                            else
                                b = (int)o == 0 ? false : true;
                        }
                        ((CheckBox)c.Control).Checked = b;
                    }
                    else if (c.Control is RadioButton) ((RadioButton)c.Control).Checked = (bool)c.Field.GetValue(this, null);
                    else if (c.Control is DateTimePicker)
                    {
                        if (((DateTimePicker)c.Control).MinDate > (DateTime)c.Field.GetValue(this, null))
                            ((DateTimePicker)c.Control).Value = ((DateTimePicker)c.Control).MinDate;
                        else
                            ((DateTimePicker)c.Control).Value = (DateTime)c.Field.GetValue(this, null);
                    }
                    else if (c.Control is NumericUpDown) ((NumericUpDown)c.Control).Value = (decimal)(int)c.Field.GetValue(this, null);
                    else if (c.Control is ComboBox)
                    {
                        ComboBox cb = (ComboBox)c.Control;
                        object o = cb.SelectedValue;
                        if (o is int)
                            cb.SelectedValue = c.Field.GetValue(this, null);
                        else
                            cb.SelectedValue = c.Field.GetValue(this, null).ToString();
                    }
                    else if (c.Control is ListBox)
                    {
                        ListBox lb = (ListBox)c.Control;
                        object o = lb.SelectedValue;
                        if (o is int)
                            lb.SelectedValue = c.Field.GetValue(this, null);
                        else
                            lb.SelectedValue = c.Field.GetValue(this, null).ToString();
                    }
                    //else if (count.Control is NumericUpDown) ((NumericUpDown)count.Control).Value = (decimal)count.Field.GetValue(this, null);
                    
            }
        }
        /// <summary>Считывание данных из контролов на форме в привязанный объект
        /// </summary>
        public void ReadFromForm()
        {
            foreach (BindingFormLink c in _Controls)
            {
                if (c.Control is Label) SetValue(c, c.Control.Text);
                else if (c.Control is TextBox) SetValue(c, c.Control.Text);
                else if (c.Control is Button) SetValue(c, c.Control.Text);
                else if (c.Control is CheckBox)
                {
                    if(c.Field.PropertyType == typeof(bool))
                        c.Field.SetValue(this, ((CheckBox)c.Control).Checked,null);
                    else
                        c.Field.SetValue(this, ((CheckBox)c.Control).Checked ? 1 : 0, null);

                }
                else if (c.Control is RadioButton) c.Field.SetValue(this, ((RadioButton)c.Control).Checked, null);
                else if (c.Control is DateTimePicker) c.Field.SetValue(this, ((DateTimePicker)c.Control).Value, null);
                else if (c.Control is NumericUpDown) c.Field.SetValue(this, (int)((NumericUpDown)c.Control).Value, null);
                else if (c.Control is ComboBox)
                {
                    string v;
                    if (((ComboBox)c.Control).SelectedValue == null)
                        v = "0";
                    else
                        v = ((ComboBox)c.Control).SelectedValue.ToString();
                    SetValue(c, v);
                }
                else if (c.Control is ListBox)
                {
                    string v;
                    if (((ListBox)c.Control).SelectedValue == null)
                        v = "0";
                    else
                        v = ((ListBox)c.Control).SelectedValue.ToString();
                    SetValue(c, v);
                }
                
            }
        }
        private void SetValue(BindingFormLink c, string value)
        {
            if (c.Field.PropertyType == typeof(string)) c.Field.SetValue(this, value,null);
            else if (c.Field.PropertyType == typeof(int)) c.Field.SetValue(this, int.Parse(value), null);
            else if (c.Field.PropertyType == typeof(bool))
            {
                bool b = false;
                bool hr = false;
                b = bool.TryParse(value, out hr);
                if (!hr)
                {
                    if (value == "0")
                        b = false;
                    else
                        b = true;
                }
                c.Field.SetValue(this, b, null);
            }
            else if (c.Field.PropertyType == typeof(float)) c.Field.SetValue(this, float.Parse(value), null);
            else if (c.Field.PropertyType == typeof(decimal)) c.Field.SetValue(this, decimal.Parse(value, System.Globalization.NumberStyles.AllowDecimalPoint), null);
            else if (c.Field.PropertyType == typeof(double)) c.Field.SetValue(this, double.Parse(value), null);
            else if (c.Field.PropertyType == typeof(byte)) c.Field.SetValue(this, byte.Parse(value), null);
            else if (c.Field.PropertyType == typeof(char)) c.Field.SetValue(this, char.Parse(value), null);
        }
    }
    /// <summary>Класс элемент списка привязки контрола к свойству
    /// </summary>
    public class BindingFormLink
    {
        /// <summary>
        /// Контрол который привязан к свойству
        /// </summary>
        public Control Control;
        /// <summary>
        /// Свойство к которому привязан контрол
        /// </summary>
        public PropertyInfo Field;

        public BindingFormLink(Control control, PropertyInfo field)
        {
            Control = control;
            Field = field;
        }
    }
    /// <summary>Пользовательский аттрибут, обозначающий что полю назначается идентификатор для привязки с контролом
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class FBAttribute : System.Attribute
    {
        public string Id = "";

        public FBAttribute() { }
        public FBAttribute(string id)
        {
            this.Id = id;
        }
    }
}