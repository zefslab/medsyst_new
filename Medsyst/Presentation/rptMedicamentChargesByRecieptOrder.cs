﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.DataSet;
using Medsyst.DataSet.datMedicamentChargesByRecieptOrderTableAdapters;
using Medsyst.Class;
using CrystalDecisions.CrystalReports.Engine;




namespace Medsyst
{
    public partial class rptMedicamentChargesByRecieptOrder : Form
    {
        datMedicamentChargesByRecieptOrder dataSet = new datMedicamentChargesByRecieptOrder(); //создается экземпляр набора данных
        MedicamentChargesByReceiptOrderTableAdapter tableAdapter = new MedicamentChargesByReceiptOrderTableAdapter(); //Инициализируется адаптер с последующим присвоением занчений параметрам


        public rptMedicamentChargesByRecieptOrder()
        {
            InitializeComponent();
        }

        private void rptMedicamentChargesByRecieptOrder_Load(object sender, EventArgs e)
        {
            medicamentChargesByRecieptOrder1.Load(); //Загружается отчет. Экзкмпляр отчета определен в файле rptMedicamentCharges.Desiner.cs
            tableAdapter.Connection.ConnectionString = DB.CS;//Строка подключения
            //Загрузка групп категорий
            MedicamentCategoryGroups mcgs = new MedicamentCategoryGroups();
            mcgs.Load();
            gridCategoryGroups.AutoGenerateColumns = false;
            gridCategoryGroups.DataSource = null;
            gridCategoryGroups.DataSource = mcgs;

        }
        /// <summary>Назначаются значания параметрам запроса и указывается запрос в качестве источника
        /// Заплатин Е.Ф.
        /// 23.11.2011
        /// </summary>
        private void ExecutReport()
        {
            //Разбор обгалоченых групп категорий
            string txtGroups = "";//Формируемая строка со списком идентификаторов обглоченых групп
            string txtGroupsName = "";
            foreach (DataGridViewRow row in gridCategoryGroups.Rows)
            {
                if (row.Cells["colChk"].Value != null)
                    if ((bool)row.Cells["colChk"].Value)//Найдена обгалоченая группа
                    {
                        txtGroups += ", " + row.Cells["colID"].Value.ToString();//Добавляется в список групп
                        txtGroupsName += ", " + row.Cells["colGroup"].Value;//Добавляется наименования в список групп
                    }
            }
            if (txtGroups != "")
            {
                txtGroups = txtGroups.Remove(0, 2); //Удаление лишней запятой  спереди списка
                txtGroupsName = txtGroupsName.Remove(0, 2); //Удаление лишней запятой спереди списка
            }
            else
                txtGroups = "0";//Если не выделено ни одной группы категорий

            //Заполнение данными таблицы набора данных с присвоением занчений параметрам хранимой процедуре
            tableAdapter.Fill(dataSet.MedicamentChargesByReceiptOrder
                            , dtpBeginPeriod.Value.ToString("dd.MM.yyyy")
                            , dtpEndPeriod.Value.ToString("dd.MM.yyyy")
                            , (bool)rbtnBudget.Checked
                            , txtGroups);
            medicamentChargesByRecieptOrder1.SetDataSource(dataSet); //вызываемый метод передает отчету заполненный экземпляр набора данных dataSet

            //Вспомогательные параметры для оформления заголовка отчета, указывающие даты начала и окончания отчетного периода
            medicamentChargesByRecieptOrder1.SetParameterValue("DateBegin", dtpBeginPeriod.Value.ToString("dd.MM.yyyy"));
            medicamentChargesByRecieptOrder1.SetParameterValue("DateEnd", dtpEndPeriod.Value.ToString("dd.MM.yyyy"));
            medicamentChargesByRecieptOrder1.SetParameterValue("Средства", (bool)rbtnBudget.Checked);
            medicamentChargesByRecieptOrder1.SetParameterValue("Группы", txtGroupsName);

            VeiwReport();
        }

        private void btnExecutReportByPeriod_Click(object sender, EventArgs e)
        {
            chkSclad.Enabled = true;
            ExecutReport();
        }
        /// <summary>Настройка отображения отчета
        /// Заплатин Е.Ф.
        /// 12.07.2012
        /// </summary>
        private void VeiwReport()
        {
            medicamentChargesByRecieptOrder1.ReportDefinition.ReportObjects["lblSclad"].ObjectFormat.EnableSuppress =
             medicamentChargesByRecieptOrder1.ReportDefinition.ReportObjects["txtSclad"].ObjectFormat.EnableSuppress = 
             medicamentChargesByRecieptOrder1.ReportDefinition.ReportObjects["sumSclad"].ObjectFormat.EnableSuppress = !chkSclad.Checked;

            crystalReportViewer1.ReportSource = medicamentChargesByRecieptOrder1; //Элементу формы просмотра отчетов передается загруженный отчет
            crystalReportViewer1.Zoom(1);//Маштабирование предпросмотра отчета по ширине страницы
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            VeiwReport();
        }
    }
}
