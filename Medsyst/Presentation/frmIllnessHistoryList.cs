﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Forms;
using System.IO;
using Medsyst.Class;
using System.Data.OleDb;
using Medsyst.Class.Core;
using Medsyst.Extentions;

namespace Medsyst
{
    public partial class frmIllnessHistoryList : Form
    {
        ///<summary>Список историй болезни</summary>
        IllnessHistoryShort2List ihs = new IllnessHistoryShort2List();

        IH_MKBCodeList DiagAttend = new IH_MKBCodeList(MKBType.DiagAttend);
        IH_MKBCodeList DiagMain = new IH_MKBCodeList(MKBType.DiagMain);
        IH_MKBCodeList DiagOut = new IH_MKBCodeList(MKBType.DiagOut);
        ///<summary>Текущая страница</summary>
        int PageCurrent = 0;
        ///<summary>Список сезонов</summary>
        Seasons _seasons = new Seasons();
        /// <summary>Список лечащих врачей</summary>
        Doctors doctors = new Doctors();
        /// <summary>Категории пациентов</summary>
        PatientCategoryList categoris = new PatientCategoryList();
        
        private string order_fild = "";//Порядок сортировки списка историй болезней
        private int  prev_order_fild_index = 1;//Поле предыдущего порядка сорировки. По умолчанию используется сортировка по колонке с ФИО
        private string fild_suf = ""; //Используется для изменения порядка сортировки

        public frmIllnessHistoryList()
        {
            InitializeComponent();
        }

        ///<summary>Загрузка формы</summary>
        private void frmIllnessHistoryList_Load(object sender, EventArgs e)
        {
            dgDiagAttand.AutoGenerateColumns = false;
            //Заполнение списка категорий пациентов 
            chlbCategory.Items.AddRange(categoris.ToArray());
            //Отметка первой  категории пациентов в списке. Кривовато. Нужно сохранять в настройках последнее состояние работы с формой.
            chlbCategory.SetItemChecked(0, true);
            
            // Заполнение выпадающих списков годами рождения
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                cmbYear1.Items.Add(i);
                cmbYear2.Items.Add(i);
            }
            // Установка начальных значений в выпадающих списках дат рождения
            cmbYear1.SelectedIndex = cmbYear1.Items.Count - 61;
            cmbYear2.SelectedIndex = 0;
            udPage.Minimum = 1; // минимальное число страниц

            chkSeasons.Checked = true; //отмечается разлел сезонов в фильтре
            if (p.trace == true) dgSeasons.Columns["colSeasonID"].Visible = true;//поле показывается только в режиме отладки

            //Загрузка списка сезонов
            LoadSeasons();
            
            //Загрузка списка докторов
            doctors.Load(CommandDirective.AddNonDefined);
            cmbDoctor.DataSource = doctors;

            //Настройка фильтра по терапевтам
            if (Doctor.isdoctor())//Если текущий пользователь является терапевтом
            {
                chkDoctor.Checked = true;//Включается фильтр по докторам
                cmbDoctor.SelectedValue = Security.user.PersonID;//В качестве терапевта указывается текущий пользователь
            }
            if (Security.Access(Groups.Admin, Groups.Manager, Groups.Doctor))//ЗЕФ. 16.01.2012   
                //Ограничение доступа к прерыванию перехода к редактированию истоии болезней. 
                gridIHL.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridIHL_CellDoubleClick);

            Filtrate();
            //Перенесены из frmIllnessHistoryList.Desiner.cs чтобы не вызывалась повторно загрузка списка историй болезней
 
       }

        ///<summary>Загрузка списка сезонов</summary>
        public void LoadSeasons()
        {
            dgSeasons.AutoGenerateColumns = false;
            dgSeasons.DataSource = null;

            _seasons = new Seasons();
            _seasons.Load(CommandDirective.Non);
            
            //Выделение текущего сезона в списке. ЗЕФ. 10.04.2012
            Settings set = new Settings(true);
            set.Load();//Загрузка текущего сезона
            int index = _seasons.FindIndex(delegate(Season s) { return (s.SeasonID == set.CurrentSeason); });//Поиск зваписи с текущим сезоном
            _seasons[index].Chk = true;
            dgSeasons.DataSource = _seasons;
            ((DataGridViewCheckBoxCell)dgSeasons["colChk", index]).Selected = true;//Дополнительно выделяется, чтобы пролистнулся список на отмеченую запись
        }

        ///<summary>Загрузка списка историй болезни с учетом установленного фильтра
        ///обновление 07.05.2013 ЗЕФ. Заменено дерево категорий на список чекбоксов</summary>
        private void Filtrate()
        {
            int cc = 0;//Кол-во историй болезней

            int[] selcats = new int[chlbCategory.CheckedIndices.Count];//Массив равный количеству отмеченых катрегорий
            for (int i = 0; i < chlbCategory.CheckedIndices.Count; i++)
            {
                selcats[i] = categoris[chlbCategory.CheckedIndices[i]].CategoryID;
            }

            //Настройка списка историй болезней в управляющем элементе grid
            gridIHL.AutoGenerateColumns = false;
            gridIHL.DataSource = null;

//            ihs = new IllnessHistoryShort2List();
            //Настройка постраничного вывода
            ihs.Pagination = true;// включаем постраничный вывод
            ihs.PageSize = (int)udPageCount.Value; // устанавливаем текущий размер страницы
            ihs.Page = PageCurrent; // устанавливаем текущую страницу
            
            //Настройка фильтра по сезонам
            var filterSeasons = new List<int>();
            if (chkSeasons.Checked)// если установлен фильтр по сезонам
                //Формируется массив отмеченых сезонов для фильтрации историй болезней
                for (int i = 0; i < dgSeasons.Rows.Count; i++)
                {
                    object o = dgSeasons["colChk", i].Value; //Идет обращение к ячейке в которой находятся чекбоксы

                    if (o != null)
                        if ((bool)o) //Если в этой ячейке true
                            filterSeasons.Add(_seasons[i].SeasonID);//добавляется в массив идентификатор сезона
                }
            // Загрузка списка историй болезни с учетом всех фильтров
            ihs.Load(selcats, 
                rbSexF.Checked,
                cmbYear1.Text == "" ? 0 : int.Parse(cmbYear1.Text), 
                cmbYear2.Text == "" ? 0 : int.Parse(cmbYear2.Text), 
                chkSex.Checked, chkYear.Checked, tstFind.Text,
                filterSeasons.ToArray(), 
                chkSeasons.Checked && dgSeasons.Rows.Count > filterSeasons.Count,
                chkDoctor.Checked, 
                cmbDoctor.SelectedValue == null ? 0 : (int)cmbDoctor.SelectedValue,
                order_fild);
            
            // количество найденых строк
            cc = ihs.Count;
            lblCount.Text = cc.ToString();// вывод общего числа элементов
            lblPageCount.Text = ihs.PageCount.ToString(); // вывод количества страниц
            udPage.Maximum = ihs.PageCount; // максимальное число страниц

            var seasons = new Seasons();

            seasons.Load(filterSeasons);

            gridIHL.DataSource = ihs.Join( seasons, h => h.SeasonID, s=> s.SeasonID, (h, s) => new
            {
                h.DocumentID,
                ihNbr = h.NbrIH +"/"+s.SeasonNbr,
                h.Fullname,
                h.DoctorName,
                h.BirthD,
                h.RegisterD,
                h.Summ
            }).ToList();

            decimal summ = ihs.GetSumm();//Подсчет итоговой суммы по всем историям болезней списка
            txtSumm.Text = summ.ToString("0.00р."); //Вывод суммы стоимости всех историй болезней
            if (cc == 0)
            {
                //Если нет ни одной истории болезни то, чтобы избежать ошибки деления на ноль сразу присваивается ноль.
                txtCostSite.Text = "0.00р.";
               
            }
            else
            {
                //Стоимость одного койкоместа при условии, 
                //что отфильтрован один сезон и продолжительность сезона точно 21 дней. 
                //В перспективе нужно усовершенствовать этот расчет, подсчитывая количество дней с учетом 
                //количества выделеных сезонов и продолжительности каждого из них.
                txtCostSite.Text = (summ/cc/15).ToString("0.00 р./день");
            }
        }

        ///<summary>Выбор позиции в таблице. Обображение диагнозов выбранной истории болезни
        ///Заплатин Е.Ф.
        ///09.04.2012</summary>
        private void gridIHL_SelectionChanged(object sender, EventArgs e)
        {
            if (gridIHL.SelectedRows != null && gridIHL.SelectedRows.Count > 0)
            {
                
                dgDiagMain.AutoGenerateColumns = false;
                dgDiagMain.DataSource = null;
                DiagMain.Load(ihs[gridIHL.SelectedRows[0].Index].DocumentID, false);
                dgDiagMain.DataSource = DiagMain;
                
                dgDiagOut.AutoGenerateColumns = false;
                dgDiagOut.DataSource = null;
                DiagOut.Load(ihs[gridIHL.SelectedRows[0].Index].DocumentID, false);
                dgDiagOut.DataSource = DiagOut;

                dgDiagAttand.AutoGenerateColumns = false;
                dgDiagAttand.DataSource = null;
                DiagAttend.Load(ihs[gridIHL.SelectedRows[0].Index].DocumentID, false);
                dgDiagAttand.DataSource = DiagAttend;
            }
        }

        ///<summary>Открытие выбранной истории болезни</summary>
        private void gridIHL_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridIHL.SelectedRows != null && gridIHL.SelectedRows.Count > 0)
            {
                int curidx = gridIHL.SelectedRows[0].Index;
                frmIllnessHistory frm = new frmIllnessHistory();
                frm.Tag = new Command   (CommandModes.Edit, //Открытие формы в режиме редактирования
                                        ihs[curidx].DocumentID, //Передача идентификатора истории болезни 
                                        this); //Указатель на вызывающую форму
                //frm.IsNew = false;
                frm.ShowDialog(this);//Открытие выбранной истории болезни 
                Filtrate();
                //Установка селектора на элементе списка историй болезней
                if (curidx < ihs.Count && ihs.Count>0)
                {
                    gridIHL.Rows[0].Selected = false;
                    gridIHL.Rows[curidx].Selected = true;
                }
            }
        }

        ///<summary>Кнопка "Перейти"</summary>
        private void tsbView_Click(object sender, EventArgs e)
        {
            gridIHL_CellDoubleClick(sender, null);
        }

        #region Постраничный вывод
        
        ///<summary>Кнопка "Перейти на первую страницу"</summary>
        private void btnPageBegin_Click(object sender, EventArgs e)
        {
            udPage.Value = 1;
        }

        ///<summary>Кнопка "Перейти на предыдущую страницу"</summary>
        private void btnPagePrev_Click(object sender, EventArgs e)
        {
            if (udPage.Value > 1) udPage.Value--;
        }

        //Закоментировал Заплатин пока не работат счетчик страниц
        /////<summary>Изменение значения счетчика текущей страницы</summary>
        //private void udPage_ValueChanged(object sender, EventArgs e)
        //{
        //    PageCurrent = (int)udPage.Value - 1;
        //    LoadEmployees();
        //}

        ///<summary>Кнопка "Перейти на следующую страницу"</summary>
        private void btnPageNext_Click(object sender, EventArgs e)
        {
            if (udPage.Value < udPage.Maximum) udPage.Value++;
        }

        ///<summary>Кнопка "Перейти на последнюю страницу"</summary>
        private void btnPageEnd_Click(object sender, EventArgs e)
        {
            udPage.Value = udPage.Maximum;
        }
        
        //Закоментировал Заплатин пока не работат счетчик страниц
        /////<summary>Изменение количества строк на странице</summary>
        //private void udPageCount_ValueChanged(object sender, EventArgs e)
        //{
        //    LoadEmployees();
        //}

        #endregion

        #region Фильтр

        ///<summary>Изменение чекбокса в дереве категорий</summary>
        private void treeCat_AfterCheck(object sender, TreeViewEventArgs e)
        {
            tstFind.Text = "";
            // выделение дочерних чекбоксов в дереве
            e.Node.Tag = true;
            foreach (TreeNode tn in e.Node.Nodes)
                tn.Checked = e.Node.Checked;
            e.Node.Tag = false;
            if (e.Node.Parent != null && e.Node.Parent.Tag != null && (bool)e.Node.Parent.Tag) return; 
            Filtrate();
        }

        ///<summary>Чекбокс пола</summary>
        private void chkSex_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSex.Checked == true)
            {
                rbSexF.Enabled = true;
                rbSexM.Enabled = true;
            }
            else
            {
                rbSexF.Enabled = false;
                rbSexM.Enabled = false;
            }
        }

        ///<summary>Чекбокс возраста</summary>
        private void chkYear_CheckedChanged(object sender, EventArgs e)
        {
            if (chkYear.Checked == true)
            {
                cmbYear1.Enabled = true;
                cmbYear2.Enabled = true;
            }
            else
            {
                cmbYear1.Enabled = false;
                cmbYear2.Enabled = false;
            }
        }

        ///<summary>Кнопка "Применить фильтр"</summary>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            tstFind.Text = "";//Обнуление поисковой строки
            Filtrate();
        }

        ///<summary>Кнопка "Отменить фильтр"</summary>
        private void btnFilterCancel_Click(object sender, EventArgs e)
        {
            chkSex.Checked = false;
            chkYear.Checked = false;
            chkSeasons.Checked = false;
            chkDoctor.Checked = false;
            tstFind.Text = "";
            Filtrate();
        }

        ///<summary>Кнопка "Найти"</summary>
        private void tsbFind_Click(object sender, EventArgs e)
        {
            chkSex.Checked = false;
            chkYear.Checked = false;
            chkSeasons.Checked = false;
            chkDoctor.Checked = false;
            Filtrate();
        }

        ///<summary>Чекбокс влючения фильтра по сезонам</summary>
        private void chkSeasons_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSeasons.Checked)
            {
                dgSeasons.Enabled = true;
            }
            else
            {
                dgSeasons.Enabled = false;
            }
        }

        ///<summary>Нажатие клавиши Enter в строковом поиске</summary>
        private void tstFind_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                tsbFind_Click(sender, null);
        }

        #endregion
        /// <summary>Изменение порядка сортировки историй болезней
        /// Заплатин Е.Ф.
        /// 09.04.2012
        /// </summary>
        private void gridIHL_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (prev_order_fild_index == e.ColumnIndex)
            {//Повторный клик на заголовке одной и той же колонки подряд
                if (fild_suf == " DESC") fild_suf = "";
                else fild_suf = " DESC";
            }
            else fild_suf = "";//Клик по другой колонке

            prev_order_fild_index = e.ColumnIndex;

            if (e.ColumnIndex == gridIHL.Columns["colIHNbr"].Index)
            {//Организовать сортировку по номерам историй болезней
                order_fild = "NbrIH" + fild_suf;
            }
            else if (e.ColumnIndex == gridIHL.Columns["colName"].Index)
            {//Организовать сортировку по ФИО
                order_fild = "LastName" + fild_suf;
            }
            else if (e.ColumnIndex == gridIHL.Columns["colBirthD"].Index)
            {//Организовать сортировку по сумме
                order_fild = "BirthD" + fild_suf;
            }

            Filtrate();
        }
        /// <summary>Книжка пациента по выбранной истории болезни для печати
        /// Заплатин Е.Ф.
        /// </summary>
        private void книжкаНазначенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridIHL.SelectedRows.Count <= 0) return;
            rptPatientBook rpt = new rptPatientBook();
            //В качестве параметра передается выбранный элемент из списка историй болезни
            rpt.Tag = new Command(CommandModes.Open, ihs[gridIHL.SelectedRows[0].Index].DocumentID, this);
            rpt.ShowDialog(this);

        }
        /// <summary>Отчет по выбранной истории болезни для печати
        /// Заплатин Е.Ф.
        /// </summary>
        private void историяБолезниToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridIHL.SelectedRows.Count <= 0) return;
            rptIllnessHistory rpt = new rptIllnessHistory();
            //В качестве параметра передается выбранный элемент из списка историй болезни
            rpt.Tag = new Command(CommandModes.Open, ihs[gridIHL.SelectedRows[0].Index].DocumentID, this);
            rpt.ShowDialog(this);

        }

        private void cmbDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkDoctor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDoctor.Checked)
            {
                cmbDoctor.Enabled = true;
            }
            else
            {
                cmbDoctor.Enabled = false;
            }

        }
        /// <summary>Создание шаблона на основе выбранной истории болезни
        /// Заплатин Е.Ф.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbCreateTemplate_Click(object sender, EventArgs e)
        {
            if (gridIHL.SelectedRows.Count <= 0) return;
            string value = "";
            string msg = "Для создания шаблона на основе выделенной истории болезни задайте его наименование.";
            frmInputText frm = new frmInputText(msg, "Создание шаблона истории болезни",  value);
            bool friest = true;
            do //повторять вывод диалогового окна, пока не будет введено имя шаблона или не будет нажата отмена
            {

                if (!friest) frm.LabelText = msg + "\r\n\r\nНе указано наименование шаблона."; 
                if (DialogResult.OK != frm.ShowDialog()) return;
                friest = false;

            } while (frm.Value == "");
            
            
            IllnessHistoryTemplate iht = new IllnessHistoryTemplate(true);
            iht.DocumentID = ihs[(int)gridIHL.SelectedRows[0].Index].DocumentID;
            iht.Date = DateTime.Now;
            iht.Name = frm.Value;
            iht.EmploeeyID = Security.user.UserID;
            iht.Sex = ihs[(int)gridIHL.SelectedRows[0].Index].Sex;
            if (iht.Insert())
            {
                MessageBox.Show("Шаблон " + iht.Name + " успешно создан", "Завершение создания шаблона", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        /// <summary>Отчет - Журнал регистрации пациентов по сезонам
        /// Заплатин Е.Ф.
        /// 29.02.2012
        /// </summary>
        private void отчетПоИсториямБолезнейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptIllnessHistoryList frm = new rptIllnessHistoryList();
            frm.Text = "Журнал регистрации пациентов";
            frm.Show();
        }

        private void списокСтудентовПоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptIllnessHistoryList frm = new rptIllnessHistoryList();
            frm.Text = "Список студентов по факультетам";
            frm.Show();
        }

        private void обратныйТалонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridIHL.SelectedRows.Count <= 0) return;
            rptReverseTicket rpt = new rptReverseTicket();
            rpt.Tag = new Command(CommandModes.Open, ihs[gridIHL.SelectedRows[0].Index].DocumentID, this);
            rpt.ShowDialog(this);
        }
        /// <summary>Перегрузка списка и.б. после выбора категории пациента
        /// Заплатин Е.Ф.
        /// 07.05.2013
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chlbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filtrate();
        }

        private void menuItemShowIdIh_Click(object sender, EventArgs e)
        {
            gridIHL.Columns["colId"].Visible = menuItemShowIdIh.Checked;
        }

        private void deleteIHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var id = gridIHL.GetSelectedItemColumnValue<int>("colId");
            if (id == 0) return;//не выбран ни один элемент
                                //Checking what IH don't have any linked entities: medicine, directions, obcervations 

            if(MessageBox.Show($"Удалить историю болезни?", "Удаление истории болезни",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (IllnessHistory.Delete(id, CommandDirective.MarkAsDeleted))
                {
                    MessageBox.Show("История болезни успешно удалена.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Filtrate();
                }

            }
            else
            {
                MessageBox.Show("Не удалось удалить историю болезни." +
                                " Возможно у нее имеются связанные записи: назначения, обследования." +
                                " В такм случае удалите сначала связанные записи.",
                                "Ошибка удаления", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
