﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Forms;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;


namespace Medsyst
{
    public partial class frmOptions : Form
    {
        Seasons sss = new Seasons();//Список сезонов
        Seasons sss1 = new Seasons();//Список сезонов (вспомогательный для поля текущего сезона)
        Settings set = new Settings(true);// Настройки системы
        /// <summary>список категорий пациентов</summary>
        PatientCategoryList pcategorys = new PatientCategoryList();
        MedicalFirmList firms = new MedicalFirmList();
        MemberList members = new MemberList();
        
        public frmOptions()
        {
            InitializeComponent();
        }

        private void frmOptions_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "datIllnessHistory.DirectionType". При необходимости она может быть перемещена или удалена.
            set.FormBinding("set", this);//Привязка объекта к полям формы
            treeOptionsGroup.HideSelection = false;
            gridSeasons.AutoGenerateColumns = false;
            set.Load();//Загрузка настроек системы
            
            
            //udCurrentYear.Value = set.CurrentYear;//Устанавливается текущий год. Непонятно зачем нужно было такое усложнение  set.CurrentYear< 2009 ? 2009 : set.CurrentYear;?
           
            //Загрузка значений в список текущего сезона
            sss1.Load(set.CurrentYear);
            cmbCurrentSeason.DataSource = null;
            cmbCurrentSeason.DataSource = sss1;
            
            //Загрузка категорий пациентов
 
            intYear.Value = set.CurrentYear; //Год для фильтрации списка сезонов устанавливать в текущее значение
            LoadSeasons(); //Загрузка списка сезонов по указанному году

            cmbMembers.DataSource = null;
            members.Load(CommandDirective.AddNonDefined);
            cmbMembers.DataSource = members;

            ResetMedicalCmbSource(); //Загрузка списка медучреждений. Чирков Е.О., 11.04.10
            set.WriteToForm();
        }
        
        
        /// <summary>
        /// Загрузка списков сезонов
        /// </summary>
        void LoadSeasons()
        {
            //Обновление списка сезонов с фильтром по установленному году
            gridSeasons.DataSource = null; //Без обнуления присвоение нового источника данных не работает
            sss.Load((int)intYear.Value);//Загрузка коллекции сезонов по установленному году
            gridSeasons.Columns["ID"].Visible = false;//Не отображается поле с идентификатором сезона. Можно отображать его для тестирования.
            gridSeasons.DataSource = sss; //Указание комбобоксу в качестве источника данных заполненной коллекции
            
             
        }

        /// <summary>
        /// Загрузка медицинских учреждений в комбинированный список
        /// </summary>
        void ResetMedicalCmbSource()
        {
            cmbMedical.DataSource = null;
            //firms = new MedicalFirmList();
            firms.Load(CommandDirective.AddNonDefined);
            cmbMedical.DataSource = firms;
        }

        private void treeOptionsGroup_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.tabControl1.SelectTab(int.Parse(treeOptionsGroup.SelectedNode.Name));

        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            this.treeOptionsGroup.SelectedNode = treeOptionsGroup.Nodes.Find(e.TabPage.Name.Substring(7), false)[0];

        }

        private void btnAddMedicine_Click(object sender, EventArgs e)
        {
            frmSeason childForm = new frmSeason();
            childForm.Tag = new Command(CommandModes.New,(int)intYear.Value);
            childForm.ShowDialog(this);
            LoadSeasons();
            
        }
        //Изменения года по которому фильтруется список сезонов
        private void intYear_ValueChanged(object sender, EventArgs e)
        {
           /* //Загрузка значений в список текущего сезона
            cmbCurrentSeason.DataSource = null;
            sss1.Load(set.CurrentYear);
            cmbCurrentSeason.DataSource = sss1;
             
            Мне кажется это лишнее. Зачем менять настройки системы, если ведется работа с сезонами? ЗЕФ.
            Settings set1 = new Settings(true);
            set1.CurrentYear = (int)udCurrentYear.Value;
            set.CurrentYear = (int)udCurrentYear.Value;
            set1.Update();
            */
            LoadSeasons();//Обновление списков сезонов
             
        }
        /// <summary>
        /// Изменение значения текущего года приводит к обновлению списка поля текущего сезона
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void udCurrentYear_ValueChanged(object sender, EventArgs e)
        {
            //Загрузка значений в список текущего сезона
            cmbCurrentSeason.DataSource = null;
            sss1.Load((int)udCurrentYear.Value);//Загружается коллекция сезонов по вновь установленному значению текушего года
            cmbCurrentSeason.DataSource = sss1;
            dataSaved = false;
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridSeasons.SelectedRows == null || gridSeasons.SelectedRows.Count <= 0)
                return;
            frmSeason childForm = new frmSeason();
            childForm.Tag = new Command(CommandModes.Edit, sss[gridSeasons.SelectedRows[0].Index].SeasonID);
            childForm.ShowDialog(this);
            LoadSeasons();
        }

        private void gridSeasons_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmSeason childForm = new frmSeason();
            childForm.Tag = new Command(CommandModes.Edit, sss[e.RowIndex].SeasonID);
            childForm.ShowDialog(this);
            LoadSeasons();
        }
        /// <summary>
        /// Установка значения текущего сезона и текущего года. Обращается внимание, что значение текущего года не может
        /// быть высталено при изменении поля счетчика текущего года. Оно изменяется лишь в том случае, когда 
        /// имеется как минимум один сезон в этом году и он выбран в поле списка текущего сезона
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCurrentSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
                dataSaved = false;
        }

        //Чирков Е.О.
        //28.04.10
        private void btnAddMedical_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Caption = "Добавить заведение";
            frm.Text = "Название лечебного заведения:";
            if (frm.ShowDialog() != DialogResult.OK) return;
            MedicalFirm f = new MedicalFirm(true);
            f.FirmN = frm.Value;
            f.Insert();
            ResetMedicalCmbSource();
            cmbMedical.SelectedValue = f.Identity("Firm");
        }

        private void btnDeleteMedical_Click(object sender, EventArgs e)
        {
            if (cmbMedical.SelectedValue == null) return;
            MedicalFirm f = new MedicalFirm(true);
            f.FirmID = (int)cmbMedical.SelectedValue;
            f.Load();
            f.Deleted = true;
            f.Update();
            ResetMedicalCmbSource();
        }

        //28.04.10
        //Чирков Е.О.
        //Закрытие формы при нажатии "Готово"
        private void toolStripOk_Click(object sender, EventArgs e)
        {
            //Сохранение внесенных изменений в настройки системы
            if (!dataSaved)
            {
                set.ReadFromForm();//Считывание измененных данных
                set.Update();
            }
            DialogResult = DialogResult.OK;
            if (IsMdiChild) Close();
        }

        //28.04.10
        //Чирков Е.О.
        private void btnEditMedical_Click(object sender, EventArgs e)
        {
            if (cmbMedical.SelectedValue == null) return;
            MedicalFirm f = new MedicalFirm(true);
            f.FirmID = (int)cmbMedical.SelectedValue;
            f.Load();
            InputBox frm = new InputBox();
            frm.Caption = "Изменить название заведения";
            frm.Text = "Название лечебного заведения:";
            frm.Value = f.FirmN;
            if (frm.ShowDialog() != DialogResult.OK) return;
            
            f.FirmN = frm.Value;
            f.Update();
            ResetMedicalCmbSource();
            cmbMedical.SelectedValue = f.FirmID;
        }
        /// <summary>
        /// Удаление сезона из списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelSeason_Click(object sender, EventArgs e)
        {
            //Проверяется выделен ли хотябы один сезон в списке
            if (gridSeasons.SelectedRows != null || gridSeasons.SelectedRows.Count > 0)
            {
                set.Load();//Загрузка настроек системы
                //Проверка не является ли удаляемый сезон текущей настройкой системы
                if ((int)gridSeasons.SelectedRows[0].Cells["ID"].Value==set.CurrentSeason)
                {
                   System.Windows.Forms.MessageBox.Show ("Невозможно удалить текущий сезон", "Удаление сезона",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                   return;
                }
                if (MessageBox.Show("Будьте внимательны, возможно в этом сезоне уже выписаны истории болезей пациентов. Вы действительно хотите удалить " + sss[gridSeasons.SelectedRows[0].Index].SeasonNbr + " сезон? ", "Подтверждение удаления сезона", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) != DialogResult.Yes) return;
                //Производится удаление сезона
                sss[gridSeasons.SelectedRows[0].Index].Delete();
                LoadSeasons();//Перезагружается список сезонов после удаления
            }

        }

        private void cmbMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataSaved = false;
        }

        private void cmbMedical_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataSaved = false;
        }
        /// <summary>Чистка удаленных назначений из базы данных
        ///  Заплатин Е.Ф.
        ///  18.01.2013
        /// </summary>
        private void btnCleanDeleted_Click(object sender, EventArgs e)
        {
            string SQLcommand = "";
            string head = "Результат чистки базы данных от удаленных назначений";//Заголовок диалогового окна с сообщением результата.
            string msg = "";
            int res = 0; //Результирующее количество удаленных строк

            //Шаг 1. Удаление расходных медикаментов отмеченных как удаленные и не связанные с услугами
                    SQLcommand = "DELETE FROM [Medsyst_test].[dbo].[MedicamentOutput] "+
                                 "WHERE DeletedOut = 1 AND ServiceOutputID = 0";
                    DB cmd = new DB(SQLcommand);
                    res += cmd.ExecuteQuery();    
            
            //Шаг 2. Удаление расходных медикаментов связанных с услугами отмеченных как удаленные
                    SQLcommand = "DELETE FROM MedicamentOutput WHERE MedicamenOutputID in " +
                                 "(SELECT  MedicamenOutputID FROM MedicamentOutput WHERE ServiceOutputID in "+
							     "(SELECT ServiceOutputID FROM ServiceOutput WHERE Deleted = 1 AND Fulfil = 0 ))";
                    cmd.Create(SQLcommand);//Обновление командной строки для выполнения нового запроса
                    res += cmd.ExecuteQuery();  

            //Шаг 3. Удаление назначенных услуг отмеченных как удаленные
                    SQLcommand = "DELETE FROM ServiceOutput WHERE Deleted = 1";
                    cmd.Create(SQLcommand);//Обновление командной строки для выполнения нового запроса
                    res += cmd.ExecuteQuery();

            //Шаг 4. Удаление медикаментов назначенных по требованиям, которые ссылаются на несуществующие требования
            //TO DO        
                    /*DELETE FROM OnDemandMedicine
                      WHERE MedicamentByDemandID in (SELECT mbd.MedicamentByDemandID
                                          FROM [Medsyst_test].[dbo].[OnDemandMedicine] as mbd
                                          Left JOIN Document as d on d.DocumentID = mbd.DemandID
                                          where d.DocumentID is null)
                    */

            msg = "Удалено " + res.ToString() + " назначений."; //результат чистки
            MessageBox.Show(msg, head);//сообщается результат выполнения операции сжатия

        }
        /// <summary>Чистка назначений с нулевым количеством из базы данных
        ///  Заплатин Е.Ф.
        ///  19.01.2013
        /// </summary>
        private void btnCleanZero_Click(object sender, EventArgs e)
        {
            string SQLcommand = "";
            string head = "Результат чистки базы данных от нулевых назначений";//Заголовок диалогового окна с сообщением результата.
            string msg = "";
            int res = 0; //Результирующее количество удаленных строк
            
            //Шаг 1. Удаление расходных медикаментов связанных с услугами с нулевым количеством
                 SQLcommand = "DELETE FROM MedicamentOutput WHERE ServiceOutputID in " +
	                          "(Select ServiceOutputID FROM ServiceOutput WHERE [Count] = 0)";
                 DB cmd = new DB(SQLcommand);
                 res += cmd.ExecuteQuery();
            
            //Шаг 2. Удаление расходных медикаментов с нулевым количеством не связанных с услугами.
                SQLcommand = "DELETE FROM MedicamentOutput WHERE CountOutput = 0 AND ServiceOutputID = 0";
                cmd.Create(SQLcommand);//Обновление командной строки для выполнения нового запроса
                res += cmd.ExecuteQuery();
            
            //Шаг 3. Удаление назначенных услуг с нулевым значением
                SQLcommand = "DELETE FROM ServiceOutput WHERE [COUNT] = 0";
                cmd.Create(SQLcommand);//Обновление командной строки для выполнения нового запроса
                res += cmd.ExecuteQuery();

                msg = "Удалено " + res.ToString() + " назначений."; //результат чистки
            MessageBox.Show(msg, head);//сообщается результат выполнения операции сжатия
        }
        /// <summary>Сжатие базы данных
        ///  Заплатин Е.Ф.
        ///  19.01.2013
        /// </summary>
        private void btnCompress_Click(object sender, EventArgs e)
        {
            //TODO перенести этот код в метод класса
            
            DB cmd = new DB("DBCC SHRINKDATABASE(0)");//Параметр - 0 означает обращение к текущей базе данных 
                                                      //вместо него можно использовать обращение к базе по названию - N'Medsyst_test'
            string head = "Результат сжатия базы данных";//Заголовок диалогового окна с сообщением результата.
            string msg = "";
            if (!cmd.ExecuteNonQuery())
                msg= cmd.Error;//если результат сжатия отрицательный
             else
               msg = "Cжатие базы данных прошло успешно.";
            
            MessageBox.Show(msg, head);//сообщается результат выполнения операции сжатия
        }

        /// <summary>Резервное копирование базы данных
        ///  Заплатин Е.Ф.
        ///  21.01.2013
        /// </summary>
        private void btnBackupBD_Click(object sender, EventArgs e)
        {
            string SQLCommand = "";
            string head = "Результат резервирования базы данных";//Заголовок диалогового окна с сообщением результата.
            string msg = "";

            //Запрашивается место для сохранения резервной копии на локальном диске
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Medsyst " + DateTime.Now.ToShortDateString() + ".bak";
            if (DialogResult.OK != sf.ShowDialog()) return;//Произведен отказ от сохранения резервной копии
            
            SQLCommand = "BACKUP DATABASE Medsyst TO  DISK = N'" + sf.FileName + "' "+
                "WITH NOFORMAT, NOINIT,  NAME = N'Medsyst-Полная База данных Резервное копирование', " +
                "SKIP, NOREWIND, NOUNLOAD,  STATS = 10"; //SamsungQ70\\SQLEXPRESS\\
            DB cmd = new DB(SQLCommand);

            if (!cmd.ExecuteNonQuery())
                msg = cmd.Error;//если результат резервирования отрицательный
            else
                msg = "Резервирование базы данных прошло успешно.";

            MessageBox.Show(msg, head);//сообщается результат выполнения операции резервирования
            

        }
        /// <summary>Восстановление резервной копии базы данных
        ///  Заплатин Е.Ф.
        ///  22.01.2013
        /// </summary>
        private void btnRestoreDB_Click(object sender, EventArgs e)
        {
            string head = "Результат восстановления базы данных";//Заголовок диалогового окна с сообщением результата.
            string msg = "";
            string SQLCommand = "";//Командная SQL строка
            //Запрашивается место расположения резервной копии на локальном диске
            OpenFileDialog of = new OpenFileDialog();
            if (DialogResult.OK != of.ShowDialog()) return;//Произведен отказ отврытия резервной копии
            SQLCommand = "RESTORE DATABASE [Medsyst] FROM  DISK = N'" + of.FileName + "' WITH  FILE = 1,  NOUNLOAD,  STATS = 10";
            DB cmd = new DB(SQLCommand);

            if (!cmd.ExecuteNonQuery())
                msg = cmd.Error;//если результат восстановления отрицательный
            else
                msg = "Восстановление базы данных прошло успешно.";

            MessageBox.Show(msg, head);//сообщается результат выполнения операции восстановления

        }
        /// <summary>Чистка документов в базе данных
        /// Заплатин Е.Ф.
        /// 05.02.2013
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteDocuments_Click(object sender, EventArgs e)
        {
            //Удаление историй болезней не свзанных с амбулаторными картами
            /* DELETE FROM Document
                    WHERE DocumentID in (SELECT ih.DocumentID
		                                 FROM IllnessHistory as ih
		                                 where ih.AmbCardID = 0)
             * DELETE FROM IllnessHistory
                      WHERE DocumentID in (SELECT ih.DocumentID
		                                      FROM IllnessHistory as ih
		                                      where ih.AmbCardID = 0)
             */


        }

       
    }
}
