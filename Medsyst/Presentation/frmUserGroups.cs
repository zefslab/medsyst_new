﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmUserGroups : Form
    {
        UserGroups groups = new UserGroups();

        public frmUserGroups()
        {
            InitializeComponent();
        }

        //Чирков Е.О.
        //15.03.10
        private void frmUserGroups_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            groups.Load();
            dataGridView1.DataSource = groups;
        }

        //Чирков Е.О.
        //15.03.10
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null & dataGridView1.SelectedRows.Count > 0)
            {
                Tag = new Command(CommandModes.Return, groups[dataGridView1.SelectedRows[0].Index].UGroupID);
                Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            toolStripButton1_Click(sender, e);
        }
    }
}
