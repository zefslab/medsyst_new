﻿//Чирков Е.О.
//18.03.2010
//Форма для смены пароля пользователя системы
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmUserPasswordChange : Form
    {
        User user = new User();
        public frmUserPasswordChange()
        {
            InitializeComponent();
        }

        //Чирков Е.О.
        //22.03.10
        //Смещение элемента управления на dy точек по вертикали
        void OffsetControl(Control ctrl, int dy)
        {
            ctrl.Location = new Point(ctrl.Left, ctrl.Top + dy);
        }

        private void frmPasswordChange_Load(object sender, EventArgs e)
        {
            //Чирков Е.О.
            //22.03.10
            //Администратору не нужно вводить старый пароль
            if (Security.user.UGroupIDs.Contains((int)Groups.Admin))
            {
                label1.Visible = false;
                txtOldPassword.Visible = false;

                int h = label2.Top - label1.Top;
                OffsetControl(label2, -h);
                OffsetControl(txtNewPassword, -h);
                OffsetControl(label3, -h);
                OffsetControl(txtReNewPassword, -h);
                Size = new Size(Size.Width, Size.Height - h);
            }

            int UserID = 0;
            if ((Tag != null) && (Tag is Command)) UserID = cmd.Id;
            else return;

            user.PersonID = UserID;
            user.UserID = UserID;
            user.Load();
            Text += " для '" + user.Login + "'";
        }

        private void toolbtnReady_Click(object sender, EventArgs e)
        {
            //Чирков Е.О.
            //22.03.10
            //Неадминистратору нужно вводить старый пароль
            if (!Security.user.UGroupIDs.Contains((int)Groups.Admin))
            {
                if (Security.GetMD5Hash(txtOldPassword.Text) != user.Password)
                {
                    MessageBox.Show("Неверно введен старый пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtOldPassword.Text = "";
                    return;
                }
            }
            if (txtNewPassword.Text == "")
            {
                MessageBox.Show("Пустые пароли запрещены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (txtNewPassword.Text != txtReNewPassword.Text)
            {
                MessageBox.Show("Неверно подтвержден пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            user.Password = txtNewPassword.Text;
            user.Update();
            MessageBox.Show("Пароль успешно изменен", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);

            Tag = new Command(CommandModes.Return, user.UserID, this);
            DialogResult = DialogResult.OK;
            if (IsMdiChild) Close();
        }
    }
}
