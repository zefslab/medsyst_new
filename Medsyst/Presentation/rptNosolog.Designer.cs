﻿namespace Medsyst
{
    partial class rptNosolog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rbtnWorkers = new System.Windows.Forms.RadioButton();
            this.rbtnStudents = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSeasonEnd = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSeasonBegin = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExecutReport = new System.Windows.Forms.Button();
            this.nosolog1 = new Medsyst.Reports.Nosolog();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.ShowParameterPanelButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(714, 528);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rbtnWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnStudents);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeasonEnd);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeasonBegin);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.btnExecutReport);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.crystalReportViewer1);
            this.splitContainer1.Size = new System.Drawing.Size(893, 528);
            this.splitContainer1.SplitterDistance = 175;
            this.splitContainer1.TabIndex = 1;
            // 
            // rbtnWorkers
            // 
            this.rbtnWorkers.AutoSize = true;
            this.rbtnWorkers.Location = new System.Drawing.Point(62, 129);
            this.rbtnWorkers.Name = "rbtnWorkers";
            this.rbtnWorkers.Size = new System.Drawing.Size(84, 17);
            this.rbtnWorkers.TabIndex = 84;
            this.rbtnWorkers.Text = "Сотрудники";
            this.rbtnWorkers.UseVisualStyleBackColor = true;
            // 
            // rbtnStudents
            // 
            this.rbtnStudents.AutoSize = true;
            this.rbtnStudents.Checked = true;
            this.rbtnStudents.Location = new System.Drawing.Point(62, 106);
            this.rbtnStudents.Name = "rbtnStudents";
            this.rbtnStudents.Size = new System.Drawing.Size(73, 17);
            this.rbtnStudents.TabIndex = 83;
            this.rbtnStudents.TabStop = true;
            this.rbtnStudents.Text = "Студенты";
            this.rbtnStudents.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 82;
            this.label4.Text = "Пациенты";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 81;
            this.label2.Text = "по";
            // 
            // cmbSeasonEnd
            // 
            this.cmbSeasonEnd.DisplayMember = "FullNbr";
            this.cmbSeasonEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasonEnd.FormattingEnabled = true;
            this.cmbSeasonEnd.Location = new System.Drawing.Point(62, 55);
            this.cmbSeasonEnd.Name = "cmbSeasonEnd";
            this.cmbSeasonEnd.Size = new System.Drawing.Size(108, 21);
            this.cmbSeasonEnd.TabIndex = 80;
            this.cmbSeasonEnd.ValueMember = "SeasonId";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 79;
            this.label1.Text = "с";
            // 
            // cmbSeasonBegin
            // 
            this.cmbSeasonBegin.DisplayMember = "FullNbr";
            this.cmbSeasonBegin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasonBegin.FormattingEnabled = true;
            this.cmbSeasonBegin.Location = new System.Drawing.Point(62, 28);
            this.cmbSeasonBegin.Name = "cmbSeasonBegin";
            this.cmbSeasonBegin.Size = new System.Drawing.Size(108, 21);
            this.cmbSeasonBegin.TabIndex = 78;
            this.cmbSeasonBegin.ValueMember = "SeasonId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 77;
            this.label3.Text = "Сезоны";
            // 
            // btnExecutReport
            // 
            this.btnExecutReport.Location = new System.Drawing.Point(12, 161);
            this.btnExecutReport.Name = "btnExecutReport";
            this.btnExecutReport.Size = new System.Drawing.Size(155, 23);
            this.btnExecutReport.TabIndex = 76;
            this.btnExecutReport.Text = "Гененрировать отчет";
            this.btnExecutReport.UseVisualStyleBackColor = true;
            this.btnExecutReport.Click += new System.EventHandler(this.btnExecutReport_Click);
            // 
            // rptNosolog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 528);
            this.Controls.Add(this.splitContainer1);
            this.Name = "rptNosolog";
            this.Text = "Отчет по нозологическим формам";
            this.Load += new System.EventHandler(this.rptNosolog_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cmbSeasonBegin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExecutReport;
        private Reports.Nosolog nosolog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbSeasonEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnWorkers;
        private System.Windows.Forms.RadioButton rbtnStudents;
        private System.Windows.Forms.Label label4;

    }
}