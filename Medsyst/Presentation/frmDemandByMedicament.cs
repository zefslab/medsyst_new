﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmDemandByMedicament : Form
    {
        ///<summary>Требование на склад</summary>
        DemandMedicament demand = new DemandMedicament(true);
        MedicamentByDemandList mbds = new MedicamentByDemandList();
        /// <summary>Временно удаленные медикаменты на момент работы с формой для возможности их восстановления
        /// если выйти без сохранения изменений в форме</summary>
        MedicamentByDemandList mbds_deleted = new MedicamentByDemandList();        
        ///<summary>Сотрудники профилактория</summary>
        MemberList members = new MemberList();
        /// <summary>Единицы измерений</summary>
        Measures measures = new Measures();
        /// <summary>Сезоны</summary>
        Seasons ss = new Seasons();
        Settings set = new Settings(true);// Настройки системы

        public frmDemandByMedicament()
        {
            InitializeComponent();
        }
        /// <summary>Загрузка формы
        /// Заплатин Е.Ф.
        /// 16.08.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDemandByMedicament_Load(object sender, EventArgs e)
        {
            gridMeds.AutoGenerateColumns = false;
            demand.FormBinding("demand", this);

            set.Load();//Загрузка настроек системы для получения в последствии текущего сезона

            //Загружается список сотрудников для выбора ответственного лица за требование
            cmbMembers.DataSource = null;
            members.Load(CommandDirective.AddNonDefined);
            cmbMembers.DataSource = members;
 
            //Загрузка коллекции единиц измерений для подстановки в соответсвующий столбец
            colMeasures.ValueMember = "MeasureID";
            colMeasures.DisplayMember = "ShotName";
            colMeasures.DataSource = null;
            measures.Load();
            colMeasures.DataSource = measures;

            //Загрузка сезонов
            cmbSeason.DataSource = null;
            ss.Load(CommandDirective.AddNonDefined);
            cmbSeason.DataSource = ss;

            cmd = Tag as Command;//Принятие интерфейса от вызывающей формы
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
            {//Создание нового требования на склад
                demand.RegisterD = DateTime.Now;
                string doctypes = ((int)DocTypes.DemandMaterial).ToString() + "," +
                                  ((int)DocTypes.DemandMedicament).ToString() + "," +
                                  ((int)DocTypes.DemandService).ToString(); //Формируется список типов документов (все требования) у которых следует обеспечить сквозную нумерацию
                demand.Nbr = demand.GetNewNumber(doctypes, false);
                demand.SeasonID = set.CurrentSeason;
                demand.Insert();
            }
            else
            {//загрузка уже существующего требования на склад
                demand.DocumentID = cmd.Id;
                demand.Load();
                gridMeds.DataSource = null;//Обнуление поля источника данных для присвоение нового значения
                mbds = demand.LoadMed();
                gridMeds.DataSource = mbds;
                
                rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = !(mbds.Count > 0);//Если в требование уже добавлены медикаменты из определенного
                //источника финансирования, то изменять свойство источника финансирования самого требования недозволено
                
                SetFormControls();//ЗЕФ. 2012.05.10
                
            }
            demand.WriteToForm();
            //Вынужденное добавление этого условия после метода WriteToForm. Иначе этот флаг всегда будет - false
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
                DataSaved(false);
            else
                DataSaved(true);

        }
        /// <summary>В зависимости от состояния свойств: Signed устанавливается доступность элементов управления
        /// Заплатин Е.Ф.
        /// 10.05.2012
        /// </summary>
        private void SetFormControls()
        {
            if (demand.Signed)
            {//Документ был уже подписан, поэтому внесение изменений недопустимо
                //элементы управления по внесению изменений в подписанное требование становятся недоступны
                btnSign.Text = "Снять подпись";
                btnAddAllMedicine.Enabled = btnDel_Medicine.Enabled = false;
                colCount.ReadOnly = true;
                cmbMembers.Enabled = txtNbr.Enabled = dtpDate.Enabled = cmbSeason.Enabled = false;
                btnSave.Enabled = false;
            }
            else
            {
                btnSign.Text = "Подписать";
                btnAddAllMedicine.Enabled = btnDel_Medicine.Enabled = true;
                colCount.ReadOnly = false;
                cmbMembers.Enabled = txtNbr.Enabled = dtpDate.Enabled = cmbSeason.Enabled = true;
                btnSave.Enabled = true;
            }
            rbtnNotBudget.Enabled = rbtnNotBudget.Enabled = mbds.Count == 0; //Доступно к изменению только если нет медикаментов в требовании
        }
 
        /// <summary>TODO Эту логику изменения статуса и доступности кнопки "Сохранить" лучше всего перенести в родительскую форму
        /// Заплатин Е.Ф.
        /// 02.09.2011
        /// </summary>
        /// <param name="saved"></param>
        public void DataSaved(bool saved)
        {
            dataSaved = saved;
            btnSave.Enabled = !saved;
        }
        /// <summary>Добавление всех медикаментов выписанных по историям болезней, но еще не выданных,
        /// с учетом уже выписанных ранее требований
        /// Заплатин Е.Ф.
        /// 09.08.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddAllMedicine_Click(object sender, EventArgs e)
        {

            if (!dataSaved)
            {//Если данные не сохранены в ребовании
                string txtMsg = "В требовании не сохранены внесенные изменения. \n\rРекомендуется перед выполнением " +
                    "операции поиска и добавления назначенных медикаментов, сохранить возможные изменения с медикаментами. " +
                    "В противном случе могу не обнаружиться назначеные медикаменты или наоборот добавиться дублирующие.\n\r\n" +
                    "Выполнить сохранение последних изменений в требовании перед началом поиска и добавления?";

                if (DialogResult.Yes == MessageBox.Show(txtMsg ,
                    "Не сохраненные изменения", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    Save();
                }
            }
        //Шаг 1. Формируется коллекция назначенных, неудаленнх медикаментов с ненулевым расходным количеством  по неудаленным расходным документам указанного типа
            // и у которых не стоит отметка о выдачи пациенту, принадлежащих к заданному источнику финансирования
            OutputMedicamentList mos = new OutputMedicamentList();
            List <int> list = new List<int>();//Список типов расходных документов по которым нужно искать медикаменты
            list.Add ((int)DocTypes.IllnessHistory);//Используется пока только один тип расходных документов
            mos = demand.LoadOutputMeds();
            if (mos.Count == 0)
            {//Не нашлось ни одного медикамента назначенного пациенту.
                MessageBox.Show("Не найдено ни одного назначенного медикамента пациентам по указанному источнику финансирования.",
                                "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
        //Шаг 2. Формируется коллеекция медикаментов по требованию на основе 
            //назначенных с учетом уже имеющиеся "на руках" у мед персонала, полученных по предыдущим требованиям. 
            //Для этого из числа найденных на предыдущем шаге медикаментов находятся те, 
            //которые выданы мед. персоналу по требованиям и у которых 
            //остаток больше нуля и они не назначены по услугам
            //Вычисляется количество недостающих медикаментов, которое будет рекомендовано
            //к выписке по текущему требованию
            Finance f;
            if (demand.IsBudget) f = Finance.Budget; else f = Finance.ExtraBudget;
            MedicamentByDemandList mbds_new = demand.AddMeds(mos.GroupByMedicament(), f);
            if (mbds_new.Count > 0)
            {//Сформирована коллекция рекомендуемых к выдаче медикаментов по требованию
                mbds.AddRange(mbds_new);
                DataSaved(false);
            }
         
        //Шаг 3. Заполнение грида коллекцией рекомендуемых к выписке медикаментов
            gridMeds.DataSource = null;
            gridMeds.DataSource = mbds;
            rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = !(mbds.Count > 0);//Если в требование уже добавлены медикаменты из определенного
            //источника финансирования, то изменять свойство источника финансирования самого требования недозволено
         }
 
        ///<summary>Кнопка "Подписать"
        ///Заплатин Е.Ф.
        ///09.08.2011</summary>
        private void btnSign_Click(object sender, EventArgs e)
        {
           if(demand.Signed)
            {//Документ уже был подписан
                demand.Sign();//Снятие подписи
                SetFormControls();//Элементы управления делаются доступными
            }
            else
            {//Документ небыл подписан
            //Прежде чем закрывать форму, поставить подпись, подтвердающую завершение формирование требования.
            //После того как будет поставлена подпись, внесение изменений станет невозможным
            DialogResult res = MessageBox.Show("Выберите - Да, если Вы уверены в том, что хотите подписать требование. " +
                        " После наложения подписи внесение изменений в требование и его удаление станет невозможным!\n\r" +
                        " Выберите - Нет, для сохранения изменений и завершения редактирования требования без наложения подписи.\n\r" +
                        " Выберите - Отмена, для продолжения редактирования требования.", "Подпись документа",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {//Сохранение изменений с подписью
                demand.Sign();
            }
            else if (res == DialogResult.No)
            {//Сохранение документа без подписи

            }
            else if (res == DialogResult.Cancel)
            {//Продолжение редактирования документа
                return;
            }
            Save();
            Close();
           }
        }
         ///<summary>Сохранение изменений в форме. Все изменения и дополнения коллекции медикаментов принимают силу и сохраняются в базе
        ///Заплатин Е.Ф.
        ///09.08.2011</summary>       
        private void Save()
        {
            demand.ReadFromForm();
            demand.Update();
            //Простановка отметки об удалении всех удаленных медикаментов в текущем сеансе работы с требованием 
            if (!demand.DeleteMeds(ref mbds_deleted, CommandDirective.MarkAsDeleted))
                MessageBox.Show("Не удалось удалить медикаменты по требованию, поэтому не возможно назначить новую услугу. \n\rЭто нештатная ситуация. Сообщите о ней администратору Медсист.",
                "Прерывание операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
                //mbds_deleted.Clear();//Очистка коллекции после успешного удаления
            //foreach (OnDemandMedicine mi in mbds_deleted)
            //{
            //   mi.Delete(CommandDirective.MarkAsDeleted);
            //}
            mbds.SaveNotSaved();
            mbds.UpdateModified();
            
            //Обновление записей медикаментов после сохранения всех изменений
            gridMeds.DataSource = null;
            mbds = demand.LoadMed();
            gridMeds.DataSource = mbds;

            cmd.UpdateParent();
            DataSaved(true);
        }
        ///<summary>Закрытие формы
        ///Заплатин Е.Ф.
        ///09.08.2011</summary>
        private void frmDemandByMedicament_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!dataSaved)
            {//Какая-то часть требования в форме была изменена
                DialogResult dr = MessageBox.Show(this, "Сохранить требование на медикаменты?",
                    "Редактирование требования",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    Save();
                    cmd.UpdateParent();
                }
                else if (dr == DialogResult.No)
                {
                    if (cmd.Mode == CommandModes.New)
                    {//Требование вновь созданное
                        demand.Delete(); //Удаление записи самого требования
                        cmd.UpdateParent();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
            
        }
        /// <summary>Изменение количества выписываемых по требованию медикаментов
        /// Заплатин Е.Ф.
        /// 17.08.2011
        /// </summary>
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {//Проверка на выделение хотя бы одной строки в списке медикаментов
                if (e.ColumnIndex == gridMeds.Columns["colCount"].Index)
                {//Рерактировалась колонка с количеством медикаментов
                    //Проверить насколько уменьшилось количество. Уменьшение не должно превысить величину остатка
                    if (mbds[e.RowIndex].NotSaved)
                    {//Изменяется количество вновь добавленного медикамента, поэтому нужно всего лишь синхронизировать остаток
                        mbds[e.RowIndex].CountResidue = mbds[e.RowIndex].CountGet = (int)gridMeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    }
                    else
                    {//Изменяется количество у ранее сохраненного медикамента, поэтому нужно следить чтобы остаток не стал меньше нуля
                        OnDemandMedicine md = new OnDemandMedicine(true);
                        md.MedicamentByDemandID = mbds[e.RowIndex].MedicamentByDemandID;
                        md.Load();//Загружаются сохраненные сведения о количестве
                        int BeforeCount = md.CountGet;// Предудщее назначение количество
                        md.CountGet = (int)gridMeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;//Присваивается вновь введенное количество
                        int difference = md.CountGet - BeforeCount;
                        /* Здесь может возникнуть следующая проблема. 
                         * Если уменьшение количества превысисит величину остатка, то остаток может стать орицательным.
                         * Лучше всего анализировать эту ситуацию и не позволять настолько уменьшать общее количество.*/
                        if (difference < 0 && Math.Abs(difference) > md.CountResidue)
                        {//Разность между новым и старым значениями количества приняла отрицательное значение и 
                            //превысла величину остатка. Такого быть не должно.
                            MessageBox.Show(this, "Величина уменьшения количества превысила величину остатка медикамента по требованию.\r\n" +
                                                    "Такое уменьшение недопустимо.",
                                                    "Недопустимое значение количества.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            md.CountGet = BeforeCount - md.CountResidue; //Переназначается минимально возможное уменьшение количества.
                            difference = -md.CountResidue;// разность приравнивается величине остатка
                        }
                        md.CountResidue += difference; //Увеличение (уменьшение) остатка на величину измененного количества
                        //Обновление полей с количеством и остатком после окончания редактирования записи
                        gridMeds.Rows[e.RowIndex].Cells["colCount"].Value = md.CountGet.ToString();
                        gridMeds.Rows[e.RowIndex].Cells["colCountResidue"].Value = md.CountResidue.ToString();
                        mbds[e.RowIndex] = md;
                        mbds[e.RowIndex].Modifie();
                    }
                    DataSaved(false);
                }
            }
        }

        private void txtNbr_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void cmbSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void cmbMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        /// <summary>Удаление медикамента из требования
        /// Возможны два варианта развертывания события:
        /// 1. Удаляется медикамент, который был уже сохранен в базе
        /// 2. Удаляется только что добавленный, но не сохраненный в базе медикамент
        /// В первом случае нужно переместить удаляемый медикамент во временную колекцию удаленных медикаментов
        /// Во втором сучае нужно только удалить из коллекции
        /// Заплатин Е.Ф.
        /// 19.08.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDel_Medicine_Click(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows == null) return;
            string med = mbds[gridMeds.SelectedRows[0].Index].ChekResidue();
            if (med!="")
            {
                MessageBox.Show("Медикамент: " + med + " уже выдавался пациентам? поэтому его невозможно удалить.",
                               "Невозможность удаления", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DialogResult res = MessageBox.Show("Действительно хотите удалить медикамент из требования?",
                               "Удаление медикамента из требования", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                if (!mbds[gridMeds.SelectedRows[0].Index].NotSaved)
                {//Сохраненный в базе данных медикамент. Нужно переместить в удаленную колекцию
                    mbds_deleted.Add(mbds[gridMeds.SelectedRows[0].Index]);
                }
                else
                {//Не сохраненный медикамент в базе даннsх. Нужно лишь удалить из коллекции
                }
                //Удаление медикамента из коллекции
                mbds.Remove(mbds[gridMeds.SelectedRows[0].Index]);

                DataSaved(false);
                gridMeds.DataSource = null;
                gridMeds.DataSource = mbds;
                rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = !(mbds.Count > 0);//Если в требование уже добавлены медикаменты из определенного
                //источника финансирования, то изменять свойство источника финансирования самого требования недозволено
            }
        }

        private void rbtnIsBudget_CheckedChanged(object sender, EventArgs e)
        {
            demand.IsBudget = rbtnIsBudget.Checked;//Требуется изменение т.к. нужно гибко реагировать не дожидаясь сохранения изменений в форме
            rbtnNotBudget.Checked = !demand.IsBudget;//Установка контрола для внебюджетных средств
            DataSaved(false);
        }
        /// <summary>Открытие отчета по требованию для его распечатки
        /// Заплатин Е.Ф.
        /// 05.12.2011
        /// </summary>
        private void tbPrint_Click(object sender, EventArgs e)
        {
            rptDemand rpt = new rptDemand();
            rpt.Tag = new Command(CommandModes.Open, demand.DocumentID, this);
            rpt.ShowDialog(this);
        }

    }
}
