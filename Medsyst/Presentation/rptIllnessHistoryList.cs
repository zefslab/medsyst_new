﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;
using Medsyst.DataSet;
using Medsyst.DataSet.datIllnessHistoryListTableAdapters;

namespace Medsyst
{
    public partial class rptIllnessHistoryList : Form
    {
        Seasons ss = new Seasons(); //Коллекция для списка выбора начального сезонасезона
        //Инициализируется адаптер с последующим присвоением занчений параметрам
        FacultyList facults = new FacultyList();///Факультеты
        IHListTableAdapter IHListTA = new IHListTableAdapter();
        IHListMKBTableAdapter IHListMKBTA = new IHListMKBTableAdapter();
        StudentByFacultTableAdapter StudentByFalultTA = new StudentByFacultTableAdapter();

        datIllnessHistoryList DS = new datIllnessHistoryList();


        public rptIllnessHistoryList()
        {
            InitializeComponent();
        }

        private void rptIllnessHistoryList_Load(object sender, EventArgs e)
        {
            // инициализируется строки подключения
            IHListTA.Connection.ConnectionString = DB.CS;    
            IHListMKBTA.Connection.ConnectionString = DB.CS;
            StudentByFalultTA.Connection.ConnectionString = DB.CS;

            //Загрузка коллекций сезонов для списков выбора
            ss.Load(CommandDirective.Non); //Загрузка списка сезонов
            cmbSeason.DataSource = ss;
            Settings set = new Settings(true);
            set.Load();//Загрузка настроек приложения
            cmbSeason.SelectedValue = set.CurrentSeason; //Устанавливается селектор на текущий сезон
            
            //Загрузка факультетов
            facults.Load();
            cmbFacult.DataSource = facults;
        }

        private void btnExecutReport_Click(object sender, EventArgs e)
        {
            Season s = new Season();
            s = ss[cmbSeason.SelectedIndex]; //инициализируется сезон выбранный из выпадающего списка
            //Выбор одного из двух отчетов в зависимости от чекбокса факультетов
            if (!chkFacult.Checked)
            {//Отчет: Журнал регистрации
                IHListTA.Fill(DS.IHList, s.SeasonID, Convert.ToInt32(rbtnStudents.Checked));
                IHListMKBTA.Fill(DS.IHListMKB, s.SeasonID, Convert.ToInt32(rbtnStudents.Checked));

                illnessHistoryList1.SetDataSource(DS); //передача отчету заполненного экземпляра набора данных - DS

                //Передача в отчет вспомогательных параметров для оформления  отчета
                illnessHistoryList1.SetParameterValue("Season", s.SeasonNbr);
                illnessHistoryList1.SetParameterValue("DateBegin", s.SeasonOpen);
                illnessHistoryList1.SetParameterValue("DateEnd", s.SeasonClose);
                illnessHistoryList1.SetParameterValue("Students", rbtnStudents.Checked);

                crystalReportViewer1.ReportSource = illnessHistoryList1; //Элементу формы просмотра отчетов передается загруженный отчет
            }
            else
            {//Отчет: список студентов по факультетам
                StudentByFalultTA.Fill(DS.StudentByFacult, s.SeasonID, (int)cmbFacult.SelectedValue);
                studentByFacult1.SetDataSource(DS);
                studentByFacult1.SetParameterValue("Season", s.SeasonNbr);
                studentByFacult1.SetParameterValue("DateBegin", s.SeasonOpen);
                studentByFacult1.SetParameterValue("DateEnd", s.SeasonClose);
                studentByFacult1.SetParameterValue("Facult", facults[cmbFacult.SelectedIndex].ShortName);
                crystalReportViewer1.ReportSource = studentByFacult1;
            }
        }
        /// <summary>Включение фильтра для представления пофакультетного списка студентов
        /// Заплатин Е.Ф.
        /// 29.03.2012
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkFacult_CheckedChanged(object sender, EventArgs e)
        {
            cmbFacult.Enabled = chkFacult.Checked;
            rbtnStudents.Enabled = rbtnWorkers.Enabled = !chkFacult.Checked; 
        }
    }
}
