﻿//29.07.09
//Выбор подразделения и связь с ComboBox
//Выбор должности

//!Выбор должности в алфавитном порядке
//!Добавление должности в подразделение

//3.08.09
//Заполнение списка зданий из XML-файла
//Автоматическое заполнение полей 'Здание' и 'Этаж' при вводе номера аудитории
//Добавление улицы
//Формирование полного названия улицы с учетом типа (пр, ул, пер, б-р)

//5.08.09
//Сортировка списка улиц
//После добавления улицы она добавляется в список улиц
//Выбор рабочего и внутреннего телефонов из соотв. списков (не отображать другие типы в этих списках)
//Добавление рабочего телефона

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Class.Forms;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmEmployee : Form
    {
        ///<summary>Сотрудник профилактория</summary>
        Member member = new Member();
        ///<summary>Список должностей</summary>
        PositionList positions = new PositionList();
        DivisionList divisions = new DivisionList();
        Address address = new Address(true);
        Cities cities = new Cities();
        Months months = new Months(); //Инициализация коллекции месяцев в году. ЗЕФ. 23.03.2012.
        Settings set = new Settings(true);// Настройки системы

        public frmEmployee()
        {
            InitializeComponent();
        }

        ///<summary>Загрузка формы</summary>
        private void frmEmployee_Load(object sender, EventArgs e)
        {
            set.Load();
            
            member.FormBinding("employee", this);

            cmbPositions.DataSource = positions.Get();
            
            cbCity.DataSource = cities.Get();

            LoadDivisions(set.Sanatorium);//Загрузка сотрудников санатория профилактория выбранного в настройках системы

            numYear.Value = DateTime.Now.Year; //Установление счетчика годов на текущий год
            cmbMonths.DataSource = months;


            address.FormBinding("address", this);

            if (cmd.Mode == CommandModes.New)
            {
                member.LastName = "Фамилия";
                member.Name = "Имя";
                member.SurName = "Отчество";
                member.PositionID = positions[0].PositionID;
                member.Insert();
            }
            else
            {
                member.PersonID = cmd.Id;
                member.Load();
                address.AddressID = member.HomeAdressID;
                address.Load();
            }

            address.WriteToForm();
            member.WriteToForm();
        }

        /// <summary>Загрузка подразделений
        /// Заплатин Е.Ф.
        /// 02.04.2013
        /// </summary>
        /// <param name="firmid">идентификатор фирмы</param>
        private void LoadDivisions(int firmid)
        {
            cmbDivision.DataSource = null;
            divisions.Load(firmid, CommandDirective.AddNonDefined, CommandDirective.NotLoadDeleted);
            cmbDivision.DisplayMember = "Name";//По непонятной причине свойство cmbDivision.DisplayMember сбрасывается после операции cmbDivision.DataSource = null;
            cmbDivision.ValueMember = "DivisionID";
            cmbDivision.DataSource = divisions;
        }

        ///<summary>Кнопка "Готово"</summary>
        private void toolbtnSave_Click(object sender, EventArgs e)
        {
            address.ReadFromForm();
            if (cmd.Mode == CommandModes.New)
            {
                address.Insert();
            }
            else
            {
                address.Update("Address");
            }
            member.HomeAdressID = address.AddressID;
            member.HomeAdress = address;

            member.ReadFromForm();
            member.Update();
                       
            cmd.Mode = CommandModes.Edit;
            Close();
        }

        ///<summary>Закрытие формы</summary>
        private void frmEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmd.Mode == CommandModes.New)
            {
                DialogResult hr = MessageBox.Show(this, "Сохранить изменения?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (hr == DialogResult.Cancel)
                    e.Cancel = true;
                else if (hr == DialogResult.No)
                {
                    member.Delete();
                }
                else
                {
                    member.ReadFromForm();
                }
            }
        }


        private void cmbMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            WorkCalendar();

        }
        private void WorkCalendar ()
        {
            //Заполнение календаря рабочих выходов добавлением необходимого количества колонок с днями
            int Days = DateTime.DaysInMonth((int)numYear.Value, cmbMonths.SelectedIndex+1);
            dgDays.Columns.Clear();
            for (int d=1; d <= Days ; d++)
            {
                DataGridViewColumn col = new DataGridViewColumn(dgTemplate.Rows[0].Cells["colDay"]);
                col.HeaderText= d.ToString();
                col.Name = "day"+d.ToString();
                col.Width = 21;
                dgDays.Columns.Add(col);

            }
        }

        /// <summary>Выбор подразделения из формы списка подразделений
        /// Заплатин Е.Ф.
        /// 10.04.2013
        /// </summary>
        private void btnDivision_Click(object sender, EventArgs e)
        {
            //передать форме идентификатор организации и заблокировать возможность изменения названия организации
            //разрешив только выбирать подразделение
            frmDivisionList frm = new frmDivisionList();
            Command cmd_div = new Command(CommandModes.Select, set.Sanatorium, this);
            cmd_div.type = typeof(Firm);//Передается тип идентификатора указанного в свойстве id интерфейса
            frm.Tag = cmd_div;
            DialogResult res = frm.ShowDialog();
            if (res != DialogResult.OK) return; //Выбор не произведен
            LoadDivisions(set.Sanatorium);//Обновление списка в связи с возможным переименованием или добавлением подразделений
            //выделение выбранного подразделения в списке
            cmbDivision.SelectedValue = ((Command)frm.Tag).Id;


        }

       

       

    }



}
