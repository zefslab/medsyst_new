﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;

namespace Medsyst
{
    //Универсальная форма для просмотра списков требований двух типов: по материалам и по медикаментам
    // в зависимости от типа требований передавеаемого в интерфейсе отображаются соответсвующие требования
    public partial class frmDemandByMedicamentAndMaterialList : Form
    {
        DemandList ds = new DemandList();
        Demand d = new Demand(true);
        MemberList members = new MemberList();
        Seasons ss = new Seasons();
        int docType = 0; //Тип требований

        public frmDemandByMedicamentAndMaterialList()
        {
            InitializeComponent();
        }

        private void frmDemandByMedicamentAndMaterialList_Load(object sender, EventArgs e)
        {
            cmd = (Command)Tag;
            docType = cmd.Id;
            //TODO попробовать здесь выполнить преобразование типов
            if (docType == (int)DocTypes.DemandMedicament)
            {//Открывается список требований по назначенным медикаментам
                this.Text = "Требования на  медикаменты назначенные пациентам";
                DemandMedicament d = new DemandMedicament(true);
            }
            else 
            {//Открывается список требований по выписанным материалам для лечебных кабинетов
                this.Text = "Требования на материалы для лечебных кабинетов";
                colIsDefected.Visible = true;
                DemandMaterial d = new DemandMaterial(true);
            }

            gridDemands.AutoGenerateColumns = false;//Настройка грида
            //Загрузка списка сотрудников
            members.Load(CommandDirective.AddNonDefined);
            //Колонка ФИО сотрудника
            colName.DataSource = null;
            colName.ValueMember = "PersonID_member";
            colName.DisplayMember = "FIOBrif";
            colName.DataSource = members;
            
            //Фильтр по сотруднику
            cmbEmployees.DataSource = null;
            cmbEmployees.ValueMember = "PersonID_member";
            cmbEmployees.DisplayMember = "FIOBrif";
            cmbEmployees.DataSource = members;
            ss.Load(CommandDirective.AddNonDefined);
            
            //Колонка сезона
            colSeason.DataSource = null;
            colSeason.ValueMember = "SeasonID";
            colSeason.DisplayMember = "FullNbr";
            colSeason.DataSource = ss;
            //Фильтр по сезону
            cmbSeasons.DataSource = null;
            cmbSeasons.DataSource = ss;
           
            ApplyFilter();
            
        }
        /// <summary>Применение фильтра с учетом установки различных критериев
        /// Заплатин Е.Ф.
        /// 06.08.2011
        /// </summary>
        private void ApplyFilter()
        {
            ds.Clear();
            ds.Load(chkDateBegin.Checked, dateBegin.Value.Date,
                         chkDateEnd.Checked, dateEnd.Value.Date,
                         chkProviders.Checked, (int)cmbEmployees.SelectedValue,
                         cmd.Id);//Идентификатор типа требования
            gridDemands.DataSource = null;
            gridDemands.DataSource = ds;
        }

        private void btnApplyFilter_Click(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void btnFilterCancel_Click(object sender, EventArgs e)
        {
            chkProviders.Checked = chkDateEnd.Checked = chkDateBegin.Checked = chkShowDeleted.Checked = chkSeason.Checked = false;
            ApplyFilter();
        }

        private void chkProviders_CheckedChanged(object sender, EventArgs e)
        {
            cmbEmployees.Enabled = chkProviders.Checked;
        }

        private void chkDateBegin_CheckedChanged(object sender, EventArgs e)
        {
            dateBegin.Enabled = chkDateBegin.Checked;
        }

        private void chkDateEnd_CheckedChanged(object sender, EventArgs e)
        {
            dateEnd.Enabled = chkDateEnd.Checked;
        }
        private void chkSeason_CheckedChanged(object sender, EventArgs e)
        {
            cmbSeasons.Enabled = chkSeason.Checked;
        }
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            //Отображается столбец с отметкой удаленных записей
            colDeleted.Visible = chkShowDeleted.Checked;//Отображение колонки с отметками об удалении
            ds.showdeleted = chkShowDeleted.Checked;//Установка или снятие флага показа удаленных требований
            tsbRecover.Visible = chkShowDeleted.Checked;//Управление видимостью кнопки Восстановить
            ApplyFilter();
        }
        ///<summary>Удаление требования
        ///Заплатин Е.Ф.
        ///06.08.2011</summary>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (gridDemands.SelectedRows != null && gridDemands.SelectedRows.Count > 0)
            {
                d.DocumentID = (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value;
                d.Load();
                if (d.Signed)
                {//Документ завизирован поэтому его удаление невозможно
                    MessageBox.Show("Невозможно удалить завизированное требование №" + d.Nbr,
                                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                //Проверяется ситуация когда по этому требованию уже был выписан медикамент для оказания услуги
                //Если такое произошло, то остаток должен быть меньше количества выписанного по требованию
                //В этом случае нельзя удалять требование

                string meds = d.ChekDesidue();//Списанные медикаменты по требованию
                if (meds != "")
                {//Невозможно удалить требование со списанными медикаментами
                    MessageBox.Show(this, "Невозможно удалить требование №" + d.Nbr +
                    " т.к. по нему уже списывались медикаменты: " + meds, "Невозможность удаления", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return; //Прерывается процесс удаления
                }
                else
                {
                    //Медикаменты по требованию не списывались поэтому можно его удалить
                    if (d.DocTypeID == (int)DocTypes.DemandMedicament)
                        d.DeleteDemandMedicament();
                    else
                        d.DeleteDemandMaterial();
                    ApplyFilter();
                }
            }
        }
        private void tsbGo_Click(object sender, EventArgs e)
        {
            EditDemand();
        }
        private void gridDemands_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditDemand();
        }
        /// <summary>Редактирование выделенного требования
        /// Заплатин Е.Ф.
        /// 06.08.2011
        /// </summary>
        private void EditDemand()
        {
            if (gridDemands.SelectedRows != null && gridDemands.SelectedRows.Count > 0)
            {
                int curIndex = gridDemands.SelectedRows[0].Index;//Сохранение индекса строки, которая редактируется
                Command c = new Command(CommandModes.Edit, (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value) { OnUpdate = ApplyFilter };
                var frm = new Form();
                if (docType == (int)DocTypes.DemandMedicament)
                {
                    frm = new frmDemandByMedicament();
                }
                else
                {
                     frm = new frmDemandByMaterial();
                }
                frm.Tag = c;
                frm.MdiParent = MdiParent;
                frm.Show();
                gridDemands.Rows[curIndex].Selected = true;//Выделение редактируемой записи
            }
        }

        /// <summary>Восстановление удаленного требования
        /// Заплатин Е.Ф.
        /// 06.08.2011
        /// </summary>
        private void tsbRecover_Click(object sender, EventArgs e)
        {
            d.DocumentID = (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value;
            d.Load();
            d.Recover();
            ApplyFilter();
        }
        /// <summary>Добавление нового требования
        /// Заплатин Е.Ф.
        /// 06.08.2011
        /// </summary>
        private void tsbAdd_Click(object sender, EventArgs e)
        {
            Command c = new Command(CommandModes.New) { OnUpdate = ApplyFilter };
            var frm = new Form();
            if (docType == (int)DocTypes.DemandMedicament)
            {
                frm = new frmDemandByMedicament();
            }
            else
            {
                frm = new frmDemandByMaterial();
            }
            frm.Tag = c;
            frm.MdiParent = MdiParent;
            frm.Show();
        }

 


    }
}
