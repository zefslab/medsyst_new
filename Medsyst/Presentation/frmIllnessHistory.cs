﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmIllnessHistory : Form
    {

        public int OldIHNbr = 0;
        public int OldSeasonID = 0;

        public IllnessHistory ih = new IllnessHistory(true);//История болезни
        Patient patient = new Patient(true);//Пациент
        Finance f; //Источник финансовых средств к которому относится история болезни
        AmbulanceCard amb = new AmbulanceCard();//Амбулаторная карта
        OutputMedicamentList mos = new OutputMedicamentList();//Назначенные медикаменты
        ServiceOutputList so = new ServiceOutputList();//Назначенные услуги
        Seasons sss = new Seasons();//Список сезонов
        //Observation observat;//Наблюдение
        int CurObs = -1;//Индекс текущего наблюдения терапевта
        int CurObsDerma = -1;//Индекс текущего наблюдения дерматоломениролога
        Observations obs = new Observations();
        ObservationsDerma obsDerma = new ObservationsDerma();
        DirectionTypeList dl = new DirectionTypeList();

        /// <summary>Список диагнозов с места отбора</summary>
        IH_MKBCodeList MKBOut = new IH_MKBCodeList(MKBType.DiagOut);
        /// <summary>Список диагнозов профилактория при поступлении</summary>
        IH_MKBCodeList MKBAttend = new IH_MKBCodeList(MKBType.DiagAttend);
        /// <summary>Список диагнозов профилакторияпри выписке</summary>
        IH_MKBCodeList MKBMain = new IH_MKBCodeList(MKBType.DiagMain);
        
        /// <summary>Список докторов</summary>
        Doctors doctors = new Doctors(); 


        /// <summary>Вспомогательное свойство для отслеживания изменения значения поля mkblistout</summary>
        int MKB = 0;
        /// <summary>Вспомогательные переменные для отслеживания статуса редактирования количества назначаемых услуг или медикаментов
        /// Подругому не удается отследить именение количества при нажатии сразу на кнопку Готово</summary>
        bool medEdit, serviceEdit = false; 


        public frmIllnessHistory()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Загрузка формы Истории болезни
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IllnessHistory_Load(object sender, EventArgs e)
        {

            //Важный элемент без которого элементы дерева не будут подсвечиваться при выделении закладок
            treeView1.HideSelection = false;

            dl.Load();
            cmbTypeIH.DataSource = dl;
            
            lbObserv.Items.Clear();//Если не очищатьть то выхватываются из памяти какие-то записи при создании на основе шаблона
            gridService.AutoGenerateColumns = dgMKBOut.AutoGenerateColumns = dgMKBAttend.AutoGenerateColumns = dgMKBMain.AutoGenerateColumns = false;
            
            Settings set = new Settings(true);
            set.Load();
            cmd = (Command)Tag;
            if (cmd.Mode == CommandModes.New)
            {//Создание новой истории болезни
                patient.PatientID = cmd.Id;//При создании новой истории болезни передается идентификатор пациента
                patient.Load();
                amb.PatientID = patient.PatientID;
                amb.Load();
                ih.AmbCardID = amb.DocumentID;
                ih.RegisterD = DateTime.Now;
                //Установка года, загрузка списка сезонов. Чирков Е.О.30.06.10
                nmYear.Value = set.CurrentYear;//Устанавливается текущий год из параметров системы. 
                //Загрузка списка сезонов выполняется в событии nmYear_ValueChanged
                //Установка текущего сезона и изменение выделеного элемента в выпадающем списке
                //последнее делать нужно обязательно, т.к. на основе выделеного элемента вычисляется номер  и.б.
                cmbSeason.SelectedValue = ih.SeasonID = set.CurrentSeason;
                cmbTypeIH.SelectedValue = ih.GetTypeIH(patient.CategoryID);
                ih.NbrIH = NewNumber();//Генерируется новый, очередной номер истории болезни
                ih.Insert();//Создается новая история болезни и добавляется запись в базу данных
            }
            else if (cmd.Mode == CommandModes.Edit)
            {//Изменение существующей истории болезни
                ih.DocumentID = cmd.Id;//При открытии и.б. передается идентификатор истории болезни из родительской формы
                ih.Load();//Загружается история болезин со всеми свойствами
                //Требуется загрузка информации о пациенте через амбулаторную карту по ее идентификатору
                var ac = new AmbulanceCardFromDocumentID(true) {DocumentID = ih.AmbCardID };
                ac.Load();
                patient.PatientID = ac.PatientID;
                patient.Load();
                //Чирков Е.О.
                //30.06.10
                //Сохранение номера сезона и номера ИБ, открытой для редактирования
                //при изменении сезона на сохраненный ей будет присвоен соответствующий номер
                OldIHNbr = ih.NbrIH;
                OldSeasonID = ih.SeasonID;

                //Установка года, загрузка списка сезонов. Чирков Е.О. 30.06.10
                //загрузка списка сезонов выполняется в событии nmYear_ValueChanged 
                var season = new Season { SeasonID = ih.SeasonID };
                season.Load();
                nmYear.Value = season.Year; //ЗЕФ 02.10.2015;
                
                //Установка сезона
                cmbSeason.SelectedValue = ih.SeasonID;

                LoadMed(); //Загрузка назначеных медикаментов
                lblMedPrice.Text = ih.SummMedicament.ToString("C", CultureInfo.CreateSpecificCulture("ru-RU"));//сумма медикаментов
                LoadService();//Загрузка назначеных услуг
                //Если назначена хотя бы отдна услуга или один медикамент, то блокируется возможность смены типа истории болезни (путевка/курсовка/платная основа)
                //т.к. это будет равносильно смене источника финансирования
                if (mos.Count > 0 || so.Count > 0)
                    cmbTypeIH.Enabled = false;
                lblServicePrice.Text = ih.SummService.ToString("C", CultureInfo.CreateSpecificCulture("ru-RU"));//Сумма услуг
                CalcSum();//Суммарная стоимости медикаментов и услуг
                LoadObserver(); //Загрузка наблюдений терапевта
                LoadObserverDerma(); //Загрузка наблюдений дерматоловинеролога
            }
            else Close(); //Не определено действие в интерфейсе. Добавил Заплатин Е.Ф. 04.08.2011

            ih.FormBinding("history", this);//Привязка свойств бизнес класса history  к форме
            patient.FormBinding("patient", this);//Привязка свойств бизнес класса patient  к форме
            dataSaved = false;//предполагается, что всегда открывается форма для внесения изменений. 
                              //В виду большого объема изменяемых данных не отслеживается изменение каждого поля.
            //Загрузка диагнозов с места отбора, при поступлении, при выписке. Добавил ЗЕФ, 24.01.2012
            dgMKBOut.DataSource = null;
            dgMKBAttend.DataSource = null;
            dgMKBMain.DataSource = null;
            MKBOut.Load(ih.DocumentID, false);
            MKBAttend.Load(ih.DocumentID, false);
            MKBMain.Load(ih.DocumentID, false);
            dgMKBOut.DataSource= MKBOut;
            dgMKBAttend.DataSource = MKBAttend;
            dgMKBMain.DataSource = MKBMain;

            //ЗЕФ. 07.10.2010
            doctors.Load();//Загрузка списка врачей
            cmbDoctor.DataSource = doctors;//Комбобокусу передается источник данных
            patient.WriteToForm();//Запись значений свойств patient связанных с фрмой в элементы управления 
            ih.WriteToForm();//Запись значений свойств history связанных с фрмой в элементы управления 
            FillSeasonDate(sss[cmbSeason.SelectedIndex]);
            if (ih.TypeIH == 1)
                //Тип - путевка, поэтому должны использоваться медикаменты приобретенные на бюджетные средства 
                f = Finance.Budget;
            else
                //Тип - курсовка или платная основа, поэтому должны использоваться медикаменты приобретенные на вне бюджетные средства
                f = Finance.ExtraBudget;

            amb.DocumentID = ih.AmbCardID;
            amb.Load();
           

            // Перенесены обработчики прерываний для устраниения повторных вызовов при открытии формы 
            // (издержки технологии заполнения формы Марьенкова Е.)
            this.cmbSeason.SelectedValueChanged += new EventHandler(this.comboSeason_SelectedValueChanged);
            this.cmbTypeIH.SelectedIndexChanged += new EventHandler(this.cmbTypeIH_SelectedIndexChanged);
        }
       
        
        ///<summary>Рассчет суммарной стоимости медикаментов и услуг</summary>
        public void CalcSum()
        {
            var cult = CultureInfo.CreateSpecificCulture("ru-RU");
            lblPrice.Text = (ih.SummMedicament + ih.SummService).ToString("C", cult); // Сумма стомости медикаментов и услуг
        }

        private void treeView1_AfterSelect_1(object sender, TreeViewEventArgs e)
        {
            this.tabControl1.SelectTab(treeView1.SelectedNode.Index);
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            this.treeView1.SelectedNode = treeView1.Nodes.Find(e.TabPage.Name.Substring(7), false)[0];

        }

        private void comboBox14_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHeartNoiseIH.Text == "определяются")
            {
                cmbHeartNoise1IH.Visible = true;
                cmbHeartNoise2IH.Visible = true;
            }
            else
            {
                cmbHeartNoise1IH.Visible = false;
                cmbHeartNoise2IH.Visible = false;
            }

        }

        private void comboBox24_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLiver1IH.Text == "выступает из-под края реберной дуги")
            {
                label43.Visible = true;
                label56.Visible = true;
                textBox51.Visible = true;
            }
            else
            {
                label43.Visible = false;
                label56.Visible = false;
                textBox51.Visible = false;
            }

        }

        private void comboBox54_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNoiseHeart1OSV.Text == "определяются")
            {
                cmbNoiseHeart3OSV.Visible = true;
                cmbNoiseHeart2OSV.Visible = true;
            }
            else
            {
                cmbNoiseHeart3OSV.Visible = false;
                cmbNoiseHeart2OSV.Visible = false;
            }

        }

        //Чирков Е.О.
        //30.06.10
        //Чтение данных ИБ из формы
        void ReadHistoryFromForm()
        {
            ih.ReadFromForm();
            //Данные о сезонах, номерах и пр. читаются отдельно
            if (cmbSeason.SelectedValue != null) ih.SeasonID = (int)cmbSeason.SelectedValue;
            ih.TypeNbr = textTypeNbr.Text;
            if (cmbTypeIH.SelectedValue != null) ih.TypeIH = (int)cmbTypeIH.SelectedValue;
            int i = ih.NbrIH;
            Int32.TryParse(txtIHNbr.Text, out i);
            ih.NbrIH = i;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReadHistoryFromForm();
            //history.ReadFromForm();
            ih.Update();
        }
        ///<summary>Сохранение истории болезни
        ///Запалтин Е.Ф.
        ///22.01.2012</summary>
        private void Save()
        {
            //Проверка не находится ли ячейка грида в стадии редактирования
            if (medEdit || serviceEdit)
            {
                MessageBox.Show("Редактирование одной из записей назначения не завершено. Сначала завершите редактирование, а затем  назмите кнопу Готово",
                                "Незаконченная операция редактирования",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (cmd.Mode == CommandModes.New)
                cmd.Mode = CommandModes.Edit; //После принудительного сохранения данных меняется режим работы формы
            ReadHistoryFromForm();
            Season s = new Season(true);
            s.SeasonID = ih.SeasonID;
            s.Load();
            ih.BeginD = s.SeasonOpen;
            ih.EndD = s.SeasonClose;
            ih.Update();
            
            // сохранение наблюдений
            SaveObs(); 
            SaveObsDerma();
            
            //Обновление вызывающей формы
            cmd.UpdateParent();
        }
        ///<summary>Кнопка "Сохранения данных и закрытия формы"</summary>
        private void btnOk_Click(object sender, EventArgs e)
        {
            Close(); //Вызывается обработчик закрытия формы в котором производится запрос на сохранение данных
        }
        ///<summary> Автоматическое присвоение номера истории болезни, если изменился сезон
        ///Заплатин Е.Ф.
        ///</summary>
        private void comboSeason_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbSeason.SelectedIndex < 0) return;
            if (cmbSeason.SelectedIndex < sss.Count)
            {
                FillSeasonDate(sss[cmbSeason.SelectedIndex]);
            }
            //Изменение номера сезона
            if ((int)cmbSeason.SelectedValue != OldSeasonID)
             //Выбран новый сезон отличный от старого и нужно заново определить номер истории болезни
            {
                txtIHNbr.Text = NewNumber().ToString();
                OldSeasonID = (int)cmbSeason.SelectedValue;
            }
        }
        ///<summary> Заполнение полей формы начала и окончания сезона
        ///Заплатин Е.Ф.
        ///07.05.2013
        ///</summary>
        private void FillSeasonDate(Season s)
        {
            if (s == null) return;
            {
                //Изменение даты начала сезона
                txtSeasonOpen.Text = s.SeasonOpen.ToShortDateString();
                //Изменение даты закрытия сезона
                txtSeasonClose.Text = s.SeasonClose.ToShortDateString();
            }
        }
        ///<summary> ЗЕФ. Автоматическое присвоение номера истории болезни, если изменился тип путевки или курсовки
        private void cmbTypeIH_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtIHNbr.Text = NewNumber().ToString();
            //Смена источника финансирования
            if (cmbTypeIH.Text == "Путевка")
                //Тип - путевка, поэтому должны использоваться медикаменты приобретенные на бюджетные средства 
                f = Finance.Budget;
            else
                //Тип - курсовка или платная основа, поэтому должны использоваться медикаменты приобретенные на вне бюджетные средства
                f = Finance.ExtraBudget;
        }
        ///<summary> ЗЕФ. Изменение списка сезонов и автоматическое присвоение номера истории болезни, если изменился год
        private void nmYear_ValueChanged(object sender, EventArgs e)
        {
            Settings set = new Settings(true);
            set.Load();

            LoadSeasonsByYear((int)nmYear.Value);
            
         }

        private void LoadSeasonsByYear(int year)
        {
            cmbSeason.DataSource = null;
            sss = new Seasons();
            sss.Load(year);//Фильтруются сезоны по текущему году
            cmbSeason.DataSource = sss;
        }


        ///<summary>Назначение нового номера истории болезни </summary>
        private int NewNumber()
        {
            if (cmbSeason.SelectedValue == null) return 0;
            if (cmbTypeIH.SelectedValue == null) return 0;
            
            return ih.GetNewNbrIH((int)cmbSeason.SelectedValue, (int)cmbTypeIH.SelectedValue);
        }
        
#region  Работа с медикаментами

        ///<summary>Загрузка списка назначенных медикаментов</summary>
        public void LoadMed()
        {
            gridMeds.AutoGenerateColumns = false;
            gridMeds.DataSource = null;
            mos = new OutputMedicamentList();
            mos.Load(ih.DocumentID);
            gridMeds.DataSource = mos;
        }
        /// <summary>
        /// Пересчет суммы назначеных медиакментов
        /// </summary>
        private void CalcMed()
        {
            var cult = CultureInfo.CreateSpecificCulture("ru-RU");
            decimal s_med = ih.SumMedic();//Вычисление стоимости медикаментов
            lblMedPrice.Text = s_med.ToString("C", cult);//Отображение суммарных стоимостей медикаментов и услуг  
            ih.SummMedicament = s_med;//Передача суммы свойству класса
            ih.Update();//Запись суммы в базу данных
        }

        Command c; //Интерфейс для обмена с формой выбора назначений

        /// <summary>Добавление  медикамента в назначения
        /// Заплатин Е.Ф.
        /// 05.12.2012
        /// </summary>
        private void btnAddMedicine_Click(object sender, EventArgs e)
        {
            //Подготовка к открытию формы Склада медикаментов с учетом формы финансирования
            frmSclad frm = new frmSclad(f);
            //Указывается роль формы - для выбора медикаментов и обработчик события обновления
            c = new Command(CommandModes.MultiSelect, this){OnUpdate = addMedicine};
            c.obj = mos; //Передается в форму выбора коллекция выбранных медикаментов для исключения их из списка выбора,
            //чтобы не производилось повторного назначения
            frm.Tag = c;
            //Открытие формы Склада медикаментов
            frm.ShowDialog(this);
        }
        /// <summary>Обработчик обновления формы вызываемый через интерфейс
        /// Заплатин Е.Ф.
        /// 05.12.2012
        /// </summary>
        private void addMedicine()
        {
            c = (Command)Tag;//Принимает параметры переданные от дочерней формы выбора назначений
            if (c.Id > 0)
            {  //Если идентификатор больше нуля, то продолжается обработка. 
                //Создается экземпляр класса расходного медикамента
                MedicamentOutput m = new MedicamentOutput(true);
                m.New(c.Id, ih.DocumentID);
                m.Insert();
                //Заново инициализируется список назначенных медикаментов
                LoadMed();//Перезагрузка списка медикаментов
                CalcMed();//Пересчет суммы назначеных медикаментов
                CalcSum();//Пересчет общей суммы назначений
                cmbTypeIH.Enabled = false;//Запрет изменения типа истории болезни после добавления хотя бы одной услуги или медикамента
            }
        }
        /// <summary>
        /// Удаление назначенных медиакментов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelMedecine_Click(object sender, EventArgs e)
        {
            //Проверка на выделение элемента списка
            if (gridMeds.SelectedRows != null && gridMeds.SelectedRows.Count > 0)
            {
                /*Получение указателя на удаляемый медикамент. Это нужно для того, чтобы потом можно было восстановить 
                 выделение на том же элементе, если удаление не произошло или выделить следующий элемент если медикамент
                 был удален из списка*/
                int row = gridMeds.SelectedRows[0].Index;
                int id = mos[gridMeds.SelectedRows[0].Index].MedicamenOutputID;//Получение идентификатора удаляемого медикамента
                MedicamentOutput m = new MedicamentOutput(true);
                m.MedicamenOutputID = id;
                m.Load();//Загрузка информации о назначенном медикаменте
                if (DialogResult.Yes == MessageBox.Show("Вы уверены, что хотите удалить медикамент из назначения?", "Удаление назначенного медикамента",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    if (m.Delete(CommandDirective.MarkAsDeleted)) // Удаление выбранного медикамента из списка назначений
                    {//Медикамент успешно удален
                        LoadMed();//Перезагрузка списка медикаментов
                        CalcMed();//Пересчет суммы назначеных медикаментов
                        CalcSum();//Пересчет общей суммы назначений
                        if (!(mos.Count > 0 || so.Count > 0))
                            cmbTypeIH.Enabled = true;//Разрешается изменения типа истории болезни не осталось ни одной услуги или медикамента
                        /*Проверить не являляся ли удаленный медикамент последним в списке. 
                         * Если это так, то выдление нужно сместить на один элемент выше.
                         * А если он является единственным или первым элеменотом в  списке то не нужно утанавливать 
                         * выделение*/
                        if (gridMeds.RowCount != 0)// Если элементов в списке не осталось, то удаленный медикамент
                        //являлся единственным,  поэтому не нужно устанавливать выделение
                        {//Элементы остались значит был не единственным
                            if (row != 1) //удаленный медикамент не являлся первым элеменотом поэтому нужно устанавливать выделение
                            {
                                if (gridMeds.RowCount == row) //удаленный медикамент являлся последним элеменотом 
                                    gridMeds.Rows[row - 1].Selected = true;//Востанавливается выделение предыдущего удаленному медикаменту
                                else //удаленный медикамент являлся ни последним, ни первым, ни эдинственным элеменотом в списке
                                    gridMeds.Rows[row].Selected = true;//Востанавливается выделение последующего за удаленным медикаментом
                            }//Если удаленный элемент был первым, то список сам установит селектор на первый элемент
                            //Поэтому никаких ждействий предпринимать не нужно
                        }//Если элементов в списке не осталось после удаления, то селектор не нужно никуда устанавливать
                    }

                    else
                    { //Медикамент небыл удален
                        gridMeds.Rows[row].Selected = true;//Востанавливается выделение удаляемого медикамента 
                    }
                }
            }
        }

        /// <summary>Завершение редактирования записи назначенного медикамента. Обрабатывается редактирования  полей: количества и рецепта
        /// Запалтин Е.Ф.
        /// 26.09.2011 - исключил использование параметров из кода метода
        /// </summary>
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (gridMeds.CurrentCell.RowIndex >= 0)//Если индекс редактируемой сторки больше или равен нулю, значит редактируется существующая запись назначенного медикамента 
            {
                int rowIndex = gridMeds.CurrentCell.RowIndex;//Вспомогательная локальная переменная указывающая на индекс редактируемой строки 
                int colIndex = gridMeds.CurrentCell.ColumnIndex; //Вспомогательная локальная переменная указывающая на индекс редактируемой колонки
                //Редактируется колонка с количеством
                if (colIndex == gridMeds.Columns["colCountMed"].Index)
                {
                    int id = mos[rowIndex].MedicamenOutputID;//Определяется идентификатор редактируемого медикамента 
                    MedicamentOutput mo = new MedicamentOutput(true);//создается экземпляр расходуемого медикамента
                    mo.MedicamenOutputID = id;//Указывается идентификатор редактируемого медикамента
                    mo.Load();//Загружается запись расходуемого медикамента

                 //TODO Весь ниже идущий код нужно перенести в метод класса MedicamentOutput.Output(count). Метод уже реализован и нужно лишь корректно его встроить в код. ЗЕФ. 
                    int BeforeCount = mo.CountOutput;// Сохраняется предудщее количество назначения
                    if (mo.Reserved) 
                    {//Медикамент зарезервирован, а значит еще не выдан. Изменение количества возможно
                        int c = (int)gridMeds.Rows[rowIndex].Cells[colIndex].Value;// Rows[e.RowIndex].Cells[e.ColumnIndex].Value;//Вновь назначенное количество
                        if ((c - BeforeCount) > mo.CountDivide) //TODO Не верное сравнение
                            /* Сравнивать нужно не с medOnSclad.CountDivide, а со всеми приходами этого медикаментами
                             * 
                             */
                        {// Недостаточно запрошенного количества медикаментов на складе, чтобы зарезервировать, 
                         //поэтому оно должно быть уменьшено
                            if (mo.CountDivide <= 0)
                            {//Запрашиваемого медикамента не осталось на складе
                                MessageBox.Show(this, "Медикамента " + mo.MedicamentN +
                                    " на складе не осталось",
                                    "Склад", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                c = mo.CountOutput; //Количество назначаемых в расход медикаментов остается прежним
                            }
                            else
                            {//На складе есть, недостаточное количество медикаментов
                                MessageBox.Show(this, "Количество медикамента " + mo.MedicamentN +
                                    " на складе " + mo.CountDivide + "\r\n Этого не хватает для назначения в необходимом количестве.",
                                    "Склад", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                c = mo.CountOutput + mo.CountDivide;//Количество добавляемых в расход медикаментов увеличивается на величину остатка на складе
                            }
                            //Если медикаменты на складе еще имеются, то они выбираются и добавляются к расходу
                            //А если медикаментов нет на складе совсем, то расход не меняется.
                            //count = medOnSclad.CountDivide >= 0 ? medOnSclad.CountOutput + medOnSclad.CountDivide : medOnSclad.CountOutput;
                        }
                        mo.CountOutput = c;// Текущее количество назначений

                        if (mo.Update(false))
                        {//Изменения в расход медикамента внесены успешно
                            gridMeds.Rows[rowIndex].Cells[colIndex].Value = mo.CountOutput;
                            // Обновляем состояние склада
                            MedicamentOnSclad mons = new MedicamentOnSclad(true);
                            mons.MedicamentOnScladID = mo.MedicamentOnScladID;
                            mons.Load();
                            mons.Resrved(mo.CountOutput - BeforeCount);
                            mons.Update();

                            //mos[e.RowIndex].CountOnSclad = medOnSclad.CountOnSclad;
                            mos[rowIndex].CountOutput = mo.CountOutput;
                            mos[rowIndex].CountReserve = mons.CountReserve;
                            //dataGridView2.Rows[e.RowIndex].Cells["CountOnSclad"].Value = medOnSclad.CountOnSclad;
                            gridMeds.Rows[rowIndex].Cells["CountDivide"].Value = mons.CountDivide;
                            gridMeds.Refresh();
                            CalcMed();
                            CalcSum();
                        }
                    }
                    else
                    {//Медикамент уже выдан и его количество невозможно изменить
                        gridMeds.Rows[rowIndex].Cells[colIndex].Value = BeforeCount; //Восстанавливается прежнее количество назначения
                        gridMeds.Refresh();//Обновляется содержиое грида
                        gridMeds.Rows[rowIndex].Selected = true;//Востанавливается выделение редактируемого медикамента после обновление грида
                        MessageBox.Show("У выданного медикамента невозможно изменить количество", "Отказ в изменении количества",
                                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                }
                else if (colIndex == gridMeds.Columns["colRecept"].Index)
                {//Редактируется колонка с рецептом
                     //Копирование идентификатора отпускаемого товара в переменную
                    int id = mos[rowIndex].MedicamenOutputID;
                    //Инициализация экземпляра класса отпускаемого медикамента
                    MedicamentOutput m = new MedicamentOutput(true);
                    m.MedicamenOutputID = id;
                    m.Load();
                    //Сохранение рецепта в свойстве класса 
                    if (gridMeds.Rows[rowIndex].Cells[colIndex].Value != null)
                        //при условии что ячейка с рецептом не пуста. Иначе возникает ошибка
                        m.Prescription = gridMeds.Rows[rowIndex].Cells[colIndex].Value.ToString();
                    else
                        m.Prescription = "";//Очевидно что строка рецепта пуста

                    m.Update(false);//Обновление записи с экземпляром класса
                    
                }
                medEdit = false; //Редактирование записи завершено
            }
        }

#endregion

#region  Работа с услугами

        ///<summary>Кнопка "Добавить услугу" вызывает прейскурант услуг и добавляет выбранную услугу в назначения 
        ///Заплатин Е.Ф.
        ///06.01.2012
        ///</summary>
        private void btnAddService_Click(object sender, EventArgs e)
        {
            frmServiceList frm = new frmServiceList(f, CommandModes.SelectWithUnavaleble);//Форма прейскуранта услуг
            Command c = new Command(CommandModes.MultiSelect, this) { OnUpdate = addService };
            
            c.obj = so; //Передается в форму выбора коллекция выбранных услуг для исключения их из списка выбора,
                        //чтобы не производилось повторного назначения
            frm.Tag = c;
            frm.ShowDialog(this);//Открывается прейскурант услуг
        }
        ///<summary>Добавляется выбраная услугуа в назначения. Метод передается в дочернюю форму
        ///посредством интерфейса для вызова из нее после выбора услуги из прейскуранта.
        ///Заплатин Е.Ф.
        ///06.01.2012
        ///</summary>
        private void addService()
        {
            DialogResult respons; //Ответ на запрос о вводе количества назначаемых услуг 
            c = (Command)Tag;
            if (c.Mode != CommandModes.Return) return;//Выбор услугие состоялся 
            //услуга была выбрана 
            int count = 0; //Количество назначемых услуг
            
            InputBox frmInput = new InputBox();//Форма ввода количества назначаемых услуг
            
            //Цикл №2 ввода количества услуги выбранной из прейскуранта
            do
            {//Повторять цикл ввода количества услуг пока не получим отказ от ввода количества услуг 
             //или медикаментов будет достаточно на складе для назначения услуги
                
                //Запрашивается количество назначаемых услуг
                frmInput.Caption = "Ввод значения";
                frmInput.Text = "Укажите количество назначаемых услуг";
                frmInput.Filter = FilterType.NotSignedInteger; //Настраивается форма на ввод исключительно положительных целых чисел
                frmInput.Value = count.ToString();//Начальное значение количества
                
                if (frmInput.Show(this) == DialogResult.Cancel)
                {//При отмене ввода количества услуг осуществляется возврат в прейскурант
                    //Выход из цикла №2 ввода количества услуг и возврата в прейскурант
                    break; 
                }
                //Введено необходимое количество и нажата кнопка Ок
                //Необходимо организовать проверку полученного значения
                if (frmInput.Value != "")
                {//Значение введено
                    count = int.Parse(frmInput.Value);
                    
                    string absentmeds = ""; //аргумент передаваемый по ссылке для анализа наличия достаточного количества медикаментов
                    //Вызывается статический метод добавления услуги в назначения

                    if (ServiceOutputList.Apply(c.Id, count, ih.DocumentID, (Finance)c.obj, ref absentmeds))
                    {//добавление услуги прошло успешно
                        LoadService();//Перезагрузка списка услуг
                        CalcService();//Перерасчет суммарной стоимости услуг
                        CalcSum();//Пересчет общей суммы назначений
                        //Запрет изменения типа истории болезни после добавления хотя бы одной услуги или медикамента
                        cmbTypeIH.Enabled = false;
                        return;//завершается выполнение метода
                    }
                    //услуга не добавлена. Если в процессе проверки возвращается False, значит нет 
                    //достаточного количества каких-то медикаментов на складе
                    //Выдается предупреждение о недостающих медикаментах и предлагается на усмотрение доктора указать другое количество 
                    //или отказаться от выбора услугиуслугу пациенту.
                    string message = "Некоторые медикаменты отсутсвуют на складе: " + absentmeds + ".\r\n" +
                        "Вы не можете назначить такое количество услуг.\r\n" +
                        "Если желаете указать другое количество, нажмите - 'Да',\r\n" +
                        "Если отказываетесь от назначения выбранной услуги, нажмите - 'Нет' и выберите другую услугу";//Выводится список отсутсвующих медикаментов
                    string caption = "Отсутствие медикаментов на складе";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    respons = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Warning);
                    
                    if (respons != DialogResult.Yes)
                    {//Не получено согласие указавать др. количество и заново выбирается услуга в прейскуранте
                        break; //Выход из цикла №2 ввода количества услуг и возврата в прейскурант
                    }
                    //Согласились указать другое количество, поэтому нужно подсчитать какое количество минимально возможно
                    count = 3;//TODO Здесь следует произвести рассчет или его нужно произвести при проверке наличия на складе. Пока стоит заглушка.
                    //MessageBox.Show("Высчитано максимально возможное количество услуг исходя из наличия медикаментов на складе: " +
                    //                   count.ToString(), "Заглушка");
 
                }
                
            } while (true);
        }

        ///<summary>Кнопка "Удалить услугу"</summary>
        private void btnDelService_Click(object sender, EventArgs e)
        {
            if (gridService.SelectedRows != null && gridService.SelectedRows.Count > 0)
            {
                var s = new ServiceOutput();
                s = so[gridService.SelectedRows[0].Index];//Экземпляр удаляемой услуги
                //Снятие с резерва медикаментов может производиться  если услуга еще не отмечена как оказаная
                if (s.Fulfil)
                {//Услуга уже отмечена как оказаная и удалить из назначения ее невозможно
                    MessageBox.Show("Не возможно удалить уже оказанную услугу",
                        "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                DialogResult answer = MessageBox.Show("Удалить улугу?",
                        "Удаление услуги", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.No) return;//Пользователь не подтвердил удаление
                 
                //Удаление услуги
                s.Delete();//Удаление  услуги.
                LoadService();//Перезагрузка списка услуг
                CalcService();//Перерасчет суммарной стоимости услуг
                CalcSum();//Пересчет общей суммы назначений
                if (!(mos.Count > 0 || so.Count > 0))
                    cmbTypeIH.Enabled = true;//Разрешается изменения типа истории болезни не осталось ни одной услуги или медикамента
            }
        }

        ///<summary>Завершение редактирования записи услуги в таблице связанное </summary>
        private void gridService_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == gridService.Columns["colCharacter"].Index)
            {//Изменение произошло в колонке указания характеристик
                int id = so[e.RowIndex].ServiceOutputID;//Копирование идентификатора назначаемой услуги в переменную

                ServiceOutput s = new ServiceOutput();//Инициализация экземпляра класса отпускаемого медикамента
                s.ServiceOutputID = id;
                s.Load();
                //Сохранение рецепта в свойстве класса 
                if (gridService.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    //при условии что ячейка с рецептом не пуста. Иначе возникает ошибка
                    s.Character = gridService.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                else
                    s.Character = "";//Очевидно что строка карактеристики пуста

                s.Update();//Обновление записи с экземпляром класса
                serviceEdit = false;
            }
            else if (e.ColumnIndex == gridService.Columns["colCount"].Index)
            {//Изменение произошло в колонке количества назначаемыхуслуг
                ServiceOutput s = so[e.RowIndex];//создается экземпляр услуги для истории болезни
                //Изменение количества может производиться  если услуга еще не отмечена как оказаная
                if (s.Fulfil)
                {//Услуга отмечена как оказаная и  изменение ее количества недопустимо
                    MessageBox.Show("Не возможно изменить количество уже оказанной услуги, но можно добавить такую же услугу и указать необходимое количество.",
                        "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    gridService.Refresh();//Перезагружаются данные в списке услуг 
                    return;
                }
                
                int countNew = (int)gridService[e.ColumnIndex, e.RowIndex].Value; //Сохраняется новое значение количества услуг
                s.Load();//Повторная перезагрузка данных из базы для восстановления прежнего количества услуг
                int countOld = s.Count; //Сохраняется предыдущее значение количества услуг
                CommandDirective directive; //Указание на то, как поступить в связи с изменением количества
                /* Прежде чем резервировать новое количество, нужно вычислить на сколько увеличивается или
                 * уменьшается резервируемое количество */
                int CountChange = countNew - countOld; // Количественное изменение услуги

                if (countNew == countOld)
                    directive = CommandDirective.Non; //Количество не изменилось после редактирования и никаких действий производить не требуется

                else if (countNew > countOld)
                {//Количество увеличилось
                    //Поэтому требуется проверка достаточного наличия медикаментов на складе
                        string absentmeds = s.CheckMedicamentOnSclad(CountChange, f);//Проверяется наличие медикаментов на складе
                    if (absentmeds != "")
                    {//Медикаментов недостаточно на складе требуется указать другое количество
                        string message = "Некоторые медикаменты отсутсвуют на складе: " + absentmeds + ". " +
                        " Вы не можете назначить такое количество услуг. ";//Выводится список отсутсвующих медикаментов
                        string caption = "Отсутствие медикаментов на складе";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        MessageBox.Show(message, caption, buttons, MessageBoxIcon.Warning);
                        return;//
                    }
                    else
                    {//Медикаментов достаточно на складе и можно производить их резервирование и расходование
                        directive = CommandDirective.Add;//Лучше заменить на директиву Change в этом случае можно изменить код обработки
                    }
                }
                else
                {//Количество уменьшилось и нужно уменьшить расход и снять с резерва
                    directive = CommandDirective.DeletePermanently;
                }

                //Выполняется добавление или удаление резерва и расхода в зависимости от поступившей директивы
                s.OutputMedicaments(directive, f, Math.Abs(CountChange));
                s.Count = countNew;//Присваивается новое значение количества услуг
                if (!s.Update())
                {//Обновление не было успешным
                    MessageBox.Show("Не удалось изменить количество назначенных услуг",
                        "Ошибка операции с базой данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //TODO В этом случае необходимо произвести откат всех произведенных действий с медикаментами
                    //... предположительно:  s.OutputMedicaments(directive, -CountChange); только нужно решить вопрос с инвертированием директивы
                }
                else
                {//Обновление произошло успешно
                    gridService.Refresh();
                    CalcService();//Пересчет суммы услуг
                    CalcSum();//Пересчет общей суммы назначений
                }
                serviceEdit = false;
            }           
        }

        ///<summary>Загрузка списка назначенных услуг</summary>
        void LoadService()
        {
            gridService.DataSource = null;
            so = new ServiceOutputList();
            so.Load(ih.DocumentID);
            gridService.DataSource = so;
        }
        ///<summary>Расчет суммы назначенных услуг</summary>
        void CalcService()
        {
            var cult = CultureInfo.CreateSpecificCulture("ru-RU");
            //Подсчет стоимости услуг    
            decimal s_ser = ih.SumService();//Вычисление стоимости услуг
            lblServicePrice.Text = s_ser.ToString("C", cult);//Отображение суммарных стоимостей услуг  
            ih.SummService = s_ser;//Передача суммы свойству класса
            ih.Update();//Запись суммы в базу данных
         }

#endregion



#region Вкладка "Наблюдения"

        ///<summary>Добавление наблюдения через шаблон"
        ///Заплатин
        ///12.10.2012</summary>
        private void tsbAddObservation_Click(object sender, EventArgs e)
        {
            if (tabControlObservations.SelectedTab.Name == tabPageTherapist.Name)
            {//Выбрана вкладка наблюдения терапевта 
                var o = new Observation(true);
                //Загрузка шаблона наблюдения
                var ihots = new IllnessHistoryObservationTemplateList();
                ihots.Load();//Загружаются все шаблоны и выбирается один последний 
                if (ihots.Count > 0)
                {//Найден хотя бы один шаблон
                    o.ObservationID = ihots[0].ObservationID;
                    o.Load();//Загрузка информации о шаблоне-наблюдениии
                }
                else
                {//шаблонов для историй болезней нет
                    MessageBox.Show("Не удается найти шаблон", "Ошибка загрузки  шаблона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                SaveObs();//Прежде чем создавать новое наблюдение, нужно сохранить возможные изменения уже в существующем
                o.DocumentID = ih.DocumentID;//Установка ссылки на некущую и.б. 
                o.ReceptionD = DateTime.Now;//Изменение даты шаблона на текущую
                o.DoctorID = Security.user.UserID;
                if (!o.Insert())
                    MessageBox.Show(this, "Не удалось добавить наблюдение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                LoadObserver();
            }
            else if (tabControlObservations.SelectedTab.Name == tabPageDerma.Name)
            {//Выбрана вкладка наблюдения дерматоловениролога
                var  o = new ObservationDerma(true);
                
                 //Загрузка шаблона наблюдения
                var ihots = new IllnessHistoryObservationDermaTemplateList();
                ihots.Load();//Загружаются все шаблоны и выбирается один последний 
                if (ihots.Count > 0)
                {//Найден хотя бы один шаблон
                    o.ObservationID = ihots[0].ObservationID;
                    o.Load();//Загрузка информации о шаблоне-наблюдениии
                }
                else
                {//шаблонов для историй болезней нет
                    MessageBox.Show("Не удается найти шаблон", "Ошибка загрузки  шаблона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                SaveObsDerma();//Прежде чем создавать новое наблюдение, нужно сохранить возможные изменения уже в существующем
                o.DocumentID = ih.DocumentID;//Установка ссылки на текущую и.б. 
                o.ReceptionD = DateTime.Now;//Изменение даты шаблона на текущую
                o.DoctorID = Security.user.UserID;
                if (!o.Insert())
                    MessageBox.Show(this, "Не удалось добавить наблюдение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                LoadObserverDerma();
            }

        }

        ///<summary>Кнопка "удалить наблюдение"</summary>
        private void tsbDeleteObservation_Click(object sender, EventArgs e)
        {
            if (tabControlObservations.SelectedTab.Name == tabPageTherapist.Name)
            {//Выбрана вкладка наблюдения терапевта 
                SaveObs();
                if (lbObserv.SelectedIndex >= 0)
                {
                    if (!obs[lbObserv.SelectedIndex].Delete())
                        MessageBox.Show(this, "Не удалось удалить наблюдение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                LoadObserver();
            }
            else if (tabControlObservations.SelectedTab.Name == tabPageDerma.Name)
            {//Выбрана вкладка наблюдения дерматоловениролога
                SaveObsDerma();
                if (lbObservDerma.SelectedIndex >= 0)
                {
                    if (!obsDerma[lbObservDerma.SelectedIndex].Delete())
                        MessageBox.Show(this, "Не удалось удалить наблюдение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                LoadObserverDerma();
            }
        }

        ///<summary>Загрузка списка наблюдений терапевта</summary>
        void LoadObserver()
        {
            lbObserv.DataSource = null;
            lbObserv.DisplayMember = "Head";
            lbObserv.ValueMember = "ObservationID";
            obs.Load(ih.DocumentID);
            for (int i = 0; i < obs.Count; i++)
                obs[i].FormBinding("observat", this);
            tabPageTherapist.Text =  "Терапевт (" + obs.Count.ToString() + ")";
            lbObserv.DataSource = obs.Select(x => new
            {
                x.ObservationID,
                x.Head
            }).ToList();
        }

        ///<summary>Загрузка списка наблюдений дермовенеролога</summary>
        void LoadObserverDerma()
        {
            lbObservDerma.DataSource = null;
            lbObservDerma.DisplayMember = "Head";
            lbObservDerma.ValueMember = "ObservationID";
            obsDerma.Load(ih.DocumentID);
            for (int i = 0; i < obsDerma.Count; i++)
                obsDerma[i].FormBinding("observatDerma", this);
            tabPageDerma.Text = "Дерматовенеролог (" + obsDerma.Count.ToString() + ")";
            lbObservDerma.DataSource = obsDerma.Select(x=> new 
            {
                x.ObservationID,
                x.Head
            }).ToList();
        }

        ///<summary>Клик по списку наблюдений</summary>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveObs();
            CurObs = lbObserv.SelectedIndex;
            if (CurObs >= 0)
                obs[CurObs].WriteToForm();
        }

        ///<summary>Сохранение текущего наблюдения терапевта </summary>
        void SaveObs()
        {
            if (CurObs < 0) return;
            obs[CurObs].ReadFromForm();
            obs[CurObs].Update();
        }

        /// <summary>Выбор наблюдения врача дерматоловениролога</summary>
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveObsDerma();
            CurObsDerma = lbObservDerma.SelectedIndex;
            if (CurObsDerma >= 0)
                obsDerma[CurObsDerma].WriteToForm();
        }

       
        ///<summary>Сохранение текущего наблюдения дерматоловениролога</summary>
        void SaveObsDerma()
        {
            if (CurObsDerma < 0) return;
            obsDerma[CurObsDerma].ReadFromForm();
            obsDerma[CurObsDerma].Update();
        }

        /// <summary>Создание шаблона на основе выбранного наблюдения
        /// Заплатин Е.Ф.
        /// 12.10.2012
        /// </summary>
        private void tsbMakeTemplate_Click(object sender, EventArgs e)
        {
            if (tabControlObservations.SelectedTab.Name == tabPageTherapist.Name)
            {//Выбрана вкладка наблюдения терапевта 

                if (lbObserv.SelectedItems.Count <= 0) 
                    return; //Нужно уведомить пользователя о необходимости выделения наблюдения на основе которого будет создан шаблон
 
                IllnessHistoryObservationTemplate ihot = new IllnessHistoryObservationTemplate(true);

                _MakeTemplate<IObservationDermaTemplate>(ihot, obs[lbObserv.SelectedIndex].ObservationID);
            }
            else if (tabControlObservations.SelectedTab.Name == tabPageDerma.Name)
            {//Выбрана вкладка наблюдения дерматоловениролога
                if (lbObservDerma.SelectedItems.Count <= 0)
                    return; //Нужно уведомить пользователя о необходимости выделения наблюдения на основе которого будет создан шаблон

                IllnessHistoryObservationDermaTemplate ihot = new IllnessHistoryObservationDermaTemplate(true);

                _MakeTemplate<IObservationDermaTemplate>(ihot, obsDerma[lbObservDerma.SelectedIndex].ObservationID);
            }

        }

        private void _MakeTemplate<T>(T ihot, int id) where T : IObservationDermaTemplate
        {
            string msg = "Для создания шаблона на основе выделеного наблюдения задайте его наименование.";

            frmInputText frm = new frmInputText(msg, "Создание шаблона наблюдения", "");

            bool friest = true;

            do //повторять вывод диалогового окна, пока не будет введено имя шаблона или не будет нажата отмена
            {
                if (!friest)
                    frm.LabelText = msg + "\r\n\r\nНе указано наименование шаблона.";

                if (DialogResult.OK != frm.ShowDialog())
                    return;

                friest = false;

            } while (frm.Value == "");

            ihot.ObservationID = id;
            ihot.Date = DateTime.Now;
            ihot.Name = frm.Value;
            ihot.EmploeeyID = Security.user.UserID;

            bool result = ihot.Insert();

            if (result)
            {
                MessageBox.Show("Шаблон успешно создан", "Завершение создания шаблона", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Не удается создать шаблон", "Ошибка создания шаблона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

 
        #endregion
        /// <summary>Закрытие формы с подтверждением о сохранении измененных данных
        /// Заплатин Е.Ф.
        /// 24.04.2013г.
        /// </summary>
        private void frmIllnessHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
                DialogResult hr = MessageBox.Show(this, "Сохранить изменения?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (hr == DialogResult.Yes)
                {
                    Save();
                }
                else if (hr == DialogResult.No)
                {
                    if (cmd.Mode == CommandModes.New) //Если создавалась новая история болезни
                        ih.Delete();//Удаление вновь созданной истории болезни
                }
                else if (hr == DialogResult.Cancel)
                    e.Cancel = true;
            }
            


        //Чирков Е.О.
        //27.06.10
        //Перехват ошибки, возникающей при вводе нецифрового значения в ячейку "Ко-во"
        private void gridService_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex != colCount.Index) return;
            MessageBox.Show("Ошибка при вводе количества услуг. Повторите ввод.", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.ThrowException = false;
        }

        //Чирков Е.О.
        //27.06.10
        //Перехват ошибки, возникающей при вводе нецифрового значения в ячейку "Ко-во"
        private void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex != colCountMed.Index) return;
            MessageBox.Show("Ошибка при вводе количества медикаментов. Повторите ввод.", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.ThrowException = false;
        }

        //Заплатин Е.Ф
        //08.09.2010
        //Открывается форма отчета книжки назначений и передается параметр с идентификатором истории болезни в свойстве TAG
        private void книжкаНазначенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptPatientBook rpt = new rptPatientBook();
            rpt.Tag = new Command(CommandModes.Open, ih.DocumentID, this);
            rpt.ShowDialog(this);
        }
        //Заплатин Е.Ф
        //09.09.2010
        //Открывается форма с отчетом истории болезни и передается параметр с идентификатором истории болезни в свойстве TAG
        private void историяБолезниToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptIllnessHistory rpt = new rptIllnessHistory();
            rpt.Tag = new Command(CommandModes.Open, ih.DocumentID, this);
            rpt.ShowDialog(this);
        }

        //private void cmbMKBOut_Enter(object sender, EventArgs e)
        //{
        //    //MKB = cmbMKBOut.SelectedIndex;//Фиксируется значение кода при получении фокуса 
        //}
        /// <summary>Отслеживание состояния редактирования ячейки
        /// Заплатин Е.Ф.
        /// 26.09.2011
        /// </summary>
        private void gridMeds_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            medEdit = true; //Производится редактирование записи
        }
        /// <summary>Отслеживание состояния редактирования ячейки
        /// Заплатин Е.Ф.
        /// 26.09.2011
        /// </summary>
        private void gridService_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            serviceEdit = true; //Производится редактирование записи
        }
        /// <summary>Визуализация текстового поля в зависимости от выбранного элемента списка
        /// Заплатин Е.Ф.
        /// 22.01.2012
        /// </summary>
        private void cmbStomachNormalIH_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtStomachNormal.Visible = cmbStomachNormalIH.SelectedIndex == (3-1);//Если выбирается вариант "другое" то визуализируется текстовое поле
        }
        /// <summary>Открытие амбулаторной карты для просмотра деталей по пациенту
        /// Заплатин Е.Ф.
        /// 22.01.2012
        /// </summary>
        private void амбулаторнаяКартаToolStripMenuItem_Click(object sender, EventArgs e)
        {
                frmAmbulanceCard frm = new frmAmbulanceCard();
                frm.Tag = new Command(CommandModes.Veiw, patient.PatientID);//Как ни странно но передается именно идентификатор пациента, а не амбулаторной карты. ЗЕФ.
                frm.MdiParent = MdiParent;
                frm.Show();
        }

        private void сохранитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Save();
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkOtherDinamic_CheckedChanged(object sender, EventArgs e)
        {
            txtOtherDinamic.Enabled = !chkOtherDinamic.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            txtNervose.Enabled = !chkNoPeculiarity.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkComplaint_CheckedChanged(object sender, EventArgs e)
        {
            txtComplaint.Enabled = !chkComplaint.Checked;
        }
        /// <summary>Регулируется видимость поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void cmbStomachNormalOSV_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtStomachNormalOSV.Visible = cmbStomachNormalOSV.SelectedIndex == (3 - 1);//Если выбирается вариант "другое" то визуализируется текстовое поле
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkInherit_CheckedChanged(object sender, EventArgs e)
        {
            txtInherit.Enabled = !chkInherit.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkDisorder_CheckedChanged(object sender, EventArgs e)
        {
            txtDisorder.Enabled = !chkDisorder.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkPregnancy_CheckedChanged(object sender, EventArgs e)
        {
            numPregnancy.Enabled = chkPregnancy.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkMenstrual_CheckedChanged(object sender, EventArgs e)
        {
            numMenstrualYear.Enabled = numMenstrualDays.Enabled = numMenstrualDaysFrom.Enabled = cmbMenstrualFormIH.Enabled = chkMenstrual.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkBirth_CheckedChanged(object sender, EventArgs e)
        {
            numBirth.Enabled = chkBirth.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkAbortion_CheckedChanged(object sender, EventArgs e)
        {
            numAbortion.Enabled = chkAbortion.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkGenecologyHarmfulness_CheckedChanged(object sender, EventArgs e)
        {
            txtGenecologyHarmfulness.Enabled = !chkGenecologyHarmfulness.Checked;

        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkTrauma_CheckedChanged(object sender, EventArgs e)
        {
            txtTrauma.Enabled = !chkTrauma.Checked;
        }
        /// <summary>Регулируется доступность поля
        /// Заплатин Е.Ф.
        /// 23.01.2012
        /// </summary>
        private void chkSupportMotion_CheckedChanged(object sender, EventArgs e)
        {
            txtSupportMotion.Enabled = !chkSupportMotion.Checked;
        }
        /// <summary>Выбор и добавление диагноза
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void AddMKB(bool main, MKBType type, IH_MKBCodeList MKBList, DataGridView dg)
        {
            //Проверка существования основного диагноза. Если уже существует, то невозможно добавить второй основной диагноз
            if (main) //добавляется основной диагноз
                if (ih.CheckMainMKB(type))
                {//Основной диагноз уже существует
                    MessageBox.Show("Не возможно добавить два и более основных диагноза. Чтобы заменить основной диагноз, удалите уже назначеный и добавьте новый.",
                                    "Невозможно добавить основной диагноз");
                    return;
                }
                        
            //Назначение терапевта
            if (ih.DoctorID == 0 && MKBOut.Count == 0)//Если за историей болезни не закреплен лечащий врач и не назначено ни одного диагноза при поступлении
                if (Doctor.isdoctor())//Проверить является ли текущей пользователь терапевтом. Вызывается статический метод класса
                {
                    ih.DoctorID = Security.user.PersonID;//Закрепить за историей текущего пользователя в качестве лечащего врача
                    cmbDoctor.SelectedValue = ih.DoctorID;//Установить значение поля терапевта 
                }

            //Выбор диагноза
            frmMKBCode frm = new frmMKBCode();
            frm.Tag = new Command(CommandModes.Select);
            if (frm.ShowDialog() != DialogResult.OK) return ;

            Command cmdMKB = (Command)frm.Tag;
            ih.AddMKB(cmdMKB.Id, main, type);
                       
            //Обновление списка диагнозов
            dg.DataSource = null;
            MKBList.Load();
            dg.DataSource = MKBList;
            
        }
        /// <summary>Выбор и добавление основного диагноза с места отбора
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void btnAddMKBOutMain_Click(object sender, EventArgs e)
        {
            AddMKB(true, MKBType.DiagOut, MKBOut, dgMKBOut);
        }
        /// <summary>Выбор и добавление сопутсвующего диагноза с места отбора
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void btnAddMKBOutNotMain_Click(object sender, EventArgs e)
        {
            AddMKB(false, MKBType.DiagOut, MKBOut, dgMKBOut);
        }
        /// <summary>Удаление диагноза
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void DeleteMKB(int index, IH_MKBCodeList MKBList, DataGridView dg)
        {
            MKBList[index].Delete();//Удаление выделенного диагноза
            //Обновление списка диагнозов
            dg.DataSource = null;
            MKBList.Load();
            dg.DataSource = MKBList;
        }
        /// <summary>Удаление диагноза с места отбора
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void btnDeleteMKBOut_Click(object sender, EventArgs e)
        {
            if (dgMKBOut.SelectedRows.Count > 0)
            {
                DeleteMKB(dgMKBOut.SelectedRows[0].Index, MKBOut, dgMKBOut); 
            }
        }
        /// <summary>Выбор и добавление основного диагноза с при поступлении
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton5_Click(object sender, EventArgs e)
        {
            AddMKB(true, MKBType.DiagAttend, MKBAttend, dgMKBAttend);
        }
        /// <summary>Выбор и добавление сопутсвующего диагноза с при поступлении
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton2_Click(object sender, EventArgs e)
        {
            AddMKB(false, MKBType.DiagAttend, MKBAttend, dgMKBAttend);
        }
        /// <summary>Удаление диагноза с при поступлении
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton6_Click(object sender, EventArgs e)
        {
            if (dgMKBAttend.SelectedRows.Count > 0)
            {
                DeleteMKB(dgMKBAttend.SelectedRows[0].Index, MKBAttend, dgMKBAttend);
            }
        }
        /// <summary>Выбор и добавление основного диагноза с при выписке
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton8_Click(object sender, EventArgs e)
        {
            AddMKB(true, MKBType.DiagMain, MKBMain, dgMKBMain);
        }
        /// <summary>Выбор и добавление сопутсвующего диагноза с при выписке
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton7_Click(object sender, EventArgs e)
        {
            AddMKB(false, MKBType.DiagMain, MKBMain, dgMKBMain);

        }
        /// <summary>Удаление диагноза с при выписке 
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void exButton9_Click(object sender, EventArgs e)
        {
            if (dgMKBMain.SelectedRows.Count > 0)
            {
                DeleteMKB(dgMKBMain.SelectedRows[0].Index, MKBMain, dgMKBMain);
            }
        }
        /// <summary>Перенос всех диагнозов при поступлении  в диагнозы при приеме
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// </summary>
        private void exButton3_Click(object sender, EventArgs e)
        {
            ih.MKBMove(MKBType.DiagOut, MKBType.DiagAttend);
            //Обновление списка диагнозов
            dgMKBAttend.DataSource = null;
            MKBAttend.Load();
            dgMKBAttend.DataSource = MKBAttend;
        }
        /// <summary>Перенос всех диагнозов  при приеме в  диагнозы заключительные при выписке
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// </summary>
        private void exButton4_Click(object sender, EventArgs e)
        {
            ih.MKBMove(MKBType.DiagAttend, MKBType.DiagMain);
            //Обновление списка диагнозов
            dgMKBMain.DataSource = null;
            MKBMain.Load();
            dgMKBMain.DataSource = MKBMain;
        }

        private void обратныйТалонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptReverseTicket rpt = new rptReverseTicket();
            rpt.Tag = new Command(CommandModes.Open, ih.DocumentID, this);
            rpt.ShowDialog(this);
        }

       

     



        

       

      

       


      

 


    }
}
