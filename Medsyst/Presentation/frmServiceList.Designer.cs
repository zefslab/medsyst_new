﻿namespace Medsyst
{
    partial class frmServiceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmServiceList));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbSelectAndClose = new System.Windows.Forms.ToolStripButton();
            this.btnSelect = new System.Windows.Forms.ToolStripButton();
            this.tsbtnClose = new System.Windows.Forms.ToolStripButton();
            this.treeCat = new System.Windows.Forms.TreeView();
            this.gridServices = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnit = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccessed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colServiceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd_Medicine = new ExButton.NET.ExButton();
            this.btnDelService = new ExButton.NET.ExButton();
            this.exButton1 = new ExButton.NET.ExButton();
            this.btnCut_Medicine = new ExButton.NET.ExButton();
            this.btnInsert_Medicine = new ExButton.NET.ExButton();
            this.btnToCategoryList = new ExButton.NET.ExButton();
            this.rbtnNotBudget = new System.Windows.Forms.RadioButton();
            this.rbtnIsBudget = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridServices)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelectAndClose,
            this.btnSelect,
            this.tsbtnClose});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1002, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbSelectAndClose
            // 
            this.tsbSelectAndClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelectAndClose.Image")));
            this.tsbSelectAndClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelectAndClose.Name = "tsbSelectAndClose";
            this.tsbSelectAndClose.Size = new System.Drawing.Size(131, 22);
            this.tsbSelectAndClose.Text = "Выбрать и закрыть";
            this.tsbSelectAndClose.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(74, 22);
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // tsbtnClose
            // 
            this.tsbtnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnClose.Image")));
            this.tsbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClose.Name = "tsbtnClose";
            this.tsbtnClose.Size = new System.Drawing.Size(57, 22);
            this.tsbtnClose.Text = "Закрыть";
            this.tsbtnClose.Visible = false;
            this.tsbtnClose.Click += new System.EventHandler(this.tsbtnClose_Click);
            // 
            // treeCat
            // 
            this.treeCat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeCat.Location = new System.Drawing.Point(3, 57);
            this.treeCat.Name = "treeCat";
            this.treeCat.Size = new System.Drawing.Size(258, 463);
            this.treeCat.TabIndex = 1;
            this.treeCat.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeCat_AfterSelect);
            // 
            // gridServices
            // 
            this.gridServices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridServices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.gridServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridServices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ServiceName,
            this.colUnit,
            this.Column2,
            this.Cost,
            this.Comment,
            this.colAccessed,
            this.colServiceID});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridServices.DefaultCellStyle = dataGridViewCellStyle14;
            this.gridServices.Location = new System.Drawing.Point(270, 57);
            this.gridServices.Name = "gridServices";
            this.gridServices.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridServices.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.gridServices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridServices.Size = new System.Drawing.Size(723, 434);
            this.gridServices.TabIndex = 2;
            this.gridServices.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridServices_CellDoubleClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Code";
            this.Column1.HeaderText = "Шифр";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // ServiceName
            // 
            this.ServiceName.DataPropertyName = "ServiceN";
            this.ServiceName.HeaderText = "Наименование";
            this.ServiceName.Name = "ServiceName";
            this.ServiceName.ReadOnly = true;
            this.ServiceName.Width = 200;
            // 
            // colUnit
            // 
            this.colUnit.DataPropertyName = "MeasureID";
            this.colUnit.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colUnit.HeaderText = "Ед. изм.";
            this.colUnit.Name = "colUnit";
            this.colUnit.ReadOnly = true;
            this.colUnit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colUnit.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "PriceMed";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle12.Format = "C2";
            dataGridViewCellStyle12.NullValue = null;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column2.HeaderText = "Цена лек. ср-в";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Cost
            // 
            this.Cost.DataPropertyName = "Cost";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle13.Format = "C2";
            dataGridViewCellStyle13.NullValue = null;
            this.Cost.DefaultCellStyle = dataGridViewCellStyle13;
            this.Cost.HeaderText = "Цена услуги";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // Comment
            // 
            this.Comment.DataPropertyName = "Comment";
            this.Comment.HeaderText = "Комментарий";
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            // 
            // colAccessed
            // 
            this.colAccessed.DataPropertyName = "Accessed";
            this.colAccessed.HeaderText = "Доступна";
            this.colAccessed.Name = "colAccessed";
            this.colAccessed.ReadOnly = true;
            this.colAccessed.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colAccessed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colAccessed.Width = 60;
            // 
            // colServiceID
            // 
            this.colServiceID.DataPropertyName = "ServiceID";
            this.colServiceID.HeaderText = "ID";
            this.colServiceID.Name = "colServiceID";
            this.colServiceID.ReadOnly = true;
            this.colServiceID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ктегории услуг";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Услуги";
            // 
            // btnAdd_Medicine
            // 
            this.btnAdd_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd_Medicine.Image")));
            this.btnAdd_Medicine.Location = new System.Drawing.Point(562, 28);
            this.btnAdd_Medicine.Name = "btnAdd_Medicine";
            this.btnAdd_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnAdd_Medicine.TabIndex = 51;
            this.btnAdd_Medicine.Text = "Добавить";
            this.btnAdd_Medicine.Click += new System.EventHandler(this.btnAdd_Medicine_Click);
            // 
            // btnDelService
            // 
            this.btnDelService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelService.Image = ((System.Drawing.Image)(resources.GetObject("btnDelService.Image")));
            this.btnDelService.Location = new System.Drawing.Point(910, 28);
            this.btnDelService.Name = "btnDelService";
            this.btnDelService.Size = new System.Drawing.Size(81, 23);
            this.btnDelService.TabIndex = 61;
            this.btnDelService.Text = "Удалить";
            this.btnDelService.Click += new System.EventHandler(this.btnDelService_Click);
            // 
            // exButton1
            // 
            this.exButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton1.Image = ((System.Drawing.Image)(resources.GetObject("exButton1.Image")));
            this.exButton1.Location = new System.Drawing.Point(649, 28);
            this.exButton1.Name = "exButton1";
            this.exButton1.Size = new System.Drawing.Size(81, 23);
            this.exButton1.TabIndex = 66;
            this.exButton1.Text = "Изменить";
            this.exButton1.Click += new System.EventHandler(this.exButton1_Click);
            // 
            // btnCut_Medicine
            // 
            this.btnCut_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCut_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnCut_Medicine.Image")));
            this.btnCut_Medicine.Location = new System.Drawing.Point(736, 28);
            this.btnCut_Medicine.Name = "btnCut_Medicine";
            this.btnCut_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnCut_Medicine.TabIndex = 67;
            this.btnCut_Medicine.Text = "Вырезать";
            // 
            // btnInsert_Medicine
            // 
            this.btnInsert_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsert_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnInsert_Medicine.Image")));
            this.btnInsert_Medicine.Location = new System.Drawing.Point(823, 28);
            this.btnInsert_Medicine.Name = "btnInsert_Medicine";
            this.btnInsert_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnInsert_Medicine.TabIndex = 68;
            this.btnInsert_Medicine.Text = "Вставить";
            // 
            // btnToCategoryList
            // 
            this.btnToCategoryList.Image = null;
            this.btnToCategoryList.Location = new System.Drawing.Point(233, 28);
            this.btnToCategoryList.Name = "btnToCategoryList";
            this.btnToCategoryList.Size = new System.Drawing.Size(28, 23);
            this.btnToCategoryList.TabIndex = 54;
            this.btnToCategoryList.Text = "...";
            this.btnToCategoryList.Click += new System.EventHandler(this.btnDelCategory_Click);
            // 
            // rbtnNotBudget
            // 
            this.rbtnNotBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbtnNotBudget.AutoSize = true;
            this.rbtnNotBudget.Location = new System.Drawing.Point(428, 503);
            this.rbtnNotBudget.Name = "rbtnNotBudget";
            this.rbtnNotBudget.Size = new System.Drawing.Size(102, 17);
            this.rbtnNotBudget.TabIndex = 155;
            this.rbtnNotBudget.Tag = "";
            this.rbtnNotBudget.Text = "внебюджетные";
            this.rbtnNotBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnIsBudget
            // 
            this.rbtnIsBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbtnIsBudget.AutoSize = true;
            this.rbtnIsBudget.Checked = true;
            this.rbtnIsBudget.Location = new System.Drawing.Point(338, 503);
            this.rbtnIsBudget.Name = "rbtnIsBudget";
            this.rbtnIsBudget.Size = new System.Drawing.Size(84, 17);
            this.rbtnIsBudget.TabIndex = 154;
            this.rbtnIsBudget.TabStop = true;
            this.rbtnIsBudget.Tag = "demand.IsBudget";
            this.rbtnIsBudget.Text = "бюджетные";
            this.rbtnIsBudget.UseVisualStyleBackColor = true;
            this.rbtnIsBudget.CheckedChanged += new System.EventHandler(this.rbtnIsBudget_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(274, 505);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 153;
            this.label9.Text = "Средства";
            // 
            // frmServiceList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 532);
            this.Controls.Add(this.rbtnNotBudget);
            this.Controls.Add(this.rbtnIsBudget);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnInsert_Medicine);
            this.Controls.Add(this.btnCut_Medicine);
            this.Controls.Add(this.exButton1);
            this.Controls.Add(this.btnDelService);
            this.Controls.Add(this.btnToCategoryList);
            this.Controls.Add(this.btnAdd_Medicine);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridServices);
            this.Controls.Add(this.treeCat);
            this.Controls.Add(this.toolStrip1);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "frmServiceList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Прейскурант услуг";
            this.Load += new System.EventHandler(this.frmServices_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TreeView treeCat;
        private System.Windows.Forms.DataGridView gridServices;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, true)]
        public ExButton.NET.ExButton btnAdd_Medicine;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, true)]
        public ExButton.NET.ExButton btnCut_Medicine;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, true)]
        public ExButton.NET.ExButton btnInsert_Medicine;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, true)]
        public ExButton.NET.ExButton btnDelService;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, true)]
        public ExButton.NET.ExButton exButton1;
        private System.Windows.Forms.ToolStripButton btnSelect;
        public ExButton.NET.ExButton btnToCategoryList;
        private System.Windows.Forms.ToolStripButton tsbSelectAndClose;
        private System.Windows.Forms.RadioButton rbtnNotBudget;
        private System.Windows.Forms.RadioButton rbtnIsBudget;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripButton tsbtnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colAccessed;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServiceID;
    }
}