﻿//30.07.09
//Проект и частичная реализация диалога добавления подразделения

//3.08.09
//Выбор рукододителя
//Быбор м/о
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmDivision : Form //перед выводом формы в свойстве Tag передается экзэмпляр Division
    {
        Division div = new Division(true);
        bool DelAfterCancel = true;//Указывает нужно ли удалять созданное подразделение после закрытия формы

        
        public frmDivision()
        {
            InitializeComponent();
        }
    private void frmDivAdd_Load(object sender, EventArgs e)
        {
            cmd = (Command)Tag;
            if (cmd.Mode == CommandModes.Edit)//Передан интерфейс, значит следует редактировать 
                                              //свойства существующего подразделения
            {
                this.Text = "Изменить данные подразделения";
                this.toolbtnAdd.Text = "Обновить";
                div.DivisionID = cmd.Id;
                div.Load();
            }
            else if (cmd.Mode == CommandModes.New)//Создается новое подразделение
            {
                div.ParentID = ((List<int>)cmd.obj)[0];//Родительское подразделение
                div.FirmID = ((List<int>)cmd.obj)[1];//Фирма к которой принадлежит подразделение
                div.Insert("Division");
                div.DivisionID = div.Identity("Division");
            }
            
            DelAfterCancel = Tag == null;

            txtName.Text = div.Name;
            txtURL.Text = div.URL;
            txtMail.Text = div.EMail;
            txtShortName.Text = div.ShortName;
           
        }
        /// <summary>Закрытие формы с сохранением свойств нового подразделения 
        /// Заплатин Е.Ф.
        /// 
        /// </summary>

        private void toolbtnAdd_Click(object sender, EventArgs e)
        {
            DelAfterCancel = false;
            div.Name = txtName.Text;
            div.URL = txtURL.Text;
            div.EMail = txtMail.Text;
            div.ShortName = txtShortName.Text;
            div.Update();
            
            cmd.Id = div.DivisionID;
            Tag = cmd;
            DialogResult = DialogResult.OK;
            if (IsMdiChild) Close();
        }
        private void frmDivAdd_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DelAfterCancel)
            {
                Division d = new Division(true);
                d.DivisionID = div.DivisionID;
                d.Load();
                d.Delete();
            };
        }
    }
}
