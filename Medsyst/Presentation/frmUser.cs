﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmUser : Form
    {
        User user = new User();

        public frmUser()
        {
            InitializeComponent();
        }

        //Чирков Е.О.
        //15.03.10
        //Выбор пользователя из сотрудников
        private void btnSelectUser_Click(object sender, EventArgs e)
        {
            frmEmployeeList frm = new frmEmployeeList();
            //Предается в вызываемую форму режим выбора
            frm.Tag = new Command(CommandModes.Select, user.UserID, this);
            frm.ShowDialog();
            Command cmd = frm.Tag as Command;
            if (cmd.Mode != CommandModes.Return) return;

            //Чирков
            //19.03.2010
            int EmpID = cmd.Id;
            cmd = Tag as Command;
            if (cmd.Mode == CommandModes.New) //создаем нового пользователя из существующего сотрудника
            {
                user = new User();
                user.PersonID = EmpID;
                user.UserID = EmpID;
                user.Load();
                //22.03.10
                //Чирков Е.О.
                if (user.IsExists())
                {
                    MessageBox.Show("Пользователь " + user.Fullname + " уже существует, выберите другого сотрудника", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    NewUserSaved = true;
                    return;
                }
                //user.UserID = user.PersonID;
                user.InsertFull("PersonUser");

                //добавляем созданного пользователя в группу "Гость", если его нет ни в одной группе
                User_UserGroups ugs = new User_UserGroups();
                ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + user.UserID;
                ugs.Load();
                if (ugs.Count == 0)
                {
                    User_UserGroup ug = new User_UserGroup();
                    ug.UserID = user.UserID;
                    ug.UGroupID = (int)Groups.Guest;
                    ug.Insert();
                    ug.User_UserGroupID = ug.Identity("PersonUser_UserGroup");
                    user.UGroupIDs.Add(ug.UGroupID);
                }
            }
            FillForm(user.UserID);
        }

        //Чирков Е.О.
        //15.03.10
        //Заполнение полей формы
        void FillForm(int id)
        {
            user.PersonID = id;
            user.UserID = id;
            user.Load();
            txtFIO.Text = user.Fullname;
            txtLogin.Text = user.Login;

            lbUserGroups.Items.Clear();
            User_UserGroups ugs = new User_UserGroups();
            ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + user.UserID;
            ugs.Load();
            foreach (User_UserGroup ug in ugs)
            {
                UserGroup g = new UserGroup();
                g.UGroupID = ug.UGroupID;
                g.Load();
                lbUserGroups.Items.Add(g);
            }
        }

        //Чирков Е.О.
        //15.03.10
        //Добавить пользователя в группу
        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            frmUserGroups frm = new frmUserGroups();
            frm.Tag = new Command(CommandModes.Select, this);
            frm.ShowDialog();
            Command cmd = frm.Tag as Command;
            if (cmd.Mode != CommandModes.Return) return;

            UserGroup ug = new UserGroup();
            ug.UGroupID = cmd.Id;
            ug.Load();
            lbUserGroups.Items.Add(ug);
            user.UGroupIDs.Add(ug.UGroupID);

            User_UserGroup g = new User_UserGroup();
            g.UserID = user.UserID;
            g.UGroupID = ug.UGroupID;
            g.Insert();
        }

        //Чирков Е.О.
        //15.03.10, 19.03.10, 22.03.10
        bool NewUserSaved = false;
        private void frmUser_Load(object sender, EventArgs e)
        {
            int id = 0;
            if ((Tag != null) && (Tag is Command))
            {
                Command cmd = Tag as Command;
                if (cmd.Mode == CommandModes.Select) toolbtnReady.Text = "Выбрать";
                else if (cmd.Mode == CommandModes.Edit)
                {
                    txtPassword.ReadOnly = true;
                    txtPassword.Text = "******";
                    txtPassword2.Visible = false;
                    btnChangePassword.Visible = true;
                    btnSelectUser.Visible = false;
                    txtLogin.ReadOnly = true;
                }
                NewUserSaved = (cmd.Mode != CommandModes.New);
                id = cmd.Id;
            }
            if (id == 0) return;
            FillForm(id);
        }

        //Чирков Е.О.
        //15.03.10, 19.03.10, 22.03.10
        private void toolbtnReady_Click(object sender, EventArgs e)
        {
            Command cmd = Tag as Command;
            bool IsEdit = (cmd != null) && (cmd.Mode == CommandModes.Edit);

            //Чирков Е.О.
            //22.03.10
            if ((!IsEdit) && (txtLogin.Text == ""))
            {
                MessageBox.Show("Необходимо задать логическое имя", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            /////////////

            if ((!IsEdit) && (txtPassword.Text == ""))
            {
                MessageBox.Show("Пустые пароли запрещены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ((!IsEdit) && (txtPassword.Text != txtPassword2.Text))
            {
                MessageBox.Show("Неверно подтвержден пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!IsEdit)
            {
                user.Password = txtPassword.Text;
                user.Login = txtLogin.Text;
                user.Update();
            }
            else user.UpdateNoPassword();

            NewUserSaved = true;
            Tag = new Command(CommandModes.Return, user.UserID, this);
            DialogResult = DialogResult.OK;
            if (IsMdiChild) Close();
        }

        //Чирков Е.О.
        //15.03.10
        //Исключить пользователя из выделенной группы
        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (lbUserGroups.SelectedIndex < 0) return;
            UserGroup ug = lbUserGroups.SelectedItem as UserGroup;
            User_UserGroups gs = new User_UserGroups();
            gs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE (UserID=" + user.UserID + ") AND (UGroupID=" + ug.UGroupID + ")";
            gs.Load();
            user.UGroupIDs.Remove(ug.UGroupID);
            foreach (User_UserGroup g in gs)
            {
                User_UserGroup gg = new User_UserGroup();
                gg.User_UserGroupID = g.User_UserGroupID;
                gg.Load();
                gg.Delete();
            }
            lbUserGroups.Items.Remove(ug);
        }

        //Чирков
        //18.03.10
        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            frmUserPasswordChange frm = new frmUserPasswordChange();
            frm.Tag = new Command(CommandModes.Edit, user.UserID, this);
            frm.ShowDialog();
        }

        //Чирков Е.О.
        //22.03.10
        private void frmUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NewUserSaved) return;
            user.RemoveFromTable(false); //удалить полностью
        }
    }
}
