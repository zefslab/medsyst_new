﻿//9.07.09
//добавление подразделения
//изменение названия подразделения
//14.07.09
//добавление в корень или подузел
//22.07.09
//удаление подразделения
//3.08.09
//Кнопки 'Вверх' и 'Вниз' работают только для дерева, но не для данных БД (изменения не сохраняются)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmDivisionList : Form
    {
        DivisionList divs;
        FirmList firms;
        int Div = 0;

        public frmDivisionList()
        {
            InitializeComponent();
        }

        private void frmDivisionList_Load(object sender, EventArgs e) //в свойстве Tag может передаваться DivisionID - для выбора узла
        {
            cmd = (Command)Tag;

           // Загрузка списка организаций для которых будет редактироватся дерево подразделений
            firms = new FirmList();
            firms.Load();
            cmbFirmList.DataSource = null;
            cmbFirmList.DataSource = firms;
            
            if (cmd.Mode == CommandModes.Select)//Форма открыта для выбора подразделения
            {
                if (cmd.type == typeof(Firm))
                {
                    cmbFirmList.SelectedValue = cmd.Id;
                }
                toolbtnSelect.Visible = true;
                cmbFirmList.Enabled = false;
            }
            DivisionLoad();
            btnPaste.Enabled = false;
        }
        /// <summary> Загрузка дерева подразделений  
        /// Заплатин Е.Ф.
        /// 09.04.2013
        /// </summary>
        private void DivisionLoad()
        {
                       
            divs = new DivisionList();
            CommandDirective cd;
            cd = (chkShowDeleted.Checked? CommandDirective.LoadDeleted : CommandDirective.NotLoadDeleted);
            divs.Load((int)cmbFirmList.SelectedValue, CommandDirective.Non, cd);
            divs.FillTree(treeDivision);
            if (Div != 0)
                treeDivision.SelectedNode = GetNodeFromID(null, (int)Tag);
        }

        /// <summary>возвращает экземпляр узла дерева с идентификатором ID для его последующего выделения</summary>
        /// <param name="Start"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        TreeNode GetNodeFromID(TreeNode Start, int ID)
        {
            TreeNode node = null;
            TreeNodeCollection st;
            if (Start == null) st = treeDivision.Nodes;
            else st = Start.Nodes;
            foreach(TreeNode n in st)
            {
                if (n.Nodes.Count > 0) node = GetNodeFromID(n, ID);
                if (node != null) return node;
                if (Convert.ToInt32(n.Tag) == ID) return n;
            }
            return node;
        }


        /// <summary>Добавление узла в дерево
        /// Заплатин Е.Ф. (основательно переработал)
        /// 09.04.2013
        /// </summary>
        /// <param name="InRoot">Указывает добавлять в корень или в существующий узел</param>
        void AddToTree(bool InRoot)
        {
            if (treeDivision.SelectedNode == null) InRoot = true;//Если добавляется первый узел то независимо отнажатия кнопки добавление производится в корень ЗЕФ. 26.01.2013
            //Подготоваливается к открытию форма содержащая свойства подразделения
            frmDivision dlg = new frmDivision();
            Command cmd_div = new Command(CommandModes.New);
            //Передается идентификатор родительского подразделения через дополнительное объектное свойство интерфейса
            List<int> l = new List<int>();
            l.Add(InRoot ? 0 : (int)treeDivision.SelectedNode.Tag); //первым в списке передается индекс родительского подразделения
            l.Add((int)cmbFirmList.SelectedValue);//вторым передается фирма к которой принадлежит подраделение
            cmd_div.obj = l ;
            dlg.Tag = cmd_div;
            DialogResult res = dlg.ShowDialog();
            if (res != DialogResult.OK) return;

            Division div = new Division(true);
            div.DivisionID = ((Command)dlg.Tag).Id;
            div.Load();

            div.list_order = (InRoot ? treeDivision.Nodes.Count + 1 : treeDivision.SelectedNode.Nodes.Count + 1);
            TreeNode node = new TreeNode();
            node.Text = div.Name;
            div.DivisionID = div.Identity("Division");
            node.Tag = div.DivisionID;
            if (InRoot) treeDivision.Nodes.Add(node);
            else treeDivision.SelectedNode.Nodes.Add(node);

            divs.Add(div);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddToTree(false);
        }
        /// <summary>Редактирование подразделения
        /// Заплатин Е.Ф.
        /// 09.04.2013
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (treeDivision.SelectedNode == null) return;
            
            frmDivision dlg = new frmDivision();
            Division div = divs[divs.GetIndexFromID(Convert.ToInt32(treeDivision.SelectedNode.Tag))];
            Command  cmd_div = new Command(CommandModes.Edit, div.DivisionID, this);
            dlg.Tag = cmd_div;
            DialogResult res = dlg.ShowDialog();
            if (res != DialogResult.OK) return;
            div.DivisionID = ((Command)dlg.Tag).Id;
            div.Load();
            treeDivision.SelectedNode.Text = div.Name;
        }

        private void вКореньToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddToTree(true);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            divs.DeleteFromTable(treeDivision.SelectedNode);
            if (treeDivision.SelectedNode != null) treeDivision.SelectedNode.Remove();
            DivisionLoad();
        }

        /// <summary>Выбор подразделения и передача его вызвавшей форме
        //// Заплатин Е.Ф. 
        /// </summary>

        private void toolbtnSelect_Click(object sender, EventArgs e) //Выбрать - свойство Tag = DivisionID, DialogResult = OK
        {
            if (treeDivision.SelectedNode == null) return;
            cmd.Id = Convert.ToInt32(treeDivision.SelectedNode.Tag);
            DialogResult = DialogResult.OK;
            Close();
        }
        /// <summary>Управление кнопкой "Восстановления" в зависимости от выбранного подразделения
        /// Если выбрано удаленное подразделение, то кнопка восстановления активизируется
        /// Заплатин Е.Ф.
        /// 09.04.2013
        /// </summary>
        private void treeDivision_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeDivision.SelectedNode == null) return;
            btnRestore.Enabled = (divs[divs.GetIndexFromID(Convert.ToInt32(treeDivision.SelectedNode.Tag))].Deleted == true);
        }

        void MoveDivision(int DivID, int ParentTo)
        {
            Division div = new Division(true);
            div.DivisionID = DivID;
            div.Load();
            div.ParentID = ParentTo;
            div.Update();

            TreeNode node = GetNodeFromID(null, DivID);
            TreeNode parent = GetNodeFromID(null, ParentTo);
            
            if (parent != null) parent.Nodes.Add(node);
        }

        private void btnAdd_SplitButtonClick(object sender, EventArgs e)
        {
            menuAdd.Show(btnAdd, new Point(0, btnAdd.Height));
        }

        bool WasCut = false;
        TreeNode cutNode = null;
        int cutid = 0;
        /// <summary>
        /// Вырезание подразделения из дерева для последующей его вставки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCut_Click(object sender, EventArgs e)
        {
            if (treeDivision.SelectedNode == null) return;
            WasCut = true;
            cutNode = treeDivision.SelectedNode;//Получение выделенного узла для вырезания
            cutid = (int)cutNode.Tag;
            cutNode.Remove();//удаление узла из дерева
            btnPaste.Enabled = true;
        }
        /// <summary>
        /// Вставка вырезанного подразделения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPaste_Click(object sender, EventArgs e)
        {
            if (!WasCut) return;
            TreeNode node = treeDivision.SelectedNode;
            int id = 0;
            if (node != null) id = (int)node.Tag;

            Division div = new Division(true);
            div.DivisionID = cutid;
            div.Load();
            div.ParentID = id;
            div.Update();

            TreeNode parent = GetNodeFromID(null, id);

            if (parent != null) parent.Nodes.Add(cutNode);

            WasCut = false;
            cutNode = null;
            btnPaste.Enabled = false;
            cutid = 0;
        }
        /// <summary>Загрузка дерева подразделений по выбранной фирме 
        /// Заплатин Е.Ф.
        /// 26.03.2013
        /// </summary>
        private void cmbFirmList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DivisionLoad();
        }
        /// <summary>Регулирует отображение удаленных подразделений
        /// Заплатин Е.Ф.
        /// 09.04.2013
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DivisionLoad();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            Division div = divs[divs.GetIndexFromID(Convert.ToInt32(treeDivision.SelectedNode.Tag))];
            div.Deleted = false;
            div.Update();
            DivisionLoad();
        }

  
    }
}
