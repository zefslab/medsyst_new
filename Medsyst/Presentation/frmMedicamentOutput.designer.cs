﻿using Medsyst.DataSet;

namespace Medsyst
{
    partial class frmMedicamentOutput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMedicamentOutput));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridOutDocuments = new System.Windows.Forms.DataGridView();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.colUnReserved = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedicamentN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCountOutput = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabDocOutType = new System.Windows.Forms.TabControl();
            this.tabPageIH = new System.Windows.Forms.TabPage();
            this.groupFilterIH = new System.Windows.Forms.GroupBox();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.cmbCategoryPacient = new System.Windows.Forms.ComboBox();
            this.personPatientCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.labelFilter3 = new System.Windows.Forms.Label();
            this.labelFilter2 = new System.Windows.Forms.Label();
            this.labelFilter1 = new System.Windows.Forms.Label();
            this.tabPagePayService = new System.Windows.Forms.TabPage();
            this.groupFilterActChargeoff = new System.Windows.Forms.GroupBox();
            this.tabPageChek = new System.Windows.Forms.TabPage();
            this.groupFilterChek = new System.Windows.Forms.GroupBox();
            this.tabPageDemand = new System.Windows.Forms.TabPage();
            this.groupFilterDemand = new System.Windows.Forms.GroupBox();
            this.cmbSeasonDemand = new System.Windows.Forms.ComboBox();
            this.intYearDemand = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbMember = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbShowAll = new System.Windows.Forms.RadioButton();
            this.rbShowReserved = new System.Windows.Forms.RadioButton();
            this.rbShowNonReserved = new System.Windows.Forms.RadioButton();
            this.cachedStudentByFacult1 = new Medsyst.Reports.CachedStudentByFacult();
            this.personPatientCategoryTableAdapter = new Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter();
            this.colDocID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridOutDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabDocOutType.SuspendLayout();
            this.tabPageIH.SuspendLayout();
            this.groupFilterIH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            this.tabPagePayService.SuspendLayout();
            this.tabPageChek.SuspendLayout();
            this.tabPageDemand.SuspendLayout();
            this.groupFilterDemand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intYearDemand)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridOutDocuments
            // 
            this.gridOutDocuments.AllowUserToAddRows = false;
            this.gridOutDocuments.AllowUserToDeleteRows = false;
            this.gridOutDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridOutDocuments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridOutDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOutDocuments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDocID,
            this.colNum,
            this.colDesc,
            this.colDate,
            this.colSum,
            this.colDoctor});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridOutDocuments.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridOutDocuments.Location = new System.Drawing.Point(0, 148);
            this.gridOutDocuments.Name = "gridOutDocuments";
            this.gridOutDocuments.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridOutDocuments.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridOutDocuments.RowHeadersVisible = false;
            this.gridOutDocuments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridOutDocuments.Size = new System.Drawing.Size(519, 453);
            this.gridOutDocuments.TabIndex = 70;
            this.gridOutDocuments.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridOutDocuments_CellMouseClick);
            this.gridOutDocuments.SelectionChanged += new System.EventHandler(this.gridOutDocuments_SelectionChanged);
            // 
            // gridMeds
            // 
            this.gridMeds.AllowUserToAddRows = false;
            this.gridMeds.AllowUserToDeleteRows = false;
            this.gridMeds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUnReserved,
            this.colNbr,
            this.colMedicamentN,
            this.colCountOutput,
            this.colPrice});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridMeds.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridMeds.Location = new System.Drawing.Point(0, 148);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridMeds.RowHeadersVisible = false;
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(469, 453);
            this.gridMeds.TabIndex = 74;
            this.gridMeds.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellContentClick);
            // 
            // colUnReserved
            // 
            this.colUnReserved.DataPropertyName = "UnReserved";
            this.colUnReserved.FalseValue = "0";
            this.colUnReserved.FillWeight = 50F;
            this.colUnReserved.HeaderText = "Выдано";
            this.colUnReserved.Name = "colUnReserved";
            this.colUnReserved.ReadOnly = true;
            this.colUnReserved.TrueValue = "1";
            this.colUnReserved.Width = 50;
            // 
            // colNbr
            // 
            this.colNbr.DataPropertyName = "MedicamentID";
            this.colNbr.FillWeight = 30F;
            this.colNbr.HeaderText = "№";
            this.colNbr.Name = "colNbr";
            this.colNbr.ReadOnly = true;
            this.colNbr.Width = 30;
            // 
            // colMedicamentN
            // 
            this.colMedicamentN.DataPropertyName = "MedicamentN";
            this.colMedicamentN.FillWeight = 240F;
            this.colMedicamentN.HeaderText = "Наименование";
            this.colMedicamentN.Name = "colMedicamentN";
            this.colMedicamentN.ReadOnly = true;
            this.colMedicamentN.Width = 240;
            // 
            // colCountOutput
            // 
            this.colCountOutput.DataPropertyName = "CountOutput";
            this.colCountOutput.FillWeight = 30F;
            this.colCountOutput.HeaderText = "Кол-во";
            this.colCountOutput.Name = "colCountOutput";
            this.colCountOutput.ReadOnly = true;
            this.colCountOutput.Width = 30;
            // 
            // colPrice
            // 
            this.colPrice.DataPropertyName = "PriceSum";
            this.colPrice.HeaderText = "Цена";
            this.colPrice.Name = "colPrice";
            this.colPrice.ReadOnly = true;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(995, 604);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(995, 629);
            this.toolStripContainer1.TabIndex = 76;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridOutDocuments);
            this.splitContainer1.Panel1.Controls.Add(this.tabDocOutType);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilter);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.gridMeds);
            this.splitContainer1.Size = new System.Drawing.Size(995, 604);
            this.splitContainer1.SplitterDistance = 522;
            this.splitContainer1.TabIndex = 76;
            // 
            // tabDocOutType
            // 
            this.tabDocOutType.Controls.Add(this.tabPageIH);
            this.tabDocOutType.Controls.Add(this.tabPagePayService);
            this.tabDocOutType.Controls.Add(this.tabPageChek);
            this.tabDocOutType.Controls.Add(this.tabPageDemand);
            this.tabDocOutType.Location = new System.Drawing.Point(8, 20);
            this.tabDocOutType.Name = "tabDocOutType";
            this.tabDocOutType.SelectedIndex = 0;
            this.tabDocOutType.Size = new System.Drawing.Size(503, 99);
            this.tabDocOutType.TabIndex = 86;
            this.tabDocOutType.SelectedIndexChanged += new System.EventHandler(this.tabDocOutType_SelectedIndexChanged);
            // 
            // tabPageIH
            // 
            this.tabPageIH.Controls.Add(this.groupFilterIH);
            this.tabPageIH.Location = new System.Drawing.Point(4, 22);
            this.tabPageIH.Name = "tabPageIH";
            this.tabPageIH.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIH.Size = new System.Drawing.Size(495, 73);
            this.tabPageIH.TabIndex = 0;
            this.tabPageIH.Tag = "4";
            this.tabPageIH.Text = "Истории болезней";
            this.tabPageIH.UseVisualStyleBackColor = true;
            // 
            // groupFilterIH
            // 
            this.groupFilterIH.Controls.Add(this.cmbSeason);
            this.groupFilterIH.Controls.Add(this.numYear);
            this.groupFilterIH.Controls.Add(this.cmbCategoryPacient);
            this.groupFilterIH.Controls.Add(this.labelFilter3);
            this.groupFilterIH.Controls.Add(this.labelFilter2);
            this.groupFilterIH.Controls.Add(this.labelFilter1);
            this.groupFilterIH.Location = new System.Drawing.Point(6, 3);
            this.groupFilterIH.Name = "groupFilterIH";
            this.groupFilterIH.Size = new System.Drawing.Size(484, 65);
            this.groupFilterIH.TabIndex = 85;
            this.groupFilterIH.TabStop = false;
            this.groupFilterIH.Text = "Фильтр по историям болезней";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "SeasonNbr";
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(390, 38);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(87, 21);
            this.cmbSeason.TabIndex = 88;
            this.cmbSeason.ValueMember = "SeasonID";
            // 
            // numYear
            // 
            this.numYear.Location = new System.Drawing.Point(390, 14);
            this.numYear.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.numYear.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.numYear.Name = "numYear";
            this.numYear.Size = new System.Drawing.Size(87, 20);
            this.numYear.TabIndex = 87;
            this.numYear.Value = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.numYear.ValueChanged += new System.EventHandler(this.numYear_ValueChanged);
            // 
            // cmbCategoryPacient
            // 
            this.cmbCategoryPacient.DataSource = this.personPatientCategoryBindingSource;
            this.cmbCategoryPacient.DisplayMember = "CategoryN";
            this.cmbCategoryPacient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategoryPacient.FormattingEnabled = true;
            this.cmbCategoryPacient.Location = new System.Drawing.Point(126, 14);
            this.cmbCategoryPacient.Name = "cmbCategoryPacient";
            this.cmbCategoryPacient.Size = new System.Drawing.Size(168, 21);
            this.cmbCategoryPacient.TabIndex = 86;
            this.cmbCategoryPacient.ValueMember = "CategoryID";
            // 
            // personPatientCategoryBindingSource
            // 
            this.personPatientCategoryBindingSource.DataMember = "PersonPatientCategory";
            this.personPatientCategoryBindingSource.DataSource = this.medsystDataSet;
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelFilter3
            // 
            this.labelFilter3.AutoSize = true;
            this.labelFilter3.Location = new System.Drawing.Point(4, 16);
            this.labelFilter3.Name = "labelFilter3";
            this.labelFilter3.Size = new System.Drawing.Size(116, 13);
            this.labelFilter3.TabIndex = 85;
            this.labelFilter3.Text = "Категория пациентов";
            // 
            // labelFilter2
            // 
            this.labelFilter2.AutoSize = true;
            this.labelFilter2.Location = new System.Drawing.Point(312, 41);
            this.labelFilter2.Name = "labelFilter2";
            this.labelFilter2.Size = new System.Drawing.Size(38, 13);
            this.labelFilter2.TabIndex = 83;
            this.labelFilter2.Text = "Сезон";
            // 
            // labelFilter1
            // 
            this.labelFilter1.AutoSize = true;
            this.labelFilter1.Location = new System.Drawing.Point(314, 15);
            this.labelFilter1.Name = "labelFilter1";
            this.labelFilter1.Size = new System.Drawing.Size(25, 13);
            this.labelFilter1.TabIndex = 80;
            this.labelFilter1.Text = "Год";
            // 
            // tabPagePayService
            // 
            this.tabPagePayService.Controls.Add(this.groupFilterActChargeoff);
            this.tabPagePayService.Location = new System.Drawing.Point(4, 22);
            this.tabPagePayService.Name = "tabPagePayService";
            this.tabPagePayService.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePayService.Size = new System.Drawing.Size(495, 73);
            this.tabPagePayService.TabIndex = 2;
            this.tabPagePayService.Tag = "5";
            this.tabPagePayService.Text = "Платные услуги";
            this.tabPagePayService.UseVisualStyleBackColor = true;
            // 
            // groupFilterActChargeoff
            // 
            this.groupFilterActChargeoff.Location = new System.Drawing.Point(6, 5);
            this.groupFilterActChargeoff.Name = "groupFilterActChargeoff";
            this.groupFilterActChargeoff.Size = new System.Drawing.Size(486, 65);
            this.groupFilterActChargeoff.TabIndex = 87;
            this.groupFilterActChargeoff.TabStop = false;
            this.groupFilterActChargeoff.Text = "Фильтр по актам на списание";
            // 
            // tabPageChek
            // 
            this.tabPageChek.Controls.Add(this.groupFilterChek);
            this.tabPageChek.Location = new System.Drawing.Point(4, 22);
            this.tabPageChek.Name = "tabPageChek";
            this.tabPageChek.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageChek.Size = new System.Drawing.Size(495, 73);
            this.tabPageChek.TabIndex = 3;
            this.tabPageChek.Tag = "6";
            this.tabPageChek.Text = "Кассовые чеки";
            this.tabPageChek.UseVisualStyleBackColor = true;
            // 
            // groupFilterChek
            // 
            this.groupFilterChek.Location = new System.Drawing.Point(6, 5);
            this.groupFilterChek.Name = "groupFilterChek";
            this.groupFilterChek.Size = new System.Drawing.Size(483, 65);
            this.groupFilterChek.TabIndex = 88;
            this.groupFilterChek.TabStop = false;
            this.groupFilterChek.Text = "Фильтр";
            // 
            // tabPageDemand
            // 
            this.tabPageDemand.Controls.Add(this.groupFilterDemand);
            this.tabPageDemand.Location = new System.Drawing.Point(4, 22);
            this.tabPageDemand.Name = "tabPageDemand";
            this.tabPageDemand.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDemand.Size = new System.Drawing.Size(495, 73);
            this.tabPageDemand.TabIndex = 1;
            this.tabPageDemand.Tag = "3";
            this.tabPageDemand.Text = "Требования на склад";
            this.tabPageDemand.UseVisualStyleBackColor = true;
            // 
            // groupFilterDemand
            // 
            this.groupFilterDemand.Controls.Add(this.cmbSeasonDemand);
            this.groupFilterDemand.Controls.Add(this.intYearDemand);
            this.groupFilterDemand.Controls.Add(this.label1);
            this.groupFilterDemand.Controls.Add(this.label4);
            this.groupFilterDemand.Controls.Add(this.cmbMember);
            this.groupFilterDemand.Controls.Add(this.label3);
            this.groupFilterDemand.Location = new System.Drawing.Point(6, 3);
            this.groupFilterDemand.Name = "groupFilterDemand";
            this.groupFilterDemand.Size = new System.Drawing.Size(483, 65);
            this.groupFilterDemand.TabIndex = 86;
            this.groupFilterDemand.TabStop = false;
            this.groupFilterDemand.Text = "Фильтр по требованиям";
            // 
            // cmbSeasonDemand
            // 
            this.cmbSeasonDemand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSeasonDemand.DisplayMember = "SeasonNbr";
            this.cmbSeasonDemand.FormattingEnabled = true;
            this.cmbSeasonDemand.Location = new System.Drawing.Point(390, 38);
            this.cmbSeasonDemand.Name = "cmbSeasonDemand";
            this.cmbSeasonDemand.Size = new System.Drawing.Size(87, 21);
            this.cmbSeasonDemand.TabIndex = 92;
            this.cmbSeasonDemand.ValueMember = "SeasonID";
            // 
            // intYearDemand
            // 
            this.intYearDemand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.intYearDemand.Location = new System.Drawing.Point(390, 14);
            this.intYearDemand.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.intYearDemand.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.intYearDemand.Name = "intYearDemand";
            this.intYearDemand.Size = new System.Drawing.Size(87, 20);
            this.intYearDemand.TabIndex = 91;
            this.intYearDemand.Value = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.intYearDemand.ValueChanged += new System.EventHandler(this.intYearDemand_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(312, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "Сезон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(314, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 89;
            this.label4.Text = "Год";
            // 
            // cmbMember
            // 
            this.cmbMember.DisplayMember = "Fullname";
            this.cmbMember.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMember.FormattingEnabled = true;
            this.cmbMember.Location = new System.Drawing.Point(126, 14);
            this.cmbMember.Name = "cmbMember";
            this.cmbMember.Size = new System.Drawing.Size(168, 21);
            this.cmbMember.TabIndex = 86;
            this.cmbMember.ValueMember = "PersonID_member";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 85;
            this.label3.Text = "Сотрудник";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Расходные документы";
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilter.Location = new System.Drawing.Point(8, 121);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(153, 23);
            this.btnFilter.TabIndex = 78;
            this.btnFilter.Text = "Применить фильтр";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.rbShowAll);
            this.groupBox1.Controls.Add(this.rbShowReserved);
            this.groupBox1.Controls.Add(this.rbShowNonReserved);
            this.groupBox1.Location = new System.Drawing.Point(14, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(443, 77);
            this.groupBox1.TabIndex = 89;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр по лекарственным средствам";
            // 
            // rbShowAll
            // 
            this.rbShowAll.AutoSize = true;
            this.rbShowAll.Checked = true;
            this.rbShowAll.Location = new System.Drawing.Point(19, 15);
            this.rbShowAll.Name = "rbShowAll";
            this.rbShowAll.Size = new System.Drawing.Size(95, 17);
            this.rbShowAll.TabIndex = 91;
            this.rbShowAll.TabStop = true;
            this.rbShowAll.Text = "Показать все";
            this.rbShowAll.UseVisualStyleBackColor = true;
            this.rbShowAll.CheckedChanged += new System.EventHandler(this.rbShowAll_CheckedChanged);
            // 
            // rbShowReserved
            // 
            this.rbShowReserved.AutoSize = true;
            this.rbShowReserved.Location = new System.Drawing.Point(19, 35);
            this.rbShowReserved.Name = "rbShowReserved";
            this.rbShowReserved.Size = new System.Drawing.Size(141, 17);
            this.rbShowReserved.TabIndex = 89;
            this.rbShowReserved.Text = "Показать невыданные";
            this.rbShowReserved.UseVisualStyleBackColor = true;
            this.rbShowReserved.CheckedChanged += new System.EventHandler(this.rbShowReserved_CheckedChanged);
            // 
            // rbShowNonReserved
            // 
            this.rbShowNonReserved.AutoSize = true;
            this.rbShowNonReserved.Location = new System.Drawing.Point(19, 54);
            this.rbShowNonReserved.Name = "rbShowNonReserved";
            this.rbShowNonReserved.Size = new System.Drawing.Size(129, 17);
            this.rbShowNonReserved.TabIndex = 90;
            this.rbShowNonReserved.Text = "Показать выданные";
            this.rbShowNonReserved.UseVisualStyleBackColor = true;
            this.rbShowNonReserved.CheckedChanged += new System.EventHandler(this.rbShowNonReserved_CheckedChanged);
            // 
            // personPatientCategoryTableAdapter
            // 
            this.personPatientCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // colDocID
            // 
            this.colDocID.FillWeight = 30F;
            this.colDocID.HeaderText = "ID";
            this.colDocID.Name = "colDocID";
            this.colDocID.ReadOnly = true;
            this.colDocID.Visible = false;
            this.colDocID.Width = 30;
            // 
            // colNum
            // 
            this.colNum.DataPropertyName = "Season";
            this.colNum.FillWeight = 50F;
            this.colNum.HeaderText = "№";
            this.colNum.MinimumWidth = 20;
            this.colNum.Name = "colNum";
            this.colNum.ReadOnly = true;
            this.colNum.Width = 50;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "FullName";
            this.colDesc.FillWeight = 200F;
            this.colDesc.HeaderText = "ФИО";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Width = 200;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Date";
            this.colDate.FillWeight = 60F;
            this.colDate.HeaderText = "Дата";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            this.colDate.Width = 60;
            // 
            // colSum
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.colSum.DefaultCellStyle = dataGridViewCellStyle2;
            this.colSum.HeaderText = "Сумма";
            this.colSum.Name = "colSum";
            this.colSum.ReadOnly = true;
            this.colSum.Width = 65;
            // 
            // colDoctor
            // 
            this.colDoctor.DataPropertyName = "DoctorName";
            this.colDoctor.HeaderText = "Терапевт";
            this.colDoctor.Name = "colDoctor";
            this.colDoctor.ReadOnly = true;
            // 
            // frmMedicamentOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 629);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmMedicamentOutput";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выдача лекрственных средств по расходным документам ";
            this.Load += new System.EventHandler(this.frmScladOutput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridOutDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabDocOutType.ResumeLayout(false);
            this.tabPageIH.ResumeLayout(false);
            this.groupFilterIH.ResumeLayout(false);
            this.groupFilterIH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            this.tabPagePayService.ResumeLayout(false);
            this.tabPageChek.ResumeLayout(false);
            this.tabPageDemand.ResumeLayout(false);
            this.groupFilterDemand.ResumeLayout(false);
            this.groupFilterDemand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intYearDemand)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label labelFilter2;
        private System.Windows.Forms.GroupBox groupFilterIH;
        private System.Windows.Forms.DataGridView gridOutDocuments;
        private System.Windows.Forms.ComboBox cmbCategoryPacient;
        private System.Windows.Forms.Label labelFilter3;
        private System.Windows.Forms.Label labelFilter1;
        private System.Windows.Forms.TabControl tabDocOutType;
        private System.Windows.Forms.TabPage tabPageIH;
        private System.Windows.Forms.TabPage tabPageDemand;
        private System.Windows.Forms.TabPage tabPagePayService;
        private System.Windows.Forms.TabPage tabPageChek;
        private System.Windows.Forms.GroupBox groupFilterDemand;
        private System.Windows.Forms.ComboBox cmbMember;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.GroupBox groupFilterActChargeoff;
        private System.Windows.Forms.GroupBox groupFilterChek;
        private System.Windows.Forms.ComboBox cmbSeasonDemand;
        private System.Windows.Forms.NumericUpDown intYearDemand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbShowNonReserved;
        private System.Windows.Forms.RadioButton rbShowReserved;
        private System.Windows.Forms.RadioButton rbShowAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colUnReserved;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedicamentN;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCountOutput;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrice;
        private Medsyst.Reports.CachedStudentByFacult cachedStudentByFacult1;

        private MedsystDataSet medsystDataSet;
        private System.Windows.Forms.BindingSource personPatientCategoryBindingSource;
        private Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter personPatientCategoryTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDocID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDoctor;
    }
}