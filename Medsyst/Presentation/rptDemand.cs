﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;
using CrystalDecisions.CrystalReports.Engine; // Добавил ЗЕФ 24.11.2011 согласно http://msdn.microsoft.com/ru-ru/library/ms227507(v=VS.90).aspx
using CrystalDecisions.Shared;
using Medsyst.DataSet;
using Medsyst.DataSet.datDemandTableAdapters; // Добавил ЗЕФ 04.11.2011


namespace Medsyst
{
    public partial class rptDemand : Form
    {
        public rptDemand()
        {
            InitializeComponent();
        }

        private void rptDemand_Load(object sender, EventArgs e)
        {
            cmd = (Command)Tag;//В интерфейсе передается идентификатор требования
            demandMaterial.Load(); //Загружается отчет
            datDemand DS = new datDemand(); //Строго типизированный набор данных

            //Инициализация параметров хранимых процедур в адаптерах данных
            DemandTableAdapter DTAData = new DemandTableAdapter();//Адаптер требования на склад
            DTAData.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            DTAData.Fill(DS.Demand, cmd.Id);
            
            DemandMaterialTableAdapter DTAMedicament = new DemandMaterialTableAdapter();//Адаптер медикаментов по требованию
            DTAMedicament.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            DTAMedicament.Fill(DS.DemandMaterial, cmd.Id);

            demandMaterial.SetDataSource(DS); //Передача отчету экземпляра набора данных

            crystalReportViewer.ReportSource = demandMaterial; //Элементу формы просмотра отчетов передается загруженный отчет
            
        }



    }
}
