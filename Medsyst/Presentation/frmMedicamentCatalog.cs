﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmMedicamentCatalog : Form
    {
        MedicamentList medicaments = new MedicamentList();
        MedicamentCategoryList cats = new MedicamentCategoryList();
        Measures measures = new Measures();
        int CategoryID = 0;

        public frmMedicamentCatalog()
        {
            InitializeComponent();
        }

        private void Catalog_Load(object sender, EventArgs e)
        {
            measures.Load();
            colUnit.DisplayMember = "ShotName";
            colUnit.ValueMember = "MeasureID";
            colUnit.DataSource = measures;

            btnSelect.Visible = cmd.Mode == CommandModes.Select;
            cats.Load();
            cats.FillTree(treeCat);
            treeCat.ExpandAll();
            LoadMed();
        }
        //Загружет список медикаментов?
        void LoadMed()
        {
            if (chkPrice.Checked)//Если фильтр по стоимости отмечен 
            {
                LoadMed(decimal.Parse(textBox1.Text), decimal.Parse(textBox2.Text));//Перегружается функция с параметрами интервалов цен (см. ниже)
            }
            else
            {//Тут тоже нужны пояснения
                medicaments = new MedicamentList();
                medicaments.dbOrder = "MedicamentN";
                gridMed.DataSource = null;
                medicaments.Load(CategoryID);//загружаем данные из таблиц в объект
                gridMed.AutoGenerateColumns = false;
                gridMed.DataSource = medicaments;//устанавливается связь экземпляра класса с полем формы типа таблица
            }
        }

        void LoadMed(decimal price1,decimal price2)
        {
            medicaments = new MedicamentList();
            gridMed.DataSource = null;
            medicaments.Load(CategoryID,price1, price2);
            gridMed.AutoGenerateColumns = false;
            gridMed.DataSource = medicaments;
        }

        // Добавление медикамента
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;//Если не выделено ни одного элемента в дереве то выходит из функции
            frmMedicamentAdd frm = new frmMedicamentAdd();
            frm.Tag = new Command(CommandModes.New,int.Parse(treeCat.SelectedNode.Name));//Передача полю Tag объекта типа Command с параметрми режима и названия выбранной категории
            frm.ShowDialog(this);
            LoadMed();
        }

        // Удаление медикамента
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if ((gridMed.SelectedRows != null) && (gridMed.SelectedRows.Count > 0))
            {
                DialogResult res = MessageBox.Show("Вы действительно хотите удалить медикамент из каталога?", "Подтверждение удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No) return;
                Medicament m = new Medicament(true);
                m.MedicamentID = medicaments[gridMed.SelectedRows[0].Index].MedicamentID;
                m.Load();
                m.Deleted = true;
                m.Update();
                //srvout.DeleteDemandMedicament();
                LoadMed();
            }
        }

        // Изменение медикамента
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (gridMed.SelectedRows != null && gridMed.SelectedRows.Count > 0)
            {
                int id = medicaments[gridMed.SelectedRows[0].Index].MedicamentID;
                frmMedicamentAdd frm = new frmMedicamentAdd();
                frm.Tag = new Command(CommandModes.Edit, id);
                frm.ShowDialog(this);
                LoadMed();
            }
        }

        private void gridMed_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridMed.SelectedRows != null && gridMed.SelectedRows.Count > 0)
            {
                if (cmd.Mode == CommandModes.Select)
                {
                    Tag = new Command(CommandModes.Return, medicaments[gridMed.SelectedRows[0].Index].MedicamentID);
                    Close();
                }
                else
                {
                    int id = medicaments[gridMed.SelectedRows[0].Index].MedicamentID;
                    frmMedicamentAdd frm = new frmMedicamentAdd();
                    frm.Tag = new Command(CommandModes.Edit, id);
                    frm.ShowDialog(this);
                    LoadMed();
                }
            }
        }

        // Кнопка "Выбрать" служит для выбора медикаментов из каталога
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (gridMed.SelectedRows != null && gridMed.SelectedRows.Count > 0)
            {
                Tag = new Command(CommandModes.Return, medicaments[gridMed.SelectedRows[0].Index].MedicamentID);
                Close();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            LoadMed();
        }

        // Кнопка Отменить фильтр
        private void btnCancel_Click(object sender, EventArgs e)
        {
            chkPrice.Checked = false;
            LoadMed();
        }

        private void treeCat_Click(object sender, EventArgs e)
        {
        }

        // Действия после выделения катогории медикаментов
        private void treeCat_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeCat.SelectedNode == null) return;
            CategoryID = int.Parse(treeCat.SelectedNode.Name);
            LoadMed();
        }
    }
}
