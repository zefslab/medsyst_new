﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using System.IO;

namespace Medsyst
{
    public partial class frmMedicamentAdd : Form
    {
        Medicament medicament = new Medicament(true);
        Measures measures = new Measures();
        Class.MedicamentCategory category = new Class.MedicamentCategory();

        public frmMedicamentAdd()
        {
            InitializeComponent();
        }

        private void frmMedicamentAdd_Load(object sender, EventArgs e)
        {
            measures.Load();
            comboBox1.DataSource = measures;
            medicament.FormBinding("medicament", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                medicament.MedicamentID = cmd.Id;
                medicament.Load();
                medicament.WriteToForm();
                category.MedicamentCategoryID = medicament.CategoryID;
                category.Load();
                txtCategory.Text = category.CategoryN;
                if (medicament.FileImage!="" && File.Exists(medicament.FileImage))
                    picImage.Image = Image.FromFile(medicament.FileImage);
            }
            else if (cmd.Mode == CommandModes.New)
            {
                textBox1.Text = "Новое лекарство";
                category.MedicamentCategoryID = cmd.Id;
                category.Load();
                txtCategory.Text = category.CategoryN;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (!(Tag is Command)) return;
            if (cmd.Mode == CommandModes.Edit)
            {   
                medicament.ReadFromForm();
                medicament.MeasureID = (int)comboBox1.SelectedValue;
                medicament.Update();
            }
            else if (cmd.Mode == CommandModes.New)
            {    
                medicament.ReadFromForm();
                medicament.MeasureID = (int)comboBox1.SelectedValue;
                medicament.CategoryID = cmd.Id;
                if (!medicament.Insert())
                {
                    MessageBox.Show("Не удалось добавить лекарственное средство.");
                }
                else
                    Tag = new Command(CommandModes.Return, medicament.MedicamentID);
            }
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Кнопка "Добавить картинку" 
        private void btnAddImage_Click(object sender, EventArgs e)
        {
            ofdImage.Title = "Выберете файл изображения медикамента";
            ofdImage.Multiselect = false;
            ofdImage.Filter = "Файлы изображений(*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png|Все файлы (*.*)|*.*";
            if (ofdImage.ShowDialog(this) == DialogResult.OK)
            {
                medicament.FileImage = ofdImage.FileName;
                picImage.Image = Image.FromFile(medicament.FileImage);// загрузка картинки из файла
            }
        }


    }
}
