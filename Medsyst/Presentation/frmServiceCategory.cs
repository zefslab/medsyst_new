﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;

namespace Medsyst
{
    public partial class frmServiceCategory : ftmpCategory
    {
        public frmServiceCategory()
        {
            InitializeComponent();
        }

        private void frmServiceCategory_Load(object sender, EventArgs e)
        {
            Categories = new ServiceCategoryList(); // Присваиваем объект дерева категорий
            Category = new ServiceCategory(); // Присваиваем объект категории
            LoadCategory(); // Загружаем базовую фунцию загрузки категорий
        }

    }
}
