﻿namespace Medsyst
{
    partial class rptMedicamentChargesByGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMedicamentChargesByGroup));
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabReportByPeriod = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chlbCategory = new System.Windows.Forms.CheckedListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnExecutReportByPeriod = new System.Windows.Forms.Button();
            this.rbtnNoBudget = new System.Windows.Forms.RadioButton();
            this.rbtnBudget = new System.Windows.Forms.RadioButton();
            this.dtpEndPeriod = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginPeriod = new System.Windows.Forms.DateTimePicker();
            this.crystalReportViewerByPeriod = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabReportBySeason = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbSeasonEnd = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSeasonBegin = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExecutReportBySeason = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rbtnBudget2 = new System.Windows.Forms.RadioButton();
            this.crystalReportViewerBySeason = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabGoups = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.treeCategoryInGroup = new System.Windows.Forms.TreeView();
            this.btnRemoveFromGroup = new System.Windows.Forms.Button();
            this.btnAddToGroup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnChange = new ExButton.NET.ExButton();
            this.btnAdd = new ExButton.NET.ExButton();
            this.btnDelete = new ExButton.NET.ExButton();
            this.treeCategory = new System.Windows.Forms.TreeView();
            this.lbGrop = new System.Windows.Forms.ListBox();
            this.medicamentChargesByGroup1 = new Medsyst.Reports.MedicamentChargesByGroup();
            this.medicamentChargesByGroup_21 = new Medsyst.Reports.MedicamentChargesByGroup_2();
            this.chlbCategoryBySeason = new System.Windows.Forms.CheckedListBox();
            this.tabs.SuspendLayout();
            this.tabReportByPeriod.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabReportBySeason.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabGoups.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabReportByPeriod);
            this.tabs.Controls.Add(this.tabReportBySeason);
            this.tabs.Controls.Add(this.tabGoups);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(914, 597);
            this.tabs.TabIndex = 0;
            // 
            // tabReportByPeriod
            // 
            this.tabReportByPeriod.Controls.Add(this.splitContainer1);
            this.tabReportByPeriod.Location = new System.Drawing.Point(4, 22);
            this.tabReportByPeriod.Name = "tabReportByPeriod";
            this.tabReportByPeriod.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportByPeriod.Size = new System.Drawing.Size(906, 571);
            this.tabReportByPeriod.TabIndex = 0;
            this.tabReportByPeriod.Text = "Отчет за период";
            this.tabReportByPeriod.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chlbCategory);
            this.splitContainer1.Panel1.Controls.Add(this.label10);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.btnExecutReportByPeriod);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnNoBudget);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnBudget);
            this.splitContainer1.Panel1.Controls.Add(this.dtpEndPeriod);
            this.splitContainer1.Panel1.Controls.Add(this.dtpBeginPeriod);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.crystalReportViewerByPeriod);
            this.splitContainer1.Size = new System.Drawing.Size(900, 565);
            this.splitContainer1.SplitterDistance = 166;
            this.splitContainer1.TabIndex = 1;
            // 
            // chlbCategory
            // 
            this.chlbCategory.CheckOnClick = true;
            this.chlbCategory.FormattingEnabled = true;
            this.chlbCategory.Location = new System.Drawing.Point(8, 72);
            this.chlbCategory.Name = "chlbCategory";
            this.chlbCategory.Size = new System.Drawing.Size(154, 94);
            this.chlbCategory.TabIndex = 77;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 76;
            this.label10.Text = "Категории пациентов";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "по";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "с";
            // 
            // btnExecutReportByPeriod
            // 
            this.btnExecutReportByPeriod.Location = new System.Drawing.Point(7, 226);
            this.btnExecutReportByPeriod.Name = "btnExecutReportByPeriod";
            this.btnExecutReportByPeriod.Size = new System.Drawing.Size(155, 23);
            this.btnExecutReportByPeriod.TabIndex = 9;
            this.btnExecutReportByPeriod.Text = "Гененрировать отчет";
            this.btnExecutReportByPeriod.UseVisualStyleBackColor = true;
            this.btnExecutReportByPeriod.Click += new System.EventHandler(this.btnExecutReportByPeriod_Click);
            // 
            // rbtnNoBudget
            // 
            this.rbtnNoBudget.AutoSize = true;
            this.rbtnNoBudget.Location = new System.Drawing.Point(7, 203);
            this.rbtnNoBudget.Name = "rbtnNoBudget";
            this.rbtnNoBudget.Size = new System.Drawing.Size(83, 17);
            this.rbtnNoBudget.TabIndex = 8;
            this.rbtnNoBudget.Text = "Внебюджет";
            this.rbtnNoBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnBudget
            // 
            this.rbtnBudget.AutoSize = true;
            this.rbtnBudget.Checked = true;
            this.rbtnBudget.Location = new System.Drawing.Point(7, 180);
            this.rbtnBudget.Name = "rbtnBudget";
            this.rbtnBudget.Size = new System.Drawing.Size(65, 17);
            this.rbtnBudget.TabIndex = 7;
            this.rbtnBudget.TabStop = true;
            this.rbtnBudget.Text = "Бюджет";
            this.rbtnBudget.UseVisualStyleBackColor = true;
            // 
            // dtpEndPeriod
            // 
            this.dtpEndPeriod.Location = new System.Drawing.Point(30, 32);
            this.dtpEndPeriod.Name = "dtpEndPeriod";
            this.dtpEndPeriod.Size = new System.Drawing.Size(132, 20);
            this.dtpEndPeriod.TabIndex = 6;
            this.dtpEndPeriod.Value = new System.DateTime(2011, 2, 28, 23, 59, 0, 0);
            // 
            // dtpBeginPeriod
            // 
            this.dtpBeginPeriod.Location = new System.Drawing.Point(30, 6);
            this.dtpBeginPeriod.Name = "dtpBeginPeriod";
            this.dtpBeginPeriod.Size = new System.Drawing.Size(132, 20);
            this.dtpBeginPeriod.TabIndex = 5;
            this.dtpBeginPeriod.Value = new System.DateTime(2011, 1, 1, 17, 57, 0, 0);
            // 
            // crystalReportViewerByPeriod
            // 
            this.crystalReportViewerByPeriod.ActiveViewIndex = -1;
            this.crystalReportViewerByPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewerByPeriod.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewerByPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewerByPeriod.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewerByPeriod.Name = "crystalReportViewerByPeriod";
            this.crystalReportViewerByPeriod.Size = new System.Drawing.Size(730, 565);
            this.crystalReportViewerByPeriod.TabIndex = 0;
            this.crystalReportViewerByPeriod.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // tabReportBySeason
            // 
            this.tabReportBySeason.Controls.Add(this.splitContainer2);
            this.tabReportBySeason.Location = new System.Drawing.Point(4, 22);
            this.tabReportBySeason.Name = "tabReportBySeason";
            this.tabReportBySeason.Size = new System.Drawing.Size(906, 571);
            this.tabReportBySeason.TabIndex = 2;
            this.tabReportBySeason.Text = "Отчет по сезонам";
            this.tabReportBySeason.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chlbCategoryBySeason);
            this.splitContainer2.Panel1.Controls.Add(this.label9);
            this.splitContainer2.Panel1.Controls.Add(this.label6);
            this.splitContainer2.Panel1.Controls.Add(this.cmbSeasonEnd);
            this.splitContainer2.Panel1.Controls.Add(this.label5);
            this.splitContainer2.Panel1.Controls.Add(this.cmbSeasonBegin);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.btnExecutReportBySeason);
            this.splitContainer2.Panel1.Controls.Add(this.radioButton1);
            this.splitContainer2.Panel1.Controls.Add(this.rbtnBudget2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.crystalReportViewerBySeason);
            this.splitContainer2.Size = new System.Drawing.Size(906, 571);
            this.splitContainer2.SplitterDistance = 165;
            this.splitContainer2.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 75;
            this.label9.Text = "Категории пациентов";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "с";
            // 
            // cmbSeasonEnd
            // 
            this.cmbSeasonEnd.DisplayMember = "FullNbr";
            this.cmbSeasonEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasonEnd.FormattingEnabled = true;
            this.cmbSeasonEnd.Location = new System.Drawing.Point(23, 58);
            this.cmbSeasonEnd.Name = "cmbSeasonEnd";
            this.cmbSeasonEnd.Size = new System.Drawing.Size(71, 21);
            this.cmbSeasonEnd.TabIndex = 72;
            this.cmbSeasonEnd.ValueMember = "SeasonID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 71;
            this.label5.Text = "по";
            // 
            // cmbSeasonBegin
            // 
            this.cmbSeasonBegin.DisplayMember = "FullNbr";
            this.cmbSeasonBegin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasonBegin.FormattingEnabled = true;
            this.cmbSeasonBegin.Location = new System.Drawing.Point(23, 26);
            this.cmbSeasonBegin.Name = "cmbSeasonBegin";
            this.cmbSeasonBegin.Size = new System.Drawing.Size(73, 21);
            this.cmbSeasonBegin.TabIndex = 70;
            this.cmbSeasonBegin.ValueMember = "SeasonID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 69;
            this.label3.Text = "Сезоны";
            // 
            // btnExecutReportBySeason
            // 
            this.btnExecutReportBySeason.Location = new System.Drawing.Point(6, 256);
            this.btnExecutReportBySeason.Name = "btnExecutReportBySeason";
            this.btnExecutReportBySeason.Size = new System.Drawing.Size(155, 23);
            this.btnExecutReportBySeason.TabIndex = 9;
            this.btnExecutReportBySeason.Text = "Гененрировать отчет";
            this.btnExecutReportBySeason.UseVisualStyleBackColor = true;
            this.btnExecutReportBySeason.Click += new System.EventHandler(this.btnExecutReportBySeason_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(10, 233);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(83, 17);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.Text = "Внебюджет";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // rbtnBudget2
            // 
            this.rbtnBudget2.AutoSize = true;
            this.rbtnBudget2.Checked = true;
            this.rbtnBudget2.Location = new System.Drawing.Point(10, 210);
            this.rbtnBudget2.Name = "rbtnBudget2";
            this.rbtnBudget2.Size = new System.Drawing.Size(65, 17);
            this.rbtnBudget2.TabIndex = 7;
            this.rbtnBudget2.TabStop = true;
            this.rbtnBudget2.Text = "Бюджет";
            this.rbtnBudget2.UseVisualStyleBackColor = true;
            // 
            // crystalReportViewerBySeason
            // 
            this.crystalReportViewerBySeason.ActiveViewIndex = -1;
            this.crystalReportViewerBySeason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewerBySeason.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewerBySeason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewerBySeason.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewerBySeason.Name = "crystalReportViewerBySeason";
            this.crystalReportViewerBySeason.Size = new System.Drawing.Size(737, 571);
            this.crystalReportViewerBySeason.TabIndex = 0;
            this.crystalReportViewerBySeason.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // tabGoups
            // 
            this.tabGoups.Controls.Add(this.label2);
            this.tabGoups.Controls.Add(this.treeCategoryInGroup);
            this.tabGoups.Controls.Add(this.btnRemoveFromGroup);
            this.tabGoups.Controls.Add(this.btnAddToGroup);
            this.tabGoups.Controls.Add(this.label1);
            this.tabGoups.Controls.Add(this.label4);
            this.tabGoups.Controls.Add(this.btnChange);
            this.tabGoups.Controls.Add(this.btnAdd);
            this.tabGoups.Controls.Add(this.btnDelete);
            this.tabGoups.Controls.Add(this.treeCategory);
            this.tabGoups.Controls.Add(this.lbGrop);
            this.tabGoups.Location = new System.Drawing.Point(4, 22);
            this.tabGoups.Name = "tabGoups";
            this.tabGoups.Padding = new System.Windows.Forms.Padding(3);
            this.tabGoups.Size = new System.Drawing.Size(906, 571);
            this.tabGoups.TabIndex = 1;
            this.tabGoups.Text = "Настройка групп";
            this.tabGoups.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(411, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Категории медикаментов в группе ";
            // 
            // treeCategoryInGroup
            // 
            this.treeCategoryInGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeCategoryInGroup.CheckBoxes = true;
            this.treeCategoryInGroup.HideSelection = false;
            this.treeCategoryInGroup.Location = new System.Drawing.Point(414, 242);
            this.treeCategoryInGroup.Name = "treeCategoryInGroup";
            this.treeCategoryInGroup.Size = new System.Drawing.Size(389, 321);
            this.treeCategoryInGroup.TabIndex = 3;
            // 
            // btnRemoveFromGroup
            // 
            this.btnRemoveFromGroup.Location = new System.Drawing.Point(316, 400);
            this.btnRemoveFromGroup.Name = "btnRemoveFromGroup";
            this.btnRemoveFromGroup.Size = new System.Drawing.Size(92, 23);
            this.btnRemoveFromGroup.TabIndex = 63;
            this.btnRemoveFromGroup.Text = "<<     Убрать";
            this.btnRemoveFromGroup.UseVisualStyleBackColor = true;
            this.btnRemoveFromGroup.Click += new System.EventHandler(this.btnRemoveFromGroup_Click);
            // 
            // btnAddToGroup
            // 
            this.btnAddToGroup.Location = new System.Drawing.Point(315, 370);
            this.btnAddToGroup.Name = "btnAddToGroup";
            this.btnAddToGroup.Size = new System.Drawing.Size(92, 23);
            this.btnAddToGroup.TabIndex = 62;
            this.btnAddToGroup.Text = "Добавить   >>";
            this.btnAddToGroup.UseVisualStyleBackColor = true;
            this.btnAddToGroup.Click += new System.EventHandler(this.btnAddToGroup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(411, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Группы категорий в отчете";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Категории медикаментов";
            // 
            // btnChange
            // 
            this.btnChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChange.Image = ((System.Drawing.Image)(resources.GetObject("btnChange.Image")));
            this.btnChange.Location = new System.Drawing.Point(809, 50);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(89, 23);
            this.btnChange.TabIndex = 55;
            this.btnChange.Text = "Изменить";
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(809, 21);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(89, 23);
            this.btnAdd.TabIndex = 57;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(809, 80);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 23);
            this.btnDelete.TabIndex = 56;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // treeCategory
            // 
            this.treeCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeCategory.CheckBoxes = true;
            this.treeCategory.HideSelection = false;
            this.treeCategory.Location = new System.Drawing.Point(8, 21);
            this.treeCategory.Name = "treeCategory";
            this.treeCategory.Size = new System.Drawing.Size(301, 544);
            this.treeCategory.TabIndex = 1;
            this.treeCategory.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeCategory_AfterCheck);
            // 
            // lbGrop
            // 
            this.lbGrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGrop.DisplayMember = "GroupN";
            this.lbGrop.FormattingEnabled = true;
            this.lbGrop.Location = new System.Drawing.Point(414, 21);
            this.lbGrop.Name = "lbGrop";
            this.lbGrop.Size = new System.Drawing.Size(389, 186);
            this.lbGrop.TabIndex = 2;
            this.lbGrop.ValueMember = "GroupID";
            this.lbGrop.SelectedIndexChanged += new System.EventHandler(this.lbGrop_SelectedIndexChanged);
            // 
            // chlbCategoryBySeason
            // 
            this.chlbCategoryBySeason.CheckOnClick = true;
            this.chlbCategoryBySeason.FormattingEnabled = true;
            this.chlbCategoryBySeason.Location = new System.Drawing.Point(6, 104);
            this.chlbCategoryBySeason.Name = "chlbCategoryBySeason";
            this.chlbCategoryBySeason.Size = new System.Drawing.Size(154, 94);
            this.chlbCategoryBySeason.TabIndex = 78;
            // 
            // rptMedicamentChargesByGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 597);
            this.Controls.Add(this.tabs);
            this.Name = "rptMedicamentChargesByGroup";
            this.Text = "Расход медикаментов за период по группам";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.rptMedicamentChargesByGroup_Load);
            this.tabs.ResumeLayout(false);
            this.tabReportByPeriod.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabReportBySeason.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.tabGoups.ResumeLayout(false);
            this.tabGoups.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabReportByPeriod;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabPage tabGoups;
        private System.Windows.Forms.Button btnExecutReportByPeriod;
        private System.Windows.Forms.RadioButton rbtnNoBudget;
        private System.Windows.Forms.RadioButton rbtnBudget;
        private System.Windows.Forms.DateTimePicker dtpBeginPeriod;
        private System.Windows.Forms.ListBox lbGrop;
        private System.Windows.Forms.TreeView treeCategory;
        private ExButton.NET.ExButton btnChange;
        private ExButton.NET.ExButton btnAdd;
        private ExButton.NET.ExButton btnDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRemoveFromGroup;
        private System.Windows.Forms.Button btnAddToGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TreeView treeCategoryInGroup;
        private Medsyst.Reports.MedicamentChargesByGroup medicamentChargesByGroup1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewerByPeriod;
        private System.Windows.Forms.TabPage tabReportBySeason;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button btnExecutReportBySeason;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rbtnBudget2;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewerBySeason;
        private System.Windows.Forms.ComboBox cmbSeasonEnd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbSeasonBegin;
        private System.Windows.Forms.Label label3;
        private Medsyst.Reports.MedicamentChargesByGroup_2 medicamentChargesByGroup_21;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpEndPeriod;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox chlbCategory;
        private System.Windows.Forms.CheckedListBox chlbCategoryBySeason;
    }
}