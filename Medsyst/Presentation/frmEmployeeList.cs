﻿using System;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using System.Runtime.InteropServices;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmEmployeeList : Form
    {
        ///<summary>Список сотрудников профилактория</summary>
        MemberList _employees = new MemberList();
        ///<summary>Список должностей</summary>
        PositionList _positions = new PositionList();

        public frmEmployeeList()
        {
            InitializeComponent();
        }
        
        ///<summary>Загрузка формы</summary>
        private void frmEmployees_Load(object sender, EventArgs e)
        {
            _positions.Load();

            if (cmd.Mode == CommandModes.Select)
                tbSelect.Visible = true;
            else
                tbSelect.Visible = false;

            LoadEmployees();
        }
        
        ///<summary>Загрузка списка работников медучреждения</summary>
        public void LoadEmployees()
        {
            gridEmployee.AutoGenerateColumns = false;
            gridEmployee.DataSource = null;
            _employees.Load(CommandDirective.Non);
            var positions = _positions.Select(x => new
                                                {
                                                    Id = x.PositionID,
                                                    Name = x.PositionN
                                                }).ToList();
            gridEmployee.DataSource = _employees.Join(positions, x=> x.PositionID, x=> x.Id, (m,p) => new
                                            {
                                                m.Fullname,
                                                Position = p.Name,
                                                m.PhoneWork,
                                                m.PhoneMobile,
                                                m.EMail
                                            }).ToList();
        }

        ///<summary>Кнопка "Добавить"</summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmEmployee frm = new frmEmployee();
            frm.Tag = new Command(CommandModes.New);
            frm.ShowDialog();
            LoadEmployees();
        }

        ///<summary>Кнопка "Изменить"</summary>
        private void btnChange_Click(object sender, EventArgs e)
        {
            if (gridEmployee.SelectedRows != null & gridEmployee.SelectedRows.Count > 0)
            {
                int idx = gridEmployee.SelectedRows[0].Index;
                frmEmployee frm = new frmEmployee();
                frm.Tag = new Command(CommandModes.Edit, _employees[idx].PersonID);
                frm.ShowDialog();
                LoadEmployees();
                if (idx < _employees.Count && _employees.Count > 0)
                {
                    gridEmployee.Rows[0].Selected = false;
                    gridEmployee.Rows[idx].Selected = true;
                }

            }
        }



        ///<summary>Кнопка "Выбрать"</summary>
        private void tbSelect_Click(object sender, EventArgs e)
        {
            if (gridEmployee.SelectedRows != null & gridEmployee.SelectedRows.Count > 0)
            {
                Tag = new Command(CommandModes.Return, _employees[gridEmployee.SelectedRows[0].Index].PersonID);
                Close();
            }
        }

        ///<summary>Двойной клик по таблице</summary>
        private void gridEmployee_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (cmd.Mode == CommandModes.Select)
            {
                tbSelect_Click(sender, null);
            }
            else
            {
                btnChange_Click(sender, null);
            }
        }

        ///<summary>Кнопка "Удалить"</summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridEmployee.SelectedRows != null & gridEmployee.SelectedRows.Count > 0)
            {
                int i = gridEmployee.SelectedRows[0].Index;
                if (MessageBox.Show("Вы действительно хотите удалить сотрудника " + _employees[i].Fullname + "?", "Подтверждение удаления", MessageBoxButtons.YesNo,
                     MessageBoxIcon.Question) != DialogResult.Yes) return;
                //23.03.10
                //Чирков Е.О.
                //Физического удаления не производится, а делается запись в таблице об удалении
                _employees[i].Deleted = true;
                _employees[i].Update();
                LoadEmployees();
            }
        }

        //23.03.10
        //Чирков Е.О.
        //Функция для запуска внешней программы
        [DllImport("shell32.dll", EntryPoint = "ShellExecuteW", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ShellExecute(IntPtr Wnd, string Op, string FName, string Params, string Dir, int CmdShow);

        //23.03.10
        //Чирков Е.О.
        //Отправка письма (с помощью почтового клиента по умолчанию) на выбранный E-Mail
        private void gridEmployee_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = gridEmployee.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (cell is DataGridViewLinkCell) ShellExecute(Handle, "open", "mailto: " + cell.Value.ToString(), null, null, 0);
        }

        //24.03.10
        //Чирков Е.О.
        private void frmEmployees_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmMain.empForm = null;
        }
    }
}
