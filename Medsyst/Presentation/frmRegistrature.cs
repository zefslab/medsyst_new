﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Forms;
using System.IO;
using Medsyst.Class;
using System.Data.OleDb;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmRegistrature : Form
    {
        /// <summary>Пациенты</summary>
        PatientList patients = new PatientList();

        /// <summary>Амбулаторная карта пациента</summary>
        AmbulanceCard AmbCard = new AmbulanceCard(true);

        PatientCategoryList categoris = new PatientCategoryList();

        int PageCurrent = 0;
        bool IsActiveFind = false;

        public frmRegistrature()
        {
            InitializeComponent();
        }
        private void frmRegistrature_Load(object sender, EventArgs e)
        {
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                cmbYear1.Items.Add(i);
                cmbYear2.Items.Add(i);
            }
            
            //Заполнение списка категорий пациентов 
            chlbCategory.Items.AddRange(categoris.ToArray());
            //Отметка первой  категории пациентов в списке. Кривовато. Нужно сохранять в настройках последнее состояние работы с формой.
            chlbCategory.SetItemChecked(0, true);
 
            cmbYear1.SelectedIndex = cmbYear1.Items.Count-61;
            cmbYear2.SelectedIndex = 0;
            gridRegistrature.AutoGenerateColumns = false;
            AmbCard.FormBinding("AmbCard", this);
            Filter();
        }

        //Применить фильтр
        private void btnFilter_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            Filter();
        }

        //Добавление регистрационной карты пациента
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmAmbulanceCard frm = new frmAmbulanceCard();
            Command cmd_amb = new Command(CommandModes.New) { OnUpdate = Filter};
            
            if (chlbCategory.CheckedIndices.Count == 0)
                cmd_amb.Id = 0;//Указывается нулевой идентификатор, что означает "Не указано"
            else
                cmd_amb.Id = categoris[chlbCategory.CheckedIndices[0]].CategoryID;//берется первая категория из омеченых
            
            frm.Tag = cmd_amb;
            
            frm.MdiParent = MdiParent;
            frm.Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.Enabled = radioButton2.Enabled = chkSex.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            cmbYear1.Enabled = cmbYear2.Enabled = chkAdge.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker1.Enabled = dateTimePicker2.Enabled = chkBirthDay.Checked;
        }
        //Отметка чекбокса в дереве типов пациентов
        private void treePatientType_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // выделение дочерних чекбоксов в дереве
            e.Node.Tag = true; 
            foreach (TreeNode tn in e.Node.Nodes)
                tn.Checked = e.Node.Checked;
            e.Node.Tag = false;
            if (e.Node.Parent != null && e.Node.Parent.Tag != null && (bool)e.Node.Parent.Tag) return;
            Filter();
        }
 

        // кнопка Отменить фильтр
        private void btnFilterCancel_Click(object sender, EventArgs e)
        {
            chkSex.Checked = chkAdge.Checked = chkBirthDay.Checked = false;
            Filter();
        }
        /// <summary>Загрузка формы с амбулаторной карты
        /// </summary>
        private void LoadAmbulCard()
        {
            if (gridRegistrature.SelectedRows != null && gridRegistrature.SelectedRows.Count > 0)
            {
                frmAmbulanceCard frmAmbulCard = new frmAmbulanceCard();
                int AmbulCardID = (int)gridRegistrature.SelectedRows[0].Cells["colPatientID"].Value;
                frmAmbulCard.Tag = new Command(CommandModes.Edit, AmbulCardID) { OnUpdate = Filter };
                frmAmbulCard.MdiParent = MdiParent;
                frmAmbulCard.Show();
                //Filter();Закоментировал ЗЕФ 22.01.2012. Избыточное применение фильтра
                
                //Хорошо бы вернуть селектор на редактируемого пациента

                //if (gridRegistrature.SelectedRows != null && gridRegistrature.SelectedRows.Count > 0 && gridRegistrature.SelectedRows[0].Index >= 0)
                //    gridRegistrature_SelectionChanged(sender, new EventArgs());
            }
        }
        /// <summary>Редактирование регистрационной карты пациента
        /// Заплатин Е.Ф.
        /// 04.08.2011
        /// </summary>
        private void gridRegistrature_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LoadAmbulCard();
        }

        /// <summary>Редактирование регистрационной карты пациента
        /// Заплатин Е.Ф.
        /// 04.08.2011
        /// </summary>
        private void btnChange_Click(object sender, EventArgs e)
        {
            LoadAmbulCard();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            chkSex.Checked = chkAdge.Checked = chkBirthDay.Checked = false;

            PatientList pl = new PatientList();
            patients.Pagination = true;
            pl.Pagination = true;
            pl.Page = PageCurrent;
            patients.Page = PageCurrent;
            patients.Load(txtSearch.Text);
            pl.Load(txtSearch.Text);
            gridRegistrature.DataSource = pl;
            lblCount.Text = pl.GetCount().ToString();
            lblPageCount.Text = pl.PageCount.ToString();
            udPage.Minimum = 1;
            udPage.Maximum = pl.PageCount;
        }
        /// <summary>Условное удаление амбулаторной карты но не персоны
        /// Заплатин Е.Ф.
        /// 05.08.2011
        /// 20.10.2011 реализовано удаление пациента и амбулаторной карты
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridView t = gridRegistrature;
            if (t.SelectedRows[0] == null) return;
            if (t.SelectedRows[0].Index < 0) return;
            int id = patients[t.SelectedRows[0].Index].PatientID;
            if (id > 0)
            {
                AmbCard.Clear();
                AmbCard.PatientID = id;
                AmbCard.Load();//Загрузка амбулаторной карты пациента
                //Делается попытка загрузки историй болезней
                IllnessHistoryList ihs = new IllnessHistoryList();
                ihs.Load(AmbCard.DocumentID);
                if (ihs.Count > 0)
                {//В амбулаторной карте зарегистрированы истории болезни
                    MessageBox.Show("Не возможно удалить амбулаторную карту в которой зарегистрировыны истории болезни или платные услуги. Необходимо сначала удалить все истории болезней и оказанные платные услуги.",
                                    "Удаление амбулаторной карты", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                //По идентификатору пациента находится его амбулаторная карта. Для одного пациента возможно завести всего одну карту.
                
                
                AmbCard.Delete();//Делается отметка об удалении амбулаторной карты
                Patient p = new Patient(true);
                p.PatientID = patients[t.SelectedRows[0].Index].PatientID;
                p.Load();//Загрузка пациента необходима для загрузки адреса иначе при обновлении произойдет ошабка суть которой в том, что адрес оказался не прогруженным
                p.Delete();//Делается отметка об удалении пациента
                Filter();
            }
        }

 

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            IsActiveFind = !IsActiveFind;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (IsActiveFind)
            {
                PatientList pl = new PatientList();
                pl.Page = PageCurrent;
                patients.Page = PageCurrent;
                patients.Pagination = true;
                pl.Pagination = true;
                patients.Load(txtSearch.Text);
                pl.Load(txtSearch.Text);
                gridRegistrature.DataSource = pl;
                lblCount.Text = pl.GetCount().ToString();
                lblPageCount.Text = pl.PageCount.ToString();
                udPage.Minimum = 1;
                udPage.Maximum = pl.PageCount;
            }
        }


        private void udPage_ValueChanged(object sender, EventArgs e)
        {
            PageCurrent = (int)udPage.Value - 1;

            Filter1();

        }

        private void Filter()
        {

            _filter();

            udPage.Minimum = 1;
            udPage.Maximum = patients.PageCount;
        }

        private void _filter()
        {
            int[] selcats = new int[chlbCategory.CheckedIndices.Count];//Массив равный количеству отмеченых катрегорий
            for (int i = 0; i < chlbCategory.CheckedIndices.Count; i++)
            {
                selcats[i] = categoris[chlbCategory.CheckedIndices[i]].CategoryID;
            }

            if (cmbYear1.Text != "" && cmbYear2.Text != "" && int.Parse(cmbYear1.Text) > int.Parse(cmbYear2.Text))
                cmbYear2.Text = cmbYear1.Text;

            patients.Pagination = true;
            patients.PageSize = (int)udPageCount.Value;
            patients.Page = PageCurrent;

            patients.Load(selcats, radioButton2.Checked, cmbYear1.Text == "" ? 0 : int.Parse(cmbYear1.Text), cmbYear2.Text == "" ? 0 : int.Parse(cmbYear2.Text),
                dateTimePicker1.Value.Date, dateTimePicker2.Value.Date,
                chkSex.Checked, chkAdge.Checked, chkBirthDay.Checked);//c.ToArray()

            gridRegistrature.AutoGenerateColumns = false;

            gridRegistrature.DataSource = patients.Select(p => new
            {
                p.PatientID,
                p.LastName,
                p.SurName,
                p.Name,
                p.BirthD
            }).ToList();

            lblCount.Text = patients.GetCount().ToString();
            lblPageCount.Text = patients.PageCount.ToString();
        }


        private void Filter1()
        {
            _filter();
        }

        private void button7_Click_2(object sender, EventArgs e)
        {
            if (udPage.Value < udPage.Maximum) udPage.Value++;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if(udPage.Value>1)udPage.Value--;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            udPage.Value = 1;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            udPage.Value = udPage.Maximum;
        }

        private void udPageCount_ValueChanged(object sender, EventArgs e)
        {
            Filter1();

        }

        private void gridRegistrature_SelectionChanged(object sender, EventArgs e)
        {
            if (gridRegistrature.SelectedRows == null || gridRegistrature.SelectedRows.Count<=0) return;
            DataGridView t = gridRegistrature;
            int id = patients[gridRegistrature.SelectedRows[0].Index].PatientID;
            AmbCard.Clear();
            AmbCard.PatientID = id;
            AmbCard.Load();
            AmbCard.WriteToForm();
        }

        private void chlbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

 


    }
}
