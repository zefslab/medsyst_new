﻿namespace Medsyst
{
    partial class frmInputText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.btnCancel = new ExButton.NET.ExButton();
            this.btnOK = new ExButton.NET.ExButton();
            this.labelText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxText
            // 
            this.textBoxText.Location = new System.Drawing.Point(12, 92);
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.Size = new System.Drawing.Size(313, 20);
            this.textBoxText.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = null;
            this.btnCancel.ImageIndex = 3;
            this.btnCancel.ImagesCount = 9;
            this.btnCancel.Location = new System.Drawing.Point(258, 118);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(67, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Image = null;
            this.btnOK.ImageIndex = 5;
            this.btnOK.ImagesCount = 9;
            this.btnOK.Location = new System.Drawing.Point(191, 118);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(61, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // labelText
            // 
            this.labelText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labelText.Location = new System.Drawing.Point(12, 4);
            this.labelText.Multiline = true;
            this.labelText.Name = "labelText";
            this.labelText.ReadOnly = true;
            this.labelText.Size = new System.Drawing.Size(313, 82);
            this.labelText.TabIndex = 4;
            this.labelText.Text = "Текстовое сообщение";
            // 
            // frmInputText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 157);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.textBoxText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInputText";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ввод текста";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxText;
        public ExButton.NET.ExButton btnOK;
        public ExButton.NET.ExButton btnCancel;
        private System.Windows.Forms.TextBox labelText;
    }
}