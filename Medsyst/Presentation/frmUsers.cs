﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmUsers : Form
    {
        Users users = new Users();

        public frmUsers()
        {
            InitializeComponent();
        }

        //Чирков Е.О.
        //15.03.10
        //Добавление нового пользователя
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmUser childForm = new frmUser();
            childForm.Tag = new Command(CommandModes.New, this);
            childForm.ShowDialog();
            Command cmd = childForm.Tag as Command;
            if (cmd.Mode != CommandModes.Return) return;
            FillForm();
        }

        //Чирков Е.О.
        //15.03.10
        //Изменение данных пользователя
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;
            frmUser childForm = new frmUser();
            childForm.Tag = new Command(CommandModes.Edit, users[dataGridView1.SelectedRows[0].Index].PersonID);
            childForm.ShowDialog();
            Command cmd = childForm.Tag as Command;
            if (cmd.Mode != CommandModes.Return) return;
            FillForm();
        }

        //Чирков Е.О.
        //15.03.10
        //Наполнение списка пользователей
        void FillForm()
        {
            dataGridView1.Rows.Clear();

            users = new Users();
            users.dbOrder = "LastName";
			//Чирков Е.О.
			//15.04.10
			users.dbWhere = "Deleted=0";
            users.Load();
            foreach (User u in users)
            {
                if (u.Deleted) continue;
                User_UserGroups ugs = new User_UserGroups();
                ugs.dbSelect = "SELECT * FROM PersonUser_UserGroup WHERE UserID=" + u.UserID;
                ugs.Load();
                string s = "";
                foreach (User_UserGroup ug in ugs)
                {
                    UserGroup g = new UserGroup();
                    g.UGroupID = ug.UGroupID;
                    g.Load();
                    s += g.UGroupN + ", ";
                }
                if (ugs.Count > 0) s = s.Remove(s.Length - 2);
                dataGridView1.Rows.Add(u.Fullname, u.Login, s);
            }
        }

        //Чирков Е.О.
        //15.03.10
        private void frmUsers_Load(object sender, EventArgs e)
        {
            FillForm();
        }

        //Чирков Е.О.
        //18.03.10
        //Удаление пользователя
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;
            User user = users[dataGridView1.SelectedRows[0].Index];

            if (MessageBox.Show("Вы действительно хотите удалить пользователя '" + user.Fullname + "'?", "Подтверждение удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            //Чирков Е.О.
            //22.03.10
            user.RemoveFromTable(true); //удаление в корзину
            users.Remove(user);

            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!tsbOk.Visible) btnEdit_Click(sender, e);
        }
    }
}
