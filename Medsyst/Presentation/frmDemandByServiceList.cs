﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmDemandByServiceList : Form
    {
        DemandServiceList demands = new DemandServiceList();
        DemandService d = new DemandService(true);
        ServiceList sl = new ServiceList();//Коллекция услуг из прейскуранта
        ///<summary>Сотрудники профилактория</summary>
        MemberList members = new MemberList();
        /// <summary>Сезоны</summary>
        Seasons ss = new Seasons();

        public frmDemandByServiceList()
        {
            InitializeComponent();
        }
        ///<summary>Загрузка формы
        ///Заплатин Е.Ф.
        ///05.07.2011</summary>
        private void frmDemands_Load(object sender, EventArgs e)
        {
            gridDemands.AutoGenerateColumns = false;//Настройка грида
            gridServices.AutoGenerateColumns = false;
            //Загрузка коллекции услуг для отображения наименования в колонке УСЛУГА
            sl.showdeleted = true;//Загружать удаленные записи услуг из прейскуранта
            sl.Load(CommandDirective.AddNonDefined);
            colService.DataSource = null;
            colService.ValueMember = "ServiceID";
            colService.DisplayMember = "ServiceN";
            colService.DataSource = sl;
            //Настройка поля грида услуг
            colServiceID.DataSource = null;
            colServiceID.ValueMember = "ServiceID";
            colServiceID.DisplayMember = "ServiceN";
            colServiceID.DataSource = sl;

            //Фильтр по услуге
            cmbServices.DataSource = null;
            cmbServices.DataSource = sl;
            
            //Загрузка списка сотрудников
            members.showdeleted = true;//Загружать удаленные записи сотрудников
            members.Load(CommandDirective.AddNonDefined);
            //Колонка ФИО сотрудника
            colName.DataSource = null;
            colName.ValueMember = "PersonID_member";
            colName.DisplayMember = "FIOBrif";
            colName.DataSource = members;

            ss.Load(CommandDirective.AddNonDefined);
            //Колонка сезона
            colSeason.DataSource = null;
            colSeason.ValueMember = "SeasonID";
            colSeason.DisplayMember = "FullNbr";
            colSeason.DataSource = ss;
            //Фильтр по сезону
            cmbSeasons.DataSource = null;
            cmbSeasons.DataSource = ss;

            //Фильтр по сотруднику
            cmbEmployees.DataSource = null;
            cmbEmployees.ValueMember = "PersonID_member";
            cmbEmployees.DisplayMember = "FIOBrif";
            cmbEmployees.DataSource = members;
            

            ApplyFilter();
        }

        private void Temp() //Использовалос единожды для сохранения идентификаторов MedicamentID в таблице OnDemandMedicine
        {
            MedicamentByDemandList medbds = new MedicamentByDemandList();
            foreach (DemandService d in demands)
            {
                medbds.Load(d.DocumentID, CommandDirective.LoadDeleted);
                foreach (OnDemandMedicine medbd in medbds)
                {
                    medbd._MedicamentID = medbd.MedicamentID;
                    medbd.Update();
                }
            }

        }

        ///<summary>Открытие формы с содержимым требования
        ///Заплатин Е.Ф.
        ///14.07.2011
        ///</summary>
        private void tsbGo_Click(object sender, EventArgs e)
        {
            EditDemand();
        }
        /// <summary>Нажата кнопка применения фильтра
        /// Заплатин Е.Ф.
        /// 25.07.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApplyFilter_Click(object sender, EventArgs e)
        {
            ApplyFilter();
        }
 
        /// <summary>Применение фильтра с учетом установки различных критериев
        /// Заплатин Е.Ф.
        /// 25.07.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyFilter()
        {
            int curline = -1; // текущая выделенная строка
            if (gridDemands.SelectedRows != null && gridDemands.SelectedRows.Count > 0)
                curline = gridDemands.SelectedRows[0].Index; // запоминаем индекс выделенной строки
            demands.Clear();
            demands.Load(chkDateBegin.Checked, dateBegin.Value.Date,
                         chkDateEnd.Checked, dateEnd.Value.Date,
                         chkProviders.Checked, (int)cmbEmployees.SelectedValue, 
                         chkServices.Checked, (int)cmbServices.SelectedValue,
                         chkSeason.Checked, (int)cmbSeasons.SelectedValue, (int)DocTypes.DemandService);
            
            gridDemands.DataSource = null;
            gridDemands.DataSource = demands;
        }

        ///<summary>Кнопка "Добавить"</summary>
        private void tsbAdd_Click(object sender, EventArgs e)
        {
            frmDemandByService frm = new frmDemandByService();
            cmd = new Command(CommandModes.New) { OnUpdate = ApplyFilter };
            frm.Tag = cmd;
            frm.MdiParent = MdiParent;
            frm.Show();
        }

        ///<summary>Удаление требовани"</summary>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (gridDemands.SelectedRows != null && gridDemands.SelectedRows.Count > 0)
            {
                
                //Проверяется ситуация когда по этому требованию уже был выписан медикамент для оказания услуги
                //Если такое произошло, то остаток должен быть меньше количества выписанного по требованию
                //В этом случае нельзя удалять требование
                d.DocumentID = (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value;
                d.Load();
                if (d.Signed)
                {//Документ завизирован поэтому его удаление невозможно
                    MessageBox.Show("Невозможно удалить завизированное требование №" + d.Nbr,
                                    "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                var mbds = new MedicamentByDemandList();
                mbds.Load(d.DocumentID, CommandDirective.NotLoadDeleted);//Загружается список медикаментов выписанных по удаляемому требованию
                string medout = "";//Списанные медикаменты по требованию
                foreach (OnDemandMedicine mbd in mbds)
                {//Проверяется медикамент на предмет того, что некоторое его количество уже было списано по оказанным услугам
                    if (mbd.CountGet > mbd.CountResidue)
                    {//Часть медикамента уже была выписана
                        medout += mbd.MedicamentN + " в количестве: " + (mbd.CountGet - mbd.CountResidue).ToString();
                    }
                }
                if (medout!="")
                {//Невозможно удалить требование со списанными медикаментами
                    MessageBox.Show(this, "Невозможно удалить требование №" + d.Nbr +
                    " т.к. по нему уже списаны медикаменты " + medout, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                //Медикаменты по требованию не списывались поэтому можно его удалить
                d.DeleteDemandMedicament();//Удаляется требование методом установки признака удаления
                ApplyFilter();
            }
        }

        ///<summary>Открытие формы требования на склад
        ///Заплатин Е.Ф.
        ///14.07.2011
        ///</summary>
        private void gridDemands_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditDemand();
        }
        /// <summary>Редактирование выделенного требования
        /// Заплатин Е.Ф.
        /// 14.07.2011
        /// </summary>
        private void EditDemand()
        {
            if (gridDemands.SelectedRows != null && gridDemands.SelectedRows.Count > 0)
            {
                int curIndex = gridDemands.SelectedRows[0].Index;//Сохранение индекса строки, которая редактируется
                cmd = new Command(CommandModes.Edit, (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value) { OnUpdate = ApplyFilter };
                frmDemandByService frm = new frmDemandByService();
                frm.Tag = cmd;
                frm.MdiParent = MdiParent;
                frm.Show();
                gridDemands.Rows[curIndex].Selected = true;//Выделение редактируемой записи
            }
        }
        /// <summary>Восстановление удаленного требования
        /// Заплатин Е.Ф.
        /// 25.07.2011
        /// </summary>
        private void tsbRecover_Click(object sender, EventArgs e)
        {
            d.DocumentID = (int)gridDemands.SelectedRows[0].Cells["colDemandID"].Value;
            d.Load();
            d.Recover();
            ApplyFilter();
        }
        /// <summary>Отображение удаленных требований
        /// Заплатин У.Ф.
        /// 25.07.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            //Отображается столбец с отметкой удаленных записей
            colDeleted.Visible = chkShowDeleted.Checked;//Отображение колонки с отметками об удалении
            demands.showdeleted = chkShowDeleted.Checked;//Установка или снятие флага показа удаленных требований
            ApplyFilter();
        }

        private void chkDateBegin_CheckedChanged(object sender, EventArgs e)
        {
            dateBegin.Enabled = chkDateBegin.Checked;
        }

        private void chkDateEnd_CheckedChanged(object sender, EventArgs e)
        {
            dateEnd.Enabled = chkDateEnd.Checked;
        }

        private void chkProviders_CheckedChanged(object sender, EventArgs e)
        {
            cmbEmployees.Enabled = chkProviders.Checked;
        }

        private void chkServices_CheckedChanged(object sender, EventArgs e)
        {
            cmbServices.Enabled = chkServices.Checked;
        }
        private void chkSeason_CheckedChanged(object sender, EventArgs e)
        {
            cmbSeasons.Enabled = chkSeason.Checked;
        }
        /// <summary>Отмена всех критериев филтра
        /// Заплатин Е.Ф.
        /// 27.07.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFilterCancel_Click(object sender, EventArgs e)
        {
            chkServices.Checked = chkProviders.Checked = chkDateEnd.Checked = chkDateBegin.Checked = chkShowDeleted.Checked = chkSeason.Checked = false;
            ApplyFilter();
        }

        private void tsbSearch_Click(object sender, EventArgs e)
        {
            Temp();
        }
        /// <summary>Вывод списка услуг по которым требуется выдать медикаменты
        /// Заплатин Е.Ф.
        /// 30.10.2012
        /// </summary>
        private void btnServiceWOMedic_Click(object sender, EventArgs e)
        {
            //Перечень услуг, по которым требуется выписать медикаменты. Накапливается в процессе работы функции.
            ServiceList ssd = new ServiceList();

           
            //Производится поиск назначеных, но не оказанных услуг
            ServiceList ss = new ServiceList();
            ss.Load();//Загрузка всех неоказанных услуг

            //По каждой из найденых услуг анализируется обеспеченность медикаментами и необходимость их выдачи
            //при этом сравнивается количество медикаментов которое требуется для оказания услуг 
            //и количество медикаментов, которое уже выдано по требованиям
            foreach (Service s in ss)
            {
                MedicamentByDemandList mbds = new MedicamentByDemandList();
                mbds = DemandService.Meds(s.ServiceID, isBudget:rbtnBudget.Checked, documentID:0, pressmsg: true);//пока формируется список только по бюджетным средствам
                
                foreach (OnDemandMedicine mbd in mbds)
                {
                    if (mbd.CountGet > 0) //Количество требуемых для выдачи медикаментов больше нуля
                    {
                        //MessageBox.Show("Добавляется услуга: " + s.ServiceN + "\r\n с медикаментом: " + mbd.MedicamentN + " количество: " + mbd.CountGet.ToString());
                        ssd.Add(s);//Добавляется услуга в список услуг для выписки требований
                    }
                }
            }

            //Выводится список тех услуг, для которых необходимо оформить требования, т.е. необеспеченых медикаментами
            gridServices.DataSource = null;
            gridServices.DataSource = ssd;

        }
        /// <summary>Создание требования для услуги необеспеченной медикаментами
        /// Заплатин
        /// 10.11.2012
        /// </summary>
                private class srv: List<Object>//Вспомогательный класс для передачи двух параметров целого и булевого типа
                {
                    public srv() { }
                }
        private void btnNewDemandOnService_Click(object sender, EventArgs e)
        {
            //Анализ выделения услуги в списке услуг
            if (gridServices.SelectedRows != null && gridServices.SelectedRows.Count > 0)
            {
                //Открытие формы редатирования требования с одновременным выбором услуги и подсчетом медикаментов
                int curIndex = gridServices.SelectedRows[0].Index;//Сохранение индекса услуги, на основе которой будет создано требование
                cmd = new Command(CommandModes.New ) { OnUpdate = ApplyFilter };
                srv sr = new srv();
                sr.Add((int)gridServices.SelectedRows[0].Cells["colServiceID"].Value);//Передается идентификатор услуги на основе которой нужно подготовить требование
                sr.Add(rbtnBudget.Checked);//Передается вид бюджета
                cmd.obj = sr;
                frmDemandByService frm = new frmDemandByService();
                frm.Tag = cmd;
                frm.MdiParent = MdiParent;
                frm.Show();
                gridServices.Rows[curIndex].Selected = true;//Выделение записи услуги на основе которой создавалось требование
            }
        }
    }
}
