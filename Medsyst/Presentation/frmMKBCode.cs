﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmMKBCode : ftmpCategory
    {
        public frmMKBCode()
        {
            InitializeComponent();
        }
        ///<summary>Загрузка формы
        ///Заплатин Е.Ф
        ///24.01.2012
        ///</summary>
        private void frmMKBCode_Load(object sender, EventArgs e)
        {
            Categories = new MKBCodeList(); // Присваиваем объект дерева категорий
            Category = new MKBCode (); // Присваиваем объект категории
            LoadCategory(); // Загружаем базовую фунцию загрузки категорий
            if (cmd.Mode == Class.Core.CommandModes.Select)
            {//Форма открыта для выбора кода МКБ
                toolStrip1.Visible = true;//Визуализируется панель инструментов с кнопкой выбора
            }
            toolStripButton7.Click += new System.EventHandler(this.btnSelect_Click);//Назначается обработчик для кнопки ВЫБРАТЬ
            treeCat.CollapseAll();//Сворачивание дерева
        }
        ///<summary>Обработчик для кнопки ВЫБРАТЬ
        ///Заплатин Е.Ф
        ///24.01.2012
        ///</summary>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;
            Tag = new Command(CommandModes.Return, int.Parse(treeCat.SelectedNode.Name));//Возвращается идентификатор выбранного МКБ кода
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
