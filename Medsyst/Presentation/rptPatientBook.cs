﻿using System;
using DMS._technical;
using Medsyst.Class;
using Medsyst.DataSet;
using Medsyst.DataSet.datPatientBookTableAdapters;// Добавил ЗЕФ 19.08.2010

namespace Medsyst
{
    public partial class rptPatientBook : Form
    {
        public rptPatientBook()
        {
            InitializeComponent();
        }
    
 
        private void rptPatientBook_Load(object sender, EventArgs e)
        {
            patientBook.Load(); //Загружается отчет
            datPatientBook DS = new datPatientBook(); //Строго типизированный набор данных
            

          //Инициализация параметров хранимых процедур в адаптерах данных
            PatientBookIHDataTableAdapter TAHIData = new PatientBookIHDataTableAdapter();//Данные книжки назначений
            TAHIData.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            TAHIData.Fill(DS.PatientBookIHData,cmd.Id);
            PatientBookMedicamentTableAdapter TAMedicament = new PatientBookMedicamentTableAdapter();//Назанченные медикаменты
            TAMedicament.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            TAMedicament.Fill(DS.PatientBookMedicament, cmd.Id);
            PatientBookServiceTableAdapter TAService = new PatientBookServiceTableAdapter();//Назначаемые услуги
            TAService.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            TAService.Fill(DS.PatientBookService, cmd.Id);
            IHDiagnosMKBTableAdapter TADiagnos = new IHDiagnosMKBTableAdapter();//Диагноз профилактория при поступлении
            TADiagnos.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            TADiagnos.Fill(DS.IHDiagnosMKB, (int)MKBType.DiagAttend, cmd.Id);

            patientBook.SetDataSource(DS); //Передача отчету экземпляра набора данных
            crystalReportViewer.ReportSource = patientBook; //Элементу формы просмотра отчетов передается загруженный отчет
        }

     }
}
