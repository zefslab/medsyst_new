﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class InputBox : Form
    {
        //private DialogResult dr = DialogResult.Cancel;
        /// <summary>Возвращаемое значение </summary>
        public string Value 
        {
            get { return rtbValue.Text; }////Изменил на RegularTextBox. ЗЕФ. 10.01.2011
            set { rtbValue.Text = value; }//Изменил на RegularTextBox. ЗЕФ. 10.01.2011
        }

        [DefaultValue("Введите название")]
        public new string Text
        {
            get { return lblText.Text; }
            set { lblText.Text = value; }
        }
        [DefaultValue("Ввод")]
        public string Caption
        {
            get { return base.Text; }
            set { base.Text = value; }
        }
        
        /// <summary>Фильтр задающий шаблон для ввода. Добавил ЗЕФ. 10.01.2011 </summary>
        public FilterType Filter = FilterType.Any;

        public InputBox()
        {
            InitializeComponent();
        }
        private void InputBox_Load(object sender, EventArgs e)
        {

        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            //Close();
        }

        public new DialogResult Show(IWin32Window owner)
        {
            rtbValue.Filter = Filter;
            return ShowDialog(owner);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            //Close();
        }

 

 
    }
}
