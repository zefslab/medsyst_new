﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Dto;
using Medsyst.Data.Entity;
using Medsyst.Data.Filter;
using Medsyst.Services.Bl;

namespace Medsyst
{
    public partial class frmDemandByMaterial : Form
    {
        /* На момент работы с формой, внесенные изменения: добавления, удаления, изменение количества медикаментов пока не будут сохранены
         * не актуализируется состояние склада и расхода.
         * И только после сохранения изменений можно проверять состояние склада.
         * Это сделано для того, чтобы не производить откат на складе и расходе, в случае отказа от сохранения внесенных изменений.
         * Возможно потребуется в дальнейшем все же обеспечивать резервирование складских остатков для корректной совместной работы нескольких пользователей.
         * Это придется делать с использованием транзакций. ЗЕФ. 01.09.2011
         */
        
        
        ///<summary>Требование на склад</summary>
        DemandMaterial demand = new DemandMaterial(true);

        ///<summary>Источник финансовых средств к которому относится история болезни</summary>
        Finance finance; 
        
        ///<summary>Дефектная ведомость к требованию на склад</summary>
        DefectiveSheet ds = new DefectiveSheet(true);
        
        /// <summary>Коллекция медикаментов по требованию</summary>
        MedicamentByDemandList mbds = new MedicamentByDemandList();
        
        /// <summary>Коллекция расходуемых медикаментов</summary>
        OutputMedicamentList OutputMedicines = new OutputMedicamentList();
        
        /// <summary>Коллекция используемая для заполнения грида формы</summary>
        IList<OnDemandMaterialDto> _materials = new List<OnDemandMaterialDto>(); 
        
        /// <summary>Временно удаленные медикаменты по требованию на время работы с формой для возможности их восстановления
        /// если выйти без сохранения изменений в форме</summary>
        MedicamentByDemandList mbds_deleted = new MedicamentByDemandList();
        
        ///<summary>Сотрудники профилактория</summary>
        MemberList members = new MemberList();
        
        /// <summary>Единицы измерений</summary>
        Measures measures = new Measures();
        
        /// <summary>Сезоны</summary>
        Seasons ss = new Seasons();
        Settings set = new Settings(true);// Настройки системы

        OutputMedicineCrudService _outputMedicineCrudService = new OutputMedicineCrudService();

        OnDemandMaterialCrudService _onDemandMaterialCrudService = new OnDemandMaterialCrudService();

        public frmDemandByMaterial()
        {
            InitializeComponent();
        }
        private void frmDemandByMaterial_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "medsystDataSet.PersonPatientCategory". При необходимости она может быть перемещена или удалена.
            this.personPatientCategoryTableAdapter.Connection.ConnectionString = DB.CS;
            this.personPatientCategoryTableAdapter.Fill(this.medsystDataSet.PersonPatientCategory);
            
            gridMeds.AutoGenerateColumns = false;
            demand.FormBinding("demand", this);
            set.Load();//Загрузка настроек системы для получения в последствии текущего сезона

            //Загружается список сотрудников для выбора ответственного лица за требование
            cmbMembers.DataSource = null;
            members.Load(CommandDirective.AddNonDefined);
            cmbMembers.DataSource = members;

            //Загрузка коллекции единиц измерений для подстановки в соответсвующий столбец
            colMeasures.ValueMember = "MeasureID";
            colMeasures.DisplayMember = "ShotName";
            colMeasures.DataSource = null;
            measures.Load();
            colMeasures.DataSource = measures;

            //Загрузка сезонов
            cmbSeason.DataSource = null;
            ss.Load(CommandDirective.AddNonDefined);
            cmbSeason.DataSource = ss;

            cmd = Tag as Command;//Принятие интерфейса от вызывающей формы
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
            {//Создание нового требования на склад
                demand.RegisterD = DateTime.Now;
                string doctypes = ((int)DocTypes.DemandMaterial).ToString() + "," +
                                  ((int)DocTypes.DemandMedicament).ToString() + "," +
                                  ((int)DocTypes.DemandService).ToString(); //Формируется список типов документов (все требования) у которых следует обеспечить сквозную нумерацию
                demand.Nbr = demand.GetNewNumber(doctypes, false);
                demand.SeasonID = set.CurrentSeason;
                demand.Insert();//Добавляется запись в базу о вновь созданном требовании. Если форма
                //будет закрыта без сохранения, то эта запись должна быть удалена, а с ней и все записи о 
                //добавленных медикаментах
             }
            else
            {//загрузка уже существующего требования
                demand.DocumentID = cmd.Id;
                demand.Load();
                
                //mbds = demand.LoadMed(chkShowDeleted.Checked);//Загрузка медикаментов по требованию
                _materials = _onDemandMaterialCrudService
                        .Get(new OnDemandMedicineFilter { DocumentId = demand.DocumentID, withDeleted = true });

                SetFormControls();
            }

            //Установка источника финансовых средств
            if (demand.IsBudget)
                finance = Finance.Budget;
            else
                finance = Finance.ExtraBudget;
            
            LoadMedsToGrid();
            
            demand.WriteToForm();
            
            //Вынужденное добавление этого условия после метода WriteToForm. Иначе этот флаг всегда будет - false
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
                DataSaved (false);
            else
                DataSaved ( true);
        }
        /// <summary>В зависимости от состояния свойств: Signed и IsDefected устанавливается доступность элементов управления
        /// Заплатин Е.Ф.
        /// 06.09.2011
        /// </summary>
        private void SetFormControls()
        {
            rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = !(OutputMedicines.Count > 0);//Если в требование уже добавлены медикаменты из определенного
            //источника финансирования, то изменять свойство источника финансирования самого требования недозволено

            //Настройка доступности управляющих элементов формы
            tbtnSign.Text = (demand.Signed?"Снять подпись":"Подписать");
            cmbMembers.Enabled = txtNbr.Enabled = dtpDate.Enabled = cmbCategory.Enabled = cmbSeason.Enabled = !demand.Signed;
            tbtnSave.Enabled = !demand.Signed;
            colCount.ReadOnly = demand.Signed;
            btnAdd_Medicine.Enabled = btnDel_Medicine.Enabled = btnRestoreMedecine.Enabled = !demand.Signed;
            tbtnDifect.Enabled = demand.Signed;//Возможность дифектования доступна только после наложения визы гл. врача
            
            if (demand.IsDefected)
            {//Дефектная ведомость уже была оформлена поэтому загружается информация о ней
                tbtnDifect.Enabled = false;
                ds.DocumentID = demand.DocumentID;
                ds.Load();
                Employee emp = new Employee(true);
                emp.PersonID = ds.EmployeeDefectID;
                emp.Load();

                //Формируется информационная строка с указанием на дату оформления и сотрудника оформившего дефектную ведомость
                lblDefectiveSheet.Text = "Дефектную ведомость от " + ds.DefectiveD.ToString("dd.MM.yyyy") + " оформил"+ (emp.Sex?"а":"") + ": "+  emp.FIOBrif;
                lblDefectiveSheet.Visible = true;

                //Элементы управления для изменения количества и состава медикаментов не доступны
                colCount.ReadOnly = true;
                btnAdd_Medicine.Enabled = btnDel_Medicine.Enabled = btnRestoreMedecine.Enabled = false;
            }
        }
        /// <summary>Эту логику изменения статуса и доступности кнопки "Сохранить" лучше всего перенести в родительскую форму
        /// Заплатин Е.Ф.
        /// 02.09.2011
        /// </summary>
        /// <param name="saved"></param>
        public void DataSaved(bool saved)
        {
            dataSaved = saved;
            tbtnSave.Enabled = !saved;
        }


        /// <summary>Загрузка в грид медикаментов назначенных в расход по требованию. 
        /// Принято решение загружать в грид именно расходные документы, а не медикаменты по требованию
        /// т.к. в них хранится больше информации о медикаментах на складе.
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// </summary>      
        private void LoadMedsToGrid()
        {
            gridMeds.DataSource = null;
            gridMeds.DataSource =_materials.Where(x => x.Deleted == chkShowDeleted.Checked).ToList();
        }
        /// <summary>Добавление к требованию произвольного медикамента со склада.
        /// Заплатин Е.Ф.
        /// 23.08.2011
        /// </summary>
        private void btnAdd_Medicine_Click(object sender, EventArgs e)
        {
            //Параметр указывает на ограничение показа медикаментов по признаку источника финансирования
            var frm = new frmSclad(finance)
            {
                Tag = new Command(CommandModes.MultiSelect, this)
                            {
                                OnUpdate = addMedicine,
                                // для исключения из списка выбора на складе
                                obj = _materials.Select(x=>x.MedicamenOnScladID).ToList()
                            }
            };

            frm.ShowDialog();//Обращение на склад за выбором медикамента
        }
        
        /// <summary> Добавляется медикамент в грид формы требования, при выборе его со склада.
        /// </summary>
        public void addMedicine()
        {
            //Добавить новый  медикамент в коллекцию медикаментов по требованию
            OnDemandMaterialDto material = new OnDemandMaterialDto()
            {
                MedicamenOnScladID = ((Command)Tag).Id,
                CountOutput = 0,
                DemandID = demand.DocumentID,
                
            };

            if (!_materials.Any(x=> x.MedicamenOnScladID == material.MedicamenOnScladID))
            {
                material = _onDemandMaterialCrudService.Add(material);

                MedicamentOutput mo = new MedicamentOutput();
                mo.New(material.MedicamenOnScladID, demand.DocumentID);
                OutputMedicines.Add(mo);

                OnDemandMedicine mbd = new OnDemandMedicine();
                mbd.New(mo.MedicamentID, demand.DocumentID);
                mbds.Add(mbd);

                _materials.Add(material);
            }
            else
            {
                //Медикамент с таким идентификаторм уже существует в требовании, поэтому добавление его не возможно
                MessageBox.Show("Медикамент с идентификатором" + material.Id + " уже был добавлен к текущему требованию. Повторное добавление идентичного медикамента не возможно в одном требовании.\n\r" +
                    "Чтобы выписать требование именно на этот медикамент, нужно создать новое требование.", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                
                return;
            }

            DataSaved(false);
            
            rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = false;//После добавления медикамента в требование
            //изменять свойство источника финансирования самого требования становится невозможно

            LoadMedsToGrid();//Заполнение грида коллекцией расходных медикаментов по требованию

        }
       
        ///<summary>Кнопка "Подписать"
        ///Заплатин Е.Ф.
        ///23.08.2011</summary>
        private void btnSign_Click(object sender, EventArgs e)
        {
            if (demand.Signed)
            {//Документ уже был подписан
                demand.Sign();//Снятие подписи
                SetFormControls();//Элементы управления делаются доступными
            }
            else
            {//Документ еще не подписан
                //Прежде чем закрывать форму, поставить подпись, подтвердающую завершение формирование требования.
                //После того как будет поставлена подпись, внесение изменений станет невозможным
                DialogResult res = MessageBox.Show("Выберите - Да, если Вы уверены в том, что хотите подписать требование. " +
                            " После наложения подписи внесение изменений в требование и его удаление станет невозможным!\n\r" +
                            " Выберите - Нет, для сохранения изменений и завершения редактирования требования без наложения подписи.\n\r" +
                            " Выберите - Отмена, для продолжения редактирования требования.", "Подпись документа",
                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {//Сохранение изменений с подписью
                    demand.Sign();
                }
                else if (res == DialogResult.No)
                {//Сохранение документа без подписи

                }
                else if (res == DialogResult.Cancel)
                {//Продолжение редактирования документа
                    return;
                }
                Save();
                Close();
            }
            
        }
        ///<summary>Сохранение изменений в форме. 
        /// Все изменения и дополнения коллекции медикаментов принимают силу и сохраняются в базе
        ///Заплатин Е.Ф.
        ///23.08.2011</summary>       
        private void Save()
        {
        //Шаг 1. Сохраняются все свойства самого требования
            demand.ReadFromForm();
            demand.Update();

        //Шаг 2. Сохраняются измененные или добавленные элементы коллекции расходных медикаментов, 
        //а удаленные в текущем сеансе отмечаются как удаленные 14.12.2013
            var deletedOutputMedicines = OutputMedicines.Where(x => x.DeletedOut).ToList();

            if (deletedOutputMedicines.Any())
            {
                var isDeleteSucsess = ((OutputMedicamentList) deletedOutputMedicines).Delete(CommandDirective.MarkAsDeleted);
                
                if (!isDeleteSucsess)
                    MessageBox.Show("Не удалось удалить медикаменты из расхода, поэтому не возможно назначить новую услугу. \n\rЭто нештатная ситуация. Сообщите о ней администратору Медсист.",
                                   "Прерывание операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
            OutputMedicines.SaveNotSaved();
            OutputMedicines.UpdateModified();

           
        //Шаг 3. Сохраняются измененные или добавленные элементы коллекции медикаментов по требованию,
        //а удаленные в текущем сеансе отмечаются как удаленные
            if (!demand.DeleteMeds(ref mbds_deleted, CommandDirective.MarkAsDeleted))
                 MessageBox.Show("Не удалось удалить медикаменты по требованию, поэтому не возможно назначить новую услугу. \n\rЭто нештатная ситуация. Сообщите о ней администратору Медсист.",
                                "Прерывание операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            mbds.SaveNotSaved();
            mbds.UpdateModified();

        //Шаг 4. Обновление записей в гриде медикаментов после сохранения всех изменений
            OutputMedicines.Load(demand.DocumentID);
            
            LoadMedsToGrid();//Заполнение грида коллекцией рекомендуемых к выписке медикаментов

            cmd.UpdateParent();
            
            DataSaved (true);
        }

        ///<summary>Закрытие формы
        ///Заплатин Е.Ф.
        ///23.08.2011</summary>
        private void frmDemandByMaterial_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!dataSaved)
            {//Какая-то часть требования в форме была изменена
                DialogResult dr = MessageBox.Show(this, "Сохранить изменения внесенные в требование?",
                    "Редактирование требования",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {//Закрытие с сохранением изменений внесенных в текущем сеансе работы
                    Save();
                }
                else if (dr == DialogResult.No)
                {//Закрытие без сохранения изменений внесенных в текущем сеансе работы
                    /* Т.к. в процессе внесения изменений в медикаменты не производилось их сохранение в базу данных, то откатывать ничего не нужно.
                     * В перспективе предполагается актуализировать состояние склада при каждом изменении медикаментов.
                     * Тогда потребуется производить откатные действия. ЗЕФ. 01.09.2011
                     */
                    if (cmd.Mode == CommandModes.New)
                    {//Требование вновь созданное
                        demand.Delete(); //Удаление записи самого требования
                        cmd.UpdateParent();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        

        /// <summary>Изменение количества выписываемых по требованию медикаментов. При этом должно корректироваться 
        /// количество:
        /// 1. резерва на складе с проверкой доступности добавляемого количества на остатках 
        /// 2. расхода, 
        /// 3. медикаментов по требованию
        /// Заплатин Е.Ф.
        /// 23.08.2011
        /// </summary>
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == gridMeds.Columns["colCount"].Index)
            {//Проверка на выделение хотя бы одной строки в списке медикаментов и редактирование колонки с количеством медикаментов
                
                //В зависимости от того был ли ранее сохранен медикамент у него может быть разное старое количество
                if (_materials[e.RowIndex].Id == 0)
                {//Медикамент был добавлен лишь в текущем сеансе работы и еще не сохранен
                    int count = (int)gridMeds.Rows[e.RowIndex].Cells["colCount"].Value;
                    var ms = new MedicamentOnSclad();
                    ms.MedicamentOnScladID = _materials[e.RowIndex].MedicamenOnScladID;
                    ms.Load();
                    //Проверка на доступность медикаментов на складе
                    if (count > ms.CountDivide)
                    {//Остаток на складе меньше запрошенного количества, поэтому это количество корректируется 
                        MessageBox.Show("Указанное количество медикамента превышает доступное на складе и поэтому будет уменьшено. ", "Недостаточно медикаментов на складе", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        gridMeds.Rows[e.RowIndex].Cells["colCount"].Value = ms.CountDivide.ToString();
                    }
                    _materials[e.RowIndex].CountOutput = mbds[e.RowIndex].CountGet = mbds[e.RowIndex].CountResidue = count;
                }
                else
                {//Требуется загрузка из базы старых данных о количестве расхода если медикамент не вновь добавлен
                    MedicamentOutput old_mo = new MedicamentOutput();
                    old_mo.MedicamenOutputID = _materials[e.RowIndex].Id;
                    old_mo.Load();
                    int oldCount = old_mo.CountOutput; //Определяет предыдущее сохранненое в базе значение количества медикамента до его изменения
                //Увеличение количества расхода не должно превысить доступное количестов медикаментов на складе,
                //а уменьшение не должно привезти к установке резерва меньше нуля
                    if (OutputMedicines[e.RowIndex].Output(oldCount))//Изменение количества расхода с корректировкой резерва на складе но без обновления состояния склада
                    {//Изменение количества произошло успешно
                        //Проверка превышения величины остатка медикамента по требованию не нужна, т.к. медикамент списывается целиком
                        //через дефектную ведомость, а не частями как в других требованиях. Поэтому нужно лишь скорректировать количество 
                        //медикамента по требованию
                        int index = FindMedByDemand(OutputMedicines[e.RowIndex].MedicamentID);
                        if (index != -1)
                        {
                            mbds[index].CountResidue = mbds[index].CountGet = OutputMedicines[e.RowIndex].CountOutput;
                            mbds[index].Modifie();
                        }

                        gridMeds.Rows[e.RowIndex].Cells["colCount"].Value = OutputMedicines[e.RowIndex].CountOutput.ToString(); //Обновление поля с количеством  после окончания редактирования записи
                    }
                    DataSaved (false);
                }
            }
        }

        private void txtNbr_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void cmbSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void cmbMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void rbtnIsBudget_CheckedChanged(object sender, EventArgs e)
        {
            demand.IsBudget = rbtnIsBudget.Checked;//Требуется изменение т.к. нужно гибко реагировать не дожидаясь сохранения изменений в форме
            //Установка источника финансовых средств
            if (demand.IsBudget)
                finance = Finance.Budget;
            else
                finance = Finance.ExtraBudget;
            rbtnNotBudget.Checked = !rbtnIsBudget.Checked;//Установка контрола для внебюджетных средств
            DataSaved(false);
        }
        /// <summary>Производится поиск медикамента по требованию в коллекции соответсвующего расходногму медикаменту
        /// Заплатин Е.Ф.
        /// 31.08.2011
        /// </summary>
        /// <param name="moID">Идентификатор расходного медикамента</param>
        /// <returns>порядковый номер найденного медикамента в коллекции или -1 если не найдено соответсвия</returns>
        private int FindMedByDemand(int moID)
        {
            int m = 0 ;//Вероятности того, что медикамент не будет найден не существует, т.к. производится синхронное добавление медикаментов в расход и по требованию
            for (int i = 1; i <= mbds.Count; i++)
                if (mbds[i-1].MedicamentID == moID)
                    m = i;
            if (m == 0)
            {//не найдено ни одного медикамента по требованию соответствующего медикаменту расходному
                MessageBox.Show("Не найдено ни одного медикамента по требованию соответствующего медикаменту расходному", "Проблема!", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Error);
            }
            return m-1;//Если не найдено, то возвратиться -1
        }
        /// <summary>Удаление медикамента из коллекций медикаментов по требованию, и коллекции расходных медикаментов 
        /// Возможны два варианта развертывания события:
        /// 1. Удаляется медикамент, который был уже сохранен в базе
        /// 2. Удаляется только что добавленный, но не сохраненный в базе медикамент
        /// В первом случае нужно переместить удаляемый медикамент во временную колекцию удаленных медикаментов
        /// Во втором сучае нужно только удалить из коллекции
        /// Заплатин Е.Ф.
        /// 23.08.2011
        /// </summary>
        private void btnDel_Medicine_Click(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows == null) return;

            DialogResult res = MessageBox.Show("Действительно хотите удалить медикамент из требования?",
                               "Удаление медикамента из требования", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
         //Шаг 1. Удаление медикамента из колекции расходных медикаментов
                MedicamentOutput mo = new MedicamentOutput();
                if (chkShowDeleted.Checked)
                {//Если выводится список без удаленных медикаментов
                   mo = OutputMedicines.Where(x=>!x.DeletedOut).ToList()[gridMeds.SelectedRows[0].Index];
                }
                else
                {
                    mo = OutputMedicines[gridMeds.SelectedRows[0].Index];
                }
                
                

                if (!mo.NotSaved)
                {//Сохраненный в базе данных медикамент. Нужно переместить в удаленную колекцию
                    //Перед проверкой на возможность удаления нужно загрузить сведения из базы данных, т.к. могут быть внесены изменения в количество, но еще не сохранены в текущем сеансе работы с данными.
                    //Несохраненное значение  количества нельзя использовать для корректирования состояния склада.
                    mo.Load();
                    if (!mo.TryDelete()) return;//Проверка на предмет возможности удаления не пройдена
                 }
                else
                {//Не сохраненный медикамент в базе данных. Нужно лишь отметить как удаленный
                }
                //Удаление медикамента из коллекции расхода немного отложено (см. ниже) 
                //т.к. полной коллекцией приходится пользоваться для нахождения идентичного медикамента по требованию

         //Шаг 2. Удаление медикамента из коллекции медикаментов по требованию
                //находтися медикамент по требованию, соотвтсвующий расходному
                OnDemandMedicine mbd = new OnDemandMedicine();
                mbd = mbds[FindMedByDemand(OutputMedicines[gridMeds.SelectedRows[0].Index].MedicamentID)];//Соответсвие определяется по совпадению идентификатора каталожного медикамента
                if (!mbd.NotSaved)
                {
                    //Сохраненный в базе данных медикамент. Нужно переместить в удаленную колекцию
                    mbds_deleted.Add(mbd);
                    DataSaved(false);
                }
                else
                {//Не сохраненный медикамент в базе данных. Нужно лишь удалить из коллекции
                    //Отметку об изменении данных делать не нужно, т.к. удаление вновь добавленного медикамента не требует внесения изменений в базу
                }

                //Удаление медикамента из коллекции медикаментов по требованию
                mbds.Remove(mbd);
                //Удаление медикамента из коллекции расхода.
                //OutputMedicines.Remove(mo);
                mo.DeletedOut = true;
                rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = !(OutputMedicines.Count > 0);//Если после удаленияв требовании не осталось медикаментов
                //то изменять свойство источника финансирования самого требования становится возможно

                LoadMedsToGrid();
            }
        }
        /// <summary>Выписка дефектной ведомости. Это значит, что:
        /// 1. Признак резерва с расхода снимается, 
        /// 2. на складе удаляется резерв и уменьшается общее количество. 
        /// 3. Уменьшается остаток медикаментов по требованиям.
        /// Визируется документ
        /// Заплатин Е.Ф.
        /// 04.09.2011
        /// </summary>
        private void tbtnDifect_Click(object sender, EventArgs e)
        {
            if (!demand.Signed)
            {//проверка на предмет наложения визы руководителем
                MessageBox.Show("Не возможно произвести дефектование медикаментов без наложения визы."
                              , "Отказ в создании дефектной ведомости", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Выписка дефектной ведомости приведет к снятию резерва и списанию со склада медикаментов выписанных по текущему требованию.\n\r\n" +
                                                 "Вы подверждаете выполнение этой операции, отменить которую будет невозможно?"
                                                , "Создание дефектной ведомости", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
            {
                //Cохранение последних изменений в требовании перед оформлением дефектной ведомости
                if (!dataSaved) Save();
                
                //Создание дефектной ведомости
                DefectiveSheet dsheet = new DefectiveSheet(true);
                dsheet.DocumentID = demand.DocumentID;
                //((DemandMaterial)ds).Load();//Загрузка данных в родительские свойства для последующего частичного обновления их в методе Insert. Почему-то не работает
                if (dsheet.Insert(demand.DocumentID))
                {//Дефектная ведомость создана успешно

                    //Снятие признака резерва с расходных медикаментов, вычитание резерва и количества медикаментов на складе
                    OutputMedicines.UnreservateMaterial();

                    //Уменьшение остатков медикаментов по требованию
                    mbds.UnreservateMaterial();

                    //Установка отметки о создании дефектной ведомости после окончательного списания медикаментов со склада
                    demand.IsDefected = true;
                    demand.Update();

                    //Обновление записи с требованием в вызывающей форме и переустановка доступности элементов управления формы
                    cmd.UpdateParent();
                    SetFormControls();
                }
            }
        }
        //Показывается столбец с отметкой об удалении
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            colDeleted.Visible = btnRestoreMedecine.Visible = chkShowDeleted.Checked;
            //OutputMedicines = demand.LoadMedsOutput(chkShowDeleted.Checked);
            LoadMedsToGrid();
        }
        /// <summary>Открытие отчета по требованию для его распечатки
        /// Заплатин Е.Ф.
        /// 20.11.2011
        /// </summary>
        private void tbtnPrint_Click(object sender, EventArgs e)
        {
            rptDemand rpt = new rptDemand();
            rpt.Tag = new Command(CommandModes.Open, demand.DocumentID, this);
            rpt.ShowDialog(this);
        }

        private void btnRestoreMedecine_Click(object sender, EventArgs e)
        {
            //if (gridMeds.SelectedRows == null) return;
            ////Операция применима только к удаленным медикаментам
            //if (!(bool)gridMeds.SelectedRows[0].Cells["colDeleted"].Value) return;

            ////Снять отметку об удалении у медикамента
            //OutputMedicines[gridMeds.SelectedRows[0].Index].DeletedOut = false;
            //OutputMedicines[gridMeds.SelectedRows[0].Index].Update(); //Обновление в базе данных

        }
    }
}
