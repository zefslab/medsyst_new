﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Class.Forms;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;
using Medsyst.Extentions;


namespace Medsyst
{
    public partial class frmMedicamentOutput : Form
    {
        //Расходные документы  
        ///<summary>Историй болезней</summary>
        IllnessHistoryShortList histories = new IllnessHistoryShortList();

        ///<summary>Требования на склад</summary>
        DemandShortList demands = new DemandShortList();
        ///<summary>Список расходных медикаментов</summary>
        OutputMedicamentList mos = new OutputMedicamentList();
        /// <summary>Список сезонов </summary>
        Seasons seasons = new Seasons(); //Инициируется экземпляр коллекции сезонов;
        /// <summary>Список сотрудников мед. учреждения </summary>
        MemberList members;
        /// <summary>Список типов расходных документов</summary>
        DocumentTypeList DTOlist = new DocumentTypeList();

        /// <summary> Тип расходного документа представленный в виде индексного списочного значения</summary>
        int doctype = 4; //По умолчанию устанавливается тип расходного документа - Истории болезней

        public frmMedicamentOutput()
        {
            InitializeComponent();
        }
        /// <summary>Загрузка формы</summary>
        private void frmScladOutput_Load(object sender, EventArgs e)
        {
            this.personPatientCategoryTableAdapter.Connection.ConnectionString = DB.CS;
            this.personPatientCategoryTableAdapter.Fill(this.medsystDataSet.PersonPatientCategory);
            gridMeds.AutoGenerateColumns = false;
            gridOutDocuments.AutoGenerateColumns = false;            //Сначала настраиваются фильтры всех расходных документов на значения по умолчанию 
            FilterIllnessHistory(); //Настраивается фильр истории болезней настройками по умолчанию
            FilterDemand(); //Настраивается фильр требований на склад настройками по умолчанию
            //Затем запускается главный механизм отображения расходных документов
            //с учетом установок фильров
            Main();
        }
        /// <summary>Определяет какая закладка является текущей и сопоставляет с ней тип расходного документа
        /// Устанавливаются значения по умолчанию для фильтра
        /// Загружается соответсвующий список расходных документов
        /// Автор: Заплатин 18.10.2010
        /// </summary>
        private void Main()
        {
            if (tabDocOutType.SelectedIndex == 0)
            {//открыта закладка Истории болезни
                LoadHistories();//Настраивается элемент вывода списка расходных документов
            }

            else if (tabDocOutType.SelectedIndex == 1)
            {//Открыта закладка Требования на склад
                LoadDemands();//Настраивается элемент вывода списка расходных документов
            }
            else
            {//Дополнить структуру др. вариантами выбора
                // А пока...
                gridOutDocuments.DataSource = null;// ... просто очищается элемент grid

            }
            /* Закоментировал ЗЕФ т.к приводит к бесполезному, дублирующему обращению метода загрузки медикаментов
            //Анализируется выделение строк и их количество
            if (gridOutDocuments.SelectedRows != null & gridOutDocuments.SelectedRows.Count > 0)
                LoadMedsToGrid((int)gridOutDocuments.SelectedRows[0].Cells["colDocID"].Value);//Загрузка медикаментов по выбранному расходному документу
            else 
                {   // иначе нужно очитстить список медикаментов
                    gridMeds.DataSource = null;
                }
             */
        }
        /// <summary>Настройка фильтра Истории болезни и установка стартовых значений элементов фильтра</summary>
        private void FilterIllnessHistory()
        {
            //Считывание настроек системы
            Settings set = new Settings(true);
            set.Load();
            numYear.Value = new decimal(new int[] { set.CurrentYear, 0, 0, 0 });//Устанавливается текущий год в качестве стартового значения фильтра
            LoadSeasonsIH((int)numYear.Value);//Загружается список сезонов 
        }
        /// <summary>Загрузка списка сезонов для фильтра историй болезней</summary>
        /// <param name="year"></param>
        private void LoadSeasonsIH(int year)
        {
            seasons.Load(year);//Загрузка сезонов по установленному фильтру поля года
            cmbSeason.DataSource = null; //Обнуление источника данных
            cmbSeason.DataSource = seasons;//Назначение загруженных сезонов в качестве источника данных комбобоксу   

            Settings set = new Settings(true);
            set.Load();//Загружаются настройки системы
            if (year == set.CurrentYear)//если установленный год является текущим
            {
                cmbSeason.SelectedValue = set.CurrentSeason; //Устанавливается текущий сезон в качестве выделенного в списке.
            }
        }
        /// <summary>Настройка фильтра Требований на склад и установка стартовых значений элементов фильтра</summary>
        private void FilterDemand()
        {
            //Инициируется список сотрудников компании
            members = new MemberList();
            members.Load(CommandDirective.Non);
            cmbMember.DataSource = members;
            //Инициируются сезоны
            //Считывание настроек системы
            Settings set = new Settings(true);
            set.Load();
            intYearDemand.Value = new decimal(new int[] { set.CurrentYear, 0, 0, 0 });//Устанавливается текущий год в качестве стартового значения фильтра
            LoadSeasonsDemand((int)intYearDemand.Value);//Загружается список сезонов 

        }
        /// <summary>Загрузка списка сезонов для фильтра требований на склад</summary>
        /// <param name="year"></param>
        private void LoadSeasonsDemand(int year)
        {
            seasons.Load(year);//Загрузка сезонов по установленному фильтру поля года
            cmbSeasonDemand.DataSource = null; //Обнуление источника данных
            cmbSeasonDemand.DataSource = seasons;//Назначение загруженных сезонов в качестве источника данных комбобоксу   

            Settings set = new Settings(true);
            set.Load();//Загружаются настройки системы
            if (year == set.CurrentYear)//если установленный год является текущим
            {
                cmbSeasonDemand.SelectedValue = set.CurrentSeason; //Устанавливается текущий сезон в качестве выделенного в списке.
            }
        }
        /// <summary>Загружается список историй болезней с учетом значений фильтра по умолчанию</summary>
        void LoadHistories()
        {
            //Первое поле используется для тестирования и поэтому скрыто
            gridOutDocuments.Columns["colDocID"].HeaderText = "ID";
            gridOutDocuments.Columns["colDocID"].DataPropertyName = "DocumentID";
            gridOutDocuments.Columns["colDocID"].Visible = false;

            gridOutDocuments.Columns["colNum"].HeaderText = "№";
            gridOutDocuments.Columns["colNum"].DataPropertyName = "Season";

            gridOutDocuments.Columns["colDesc"].HeaderText = "ФИО";
            gridOutDocuments.Columns["colDesc"].DataPropertyName = "FullName";

            gridOutDocuments.Columns["colDate"].Visible = true;//подумать и может быть совсем убрать это неиспользуемое поле. ЗЕФ
            gridOutDocuments.Columns["colDoctor"].Visible = true;//востанавливает видимость поля после перехода из др. закладок расходных документов

            gridOutDocuments.Columns["colSum"].DataPropertyName = "SummMedicament";

            gridOutDocuments.DataSource = null;
            //Проанализировать значения фильтра и если они установлены верно то загрузить список расходных документов
            if (cmbSeason.SelectedValue == null || cmbCategoryPacient.SelectedValue == null)
            {
                MessageBox.Show("Один или более параметров фильтра не установлены. Заполните фильтр и повторите запрос на фильтрацию.",
                    "Ошибка в фильтре", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            histories.Load((int)cmbSeason.SelectedValue, (int)cmbCategoryPacient.SelectedValue);//Загрузка с учетом фильтра

            Season season = new Season {SeasonID = (int)cmbSeason.SelectedValue };
            season.Load();

            gridOutDocuments.DataSource = histories.Select(h => new DocDto
            {
                DocumentID = h.DocumentID,
                Season = h.NbrIH + "/" + season.SeasonNbr,
                FullName = h.FullName,
                DoctorName = h.DoctorName,
                SummMedicament = h.SummMedicament
            }).ToList();
        }
        /// <summary>Загружается список требований на склад</summary>
        void LoadDemands()
        {
            gridOutDocuments.Columns["colDocID"].HeaderText = "ID";
            gridOutDocuments.Columns["colDocID"].DataPropertyName = "DocumentID";
            gridOutDocuments.Columns["colDocID"].Visible = false;
            gridOutDocuments.Columns["colNum"].HeaderText = "№";
            gridOutDocuments.Columns["colNum"].DataPropertyName = "Nbr";
            gridOutDocuments.Columns["colDesc"].HeaderText = "ФИО";
            gridOutDocuments.Columns["colDesc"].DataPropertyName = "FullName";
            gridOutDocuments.Columns["colDate"].HeaderText = "Дата";
            gridOutDocuments.Columns["colDate"].DataPropertyName = "RegisterD";
            gridOutDocuments.Columns["colDate"].Visible = true;
            gridOutDocuments.Columns["colDoctor"].Visible = false;
            gridOutDocuments.Columns["colSum"].Visible = true;
            gridOutDocuments.DataSource = null;

            //Проанализировать значения фильтра
            if (cmbSeasonDemand.SelectedValue == null || cmbMember.SelectedValue == null)
            {
                MessageBox.Show("Один или более параметров фильтра не установлены. Заполните фильтр и повторите запрос на фильтрацию.",
                    "Ошибка в фильтре", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            demands.Clear();

            demands.Load((int)cmbSeasonDemand.SelectedValue, (int)cmbMember.SelectedValue);

            gridOutDocuments.DataSource = demands;
        }
        /// <summary>Нажатие на кнопку "Установить фильтр"
        /// Автор: Заплатин Е.Ф. 04.10.2010
        /// </summary>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            Main();
        }
        /// <summary>Изменение значения года влечет за собой изменение наполнения списка сезонов</summary>
        private void numYear_ValueChanged(object sender, EventArgs e)
        {
            //После изменения значения счетчика года необходимо обновить список сезонов
            LoadSeasonsIH((int)numYear.Value);
        }
        /// <summary>Переход по закладкам и вывод списка расходных документов в зависимости от выбранной закладки</summary>
        private void tabDocOutType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Main();//Главный механизм отображения расходных документов
        }

        /// <summary>Выдача медикамента со склада, снятие с резерва и уменьшение остатка по требованиям в соответсвии с источником финансирования
        /// Заплатин Е.Ф.
        /// 20.08.2011
        /// </summary>
        private void gridMeds_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridMeds.SelectedRows.Count == 0) return;

            //Клик сделан в пределах ячеек таблицы
            //Клик сделан на ячейке с чекитом разрезервирования
            if (gridMeds.Columns[e.ColumnIndex].Name != "colUnReserved") return;

            var mbds = new MedicamentByDemandList();

            var f = mos[e.RowIndex].IsBudget ? Finance.Budget : Finance.ExtraBudget;

            if (mos[e.RowIndex].Reserved)
            {//Медикамент стоит в резерве и его можно выдавать пациентам
                //Прежде чем снимать резервы со склада и требования нужно убедиться, что по требованиям выписано достаточное количество медикаментов
                mbds.Load(0, mos[e.RowIndex].MedicamentID, (int)DocTypes.DemandMedicament, true, f);//Загрузка медикаментов по требованиям с ненулевыми остатками
                //для проверки достаточности для списания и оменьшения/увеличения остатков
                if (!mbds.CheckCount(mos[e.RowIndex].CountOutput, mos[e.RowIndex].MedicamentID, f))//Проверка достаточности медикаментов по требованиям для списания
                    return;// Недостаточно медикаментов выписанных по требованиям
            }
            else
            {//Медикамент уже выдавался пациентам и его нужно вернуть на склад поставив в резерв и увеличить остаток по требованию
                mbds.Load(0, mos[e.RowIndex].MedicamentID, (int)DocTypes.DemandMedicament, false, f);//Загрузка медикаментов по требованиям 
            }

            mos[e.RowIndex].Unreservate(mbds);//Снятие резервов и уменьшение остатков по требованиям, а так же обратная операция
            LoadMedicaments();
            gridMeds.Refresh();

        }

        /// <summary>
        /// Изменение значения года влечет за собой изменение наполнения списка сезонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void intYearDemand_ValueChanged(object sender, EventArgs e)
        {
            //После изменения значения счетчика года необходимо обновить список сезонов
            LoadSeasonsDemand((int)intYearDemand.Value);

        }
        /// <summary>Изменена область выбеления историй болезней
        /// Заплатин Е.Ф.
        /// 08.04.2011
        /// </summary>
        private void gridOutDocuments_SelectionChanged(object sender, EventArgs e)
        {
            LoadMedicaments();
        }
        
        private void gridOutDocuments_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            LoadMedicaments();
        }

        /// <summary>Загрузка расходных медикаментов по всем выделенным расходным документам
        /// Заплатин Е.Ф.
        /// 08.04.2011
        /// 8.02.2016 - рефакторинг
        /// </summary>
        private void LoadMedicaments()
        {
            //Анализируется выделение строк в списке документови их количество
            if (gridOutDocuments.SelectedRows.Count == 0) return;

            //выделена одна или более записей расходных документов для отображения списка медикаментов
            mos.Clear();

            var docsId = gridOutDocuments.SelectedRows.Select(r => (int)r.Cells["colDocID"].Value);

            foreach (var id in docsId)
            {//Перебираются все выделенные строки в таблице расходных документов
                var mos_doc = new OutputMedicamentList();

                //анализируется фильтрующий критерий по медикаментам 
                if (rbShowAll.Checked)//Выбран критерий - "Загружать все  медикаменты" 
                    mos_doc.Load(CommandDirective.NotLoadDeleted, id);//Исполнение метода загрузки медикаментов. В поле Tag каждой закладки забит идентификатор типа документа. Не совсем красивое решение, жестко привязанное к форме.
                else //Указан какой либо из ограничивающих фильтров?
                    mos_doc.Load(id, rbShowReserved.Checked);

                //по указанному идентификатору расходного документа и типа расходного документа. Должно быть соответсвие 
                //индексов списка идентификаторам типов расходных документов
                if (mos_doc.Count != 0) mos.AddRange(mos_doc);//Добавление загруженного списка по очередному выделенному документу в общий список всех расхдных медикаментов 
            }

            gridMeds.DataSource = null;

            //Добавлено в связи с возникновением ошибки вывода данных в форму 03.02.2016 ЗЕФ
            var list = mos.Select(m => new
            {
                m.UnReserved,
                m.MedicamentD,
                m.MedicamentN,
                m.CountOutput,
                m.PriceSum
            });

            gridMeds.DataSource = list.ToList();
        }
        /// <summary>Выбран фильтрующий элемент отображения только зарезервированных медикаментов
        /// Заплатин Е.Ф.
        /// 30.07.2011
        /// </summary>
        private void rbShowReserved_CheckedChanged(object sender, EventArgs e)
        {
            LoadMedicaments();
        }
        /// <summary>Выбран фильтрующий элемент отображения всех медикаментов
        /// Заплатин Е.Ф.
        /// 30.07.2011
        /// </summary>
        private void rbShowAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadMedicaments();
        }
        /// <summary>Выбран фильтрующий элемент отображения только незарезервированных медикаментов
        /// Заплатин Е.Ф.
        /// 30.07.2011
        /// </summary>
        private void rbShowNonReserved_CheckedChanged(object sender, EventArgs e)
        {
            LoadMedicaments();
        }

        /// <summary>
        /// Дасходные документы
        /// </summary>
        private class DocDto
        {
            public int DocumentID { get; set; }
            public string Season { get; set; }
            public string FullName { get; set; }
            public string DoctorName { get; set; }
            public decimal SummMedicament { get; set; }
        }
    }
}
