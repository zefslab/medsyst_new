﻿namespace Medsyst
{
    partial class frmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOptions));
            this.treeOptionsGroup = new System.Windows.Forms.TreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage0 = new System.Windows.Forms.TabPage();
            this.cmbMembers = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEditMedical = new ExButton.NET.ExButton();
            this.btnDeleteMedical = new ExButton.NET.ExButton();
            this.btnAddMedical = new ExButton.NET.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbMedical = new System.Windows.Forms.ComboBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmbCurrentSeason = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEdit = new ExButton.NET.ExButton();
            this.btnAddMedicine = new ExButton.NET.ExButton();
            this.btnDelSeason = new ExButton.NET.ExButton();
            this.gridSeasons = new System.Windows.Forms.DataGridView();
            this.Nbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Open = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.End = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.udCurrentYear = new System.Windows.Forms.NumericUpDown();
            this.intYear = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnRestoreDB = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.btnBackupBD = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteDocuments = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.btnCleanDeleted = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCleanZero = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCompress = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripOk = new System.Windows.Forms.ToolStripButton();
            this.tabControl1.SuspendLayout();
            this.tabPage0.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSeasons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCurrentYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYear)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeOptionsGroup
            // 
            this.treeOptionsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeOptionsGroup.Location = new System.Drawing.Point(4, 26);
            this.treeOptionsGroup.Name = "treeOptionsGroup";
            this.treeOptionsGroup.Size = new System.Drawing.Size(205, 413);
            this.treeOptionsGroup.TabIndex = 0;
            this.treeOptionsGroup.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeOptionsGroup_AfterSelect);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage0);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(212, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(469, 412);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage0
            // 
            this.tabPage0.Controls.Add(this.cmbMembers);
            this.tabPage0.Controls.Add(this.label5);
            this.tabPage0.Controls.Add(this.btnEditMedical);
            this.tabPage0.Controls.Add(this.btnDeleteMedical);
            this.tabPage0.Controls.Add(this.btnAddMedical);
            this.tabPage0.Controls.Add(this.label4);
            this.tabPage0.Controls.Add(this.cmbMedical);
            this.tabPage0.Location = new System.Drawing.Point(4, 22);
            this.tabPage0.Name = "tabPage0";
            this.tabPage0.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage0.Size = new System.Drawing.Size(461, 386);
            this.tabPage0.TabIndex = 0;
            this.tabPage0.Text = "Общие настройки";
            this.tabPage0.UseVisualStyleBackColor = true;
            // 
            // cmbMembers
            // 
            this.cmbMembers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMembers.DisplayMember = "Fullname";
            this.cmbMembers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMembers.FormattingEnabled = true;
            this.cmbMembers.Location = new System.Drawing.Point(6, 79);
            this.cmbMembers.Name = "cmbMembers";
            this.cmbMembers.Size = new System.Drawing.Size(349, 21);
            this.cmbMembers.TabIndex = 68;
            this.cmbMembers.Tag = "set.mainDoctor";
            this.cmbMembers.ValueMember = "PersonID";
            this.cmbMembers.SelectedIndexChanged += new System.EventHandler(this.cmbMembers_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(270, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "Главный врач/Директор санатория-профилактория";
            // 
            // btnEditMedical
            // 
            this.btnEditMedical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditMedical.Image = ((System.Drawing.Image)(resources.GetObject("btnEditMedical.Image")));
            this.btnEditMedical.Location = new System.Drawing.Point(393, 26);
            this.btnEditMedical.Name = "btnEditMedical";
            this.btnEditMedical.Size = new System.Drawing.Size(27, 23);
            this.btnEditMedical.TabIndex = 66;
            this.btnEditMedical.Click += new System.EventHandler(this.btnEditMedical_Click);
            // 
            // btnDeleteMedical
            // 
            this.btnDeleteMedical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteMedical.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteMedical.Image")));
            this.btnDeleteMedical.Location = new System.Drawing.Point(426, 26);
            this.btnDeleteMedical.Name = "btnDeleteMedical";
            this.btnDeleteMedical.Size = new System.Drawing.Size(29, 23);
            this.btnDeleteMedical.TabIndex = 65;
            this.btnDeleteMedical.Click += new System.EventHandler(this.btnDeleteMedical_Click);
            // 
            // btnAddMedical
            // 
            this.btnAddMedical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMedical.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMedical.Image")));
            this.btnAddMedical.Location = new System.Drawing.Point(361, 26);
            this.btnAddMedical.Name = "btnAddMedical";
            this.btnAddMedical.Size = new System.Drawing.Size(26, 23);
            this.btnAddMedical.TabIndex = 64;
            this.btnAddMedical.Click += new System.EventHandler(this.btnAddMedical_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Лечебно-профилактическое заведение";
            // 
            // cmbMedical
            // 
            this.cmbMedical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMedical.DisplayMember = "FirmN";
            this.cmbMedical.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMedical.FormattingEnabled = true;
            this.cmbMedical.Location = new System.Drawing.Point(6, 26);
            this.cmbMedical.Name = "cmbMedical";
            this.cmbMedical.Size = new System.Drawing.Size(349, 21);
            this.cmbMedical.TabIndex = 0;
            this.cmbMedical.Tag = "set.Sanatorium";
            this.cmbMedical.ValueMember = "FirmID";
            this.cmbMedical.SelectedIndexChanged += new System.EventHandler(this.cmbMedical_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.cmbCurrentSeason);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnEdit);
            this.tabPage1.Controls.Add(this.btnAddMedicine);
            this.tabPage1.Controls.Add(this.btnDelSeason);
            this.tabPage1.Controls.Add(this.gridSeasons);
            this.tabPage1.Controls.Add(this.udCurrentYear);
            this.tabPage1.Controls.Add(this.intYear);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(461, 386);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Календарь сезонов";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(6, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(444, 1);
            this.pictureBox1.TabIndex = 69;
            this.pictureBox1.TabStop = false;
            // 
            // cmbCurrentSeason
            // 
            this.cmbCurrentSeason.DisplayMember = "SeasonNbr";
            this.cmbCurrentSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCurrentSeason.FormattingEnabled = true;
            this.cmbCurrentSeason.Location = new System.Drawing.Point(303, 12);
            this.cmbCurrentSeason.Name = "cmbCurrentSeason";
            this.cmbCurrentSeason.Size = new System.Drawing.Size(121, 21);
            this.cmbCurrentSeason.TabIndex = 68;
            this.cmbCurrentSeason.Tag = "set.CurrentSeason";
            this.cmbCurrentSeason.ValueMember = "SeasonID";
            this.cmbCurrentSeason.SelectedIndexChanged += new System.EventHandler(this.cmbCurrentSeason_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(211, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 66;
            this.label3.Text = "Текущий сезон";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 66;
            this.label2.Text = "Текущий год";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(381, 107);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(77, 23);
            this.btnEdit.TabIndex = 65;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddMedicine
            // 
            this.btnAddMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMedicine.Image")));
            this.btnAddMedicine.Location = new System.Drawing.Point(381, 78);
            this.btnAddMedicine.Name = "btnAddMedicine";
            this.btnAddMedicine.Size = new System.Drawing.Size(77, 23);
            this.btnAddMedicine.TabIndex = 64;
            this.btnAddMedicine.Text = "Добавить";
            this.btnAddMedicine.Click += new System.EventHandler(this.btnAddMedicine_Click);
            // 
            // btnDelSeason
            // 
            this.btnDelSeason.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelSeason.Image = ((System.Drawing.Image)(resources.GetObject("btnDelSeason.Image")));
            this.btnDelSeason.Location = new System.Drawing.Point(381, 136);
            this.btnDelSeason.Name = "btnDelSeason";
            this.btnDelSeason.Size = new System.Drawing.Size(77, 23);
            this.btnDelSeason.TabIndex = 63;
            this.btnDelSeason.Text = "Удалить";
            this.btnDelSeason.Click += new System.EventHandler(this.btnDelSeason_Click);
            // 
            // gridSeasons
            // 
            this.gridSeasons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSeasons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSeasons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nbr,
            this.Open,
            this.End,
            this.ID});
            this.gridSeasons.Location = new System.Drawing.Point(6, 78);
            this.gridSeasons.MultiSelect = false;
            this.gridSeasons.Name = "gridSeasons";
            this.gridSeasons.RowHeadersVisible = false;
            this.gridSeasons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSeasons.Size = new System.Drawing.Size(369, 302);
            this.gridSeasons.TabIndex = 62;
            this.gridSeasons.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSeasons_CellDoubleClick);
            // 
            // Nbr
            // 
            this.Nbr.DataPropertyName = "SeasonNbr";
            this.Nbr.HeaderText = "№ сезона";
            this.Nbr.Name = "Nbr";
            this.Nbr.ReadOnly = true;
            // 
            // Open
            // 
            this.Open.DataPropertyName = "SeasonOpen";
            this.Open.HeaderText = "Начало";
            this.Open.Name = "Open";
            this.Open.ReadOnly = true;
            // 
            // End
            // 
            this.End.DataPropertyName = "SeasonClose";
            this.End.HeaderText = "Окончание";
            this.End.Name = "End";
            this.End.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "SeasonID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // udCurrentYear
            // 
            this.udCurrentYear.Location = new System.Drawing.Point(85, 14);
            this.udCurrentYear.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.udCurrentYear.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.udCurrentYear.Name = "udCurrentYear";
            this.udCurrentYear.Size = new System.Drawing.Size(120, 20);
            this.udCurrentYear.TabIndex = 2;
            this.udCurrentYear.Tag = "set.CurrentYear";
            this.udCurrentYear.Value = new decimal(new int[] {
            2012,
            0,
            0,
            0});
            this.udCurrentYear.ValueChanged += new System.EventHandler(this.udCurrentYear_ValueChanged);
            // 
            // intYear
            // 
            this.intYear.Location = new System.Drawing.Point(37, 52);
            this.intYear.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.intYear.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.intYear.Name = "intYear";
            this.intYear.Size = new System.Drawing.Size(120, 20);
            this.intYear.TabIndex = 2;
            this.intYear.Value = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.intYear.ValueChanged += new System.EventHandler(this.intYear_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Год";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnRestoreDB);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.btnBackupBD);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.btnCompress);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(461, 386);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Обслуживание базы данных";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnRestoreDB
            // 
            this.btnRestoreDB.Location = new System.Drawing.Point(427, 354);
            this.btnRestoreDB.Name = "btnRestoreDB";
            this.btnRestoreDB.Size = new System.Drawing.Size(31, 23);
            this.btnRestoreDB.TabIndex = 19;
            this.btnRestoreDB.Text = ">>";
            this.btnRestoreDB.UseVisualStyleBackColor = true;
            this.btnRestoreDB.Click += new System.EventHandler(this.btnRestoreDB_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(225, 362);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(200, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Восстановить резервную копию базы";
            // 
            // btnBackupBD
            // 
            this.btnBackupBD.Location = new System.Drawing.Point(188, 354);
            this.btnBackupBD.Name = "btnBackupBD";
            this.btnBackupBD.Size = new System.Drawing.Size(31, 23);
            this.btnBackupBD.TabIndex = 17;
            this.btnBackupBD.Text = ">>";
            this.btnBackupBD.UseVisualStyleBackColor = true;
            this.btnBackupBD.Click += new System.EventHandler(this.btnBackupBD_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 362);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(171, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Создать резервную копию базы";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(11, 243);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(444, 80);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Пациенты";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDeleteDocuments);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(9, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(444, 107);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Документы";
            // 
            // btnDeleteDocuments
            // 
            this.btnDeleteDocuments.Enabled = false;
            this.btnDeleteDocuments.Location = new System.Drawing.Point(289, 15);
            this.btnDeleteDocuments.Name = "btnDeleteDocuments";
            this.btnDeleteDocuments.Size = new System.Drawing.Size(31, 23);
            this.btnDeleteDocuments.TabIndex = 3;
            this.btnDeleteDocuments.Text = ">>";
            this.btnDeleteDocuments.UseVisualStyleBackColor = true;
            this.btnDeleteDocuments.Click += new System.EventHandler(this.btnDeleteDocuments_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(187, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Чистить все удаленные документы";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.btnCleanDeleted);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.btnCleanZero);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(9, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 118);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Назначения";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(289, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = ">>";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(168, 87);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(115, 20);
            this.dateTimePicker2.TabIndex = 12;
            // 
            // btnCleanDeleted
            // 
            this.btnCleanDeleted.Location = new System.Drawing.Point(289, 15);
            this.btnCleanDeleted.Name = "btnCleanDeleted";
            this.btnCleanDeleted.Size = new System.Drawing.Size(31, 23);
            this.btnCleanDeleted.TabIndex = 0;
            this.btnCleanDeleted.Text = ">>";
            this.btnCleanDeleted.UseVisualStyleBackColor = true;
            this.btnCleanDeleted.Click += new System.EventHandler(this.btnCleanDeleted_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(31, 87);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(115, 20);
            this.dateTimePicker1.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(213, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Очистить базу от удаленных назначений";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(149, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "по";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(238, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Удалить назначения с нулевым количеством";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "с";
            // 
            // btnCleanZero
            // 
            this.btnCleanZero.Location = new System.Drawing.Point(289, 40);
            this.btnCleanZero.Name = "btnCleanZero";
            this.btnCleanZero.Size = new System.Drawing.Size(31, 23);
            this.btnCleanZero.TabIndex = 6;
            this.btnCleanZero.Text = ">>";
            this.btnCleanZero.UseVisualStyleBackColor = true;
            this.btnCleanZero.Click += new System.EventHandler(this.btnCleanZero_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(172, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Удалить назначения  за период:";
            // 
            // btnCompress
            // 
            this.btnCompress.Location = new System.Drawing.Point(188, 329);
            this.btnCompress.Name = "btnCompress";
            this.btnCompress.Size = new System.Drawing.Size(31, 23);
            this.btnCompress.TabIndex = 4;
            this.btnCompress.Text = ">>";
            this.btnCompress.UseVisualStyleBackColor = true;
            this.btnCompress.Click += new System.EventHandler(this.btnCompress_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Сжать базу данных";
            // 
            // btnSelect
            // 
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(71, 22);
            this.btnSelect.Text = "Выбрать";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripOk});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(681, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripOk
            // 
            this.toolStripOk.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOk.Image")));
            this.toolStripOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOk.Name = "toolStripOk";
            this.toolStripOk.Size = new System.Drawing.Size(65, 22);
            this.toolStripOk.Text = "Готово";
            this.toolStripOk.Click += new System.EventHandler(this.toolStripOk_Click);
            // 
            // frmOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 437);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.treeOptionsGroup);
            this.Name = "frmOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки системы";
            this.Load += new System.EventHandler(this.frmOptions_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage0.ResumeLayout(false);
            this.tabPage0.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSeasons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCurrentYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYear)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeOptionsGroup;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage0;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown intYear;
        private ExButton.NET.ExButton btnAddMedicine;
        private ExButton.NET.ExButton btnDelSeason;
        private System.Windows.Forms.DataGridView gridSeasons;
        private ExButton.NET.ExButton btnEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udCurrentYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCurrentSeason;
        private System.Windows.Forms.ToolStripButton btnSelect;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripOk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbMedical;
        public ExButton.NET.ExButton btnAddMedical;
        public ExButton.NET.ExButton btnDeleteMedical;
        private ExButton.NET.ExButton btnEditMedical;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Open;
        private System.Windows.Forms.DataGridViewTextBoxColumn End;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.ComboBox cmbMembers;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCleanDeleted;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCleanZero;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCompress;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeleteDocuments;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnRestoreDB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnBackupBD;
        private System.Windows.Forms.Label label13;
    }
}