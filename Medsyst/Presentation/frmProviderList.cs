﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmProviderList : Form
    {
        ProviderList providers = null;

        public frmProviderList()
        {        
            InitializeComponent();
        }

 
        // Добавление провайдера
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmProvider frm = new frmProvider();
            frm.Tag = new Command(CommandModes.New, this);
            frm.ShowDialog();
            LoadProviders();
        }

        // Изменение провайдера
        private void btnChange_Click(object sender, EventArgs e)
        {
            if (gridProviders.SelectedRows == null || gridProviders.SelectedRows.Count <= 0) return;
            frmProvider frm = new frmProvider();
            frm.Tag = new Command(CommandModes.Edit,providers[gridProviders.SelectedRows[0].Index].FirmID);
            frm.ShowDialog();
            LoadProviders();
        }

  
        private void ProviderList_Load(object sender, EventArgs e)
        {
            tbSelect.Visible = cmd.Mode == CommandModes.Select;
            LoadProviders();
        }

        void LoadProviders()
        {
            gridProviders.AutoGenerateColumns = false;
            gridProviders.DataSource = null;
            providers = new ProviderList();
            providers.dbWhere = "Deleted = 0";
            providers.Load();
            gridProviders.DataSource = providers;
        }

        private void tbSelect_Click(object sender, EventArgs e)
        {
            if (gridProviders.SelectedRows != null && gridProviders.SelectedRows.Count > 0)
            {
                //Tag = providers[gridProviders.SelectedRows[0].Index].FirmID;
                Tag = new Command(CommandModes.Return, providers[gridProviders.SelectedRows[0].Index].FirmID);
                Close();

            }
        }

        private void gridProviders_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridProviders.SelectedRows != null && gridProviders.SelectedRows.Count > 0)
            {

                if (cmd.Mode == CommandModes.Select)
                {
                    Tag = new Command(CommandModes.Return, providers[gridProviders.SelectedRows[0].Index].FirmID);
                    Close();
                }
                else
                {
                    int id = providers[gridProviders.SelectedRows[0].Index].FirmID;
                    frmProvider frm = new frmProvider();
                    frm.Tag = new Command(CommandModes.Edit, id);
                    frm.ShowDialog(this);
                    LoadProviders();
                }
            }
        }

 
        // Удаление провайдера
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridProviders.SelectedRows == null || gridProviders.SelectedRows.Count <= 0) return;
            Provider p = new Provider(true);
            p.FirmID = providers[gridProviders.SelectedRows[0].Index].FirmID;
            p.Load();
            p.Deleted = true;//Фирма-поставщик не удаляется, а лишь ставится признак ее удаления
            p.Update();
            LoadProviders();
        }

        //11.04.10
        //Чирков Е.О.
        private void gridProviders_SelectionChanged(object sender, EventArgs e)
        {
            lbContacts.Items.Clear();
            rtbDetails.Text = "";
            if (gridProviders.SelectedRows.Count == 0) return;
            int i = gridProviders.SelectedRows[0].Index;
            FirmContacts fcs = new FirmContacts();
            fcs.dbWhere = "FirmID=" + providers[i].FirmID;
            fcs.Load();
            string s = "";
            foreach (FirmContact fc in fcs)
            {
                PersonContact p = new PersonContact(true);
                p.PersonID = fc.PersonID;
                p.Load();

                s = p.LastName + " " + p.Name.Substring(0, 1) + "." + p.SurName.Substring(0, 1) + ".";
                if (p.PhoneWork != "") s += ", раб.:" + p.PhoneWork;
                if (p.PhoneMobile != "") s += ", сот.:" + p.PhoneMobile;
                if (p.PhoneHome != "") s += ", дом.:" + p.PhoneHome;

                lbContacts.Items.Add(s);
            }
            BankDetails bd = new BankDetails();
            bd.BankDetailsID = providers[i].BankDetailsID;
            bd.Load();

            s = providers[i].FirmN + "\n";
            s += "Юр. адрес: " + bd.Adress + "\n";
            s += "Телефон: " + providers[i].Phone + "\n";
            s += "Реквизиты:\n";
            s += "ИНН " + bd.INN + " КПП " + bd.KPP + "\n";
            s += "кор/сч " + bd.KorS + "\n";
            s += "р/сч " + bd.RS + "\n";
            s += bd.BankName + "\n";
            s += "БИК " + bd.BIK + "\n";
            s += "ОКОХН " + bd.OKOHN + " ОКПО " + bd.OKPO + "\n";
            s += "Почтовый адрес: ";
            Address addr = new Address(true);
            addr.AddressID = providers[i].AdressID;
            addr.Load();
            City c = new City(true);
            c.CityID = addr.CityID;
            c.Load();
            s += "г." + c.CityN + ", " + addr.Location + "\n";
            rtbDetails.Text = s;
        }

 
    }
}
