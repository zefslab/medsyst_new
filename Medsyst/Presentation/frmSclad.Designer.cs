﻿namespace Medsyst
{
    partial class frmSclad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSclad));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.chkNoBudget = new System.Windows.Forms.CheckBox();
            this.chkBudget = new System.Windows.Forms.CheckBox();
            this.chkZeroVisible = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new Medsyst.Class.Core.RegularTextBox();
            this.textBox1 = new Medsyst.Class.Core.RegularTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDetailsInput = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRODate = new System.Windows.Forms.TextBox();
            this.txtRONumber = new System.Windows.Forms.TextBox();
            this.btnGotoRecieptOrder = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMedComment = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.tsbSelectAndClose = new System.Windows.Forms.ToolStripButton();
            this.btnSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.tsbtnClose = new System.Windows.Forms.ToolStripButton();
            this.colMedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedicamentOnScladID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMedicamentN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(910, 414);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.chkNoBudget);
            this.splitContainer2.Panel2.Controls.Add(this.chkBudget);
            this.splitContainer2.Panel2.Controls.Add(this.chkZeroVisible);
            this.splitContainer2.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer2.Size = new System.Drawing.Size(185, 601);
            this.splitContainer2.SplitterDistance = 307;
            this.splitContainer2.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(181, 303);
            this.treeView1.TabIndex = 1;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // chkNoBudget
            // 
            this.chkNoBudget.AutoSize = true;
            this.chkNoBudget.Checked = true;
            this.chkNoBudget.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNoBudget.Location = new System.Drawing.Point(5, 53);
            this.chkNoBudget.Name = "chkNoBudget";
            this.chkNoBudget.Size = new System.Drawing.Size(155, 17);
            this.chkNoBudget.TabIndex = 19;
            this.chkNoBudget.Text = "Показать внебюджетные";
            this.chkNoBudget.UseVisualStyleBackColor = true;
            this.chkNoBudget.CheckedChanged += new System.EventHandler(this.chkNoBudget_CheckedChanged);
            // 
            // chkBudget
            // 
            this.chkBudget.AutoSize = true;
            this.chkBudget.Checked = true;
            this.chkBudget.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBudget.Location = new System.Drawing.Point(5, 32);
            this.chkBudget.Name = "chkBudget";
            this.chkBudget.Size = new System.Drawing.Size(137, 17);
            this.chkBudget.TabIndex = 18;
            this.chkBudget.Text = "Показать бюджетные";
            this.chkBudget.UseVisualStyleBackColor = true;
            this.chkBudget.CheckedChanged += new System.EventHandler(this.chkBudget_CheckedChanged);
            // 
            // chkZeroVisible
            // 
            this.chkZeroVisible.AutoSize = true;
            this.chkZeroVisible.Location = new System.Drawing.Point(5, 3);
            this.chkZeroVisible.Name = "chkZeroVisible";
            this.chkZeroVisible.Size = new System.Drawing.Size(169, 17);
            this.chkZeroVisible.TabIndex = 17;
            this.chkZeroVisible.Text = "Показать израсходованные";
            this.chkZeroVisible.UseVisualStyleBackColor = true;
            this.chkZeroVisible.CheckedChanged += new System.EventHandler(this.chkZeroVisible_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Location = new System.Drawing.Point(5, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 178);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // textBox2
            // 
            this.textBox2.Filter = Medsyst.Class.Core.FilterType.Mony;
            this.textBox2.Location = new System.Drawing.Point(101, 31);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(42, 20);
            this.textBox2.TabIndex = 21;
            this.textBox2.Text = "1000";
            this.textBox2.Value = 1000D;
            // 
            // textBox1
            // 
            this.textBox1.Filter = Medsyst.Class.Core.FilterType.Mony;
            this.textBox1.Location = new System.Drawing.Point(32, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 20);
            this.textBox1.TabIndex = 20;
            this.textBox1.Text = "0";
            this.textBox1.Value = 0D;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Цена:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(141, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "руб.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "до";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Отменить фильтр";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(12, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "по";
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.Location = new System.Drawing.Point(16, 120);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(152, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Применить фильтр";
            this.button5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(32, 68);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "от";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(6, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Дата приходования:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(13, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "с";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(32, 94);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker2.TabIndex = 13;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer5);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1030, 601);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1030, 626);
            this.toolStripContainer1.TabIndex = 3;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip3);
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(1030, 601);
            this.splitContainer5.SplitterDistance = 185;
            this.splitContainer5.TabIndex = 12;
            // 
            // splitContainer6
            // 
            this.splitContainer6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer6.IsSplitterFixed = true;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.gridMeds);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.panel2);
            this.splitContainer6.Size = new System.Drawing.Size(841, 601);
            this.splitContainer6.SplitterDistance = 644;
            this.splitContainer6.TabIndex = 0;
            // 
            // gridMeds
            // 
            this.gridMeds.AllowUserToAddRows = false;
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMedID,
            this.colMedicamentOnScladID,
            this.colMedicamentN,
            this.Column6,
            this.Column3,
            this.Column4,
            this.Column5});
            this.gridMeds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMeds.Location = new System.Drawing.Point(0, 0);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            this.gridMeds.ReadOnly = true;
            this.gridMeds.RowHeadersVisible = false;
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(640, 597);
            this.gridMeds.TabIndex = 1;
            this.gridMeds.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellClick);
            this.gridMeds.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellDoubleClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.txtMedComment);
            this.panel2.Controls.Add(this.textBox3);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(189, 597);
            this.panel2.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(3, 247);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 17);
            this.label13.TabIndex = 12;
            this.label13.Text = "Рецепт типовой";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDetailsInput);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtRODate);
            this.groupBox2.Controls.Add(this.txtRONumber);
            this.groupBox2.Controls.Add(this.btnGotoRecieptOrder);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 134);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Краткие сведения о поставке лек. ср-ва";
            // 
            // btnDetailsInput
            // 
            this.btnDetailsInput.Location = new System.Drawing.Point(6, 27);
            this.btnDetailsInput.Name = "btnDetailsInput";
            this.btnDetailsInput.Size = new System.Drawing.Size(174, 23);
            this.btnDetailsInput.TabIndex = 10;
            this.btnDetailsInput.Text = "Показать детали";
            this.btnDetailsInput.UseVisualStyleBackColor = true;
            this.btnDetailsInput.Click += new System.EventHandler(this.btnDetailsInput_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Дата поставки";
            // 
            // txtRODate
            // 
            this.txtRODate.Location = new System.Drawing.Point(95, 80);
            this.txtRODate.Name = "txtRODate";
            this.txtRODate.Size = new System.Drawing.Size(85, 20);
            this.txtRODate.TabIndex = 8;
            this.txtRODate.Tag = "ro.RegisterD";
            // 
            // txtRONumber
            // 
            this.txtRONumber.Location = new System.Drawing.Point(115, 56);
            this.txtRONumber.Name = "txtRONumber";
            this.txtRONumber.Size = new System.Drawing.Size(64, 20);
            this.txtRONumber.TabIndex = 0;
            this.txtRONumber.Tag = "ro.Nbr";
            // 
            // btnGotoRecieptOrder
            // 
            this.btnGotoRecieptOrder.Image = ((System.Drawing.Image)(resources.GetObject("btnGotoRecieptOrder.Image")));
            this.btnGotoRecieptOrder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnGotoRecieptOrder.Location = new System.Drawing.Point(3, 106);
            this.btnGotoRecieptOrder.Name = "btnGotoRecieptOrder";
            this.btnGotoRecieptOrder.Size = new System.Drawing.Size(182, 23);
            this.btnGotoRecieptOrder.TabIndex = 6;
            this.btnGotoRecieptOrder.Text = "Перейти к приходному ордеру";
            this.btnGotoRecieptOrder.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnGotoRecieptOrder.UseVisualStyleBackColor = true;
            this.btnGotoRecieptOrder.Click += new System.EventHandler(this.btnGotoRecieptOrder_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(98, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "№";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Приходный ордер";
            // 
            // txtMedComment
            // 
            this.txtMedComment.Location = new System.Drawing.Point(5, 162);
            this.txtMedComment.Multiline = true;
            this.txtMedComment.Name = "txtMedComment";
            this.txtMedComment.ReadOnly = true;
            this.txtMedComment.Size = new System.Drawing.Size(187, 82);
            this.txtMedComment.TabIndex = 2;
            this.txtMedComment.Tag = "medicament.CommentInput";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(5, 267);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(187, 142);
            this.textBox3.TabIndex = 11;
            this.textBox3.Tag = "medicament.Comment";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Примечание к поставке";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 3;
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelectAndClose,
            this.btnSelect,
            this.toolStripButton1,
            this.toolStripTextBox1,
            this.toolStripButton8,
            this.tsbtnClose});
            this.toolStrip3.Location = new System.Drawing.Point(3, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(446, 25);
            this.toolStrip3.TabIndex = 3;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // tsbSelectAndClose
            // 
            this.tsbSelectAndClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelectAndClose.Image")));
            this.tsbSelectAndClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelectAndClose.Name = "tsbSelectAndClose";
            this.tsbSelectAndClose.Size = new System.Drawing.Size(131, 22);
            this.tsbSelectAndClose.Text = "Выбрать и закрыть";
            this.tsbSelectAndClose.Click += new System.EventHandler(this.tsbSelectAndClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(74, 22);
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.Click += new System.EventHandler(this.tsbSelect_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(66, 22);
            this.toolStripButton1.Text = "Печать";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(61, 22);
            this.toolStripButton8.Text = "Найти";
            // 
            // tsbtnClose
            // 
            this.tsbtnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnClose.Image")));
            this.tsbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClose.Name = "tsbtnClose";
            this.tsbtnClose.Size = new System.Drawing.Size(57, 22);
            this.tsbtnClose.Text = "Закрыть";
            this.tsbtnClose.Visible = false;
            this.tsbtnClose.Click += new System.EventHandler(this.tsbtnClose_Click);
            // 
            // colMedID
            // 
            this.colMedID.DataPropertyName = "MedicamentID";
            this.colMedID.HeaderText = "ID";
            this.colMedID.Name = "colMedID";
            this.colMedID.ReadOnly = true;
            this.colMedID.Visible = false;
            // 
            // colMedicamentOnScladID
            // 
            this.colMedicamentOnScladID.DataPropertyName = "MedicamentOnScladID";
            this.colMedicamentOnScladID.HeaderText = "№";
            this.colMedicamentOnScladID.Name = "colMedicamentOnScladID";
            this.colMedicamentOnScladID.ReadOnly = true;
            this.colMedicamentOnScladID.Width = 60;
            // 
            // colMedicamentN
            // 
            this.colMedicamentN.DataPropertyName = "MedicamentN";
            this.colMedicamentN.HeaderText = "Наименование";
            this.colMedicamentN.Name = "colMedicamentN";
            this.colMedicamentN.ReadOnly = true;
            this.colMedicamentN.Width = 300;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Price";
            this.Column6.HeaderText = "Цена";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 60;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CountOnSclad";
            this.Column3.HeaderText = "На складе";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 85;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "CountReserve";
            this.Column4.HeaderText = "В резерве";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "CountDivide";
            this.Column5.HeaderText = "Остаток";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 60;
            // 
            // frmSclad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1030, 626);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmSclad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Склад лекарственных средств";
            this.Load += new System.EventHandler(this.Sclad_Load);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMedComment;
        private System.Windows.Forms.TextBox txtRONumber;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btnSelect;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnGotoRecieptOrder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRODate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.CheckBox chkNoBudget;
        private System.Windows.Forms.CheckBox chkBudget;
        private System.Windows.Forms.CheckBox chkZeroVisible;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDetailsInput;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private Medsyst.Class.Core.RegularTextBox textBox2;
        private Medsyst.Class.Core.RegularTextBox textBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ToolStripButton tsbSelectAndClose;
        private System.Windows.Forms.ToolStripButton tsbtnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedicamentOnScladID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedicamentN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}