﻿namespace Medsyst
{
    partial class rptMedicamentCharges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkResidue = new System.Windows.Forms.CheckBox();
            this.chkCheckit = new System.Windows.Forms.CheckBox();
            this.btnExecut = new System.Windows.Forms.Button();
            this.rbtnNoBudget = new System.Windows.Forms.RadioButton();
            this.rbtnBudget = new System.Windows.Forms.RadioButton();
            this.dtpEndPeriod = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginPeriod = new System.Windows.Forms.DateTimePicker();
            this.crystalReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.medicamentCharges = new Medsyst.DataSet.MedicamentCharges();
            this.rptMedicamentCharges1 = new Medsyst.Reports.MedicamentCharges();
            this.chlbCategory = new System.Windows.Forms.CheckedListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medicamentCharges)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.chlbCategory);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.chkResidue);
            this.splitContainer1.Panel1.Controls.Add(this.chkCheckit);
            this.splitContainer1.Panel1.Controls.Add(this.btnExecut);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnNoBudget);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnBudget);
            this.splitContainer1.Panel1.Controls.Add(this.dtpEndPeriod);
            this.splitContainer1.Panel1.Controls.Add(this.dtpBeginPeriod);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.crystalReportViewer);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 685);
            this.splitContainer1.SplitterDistance = 173;
            this.splitContainer1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "с";
            // 
            // chkResidue
            // 
            this.chkResidue.AutoSize = true;
            this.chkResidue.Location = new System.Drawing.Point(11, 230);
            this.chkResidue.Name = "chkResidue";
            this.chkResidue.Size = new System.Drawing.Size(102, 17);
            this.chkResidue.TabIndex = 21;
            this.chkResidue.Text = "С остатком > 0";
            this.chkResidue.UseVisualStyleBackColor = true;
            // 
            // chkCheckit
            // 
            this.chkCheckit.AutoSize = true;
            this.chkCheckit.Enabled = false;
            this.chkCheckit.Location = new System.Drawing.Point(11, 292);
            this.chkCheckit.Name = "chkCheckit";
            this.chkCheckit.Size = new System.Drawing.Size(152, 17);
            this.chkCheckit.TabIndex = 20;
            this.chkCheckit.Text = "Показать поле контроля";
            this.chkCheckit.UseVisualStyleBackColor = true;
            this.chkCheckit.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btnExecut
            // 
            this.btnExecut.Location = new System.Drawing.Point(8, 253);
            this.btnExecut.Name = "btnExecut";
            this.btnExecut.Size = new System.Drawing.Size(155, 23);
            this.btnExecut.TabIndex = 4;
            this.btnExecut.Text = "Гененрировать отчет";
            this.btnExecut.UseVisualStyleBackColor = true;
            this.btnExecut.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbtnNoBudget
            // 
            this.rbtnNoBudget.AutoSize = true;
            this.rbtnNoBudget.Location = new System.Drawing.Point(11, 207);
            this.rbtnNoBudget.Name = "rbtnNoBudget";
            this.rbtnNoBudget.Size = new System.Drawing.Size(83, 17);
            this.rbtnNoBudget.TabIndex = 3;
            this.rbtnNoBudget.Text = "Внебюджет";
            this.rbtnNoBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnBudget
            // 
            this.rbtnBudget.AutoSize = true;
            this.rbtnBudget.Checked = true;
            this.rbtnBudget.Location = new System.Drawing.Point(11, 184);
            this.rbtnBudget.Name = "rbtnBudget";
            this.rbtnBudget.Size = new System.Drawing.Size(65, 17);
            this.rbtnBudget.TabIndex = 2;
            this.rbtnBudget.TabStop = true;
            this.rbtnBudget.Text = "Бюджет";
            this.rbtnBudget.UseVisualStyleBackColor = true;
            // 
            // dtpEndPeriod
            // 
            this.dtpEndPeriod.Location = new System.Drawing.Point(27, 35);
            this.dtpEndPeriod.Name = "dtpEndPeriod";
            this.dtpEndPeriod.Size = new System.Drawing.Size(139, 20);
            this.dtpEndPeriod.TabIndex = 1;
            this.dtpEndPeriod.Value = new System.DateTime(2011, 2, 28, 23, 59, 0, 0);
            // 
            // dtpBeginPeriod
            // 
            this.dtpBeginPeriod.Location = new System.Drawing.Point(27, 9);
            this.dtpBeginPeriod.Name = "dtpBeginPeriod";
            this.dtpBeginPeriod.Size = new System.Drawing.Size(139, 20);
            this.dtpBeginPeriod.TabIndex = 0;
            this.dtpBeginPeriod.Value = new System.DateTime(2011, 1, 1, 17, 57, 0, 0);
            // 
            // crystalReportViewer
            // 
            this.crystalReportViewer.ActiveViewIndex = -1;
            this.crystalReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer.Name = "crystalReportViewer";
            this.crystalReportViewer.Size = new System.Drawing.Size(843, 685);
            this.crystalReportViewer.TabIndex = 0;
            this.crystalReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // medicamentCharges
            // 
            this.medicamentCharges.DataSetName = "MedicamentCharges";
            this.medicamentCharges.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chlbCategory
            // 
            this.chlbCategory.CheckOnClick = true;
            this.chlbCategory.FormattingEnabled = true;
            this.chlbCategory.Location = new System.Drawing.Point(11, 84);
            this.chlbCategory.Name = "chlbCategory";
            this.chlbCategory.Size = new System.Drawing.Size(154, 94);
            this.chlbCategory.TabIndex = 79;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 80;
            this.label9.Text = "Категории пациентов";
            // 
            // rptMedicamentCharges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 685);
            this.Controls.Add(this.splitContainer1);
            this.Name = "rptMedicamentCharges";
            this.Text = "Приход и расход медикаментов за отчетный период";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.rptMedicamentCharges_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.medicamentCharges)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer;
        private Medsyst.DataSet.MedicamentCharges medicamentCharges;
        private Medsyst.Reports.MedicamentCharges rptMedicamentCharges1;
        private System.Windows.Forms.RadioButton rbtnNoBudget;
        private System.Windows.Forms.RadioButton rbtnBudget;
        private System.Windows.Forms.DateTimePicker dtpEndPeriod;
        private System.Windows.Forms.DateTimePicker dtpBeginPeriod;
        private System.Windows.Forms.Button btnExecut;
        private System.Windows.Forms.CheckBox chkCheckit;
        private System.Windows.Forms.CheckBox chkResidue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chlbCategory;
        private System.Windows.Forms.Label label9;

    }
}