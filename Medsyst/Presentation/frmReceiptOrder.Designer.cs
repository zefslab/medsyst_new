﻿namespace Medsyst
{
    partial class frmReceiptOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmbProvider = new System.Windows.Forms.ComboBox();
            this.btnAdd_Medicine = new ExButton.NET.ExButton();
            this.btnDel_Medicine = new ExButton.NET.ExButton();
            this.rbNotBudget = new System.Windows.Forms.RadioButton();
            this.rbBudget = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAllPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCommentMedicament = new System.Windows.Forms.TextBox();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnit = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNbr = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpAccountD = new System.Windows.Forms.DateTimePicker();
            this.btnSelectProvider = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSign = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.tbPrint = new System.Windows.Forms.ToolStripButton();
            this.tbNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(840, 494);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(840, 519);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.splitContainer1.Location = new System.Drawing.Point(0, -1);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmbProvider);
            this.splitContainer1.Panel1.Controls.Add(this.btnAdd_Medicine);
            this.splitContainer1.Panel1.Controls.Add(this.btnDel_Medicine);
            this.splitContainer1.Panel1.Controls.Add(this.rbNotBudget);
            this.splitContainer1.Panel1.Controls.Add(this.rbBudget);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.txtAllPrice);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.txtCommentMedicament);
            this.splitContainer1.Panel1.Controls.Add(this.gridMeds);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.txtNbr);
            this.splitContainer1.Panel1.Controls.Add(this.dtpDate);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.dtpAccountD);
            this.splitContainer1.Panel1.Controls.Add(this.btnSelectProvider);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.txtAccount);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.txtComment);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(841, 462);
            this.splitContainer1.SplitterDistance = 355;
            this.splitContainer1.TabIndex = 40;
            // 
            // cmbProvider
            // 
            this.cmbProvider.DisplayMember = "FirmN";
            this.cmbProvider.FormattingEnabled = true;
            this.cmbProvider.Location = new System.Drawing.Point(83, 44);
            this.cmbProvider.Name = "cmbProvider";
            this.cmbProvider.Size = new System.Drawing.Size(491, 21);
            this.cmbProvider.TabIndex = 50;
            this.cmbProvider.Tag = "ro.ProviderID";
            this.cmbProvider.ValueMember = "FirmID";
            // 
            // btnAdd_Medicine
            // 
            this.btnAdd_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd_Medicine.Image")));
            this.btnAdd_Medicine.Location = new System.Drawing.Point(667, 88);
            this.btnAdd_Medicine.Name = "btnAdd_Medicine";
            this.btnAdd_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnAdd_Medicine.TabIndex = 49;
            this.btnAdd_Medicine.Text = "Добавить";
            this.btnAdd_Medicine.Click += new System.EventHandler(this.Add_Medicine_Click);
            // 
            // btnDel_Medicine
            // 
            this.btnDel_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnDel_Medicine.Image")));
            this.btnDel_Medicine.Location = new System.Drawing.Point(754, 88);
            this.btnDel_Medicine.Name = "btnDel_Medicine";
            this.btnDel_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnDel_Medicine.TabIndex = 48;
            this.btnDel_Medicine.Text = "Удалить";
            this.btnDel_Medicine.Click += new System.EventHandler(this.Del_Medicine_Click);
            // 
            // rbNotBudget
            // 
            this.rbNotBudget.AutoSize = true;
            this.rbNotBudget.Location = new System.Drawing.Point(230, 70);
            this.rbNotBudget.Name = "rbNotBudget";
            this.rbNotBudget.Size = new System.Drawing.Size(82, 17);
            this.rbNotBudget.TabIndex = 45;
            this.rbNotBudget.TabStop = true;
            this.rbNotBudget.Text = "внебюджет";
            this.rbNotBudget.UseVisualStyleBackColor = true;
            // 
            // rbBudget
            // 
            this.rbBudget.AutoSize = true;
            this.rbBudget.Checked = true;
            this.rbBudget.Location = new System.Drawing.Point(160, 70);
            this.rbBudget.Name = "rbBudget";
            this.rbBudget.Size = new System.Drawing.Size(64, 17);
            this.rbBudget.TabIndex = 44;
            this.rbBudget.TabStop = true;
            this.rbBudget.Tag = "ro.IsBudget";
            this.rbBudget.Text = "бюджет";
            this.rbBudget.UseVisualStyleBackColor = true;
            this.rbBudget.CheckedChanged += new System.EventHandler(this.rbBudget_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "Источник финансирования:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(624, 284);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Итого";
            // 
            // txtAllPrice
            // 
            this.txtAllPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAllPrice.Location = new System.Drawing.Point(667, 281);
            this.txtAllPrice.Name = "txtAllPrice";
            this.txtAllPrice.ReadOnly = true;
            this.txtAllPrice.Size = new System.Drawing.Size(168, 20);
            this.txtAllPrice.TabIndex = 41;
            this.txtAllPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 284);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Комментарий к товару";
            // 
            // txtCommentMedicament
            // 
            this.txtCommentMedicament.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommentMedicament.Location = new System.Drawing.Point(6, 307);
            this.txtCommentMedicament.Multiline = true;
            this.txtCommentMedicament.Name = "txtCommentMedicament";
            this.txtCommentMedicament.Size = new System.Drawing.Size(829, 36);
            this.txtCommentMedicament.TabIndex = 38;
            this.txtCommentMedicament.Leave += new System.EventHandler(this.textBox3_Leave);
            // 
            // gridMeds
            // 
            this.gridMeds.AllowUserToAddRows = false;
            this.gridMeds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.colUnit,
            this.colCount,
            this.Column3,
            this.colSum});
            this.gridMeds.Location = new System.Drawing.Point(10, 117);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            this.gridMeds.RowHeadersVisible = false;
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(825, 156);
            this.gridMeds.TabIndex = 36;
            this.gridMeds.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellClick);
            this.gridMeds.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "MedicamentInputID";
            this.Column1.HeaderText = "№ прих.";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "MedicamentN";
            this.Column2.HeaderText = "Наименование";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // colUnit
            // 
            this.colUnit.DataPropertyName = "MeasureID";
            this.colUnit.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colUnit.HeaderText = "Ед. изм.";
            this.colUnit.Name = "colUnit";
            this.colUnit.ReadOnly = true;
            this.colUnit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colUnit.Width = 80;
            // 
            // colCount
            // 
            this.colCount.DataPropertyName = "Count";
            this.colCount.HeaderText = "Кол-во";
            this.colCount.Name = "colCount";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Price";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column3.HeaderText = "Цена, руб";
            this.Column3.Name = "Column3";
            // 
            // colSum
            // 
            this.colSum.DataPropertyName = "PriceSum";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.colSum.DefaultCellStyle = dataGridViewCellStyle2;
            this.colSum.HeaderText = "Сумма, руб";
            this.colSum.Name = "colSum";
            this.colSum.ReadOnly = true;
            this.colSum.Width = 170;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(137, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Приходный ордер №";
            // 
            // txtNbr
            // 
            this.txtNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNbr.Location = new System.Drawing.Point(319, 11);
            this.txtNbr.Name = "txtNbr";
            this.txtNbr.Size = new System.Drawing.Size(100, 26);
            this.txtNbr.TabIndex = 24;
            this.txtNbr.Tag = "ro.Nbr";
            this.txtNbr.Text = "0";
            this.txtNbr.TextChanged += new System.EventHandler(this.txtNbr_TextChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(435, 12);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(139, 20);
            this.dtpDate.TabIndex = 27;
            this.dtpDate.Tag = "ro.RegisterD";
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Поставщик";
            // 
            // dtpAccountD
            // 
            this.dtpAccountD.Location = new System.Drawing.Point(241, 93);
            this.dtpAccountD.Name = "dtpAccountD";
            this.dtpAccountD.Size = new System.Drawing.Size(131, 20);
            this.dtpAccountD.TabIndex = 35;
            this.dtpAccountD.Tag = "ro.AccountD";
            // 
            // btnSelectProvider
            // 
            this.btnSelectProvider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectProvider.Location = new System.Drawing.Point(580, 42);
            this.btnSelectProvider.Name = "btnSelectProvider";
            this.btnSelectProvider.Size = new System.Drawing.Size(27, 23);
            this.btnSelectProvider.TabIndex = 31;
            this.btnSelectProvider.Text = "...";
            this.btnSelectProvider.UseVisualStyleBackColor = true;
            this.btnSelectProvider.Click += new System.EventHandler(this.btnSelectProvider_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(217, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "от";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Счет №";
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(57, 93);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(156, 20);
            this.txtAccount.TabIndex = 33;
            this.txtAccount.Tag = "ro.AccountNbr";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-210, -42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Комментарий к продукту";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(6, 25);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(825, 75);
            this.txtComment.TabIndex = 23;
            this.txtComment.Tag = "ro.Comment";
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Примечание к приходному ордеру";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSign,
            this.btnSave,
            this.tbPrint,
            this.tbNew});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(381, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // btnSign
            // 
            this.btnSign.Image = ((System.Drawing.Image)(resources.GetObject("btnSign.Image")));
            this.btnSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(86, 22);
            this.btnSign.Text = "Подписать";
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 22);
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPrint
            // 
            this.tbPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbPrint.Image")));
            this.tbPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbPrint.Name = "tbPrint";
            this.tbPrint.Size = new System.Drawing.Size(66, 22);
            this.tbPrint.Text = "Печать";
            // 
            // tbNew
            // 
            this.tbNew.Image = ((System.Drawing.Image)(resources.GetObject("tbNew.Image")));
            this.tbNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbNew.Name = "tbNew";
            this.tbNew.Size = new System.Drawing.Size(101, 22);
            this.tbNew.Text = "Новый ордер";
            this.tbNew.Click += new System.EventHandler(this.tbNew_Click);
            // 
            // frmReceiptOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 519);
            this.Controls.Add(this.toolStripContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "frmReceiptOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Приходный ордер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScladInput_FormClosing);
            this.Load += new System.EventHandler(this.frmMedicamentInput_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.DateTimePicker dtpAccountD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSelectProvider;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtNbr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCommentMedicament;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAllPrice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton rbNotBudget;
        private System.Windows.Forms.RadioButton rbBudget;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tbPrint;
        private System.Windows.Forms.ToolStripButton tbNew;
        private ExButton.NET.ExButton btnAdd_Medicine;
        private ExButton.NET.ExButton btnDel_Medicine;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewComboBoxColumn colUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSum;
        private System.Windows.Forms.ToolStripButton btnSign;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ComboBox cmbProvider;
    }
}