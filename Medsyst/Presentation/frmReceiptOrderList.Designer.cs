﻿namespace Medsyst
{
    partial class frmReceiptOrderList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptOrderList));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.rbIsNoBudget = new System.Windows.Forms.RadioButton();
            this.rbIsBudget = new System.Windows.Forms.RadioButton();
            this.chkFinance = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.cmbProviders = new System.Windows.Forms.ComboBox();
            this.chkProviders = new System.Windows.Forms.CheckBox();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.btnFilterDelete = new System.Windows.Forms.Button();
            this.btnFilterApply = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.gridOrders = new System.Windows.Forms.DataGridView();
            this.colIsDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccountNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccountDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsBudget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProvider = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSumm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsSigned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAdd = new System.Windows.Forms.ToolStripButton();
            this.tsbGoto = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbRestore = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOrders)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1071, 498);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1071, 525);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkShowDeleted);
            this.splitContainer1.Panel1.Controls.Add(this.rbIsNoBudget);
            this.splitContainer1.Panel1.Controls.Add(this.rbIsBudget);
            this.splitContainer1.Panel1.Controls.Add(this.chkFinance);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dateTimePicker2);
            this.splitContainer1.Panel1.Controls.Add(this.cmbProviders);
            this.splitContainer1.Panel1.Controls.Add(this.chkProviders);
            this.splitContainer1.Panel1.Controls.Add(this.chkDate);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilterDelete);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilterApply);
            this.splitContainer1.Panel1.Controls.Add(this.dateTimePicker1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.txtPrice);
            this.splitContainer1.Panel2.Controls.Add(this.gridOrders);
            this.splitContainer1.Size = new System.Drawing.Size(1071, 498);
            this.splitContainer1.SplitterDistance = 239;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(12, 249);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(133, 17);
            this.chkShowDeleted.TabIndex = 34;
            this.chkShowDeleted.Text = "Показать удаленные";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // rbIsNoBudget
            // 
            this.rbIsNoBudget.AutoSize = true;
            this.rbIsNoBudget.Enabled = false;
            this.rbIsNoBudget.Location = new System.Drawing.Point(90, 159);
            this.rbIsNoBudget.Name = "rbIsNoBudget";
            this.rbIsNoBudget.Size = new System.Drawing.Size(82, 17);
            this.rbIsNoBudget.TabIndex = 33;
            this.rbIsNoBudget.Text = "внебюджет";
            this.rbIsNoBudget.UseVisualStyleBackColor = true;
            // 
            // rbIsBudget
            // 
            this.rbIsBudget.AutoSize = true;
            this.rbIsBudget.Checked = true;
            this.rbIsBudget.Enabled = false;
            this.rbIsBudget.Location = new System.Drawing.Point(25, 159);
            this.rbIsBudget.Name = "rbIsBudget";
            this.rbIsBudget.Size = new System.Drawing.Size(64, 17);
            this.rbIsBudget.TabIndex = 32;
            this.rbIsBudget.TabStop = true;
            this.rbIsBudget.Text = "бюджет";
            this.rbIsBudget.UseVisualStyleBackColor = true;
            // 
            // chkFinance
            // 
            this.chkFinance.AutoSize = true;
            this.chkFinance.Location = new System.Drawing.Point(12, 135);
            this.chkFinance.Name = "chkFinance";
            this.chkFinance.Size = new System.Drawing.Size(74, 17);
            this.chkFinance.TabIndex = 31;
            this.chkFinance.Text = "Средства";
            this.chkFinance.UseVisualStyleBackColor = true;
            this.chkFinance.CheckedChanged += new System.EventHandler(this.chkFinance_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "с";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(25, 109);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(150, 20);
            this.dateTimePicker2.TabIndex = 28;
            // 
            // cmbProviders
            // 
            this.cmbProviders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProviders.Enabled = false;
            this.cmbProviders.FormattingEnabled = true;
            this.cmbProviders.Location = new System.Drawing.Point(25, 30);
            this.cmbProviders.Name = "cmbProviders";
            this.cmbProviders.Size = new System.Drawing.Size(150, 21);
            this.cmbProviders.TabIndex = 27;
            // 
            // chkProviders
            // 
            this.chkProviders.AutoSize = true;
            this.chkProviders.Location = new System.Drawing.Point(12, 7);
            this.chkProviders.Name = "chkProviders";
            this.chkProviders.Size = new System.Drawing.Size(84, 17);
            this.chkProviders.TabIndex = 26;
            this.chkProviders.Text = "Поставщик";
            this.chkProviders.UseVisualStyleBackColor = true;
            this.chkProviders.CheckedChanged += new System.EventHandler(this.chkProviders_CheckedChanged);
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Location = new System.Drawing.Point(12, 60);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(126, 17);
            this.chkDate.TabIndex = 25;
            this.chkDate.Text = "Дата приходования";
            this.chkDate.UseVisualStyleBackColor = true;
            this.chkDate.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // btnFilterDelete
            // 
            this.btnFilterDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnFilterDelete.Image")));
            this.btnFilterDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilterDelete.Location = new System.Drawing.Point(12, 220);
            this.btnFilterDelete.Name = "btnFilterDelete";
            this.btnFilterDelete.Size = new System.Drawing.Size(163, 23);
            this.btnFilterDelete.TabIndex = 24;
            this.btnFilterDelete.Text = "Отменить фильтр";
            this.btnFilterDelete.UseVisualStyleBackColor = true;
            this.btnFilterDelete.Click += new System.EventHandler(this.btnFilterDelete_Click);
            // 
            // btnFilterApply
            // 
            this.btnFilterApply.Image = ((System.Drawing.Image)(resources.GetObject("btnFilterApply.Image")));
            this.btnFilterApply.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilterApply.Location = new System.Drawing.Point(12, 191);
            this.btnFilterApply.Name = "btnFilterApply";
            this.btnFilterApply.Size = new System.Drawing.Size(163, 23);
            this.btnFilterApply.TabIndex = 23;
            this.btnFilterApply.Text = "Применить фильтр";
            this.btnFilterApply.UseVisualStyleBackColor = true;
            this.btnFilterApply.Click += new System.EventHandler(this.btnFilterApply_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(25, 83);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(150, 20);
            this.dateTimePicker1.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(513, 478);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Итого по приходным ордерам";
            // 
            // txtPrice
            // 
            this.txtPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrice.Location = new System.Drawing.Point(679, 474);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(146, 20);
            this.txtPrice.TabIndex = 5;
            this.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gridOrders
            // 
            this.gridOrders.AllowUserToAddRows = false;
            this.gridOrders.AllowUserToDeleteRows = false;
            this.gridOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIsDeleted,
            this.colId,
            this.colNumber,
            this.colAccountNbr,
            this.colAccountDate,
            this.colIsBudget,
            this.colProvider,
            this.colDate,
            this.colSumm,
            this.colIsSigned,
            this.colComment});
            this.gridOrders.Location = new System.Drawing.Point(0, 0);
            this.gridOrders.MultiSelect = false;
            this.gridOrders.Name = "gridOrders";
            this.gridOrders.ReadOnly = true;
            this.gridOrders.RowHeadersVisible = false;
            this.gridOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridOrders.Size = new System.Drawing.Size(806, 470);
            this.gridOrders.TabIndex = 2;
            this.gridOrders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridOrders_CellDoubleClick);
            // 
            // colIsDeleted
            // 
            this.colIsDeleted.DataPropertyName = "isDeleted";
            this.colIsDeleted.HeaderText = "удален";
            this.colIsDeleted.Name = "colIsDeleted";
            this.colIsDeleted.ReadOnly = true;
            this.colIsDeleted.Visible = false;
            this.colIsDeleted.Width = 50;
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "№";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colId.Visible = false;
            this.colId.Width = 70;
            // 
            // colNumber
            // 
            this.colNumber.DataPropertyName = "Number";
            this.colNumber.HeaderText = "№ прих. ордера";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            this.colNumber.Width = 60;
            // 
            // colAccountNbr
            // 
            this.colAccountNbr.DataPropertyName = "AccountNbr";
            this.colAccountNbr.HeaderText = "№ счета";
            this.colAccountNbr.Name = "colAccountNbr";
            this.colAccountNbr.ReadOnly = true;
            this.colAccountNbr.Width = 80;
            // 
            // colAccountDate
            // 
            this.colAccountDate.DataPropertyName = "AccountDate";
            this.colAccountDate.HeaderText = "Дата счета";
            this.colAccountDate.Name = "colAccountDate";
            this.colAccountDate.ReadOnly = true;
            // 
            // colIsBudget
            // 
            this.colIsBudget.DataPropertyName = "SourceFinancing";
            this.colIsBudget.HeaderText = "Источник финанс.";
            this.colIsBudget.Name = "colIsBudget";
            this.colIsBudget.ReadOnly = true;
            this.colIsBudget.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIsBudget.Width = 80;
            // 
            // colProvider
            // 
            this.colProvider.DataPropertyName = "Provider";
            this.colProvider.HeaderText = "Поставщик";
            this.colProvider.Name = "colProvider";
            this.colProvider.ReadOnly = true;
            this.colProvider.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colProvider.Width = 120;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Date";
            this.colDate.HeaderText = "Дата приход.";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            // 
            // colSumm
            // 
            this.colSumm.DataPropertyName = "Summ";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.colSumm.DefaultCellStyle = dataGridViewCellStyle1;
            this.colSumm.HeaderText = "Сумма, руб";
            this.colSumm.Name = "colSumm";
            this.colSumm.ReadOnly = true;
            // 
            // colIsSigned
            // 
            this.colIsSigned.DataPropertyName = "isSigned";
            this.colIsSigned.HeaderText = "Подписан";
            this.colIsSigned.Name = "colIsSigned";
            this.colIsSigned.ReadOnly = true;
            this.colIsSigned.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colIsSigned.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIsSigned.Width = 60;
            // 
            // colComment
            // 
            this.colComment.DataPropertyName = "Comment";
            this.colComment.HeaderText = "Комментарий";
            this.colComment.Name = "colComment";
            this.colComment.ReadOnly = true;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbGoto,
            this.tsbDelete,
            this.tsbRestore,
            this.toolStripTextBox1,
            this.toolStripButton4});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(415, 27);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAdd
            // 
            this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
            this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdd.Name = "tsbAdd";
            this.tsbAdd.Size = new System.Drawing.Size(83, 24);
            this.tsbAdd.Text = "Добавить";
            this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // tsbGoto
            // 
            this.tsbGoto.Image = ((System.Drawing.Image)(resources.GetObject("tsbGoto.Image")));
            this.tsbGoto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGoto.Name = "tsbGoto";
            this.tsbGoto.Size = new System.Drawing.Size(78, 24);
            this.tsbGoto.Text = "Перейти";
            this.tsbGoto.Click += new System.EventHandler(this.tsbGoto_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsbDelete.Image")));
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(75, 24);
            this.tsbDelete.Text = "Удалить";
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // tsbRestore
            // 
            this.tsbRestore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbRestore.Image = ((System.Drawing.Image)(resources.GetObject("tsbRestore.Image")));
            this.tsbRestore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRestore.Name = "tsbRestore";
            this.tsbRestore.Size = new System.Drawing.Size(86, 24);
            this.tsbRestore.Text = "Восстановить";
            this.tsbRestore.Visible = false;
            this.tsbRestore.Click += new System.EventHandler(this.tsbRestore_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(65, 24);
            this.toolStripButton4.Text = "Найти";
            // 
            // frmReceiptOrderList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1071, 525);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmReceiptOrderList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Реестр приходных ордеров";
            this.Load += new System.EventHandler(this.frmRecieptOrders_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridOrders)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAdd;
        private System.Windows.Forms.ToolStripButton tsbGoto;
        private System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView gridOrders;
        private System.Windows.Forms.CheckBox chkDate;
        private System.Windows.Forms.Button btnFilterDelete;
        private System.Windows.Forms.Button btnFilterApply;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbProviders;
        private System.Windows.Forms.CheckBox chkProviders;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.RadioButton rbIsNoBudget;
        private System.Windows.Forms.RadioButton rbIsBudget;
        private System.Windows.Forms.CheckBox chkFinance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        [sec(new Groups[] { Groups.Admin, Groups.Manager }, true)]
        public System.Windows.Forms.CheckBox chkShowDeleted;
        private System.Windows.Forms.ToolStripButton tsbRestore;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAccountNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAccountDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsBudget;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProvider;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsSigned;
        private System.Windows.Forms.DataGridViewTextBoxColumn colComment;
    }
}