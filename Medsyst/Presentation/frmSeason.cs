﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;


namespace Medsyst
{
    public partial class frmSeason : Form
    {
        Season ss = new Season(true);

        public frmSeason()
        {
            InitializeComponent();
        }

       
        private void frmSeason_Load_1(object sender, EventArgs e)
        {
            ss.FormBinding("ss", this);
            if(cmd.Mode == CommandModes.New)
                textBox1.Text = cmd.Id.ToString();
            else if (cmd.Mode == CommandModes.Edit)
            {
                ss.SeasonID = cmd.Id;
                ss.Load();
                ss.WriteToForm();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ss.ReadFromForm();
            if (cmd.Mode == CommandModes.New)
            {
                if (!ss.Insert())
                    MessageBox.Show("Не удалось добавить сезон.");
            }
            else if (cmd.Mode == CommandModes.Edit)
            {
                ss.Update();
            }
            Close();
        }

    }
}
