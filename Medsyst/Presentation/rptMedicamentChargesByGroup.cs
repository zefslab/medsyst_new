﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Dao.Base;
using Medsyst.DataSet;
using Medsyst.DataSet.MChBGTableAdapters; //Пространство имен для доступа к адаптерам набора данных указано в файле MedicamentChargesByGroup.Desiner.cs

namespace Medsyst
{
    public partial class rptMedicamentChargesByGroup : Form
    {
        /// <summary>Коллекция категории медикаментов не распределенные по группам</summary>
        MedicamentCategoryList mcs = new MedicamentCategoryList();
        /// <summary>Коллекция групп категорий</summary>
        MedicamentCategoryGroups mcgs = new MedicamentCategoryGroups();
        /// <summary>Коллекция категории медикаментов принадлежащие выделенной группе</summary>
        MedicamentCategoryList mcsInGpoup = new MedicamentCategoryList();
        
        Seasons ssBegin = new Seasons(); //Коллекция для списка выбора начального значения сезона
        Seasons ssEnd = new Seasons(); //Коллекция для списка выбора конечного значения сезона
        Seasons ss; //Коллекция сезонов выбранного диапазона
        Season sBegin = new Season(true);//Начальный сезон выбранного диапазона
        Season sEnd = new Season(true);//Конечный сезон выбранного диапазона

        MChBGTableAdapter tableAdapter = new MChBGTableAdapter(); //Инициализируется адаптер с последующим присвоением занчений параметрам
        MChBG dataSet = new MChBG(); //создается экземпляр набора данных
        PatientCategoryList categoris = new PatientCategoryList();

        public rptMedicamentChargesByGroup()
        {
            InitializeComponent();
        }
        /// <summary>Загрузка формы
        /// Заплатин Е.Ф.
        /// 30.05.2011</summary>
        private void rptMedicamentChargesByGroup_Load(object sender, EventArgs e)
        {
            medicamentChargesByGroup1.Load(); //Загружается отчет. Экзкмпляр отчета определен в файле rptMedicamentChargesByGroup.Desiner.cs
            tableAdapter.Connection.ConnectionString = DB.CS;//Строка подключения

            //Заполнение списков категорий пациентов 
            chlbCategory.Items.AddRange(categoris.ToArray());
            chlbCategoryBySeason.Items.AddRange(categoris.ToArray());
 
            //Загрузка коллекций сезонов для списков выбора
            ssBegin.Load(CommandDirective.Non);
            cmbSeasonBegin.DataSource = ssBegin;
            ssEnd.Load(CommandDirective.Non);
            cmbSeasonEnd.DataSource = ssEnd;

            //Загрузка категорий у который не установлена принадлежность к группе GroupID=0
            treeCategoryRefresh();

            //Загружаютя группы категорий
            FillGroups();

            //Загрузка категорий относящихся к выделенной группе произойдет автоматически, в ответ на событие выделения группы
        }
        /// <summary>Обновление дерева несвязанных с группами категорий в котором не отобразаются категории связанные с группами
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        private void treeCategoryRefresh()
        {
            mcs.Clear();
            //Загрузка полной коллекции с отметкой элементов, которые нужно выводить
            mcs.Load();
            mcs.CheckVisible(null);//Отметка категорий не принадлежащих ни одной группе
            mcs.FillTreeVisibleNodes(treeCategory);//Заплонение визуального дерева элементами коллекции
            treeCategory.ExpandAll();
        }

        /// <summary>Заполнение списка групп категорий
        /// Заплатин Е.Ф.
        /// 30.05.2011</summary>
        private void FillGroups()
        {
            lbGrop.DataSource = null;
            mcgs.Load();
            lbGrop.ValueMember = "GroupID";
            lbGrop.DisplayMember = "GroupN";
            lbGrop.DataSource = mcgs.Select(x=> new
            {
                x.GroupID,
                x.GroupN
            }).ToList();
           
        }
        /// <summary>Отображение категорий принадлежащих выделенной группе
        /// Заплатин Е.Ф.
        /// 30.05.2011</summary>
        private void lbGrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbGrop.SelectedIndex == -1) return;//не выделен ни один элемент списка групп
            treeCategoryInGroupRefresh(mcgs[lbGrop.SelectedIndex]);
        }
        /// <summary>Обновление дерева категорий включенных в группу
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        private void treeCategoryInGroupRefresh(MedicamentCategoryGroup g)
        {
            mcsInGpoup.Clear(); //Очистка дерева перед новой загрузкой категорий принадлежащих выделенной группе
            mcsInGpoup.Load();
            mcsInGpoup.CheckVisible(g.GroupID);
            mcsInGpoup.FillTreeVisibleNodes(treeCategoryInGroup);//Дерево категорий принадлежащих выделенной группе
            treeCategoryInGroup.ExpandAll();
        }
        /// <summary>Добавление новой группы категорий
        /// Заплатин Е.Ф.
        /// 03.05.2011</summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Value = "";
            frm.Caption = "Группа категорий медикаментов";
            frm.Text = "Введите наименование группы";
            if (frm.ShowDialog() == DialogResult.OK)
            {//Введено наименование новой группы
                //Добавление группы
                MedicamentCategoryGroup mcg = new MedicamentCategoryGroup();
                mcg.GroupN = frm.Value;
                mcg.Insert();
                //Перезагрузка списка групп
                mcgs.Clear();
                mcgs.Load();
                FillGroups();
                //селектор устанавливается на добавленную группу. Поиск группы осуществляется по совпадению наименования
                lbGrop.SelectedIndex = mcgs.FindIndex(delegate(MedicamentCategoryGroup g) { return g.GroupN == mcg.GroupN; });
            }

        }
        /// <summary>Изменение наименования группы
        /// Заплатин Е.Ф.
        /// 03.05.2011</summary>
        private void btnChange_Click(object sender, EventArgs e)
        {
            if (lbGrop.SelectedIndex < 0)
            {
                MessageBox.Show("Не выделено ни одной группы в списке. Укажите группу и повторите операцию.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;//Не выделена ни одна группа
            }

            InputBox frm = new InputBox();
            frm.Value = mcgs[lbGrop.SelectedIndex].GroupN;
            frm.Caption = "Группа категорий медикаментов";
            frm.Text = "Внесите изменение в наименование группы";
            if (frm.ShowDialog() == DialogResult.OK)
            {
                mcgs[lbGrop.SelectedIndex].GroupN = frm.Value;
                mcgs[lbGrop.SelectedIndex].Update();
                FillGroups();
            }
        }
        /// <summary>Удаление выделенной группы категорий
        /// Заплатин Е.Ф.
        /// 03.05.2011</summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = lbGrop.SelectedIndex;
            if (index < 0)
            {
                MessageBox.Show("Не выделено ни одной группы в списке. Укажите группу и повторите операцию.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;//Не выделена ни одна группа
            }
            //Проверка на наличие категорий в удаляемой группе
            MedicamentCategoryList mcsig = new MedicamentCategoryList();
            mcsig.Load(mcgs[lbGrop.SelectedIndex].GroupID);//Загружаются категории отнесенные к выделенной в списек группе
            if (mcsig.Count > 0)
            {
                MessageBox.Show("С группой связаны категории медикаментов. Чтобы удалить эту группу, нужно сначала вывести из нее все категории.", "Отказ в удалении!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            mcgs[index].Delete();//Удаляется запись из базы данных
            //mcgs.RemoveAt(index);//Удаляется элемент коллекции по указанному индексу
            FillGroups();
        }
        /// <summary>Добавление одной или нескольких выбранных категории к выделенно группе
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddToGroup_Click(object sender, EventArgs e)
        {
            //1. Выбор узлов из дерева категорий , отмеченных чекбоксами для добавления их в выделенную группу
            TreeNodeCollection checkitNodes = TreeUtils.GetCheckitNodes(treeCategory);//Выбор из дерева отмеченых чекитами узлов.
            //TODO: ЗЕФ при накоплении обгалоченых узлов в дерево добавляются лишние узлы с сылкой на null. Пока это не мешает, но этот побочный эффект нужно будет устранить.

            //2. Проверка на выделение категорий и какой либо группы
            if (checkitNodes.Count == 0 || lbGrop.SelectedIndex < 0)
            {
                MessageBox.Show("Для добавления категории в группу необходимо отметить нужные категории и указать группу, в которую они будут перенесены. Призведите выделение и повторите операцию.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;//Не выделена  группа или категория
            }

            //3. Передача отмеченых узлов дерева категорий в выделенную группу и обновление дерева категорий включенных в эту группу
            MedicamentCategoryGroup group = mcgs[lbGrop.SelectedIndex];//Выделенная группа
            mcsInGpoup.Clear();//Очистка коллекции категорий принадлежащих группе
            mcsInGpoup.ConverFromNodes(checkitNodes);//Формируется коллекция категорий из выбранных узлов для передачи группе
            group.AddCategories(mcsInGpoup);//Добавление коллекции отмеченных категорий в выделенную группу
            treeCategoryInGroupRefresh(group);//Обновление категорий в группе


            //4. Обновление дерева категорий для исключения из него переданных в группы узлов
            treeCategoryRefresh();


        }
        /// <summary>Исключение категории из группы
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveFromGroup_Click(object sender, EventArgs e)
        {
            MedicamentCategoryGroup group = mcgs[lbGrop.SelectedIndex];//Выделенная группа

            //1. Выбор узлов из дерева категорий , отмеченных чекбоксами для добавления их в выделенную группу
            TreeNodeCollection checkitNodes = TreeUtils.GetCheckitNodes(treeCategoryInGroup);//Выбор из дерева отмеченых чекитами узлов.

            //2. Формируется коллекция категорий из отмеченых узлов дерева для исключения из группы
            mcsInGpoup.Clear();
            mcsInGpoup.ConverFromNodes(checkitNodes);//Формируется коллекция категорий из выбранных узлов для передачи группе

            //3. Удаляются вся коллекция отмеченных категорий из группы
            group.DeleteCategories(mcsInGpoup);

            //4. Обновление обеих деревьев категорий
            treeCategoryRefresh();
            treeCategoryInGroupRefresh(group);

        }
        /// <summary>После изменения статуса чекбокса узла меняется статус всех дочерних подузлов
        /// Заплатин Е.Ф.
        /// 06.05.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeCategory_AfterCheck(object sender, TreeViewEventArgs e)
        {
            //Изменение значений чекбоксов подузлов в соответсвии с родительским узлом
            foreach (TreeNode treeSubNode in e.Node.Nodes)
                treeSubNode.Checked = e.Node.Checked;     
         }

        private void btnExecutReportByPeriod_Click(object sender, EventArgs e)
        {
            //промотр выделеных категориий пациентов из списка. ЗЕФ. 22.02.2013
            String c = "";//Строка-список индексов обгалоченых категорий
            String cName = ""; //Строка-список наименований обгалоченых категорий
            int[] lstCat = lstCheckedItems(chlbCategory, ref c, ref cName);

           
            //Заполнение данными таблицы набора данных с присвоением занчений параметрам 
            tableAdapter.Fill(dataSet._MChBG, 
                              dtpBeginPeriod.Value.ToString("dd.MM.yyyy"), 
                              dtpEndPeriod.Value.ToString("dd.MM.yyyy"), 
                              (bool)rbtnBudget.Checked,
                              c);
            medicamentChargesByGroup1.SetDataSource(dataSet); //вызываемый метод передает отчету заполненный экземпляр набора данных dataSet

            //Вспомогательные параметры для оформления заголовка отчета, указывающие даты начала и окончания отчетного периода
            medicamentChargesByGroup1.SetParameterValue("DateBegin", dtpBeginPeriod.Value.ToString("dd.MM.yyyy"));
            medicamentChargesByGroup1.SetParameterValue("DateEnd", dtpEndPeriod.Value.ToString("dd.MM.yyyy"));
            medicamentChargesByGroup1.SetParameterValue("Средства", (bool)rbtnBudget.Checked);
            medicamentChargesByGroup1.SetParameterValue("СтатьиРасходов", cName); 

            crystalReportViewerByPeriod.ReportSource = medicamentChargesByGroup1; //Элементу формы просмотра отчетов передается загруженный отчет
            crystalReportViewerByPeriod.Zoom(1);//Маштабирование предпросмотра отчета по ширине страницы
        }
        /// <summary> Подсчет количества отмеченых категорий пациентов  
        /// Заплатин Е.Ф.
        /// 09.05.2013
        /// </summary>
        /// <param name="c">список идентификаторов категорий пациентов через запятую в виде строки</param>
        /// <param name="cName">список названий категорий пациентов через запятую в виде строки</param>
        /// <returns></returns>
        private int[] lstCheckedItems(CheckedListBox ch, ref string c, ref string cName)
        {
            List<int> lstCat = new List<int>(); //Список идентификаторов отмеченых категорий
            
            foreach (int i in ch.CheckedIndices)
            {
                c += ", " + categoris[i].CategoryID.ToString();//Добавляется в список идентификаторов категорий
                cName += ", " + categoris[i].CategoryN;//Добавляется в список наименований категорий
                lstCat.Add(categoris[i].CategoryID);
            }
            if (c != "")
            {//Обгалочена хотя бы одна категория
                c = c.Remove(0, 2); //Удаление лишней запятой  спереди списка
                cName = cName.Remove(0, 2); //Удаление лишней запятой  спереди списка
            }
            else
                c = "0";//Не выделено ни одной категории

            return lstCat.ToArray();
        }

        /// <summary>Подготовка данных для формирования отчета
        /// Заплатин Е.Ф.
        /// 28.05.2011
        /// </summary>
        private void btnExecutReportBySeason_Click(object sender, EventArgs e)
        {
            //промотр выделеных категориий пациентов из списка. ЗЕФ. 22.02.2013
            String c = "";//Строка-список индексов обгалоченых категорий
            String cName = ""; //Строка-список наименований обгалоченых категорий
            int[] categories = lstCheckedItems(chlbCategoryBySeason, ref c, ref cName);

            DateTime dBegin, dEnd; //Дата начала и окончания отчетного периода свормированного по сезонам
          //1 шаг.  Производится формирование коллекции сезонов на основе указанного начального и конечного.
            //        Вычисляются даты начала и окончания отчетного периода по выбранным сезонам
            sBegin.SeasonID = (int)cmbSeasonBegin.SelectedValue;
            sBegin.Load(); //Загружется начальный сезон периода
            dBegin = sBegin.SeasonOpen;
            sEnd.SeasonID = (int)cmbSeasonEnd.SelectedValue;
            sEnd.Load(); //Загружется конечный сезон периода
            dEnd = sEnd.SeasonClose;
            //Проверка того, чтобы начальный сезон не был позднее конечного
            if (dBegin > dEnd)
            { //Сезон начальный оказался позднее сезона конечного, что недопустимо
                MessageBox.Show("Сезон начала периода выбран позднее сезона окончания периода. Это недопустимо. Измените выбор.", "Недопустивый диапазон.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ss = new Seasons();
            ss.Load(dBegin, dEnd); //Загружается коллекция сезонов которые входят в указанный диапазон
            
          //2 шаг. Вычисляется среднее количество дней в сезоне. По упрощенному варианту можно использовать количество дней в сезоне, 
            //     указываемое в настройках программы
            int сountDays = 0; //Количество дней в указанном периоде
            foreach (Season s in ss)
            {
                сountDays += 21; //Поправить в последствии на вычисление реального количества дней в каждом сезоне.
            }
            сountDays = сountDays / ss.Count;//Среднее количество дней в одном сезоне

          //3 шаг. Определяется количество историй болезней за все сезоны вошедшие в отчетный период.
            int сountIH = ss.CountIH(categories); 
 
          //4 шаг. Формируется отчет
            //Заполнение данными таблицы набора данных с присвоением занчений параметрам 
            tableAdapter.Fill(dataSet._MChBG, 
                              dBegin.ToString("dd.MM.yyyy"), 
                              dEnd.ToString("dd.MM.yyyy"), 
                              (bool)rbtnBudget2.Checked,
                              c);
            medicamentChargesByGroup_21.SetDataSource(dataSet); //вызываемый метод передает отчету заполненный экземпляр набора данных dataSet

            //Вспомогательные параметры для оформления заголовка отчета, указывающие даты начала и окончания отчетного периода
            medicamentChargesByGroup_21.SetParameterValue("SeasonBegin", sBegin.FullNbr);
            medicamentChargesByGroup_21.SetParameterValue("SeasonEnd", sEnd.FullNbr);
            medicamentChargesByGroup_21.SetParameterValue("Средства", (bool)rbtnBudget2.Checked);
            medicamentChargesByGroup_21.SetParameterValue("Истории_болезней", сountIH);
            medicamentChargesByGroup_21.SetParameterValue("СтатьиРасходов", cName); 
            

            crystalReportViewerBySeason.ReportSource = medicamentChargesByGroup_21; //Элементу формы просмотра отчетов передается загруженный отчет


        }
 


       
    }
}
