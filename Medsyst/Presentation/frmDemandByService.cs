﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmDemandByService : Form
    {
        ///<summary>Требование на склад</summary>
        DemandService demand = new DemandService(true);
        ///<summary>Выдаваемые  медикаменты</summary>
        MedicamentByDemandList mbds = new MedicamentByDemandList();
        ///<summary>Временно удаленные медикаменты  </summary>
        MedicamentByDemandList mbds_deleted = new MedicamentByDemandList();
        ///<summary>Сотрудники профилактория</summary>
        MemberList members = new MemberList();
        /// <summary>Услуга по которой выдаются медикаменты</summary>
        Service s = new Service();
        /// <summary>Единицы измерений</summary>
        Measures measures = new Measures();
        /// <summary>Сезоны</summary>
        Seasons ss = new Seasons();
        Settings set = new Settings(true);// Настройки системы
        Finance f; //Источник финасирования
        private class srv //Вспомогательный класс для передачи двух параметров целого и булевого типа
        {
            public int s = 0; //Идентификатор услуги
            public bool b = true; //бюджет или внебюджет
            public srv() { }
        }

        public frmDemandByService()
        {
            InitializeComponent();
        }
        private void frmDemandByService_Load(object sender, EventArgs e)
        {
            gridMeds.AutoGenerateColumns = false;
            demand.FormBinding("demand",this);
            set.Load();//Загрузка настроек системы для получения в последствии текущего сезона            
            
            //Загружается список сотрудников для выбора ответственного лица за требование
            cmbMembers.DataSource = null;
            members.Load(CommandDirective.AddNonDefined);
            cmbMembers.DataSource = members;
            
            //Загрузка коллекции единиц измерений для подстановки в соответсвующий столбец
            colMeasures.ValueMember = "MeasureID";
            colMeasures.DisplayMember = "ShotName";
            colMeasures.DataSource = null;
            measures.Load();
            colMeasures.DataSource = measures;
           
            //Загрузка сезонов
            cmbSeason.DataSource = null;
            ss.Load(CommandDirective.AddNonDefined);
            cmbSeason.DataSource = ss;
            
            
            cmd = Tag as Command;//Принятие интерфейса от вызывающей формы
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
            {//Создание нового требования на склад
                demand.RegisterD = DateTime.Now;
                string doctypes = ((int)DocTypes.DemandMaterial).ToString() + "," +
                                  ((int)DocTypes.DemandMedicament).ToString() + "," +
                                  ((int)DocTypes.DemandService).ToString(); //Формируется список типов документов (все требования) у которых следует обеспечить сквозную нумерацию
                demand.Nbr = demand.GetNewNumber(doctypes, false);
                demand.SeasonID = set.CurrentSeason;
                demand.Insert();
                //ЗЕФ. 12.11.2012
                List<Object> l = new List<Object>();
                if (((Command)Tag).obj != null)
                {
                    l = (List<Object>)((Command)Tag).obj;
                    if ((int)l[0] > 0)//Если передан идентификатор услуги  в первой позиции списка
                    {
                        ((Command)Tag).Id = (int)l[0]; //Передается в Id идентификатор услуги чтобы на его основе добавить медикаменты
                        rbtnIsBudget.Checked = (bool)l[1];//Второй параметр указывает на тип бюджета
                        rbtnNotBudget.Checked = !rbtnIsBudget.Checked;
                        serviсeSelect();
                    }
                }
            }
            else
            {//загрузка уже существующего требования на склад
                demand.DocumentID = cmd.Id;
                demand.Load();
                mbds = demand.LoadMed();
                s.ServiceID = demand.ServiceID;
                s.Load();//Загрузка услуги связанной с требованием
                txtService.Text = s.ServiceN;//Передача в сторку наименования услуги
                if (demand.ServiceID != 0) rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = false;

                SetFormControls();
            }
            f = demand.IsBudget ? Finance.Budget : Finance.ExtraBudget;//уточняется источник финансирования
            gridMeds.DataSource = null;//Обнуление поля источника данных для присвоение нового значения
            gridMeds.DataSource = mbds;//Назначается новый источник данных
            //rbtnNotBudget.Checked = !demand.IsBudget;//Установка контрола для внебюджетных средств
            demand.WriteToForm();

            //Вынужденное добавление этого условия после метода WriteToForm. Иначе этот флаг всегда будет - false
            if (cmd.Mode == CommandModes.New)//Анализируется режим открытия формы
                DataSaved(false);
            else
                DataSaved(true);
       }
        /// <summary>В зависимости от состояния свойств: Signed устанавливается доступность элементов управления
        /// Заплатин Е.Ф.
        /// 10.05.2012
        /// </summary>
        private void SetFormControls()
        {
            if (demand.Signed)
            {//Документ был уже подписан, поэтому внесение изменений недопустимо
                //элементы управления по внесению изменений в подписанное требование становятся недоступны
                btnSign.Text = "Снять подпись";
                colCount.ReadOnly = true;
                cmbMembers.Enabled = txtNbr.Enabled = dtpDate.Enabled = cmbSeason.Enabled = false;
                btnSave.Enabled = btnServiсeSelect.Enabled = btnServiceDelete.Enabled = false;
            }
            else
            {
                btnSign.Text = "Подписать";
                colCount.ReadOnly = false;
                cmbMembers.Enabled = txtNbr.Enabled = dtpDate.Enabled = cmbSeason.Enabled = true;
                btnSave.Enabled = btnServiсeSelect.Enabled = btnServiceDelete.Enabled = true;
            }
            rbtnNotBudget.Enabled = rbtnNotBudget.Enabled = mbds.Count == 0; //Доступно к изменению только если нет медикаментов в требовании
        }

        ///<summary>Закрытие формы</summary>
        private void frmDemandByService_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!dataSaved)
            {//Какая-то часть требования в форме была изменена
                DialogResult dr = MessageBox.Show(this, "Сохранить внесенные изменения в требование?",
                    "Не сохраненные изменения",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    Save();
                    cmd.UpdateParent();
                }
                else if (dr == DialogResult.No)
                {
                    if (cmd.Mode == CommandModes.New)
                    {//Требование вновь созданное
                        demand.Delete(); //Удаление записи самого требования
                        cmd.UpdateParent();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
       }
        /// <summary>Изменение количества выписываемых по требованию медикаментов
        /// Заплатин Е.Ф.
        /// 27.07.2011
        /// </summary>
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {//Проверка на выделение хотя бы одной строки в списке медикаментов
                if (e.ColumnIndex == gridMeds.Columns["colCount"].Index)
                {//Рерактировалась колонка с количеством медикаментов
                    int BeforeCount = 0;
                    OnDemandMedicine mbd = new OnDemandMedicine();

                    if (mbds[e.RowIndex].NotSaved)
                    {//т.к. медикамент в базе не сохранен, то тостаточно синхронизировать его выдаваемое количество с остатком
                        mbds[e.RowIndex].CountGet = mbds[e.RowIndex].CountResidue = (int)gridMeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                        gridMeds.Rows[e.RowIndex].Cells["colCountResidue"].Value = gridMeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    }
                    else
                    {//Для ранее сохраненного медикамента по требованию восстанавливается занчение из базы данных
                        //для вновь добавленного и несохраненного оно всегда останется нулевым, пока не будет сохранено в базе
                        //Из скрытой колонки таблицы медикаментов  извлекается идентификатор медикамента по требованию
                        mbd.MedicamentByDemandID = (int)gridMeds.Rows[e.RowIndex].Cells["colMedicamentByDemandID"].Value;// medbds[e.RowIndex].MedicamentByDemandID;
                        mbd.Load();//Загружаются сведения о медикаменте у которого изменено количество
                        BeforeCount = mbd.CountGet;// Предудщее количество назначения
                        mbd.CountGet = (int)gridMeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;//Присваивается вновь введенное количество
                        int difference = mbd.CountGet - BeforeCount;
                        /* Здесь может возникнуть следующая проблема. 
                         * Если уменьшение количества превысисит величину остатка, то остаток может стать орицательным.
                         * Лучше всего анализировать эту ситуацию и не позволять настолько уменьшать общее количество.*/
                        if (difference < 0 & Math.Abs(difference) > mbd.CountResidue)
                        {//Разность между новым и старым значениями количества приняла отрицательное значение и 
                            //превысла величину остатка. Такого быть не должно.
                            MessageBox.Show(this, "Величина уменьшения количества превысила величину остатка медикамента по требованию.\r\n" +
                                                    "Такое уменьшение недопустимо.",
                                                    "Недопустимое значение количества.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            mbd.CountGet = BeforeCount - mbd.CountResidue; //Переназначается минимально возможное уменьшение количества.
                            difference = -mbd.CountResidue;// разность приравнивается величине остатка
                        }
                        mbd.CountResidue += difference; //Увеличение (уменьшение) остатка на величину измененного количества
                        mbd.Modifie();//Обновление будет производиться при сохранении требования в целом
                        //mbd.Update(); //Обновляется введенная информация о количество медикамента в базе данных

                        //Обновление полей с количеством и остатком после окончания редактирования записи
                        gridMeds.Rows[e.RowIndex].Cells["colCount"].Value = mbd.CountGet.ToString();
                        gridMeds.Rows[e.RowIndex].Cells["colCountResidue"].Value = mbd.CountResidue.ToString();

                        mbds[e.RowIndex] = mbd;
                        mbds[e.RowIndex].Modifie();
                    }
                    DataSaved(false);
                }
            }
        }
        ///<summary>Загрузка списка сотрудников</summary>
        private void LoadMembers()
        {

        }
        /// <summary>Выбор услуги из прейскуранта для формирования требования на медикаменты, связанные с этой услугой
        /// Заплатин Е.Ф.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnServiсeSelect_Click(object sender, EventArgs e)
        {
            //До начала выбора услуги нужно сохранить все внесенные изменения и главным образом в состав медикаментов. Если этого не сделать, то удаленные, 
            //но не сохраненные медикаменты в базе продолжают числиться, что конечно может сказаться на подсчете требуемых медикаментов по вновь назначаемой услуге
            if (!dataSaved)
            {//Есди данные не сохранены в ребовании
                string txtMsg = "В требовании не сохранены внесенные изменения. \n\rНастоятельно рекомендуется перед выполнением " +
                    "операции поиска и добавления назначенных услуг, сохранить возможные изменения с медикаментами. " +
                    "В противном случе могу быть не  верно произведены расчеты рекомендуемого количества медикаментов.\n\r\n" +
                    "Выполнить сохранение последних изменений в требовании перед началом поиска и добавления?";

                if (DialogResult.Yes == MessageBox.Show(txtMsg,
                    "Не сохраненные изменения", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    Save();
                }
            }            //Прежде чем приступть к замене услуги по требованию  проверяются медикаменты по требованию на совпадение остатка с выписаным количеством. 
            //Если обнаруживается несовпадение то операция замены становится невозможным, а значит невозможно сменить услугу по текущему требованию
            if (mbds.Count > 0)
            {
                if (demand.ChekDesidue() != "")
                {
                    MessageBox.Show("Заменить услугу не возможно. Выписанные медикаменты уже частично реализованы при оказании услуг.",
                                    "Недоступность операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    btnServiсeSelect.Enabled = false;
                    return; //Удаление невозможно поэтому процедура прерывается.
                }
            }

            frmServiceList frm = new frmServiceList(f);
            //Шаг 1. Выбор услуги из прейскуранта
            frm.Tag = new Command(CommandModes.Select, this) { OnUpdate = serviсeSelect };
            //Открывается прейскурант услуг
            frm.ShowDialog(this);
        }
        /// <summary>Добавляются медикаменты в требование в сообтветсвии с выбранной из прейскуранта услугой
        /// Заплатин Е.Ф.
        /// </summary>
        public void serviсeSelect()
        {
            if (((Command)Tag).Id <= 0)
            {//Не выбрано ни одной услуги из прейскуранта
                return;
            }
            else
            {//Услуга выбрана из прейскуранта
                //Шаг 2. Предварительное удаление медикаментов ранее связанных по другой услуге
                mbds_deleted.AddRange(mbds);//Окончательное удаление будет произведено при сохранении требования. (похоже что окончательного удаления не производится ЗЕФ)

                s.ServiceID = ((Command)Tag).Id;//Назначается ссылка по идентификатору на новую услугу
                s.Load();//Загружаются сведения по из прейскуранта выбранной услуге 
                demand.ServiceID = s.ServiceID;
                txtService.Text = s.ServiceN;//Добавляется наименование в строку

                //Шаг 3. Добавляются медикаменты к требованию в соответствии с выбранной услугой
                gridMeds.DataSource = null;//Обнуление поля источника данных для присвоение нового значения
                mbds = DemandService.Meds(s.ServiceID, demand.IsBudget, demand.DocumentID);//demand.AddMeds(s.ServiceID, demand.IsBudget, demand.DocumentID);
                gridMeds.DataSource = mbds;//Назначается новый источник данных
                rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = false;
                DataSaved(false);
            }
        }
        ///<summary>Кнопка "Подписать"</summary>
        private void btnSign_Click(object sender, EventArgs e)
        {
            if (demand.Signed)
            {//Документ уже был подписан
                demand.Sign();//Снятие подписи
                SetFormControls();//Элементы управления делаются доступными
            }
            else
            {//Документ небыл подписан
                //Прежде чем закрывать форму, поставить подпись, подтвердающую завершение формирование требования.
                //После того как будет поставлена подпись, внесение изменений станет невозможным
                DialogResult res = MessageBox.Show("Выберите - Да, если Вы уверены в том, что хотите подписать требование. " +
                            " После наложения подписи внесение изменений в требование и его удаление станет невозможным!\n\r" +
                            " Выберите - Нет, для сохранения изменений и завершения редактирования требования без наложения подписи.\n\r" +
                            " Выберите - Отмена, для продолжения редактирования требования.", "Подпись документа",
                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {//Сохранение изменений с подписью
                    demand.Sign();
                }
                else if (res == DialogResult.No)
                {//Сохранение документа без подписи

                }
                else if (res == DialogResult.Cancel)
                {//Продолжение редактирования документа
                    return;
                }
                Save();
                Close();
            }
        }
        /// <summary>Кнопка "Сохранить"
        /// Заплатин Е.Ф.
        /// 10.09.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        ///<summary>Сохранение изменений в форме. Все изменения и дополнения коллекции медикаментов принимают силу и сохраняются в базе
        ///Заплатин Е.Ф.
        ///10.09.2011</summary>       
        private void Save()
        {
            demand.ReadFromForm();
            demand.Update();
            if (!demand.DeleteMeds(ref mbds_deleted, CommandDirective.DeletePermanently)) // Удаление медикаментов удаленных при заменен услуги в процессе работы с формой
            {
                MessageBox.Show("Не удалось удалить медикаменты по требованию, поэтому не возможно назначить новую услугу. \n\rЭто нештатная ситуация. Сообщите о ней администратору Медсист.",
                                "Прерывание операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            mbds.SaveNotSaved();//Сохранение в базе добавленных но не сохраненных данных
            mbds.UpdateModified();//Обновление в базе данных сведений у измененных медикаментов

            //Обновление записей медикаментов после сохранения всех изменений
            gridMeds.DataSource = null;
            mbds = demand.LoadMed();
            gridMeds.DataSource = mbds;

            cmd.UpdateParent();
            DataSaved(true);
        }
        /// <summary>TODO Эту логику изменения статуса и доступности кнопки "Сохранить" лучше всего перенести в родительскую форму
        /// Заплатин Е.Ф.
        /// 10.09.2011
        /// </summary>
        /// <param name="saved"></param>
        public void DataSaved(bool saved)
        {
            dataSaved = saved;
            btnSave.Enabled = !saved;
        }
        private void txtNbr_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        private void cmbSeason_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }

        private void cmbMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            DataSaved(false);
        }
        /// <summary>Отметка источника финансирования
        /// Заплатин Е.Ф. 
        /// 10.09.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbtnIsBudget_CheckedChanged(object sender, EventArgs e)
        {
            demand.IsBudget = rbtnIsBudget.Checked;//Требуется изменение т.к. нужно гибко реагировать не дожидаясь сохранения изменений в форме
            rbtnNotBudget.Checked = !rbtnIsBudget.Checked;
            f = demand.IsBudget ? Finance.Budget : Finance.ExtraBudget;//уточняется источник финансирования
            DataSaved(false);
        }
        /// <summary>Удаление услуги по которой назначены медикаменты в требовании
        /// Заплатин Е.Ф. 
        /// 10.09.2011
        /// </summary>
        private void btnServiceDelete_Click(object sender, EventArgs e)
        {
            //Шаг 1.Проверка на возможность удаления медикаментов по остаткам
            if (mbds.Count > 0)
            {
                if (demand.ChekDesidue() != "")
                {
                    MessageBox.Show("Заменить услугу не возможно. Выписанные медикаменты уже частично реализованы при оказании услуг.",
                                    "Недоступность операции", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    btnServiсeSelect.Enabled = false;
                    return; //Удаление невозможно поэтому процедура прерывается.
                }
            }
            //Удаление медикаментов
            //Шаг 2. Предварительное удаление медикаментов ранее связанных по другой услуге
            mbds_deleted.AddRange(mbds);//Окончательное удаление будет произведено при сохранении требования.
            mbds.Clear();
            demand.ServiceID = 0;
            txtService.Text = "";//Добавляется наименование в строку
            rbtnIsBudget.Enabled = rbtnNotBudget.Enabled = true; 

            //Шаг 3. Обновление записей в таблице медикаментов
            gridMeds.DataSource = null;//Обнуление поля источника данных для присвоение нового значения
            gridMeds.DataSource = mbds;//Назначается новый источник данных
            DataSaved(false);
        }
        /// <summary>Открытие отчета по требованию для его распечатки
        /// Заплатин Е.Ф.
        /// 05.12.2011
        /// </summary>
        private void tbPrint_Click(object sender, EventArgs e)
        {
            rptDemand rpt = new rptDemand();
            rpt.Tag = new Command(CommandModes.Open, demand.DocumentID, this);
            rpt.ShowDialog(this);
        }

 
    }
}
