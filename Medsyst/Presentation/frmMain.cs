﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using NLog;


namespace Medsyst
{
    public partial class frmMain : Form
    {
        Logger logger = LogManager.GetLogger("Medsyst");

        public frmMain()
        {
            InitializeComponent();
        }
        
        private void splitContainer2_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void RegistratureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRegistrature childForm = new frmRegistrature();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void складToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSclad childForm = new frmSclad();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void ToolStripMenuAdd_Click(object sender, EventArgs e)
        {
            frmAmbulanceCard frm = new frmAmbulanceCard();
            frm.Tag = new Command(CommandModes.New);
            frm.MdiParent = this;
            frm.Show();


        }

        private void списокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmServiceList childForm = new frmServiceList(Finance.Budget);
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void категорииУслугToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmServiceCategory childForm = new frmServiceCategory();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void категорииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMedicamentCategory childForm = new frmMedicamentCategory();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void категорииПациентовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPatientCategoryList childForm = new frmPatientCategoryList();
            childForm.MdiParent = this;
            childForm.Show();
           
        }

        private void поставщикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProviderList childForm = new frmProviderList();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void плательщикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PayerList childForm = new PayerList();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void добавитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // проверка прав пользователя на выполнение функции
            if (!Security.Access(Groups.Admin, Groups.Manager, Groups.Stock))
                return; //выход из функции если у пользователя недостаточно прав

            frmReceiptOrder childForm = new frmReceiptOrder();
            childForm.MdiParent = this;
            childForm.Tag = new Command(CommandModes.New);
            childForm.Show();

        }

        private void каталогЛекарственныхСртвToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMedicamentCatalog childForm = new frmMedicamentCatalog();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void приходныеОрдераToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReceiptOrderList childForm = new frmReceiptOrderList();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOptions childForm = new frmOptions();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void btnScladOutput_Click(object sender, EventArgs e)
        {
                var childForm = new frmMedicamentOutput {MdiParent = this};
                childForm.Show();
        }


        private void tsmi_медикаментыПоУслугам_Click(object sender, EventArgs e)
        {
            frmDemandByServiceList childForm = new frmDemandByServiceList();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void MenuItemIHL_Click(object sender, EventArgs e)
        {
            frmIllnessHistoryList childForm = new frmIllnessHistoryList();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void MenuItemMeasure_Click(object sender, EventArgs e)
        {
            frmMeasure childForm = new frmMeasure();
            childForm.MdiParent = this;
            childForm.Show();

        }

        //24.03.10
        //Чирков Е.О.
        public static frmEmployeeList empForm = null;

        private void сотрудникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //24.03.10
            //Чирков Е.О.
            if (empForm != null)
            {
                empForm.Activate();
                return;
            }

            frmEmployeeList childForm = new frmEmployeeList();
            childForm.MdiParent = this;
            childForm.Show();

            //24.03.10
            //Чирков Е.О.
            empForm = childForm;
        }

        private void отчет3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptPatientBook childForm = new rptPatientBook();
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About childForm = new About();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void пользователиИПраваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsers childForm = new frmUsers();
            //childForm.MdiParent = this;
            childForm.tsbOk.Visible = false;
            childForm.Show();
        }

        private void сменитьПарольToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUserPasswordChange frm = new frmUserPasswordChange();
            frm.Tag = new Command(CommandModes.Edit, Security.user.UserID, this);
            frm.ShowDialog();
        }

        private void отметкаОказанныхУслугToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmServiceOutput childForm = new frmServiceOutput();
            childForm.Show();

        }

        private void приходрасходЗаПериодToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptMedicamentCharges childForm = new rptMedicamentCharges();
            childForm.Show();
        }
        /// <summary>Вызов формы отчета прихода/расхода медикаментов сгруппированных по приходным ордерам
        /// Заплатин Е.Ф.
        /// 23.11.2011
        /// </summary>
        private void приходрасходПоПриходнымиОрдерамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptMedicamentChargesByRecieptOrder childForm = new rptMedicamentChargesByRecieptOrder();
            childForm.Show();

        }

        private void приходрасходПоГруппамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptMedicamentChargesByGroup childForm = new rptMedicamentChargesByGroup();
            childForm.Show();
        }
        /// <summary>Вызов формы требований на медикаменты по назначениям
        /// Заплатин Е.Ф.
        /// 07.08.2011
        /// </summary>
        private void tsmi_медикаментыПоНазначениям_Click(object sender, EventArgs e)
        {
            frmDemandByMedicamentAndMaterialList frm = new frmDemandByMedicamentAndMaterialList();
            cmd = new Command(CommandModes.Open, (int)DocTypes.DemandMedicament);
            frm.MdiParent = this;
            frm.Tag = cmd;
            frm.Show();
        }
        /// <summary>Вызов формы требований на расходные материалы
        /// Заплатин Е.Ф.
        /// 07.08.2011
        /// </summary>
        private void tsmi_расходныеМатериалы_Click(object sender, EventArgs e)
        {
            frmDemandByMedicamentAndMaterialList frm = new frmDemandByMedicamentAndMaterialList();
            cmd = new Command(CommandModes.Open, (int)DocTypes.DemandMaterial);
            frm.MdiParent = this;
            frm.Tag = cmd;
            frm.Show();
        }
        /// <summary>Вызов формы управления классификатором МКБ кодов
        /// Заплатин Е.Ф.
        /// 24.01.2012
        /// </summary>
        private void мКБКлассификаторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMKBCode frm = new frmMKBCode();
            frm.MdiParent = this;
            frm.Show();
        }
        /// <summary>Вызов отчета по нозологическим формам
        /// Заплатин Е.Ф.
        /// 22.02.2012
        /// </summary>
        private void НозологическиеФормы_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptNosolog frm = new rptNosolog();
            frm.MdiParent = this;
            frm.Show();
        }
        /// <summary>Отчет - Журнал регистрации пациентов по сезонам
        /// Заплатин Е.Ф.
        /// 29.02.2012
        /// </summary>
        private void ЖурналРегистрации_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptIllnessHistoryList frm = new rptIllnessHistoryList();
            frm.Text = "Журнал регистрации пациентов";
            frm.MdiParent = this;
            frm.Show();
        }

        private void списокСтудентовПоФакультетамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptIllnessHistoryList frm = new rptIllnessHistoryList();
            frm.MdiParent = this;
            frm.Text = "Список студентов по факультетам";
            frm.Show();
        }

        private void нозологическиеФормыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rptNosolog frm = new rptNosolog();
            frm.MdiParent = this;
            frm.Show();
        }
        /// <summary>Список задач
        /// Заплатин Е.Ф.
        /// 04.07.2012
        /// </summary>
        private void задачиисполнителиуведомленияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTaskList frm = new frmTaskList();
            frm.MdiParent = this;
            frm.Show();
        }
        /// <summary>Список подразделений для редактирования
        /// Заплатин Е.Ф.
        /// 26.03.2013
        /// </summary>
        private void подразделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDivisionList frm = new frmDivisionList();
            frm.MdiParent = this;
            frm.Show();
        }

      

    }
}
