﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Medsyst
{
    public partial class frmUserAuthorization : Form
    {
        public frmUserAuthorization()
        {
            InitializeComponent();
        }

        ///<summary>Получение введенного логина</summary>
        public string Login
        {
            get { return txtLogin.Text; }
            set { txtLogin.Text = value; }
        }

        ///<summary>Получение введенного пароля</summary>
        public string Password
        {
            get { return txtPassword.Text; }
            set { txtPassword.Text = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool res = Security.Auth(Login, Password);
            
            lblHint.Visible = !res;
            
            if (res) DialogResult = DialogResult.OK;
            else
            {
                txtLogin.Text = "";
                txtPassword.Text = "";
                txtLogin.Focus();
            }
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {
            if (txtLogin.Text != "") lblHint.Visible = false;
        }

        private void frmUserAuthorization_Load(object sender, EventArgs e)
        {

        }
    }
}
