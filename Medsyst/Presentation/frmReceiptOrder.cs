﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Constant;
using Medsyst.Data.Entity;
using Medsyst.Services.Bl;


namespace Medsyst
{
    /// <summary>
    /// Форма приходного ордера
    /// </summary>
    public partial class frmReceiptOrder : Form
    {
        private InputMedicineCrudService _crudService = new InputMedicineCrudService();
        ReceiptOrder ro = new ReceiptOrder(true);
        ProviderList providers = new ProviderList();
        MedicamentInputList mis = new MedicamentInputList();
        Measures measures = new Measures();
        /// <summary> Флаг того, что приходный ордер является новым</summary>
        //bool bNew = false;//Кто и для чего создал это свойство? Почему не пишем комментарии. //Закоментировал ЗЕФ 28.09.2011f

        public frmReceiptOrder()
        {
            InitializeComponent();
        }

        private void frmMedicamentInput_Load(object sender, EventArgs e)
        {
            gridMeds.AutoGenerateColumns = false;
            
            //Настройка списка колонки единиц измерения
            measures.Load();
            colUnit.DisplayMember = "ShotName";
            colUnit.ValueMember = "MeasureID";
            colUnit.DataSource = measures;
            //
            providers.Load(CommandDirective.AddNonDefined);
            cmbProvider.DataSource = providers;

            
            ro.FormBinding("ro", this);//МЕВ: Привязка бизнес-объекта к форме 
            if (cmd.Mode == CommandModes.New) // Новый приходный ордер
            {
                ro.Nbr = ro.GetNewNumber(((int)DocTypes.ReciptOrder).ToString(), false);
                ro.RegisterD = dtpDate.Value = DateTime.Now; // Установка текущей даты создания и приходования ордера
                ro.AccountD = dtpAccountD.Value = DateTime.Now;
                if (!ro.Insert())
                {
                    MessageBox.Show("Не удалось добавть приходный ордер");
                    Close();//Закрывается форма приходного ордера
                }
                ro.WriteToForm();
                dataSaved = true;
       
            }
            else // Редатирование существующего приходного ордера
            {
                ro.DocumentID = cmd.Id;
                ro.Load();
                ro.WriteToForm();
                rbNotBudget.Checked = !ro.IsBudget;
                LoadMeds();
                SetFormControls();
                //dataSaved = false;
            }
        }
        /// <summary>В зависимости от состояния свойств: Signed устанавливается доступность элементов управления
        /// Заплатин Е.Ф.
        /// 10.05.2012
        /// </summary>
        private void SetFormControls()
        {
            if (ro.Signed)
            {//Документ был уже подписан, поэтому внесение изменений недопустимо
                //элементы управления по внесению изменений в подписанное требование становятся недоступны
                btnSign.Text = "Снять подпись";
                btnSelectProvider.Enabled = btnAdd_Medicine.Enabled = btnDel_Medicine.Enabled = false;
                colCount.ReadOnly = txtCommentMedicament.ReadOnly = txtNbr.ReadOnly = dtpDate.Enabled = txtComment.ReadOnly = true;
                dtpDate.Enabled = cmbProvider.Enabled = txtAccount.Enabled = dtpAccountD.Enabled = false;
                btnSave.Enabled = false;
                rbBudget.Enabled = rbNotBudget.Enabled = false;
            }
            else
            {
                btnSign.Text = "Подписать";
                btnSelectProvider.Enabled = btnAdd_Medicine.Enabled = btnDel_Medicine.Enabled = true;
                colCount.ReadOnly = txtCommentMedicament.ReadOnly = txtNbr.ReadOnly = dtpDate.Enabled = txtComment.ReadOnly = false;
                dtpDate.Enabled = cmbProvider.Enabled = txtAccount.Enabled = dtpAccountD.Enabled = true;
                btnSave.Enabled = true;
                rbBudget.Enabled = rbNotBudget.Enabled = true;
            }
        }
        /// <summary>Заполнение грида медикаментами
        /// Запалтин Е.Ф.
        /// 03.12.2011
        /// </summary>
        void LoadMeds()
        {
            //if (ro.DocumentID <= 0) return;
            gridMeds.DataSource = null;
            mis.Load(ro.DocumentID);
            gridMeds.DataSource = mis;
            ReCalc();
        }

        private void ScladInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!dataSaved)
            {//Данные в форме были изменены
                DialogResult res = MessageBox.Show(this, "Сохранить изменения?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {
                    if (cmd.Mode == CommandModes.New)
                    {//Вновь созданный приходный ордер
                        ro.ReadFromForm();
                        MedicamentInput m = new MedicamentInput(true);
                        for (int i = 0; i < mis.Count; i++)
                        {
                            m.MedicamentInputID = mis[i].MedicamentInputID;
                            m.Load();
                            m.IsBudget = rbBudget.Checked;
                            m.Update();
                        }
                        ro.Update();
                    }
                    else if (cmd.Mode == CommandModes.Edit)
                    {//Приходный ордер был открыт для редактирования
                        ro.ReadFromForm();
                        MedicamentInput m = new MedicamentInput(true);
                        for (int i = 0; i < mis.Count; i++)
                        {
                            m.MedicamentInputID = mis[i].MedicamentInputID;
                            m.Load();
                            m.IsBudget = rbBudget.Checked;
                            m.Update();
                        }
                        ro.Update();
                    }
                }
                else if (res == DialogResult.No)
                {
                    if (cmd.Mode == CommandModes.New)
                    {//Вновь созданный приходный ордер
                        ro.Delete(CommandDirective.MarkAsDeleted); //Отметка ордера как удаленного
                    }
                    //Для открытого ордера для редактирования ничего не делаем
                }
                else if (res == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
            
            cmd.UpdateParent();
        }

        private void btnSelectProvider_Click(object sender, EventArgs e)
        {
            frmProviderList frm = new frmProviderList();
            frm.Tag = new Command(CommandModes.Select);
            frm.ShowDialog(this);
            if (frm.Tag is Command)
            {
                Command c = (Command)frm.Tag;
                if (c.Mode != CommandModes.Return) return;
                ro.ProviderID = c.Id;
                cmbProvider.SelectedValue = ro.ProviderID;
            }
        }
        /// <summary>Сохранение изменений в приходном ордере
        /// 
        /// </summary>
        private void Save()
        {
            if (cmd.Id > 0)
            {
                ro.ReadFromForm();
                MedicamentInput m = new MedicamentInput(true);
                for (int i = 0; i < mis.Count; i++)
                {
                    m.MedicamentInputID = mis[i].MedicamentInputID;
                    m.Load();
                    m.IsBudget = rbBudget.Checked;
                    m.Update();
                }
                if (ro.Update())
                {
                    cmd.UpdateParent();
                    dataSaved = true;
                    btnSave.Enabled = false;
                }
                else
                    MessageBox.Show("Не удалось сохранить приходный ордер.", "Ошибка обновления",
                                     MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnToSclad()
        {
            ro.AccountD = DateTime.Now;
            if (!ro.Update())
                MessageBox.Show("Не удалось сохранить приходный ордер");
            else
            {
                if(mis.Debit())
                    MessageBox.Show("Медикаменты приходованы");
                else
                    MessageBox.Show("Не удалось приходовать медикаменты");
            }
        }

 
        //Обработчик завершения редактирования записи в списке приходуемых медикаментов
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Подгружается запись оприходованного медикамента по идентификатору редактируемой строки в таблице
            MedicamentInput m = new MedicamentInput(true);
            m.MedicamentInputID = mis[e.RowIndex].MedicamentInputID;
            m.Load();
            
            int BeforeCount = m.Count; // Сохраняется количестов медикамента до изменения
            //Если редактировалось поле с количеством, то обновляется количество
            if (e.ColumnIndex == 3)
                m.Count = (int)gridMeds[e.ColumnIndex, e.RowIndex].Value;
            //Если модифицировалось исключительно поле цены, то изменяется цена
            if (e.ColumnIndex == 4)
                m.Price = (decimal)gridMeds[e.ColumnIndex, e.RowIndex].Value;
            //Производится попытка обновления измененных данных
            if (!m.Update()) //Если неудачно
                MessageBox.Show("Не удалось обновить медикамент из приходного ордера");
            else 
            {//Успешно обновился, то необходимо пересчитать количество медикамента на складе
                bool IsUpdateSclad = false;//Почему не комментируется переменная?
                //Создается список медикаментов на складе. Непонятно зачем генерировать список, т.к. 
                //одной приходной записи соответсвует одна складская запись?
                MedicamentOnScladList mons = new MedicamentOnScladList();
                //Скорее всего генерируется список складских позиций по приходному медикаменту
                mons.LoadOfInputID(m.MedicamentInputID);
                if (mons.Count > 0) //Если возвращена хотя бы одна запись
                {
                    //Инициализируется складской медикамент по первой записи
                    MedicamentOnSclad mmm = new MedicamentOnSclad(true);
                    mmm.MedicamentOnScladID = mons[0].MedicamentOnScladID;
                    if (mmm.Load()) //Попытка загрузки складской позиции
                    {
                        //Кооректируется количество в связи с внесенными изменениями
                        mmm.CountOnSclad += m.Count - BeforeCount;
                        //Обновляется запись со скорректированными значениями количества
                        IsUpdateSclad = mmm.Update();
                    }
                }
                if (!IsUpdateSclad) //Если обновления не произошло выдается сообщение
                    MessageBox.Show(this, "Не удалось обновить количество на складе", "Обновление склада", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //Обновляются значения количества и стоимости в таблице оприходованых медикаментов в форме
                mis[e.RowIndex].Count = m.Count;
                mis[e.RowIndex].Price = m.Price;
                //Расчет суммарной стоимости
                gridMeds[5, e.RowIndex].Value = m.Price * (decimal)m.Count;
                //Пересчитывается таблица
                ReCalc();
            }

        }

        /// <summary> Расчет общей стоимости
        /// Марьенков Е.В.
        /// </summary>
        void ReCalc()
        {
            decimal s=0;
            for (int i = 0; i < mis.Count; i++)
            {
                s += mis[i].Price * mis[i].Count;
            }
            txtAllPrice.Text = s.ToString("0.00р.");
            ro.PriceRO = s;
        }

        private void tbNew_Click(object sender, EventArgs e)
        {
            ro = new ReceiptOrder(true);
            ro.FormBinding("ro", this);
            ro.Nbr = ro.GetNewNbr()+1;
            ro.WriteToForm();
            if (!ro.Insert())
                MessageBox.Show("Не удалось создать новый приходный ордер");
            else
            {
                LoadMeds();
                Tag = ro.DocumentID;
                dataSaved = true;
            }
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows != null && gridMeds.SelectedRows.Count > 0)
            {
                if (mis[gridMeds.SelectedRows[0].Index].CommentInput != txtCommentMedicament.Text)
                {
                    MedicamentInput m = new MedicamentInput(true);
                    m.MedicamentInputID = mis[gridMeds.SelectedRows[0].Index].MedicamentInputID;
                    m.Load();
                    m.CommentInput = txtCommentMedicament.Text;
                    if (m.Update())
                    {
                        mis[gridMeds.SelectedRows[0].Index].CommentInput = m.CommentInput;
                    }
                }
            }
        }

        private void gridMeds_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex >= 0) //Выбрана какая либо запись с медикаментом
                txtCommentMedicament.Text = mis[e.RowIndex].CommentInput;
        }
        //
        /// <summary>Добавление в приходный ордер и склад медикамента выбранного из каталога
        /// Заплатин Е.Ф.
        /// 15.0.2012
        /// </summary>
        private void Add_Medicine_Click(object sender, EventArgs e)
        {
            //Готовится к открытию каталог медикаментов
            frmMedicamentCatalog frm = new frmMedicamentCatalog();
            //Указывает, что открытие формы производится в режиме выбора
            frm.Tag = new Command(CommandModes.Select);
            frm.ShowDialog(this);
            
            //Генерируется переменная в которую возвращается свойство Tag закрываемого каталога медикаментов
            Command cmd = (Command)frm.Tag;
            //Выход из обработчика, если форма была закрыта без выбора или не возвращен идентификатор каталожного элемента
            if (cmd == null || cmd.Id <= 0) return;
            //Выбор медикамента из каталога произошел успешно и добавляется новая запись в приходном ордере
            MedicamentInput mi = new MedicamentInput(true);
            //Создание нового приходного медикамента
            mi.New(cmd.Id, ro.DocumentID, ro.DocTypeID);
            dataSaved = false;
            LoadMeds();

        }
        /// <summary>
        /// Удаление медикамента
        /// </summary>
        private void Del_Medicine_Click(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows != null && gridMeds.SelectedRows.Count > 0)
            {
                var result = _crudService.Delete(mis[gridMeds.SelectedRows[0].Index].MedicamentInputID,
                                            CommandDirective.MarkAsDeleted);
                
                if (result.Result != ProcessingResults.Successfull)
                {
                    MessageBox.Show("Удаление приходного медикамента", result.Description,
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    LoadMeds();
                }
            }
        }
        ///<summary>Кнопка "Подписать"
        ///Заплатин Е.Ф.
        ///24.08.2011</summary>
        private void btnSign_Click(object sender, EventArgs e)
        {
            if (ro.Signed)
            {//Документ уже был подписан
                ro.Sign();//Снятие подписи
                SetFormControls();//Элементы управления делаются доступными
            }
            else
            {
                //Прежде чем закрывать форму, поставить подпись, подтвердающую завершение формирование требования.
                //После того как будет поставлена подпись, внесение изменений станет невозможным
                DialogResult res = MessageBox.Show("Выберите - Да, если Вы уверены в том, что хотите подписать и сохранить приходный ордер. " +
                            " После наложения подписи внесение изменений в приходном ордере и его удаление станет невозможным!\n\r" +
                            " Выберите - Нет, для сохранения изменений и завершения редактирования приходного ордера без наложения подписи.\n\r" +
                            " Выберите - Отмена, для продолжения редактирования приходного ордера.", "Подпись документа",
                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {//Сохранение изменений с подписью
                    ro.Sign();
                }
                else if (res == DialogResult.No)
                {//Сохранение документа без подписи

                }
                else if (res == DialogResult.Cancel)
                {//Продолжение редактирования документа
                    return;
                }
                Save();
                Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        //Заплатин Е.Ф.
        //28.09.2011
        #region DataChanged 
        private void txtNbr_TextChanged(object sender, EventArgs e)
        {
            dataSaved = false;
            btnSave.Enabled = true;
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            dataSaved = false;
            btnSave.Enabled = true;
        }

        private void rbBudget_CheckedChanged(object sender, EventArgs e)
        {
            dataSaved = false;
            btnSave.Enabled = true;
        }

        private void txtProvider_TextChanged(object sender, EventArgs e)
        {
            dataSaved = false;
            btnSave.Enabled = true;
        }
        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            dataSaved = false;
            btnSave.Enabled = true;
        }
#endregion DataChanged;


   
    }
}
