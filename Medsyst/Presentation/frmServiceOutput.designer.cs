﻿using Medsyst.DataSet;

namespace Medsyst
{
    partial class frmServiceOutput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmServiceOutput));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridOutDocuments = new System.Windows.Forms.DataGridView();
            this.colDocID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tbPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnFilter = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabDocOutType = new System.Windows.Forms.TabControl();
            this.tabPageIH = new System.Windows.Forms.TabPage();
            this.groupFilterIH = new System.Windows.Forms.GroupBox();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.cmbCategoryPacient = new System.Windows.Forms.ComboBox();
            this.personPatientCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.labelFilter3 = new System.Windows.Forms.Label();
            this.labelFilter2 = new System.Windows.Forms.Label();
            this.labelFilter1 = new System.Windows.Forms.Label();
            this.tabPageDemand = new System.Windows.Forms.TabPage();
            this.groupFilterDemand = new System.Windows.Forms.GroupBox();
            this.cmbSeasonDemand = new System.Windows.Forms.ComboBox();
            this.intYearDemand = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbShowFulfil = new System.Windows.Forms.RadioButton();
            this.rbShowAll = new System.Windows.Forms.RadioButton();
            this.rbShowNotFulfil = new System.Windows.Forms.RadioButton();
            this.gridSrvs = new System.Windows.Forms.DataGridView();
            this.colSrvOutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFulfil = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personPatientCategoryTableAdapter = new Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter();
            this.toolStripAdmin = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripShowId = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridOutDocuments)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabDocOutType.SuspendLayout();
            this.tabPageIH.SuspendLayout();
            this.groupFilterIH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            this.tabPageDemand.SuspendLayout();
            this.groupFilterDemand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intYearDemand)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSrvs)).BeginInit();
            this.SuspendLayout();
            // 
            // gridOutDocuments
            // 
            this.gridOutDocuments.AllowUserToAddRows = false;
            this.gridOutDocuments.AllowUserToDeleteRows = false;
            this.gridOutDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridOutDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOutDocuments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDocID,
            this.colNum,
            this.colDesc,
            this.colDate,
            this.colSum,
            this.colDoctor});
            this.gridOutDocuments.Location = new System.Drawing.Point(-3, 155);
            this.gridOutDocuments.Name = "gridOutDocuments";
            this.gridOutDocuments.ReadOnly = true;
            this.gridOutDocuments.RowHeadersVisible = false;
            this.gridOutDocuments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridOutDocuments.Size = new System.Drawing.Size(473, 391);
            this.gridOutDocuments.TabIndex = 70;
            this.gridOutDocuments.SelectionChanged += new System.EventHandler(this.gridOutDocuments_SelectionChanged);
            // 
            // colDocID
            // 
            this.colDocID.FillWeight = 30F;
            this.colDocID.HeaderText = "ID";
            this.colDocID.Name = "colDocID";
            this.colDocID.ReadOnly = true;
            this.colDocID.Visible = false;
            this.colDocID.Width = 30;
            // 
            // colNum
            // 
            this.colNum.DataPropertyName = "Season";
            this.colNum.FillWeight = 50F;
            this.colNum.HeaderText = "№";
            this.colNum.MinimumWidth = 20;
            this.colNum.Name = "colNum";
            this.colNum.ReadOnly = true;
            this.colNum.Width = 50;
            // 
            // colDesc
            // 
            this.colDesc.DataPropertyName = "FullName";
            this.colDesc.FillWeight = 200F;
            this.colDesc.HeaderText = "ФИО";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            this.colDesc.Width = 200;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Date";
            this.colDate.FillWeight = 60F;
            this.colDate.HeaderText = "Дата";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            this.colDate.Width = 60;
            // 
            // colSum
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.colSum.DefaultCellStyle = dataGridViewCellStyle1;
            this.colSum.HeaderText = "Сумма";
            this.colSum.Name = "colSum";
            this.colSum.ReadOnly = true;
            this.colSum.Width = 65;
            // 
            // colDoctor
            // 
            this.colDoctor.DataPropertyName = "DoctorName";
            this.colDoctor.HeaderText = "Терапевт";
            this.colDoctor.Name = "colDoctor";
            this.colDoctor.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbPrint,
            this.toolStripAdmin});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(170, 27);
            this.toolStrip1.TabIndex = 2;
            // 
            // tbPrint
            // 
            this.tbPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbPrint.Image")));
            this.tbPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbPrint.Name = "tbPrint";
            this.tbPrint.Size = new System.Drawing.Size(70, 24);
            this.tbPrint.Text = "Печать";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(948, 550);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(948, 577);
            this.toolStripContainer1.TabIndex = 76;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridOutDocuments);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilter);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.tabDocOutType);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.gridSrvs);
            this.splitContainer1.Size = new System.Drawing.Size(948, 550);
            this.splitContainer1.SplitterDistance = 472;
            this.splitContainer1.TabIndex = 76;
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilter.Location = new System.Drawing.Point(3, 127);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(153, 23);
            this.btnFilter.TabIndex = 78;
            this.btnFilter.Text = "Применить фильтр";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Расходные документы";
            // 
            // tabDocOutType
            // 
            this.tabDocOutType.Controls.Add(this.tabPageIH);
            this.tabDocOutType.Controls.Add(this.tabPageDemand);
            this.tabDocOutType.Location = new System.Drawing.Point(4, 21);
            this.tabDocOutType.Name = "tabDocOutType";
            this.tabDocOutType.SelectedIndex = 0;
            this.tabDocOutType.Size = new System.Drawing.Size(466, 99);
            this.tabDocOutType.TabIndex = 86;
            this.tabDocOutType.SelectedIndexChanged += new System.EventHandler(this.tabDocOutType_SelectedIndexChanged);
            // 
            // tabPageIH
            // 
            this.tabPageIH.Controls.Add(this.groupFilterIH);
            this.tabPageIH.Location = new System.Drawing.Point(4, 22);
            this.tabPageIH.Name = "tabPageIH";
            this.tabPageIH.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPageIH.Size = new System.Drawing.Size(458, 73);
            this.tabPageIH.TabIndex = 0;
            this.tabPageIH.Tag = "4";
            this.tabPageIH.Text = "Истории болезней";
            this.tabPageIH.UseVisualStyleBackColor = true;
            // 
            // groupFilterIH
            // 
            this.groupFilterIH.Controls.Add(this.cmbSeason);
            this.groupFilterIH.Controls.Add(this.numYear);
            this.groupFilterIH.Controls.Add(this.cmbCategoryPacient);
            this.groupFilterIH.Controls.Add(this.labelFilter3);
            this.groupFilterIH.Controls.Add(this.labelFilter2);
            this.groupFilterIH.Controls.Add(this.labelFilter1);
            this.groupFilterIH.Location = new System.Drawing.Point(6, 3);
            this.groupFilterIH.Name = "groupFilterIH";
            this.groupFilterIH.Size = new System.Drawing.Size(445, 65);
            this.groupFilterIH.TabIndex = 85;
            this.groupFilterIH.TabStop = false;
            this.groupFilterIH.Text = "Фильтр по историям болезней";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "SeasonNbr";
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(356, 38);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(82, 21);
            this.cmbSeason.TabIndex = 88;
            this.cmbSeason.ValueMember = "SeasonID";
            // 
            // numYear
            // 
            this.numYear.Location = new System.Drawing.Point(356, 13);
            this.numYear.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.numYear.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.numYear.Name = "numYear";
            this.numYear.Size = new System.Drawing.Size(82, 20);
            this.numYear.TabIndex = 87;
            this.numYear.Value = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.numYear.ValueChanged += new System.EventHandler(this.numYear_ValueChanged);
            // 
            // cmbCategoryPacient
            // 
            this.cmbCategoryPacient.DataSource = this.personPatientCategoryBindingSource;
            this.cmbCategoryPacient.DisplayMember = "CategoryN";
            this.cmbCategoryPacient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategoryPacient.FormattingEnabled = true;
            this.cmbCategoryPacient.Location = new System.Drawing.Point(126, 14);
            this.cmbCategoryPacient.Name = "cmbCategoryPacient";
            this.cmbCategoryPacient.Size = new System.Drawing.Size(168, 21);
            this.cmbCategoryPacient.TabIndex = 86;
            this.cmbCategoryPacient.ValueMember = "CategoryID";
            // 
            // personPatientCategoryBindingSource
            // 
            this.personPatientCategoryBindingSource.DataMember = "PersonPatientCategory";
            this.personPatientCategoryBindingSource.DataSource = this.medsystDataSet;
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelFilter3
            // 
            this.labelFilter3.AutoSize = true;
            this.labelFilter3.Location = new System.Drawing.Point(4, 16);
            this.labelFilter3.Name = "labelFilter3";
            this.labelFilter3.Size = new System.Drawing.Size(116, 13);
            this.labelFilter3.TabIndex = 85;
            this.labelFilter3.Text = "Категория пациентов";
            // 
            // labelFilter2
            // 
            this.labelFilter2.AutoSize = true;
            this.labelFilter2.Location = new System.Drawing.Point(312, 41);
            this.labelFilter2.Name = "labelFilter2";
            this.labelFilter2.Size = new System.Drawing.Size(38, 13);
            this.labelFilter2.TabIndex = 83;
            this.labelFilter2.Text = "Сезон";
            // 
            // labelFilter1
            // 
            this.labelFilter1.AutoSize = true;
            this.labelFilter1.Location = new System.Drawing.Point(312, 15);
            this.labelFilter1.Name = "labelFilter1";
            this.labelFilter1.Size = new System.Drawing.Size(25, 13);
            this.labelFilter1.TabIndex = 80;
            this.labelFilter1.Text = "Год";
            // 
            // tabPageDemand
            // 
            this.tabPageDemand.Controls.Add(this.groupFilterDemand);
            this.tabPageDemand.Location = new System.Drawing.Point(4, 22);
            this.tabPageDemand.Name = "tabPageDemand";
            this.tabPageDemand.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPageDemand.Size = new System.Drawing.Size(458, 73);
            this.tabPageDemand.TabIndex = 1;
            this.tabPageDemand.Tag = "3";
            this.tabPageDemand.Text = "Платные услуги";
            this.tabPageDemand.UseVisualStyleBackColor = true;
            // 
            // groupFilterDemand
            // 
            this.groupFilterDemand.Controls.Add(this.cmbSeasonDemand);
            this.groupFilterDemand.Controls.Add(this.intYearDemand);
            this.groupFilterDemand.Controls.Add(this.label1);
            this.groupFilterDemand.Controls.Add(this.label4);
            this.groupFilterDemand.Location = new System.Drawing.Point(6, 3);
            this.groupFilterDemand.Name = "groupFilterDemand";
            this.groupFilterDemand.Size = new System.Drawing.Size(452, 65);
            this.groupFilterDemand.TabIndex = 86;
            this.groupFilterDemand.TabStop = false;
            this.groupFilterDemand.Text = "Фильтр";
            // 
            // cmbSeasonDemand
            // 
            this.cmbSeasonDemand.DisplayMember = "SeasonNbr";
            this.cmbSeasonDemand.FormattingEnabled = true;
            this.cmbSeasonDemand.Location = new System.Drawing.Point(356, 38);
            this.cmbSeasonDemand.Name = "cmbSeasonDemand";
            this.cmbSeasonDemand.Size = new System.Drawing.Size(82, 21);
            this.cmbSeasonDemand.TabIndex = 92;
            this.cmbSeasonDemand.ValueMember = "SeasonID";
            // 
            // intYearDemand
            // 
            this.intYearDemand.Location = new System.Drawing.Point(356, 13);
            this.intYearDemand.Maximum = new decimal(new int[] {
            2120,
            0,
            0,
            0});
            this.intYearDemand.Minimum = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            this.intYearDemand.Name = "intYearDemand";
            this.intYearDemand.Size = new System.Drawing.Size(82, 20);
            this.intYearDemand.TabIndex = 91;
            this.intYearDemand.Value = new decimal(new int[] {
            2009,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(312, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "Сезон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(312, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 89;
            this.label4.Text = "Год";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rbShowFulfil);
            this.groupBox1.Controls.Add(this.rbShowAll);
            this.groupBox1.Controls.Add(this.rbShowNotFulfil);
            this.groupBox1.Location = new System.Drawing.Point(3, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 72);
            this.groupBox1.TabIndex = 89;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр по услугам";
            // 
            // rbShowFulfil
            // 
            this.rbShowFulfil.AutoSize = true;
            this.rbShowFulfil.Location = new System.Drawing.Point(17, 49);
            this.rbShowFulfil.Name = "rbShowFulfil";
            this.rbShowFulfil.Size = new System.Drawing.Size(133, 17);
            this.rbShowFulfil.TabIndex = 92;
            this.rbShowFulfil.Text = "Показать оказанные";
            this.rbShowFulfil.UseVisualStyleBackColor = true;
            this.rbShowFulfil.CheckedChanged += new System.EventHandler(this.rbShowFulfil_CheckedChanged);
            // 
            // rbShowAll
            // 
            this.rbShowAll.AutoSize = true;
            this.rbShowAll.Checked = true;
            this.rbShowAll.Location = new System.Drawing.Point(17, 14);
            this.rbShowAll.Name = "rbShowAll";
            this.rbShowAll.Size = new System.Drawing.Size(95, 17);
            this.rbShowAll.TabIndex = 94;
            this.rbShowAll.TabStop = true;
            this.rbShowAll.Text = "Показать все";
            this.rbShowAll.UseVisualStyleBackColor = true;
            this.rbShowAll.CheckedChanged += new System.EventHandler(this.rbShowAll_CheckedChanged);
            // 
            // rbShowNotFulfil
            // 
            this.rbShowNotFulfil.AutoSize = true;
            this.rbShowNotFulfil.Location = new System.Drawing.Point(17, 31);
            this.rbShowNotFulfil.Name = "rbShowNotFulfil";
            this.rbShowNotFulfil.Size = new System.Drawing.Size(151, 17);
            this.rbShowNotFulfil.TabIndex = 93;
            this.rbShowNotFulfil.Text = "Показать не оказанные ";
            this.rbShowNotFulfil.UseVisualStyleBackColor = true;
            this.rbShowNotFulfil.CheckedChanged += new System.EventHandler(this.rbShowNotFulfil_CheckedChanged);
            // 
            // gridSrvs
            // 
            this.gridSrvs.AllowUserToAddRows = false;
            this.gridSrvs.AllowUserToDeleteRows = false;
            this.gridSrvs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSrvs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSrvs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSrvOutID,
            this.colFulfil,
            this.colNbr,
            this.colName,
            this.colCount,
            this.colPrice});
            this.gridSrvs.Location = new System.Drawing.Point(6, 155);
            this.gridSrvs.MultiSelect = false;
            this.gridSrvs.Name = "gridSrvs";
            this.gridSrvs.RowHeadersVisible = false;
            this.gridSrvs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSrvs.Size = new System.Drawing.Size(466, 391);
            this.gridSrvs.TabIndex = 75;
            this.gridSrvs.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSrvs_CellContentClick);
            // 
            // colSrvOutID
            // 
            this.colSrvOutID.DataPropertyName = "ServiceOutputID";
            this.colSrvOutID.FillWeight = 50F;
            this.colSrvOutID.HeaderText = "ID";
            this.colSrvOutID.Name = "colSrvOutID";
            this.colSrvOutID.Visible = false;
            this.colSrvOutID.Width = 50;
            // 
            // colFulfil
            // 
            this.colFulfil.DataPropertyName = "Fulfil";
            this.colFulfil.FalseValue = "0";
            this.colFulfil.FillWeight = 60F;
            this.colFulfil.HeaderText = "Оказана";
            this.colFulfil.Name = "colFulfil";
            this.colFulfil.ReadOnly = true;
            this.colFulfil.TrueValue = "1";
            this.colFulfil.Width = 60;
            // 
            // colNbr
            // 
            this.colNbr.DataPropertyName = "ServiceID";
            this.colNbr.FillWeight = 30F;
            this.colNbr.HeaderText = "№";
            this.colNbr.Name = "colNbr";
            this.colNbr.ReadOnly = true;
            this.colNbr.Width = 30;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "ServiceN";
            this.colName.FillWeight = 240F;
            this.colName.HeaderText = "Наименование";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 240;
            // 
            // colCount
            // 
            this.colCount.DataPropertyName = "Count";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colCount.DefaultCellStyle = dataGridViewCellStyle2;
            this.colCount.FillWeight = 30F;
            this.colCount.HeaderText = "Кол-во";
            this.colCount.Name = "colCount";
            this.colCount.ReadOnly = true;
            this.colCount.Width = 30;
            // 
            // colPrice
            // 
            this.colPrice.DataPropertyName = "PriceSum";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.colPrice.DefaultCellStyle = dataGridViewCellStyle3;
            this.colPrice.HeaderText = "Цена";
            this.colPrice.Name = "colPrice";
            this.colPrice.ReadOnly = true;
            // 
            // personPatientCategoryTableAdapter
            // 
            this.personPatientCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // toolStripAdmin
            // 
            this.toolStripAdmin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAdmin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripShowId});
            this.toolStripAdmin.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAdmin.Image")));
            this.toolStripAdmin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAdmin.Name = "toolStripAdmin";
            this.toolStripAdmin.Size = new System.Drawing.Size(57, 24);
            this.toolStripAdmin.Text = "Админ";
            // 
            // toolStripShowId
            // 
            this.toolStripShowId.CheckOnClick = true;
            this.toolStripShowId.Name = "toolStripShowId";
            this.toolStripShowId.Size = new System.Drawing.Size(289, 22);
            this.toolStripShowId.Text = "Показать идентификаторы документов";
            this.toolStripShowId.Click += new System.EventHandler(this.показатьИдентификаторыДокументовToolStripMenuItem_Click);
            // 
            // frmServiceOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 577);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmServiceOutput";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отметка оказанных услуги";
            this.Load += new System.EventHandler(this.frmServiceOutput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridOutDocuments)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabDocOutType.ResumeLayout(false);
            this.tabPageIH.ResumeLayout(false);
            this.groupFilterIH.ResumeLayout(false);
            this.groupFilterIH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            this.tabPageDemand.ResumeLayout(false);
            this.groupFilterDemand.ResumeLayout(false);
            this.groupFilterDemand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intYearDemand)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSrvs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tbPrint;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label labelFilter2;
        private System.Windows.Forms.GroupBox groupFilterIH;
        private System.Windows.Forms.DataGridView gridOutDocuments;
        private System.Windows.Forms.ComboBox cmbCategoryPacient;
        private System.Windows.Forms.Label labelFilter3;
        private System.Windows.Forms.Label labelFilter1;
        private System.Windows.Forms.TabControl tabDocOutType;
        private System.Windows.Forms.TabPage tabPageIH;
        private System.Windows.Forms.TabPage tabPageDemand;
        private System.Windows.Forms.GroupBox groupFilterDemand;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.ComboBox cmbSeasonDemand;
        private System.Windows.Forms.NumericUpDown intYearDemand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView gridSrvs;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDocID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDoctor;
        private System.Windows.Forms.RadioButton rbShowAll;
        private System.Windows.Forms.RadioButton rbShowNotFulfil;
        private System.Windows.Forms.RadioButton rbShowFulfil;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSrvOutID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colFulfil;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrice;
        private MedsystDataSet medsystDataSet;
        private System.Windows.Forms.BindingSource personPatientCategoryBindingSource;
        private Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter personPatientCategoryTableAdapter;
        [sec(new Groups[] { Groups.Admin }, Hide = true)]
        private System.Windows.Forms.ToolStripDropDownButton toolStripAdmin;
        private System.Windows.Forms.ToolStripMenuItem toolStripShowId;
    }
}