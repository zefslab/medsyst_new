﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.DataSet;
using Medsyst.DataSet.datNozologTableAdapters;


namespace Medsyst
{
    public partial class rptNosolog : Form
    {
        Seasons ssBegin= new Seasons(); //Коллекция для списка выбора начального сезонасезона
        Seasons ssEnd = new Seasons(); //Коллекция для списка выбора конечного сезонасезона
        Season SeasonBegin = new Season();
        Season SeasonEnd = new Season();
        //Инициализируется адаптеры с последующим присвоением занчений параметрам
        NosoologMKBTableAdapter NosoologMKB_TA = new NosoologMKBTableAdapter(); 
        NosoologPart2TableAdapter NosoologPart2_TA = new NosoologPart2TableAdapter();
        NosoologCountTableAdapter NosoologCount_TA = new NosoologCountTableAdapter();
        datNozolog DS = new datNozolog(); //Строго типизированный набор данных

        public rptNosolog()
        {
            InitializeComponent();
        }

        private void rptNosolog_Load(object sender, EventArgs e)
        {
            nosolog1.Load(); //Загружается отчет
            //Назначение строк подключения адаптерам
            NosoologMKB_TA.Connection.ConnectionString = DB.CS;
            NosoologPart2_TA.Connection.ConnectionString = DB.CS;
            NosoologCount_TA.Connection.ConnectionString = DB.CS;

            //Загрузка коллекций сезонов для списков выбора
            ssBegin.Load(CommandDirective.Non); //Загрузка списка сезонов
            cmbSeasonBegin.DataSource = ssBegin;
            ssEnd.Load(CommandDirective.Non); //Загрузка списка сезонов для второго сезона
            cmbSeasonEnd.DataSource = ssEnd;
            
            //Загрузка установленных значений начального и конечного сезонов
            SeasonBegin.SeasonID = (int)cmbSeasonBegin.SelectedValue;
            SeasonBegin.Load();
            SeasonEnd.SeasonID = (int)cmbSeasonEnd.SelectedValue;
            SeasonEnd.Load();
            
            //Перенесен обработчик прерывания в это место, чтобы не срабатывал при заполнении списков сезонов
            cmbSeasonBegin.SelectedIndexChanged += new System.EventHandler(this.cmbSeasonBegin_SelectedIndexChanged);
            cmbSeasonEnd.SelectedIndexChanged += new System.EventHandler(this.cmbSeasonEnd_SelectedIndexChanged);

        }

        private void btnExecutReport_Click(object sender, EventArgs e)
        {
            NosoologMKB_TA.Fill(DS.NosoologMKB, SeasonBegin.SeasonNbr, SeasonEnd.SeasonNbr, SeasonBegin.Year, Convert.ToInt32(rbtnStudents.Checked));
            NosoologPart2_TA.Fill(DS.NosoologPart2, SeasonBegin.SeasonNbr, SeasonEnd.SeasonNbr, SeasonBegin.Year, Convert.ToInt32(rbtnStudents.Checked));
            NosoologCount_TA.Fill(DS.NosoologCount, SeasonBegin.SeasonNbr, SeasonEnd.SeasonNbr, SeasonBegin.Year, Convert.ToInt32(rbtnStudents.Checked));

            nosolog1.SetDataSource(DS); //передача отчету заполненного экземпляра набора данных - DS

            //Передача в отчет вспомогательных параметров для оформления  отчета
            nosolog1.SetParameterValue("SeasonBeginNbr", SeasonBegin.SeasonNbr);
            nosolog1.SetParameterValue("SeasonEndNbr", SeasonEnd.SeasonNbr);
            nosolog1.SetParameterValue("DateBegin", SeasonBegin.SeasonOpen);
            nosolog1.SetParameterValue("DateEnd", SeasonEnd.SeasonClose);
            nosolog1.SetParameterValue("Year", SeasonBegin.Year);
            nosolog1.SetParameterValue("Students", rbtnStudents.Checked);
            
            crystalReportViewer1.ReportSource = nosolog1; //Элементу формы просмотра отчетов передается загруженный отчет

        }
        /// <summary>Проверяется корректность выбора конечного периода сезона
        /// Заплатин Е.Ф.
        /// 26.02.2012
        /// </summary>
        private void cmbSeasonEnd_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ssEnd[cmbSeasonEnd.SelectedIndex].Year != SeasonBegin.Year)
            {// Не допускается устанавливать сезоны начала и конца периода в разных годах
                MessageBox.Show("Не возможно указывать сезоны из разных годов", "Некорректный выбор сезона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                cmbSeasonEnd.SelectedValue = SeasonEnd.SeasonID;//Восстанавливается прежнее занчение сезона конца периода
                return;
            }
            if (ssEnd[cmbSeasonEnd.SelectedIndex].SeasonNbr < SeasonBegin.SeasonNbr)
            {// Не допускается устанавливать сезоны начала периода позже сезона конца периода
                MessageBox.Show("Не возможно указывать конечный сезон позже начального", "Некорректный выбор сезона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                cmbSeasonEnd.SelectedValue = SeasonEnd.SeasonID;//Восстанавливается прежнее занчение сезона конца периода
                return;
            }
            //Устанавливается новое значение конечного сезона
            SeasonEnd = ssEnd[cmbSeasonEnd.SelectedIndex];
        }
        /// <summary>Проверяется корректность выбора начального периода сезона
        /// Заплатин Е.Ф.
        /// 26.02.2012
        /// </summary>
        private void cmbSeasonBegin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ssBegin[cmbSeasonBegin.SelectedIndex].Year != SeasonEnd.Year)
            {// Не допускается устанавливать сезоны начала и конца периода в разных годах
                MessageBox.Show("Не возможно указывать сезоны из разных годов","Некорректный выбор сезона",MessageBoxButtons.OK, MessageBoxIcon.Stop);
                SeasonBegin = SeasonEnd = ssBegin[cmbSeasonBegin.SelectedIndex]; //Изменяется сезон начала и окончания периода 
                cmbSeasonEnd.SelectedIndex = cmbSeasonBegin.SelectedIndex;
                return;
            }
            if (ssBegin[cmbSeasonBegin.SelectedIndex].SeasonNbr > SeasonEnd.SeasonNbr)
            {// Не допускается устанавливать сезоны начала периода позже сезона конца периода
                MessageBox.Show("Не возможно указывать начальный сезон позже конечного", "Некорректный выбор сезона", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                SeasonBegin = SeasonEnd = ssBegin[cmbSeasonBegin.SelectedIndex];
                cmbSeasonEnd.SelectedIndex = cmbSeasonBegin.SelectedIndex;
                return;
            }
            //Устанавливается новое значение начального сезона
            SeasonBegin = ssBegin[cmbSeasonBegin.SelectedIndex];

        }
    }
}
