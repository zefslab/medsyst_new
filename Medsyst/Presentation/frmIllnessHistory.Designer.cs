﻿using Medsyst.DataSet;

namespace Medsyst
{
    partial class frmIllnessHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TabPage tabPage14;
            System.Windows.Forms.ComboBox cmbCracklingIH;
            System.Windows.Forms.ComboBox cmbBreathingIH;
            System.Windows.Forms.ComboBox cmbSoundIH;
            System.Windows.Forms.TabPage tabPage0;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIllnessHistory));
            System.Windows.Forms.ComboBox cmbStomachNormal2OSV;
            System.Windows.Forms.ComboBox cmbMendalinsOSV;
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Общая");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Жалобы больного");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("История настоящего заболевания");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Антропометрические данные");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Общий анамнез");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Данные объективного исследования");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Сердечно-сосудистая система");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Органы дыхания");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Органы пищеварения");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Мочеполовая система");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Органы опоры и движения");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Нервная система");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Назначения");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Течение болезни");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Заключительный прием");
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.chkOtherDinamic = new System.Windows.Forms.CheckBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.cmbResultIH = new System.Windows.Forms.ComboBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.txtOtherDinamic = new System.Windows.Forms.TextBox();
            this.cmbIntensificationIH = new System.Windows.Forms.ComboBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.cmbSensationIH = new System.Windows.Forms.ComboBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.groupButton = new System.Windows.Forms.GroupBox();
            this.btnAddMKBOutMain = new ExButton.NET.ExButton();
            this.exButton7 = new ExButton.NET.ExButton();
            this.exButton6 = new ExButton.NET.ExButton();
            this.exButton1 = new ExButton.NET.ExButton();
            this.exButton5 = new ExButton.NET.ExButton();
            this.exButton8 = new ExButton.NET.ExButton();
            this.exButton4 = new ExButton.NET.ExButton();
            this.btnDeleteMKBOut = new ExButton.NET.ExButton();
            this.exButton9 = new ExButton.NET.ExButton();
            this.exButton2 = new ExButton.NET.ExButton();
            this.exButton3 = new ExButton.NET.ExButton();
            this.dgMKBOut = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMKBName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDoctor = new System.Windows.Forms.Label();
            this.cmbDoctor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgMKBMain = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgMKBAttend = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblFIO = new System.Windows.Forms.Label();
            this.txtIHNbr = new Medsyst.Class.Core.RegularTextBox();
            this.nmYear = new System.Windows.Forms.NumericUpDown();
            this.label84 = new System.Windows.Forms.Label();
            this.textTypeNbr = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.txtSeasonClose = new System.Windows.Forms.TextBox();
            this.txtSeasonOpen = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbTypeIH = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.numAbortion = new System.Windows.Forms.NumericUpDown();
            this.numBirth = new System.Windows.Forms.NumericUpDown();
            this.numMenstrualDaysFrom = new System.Windows.Forms.NumericUpDown();
            this.numMenstrualDays = new System.Windows.Forms.NumericUpDown();
            this.numMenstrualYear = new System.Windows.Forms.NumericUpDown();
            this.numPregnancy = new System.Windows.Forms.NumericUpDown();
            this.chkGenecologyHarmfulness = new System.Windows.Forms.CheckBox();
            this.chkTrauma = new System.Windows.Forms.CheckBox();
            this.chkDisorder = new System.Windows.Forms.CheckBox();
            this.chkInherit = new System.Windows.Forms.CheckBox();
            this.cmbUnhealthyIH = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.cmbFamilyStatusIH = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.chkAbortion = new System.Windows.Forms.CheckBox();
            this.chkBirth = new System.Windows.Forms.CheckBox();
            this.cmbMenstrualFormIH = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.chkPregnancy = new System.Windows.Forms.CheckBox();
            this.chkMenstrual = new System.Windows.Forms.CheckBox();
            this.cmbHousingConditionIH = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbMaelsFormIH = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cmbMealsIH = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cmbWorkForIH = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtGenecologyHarmfulness = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTrauma = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInherit = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDisorder = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.cmbGlandIH = new System.Windows.Forms.ComboBox();
            this.txtMeals = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbMuscularIH = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbMucuosIH = new System.Windows.Forms.ComboBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cmbSkinIH = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cmbMeals2IH = new System.Windows.Forms.ComboBox();
            this.cmbFigureIH = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtFigure = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.cmbHeartNoise2IH = new System.Windows.Forms.ComboBox();
            this.cmbHeartNoise1IH = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.cmbHeartTone2IH = new System.Windows.Forms.ComboBox();
            this.cmbHeartTone1IH = new System.Windows.Forms.ComboBox();
            this.cmbPulse2IH = new System.Windows.Forms.ComboBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.cmbArteryPulseIH = new System.Windows.Forms.ComboBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cmbHeartNoiseIH = new System.Windows.Forms.ComboBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cmbHeartToneIH = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cmbPulse1IH = new System.Windows.Forms.ComboBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.cmbStool2 = new System.Windows.Forms.ComboBox();
            this.cmbStool1 = new System.Windows.Forms.ComboBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.cmbLiver4IH = new System.Windows.Forms.ComboBox();
            this.cmbLiver3IH = new System.Windows.Forms.ComboBox();
            this.cmbLiver2IH = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtStomachNormal = new System.Windows.Forms.TextBox();
            this.cmbStomachNormalIH = new System.Windows.Forms.ComboBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.cmbStomachNormal1IH = new System.Windows.Forms.ComboBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.cmbStomachNormal2IH = new System.Windows.Forms.ComboBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.cmbSpleenIH = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.cmbLiver1IH = new System.Windows.Forms.ComboBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.cmbMendalinsIH = new System.Windows.Forms.ComboBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.cmbToothsIH = new System.Windows.Forms.ComboBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.cmbTongueIH = new System.Windows.Forms.ComboBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.cmbUrinationIH = new System.Windows.Forms.ComboBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.chkSupportMotion = new System.Windows.Forms.CheckBox();
            this.txtSupportMotion = new System.Windows.Forms.TextBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.chkNoPeculiarity = new System.Windows.Forms.CheckBox();
            this.txtNervose = new System.Windows.Forms.TextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.lblServicePrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.TextBox();
            this.lblMedPrice = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCountMed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountDivide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRecept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAddMedicine = new ExButton.NET.ExButton();
            this.btnDelMedicine = new ExButton.NET.ExButton();
            this.btnAddService = new ExButton.NET.ExButton();
            this.btnDelService = new ExButton.NET.ExButton();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.gridService = new System.Windows.Forms.DataGridView();
            this.colFulfil = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCharacter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tabControlObservations = new System.Windows.Forms.TabControl();
            this.tabPageTherapist = new System.Windows.Forms.TabPage();
            this.label79 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.cmbStoolOSV = new System.Windows.Forms.ComboBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.cmbUrinationOSV = new System.Windows.Forms.ComboBox();
            this.cmbNoiseHeart3OSV = new System.Windows.Forms.ComboBox();
            this.cmbNoiseHeart2OSV = new System.Windows.Forms.ComboBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.cmbNoiseHeart1OSV = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtStomachNormalOSV = new System.Windows.Forms.TextBox();
            this.cmbStomachNormalOSV = new System.Windows.Forms.ComboBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.cmbStomachNormal1OSV = new System.Windows.Forms.ComboBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cmbCrepitationOSV = new System.Windows.Forms.ComboBox();
            this.chkComplaint = new System.Windows.Forms.CheckBox();
            this.cmbStateOSV = new System.Windows.Forms.ComboBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.cmbTongueOSV = new System.Windows.Forms.ComboBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.cmbToneHeartOSV = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.cmbBreathingOSV = new System.Windows.Forms.ComboBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtComplaint = new System.Windows.Forms.TextBox();
            this.lbObserv = new System.Windows.Forms.ListBox();
            this.label72 = new System.Windows.Forms.Label();
            this.tabPageDerma = new System.Windows.Forms.TabPage();
            this.lbObservDerma = new System.Windows.Forms.ListBox();
            this.txtDiagnosis = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.txtLymphNode = new System.Windows.Forms.TextBox();
            this.cmbLymphNode = new System.Windows.Forms.ComboBox();
            this.label96 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label98 = new System.Windows.Forms.Label();
            this.txtRecomendations = new System.Windows.Forms.TextBox();
            this.txtProcessLocalized = new System.Windows.Forms.TextBox();
            this.txtHair = new System.Windows.Forms.TextBox();
            this.txtNail = new System.Windows.Forms.TextBox();
            this.txtSkin = new System.Windows.Forms.TextBox();
            this.cmbProcessIs = new System.Windows.Forms.ComboBox();
            this.cmbHair = new System.Windows.Forms.ComboBox();
            this.cmbNail = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.cmbSkin = new System.Windows.Forms.ComboBox();
            this.txtAnamnes = new System.Windows.Forms.TextBox();
            this.txtComplaint2 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.label88 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAddObservation = new System.Windows.Forms.ToolStripButton();
            this.tsbDeleteObservation = new System.Windows.Forms.ToolStripButton();
            this.tsbMakeTemplate = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnOk = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.книжкаНазначенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.историяБолезниToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обратныйТалонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.амбулаторнаяКартаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directionTypeBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.directionTypeTableAdapter1 = new Medsyst.DataSet.MedsystDataSetTableAdapters.DirectionTypeTableAdapter();
            tabPage14 = new System.Windows.Forms.TabPage();
            cmbCracklingIH = new System.Windows.Forms.ComboBox();
            cmbBreathingIH = new System.Windows.Forms.ComboBox();
            cmbSoundIH = new System.Windows.Forms.ComboBox();
            tabPage0 = new System.Windows.Forms.TabPage();
            cmbStomachNormal2OSV = new System.Windows.Forms.ComboBox();
            cmbMendalinsOSV = new System.Windows.Forms.ComboBox();
            tabPage14.SuspendLayout();
            tabPage0.SuspendLayout();
            this.groupButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBOut)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBAttend)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmYear)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAbortion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualDaysFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPregnancy)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridService)).BeginInit();
            this.tabPage13.SuspendLayout();
            this.tabControlObservations.SuspendLayout();
            this.tabPageTherapist.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPageDerma.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.directionTypeBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage14
            // 
            tabPage14.Controls.Add(this.textBox1);
            tabPage14.Controls.Add(this.label59);
            tabPage14.Controls.Add(this.chkOtherDinamic);
            tabPage14.Controls.Add(this.textBox27);
            tabPage14.Controls.Add(this.label86);
            tabPage14.Controls.Add(this.cmbResultIH);
            tabPage14.Controls.Add(this.textBox69);
            tabPage14.Controls.Add(this.label81);
            tabPage14.Controls.Add(this.textBox68);
            tabPage14.Controls.Add(this.label80);
            tabPage14.Controls.Add(this.txtOtherDinamic);
            tabPage14.Controls.Add(this.cmbIntensificationIH);
            tabPage14.Controls.Add(this.textBox65);
            tabPage14.Controls.Add(this.label77);
            tabPage14.Controls.Add(this.cmbSensationIH);
            tabPage14.Controls.Add(this.textBox64);
            tabPage14.Controls.Add(this.label76);
            tabPage14.Location = new System.Drawing.Point(4, 22);
            tabPage14.Name = "tabPage14";
            tabPage14.Size = new System.Drawing.Size(845, 505);
            tabPage14.TabIndex = 16;
            tabPage14.Text = "Заключительный прием";
            tabPage14.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(9, 247);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(830, 64);
            this.textBox1.TabIndex = 69;
            this.textBox1.Tag = "history.Comment";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(4, 231);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(175, 13);
            this.label59.TabIndex = 68;
            this.label59.Tag = "history.";
            this.label59.Text = "Комментарий к истории болезни";
            // 
            // chkOtherDinamic
            // 
            this.chkOtherDinamic.AutoSize = true;
            this.chkOtherDinamic.Location = new System.Drawing.Point(3, 65);
            this.chkOtherDinamic.Name = "chkOtherDinamic";
            this.chkOtherDinamic.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkOtherDinamic.Size = new System.Drawing.Size(136, 17);
            this.chkOtherDinamic.TabIndex = 67;
            this.chkOtherDinamic.Tag = "history.boolOtherDinamic";
            this.chkOtherDinamic.Text = "Другой динамики нет";
            this.chkOtherDinamic.UseVisualStyleBackColor = true;
            this.chkOtherDinamic.CheckedChanged += new System.EventHandler(this.chkOtherDinamic_CheckedChanged);
            // 
            // textBox27
            // 
            this.textBox27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox27.Location = new System.Drawing.Point(9, 359);
            this.textBox27.Multiline = true;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(830, 140);
            this.textBox27.TabIndex = 66;
            this.textBox27.Tag = "history.txtDoctorRecomendation";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 343);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(306, 13);
            this.label86.TabIndex = 65;
            this.label86.Tag = "history.";
            this.label86.Text = "Лечебные и профилактические советы врача при выписке";
            // 
            // cmbResultIH
            // 
            this.cmbResultIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResultIH.FormattingEnabled = true;
            this.cmbResultIH.Location = new System.Drawing.Point(89, 319);
            this.cmbResultIH.Name = "cmbResultIH";
            this.cmbResultIH.Size = new System.Drawing.Size(159, 21);
            this.cmbResultIH.TabIndex = 64;
            this.cmbResultIH.Tag = "history.lstResult";
            // 
            // textBox69
            // 
            this.textBox69.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox69.Location = new System.Drawing.Point(254, 319);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(586, 20);
            this.textBox69.TabIndex = 63;
            this.textBox69.Tag = "history.txtResult";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 322);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(38, 13);
            this.label81.TabIndex = 62;
            this.label81.Text = "Исход";
            // 
            // textBox68
            // 
            this.textBox68.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox68.Location = new System.Drawing.Point(9, 159);
            this.textBox68.Multiline = true;
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(830, 64);
            this.textBox68.TabIndex = 61;
            this.textBox68.Tag = "history.txtObjectiveData";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(4, 143);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(118, 13);
            this.label80.TabIndex = 60;
            this.label80.Tag = "history.";
            this.label80.Text = "Объективные данные";
            // 
            // txtOtherDinamic
            // 
            this.txtOtherDinamic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOtherDinamic.Location = new System.Drawing.Point(7, 87);
            this.txtOtherDinamic.Multiline = true;
            this.txtOtherDinamic.Name = "txtOtherDinamic";
            this.txtOtherDinamic.Size = new System.Drawing.Size(830, 53);
            this.txtOtherDinamic.TabIndex = 59;
            this.txtOtherDinamic.Tag = "history.txtOtherDinamic";
            // 
            // cmbIntensificationIH
            // 
            this.cmbIntensificationIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIntensificationIH.FormattingEnabled = true;
            this.cmbIntensificationIH.Location = new System.Drawing.Point(89, 31);
            this.cmbIntensificationIH.Name = "cmbIntensificationIH";
            this.cmbIntensificationIH.Size = new System.Drawing.Size(159, 21);
            this.cmbIntensificationIH.TabIndex = 54;
            this.cmbIntensificationIH.Tag = "history.lstIntensification";
            // 
            // textBox65
            // 
            this.textBox65.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox65.Location = new System.Drawing.Point(254, 30);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(586, 20);
            this.textBox65.TabIndex = 53;
            this.textBox65.Tag = "history.txtIntensification";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(4, 34);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(68, 13);
            this.label77.TabIndex = 52;
            this.label77.Text = "Обострение";
            // 
            // cmbSensationIH
            // 
            this.cmbSensationIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensationIH.FormattingEnabled = true;
            this.cmbSensationIH.Location = new System.Drawing.Point(89, 4);
            this.cmbSensationIH.Name = "cmbSensationIH";
            this.cmbSensationIH.Size = new System.Drawing.Size(159, 21);
            this.cmbSensationIH.TabIndex = 51;
            this.cmbSensationIH.Tag = "history.lstSensation";
            // 
            // textBox64
            // 
            this.textBox64.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox64.Location = new System.Drawing.Point(254, 4);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(586, 20);
            this.textBox64.TabIndex = 50;
            this.textBox64.Tag = "history.txtSensation";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(4, 6);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(79, 13);
            this.label76.TabIndex = 49;
            this.label76.Text = "Самочувствие";
            // 
            // cmbCracklingIH
            // 
            cmbCracklingIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbCracklingIH.FormattingEnabled = true;
            cmbCracklingIH.Location = new System.Drawing.Point(209, 124);
            cmbCracklingIH.Name = "cmbCracklingIH";
            cmbCracklingIH.Size = new System.Drawing.Size(211, 21);
            cmbCracklingIH.TabIndex = 24;
            cmbCracklingIH.Tag = "history.lstCracklingIH";
            // 
            // cmbBreathingIH
            // 
            cmbBreathingIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbBreathingIH.FormattingEnabled = true;
            cmbBreathingIH.Location = new System.Drawing.Point(207, 67);
            cmbBreathingIH.Name = "cmbBreathingIH";
            cmbBreathingIH.Size = new System.Drawing.Size(211, 21);
            cmbBreathingIH.TabIndex = 21;
            cmbBreathingIH.Tag = "history.lstBreathingIH";
            // 
            // cmbSoundIH
            // 
            cmbSoundIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbSoundIH.FormattingEnabled = true;
            cmbSoundIH.Location = new System.Drawing.Point(207, 11);
            cmbSoundIH.Name = "cmbSoundIH";
            cmbSoundIH.Size = new System.Drawing.Size(211, 21);
            cmbSoundIH.TabIndex = 18;
            cmbSoundIH.Tag = "history.lstSoundIH";
            // 
            // tabPage0
            // 
            tabPage0.AllowDrop = true;
            tabPage0.Controls.Add(this.groupButton);
            tabPage0.Controls.Add(this.dgMKBOut);
            tabPage0.Controls.Add(this.lblDoctor);
            tabPage0.Controls.Add(this.cmbDoctor);
            tabPage0.Controls.Add(this.label6);
            tabPage0.Controls.Add(this.groupBox1);
            tabPage0.Location = new System.Drawing.Point(4, 22);
            tabPage0.Name = "tabPage0";
            tabPage0.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            tabPage0.Size = new System.Drawing.Size(790, 482);
            tabPage0.TabIndex = 0;
            tabPage0.Text = "Общая";
            tabPage0.UseVisualStyleBackColor = true;
            // 
            // groupButton
            // 
            this.groupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupButton.Controls.Add(this.btnAddMKBOutMain);
            this.groupButton.Controls.Add(this.exButton7);
            this.groupButton.Controls.Add(this.exButton6);
            this.groupButton.Controls.Add(this.exButton1);
            this.groupButton.Controls.Add(this.exButton5);
            this.groupButton.Controls.Add(this.exButton8);
            this.groupButton.Controls.Add(this.exButton4);
            this.groupButton.Controls.Add(this.btnDeleteMKBOut);
            this.groupButton.Controls.Add(this.exButton9);
            this.groupButton.Controls.Add(this.exButton2);
            this.groupButton.Controls.Add(this.exButton3);
            this.groupButton.Location = new System.Drawing.Point(673, 6);
            this.groupButton.Name = "groupButton";
            this.groupButton.Size = new System.Drawing.Size(114, 407);
            this.groupButton.TabIndex = 72;
            this.groupButton.TabStop = false;
            // 
            // btnAddMKBOutMain
            // 
            this.btnAddMKBOutMain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMKBOutMain.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMKBOutMain.Image")));
            this.btnAddMKBOutMain.Location = new System.Drawing.Point(5, 13);
            this.btnAddMKBOutMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddMKBOutMain.Name = "btnAddMKBOutMain";
            this.btnAddMKBOutMain.Size = new System.Drawing.Size(103, 23);
            this.btnAddMKBOutMain.TabIndex = 1;
            this.btnAddMKBOutMain.Text = "Основной";
            this.btnAddMKBOutMain.Click += new System.EventHandler(this.btnAddMKBOutMain_Click);
            // 
            // exButton7
            // 
            this.exButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton7.Image = ((System.Drawing.Image)(resources.GetObject("exButton7.Image")));
            this.exButton7.Location = new System.Drawing.Point(5, 319);
            this.exButton7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton7.Name = "exButton7";
            this.exButton7.Size = new System.Drawing.Size(103, 23);
            this.exButton7.TabIndex = 8;
            this.exButton7.Text = "Сопутсвующий";
            this.exButton7.Click += new System.EventHandler(this.exButton7_Click);
            // 
            // exButton6
            // 
            this.exButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton6.Image = ((System.Drawing.Image)(resources.GetObject("exButton6.Image")));
            this.exButton6.Location = new System.Drawing.Point(5, 219);
            this.exButton6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton6.Name = "exButton6";
            this.exButton6.Size = new System.Drawing.Size(103, 23);
            this.exButton6.TabIndex = 65;
            this.exButton6.Text = "Удалить";
            this.exButton6.Click += new System.EventHandler(this.exButton6_Click);
            // 
            // exButton1
            // 
            this.exButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton1.Image = ((System.Drawing.Image)(resources.GetObject("exButton1.Image")));
            this.exButton1.Location = new System.Drawing.Point(5, 42);
            this.exButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton1.Name = "exButton1";
            this.exButton1.Size = new System.Drawing.Size(103, 23);
            this.exButton1.TabIndex = 2;
            this.exButton1.Text = "Сопутсвующий";
            this.exButton1.Click += new System.EventHandler(this.btnAddMKBOutNotMain_Click);
            // 
            // exButton5
            // 
            this.exButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton5.Image = ((System.Drawing.Image)(resources.GetObject("exButton5.Image")));
            this.exButton5.Location = new System.Drawing.Point(5, 161);
            this.exButton5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton5.Name = "exButton5";
            this.exButton5.Size = new System.Drawing.Size(103, 23);
            this.exButton5.TabIndex = 5;
            this.exButton5.Text = "Основной";
            this.exButton5.Click += new System.EventHandler(this.exButton5_Click);
            // 
            // exButton8
            // 
            this.exButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton8.Image = ((System.Drawing.Image)(resources.GetObject("exButton8.Image")));
            this.exButton8.Location = new System.Drawing.Point(5, 290);
            this.exButton8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton8.Name = "exButton8";
            this.exButton8.Size = new System.Drawing.Size(103, 23);
            this.exButton8.TabIndex = 7;
            this.exButton8.Text = "Основной";
            this.exButton8.Click += new System.EventHandler(this.exButton8_Click);
            // 
            // exButton4
            // 
            this.exButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton4.Image = ((System.Drawing.Image)(resources.GetObject("exButton4.Image")));
            this.exButton4.Location = new System.Drawing.Point(5, 248);
            this.exButton4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton4.Name = "exButton4";
            this.exButton4.Size = new System.Drawing.Size(103, 23);
            this.exButton4.TabIndex = 4;
            this.exButton4.Text = "Перенести";
            this.exButton4.Click += new System.EventHandler(this.exButton4_Click);
            // 
            // btnDeleteMKBOut
            // 
            this.btnDeleteMKBOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteMKBOut.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteMKBOut.Image")));
            this.btnDeleteMKBOut.Location = new System.Drawing.Point(5, 71);
            this.btnDeleteMKBOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteMKBOut.Name = "btnDeleteMKBOut";
            this.btnDeleteMKBOut.Size = new System.Drawing.Size(103, 23);
            this.btnDeleteMKBOut.TabIndex = 60;
            this.btnDeleteMKBOut.Text = "Удалить";
            this.btnDeleteMKBOut.Click += new System.EventHandler(this.btnDeleteMKBOut_Click);
            // 
            // exButton9
            // 
            this.exButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton9.Image = ((System.Drawing.Image)(resources.GetObject("exButton9.Image")));
            this.exButton9.Location = new System.Drawing.Point(5, 348);
            this.exButton9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton9.Name = "exButton9";
            this.exButton9.Size = new System.Drawing.Size(103, 23);
            this.exButton9.TabIndex = 69;
            this.exButton9.Text = "Удалить";
            this.exButton9.Click += new System.EventHandler(this.exButton9_Click);
            // 
            // exButton2
            // 
            this.exButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton2.Image = ((System.Drawing.Image)(resources.GetObject("exButton2.Image")));
            this.exButton2.Location = new System.Drawing.Point(5, 190);
            this.exButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton2.Name = "exButton2";
            this.exButton2.Size = new System.Drawing.Size(103, 23);
            this.exButton2.TabIndex = 6;
            this.exButton2.Text = "Сопутсвующий";
            this.exButton2.Click += new System.EventHandler(this.exButton2_Click);
            // 
            // exButton3
            // 
            this.exButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton3.Image = ((System.Drawing.Image)(resources.GetObject("exButton3.Image")));
            this.exButton3.Location = new System.Drawing.Point(5, 100);
            this.exButton3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.exButton3.Name = "exButton3";
            this.exButton3.Size = new System.Drawing.Size(103, 23);
            this.exButton3.TabIndex = 3;
            this.exButton3.Text = "Перенести";
            this.exButton3.Click += new System.EventHandler(this.exButton3_Click);
            // 
            // dgMKBOut
            // 
            this.dgMKBOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMKBOut.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgMKBOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMKBOut.ColumnHeadersVisible = false;
            this.dgMKBOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.colMKBName,
            this.colMain});
            this.dgMKBOut.Location = new System.Drawing.Point(18, 20);
            this.dgMKBOut.Name = "dgMKBOut";
            this.dgMKBOut.RowHeadersVisible = false;
            this.dgMKBOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMKBOut.Size = new System.Drawing.Size(649, 109);
            this.dgMKBOut.TabIndex = 63;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "MKBCodeID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ID.Visible = false;
            this.ID.Width = 30;
            // 
            // colMKBName
            // 
            this.colMKBName.DataPropertyName = "Name";
            this.colMKBName.HeaderText = "Наименование диагноза";
            this.colMKBName.Name = "colMKBName";
            this.colMKBName.ReadOnly = true;
            this.colMKBName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colMKBName.Width = 520;
            // 
            // colMain
            // 
            this.colMain.DataPropertyName = "MainText";
            this.colMain.HeaderText = "Основной";
            this.colMain.Name = "colMain";
            this.colMain.ReadOnly = true;
            this.colMain.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colMain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colMain.Width = 95;
            // 
            // lblDoctor
            // 
            this.lblDoctor.AutoSize = true;
            this.lblDoctor.Location = new System.Drawing.Point(15, 422);
            this.lblDoctor.Name = "lblDoctor";
            this.lblDoctor.Size = new System.Drawing.Size(55, 13);
            this.lblDoctor.TabIndex = 10;
            this.lblDoctor.Text = "Терапевт";
            // 
            // cmbDoctor
            // 
            this.cmbDoctor.DisplayMember = "Fullname";
            this.cmbDoctor.FormattingEnabled = true;
            this.cmbDoctor.Location = new System.Drawing.Point(77, 419);
            this.cmbDoctor.Name = "cmbDoctor";
            this.cmbDoctor.Size = new System.Drawing.Size(227, 21);
            this.cmbDoctor.TabIndex = 7;
            this.cmbDoctor.Tag = "history.DoctorID";
            this.cmbDoctor.ValueMember = "DoctorID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Диагноз с места отбора:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgMKBMain);
            this.groupBox1.Controls.Add(this.dgMKBAttend);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(9, 135);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(657, 278);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Диагнозы санатория-профилактория";
            // 
            // dgMKBMain
            // 
            this.dgMKBMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMKBMain.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgMKBMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMKBMain.ColumnHeadersVisible = false;
            this.dgMKBMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn9});
            this.dgMKBMain.Location = new System.Drawing.Point(9, 161);
            this.dgMKBMain.Name = "dgMKBMain";
            this.dgMKBMain.RowHeadersVisible = false;
            this.dgMKBMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMKBMain.Size = new System.Drawing.Size(643, 111);
            this.dgMKBMain.TabIndex = 68;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MKBCodeID";
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 30;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn7.HeaderText = "Наименование диагноза";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn7.Width = 520;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MainText";
            this.dataGridViewTextBoxColumn9.HeaderText = "Основной";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 95;
            // 
            // dgMKBAttend
            // 
            this.dgMKBAttend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMKBAttend.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgMKBAttend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMKBAttend.ColumnHeadersVisible = false;
            this.dgMKBAttend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgMKBAttend.Location = new System.Drawing.Point(9, 32);
            this.dgMKBAttend.Name = "dgMKBAttend";
            this.dgMKBAttend.RowHeadersVisible = false;
            this.dgMKBAttend.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMKBAttend.Size = new System.Drawing.Size(643, 110);
            this.dgMKBAttend.TabIndex = 65;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "MKBCodeID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn3.HeaderText = "Наименование диагноза";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.Width = 520;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MainText";
            this.dataGridViewTextBoxColumn4.HeaderText = "Основной";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 95;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "а) при поступлении:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "б) заключительный:";
            // 
            // cmbStomachNormal2OSV
            // 
            cmbStomachNormal2OSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbStomachNormal2OSV.FormattingEnabled = true;
            cmbStomachNormal2OSV.Location = new System.Drawing.Point(246, 37);
            cmbStomachNormal2OSV.Name = "cmbStomachNormal2OSV";
            cmbStomachNormal2OSV.Size = new System.Drawing.Size(237, 21);
            cmbStomachNormal2OSV.TabIndex = 52;
            cmbStomachNormal2OSV.Tag = "observat.lstStomachNormal2";
            // 
            // cmbMendalinsOSV
            // 
            cmbMendalinsOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbMendalinsOSV.FormattingEnabled = true;
            cmbMendalinsOSV.Location = new System.Drawing.Point(256, 382);
            cmbMendalinsOSV.Name = "cmbMendalinsOSV";
            cmbMendalinsOSV.Size = new System.Drawing.Size(181, 21);
            cmbMendalinsOSV.TabIndex = 90;
            cmbMendalinsOSV.Tag = "observat.lstMendalins";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1020, 574);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1020, 602);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblFIO);
            this.splitContainer1.Panel1.Controls.Add(this.txtIHNbr);
            this.splitContainer1.Panel1.Controls.Add(this.nmYear);
            this.splitContainer1.Panel1.Controls.Add(this.label84);
            this.splitContainer1.Panel1.Controls.Add(this.textTypeNbr);
            this.splitContainer1.Panel1.Controls.Add(this.label83);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeason);
            this.splitContainer1.Panel1.Controls.Add(this.txtSeasonClose);
            this.splitContainer1.Panel1.Controls.Add(this.txtSeasonOpen);
            this.splitContainer1.Panel1.Controls.Add(this.label21);
            this.splitContainer1.Panel1.Controls.Add(this.cmbTypeIH);
            this.splitContainer1.Panel1.Controls.Add(this.label20);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 574);
            this.splitContainer1.SplitterDistance = 62;
            this.splitContainer1.TabIndex = 2;
            // 
            // lblFIO
            // 
            this.lblFIO.AutoSize = true;
            this.lblFIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFIO.Location = new System.Drawing.Point(43, 35);
            this.lblFIO.Name = "lblFIO";
            this.lblFIO.Size = new System.Drawing.Size(214, 20);
            this.lblFIO.TabIndex = 30;
            this.lblFIO.Tag = "patient.Fullname";
            this.lblFIO.Text = "Фамилия Имя Отчество";
            // 
            // txtIHNbr
            // 
            this.txtIHNbr.Filter = Medsyst.Class.Core.FilterType.Integer;
            this.txtIHNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIHNbr.Location = new System.Drawing.Point(168, 5);
            this.txtIHNbr.Name = "txtIHNbr";
            this.txtIHNbr.Size = new System.Drawing.Size(56, 23);
            this.txtIHNbr.TabIndex = 2;
            this.txtIHNbr.Tag = "history.NbrIH";
            this.txtIHNbr.Value = 0D;
            // 
            // nmYear
            // 
            this.nmYear.Location = new System.Drawing.Point(945, 8);
            this.nmYear.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.nmYear.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nmYear.Name = "nmYear";
            this.nmYear.Size = new System.Drawing.Size(64, 20);
            this.nmYear.TabIndex = 28;
            this.nmYear.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.nmYear.ValueChanged += new System.EventHandler(this.nmYear_ValueChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(915, 11);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(24, 13);
            this.label84.TabIndex = 27;
            this.label84.Text = "год";
            // 
            // textTypeNbr
            // 
            this.textTypeNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textTypeNbr.Location = new System.Drawing.Point(368, 5);
            this.textTypeNbr.Name = "textTypeNbr";
            this.textTypeNbr.Size = new System.Drawing.Size(164, 23);
            this.textTypeNbr.TabIndex = 1;
            this.textTypeNbr.Tag = "history.TypeNbr";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(348, 8);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(23, 17);
            this.label83.TabIndex = 25;
            this.label83.Text = "№";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "SeasonNbr";
            this.cmbSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeason.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSeason.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(600, 4);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(52, 24);
            this.cmbSeason.TabIndex = 22;
            this.cmbSeason.Tag = "history.SeasonID";
            this.cmbSeason.ValueMember = "SeasonID";
            // 
            // txtSeasonClose
            // 
            this.txtSeasonClose.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSeasonClose.Location = new System.Drawing.Point(809, 11);
            this.txtSeasonClose.Name = "txtSeasonClose";
            this.txtSeasonClose.ReadOnly = true;
            this.txtSeasonClose.Size = new System.Drawing.Size(100, 13);
            this.txtSeasonClose.TabIndex = 24;
            // 
            // txtSeasonOpen
            // 
            this.txtSeasonOpen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSeasonOpen.Location = new System.Drawing.Point(678, 11);
            this.txtSeasonOpen.Name = "txtSeasonOpen";
            this.txtSeasonOpen.ReadOnly = true;
            this.txtSeasonOpen.Size = new System.Drawing.Size(100, 13);
            this.txtSeasonOpen.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(535, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 17);
            this.label21.TabIndex = 21;
            this.label21.Text = "сезон №";
            // 
            // cmbTypeIH
            // 
            this.cmbTypeIH.DisplayMember = "TypeN";
            this.cmbTypeIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeIH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTypeIH.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbTypeIH.FormattingEnabled = true;
            this.cmbTypeIH.Location = new System.Drawing.Point(230, 5);
            this.cmbTypeIH.Name = "cmbTypeIH";
            this.cmbTypeIH.Size = new System.Drawing.Size(112, 24);
            this.cmbTypeIH.TabIndex = 20;
            this.cmbTypeIH.Tag = "history.TypeIH";
            this.cmbTypeIH.ValueMember = "TypeID";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(784, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "ФИО";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(658, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "с";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(4, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "История болезни №";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer2.Size = new System.Drawing.Size(1020, 508);
            this.splitContainer2.SplitterDistance = 218;
            this.splitContainer2.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "0";
            treeNode1.Text = "Общая";
            treeNode2.Name = "1";
            treeNode2.Text = "Жалобы больного";
            treeNode3.Name = "2";
            treeNode3.Text = "История настоящего заболевания";
            treeNode4.Name = "3";
            treeNode4.Text = "Антропометрические данные";
            treeNode5.Name = "4";
            treeNode5.Text = "Общий анамнез";
            treeNode6.Name = "5";
            treeNode6.Text = "Данные объективного исследования";
            treeNode7.Name = "6";
            treeNode7.Text = "Сердечно-сосудистая система";
            treeNode8.Name = "7";
            treeNode8.Text = "Органы дыхания";
            treeNode9.Name = "8";
            treeNode9.Text = "Органы пищеварения";
            treeNode10.Name = "9";
            treeNode10.Text = "Мочеполовая система";
            treeNode11.Name = "10";
            treeNode11.Text = "Органы опоры и движения";
            treeNode12.Name = "11";
            treeNode12.Text = "Нервная система";
            treeNode13.Name = "12";
            treeNode13.Text = "Назначения";
            treeNode14.Name = "13";
            treeNode14.Text = "Течение болезни";
            treeNode15.Name = "14";
            treeNode15.Text = "Заключительный прием";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15});
            this.treeView1.Size = new System.Drawing.Size(218, 508);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(tabPage0);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(tabPage14);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(798, 508);
            this.tabControl1.TabIndex = 7;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(845, 505);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Жалобы больного";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox6.Location = new System.Drawing.Point(7, 6);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(830, 436);
            this.textBox6.TabIndex = 1;
            this.textBox6.Tag = "history.Complaints";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.textBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(845, 505);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "История заболевания";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox7.Location = new System.Drawing.Point(7, 7);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(830, 433);
            this.textBox7.TabIndex = 2;
            this.textBox7.Tag = "history.HistiryOffIllness";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBox16);
            this.tabPage3.Controls.Add(this.textBox4);
            this.tabPage3.Controls.Add(this.textBox9);
            this.tabPage3.Controls.Add(this.textBox14);
            this.tabPage3.Controls.Add(this.textBox15);
            this.tabPage3.Controls.Add(this.textBox17);
            this.tabPage3.Controls.Add(this.textBox18);
            this.tabPage3.Controls.Add(this.textBox13);
            this.tabPage3.Controls.Add(this.textBox12);
            this.tabPage3.Controls.Add(this.textBox11);
            this.tabPage3.Controls.Add(this.textBox10);
            this.tabPage3.Controls.Add(this.textBox8);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(845, 505);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Aнтропометрические данные";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(319, 66);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(87, 20);
            this.textBox16.TabIndex = 20;
            this.textBox16.Tag = "history.txtSpirometriaOut";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(319, 40);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(87, 20);
            this.textBox4.TabIndex = 19;
            this.textBox4.Tag = "history.txtSpirometriaIn";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(404, 66);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(60, 20);
            this.textBox9.TabIndex = 18;
            this.textBox9.Tag = "history.txtPulseOut";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(230, 66);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(91, 20);
            this.textBox14.TabIndex = 17;
            this.textBox14.Tag = "history.txtDynamomentyOut";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(463, 66);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(133, 20);
            this.textBox15.TabIndex = 16;
            this.textBox15.Tag = "history.txtArterialOut";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(167, 66);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(64, 20);
            this.textBox17.TabIndex = 15;
            this.textBox17.Tag = "history.txtWeightOut";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(108, 66);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(60, 20);
            this.textBox18.TabIndex = 14;
            this.textBox18.Tag = "history.txtHeightOut";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(404, 40);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(60, 20);
            this.textBox13.TabIndex = 13;
            this.textBox13.Tag = "history.txtPulseIn";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(230, 40);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(91, 20);
            this.textBox12.TabIndex = 12;
            this.textBox12.Tag = "history.txtDynamomentyIn";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(463, 40);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(133, 20);
            this.textBox11.TabIndex = 11;
            this.textBox11.Tag = "history.txtArterialIn";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(167, 40);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(64, 20);
            this.textBox10.TabIndex = 10;
            this.textBox10.Tag = "history.txtWeightIn";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(108, 40);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(60, 20);
            this.textBox8.TabIndex = 8;
            this.textBox8.Tag = "history.txtHeightIn";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "При выписке";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "При поступлении";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(320, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Пикфлоуметрия";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(459, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Артериальное давление";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(415, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Пульс";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(222, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Пульсоксиметрия";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(174, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Вес, кг";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(113, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Рост, см.";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.numAbortion);
            this.tabPage4.Controls.Add(this.numBirth);
            this.tabPage4.Controls.Add(this.numMenstrualDaysFrom);
            this.tabPage4.Controls.Add(this.numMenstrualDays);
            this.tabPage4.Controls.Add(this.numMenstrualYear);
            this.tabPage4.Controls.Add(this.numPregnancy);
            this.tabPage4.Controls.Add(this.chkGenecologyHarmfulness);
            this.tabPage4.Controls.Add(this.chkTrauma);
            this.tabPage4.Controls.Add(this.chkDisorder);
            this.tabPage4.Controls.Add(this.chkInherit);
            this.tabPage4.Controls.Add(this.cmbUnhealthyIH);
            this.tabPage4.Controls.Add(this.label75);
            this.tabPage4.Controls.Add(this.cmbFamilyStatusIH);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.chkAbortion);
            this.tabPage4.Controls.Add(this.chkBirth);
            this.tabPage4.Controls.Add(this.cmbMenstrualFormIH);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.chkPregnancy);
            this.tabPage4.Controls.Add(this.chkMenstrual);
            this.tabPage4.Controls.Add(this.cmbHousingConditionIH);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.cmbMaelsFormIH);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.cmbMealsIH);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.cmbWorkForIH);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.txtGenecologyHarmfulness);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.textBox22);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtTrauma);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.txtInherit);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.txtDisorder);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage4.Size = new System.Drawing.Size(845, 505);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "Общий анамнез";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // numAbortion
            // 
            this.numAbortion.Location = new System.Drawing.Point(353, 308);
            this.numAbortion.Name = "numAbortion";
            this.numAbortion.Size = new System.Drawing.Size(40, 20);
            this.numAbortion.TabIndex = 54;
            this.numAbortion.Tag = "history.intAbortion";
            // 
            // numBirth
            // 
            this.numBirth.Location = new System.Drawing.Point(241, 308);
            this.numBirth.Name = "numBirth";
            this.numBirth.Size = new System.Drawing.Size(40, 20);
            this.numBirth.TabIndex = 53;
            this.numBirth.Tag = "history.intBirth";
            // 
            // numMenstrualDaysFrom
            // 
            this.numMenstrualDaysFrom.Location = new System.Drawing.Point(353, 284);
            this.numMenstrualDaysFrom.Name = "numMenstrualDaysFrom";
            this.numMenstrualDaysFrom.Size = new System.Drawing.Size(40, 20);
            this.numMenstrualDaysFrom.TabIndex = 52;
            this.numMenstrualDaysFrom.Tag = "history.intMenstrualDaysFrom";
            // 
            // numMenstrualDays
            // 
            this.numMenstrualDays.Location = new System.Drawing.Point(241, 284);
            this.numMenstrualDays.Name = "numMenstrualDays";
            this.numMenstrualDays.Size = new System.Drawing.Size(40, 20);
            this.numMenstrualDays.TabIndex = 51;
            this.numMenstrualDays.Tag = "history.intMenstrualDays";
            // 
            // numMenstrualYear
            // 
            this.numMenstrualYear.Location = new System.Drawing.Point(149, 284);
            this.numMenstrualYear.Name = "numMenstrualYear";
            this.numMenstrualYear.Size = new System.Drawing.Size(40, 20);
            this.numMenstrualYear.TabIndex = 50;
            this.numMenstrualYear.Tag = "history.intMenstrualYear";
            // 
            // numPregnancy
            // 
            this.numPregnancy.Location = new System.Drawing.Point(108, 308);
            this.numPregnancy.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPregnancy.Name = "numPregnancy";
            this.numPregnancy.Size = new System.Drawing.Size(40, 20);
            this.numPregnancy.TabIndex = 49;
            this.numPregnancy.Tag = "history.intPregnancy";
            // 
            // chkGenecologyHarmfulness
            // 
            this.chkGenecologyHarmfulness.AutoSize = true;
            this.chkGenecologyHarmfulness.Location = new System.Drawing.Point(181, 332);
            this.chkGenecologyHarmfulness.Name = "chkGenecologyHarmfulness";
            this.chkGenecologyHarmfulness.Size = new System.Drawing.Size(45, 17);
            this.chkGenecologyHarmfulness.TabIndex = 48;
            this.chkGenecologyHarmfulness.Tag = "history.boolGenecologyHarmfulness";
            this.chkGenecologyHarmfulness.Text = "Нет";
            this.chkGenecologyHarmfulness.UseVisualStyleBackColor = true;
            this.chkGenecologyHarmfulness.CheckedChanged += new System.EventHandler(this.chkGenecologyHarmfulness_CheckedChanged);
            // 
            // chkTrauma
            // 
            this.chkTrauma.AutoSize = true;
            this.chkTrauma.Location = new System.Drawing.Point(266, 102);
            this.chkTrauma.Name = "chkTrauma";
            this.chkTrauma.Size = new System.Drawing.Size(45, 17);
            this.chkTrauma.TabIndex = 47;
            this.chkTrauma.Tag = "history.boolTrauma";
            this.chkTrauma.Text = "Нет";
            this.chkTrauma.UseVisualStyleBackColor = true;
            this.chkTrauma.CheckedChanged += new System.EventHandler(this.chkTrauma_CheckedChanged);
            // 
            // chkDisorder
            // 
            this.chkDisorder.AutoSize = true;
            this.chkDisorder.Location = new System.Drawing.Point(382, 51);
            this.chkDisorder.Name = "chkDisorder";
            this.chkDisorder.Size = new System.Drawing.Size(45, 17);
            this.chkDisorder.TabIndex = 46;
            this.chkDisorder.Tag = "history.boolDisorder";
            this.chkDisorder.Text = "Нет";
            this.chkDisorder.UseVisualStyleBackColor = true;
            this.chkDisorder.CheckedChanged += new System.EventHandler(this.chkDisorder_CheckedChanged);
            // 
            // chkInherit
            // 
            this.chkInherit.AutoSize = true;
            this.chkInherit.Location = new System.Drawing.Point(118, 2);
            this.chkInherit.Name = "chkInherit";
            this.chkInherit.Size = new System.Drawing.Size(96, 17);
            this.chkInherit.TabIndex = 45;
            this.chkInherit.Tag = "history.boolInherit";
            this.chkInherit.Text = "не отягощена";
            this.chkInherit.UseVisualStyleBackColor = true;
            this.chkInherit.CheckedChanged += new System.EventHandler(this.chkInherit_CheckedChanged);
            // 
            // cmbUnhealthyIH
            // 
            this.cmbUnhealthyIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnhealthyIH.FormattingEnabled = true;
            this.cmbUnhealthyIH.Location = new System.Drawing.Point(122, 260);
            this.cmbUnhealthyIH.Name = "cmbUnhealthyIH";
            this.cmbUnhealthyIH.Size = new System.Drawing.Size(112, 21);
            this.cmbUnhealthyIH.TabIndex = 41;
            this.cmbUnhealthyIH.Tag = "history.lstUnhialthy";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(5, 263);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(104, 13);
            this.label75.TabIndex = 40;
            this.label75.Text = "Вредные привычки";
            // 
            // cmbFamilyStatusIH
            // 
            this.cmbFamilyStatusIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFamilyStatusIH.FormattingEnabled = true;
            this.cmbFamilyStatusIH.Location = new System.Drawing.Point(409, 234);
            this.cmbFamilyStatusIH.Name = "cmbFamilyStatusIH";
            this.cmbFamilyStatusIH.Size = new System.Drawing.Size(112, 21);
            this.cmbFamilyStatusIH.TabIndex = 39;
            this.cmbFamilyStatusIH.Tag = "history.lstFamilyStatus";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(287, 237);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(117, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Семейное положение";
            // 
            // chkAbortion
            // 
            this.chkAbortion.AutoSize = true;
            this.chkAbortion.Location = new System.Drawing.Point(293, 310);
            this.chkAbortion.Name = "chkAbortion";
            this.chkAbortion.Size = new System.Drawing.Size(64, 17);
            this.chkAbortion.TabIndex = 37;
            this.chkAbortion.Tag = "history.boolAbortion";
            this.chkAbortion.Text = "Аборты";
            this.chkAbortion.UseVisualStyleBackColor = true;
            this.chkAbortion.CheckedChanged += new System.EventHandler(this.chkAbortion_CheckedChanged);
            // 
            // chkBirth
            // 
            this.chkBirth.AutoSize = true;
            this.chkBirth.Location = new System.Drawing.Point(181, 310);
            this.chkBirth.Name = "chkBirth";
            this.chkBirth.Size = new System.Drawing.Size(53, 17);
            this.chkBirth.TabIndex = 36;
            this.chkBirth.Tag = "history.boolBirth";
            this.chkBirth.Text = "Роды";
            this.chkBirth.UseVisualStyleBackColor = true;
            this.chkBirth.CheckedChanged += new System.EventHandler(this.chkBirth_CheckedChanged);
            // 
            // cmbMenstrualFormIH
            // 
            this.cmbMenstrualFormIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMenstrualFormIH.FormattingEnabled = true;
            this.cmbMenstrualFormIH.Location = new System.Drawing.Point(421, 284);
            this.cmbMenstrualFormIH.Name = "cmbMenstrualFormIH";
            this.cmbMenstrualFormIH.Size = new System.Drawing.Size(100, 21);
            this.cmbMenstrualFormIH.TabIndex = 35;
            this.cmbMenstrualFormIH.Tag = "history.lstMenstrualForm";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(402, 288);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 34;
            this.label29.Text = "дн.";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(290, 288);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(57, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "дн., через";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(193, 288);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "лет, по";
            // 
            // chkPregnancy
            // 
            this.chkPregnancy.AutoSize = true;
            this.chkPregnancy.Location = new System.Drawing.Point(5, 310);
            this.chkPregnancy.Name = "chkPregnancy";
            this.chkPregnancy.Size = new System.Drawing.Size(100, 17);
            this.chkPregnancy.TabIndex = 28;
            this.chkPregnancy.Tag = "history.boolPregnancy";
            this.chkPregnancy.Text = "Беременности";
            this.chkPregnancy.UseVisualStyleBackColor = true;
            this.chkPregnancy.CheckedChanged += new System.EventHandler(this.chkPregnancy_CheckedChanged);
            // 
            // chkMenstrual
            // 
            this.chkMenstrual.AutoSize = true;
            this.chkMenstrual.Location = new System.Drawing.Point(6, 286);
            this.chkMenstrual.Name = "chkMenstrual";
            this.chkMenstrual.Size = new System.Drawing.Size(142, 17);
            this.chkMenstrual.TabIndex = 27;
            this.chkMenstrual.Tag = "history.boolMenstrual";
            this.chkMenstrual.Text = "Менструальные циклы";
            this.chkMenstrual.UseVisualStyleBackColor = true;
            this.chkMenstrual.CheckedChanged += new System.EventHandler(this.chkMenstrual_CheckedChanged);
            // 
            // cmbHousingConditionIH
            // 
            this.cmbHousingConditionIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHousingConditionIH.FormattingEnabled = true;
            this.cmbHousingConditionIH.Location = new System.Drawing.Point(120, 234);
            this.cmbHousingConditionIH.Name = "cmbHousingConditionIH";
            this.cmbHousingConditionIH.Size = new System.Drawing.Size(164, 21);
            this.cmbHousingConditionIH.TabIndex = 25;
            this.cmbHousingConditionIH.Tag = "history.lstHousingCondition";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 237);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 13);
            this.label26.TabIndex = 24;
            this.label26.Text = "Условия проживания";
            // 
            // cmbMaelsFormIH
            // 
            this.cmbMaelsFormIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaelsFormIH.FormattingEnabled = true;
            this.cmbMaelsFormIH.Location = new System.Drawing.Point(410, 207);
            this.cmbMaelsFormIH.Name = "cmbMaelsFormIH";
            this.cmbMaelsFormIH.Size = new System.Drawing.Size(111, 21);
            this.cmbMaelsFormIH.TabIndex = 22;
            this.cmbMaelsFormIH.Tag = "history.lstMaelsForm";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(330, 210);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "Фон питания";
            // 
            // cmbMealsIH
            // 
            this.cmbMealsIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMealsIH.FormattingEnabled = true;
            this.cmbMealsIH.Location = new System.Drawing.Point(217, 207);
            this.cmbMealsIH.Name = "cmbMealsIH";
            this.cmbMealsIH.Size = new System.Drawing.Size(107, 21);
            this.cmbMealsIH.TabIndex = 20;
            this.cmbMealsIH.Tag = "history.lstMeals";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(166, 210);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "Питание";
            // 
            // cmbWorkForIH
            // 
            this.cmbWorkForIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWorkForIH.FormattingEnabled = true;
            this.cmbWorkForIH.Location = new System.Drawing.Point(48, 207);
            this.cmbWorkForIH.Name = "cmbWorkForIH";
            this.cmbWorkForIH.Size = new System.Drawing.Size(112, 21);
            this.cmbWorkForIH.TabIndex = 18;
            this.cmbWorkForIH.Tag = "history.lstWorkForm";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 210);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 17;
            this.label22.Text = "Работа";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 332);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(173, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "Гинекологические заболевания:";
            // 
            // txtGenecologyHarmfulness
            // 
            this.txtGenecologyHarmfulness.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGenecologyHarmfulness.Location = new System.Drawing.Point(8, 352);
            this.txtGenecologyHarmfulness.Multiline = true;
            this.txtGenecologyHarmfulness.Name = "txtGenecologyHarmfulness";
            this.txtGenecologyHarmfulness.Size = new System.Drawing.Size(806, 41);
            this.txtGenecologyHarmfulness.TabIndex = 15;
            this.txtGenecologyHarmfulness.Tag = "history.txtGenecologyHarmfulness";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 161);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Трудовая деятельность";
            // 
            // textBox22
            // 
            this.textBox22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox22.Location = new System.Drawing.Point(7, 177);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(806, 24);
            this.textBox22.TabIndex = 13;
            this.textBox22.Tag = "history.txtBeginWork";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 102);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(257, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Нервно-психические травмы, ранения, контузии:";
            // 
            // txtTrauma
            // 
            this.txtTrauma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTrauma.Location = new System.Drawing.Point(7, 125);
            this.txtTrauma.Multiline = true;
            this.txtTrauma.Name = "txtTrauma";
            this.txtTrauma.Size = new System.Drawing.Size(806, 33);
            this.txtTrauma.TabIndex = 11;
            this.txtTrauma.Tag = "history.txtTrauma";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Наследственность:";
            // 
            // txtInherit
            // 
            this.txtInherit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInherit.Location = new System.Drawing.Point(6, 25);
            this.txtInherit.Multiline = true;
            this.txtInherit.Name = "txtInherit";
            this.txtInherit.Size = new System.Drawing.Size(806, 22);
            this.txtInherit.TabIndex = 9;
            this.txtInherit.Tag = "history.txtInherit";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(373, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Перенесенные заболевания, операции. Профессиональные вредности:";
            // 
            // txtDisorder
            // 
            this.txtDisorder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisorder.Location = new System.Drawing.Point(9, 71);
            this.txtDisorder.Multiline = true;
            this.txtDisorder.Name = "txtDisorder";
            this.txtDisorder.Size = new System.Drawing.Size(806, 25);
            this.txtDisorder.TabIndex = 7;
            this.txtDisorder.Tag = "history.txtDisorder";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.checkBox25);
            this.tabPage5.Controls.Add(this.checkBox24);
            this.tabPage5.Controls.Add(this.cmbGlandIH);
            this.tabPage5.Controls.Add(this.txtMeals);
            this.tabPage5.Controls.Add(this.textBox31);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.cmbMuscularIH);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.textBox29);
            this.tabPage5.Controls.Add(this.label33);
            this.tabPage5.Controls.Add(this.cmbMucuosIH);
            this.tabPage5.Controls.Add(this.textBox28);
            this.tabPage5.Controls.Add(this.label32);
            this.tabPage5.Controls.Add(this.cmbSkinIH);
            this.tabPage5.Controls.Add(this.label31);
            this.tabPage5.Controls.Add(this.cmbMeals2IH);
            this.tabPage5.Controls.Add(this.cmbFigureIH);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.txtFigure);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(845, 505);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "Объективное исследование";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(436, 107);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(85, 17);
            this.checkBox25.TabIndex = 30;
            this.checkBox25.Tag = "history.boolSkinRash";
            this.checkBox25.Text = "Высыпания";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(335, 107);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(95, 17);
            this.checkBox24.TabIndex = 29;
            this.checkBox24.Tag = "history.boolSkinDryness";
            this.checkBox24.Text = "Сухость кожи";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // cmbGlandIH
            // 
            this.cmbGlandIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGlandIH.FormattingEnabled = true;
            this.cmbGlandIH.Location = new System.Drawing.Point(123, 291);
            this.cmbGlandIH.Name = "cmbGlandIH";
            this.cmbGlandIH.Size = new System.Drawing.Size(241, 21);
            this.cmbGlandIH.TabIndex = 28;
            this.cmbGlandIH.Tag = "history.lstGland";
            // 
            // txtMeals
            // 
            this.txtMeals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMeals.Location = new System.Drawing.Point(6, 58);
            this.txtMeals.Multiline = true;
            this.txtMeals.Name = "txtMeals";
            this.txtMeals.Size = new System.Drawing.Size(832, 41);
            this.txtMeals.TabIndex = 27;
            this.txtMeals.Tag = "history.txtMeals2";
            // 
            // textBox31
            // 
            this.textBox31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox31.Location = new System.Drawing.Point(6, 348);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(830, 45);
            this.textBox31.TabIndex = 26;
            this.textBox31.Tag = "history.txtMuscular";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 324);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(148, 13);
            this.label35.TabIndex = 25;
            this.label35.Text = "Костно-мышечная система ";
            // 
            // cmbMuscularIH
            // 
            this.cmbMuscularIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMuscularIH.FormattingEnabled = true;
            this.cmbMuscularIH.Location = new System.Drawing.Point(160, 318);
            this.cmbMuscularIH.Name = "cmbMuscularIH";
            this.cmbMuscularIH.Size = new System.Drawing.Size(204, 21);
            this.cmbMuscularIH.TabIndex = 24;
            this.cmbMuscularIH.Tag = "history.lstMuscular";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 294);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(111, 13);
            this.label34.TabIndex = 23;
            this.label34.Text = "Щитовидная железа";
            // 
            // textBox29
            // 
            this.textBox29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox29.Location = new System.Drawing.Point(6, 211);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(830, 74);
            this.textBox29.TabIndex = 21;
            this.textBox29.Tag = "history.txtMucuos";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 188);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(162, 13);
            this.label33.TabIndex = 20;
            this.label33.Text = "Склеры и видимые слизистые";
            // 
            // cmbMucuosIH
            // 
            this.cmbMucuosIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMucuosIH.FormattingEnabled = true;
            this.cmbMucuosIH.Location = new System.Drawing.Point(171, 184);
            this.cmbMucuosIH.Name = "cmbMucuosIH";
            this.cmbMucuosIH.Size = new System.Drawing.Size(189, 21);
            this.cmbMucuosIH.TabIndex = 19;
            this.cmbMucuosIH.Tag = "history.lstMucuos";
            // 
            // textBox28
            // 
            this.textBox28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox28.Location = new System.Drawing.Point(6, 132);
            this.textBox28.Multiline = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(830, 43);
            this.textBox28.TabIndex = 18;
            this.textBox28.Tag = "history.txtSkin";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 108);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(95, 13);
            this.label32.TabIndex = 17;
            this.label32.Text = "Кожные покровы";
            // 
            // cmbSkinIH
            // 
            this.cmbSkinIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSkinIH.FormattingEnabled = true;
            this.cmbSkinIH.Location = new System.Drawing.Point(104, 105);
            this.cmbSkinIH.Name = "cmbSkinIH";
            this.cmbSkinIH.Size = new System.Drawing.Size(225, 21);
            this.cmbSkinIH.TabIndex = 16;
            this.cmbSkinIH.Tag = "history.lstSkin";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 34);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(50, 13);
            this.label31.TabIndex = 15;
            this.label31.Text = "Питание";
            // 
            // cmbMeals2IH
            // 
            this.cmbMeals2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMeals2IH.FormattingEnabled = true;
            this.cmbMeals2IH.Location = new System.Drawing.Point(59, 32);
            this.cmbMeals2IH.Name = "cmbMeals2IH";
            this.cmbMeals2IH.Size = new System.Drawing.Size(301, 21);
            this.cmbMeals2IH.TabIndex = 14;
            this.cmbMeals2IH.Tag = "history.lstMeals2";
            // 
            // cmbFigureIH
            // 
            this.cmbFigureIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFigureIH.FormattingEnabled = true;
            this.cmbFigureIH.Location = new System.Drawing.Point(91, 6);
            this.cmbFigureIH.Name = "cmbFigureIH";
            this.cmbFigureIH.Size = new System.Drawing.Size(98, 21);
            this.cmbFigureIH.TabIndex = 13;
            this.cmbFigureIH.Tag = "history.lstFigure";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Телосложение";
            // 
            // txtFigure
            // 
            this.txtFigure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFigure.Location = new System.Drawing.Point(195, 6);
            this.txtFigure.Name = "txtFigure";
            this.txtFigure.Size = new System.Drawing.Size(641, 20);
            this.txtFigure.TabIndex = 11;
            this.txtFigure.Tag = "history.txtFigure";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.cmbHeartNoise2IH);
            this.tabPage6.Controls.Add(this.cmbHeartNoise1IH);
            this.tabPage6.Controls.Add(this.label41);
            this.tabPage6.Controls.Add(this.label40);
            this.tabPage6.Controls.Add(this.cmbHeartTone2IH);
            this.tabPage6.Controls.Add(this.cmbHeartTone1IH);
            this.tabPage6.Controls.Add(this.cmbPulse2IH);
            this.tabPage6.Controls.Add(this.textBox42);
            this.tabPage6.Controls.Add(this.label47);
            this.tabPage6.Controls.Add(this.textBox41);
            this.tabPage6.Controls.Add(this.label46);
            this.tabPage6.Controls.Add(this.label45);
            this.tabPage6.Controls.Add(this.textBox40);
            this.tabPage6.Controls.Add(this.label44);
            this.tabPage6.Controls.Add(this.cmbArteryPulseIH);
            this.tabPage6.Controls.Add(this.textBox38);
            this.tabPage6.Controls.Add(this.label42);
            this.tabPage6.Controls.Add(this.cmbHeartNoiseIH);
            this.tabPage6.Controls.Add(this.textBox37);
            this.tabPage6.Controls.Add(this.label38);
            this.tabPage6.Controls.Add(this.cmbHeartToneIH);
            this.tabPage6.Controls.Add(this.label37);
            this.tabPage6.Controls.Add(this.textBox32);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.cmbPulse1IH);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(845, 505);
            this.tabPage6.TabIndex = 8;
            this.tabPage6.Text = "Сердечно-сосудистая система";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // cmbHeartNoise2IH
            // 
            this.cmbHeartNoise2IH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbHeartNoise2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartNoise2IH.FormattingEnabled = true;
            this.cmbHeartNoise2IH.Location = new System.Drawing.Point(324, 148);
            this.cmbHeartNoise2IH.Name = "cmbHeartNoise2IH";
            this.cmbHeartNoise2IH.Size = new System.Drawing.Size(512, 21);
            this.cmbHeartNoise2IH.TabIndex = 51;
            this.cmbHeartNoise2IH.Tag = "history.lstHeartNoise2IH";
            this.cmbHeartNoise2IH.Visible = false;
            // 
            // cmbHeartNoise1IH
            // 
            this.cmbHeartNoise1IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartNoise1IH.FormattingEnabled = true;
            this.cmbHeartNoise1IH.Location = new System.Drawing.Point(201, 148);
            this.cmbHeartNoise1IH.Name = "cmbHeartNoise1IH";
            this.cmbHeartNoise1IH.Size = new System.Drawing.Size(117, 21);
            this.cmbHeartNoise1IH.TabIndex = 50;
            this.cmbHeartNoise1IH.Tag = "history.lstHeartNoise1IH";
            this.cmbHeartNoise1IH.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(229, 95);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(33, 13);
            this.label41.TabIndex = 49;
            this.label41.Text = "2 тон";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(229, 69);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(99, 13);
            this.label40.TabIndex = 48;
            this.label40.Text = "1 тон на верхушке";
            // 
            // cmbHeartTone2IH
            // 
            this.cmbHeartTone2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartTone2IH.FormattingEnabled = true;
            this.cmbHeartTone2IH.Location = new System.Drawing.Point(334, 92);
            this.cmbHeartTone2IH.Name = "cmbHeartTone2IH";
            this.cmbHeartTone2IH.Size = new System.Drawing.Size(190, 21);
            this.cmbHeartTone2IH.TabIndex = 47;
            this.cmbHeartTone2IH.Tag = "history.lstHeartTone2IH";
            // 
            // cmbHeartTone1IH
            // 
            this.cmbHeartTone1IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartTone1IH.FormattingEnabled = true;
            this.cmbHeartTone1IH.Location = new System.Drawing.Point(334, 66);
            this.cmbHeartTone1IH.Name = "cmbHeartTone1IH";
            this.cmbHeartTone1IH.Size = new System.Drawing.Size(190, 21);
            this.cmbHeartTone1IH.TabIndex = 46;
            this.cmbHeartTone1IH.Tag = "history.lstHeartTone1IH";
            // 
            // cmbPulse2IH
            // 
            this.cmbPulse2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPulse2IH.FormattingEnabled = true;
            this.cmbPulse2IH.Location = new System.Drawing.Point(310, 12);
            this.cmbPulse2IH.Name = "cmbPulse2IH";
            this.cmbPulse2IH.Size = new System.Drawing.Size(214, 21);
            this.cmbPulse2IH.TabIndex = 45;
            this.cmbPulse2IH.Tag = "history.lstPulse2IH";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(344, 261);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(100, 20);
            this.textBox42.TabIndex = 44;
            this.textBox42.Tag = "history.txtArterialPressureRightIH";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(298, 264);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(43, 13);
            this.label47.TabIndex = 43;
            this.label47.Text = "справа";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(188, 261);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(100, 20);
            this.textBox41.TabIndex = 42;
            this.textBox41.Tag = "history.txtArterialPressureLeftIH";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(142, 264);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 13);
            this.label46.TabIndex = 41;
            this.label46.Text = "слева";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 264);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(133, 13);
            this.label45.TabIndex = 40;
            this.label45.Text = "Артериальное давление:";
            // 
            // textBox40
            // 
            this.textBox40.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox40.Location = new System.Drawing.Point(9, 232);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(832, 20);
            this.textBox40.TabIndex = 39;
            this.textBox40.Tag = "history.txtArteryPulseIH";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 208);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(215, 13);
            this.label44.TabIndex = 38;
            this.label44.Text = "Артерии нижних конечностей: пульсация";
            // 
            // cmbArteryPulseIH
            // 
            this.cmbArteryPulseIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbArteryPulseIH.FormattingEnabled = true;
            this.cmbArteryPulseIH.Location = new System.Drawing.Point(227, 205);
            this.cmbArteryPulseIH.Name = "cmbArteryPulseIH";
            this.cmbArteryPulseIH.Size = new System.Drawing.Size(170, 21);
            this.cmbArteryPulseIH.TabIndex = 37;
            this.cmbArteryPulseIH.Tag = "history.lstArteryPulseIH";
            // 
            // textBox38
            // 
            this.textBox38.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox38.Location = new System.Drawing.Point(5, 175);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(832, 20);
            this.textBox38.TabIndex = 33;
            this.textBox38.Tag = "history.txtHeartNoiseIH";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 151);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(76, 13);
            this.label42.TabIndex = 32;
            this.label42.Text = "Шумы сердца";
            // 
            // cmbHeartNoiseIH
            // 
            this.cmbHeartNoiseIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartNoiseIH.FormattingEnabled = true;
            this.cmbHeartNoiseIH.Location = new System.Drawing.Point(87, 148);
            this.cmbHeartNoiseIH.Name = "cmbHeartNoiseIH";
            this.cmbHeartNoiseIH.Size = new System.Drawing.Size(105, 21);
            this.cmbHeartNoiseIH.TabIndex = 31;
            this.cmbHeartNoiseIH.Tag = "history.lstHeartNoiseIH";
            this.cmbHeartNoiseIH.SelectedIndexChanged += new System.EventHandler(this.comboBox14_SelectedIndexChanged);
            // 
            // textBox37
            // 
            this.textBox37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox37.Location = new System.Drawing.Point(5, 119);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(832, 20);
            this.textBox37.TabIndex = 30;
            this.textBox37.Tag = "history.txtHeartToneIH";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(5, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(73, 13);
            this.label38.TabIndex = 29;
            this.label38.Text = "Тоны сердца";
            // 
            // cmbHeartToneIH
            // 
            this.cmbHeartToneIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHeartToneIH.FormattingEnabled = true;
            this.cmbHeartToneIH.Location = new System.Drawing.Point(86, 39);
            this.cmbHeartToneIH.Name = "cmbHeartToneIH";
            this.cmbHeartToneIH.Size = new System.Drawing.Size(121, 21);
            this.cmbHeartToneIH.TabIndex = 28;
            this.cmbHeartToneIH.Tag = "history.lstHeartToneIH";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(256, 15);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 13);
            this.label37.TabIndex = 19;
            this.label37.Text = "в 1 мин.";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(213, 12);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(37, 20);
            this.textBox32.TabIndex = 18;
            this.textBox32.Tag = "history.intPulseRateIH";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(5, 15);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(38, 13);
            this.label36.TabIndex = 17;
            this.label36.Text = "Пульс";
            // 
            // cmbPulse1IH
            // 
            this.cmbPulse1IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPulse1IH.FormattingEnabled = true;
            this.cmbPulse1IH.Location = new System.Drawing.Point(86, 12);
            this.cmbPulse1IH.Name = "cmbPulse1IH";
            this.cmbPulse1IH.Size = new System.Drawing.Size(121, 21);
            this.cmbPulse1IH.TabIndex = 16;
            this.cmbPulse1IH.Tag = "history.lstPulse1IH";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.textBox45);
            this.tabPage7.Controls.Add(this.label50);
            this.tabPage7.Controls.Add(cmbCracklingIH);
            this.tabPage7.Controls.Add(this.textBox44);
            this.tabPage7.Controls.Add(this.label49);
            this.tabPage7.Controls.Add(cmbBreathingIH);
            this.tabPage7.Controls.Add(this.textBox43);
            this.tabPage7.Controls.Add(this.label48);
            this.tabPage7.Controls.Add(cmbSoundIH);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(845, 505);
            this.tabPage7.TabIndex = 9;
            this.tabPage7.Text = "Органы дыхания";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // textBox45
            // 
            this.textBox45.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox45.Location = new System.Drawing.Point(8, 151);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(803, 20);
            this.textBox45.TabIndex = 26;
            this.textBox45.Tag = "history.txtCracklingIH";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 127);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 13);
            this.label50.TabIndex = 25;
            this.label50.Text = "Хрипы";
            // 
            // textBox44
            // 
            this.textBox44.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox44.Location = new System.Drawing.Point(8, 94);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(803, 20);
            this.textBox44.TabIndex = 23;
            this.textBox44.Tag = "history.txtBreathingIH";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 70);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(134, 13);
            this.label49.TabIndex = 22;
            this.label49.Text = "Аускультативно дыхание";
            // 
            // textBox43
            // 
            this.textBox43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox43.Location = new System.Drawing.Point(8, 38);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(803, 20);
            this.textBox43.TabIndex = 20;
            this.textBox43.Tag = "history.txtSoundIH";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 14);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(195, 13);
            this.label48.TabIndex = 19;
            this.label48.Text = "При сравнительной перекуссии звук";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.cmbStool2);
            this.tabPage8.Controls.Add(this.cmbStool1);
            this.tabPage8.Controls.Add(this.label87);
            this.tabPage8.Controls.Add(this.label43);
            this.tabPage8.Controls.Add(this.cmbLiver4IH);
            this.tabPage8.Controls.Add(this.cmbLiver3IH);
            this.tabPage8.Controls.Add(this.cmbLiver2IH);
            this.tabPage8.Controls.Add(this.groupBox3);
            this.tabPage8.Controls.Add(this.textBox52);
            this.tabPage8.Controls.Add(this.label57);
            this.tabPage8.Controls.Add(this.cmbSpleenIH);
            this.tabPage8.Controls.Add(this.label56);
            this.tabPage8.Controls.Add(this.textBox51);
            this.tabPage8.Controls.Add(this.textBox50);
            this.tabPage8.Controls.Add(this.label55);
            this.tabPage8.Controls.Add(this.cmbLiver1IH);
            this.tabPage8.Controls.Add(this.textBox48);
            this.tabPage8.Controls.Add(this.label53);
            this.tabPage8.Controls.Add(this.cmbMendalinsIH);
            this.tabPage8.Controls.Add(this.textBox47);
            this.tabPage8.Controls.Add(this.label52);
            this.tabPage8.Controls.Add(this.cmbToothsIH);
            this.tabPage8.Controls.Add(this.textBox46);
            this.tabPage8.Controls.Add(this.label51);
            this.tabPage8.Controls.Add(this.cmbTongueIH);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(845, 505);
            this.tabPage8.TabIndex = 10;
            this.tabPage8.Text = "Органы пищеварения";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // cmbStool2
            // 
            this.cmbStool2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStool2.FormattingEnabled = true;
            this.cmbStool2.Location = new System.Drawing.Point(191, 419);
            this.cmbStool2.Name = "cmbStool2";
            this.cmbStool2.Size = new System.Drawing.Size(130, 21);
            this.cmbStool2.TabIndex = 69;
            this.cmbStool2.Tag = "history.lstStool2";
            // 
            // cmbStool1
            // 
            this.cmbStool1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStool1.FormattingEnabled = true;
            this.cmbStool1.Location = new System.Drawing.Point(55, 419);
            this.cmbStool1.Name = "cmbStool1";
            this.cmbStool1.Size = new System.Drawing.Size(130, 21);
            this.cmbStool1.TabIndex = 68;
            this.cmbStool1.Tag = "history.lstStool1";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(8, 422);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(30, 13);
            this.label87.TabIndex = 67;
            this.label87.Tag = "history.";
            this.label87.Text = "Стул";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(326, 286);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(19, 13);
            this.label43.TabIndex = 66;
            this.label43.Tag = "history.";
            this.label43.Text = "на";
            this.label43.Visible = false;
            // 
            // cmbLiver4IH
            // 
            this.cmbLiver4IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiver4IH.FormattingEnabled = true;
            this.cmbLiver4IH.Location = new System.Drawing.Point(323, 310);
            this.cmbLiver4IH.Name = "cmbLiver4IH";
            this.cmbLiver4IH.Size = new System.Drawing.Size(196, 21);
            this.cmbLiver4IH.TabIndex = 65;
            this.cmbLiver4IH.Tag = "history.lstLiver4";
            // 
            // cmbLiver3IH
            // 
            this.cmbLiver3IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiver3IH.FormattingEnabled = true;
            this.cmbLiver3IH.Location = new System.Drawing.Point(162, 310);
            this.cmbLiver3IH.Name = "cmbLiver3IH";
            this.cmbLiver3IH.Size = new System.Drawing.Size(155, 21);
            this.cmbLiver3IH.TabIndex = 64;
            this.cmbLiver3IH.Tag = "history.lstLiver3";
            // 
            // cmbLiver2IH
            // 
            this.cmbLiver2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiver2IH.FormattingEnabled = true;
            this.cmbLiver2IH.Location = new System.Drawing.Point(8, 310);
            this.cmbLiver2IH.Name = "cmbLiver2IH";
            this.cmbLiver2IH.Size = new System.Drawing.Size(148, 21);
            this.cmbLiver2IH.TabIndex = 63;
            this.cmbLiver2IH.Tag = "history.lstLiver2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtStomachNormal);
            this.groupBox3.Controls.Add(this.cmbStomachNormalIH);
            this.groupBox3.Controls.Add(this.checkBox18);
            this.groupBox3.Controls.Add(this.checkBox19);
            this.groupBox3.Controls.Add(this.cmbStomachNormal1IH);
            this.groupBox3.Controls.Add(this.checkBox20);
            this.groupBox3.Controls.Add(this.cmbStomachNormal2IH);
            this.groupBox3.Controls.Add(this.checkBox21);
            this.groupBox3.Controls.Add(this.checkBox23);
            this.groupBox3.Location = new System.Drawing.Point(5, 161);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(755, 116);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Tag = "history.";
            this.groupBox3.Text = "Живот";
            // 
            // txtStomachNormal
            // 
            this.txtStomachNormal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStomachNormal.Location = new System.Drawing.Point(230, 15);
            this.txtStomachNormal.Name = "txtStomachNormal";
            this.txtStomachNormal.Size = new System.Drawing.Size(519, 20);
            this.txtStomachNormal.TabIndex = 70;
            this.txtStomachNormal.Tag = "history.txtStomachNormal";
            // 
            // cmbStomachNormalIH
            // 
            this.cmbStomachNormalIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStomachNormalIH.FormattingEnabled = true;
            this.cmbStomachNormalIH.Location = new System.Drawing.Point(6, 15);
            this.cmbStomachNormalIH.Name = "cmbStomachNormalIH";
            this.cmbStomachNormalIH.Size = new System.Drawing.Size(218, 21);
            this.cmbStomachNormalIH.TabIndex = 61;
            this.cmbStomachNormalIH.Tag = "history.lstStomachNormal";
            this.cmbStomachNormalIH.SelectedIndexChanged += new System.EventHandler(this.cmbStomachNormalIH_SelectedIndexChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(6, 92);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(207, 17);
            this.checkBox18.TabIndex = 57;
            this.checkBox18.Tag = "history.boolStomachSickly2";
            this.checkBox18.Text = "болезненный в правом подреберье";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(269, 69);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(221, 17);
            this.checkBox19.TabIndex = 60;
            this.checkBox19.Tag = "history.boolStomachSickly3";
            this.checkBox19.Text = "болезненный в панкреатической зоне";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // cmbStomachNormal1IH
            // 
            this.cmbStomachNormal1IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStomachNormal1IH.FormattingEnabled = true;
            this.cmbStomachNormal1IH.Location = new System.Drawing.Point(6, 42);
            this.cmbStomachNormal1IH.Name = "cmbStomachNormal1IH";
            this.cmbStomachNormal1IH.Size = new System.Drawing.Size(218, 21);
            this.cmbStomachNormal1IH.TabIndex = 37;
            this.cmbStomachNormal1IH.Tag = "history.lstStomachNormal1";
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(268, 92);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(240, 17);
            this.checkBox20.TabIndex = 59;
            this.checkBox20.Tag = "history.boolStomachSickly5";
            this.checkBox20.Text = "болезненный в пилородуоденальной зоне";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // cmbStomachNormal2IH
            // 
            this.cmbStomachNormal2IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStomachNormal2IH.FormattingEnabled = true;
            this.cmbStomachNormal2IH.Location = new System.Drawing.Point(230, 42);
            this.cmbStomachNormal2IH.Name = "cmbStomachNormal2IH";
            this.cmbStomachNormal2IH.Size = new System.Drawing.Size(151, 21);
            this.cmbStomachNormal2IH.TabIndex = 52;
            this.cmbStomachNormal2IH.Tag = "history.lstStomachNormal2";
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(509, 69);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(245, 17);
            this.checkBox21.TabIndex = 58;
            this.checkBox21.Tag = "history.boolStomachSickly4";
            this.checkBox21.Text = "болезненный в проекции желчного пузыря";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(7, 69);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(232, 17);
            this.checkBox23.TabIndex = 56;
            this.checkBox23.Tag = "history.boolStomachSickly1";
            this.checkBox23.Text = "болезненный в эпигастральной области";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // textBox52
            // 
            this.textBox52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox52.Location = new System.Drawing.Point(9, 389);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(828, 20);
            this.textBox52.TabIndex = 40;
            this.textBox52.Tag = "history.txtSplleen";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(7, 365);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(62, 13);
            this.label57.TabIndex = 39;
            this.label57.Tag = "history.";
            this.label57.Text = "Селезенка";
            // 
            // cmbSpleenIH
            // 
            this.cmbSpleenIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpleenIH.FormattingEnabled = true;
            this.cmbSpleenIH.Location = new System.Drawing.Point(77, 362);
            this.cmbSpleenIH.Name = "cmbSpleenIH";
            this.cmbSpleenIH.Size = new System.Drawing.Size(183, 21);
            this.cmbSpleenIH.TabIndex = 38;
            this.cmbSpleenIH.Tag = "history.lstSpleen";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(403, 287);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(24, 13);
            this.label56.TabIndex = 37;
            this.label56.Tag = "history.";
            this.label56.Text = "см.";
            this.label56.Visible = false;
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(351, 283);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(45, 20);
            this.textBox51.TabIndex = 36;
            this.textBox51.Tag = "history.txtLiver1";
            this.textBox51.Visible = false;
            // 
            // textBox50
            // 
            this.textBox50.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox50.Location = new System.Drawing.Point(8, 335);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(828, 20);
            this.textBox50.TabIndex = 35;
            this.textBox50.Tag = "history.txtLiver2";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 286);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(44, 13);
            this.label55.TabIndex = 34;
            this.label55.Tag = "history.";
            this.label55.Text = "Печень";
            // 
            // cmbLiver1IH
            // 
            this.cmbLiver1IH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiver1IH.FormattingEnabled = true;
            this.cmbLiver1IH.Location = new System.Drawing.Point(55, 283);
            this.cmbLiver1IH.Name = "cmbLiver1IH";
            this.cmbLiver1IH.Size = new System.Drawing.Size(262, 21);
            this.cmbLiver1IH.TabIndex = 33;
            this.cmbLiver1IH.Tag = "history.lstLiver1";
            this.cmbLiver1IH.SelectedIndexChanged += new System.EventHandler(this.comboBox24_SelectedIndexChanged);
            // 
            // textBox48
            // 
            this.textBox48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox48.Location = new System.Drawing.Point(8, 139);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(828, 20);
            this.textBox48.TabIndex = 29;
            this.textBox48.Tag = "history.txtMendalins";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 115);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(66, 13);
            this.label53.TabIndex = 28;
            this.label53.Tag = "history.";
            this.label53.Text = "Миндалины";
            // 
            // cmbMendalinsIH
            // 
            this.cmbMendalinsIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMendalinsIH.FormattingEnabled = true;
            this.cmbMendalinsIH.Location = new System.Drawing.Point(76, 112);
            this.cmbMendalinsIH.Name = "cmbMendalinsIH";
            this.cmbMendalinsIH.Size = new System.Drawing.Size(168, 21);
            this.cmbMendalinsIH.TabIndex = 27;
            this.cmbMendalinsIH.Tag = "history.lstMendalins";
            // 
            // textBox47
            // 
            this.textBox47.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox47.Location = new System.Drawing.Point(8, 86);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(828, 20);
            this.textBox47.TabIndex = 26;
            this.textBox47.Tag = "history.txtTooths";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 62);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(33, 13);
            this.label52.TabIndex = 25;
            this.label52.Tag = "history.";
            this.label52.Text = "Зубы";
            // 
            // cmbToothsIH
            // 
            this.cmbToothsIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToothsIH.FormattingEnabled = true;
            this.cmbToothsIH.Location = new System.Drawing.Point(76, 59);
            this.cmbToothsIH.Name = "cmbToothsIH";
            this.cmbToothsIH.Size = new System.Drawing.Size(168, 21);
            this.cmbToothsIH.TabIndex = 24;
            this.cmbToothsIH.Tag = "history.lstTooths";
            // 
            // textBox46
            // 
            this.textBox46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox46.Location = new System.Drawing.Point(8, 33);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(828, 20);
            this.textBox46.TabIndex = 23;
            this.textBox46.Tag = "history.txtTongue";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 9);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(35, 13);
            this.label51.TabIndex = 22;
            this.label51.Tag = "history.";
            this.label51.Text = "Язык";
            // 
            // cmbTongueIH
            // 
            this.cmbTongueIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTongueIH.FormattingEnabled = true;
            this.cmbTongueIH.Location = new System.Drawing.Point(76, 6);
            this.cmbTongueIH.Name = "cmbTongueIH";
            this.cmbTongueIH.Size = new System.Drawing.Size(168, 21);
            this.cmbTongueIH.TabIndex = 21;
            this.cmbTongueIH.Tag = "history.lstTongue";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.checkBox27);
            this.tabPage9.Controls.Add(this.checkBox26);
            this.tabPage9.Controls.Add(this.textBox53);
            this.tabPage9.Controls.Add(this.label58);
            this.tabPage9.Controls.Add(this.cmbUrinationIH);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(845, 505);
            this.tabPage9.TabIndex = 11;
            this.tabPage9.Text = "Мочеполовая система";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(428, 11);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(93, 17);
            this.checkBox27.TabIndex = 28;
            this.checkBox27.Tag = "history.boolPainfully";
            this.checkBox27.Text = "Болезненное";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(334, 11);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(72, 17);
            this.checkBox26.TabIndex = 27;
            this.checkBox26.Tag = "history.boolMoroFrequent";
            this.checkBox26.Text = "Учащено";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // textBox53
            // 
            this.textBox53.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox53.Location = new System.Drawing.Point(8, 36);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(829, 20);
            this.textBox53.TabIndex = 26;
            this.textBox53.Tag = "history.txtUrination";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 12);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(92, 13);
            this.label58.TabIndex = 25;
            this.label58.Text = "Мочеиспускание";
            // 
            // cmbUrinationIH
            // 
            this.cmbUrinationIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUrinationIH.FormattingEnabled = true;
            this.cmbUrinationIH.Location = new System.Drawing.Point(101, 9);
            this.cmbUrinationIH.Name = "cmbUrinationIH";
            this.cmbUrinationIH.Size = new System.Drawing.Size(212, 21);
            this.cmbUrinationIH.TabIndex = 24;
            this.cmbUrinationIH.Tag = "history.lstUrination";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.chkSupportMotion);
            this.tabPage10.Controls.Add(this.txtSupportMotion);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(845, 505);
            this.tabPage10.TabIndex = 12;
            this.tabPage10.Text = "Органы опоры и движения";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // chkSupportMotion
            // 
            this.chkSupportMotion.AutoSize = true;
            this.chkSupportMotion.Location = new System.Drawing.Point(7, 13);
            this.chkSupportMotion.Name = "chkSupportMotion";
            this.chkSupportMotion.Size = new System.Drawing.Size(100, 17);
            this.chkSupportMotion.TabIndex = 3;
            this.chkSupportMotion.Tag = "history.boolSupportMotion";
            this.chkSupportMotion.Text = "Без патологии";
            this.chkSupportMotion.UseVisualStyleBackColor = true;
            this.chkSupportMotion.CheckedChanged += new System.EventHandler(this.chkSupportMotion_CheckedChanged);
            // 
            // txtSupportMotion
            // 
            this.txtSupportMotion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSupportMotion.Location = new System.Drawing.Point(7, 36);
            this.txtSupportMotion.Multiline = true;
            this.txtSupportMotion.Name = "txtSupportMotion";
            this.txtSupportMotion.Size = new System.Drawing.Size(830, 464);
            this.txtSupportMotion.TabIndex = 2;
            this.txtSupportMotion.Tag = "history.txtSupportMotion";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.chkNoPeculiarity);
            this.tabPage11.Controls.Add(this.txtNervose);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(845, 505);
            this.tabPage11.TabIndex = 13;
            this.tabPage11.Text = "Нервная система";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // chkNoPeculiarity
            // 
            this.chkNoPeculiarity.AutoSize = true;
            this.chkNoPeculiarity.Location = new System.Drawing.Point(9, 6);
            this.chkNoPeculiarity.Name = "chkNoPeculiarity";
            this.chkNoPeculiarity.Size = new System.Drawing.Size(118, 17);
            this.chkNoPeculiarity.TabIndex = 3;
            this.chkNoPeculiarity.Tag = "history.boolNoPeculiarity";
            this.chkNoPeculiarity.Text = "без особенностей";
            this.chkNoPeculiarity.UseVisualStyleBackColor = true;
            this.chkNoPeculiarity.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // txtNervose
            // 
            this.txtNervose.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNervose.Location = new System.Drawing.Point(7, 29);
            this.txtNervose.Multiline = true;
            this.txtNervose.Name = "txtNervose";
            this.txtNervose.Size = new System.Drawing.Size(806, 470);
            this.txtNervose.TabIndex = 2;
            this.txtNervose.Tag = "history.txtNervose";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.lblServicePrice);
            this.tabPage12.Controls.Add(this.lblPrice);
            this.tabPage12.Controls.Add(this.lblMedPrice);
            this.tabPage12.Controls.Add(this.label82);
            this.tabPage12.Controls.Add(this.label71);
            this.tabPage12.Controls.Add(this.label70);
            this.tabPage12.Controls.Add(this.gridMeds);
            this.tabPage12.Controls.Add(this.btnAddMedicine);
            this.tabPage12.Controls.Add(this.btnDelMedicine);
            this.tabPage12.Controls.Add(this.btnAddService);
            this.tabPage12.Controls.Add(this.btnDelService);
            this.tabPage12.Controls.Add(this.label74);
            this.tabPage12.Controls.Add(this.label73);
            this.tabPage12.Controls.Add(this.gridService);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(845, 505);
            this.tabPage12.TabIndex = 14;
            this.tabPage12.Text = "Назначения";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // lblServicePrice
            // 
            this.lblServicePrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServicePrice.Location = new System.Drawing.Point(694, 187);
            this.lblServicePrice.Name = "lblServicePrice";
            this.lblServicePrice.ReadOnly = true;
            this.lblServicePrice.Size = new System.Drawing.Size(64, 20);
            this.lblServicePrice.TabIndex = 68;
            this.lblServicePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPrice
            // 
            this.lblPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrice.Location = new System.Drawing.Point(694, 485);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.ReadOnly = true;
            this.lblPrice.Size = new System.Drawing.Size(64, 20);
            this.lblPrice.TabIndex = 68;
            this.lblPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMedPrice
            // 
            this.lblMedPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMedPrice.Location = new System.Drawing.Point(694, 464);
            this.lblMedPrice.Name = "lblMedPrice";
            this.lblMedPrice.ReadOnly = true;
            this.lblMedPrice.Size = new System.Drawing.Size(64, 20);
            this.lblMedPrice.TabIndex = 68;
            this.lblMedPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label82
            // 
            this.label82.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(564, 488);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(125, 13);
            this.label82.TabIndex = 67;
            this.label82.Text = "Всего по назначениям:";
            // 
            // label71
            // 
            this.label71.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(554, 466);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(135, 13);
            this.label71.TabIndex = 66;
            this.label71.Text = "Итого по медикаментам:";
            // 
            // label70
            // 
            this.label70.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(590, 190);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(99, 13);
            this.label70.TabIndex = 65;
            this.label70.Text = "Итого по услугам:";
            // 
            // gridMeds
            // 
            this.gridMeds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn8,
            this.colCountMed,
            this.CountDivide,
            this.Column7,
            this.colRecept,
            this.dataGridViewTextBoxColumn10});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridMeds.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridMeds.Location = new System.Drawing.Point(6, 221);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridMeds.RowHeadersVisible = false;
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(753, 241);
            this.gridMeds.TabIndex = 63;
            this.gridMeds.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridMeds_CellBeginEdit);
            this.gridMeds.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellEndEdit);
            this.gridMeds.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "UnReserved";
            this.Column6.FillWeight = 20F;
            this.Column6.HeaderText = "";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column6.Width = 20;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MedicamentOnScladID";
            this.dataGridViewTextBoxColumn2.FillWeight = 30F;
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 30;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "MedicamentN";
            this.dataGridViewTextBoxColumn6.FillWeight = 200F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Наименование";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Price";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn8.FillWeight = 60F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Цена";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 60;
            // 
            // colCountMed
            // 
            this.colCountMed.DataPropertyName = "CountOutput";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.colCountMed.DefaultCellStyle = dataGridViewCellStyle3;
            this.colCountMed.FillWeight = 35F;
            this.colCountMed.HeaderText = "К-во";
            this.colCountMed.Name = "colCountMed";
            this.colCountMed.Width = 35;
            // 
            // CountDivide
            // 
            this.CountDivide.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CountDivide.DataPropertyName = "CountDivide";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CountDivide.DefaultCellStyle = dataGridViewCellStyle4;
            this.CountDivide.FillWeight = 55F;
            this.CountDivide.HeaderText = "На скл.";
            this.CountDivide.Name = "CountDivide";
            this.CountDivide.ReadOnly = true;
            this.CountDivide.Width = 65;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "PriceSum";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "C2";
            dataGridViewCellStyle5.NullValue = null;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column7.FillWeight = 70F;
            this.Column7.HeaderText = "Сумма";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 70;
            // 
            // colRecept
            // 
            this.colRecept.DataPropertyName = "Prescription";
            this.colRecept.FillWeight = 200F;
            this.colRecept.HeaderText = "Рецепт индивидуальный ";
            this.colRecept.Name = "colRecept";
            this.colRecept.Width = 200;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MedicamentD";
            this.dataGridViewTextBoxColumn10.FillWeight = 70F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Дата";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 70;
            // 
            // btnAddMedicine
            // 
            this.btnAddMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMedicine.Image")));
            this.btnAddMedicine.Location = new System.Drawing.Point(765, 221);
            this.btnAddMedicine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddMedicine.Name = "btnAddMedicine";
            this.btnAddMedicine.Size = new System.Drawing.Size(77, 23);
            this.btnAddMedicine.TabIndex = 61;
            this.btnAddMedicine.Text = "Добавить";
            this.btnAddMedicine.Click += new System.EventHandler(this.btnAddMedicine_Click);
            // 
            // btnDelMedicine
            // 
            this.btnDelMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnDelMedicine.Image")));
            this.btnDelMedicine.Location = new System.Drawing.Point(765, 250);
            this.btnDelMedicine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelMedicine.Name = "btnDelMedicine";
            this.btnDelMedicine.Size = new System.Drawing.Size(77, 23);
            this.btnDelMedicine.TabIndex = 60;
            this.btnDelMedicine.Text = "Удалить";
            this.btnDelMedicine.Click += new System.EventHandler(this.btnDelMedecine_Click);
            // 
            // btnAddService
            // 
            this.btnAddService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddService.Image = ((System.Drawing.Image)(resources.GetObject("btnAddService.Image")));
            this.btnAddService.Location = new System.Drawing.Point(765, 20);
            this.btnAddService.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddService.Name = "btnAddService";
            this.btnAddService.Size = new System.Drawing.Size(77, 23);
            this.btnAddService.TabIndex = 59;
            this.btnAddService.Text = "Добавить";
            this.btnAddService.Click += new System.EventHandler(this.btnAddService_Click);
            // 
            // btnDelService
            // 
            this.btnDelService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelService.Image = ((System.Drawing.Image)(resources.GetObject("btnDelService.Image")));
            this.btnDelService.Location = new System.Drawing.Point(765, 49);
            this.btnDelService.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelService.Name = "btnDelService";
            this.btnDelService.Size = new System.Drawing.Size(77, 23);
            this.btnDelService.TabIndex = 58;
            this.btnDelService.Text = "Удалить";
            this.btnDelService.Click += new System.EventHandler(this.btnDelService_Click);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(3, 205);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(79, 13);
            this.label74.TabIndex = 52;
            this.label74.Text = "Медикаменты";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(3, 5);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(43, 13);
            this.label73.TabIndex = 50;
            this.label73.Text = "Услуги";
            // 
            // gridService
            // 
            this.gridService.AllowUserToAddRows = false;
            this.gridService.AllowUserToDeleteRows = false;
            this.gridService.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridService.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.gridService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridService.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFulfil,
            this.Column1,
            this.Column5,
            this.colCount,
            this.Column2,
            this.colCharacter,
            this.Column4});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridService.DefaultCellStyle = dataGridViewCellStyle11;
            this.gridService.Location = new System.Drawing.Point(4, 20);
            this.gridService.MultiSelect = false;
            this.gridService.Name = "gridService";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridService.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.gridService.RowHeadersVisible = false;
            this.gridService.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridService.Size = new System.Drawing.Size(754, 167);
            this.gridService.TabIndex = 49;
            this.gridService.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridService_CellBeginEdit);
            this.gridService.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridService_CellEndEdit);
            this.gridService.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.gridService_DataError);
            // 
            // colFulfil
            // 
            this.colFulfil.DataPropertyName = "Fulfil";
            this.colFulfil.FillWeight = 20F;
            this.colFulfil.HeaderText = "";
            this.colFulfil.Name = "colFulfil";
            this.colFulfil.ReadOnly = true;
            this.colFulfil.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colFulfil.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colFulfil.Width = 20;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Code";
            this.Column1.FillWeight = 50F;
            this.Column1.HeaderText = "Шифр";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.Width = 50;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ServiceN";
            this.Column5.FillWeight = 250F;
            this.Column5.HeaderText = "Наименование";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 250;
            // 
            // colCount
            // 
            this.colCount.DataPropertyName = "Count";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = "0";
            this.colCount.DefaultCellStyle = dataGridViewCellStyle9;
            this.colCount.FillWeight = 35F;
            this.colCount.HeaderText = "К-во";
            this.colCount.Name = "colCount";
            this.colCount.Width = 35;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "PriceSum";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "C2";
            dataGridViewCellStyle10.NullValue = null;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column2.FillWeight = 120F;
            this.Column2.HeaderText = "Сумма лек. ср-в";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 120;
            // 
            // colCharacter
            // 
            this.colCharacter.DataPropertyName = "Character";
            this.colCharacter.FillWeight = 200F;
            this.colCharacter.HeaderText = "Характеристика";
            this.colCharacter.Name = "colCharacter";
            this.colCharacter.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Date";
            this.Column4.HeaderText = "Дата";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 80;
            // 
            // tabPage13
            // 
            this.tabPage13.AutoScroll = true;
            this.tabPage13.Controls.Add(this.tabControlObservations);
            this.tabPage13.Controls.Add(this.toolStrip2);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(790, 482);
            this.tabPage13.TabIndex = 15;
            this.tabPage13.Text = "Течение болезни";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // tabControlObservations
            // 
            this.tabControlObservations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlObservations.Controls.Add(this.tabPageTherapist);
            this.tabControlObservations.Controls.Add(this.tabPageDerma);
            this.tabControlObservations.Controls.Add(this.tabPage17);
            this.tabControlObservations.Location = new System.Drawing.Point(3, 28);
            this.tabControlObservations.Name = "tabControlObservations";
            this.tabControlObservations.SelectedIndex = 0;
            this.tabControlObservations.Size = new System.Drawing.Size(783, 454);
            this.tabControlObservations.TabIndex = 77;
            // 
            // tabPageTherapist
            // 
            this.tabPageTherapist.AutoScroll = true;
            this.tabPageTherapist.Controls.Add(this.label79);
            this.tabPageTherapist.Controls.Add(this.textBox2);
            this.tabPageTherapist.Controls.Add(this.label78);
            this.tabPageTherapist.Controls.Add(this.label67);
            this.tabPageTherapist.Controls.Add(this.cmbStoolOSV);
            this.tabPageTherapist.Controls.Add(this.checkBox28);
            this.tabPageTherapist.Controls.Add(this.checkBox29);
            this.tabPageTherapist.Controls.Add(this.textBox34);
            this.tabPageTherapist.Controls.Add(this.label65);
            this.tabPageTherapist.Controls.Add(this.cmbUrinationOSV);
            this.tabPageTherapist.Controls.Add(this.cmbNoiseHeart3OSV);
            this.tabPageTherapist.Controls.Add(this.cmbNoiseHeart2OSV);
            this.tabPageTherapist.Controls.Add(this.textBox30);
            this.tabPageTherapist.Controls.Add(this.label54);
            this.tabPageTherapist.Controls.Add(this.cmbNoiseHeart1OSV);
            this.tabPageTherapist.Controls.Add(this.groupBox2);
            this.tabPageTherapist.Controls.Add(this.label39);
            this.tabPageTherapist.Controls.Add(this.cmbCrepitationOSV);
            this.tabPageTherapist.Controls.Add(this.chkComplaint);
            this.tabPageTherapist.Controls.Add(this.cmbStateOSV);
            this.tabPageTherapist.Controls.Add(this.textBox60);
            this.tabPageTherapist.Controls.Add(this.label69);
            this.tabPageTherapist.Controls.Add(this.label68);
            this.tabPageTherapist.Controls.Add(cmbMendalinsOSV);
            this.tabPageTherapist.Controls.Add(this.label66);
            this.tabPageTherapist.Controls.Add(this.cmbTongueOSV);
            this.tabPageTherapist.Controls.Add(this.textBox59);
            this.tabPageTherapist.Controls.Add(this.label64);
            this.tabPageTherapist.Controls.Add(this.textBox58);
            this.tabPageTherapist.Controls.Add(this.label63);
            this.tabPageTherapist.Controls.Add(this.cmbToneHeartOSV);
            this.tabPageTherapist.Controls.Add(this.label62);
            this.tabPageTherapist.Controls.Add(this.label61);
            this.tabPageTherapist.Controls.Add(this.cmbBreathingOSV);
            this.tabPageTherapist.Controls.Add(this.textBox57);
            this.tabPageTherapist.Controls.Add(this.label60);
            this.tabPageTherapist.Controls.Add(this.txtComplaint);
            this.tabPageTherapist.Controls.Add(this.lbObserv);
            this.tabPageTherapist.Controls.Add(this.label72);
            this.tabPageTherapist.Location = new System.Drawing.Point(4, 22);
            this.tabPageTherapist.Name = "tabPageTherapist";
            this.tabPageTherapist.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPageTherapist.Size = new System.Drawing.Size(775, 428);
            this.tabPageTherapist.TabIndex = 0;
            this.tabPageTherapist.Text = "Терапевт";
            this.tabPageTherapist.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(52, 440);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(68, 13);
            this.label79.TabIndex = 113;
            this.label79.Text = "Назначения";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(191, 458);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(409, 53);
            this.textBox2.TabIndex = 112;
            this.textBox2.Tag = "observat.txtResult";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(44, 458);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(142, 13);
            this.label78.TabIndex = 111;
            this.label78.Text = "Результаты обследования";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(458, 385);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(30, 13);
            this.label67.TabIndex = 110;
            this.label67.Text = "Стул";
            // 
            // cmbStoolOSV
            // 
            this.cmbStoolOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStoolOSV.FormattingEnabled = true;
            this.cmbStoolOSV.Location = new System.Drawing.Point(494, 382);
            this.cmbStoolOSV.Name = "cmbStoolOSV";
            this.cmbStoolOSV.Size = new System.Drawing.Size(161, 21);
            this.cmbStoolOSV.TabIndex = 109;
            this.cmbStoolOSV.Tag = "observat.lstStool";
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(558, 411);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(93, 17);
            this.checkBox28.TabIndex = 108;
            this.checkBox28.Tag = "observat.boolUrination2";
            this.checkBox28.Text = "Болезненное";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(461, 411);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(72, 17);
            this.checkBox29.TabIndex = 107;
            this.checkBox29.Tag = "observat.boolUrination1";
            this.checkBox29.Text = "Учащено";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // textBox34
            // 
            this.textBox34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox34.Location = new System.Drawing.Point(191, 436);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(409, 20);
            this.textBox34.TabIndex = 106;
            this.textBox34.Tag = "observat.txtUrination";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(161, 412);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 13);
            this.label65.TabIndex = 105;
            this.label65.Text = "Мочеиспускание";
            // 
            // cmbUrinationOSV
            // 
            this.cmbUrinationOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUrinationOSV.FormattingEnabled = true;
            this.cmbUrinationOSV.Location = new System.Drawing.Point(256, 409);
            this.cmbUrinationOSV.Name = "cmbUrinationOSV";
            this.cmbUrinationOSV.Size = new System.Drawing.Size(181, 21);
            this.cmbUrinationOSV.TabIndex = 104;
            this.cmbUrinationOSV.Tag = "observat.lstUrination";
            // 
            // cmbNoiseHeart3OSV
            // 
            this.cmbNoiseHeart3OSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNoiseHeart3OSV.FormattingEnabled = true;
            this.cmbNoiseHeart3OSV.Location = new System.Drawing.Point(504, 175);
            this.cmbNoiseHeart3OSV.Name = "cmbNoiseHeart3OSV";
            this.cmbNoiseHeart3OSV.Size = new System.Drawing.Size(162, 21);
            this.cmbNoiseHeart3OSV.TabIndex = 103;
            this.cmbNoiseHeart3OSV.Tag = "observat.lstNoiseHeart3";
            this.cmbNoiseHeart3OSV.Visible = false;
            // 
            // cmbNoiseHeart2OSV
            // 
            this.cmbNoiseHeart2OSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNoiseHeart2OSV.FormattingEnabled = true;
            this.cmbNoiseHeart2OSV.Location = new System.Drawing.Point(381, 175);
            this.cmbNoiseHeart2OSV.Name = "cmbNoiseHeart2OSV";
            this.cmbNoiseHeart2OSV.Size = new System.Drawing.Size(117, 21);
            this.cmbNoiseHeart2OSV.TabIndex = 102;
            this.cmbNoiseHeart2OSV.Tag = "observat.lstNoiseHeart2";
            this.cmbNoiseHeart2OSV.Visible = false;
            // 
            // textBox30
            // 
            this.textBox30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox30.Location = new System.Drawing.Point(161, 202);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(439, 20);
            this.textBox30.TabIndex = 101;
            this.textBox30.Tag = "observat.txtNoiseHeart";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(159, 178);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(76, 13);
            this.label54.TabIndex = 100;
            this.label54.Text = "Шумы сердца";
            // 
            // cmbNoiseHeart1OSV
            // 
            this.cmbNoiseHeart1OSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNoiseHeart1OSV.FormattingEnabled = true;
            this.cmbNoiseHeart1OSV.Location = new System.Drawing.Point(256, 175);
            this.cmbNoiseHeart1OSV.Name = "cmbNoiseHeart1OSV";
            this.cmbNoiseHeart1OSV.Size = new System.Drawing.Size(119, 21);
            this.cmbNoiseHeart1OSV.TabIndex = 99;
            this.cmbNoiseHeart1OSV.Tag = "observat.lstNoiseHeart1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtStomachNormalOSV);
            this.groupBox2.Controls.Add(this.cmbStomachNormalOSV);
            this.groupBox2.Controls.Add(this.checkBox14);
            this.groupBox2.Controls.Add(this.checkBox17);
            this.groupBox2.Controls.Add(this.cmbStomachNormal1OSV);
            this.groupBox2.Controls.Add(this.checkBox16);
            this.groupBox2.Controls.Add(cmbStomachNormal2OSV);
            this.groupBox2.Controls.Add(this.checkBox15);
            this.groupBox2.Controls.Add(this.checkBox13);
            this.groupBox2.Location = new System.Drawing.Point(161, 247);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(592, 129);
            this.groupBox2.TabIndex = 98;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Живот";
            // 
            // txtStomachNormalOSV
            // 
            this.txtStomachNormalOSV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStomachNormalOSV.Location = new System.Drawing.Point(246, 14);
            this.txtStomachNormalOSV.Name = "txtStomachNormalOSV";
            this.txtStomachNormalOSV.Size = new System.Drawing.Size(338, 20);
            this.txtStomachNormalOSV.TabIndex = 72;
            this.txtStomachNormalOSV.Tag = "observat.txtStomachNormal";
            // 
            // cmbStomachNormalOSV
            // 
            this.cmbStomachNormalOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStomachNormalOSV.FormattingEnabled = true;
            this.cmbStomachNormalOSV.Location = new System.Drawing.Point(5, 14);
            this.cmbStomachNormalOSV.Name = "cmbStomachNormalOSV";
            this.cmbStomachNormalOSV.Size = new System.Drawing.Size(231, 21);
            this.cmbStomachNormalOSV.TabIndex = 71;
            this.cmbStomachNormalOSV.Tag = "observat.lstStomachNormal";
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(6, 84);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(207, 17);
            this.checkBox14.TabIndex = 57;
            this.checkBox14.Tag = "observat.boolStomachSickly2";
            this.checkBox14.Text = "болезненный в правом подреберье";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(6, 104);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(221, 17);
            this.checkBox17.TabIndex = 60;
            this.checkBox17.Tag = "observat.boolStomachSickly3";
            this.checkBox17.Text = "болезненный в панкреатической зоне";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // cmbStomachNormal1OSV
            // 
            this.cmbStomachNormal1OSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStomachNormal1OSV.FormattingEnabled = true;
            this.cmbStomachNormal1OSV.Location = new System.Drawing.Point(5, 37);
            this.cmbStomachNormal1OSV.Name = "cmbStomachNormal1OSV";
            this.cmbStomachNormal1OSV.Size = new System.Drawing.Size(233, 21);
            this.cmbStomachNormal1OSV.TabIndex = 37;
            this.cmbStomachNormal1OSV.Tag = "observat.lstStomachNormal1";
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(245, 84);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(240, 17);
            this.checkBox16.TabIndex = 59;
            this.checkBox16.Tag = "observat.boolStomachSickly5";
            this.checkBox16.Text = "болезненный в пилородуоденальной зоне";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(245, 64);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(245, 17);
            this.checkBox15.TabIndex = 58;
            this.checkBox15.Tag = "observat.boolStomachSickly4";
            this.checkBox15.Text = "болезненный в проекции желчного пузыря";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(6, 64);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(232, 17);
            this.checkBox13.TabIndex = 56;
            this.checkBox13.Tag = "observat.boolStomachSickly1";
            this.checkBox13.Text = "болезненный в эпигастральной области";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(161, 125);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(40, 13);
            this.label39.TabIndex = 97;
            this.label39.Text = "Хрипы";
            // 
            // cmbCrepitationOSV
            // 
            this.cmbCrepitationOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCrepitationOSV.FormattingEnabled = true;
            this.cmbCrepitationOSV.Location = new System.Drawing.Point(255, 122);
            this.cmbCrepitationOSV.Name = "cmbCrepitationOSV";
            this.cmbCrepitationOSV.Size = new System.Drawing.Size(160, 21);
            this.cmbCrepitationOSV.TabIndex = 96;
            this.cmbCrepitationOSV.Tag = "observat.lstCrepitation";
            // 
            // chkComplaint
            // 
            this.chkComplaint.AutoSize = true;
            this.chkComplaint.Location = new System.Drawing.Point(161, 44);
            this.chkComplaint.Name = "chkComplaint";
            this.chkComplaint.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkComplaint.Size = new System.Drawing.Size(81, 17);
            this.chkComplaint.TabIndex = 95;
            this.chkComplaint.Tag = "observat.boolComplaint";
            this.chkComplaint.Text = "Жалоб нет";
            this.chkComplaint.UseVisualStyleBackColor = true;
            // 
            // cmbStateOSV
            // 
            this.cmbStateOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStateOSV.FormattingEnabled = true;
            this.cmbStateOSV.Location = new System.Drawing.Point(256, 70);
            this.cmbStateOSV.Name = "cmbStateOSV";
            this.cmbStateOSV.Size = new System.Drawing.Size(159, 21);
            this.cmbStateOSV.TabIndex = 94;
            this.cmbStateOSV.Tag = "observat.lstState";
            // 
            // textBox60
            // 
            this.textBox60.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox60.Location = new System.Drawing.Point(421, 70);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(179, 20);
            this.textBox60.TabIndex = 93;
            this.textBox60.Tag = "observat.txtState";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(158, 73);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(98, 13);
            this.label69.TabIndex = 92;
            this.label69.Text = "Общее состояние";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(163, 385);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(66, 13);
            this.label68.TabIndex = 91;
            this.label68.Text = "Миндалины";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(164, 231);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(35, 13);
            this.label66.TabIndex = 89;
            this.label66.Text = "Язык";
            // 
            // cmbTongueOSV
            // 
            this.cmbTongueOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTongueOSV.FormattingEnabled = true;
            this.cmbTongueOSV.Location = new System.Drawing.Point(205, 226);
            this.cmbTongueOSV.Name = "cmbTongueOSV";
            this.cmbTongueOSV.Size = new System.Drawing.Size(231, 21);
            this.cmbTongueOSV.TabIndex = 88;
            this.cmbTongueOSV.Tag = "observat.lstTongue";
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(572, 149);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(94, 20);
            this.textBox59.TabIndex = 87;
            this.textBox59.Tag = "observat.txtToneHeart2";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(547, 152);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(23, 13);
            this.label64.TabIndex = 86;
            this.label64.Text = "АД";
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(455, 149);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(89, 20);
            this.textBox58.TabIndex = 85;
            this.textBox58.Tag = "observat.txtToneHeart1";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(418, 152);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(29, 13);
            this.label63.TabIndex = 84;
            this.label63.Text = "ЧСС";
            // 
            // cmbToneHeartOSV
            // 
            this.cmbToneHeartOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToneHeartOSV.FormattingEnabled = true;
            this.cmbToneHeartOSV.Location = new System.Drawing.Point(255, 149);
            this.cmbToneHeartOSV.Name = "cmbToneHeartOSV";
            this.cmbToneHeartOSV.Size = new System.Drawing.Size(160, 21);
            this.cmbToneHeartOSV.TabIndex = 83;
            this.cmbToneHeartOSV.Tag = "observat.lstToneHeart";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(159, 152);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(67, 13);
            this.label62.TabIndex = 82;
            this.label62.Text = "Тоны серца";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(418, 99);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(24, 13);
            this.label61.TabIndex = 81;
            this.label61.Text = "ЧД";
            // 
            // cmbBreathingOSV
            // 
            this.cmbBreathingOSV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBreathingOSV.FormattingEnabled = true;
            this.cmbBreathingOSV.Location = new System.Drawing.Point(256, 97);
            this.cmbBreathingOSV.Name = "cmbBreathingOSV";
            this.cmbBreathingOSV.Size = new System.Drawing.Size(159, 21);
            this.cmbBreathingOSV.TabIndex = 80;
            this.cmbBreathingOSV.Tag = "observat.lstBreathing";
            // 
            // textBox57
            // 
            this.textBox57.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox57.Location = new System.Drawing.Point(448, 96);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(0, 20);
            this.textBox57.TabIndex = 79;
            this.textBox57.Tag = "observat.txtBreathing";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(159, 100);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(97, 13);
            this.label60.TabIndex = 78;
            this.label60.Text = "В легких дыхание";
            // 
            // txtComplaint
            // 
            this.txtComplaint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComplaint.Location = new System.Drawing.Point(248, 44);
            this.txtComplaint.Name = "txtComplaint";
            this.txtComplaint.Size = new System.Drawing.Size(352, 20);
            this.txtComplaint.TabIndex = 77;
            this.txtComplaint.Tag = "observat.txtComplaint";
            // 
            // lbObserv
            // 
            this.lbObserv.FormattingEnabled = true;
            this.lbObserv.Items.AddRange(new object[] {
            "23.10.2008",
            "12.11.2008"});
            this.lbObserv.Location = new System.Drawing.Point(6, 56);
            this.lbObserv.Name = "lbObserv";
            this.lbObserv.Size = new System.Drawing.Size(139, 368);
            this.lbObserv.TabIndex = 2;
            this.lbObserv.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(8, 43);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(71, 13);
            this.label72.TabIndex = 47;
            this.label72.Text = "Наблюдения";
            // 
            // tabPageDerma
            // 
            this.tabPageDerma.Controls.Add(this.lbObservDerma);
            this.tabPageDerma.Controls.Add(this.txtDiagnosis);
            this.tabPageDerma.Controls.Add(this.label97);
            this.tabPageDerma.Controls.Add(this.txtLymphNode);
            this.tabPageDerma.Controls.Add(this.cmbLymphNode);
            this.tabPageDerma.Controls.Add(this.label96);
            this.tabPageDerma.Controls.Add(this.groupBox4);
            this.tabPageDerma.Controls.Add(this.txtAnamnes);
            this.tabPageDerma.Controls.Add(this.txtComplaint2);
            this.tabPageDerma.Controls.Add(this.label90);
            this.tabPageDerma.Controls.Add(this.label89);
            this.tabPageDerma.Controls.Add(this.label85);
            this.tabPageDerma.Location = new System.Drawing.Point(4, 22);
            this.tabPageDerma.Name = "tabPageDerma";
            this.tabPageDerma.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPageDerma.Size = new System.Drawing.Size(830, 451);
            this.tabPageDerma.TabIndex = 1;
            this.tabPageDerma.Text = "Дерматовенеролог";
            this.tabPageDerma.UseVisualStyleBackColor = true;
            // 
            // lbObservDerma
            // 
            this.lbObservDerma.FormattingEnabled = true;
            this.lbObservDerma.Items.AddRange(new object[] {
            "23.10.2008",
            "12.11.2008"});
            this.lbObservDerma.Location = new System.Drawing.Point(6, 21);
            this.lbObservDerma.Name = "lbObservDerma";
            this.lbObservDerma.Size = new System.Drawing.Size(139, 368);
            this.lbObservDerma.TabIndex = 116;
            this.lbObservDerma.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // txtDiagnosis
            // 
            this.txtDiagnosis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiagnosis.Location = new System.Drawing.Point(219, 314);
            this.txtDiagnosis.Multiline = true;
            this.txtDiagnosis.Name = "txtDiagnosis";
            this.txtDiagnosis.Size = new System.Drawing.Size(545, 75);
            this.txtDiagnosis.TabIndex = 114;
            this.txtDiagnosis.Tag = "observatDerma.txtDiagnosis";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(158, 314);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(51, 13);
            this.label97.TabIndex = 113;
            this.label97.Text = "Диагноз";
            // 
            // txtLymphNode
            // 
            this.txtLymphNode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLymphNode.Location = new System.Drawing.Point(520, 287);
            this.txtLymphNode.Name = "txtLymphNode";
            this.txtLymphNode.Size = new System.Drawing.Size(244, 20);
            this.txtLymphNode.TabIndex = 112;
            this.txtLymphNode.Tag = "observatDerma.txtLymphNode";
            // 
            // cmbLymphNode
            // 
            this.cmbLymphNode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLymphNode.FormattingEnabled = true;
            this.cmbLymphNode.Location = new System.Drawing.Point(328, 286);
            this.cmbLymphNode.Name = "cmbLymphNode";
            this.cmbLymphNode.Size = new System.Drawing.Size(186, 21);
            this.cmbLymphNode.TabIndex = 107;
            this.cmbLymphNode.Tag = "observatDerma.lstLymphNode";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(158, 290);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(156, 13);
            this.label96.TabIndex = 100;
            this.label96.Text = "Перефирические лимфоузлы";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label98);
            this.groupBox4.Controls.Add(this.txtRecomendations);
            this.groupBox4.Controls.Add(this.txtProcessLocalized);
            this.groupBox4.Controls.Add(this.txtHair);
            this.groupBox4.Controls.Add(this.txtNail);
            this.groupBox4.Controls.Add(this.txtSkin);
            this.groupBox4.Controls.Add(this.cmbProcessIs);
            this.groupBox4.Controls.Add(this.cmbHair);
            this.groupBox4.Controls.Add(this.cmbNail);
            this.groupBox4.Controls.Add(this.label95);
            this.groupBox4.Controls.Add(this.label94);
            this.groupBox4.Controls.Add(this.label93);
            this.groupBox4.Controls.Add(this.label92);
            this.groupBox4.Controls.Add(this.label91);
            this.groupBox4.Controls.Add(this.cmbSkin);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(151, 63);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(619, 217);
            this.groupBox4.TabIndex = 99;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Локальный статус";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label98.Location = new System.Drawing.Point(32, 157);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(82, 13);
            this.label98.TabIndex = 118;
            this.label98.Text = "Рекомендации";
            // 
            // txtRecomendations
            // 
            this.txtRecomendations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRecomendations.Location = new System.Drawing.Point(176, 154);
            this.txtRecomendations.Multiline = true;
            this.txtRecomendations.Name = "txtRecomendations";
            this.txtRecomendations.Size = new System.Drawing.Size(436, 57);
            this.txtRecomendations.TabIndex = 117;
            this.txtRecomendations.Tag = "observatDerma.txtRecomendations";
            // 
            // txtProcessLocalized
            // 
            this.txtProcessLocalized.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProcessLocalized.Location = new System.Drawing.Point(176, 128);
            this.txtProcessLocalized.Name = "txtProcessLocalized";
            this.txtProcessLocalized.Size = new System.Drawing.Size(436, 20);
            this.txtProcessLocalized.TabIndex = 111;
            this.txtProcessLocalized.Tag = "observatDerma.txtProcessLocalized";
            // 
            // txtHair
            // 
            this.txtHair.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHair.Location = new System.Drawing.Point(369, 72);
            this.txtHair.Name = "txtHair";
            this.txtHair.Size = new System.Drawing.Size(244, 20);
            this.txtHair.TabIndex = 110;
            this.txtHair.Tag = "observatDerma.txtHair";
            // 
            // txtNail
            // 
            this.txtNail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNail.Location = new System.Drawing.Point(369, 44);
            this.txtNail.Name = "txtNail";
            this.txtNail.Size = new System.Drawing.Size(244, 20);
            this.txtNail.TabIndex = 109;
            this.txtNail.Tag = "observatDerma.txtNail";
            // 
            // txtSkin
            // 
            this.txtSkin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSkin.Location = new System.Drawing.Point(369, 16);
            this.txtSkin.Name = "txtSkin";
            this.txtSkin.Size = new System.Drawing.Size(244, 20);
            this.txtSkin.TabIndex = 108;
            this.txtSkin.Tag = "observatDerma.txtSkin";
            // 
            // cmbProcessIs
            // 
            this.cmbProcessIs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcessIs.FormattingEnabled = true;
            this.cmbProcessIs.Location = new System.Drawing.Point(177, 99);
            this.cmbProcessIs.Name = "cmbProcessIs";
            this.cmbProcessIs.Size = new System.Drawing.Size(186, 21);
            this.cmbProcessIs.TabIndex = 105;
            this.cmbProcessIs.Tag = "observatDerma.lstProcessIs";
            // 
            // cmbHair
            // 
            this.cmbHair.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHair.FormattingEnabled = true;
            this.cmbHair.Location = new System.Drawing.Point(177, 72);
            this.cmbHair.Name = "cmbHair";
            this.cmbHair.Size = new System.Drawing.Size(186, 21);
            this.cmbHair.TabIndex = 104;
            this.cmbHair.Tag = "observatDerma.lstHair";
            // 
            // cmbNail
            // 
            this.cmbNail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNail.FormattingEnabled = true;
            this.cmbNail.Location = new System.Drawing.Point(177, 44);
            this.cmbNail.Name = "cmbNail";
            this.cmbNail.Size = new System.Drawing.Size(186, 21);
            this.cmbNail.TabIndex = 103;
            this.cmbNail.Tag = "observatDerma.lstNail";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.Location = new System.Drawing.Point(32, 130);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(139, 13);
            this.label95.TabIndex = 102;
            this.label95.Text = "Процесс локализуется на";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label94.Location = new System.Drawing.Point(32, 102);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(86, 13);
            this.label94.TabIndex = 101;
            this.label94.Text = "Процесс носит ";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label93.Location = new System.Drawing.Point(32, 75);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(46, 13);
            this.label93.TabIndex = 100;
            this.label93.Text = "Волосы";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(32, 47);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(113, 13);
            this.label92.TabIndex = 99;
            this.label92.Text = "Ногтевые пластинки";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.Location = new System.Drawing.Point(32, 19);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(95, 13);
            this.label91.TabIndex = 95;
            this.label91.Text = "Кожные покровы";
            // 
            // cmbSkin
            // 
            this.cmbSkin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSkin.FormattingEnabled = true;
            this.cmbSkin.Location = new System.Drawing.Point(177, 16);
            this.cmbSkin.Name = "cmbSkin";
            this.cmbSkin.Size = new System.Drawing.Size(186, 21);
            this.cmbSkin.TabIndex = 98;
            this.cmbSkin.Tag = "observatDerma.lstSkin";
            // 
            // txtAnamnes
            // 
            this.txtAnamnes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnamnes.Location = new System.Drawing.Point(304, 37);
            this.txtAnamnes.Name = "txtAnamnes";
            this.txtAnamnes.Size = new System.Drawing.Size(466, 20);
            this.txtAnamnes.TabIndex = 97;
            this.txtAnamnes.Tag = "observatDerma.txtAnamnes";
            // 
            // txtComplaint2
            // 
            this.txtComplaint2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComplaint2.Location = new System.Drawing.Point(234, 15);
            this.txtComplaint2.Name = "txtComplaint2";
            this.txtComplaint2.Size = new System.Drawing.Size(536, 20);
            this.txtComplaint2.TabIndex = 96;
            this.txtComplaint2.Tag = "observatDerma.txtComplaint";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(157, 37);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(121, 13);
            this.label90.TabIndex = 94;
            this.label90.Text = "Анамнез заболевания";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(157, 18);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(50, 13);
            this.label89.TabIndex = 93;
            this.label89.Text = "Жалобы";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(8, 5);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(71, 13);
            this.label85.TabIndex = 49;
            this.label85.Text = "Наблюдения";
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.listBox3);
            this.tabPage17.Controls.Add(this.label88);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage17.Size = new System.Drawing.Size(830, 451);
            this.tabPage17.TabIndex = 2;
            this.tabPage17.Text = "Гинеколог";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Items.AddRange(new object[] {
            "23.10.2008",
            "12.11.2008"});
            this.listBox3.Location = new System.Drawing.Point(6, 18);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(113, 368);
            this.listBox3.TabIndex = 48;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(8, 5);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(71, 13);
            this.label88.TabIndex = 49;
            this.label88.Text = "Наблюдения";
            // 
            // toolStrip2
            // 
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAddObservation,
            this.tsbDeleteObservation,
            this.tsbMakeTemplate});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(790, 27);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAddObservation
            // 
            this.tsbAddObservation.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddObservation.Image")));
            this.tsbAddObservation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddObservation.Name = "tsbAddObservation";
            this.tsbAddObservation.Size = new System.Drawing.Size(155, 24);
            this.tsbAddObservation.Text = "Добавить наблюдение";
            this.tsbAddObservation.Click += new System.EventHandler(this.tsbAddObservation_Click);
            // 
            // tsbDeleteObservation
            // 
            this.tsbDeleteObservation.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeleteObservation.Image")));
            this.tsbDeleteObservation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeleteObservation.Name = "tsbDeleteObservation";
            this.tsbDeleteObservation.Size = new System.Drawing.Size(75, 24);
            this.tsbDeleteObservation.Text = "Удалить";
            this.tsbDeleteObservation.Click += new System.EventHandler(this.tsbDeleteObservation_Click);
            // 
            // tsbMakeTemplate
            // 
            this.tsbMakeTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbMakeTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbMakeTemplate.Image")));
            this.tsbMakeTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMakeTemplate.Name = "tsbMakeTemplate";
            this.tsbMakeTemplate.Size = new System.Drawing.Size(102, 24);
            this.tsbMakeTemplate.Text = "Создать шаблон";
            this.tsbMakeTemplate.Click += new System.EventHandler(this.tsbMakeTemplate_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOk,
            this.сохранитьToolStripMenuItem,
            this.toolStripMenuItem2,
            this.амбулаторнаяКартаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1020, 28);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnOk
            // 
            this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(154, 24);
            this.btnOk.Text = "Сохранить и закрыть";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("сохранитьToolStripMenuItem.Image")));
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(97, 24);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click_1);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книжкаНазначенияToolStripMenuItem,
            this.историяБолезниToolStripMenuItem,
            this.обратныйТалонToolStripMenuItem});
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(78, 24);
            this.toolStripMenuItem2.Text = "Печать";
            // 
            // книжкаНазначенияToolStripMenuItem
            // 
            this.книжкаНазначенияToolStripMenuItem.Name = "книжкаНазначенияToolStripMenuItem";
            this.книжкаНазначенияToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.книжкаНазначенияToolStripMenuItem.Text = "Книжка назначения";
            this.книжкаНазначенияToolStripMenuItem.Click += new System.EventHandler(this.книжкаНазначенияToolStripMenuItem_Click);
            // 
            // историяБолезниToolStripMenuItem
            // 
            this.историяБолезниToolStripMenuItem.Name = "историяБолезниToolStripMenuItem";
            this.историяБолезниToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.историяБолезниToolStripMenuItem.Text = "История болезни";
            this.историяБолезниToolStripMenuItem.Click += new System.EventHandler(this.историяБолезниToolStripMenuItem_Click);
            // 
            // обратныйТалонToolStripMenuItem
            // 
            this.обратныйТалонToolStripMenuItem.Name = "обратныйТалонToolStripMenuItem";
            this.обратныйТалонToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.обратныйТалонToolStripMenuItem.Text = "Обратный талон";
            this.обратныйТалонToolStripMenuItem.Click += new System.EventHandler(this.обратныйТалонToolStripMenuItem_Click);
            // 
            // амбулаторнаяКартаToolStripMenuItem
            // 
            this.амбулаторнаяКартаToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("амбулаторнаяКартаToolStripMenuItem.Image")));
            this.амбулаторнаяКартаToolStripMenuItem.Name = "амбулаторнаяКартаToolStripMenuItem";
            this.амбулаторнаяКартаToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.амбулаторнаяКартаToolStripMenuItem.Text = "Амбулаторная карта";
            this.амбулаторнаяКартаToolStripMenuItem.Click += new System.EventHandler(this.амбулаторнаяКартаToolStripMenuItem_Click);
            // 
            // directionTypeBindingSource1
            // 
            this.directionTypeBindingSource1.DataMember = "DirectionType";
            this.directionTypeBindingSource1.DataSource = this.medsystDataSet;
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // directionTypeTableAdapter1
            // 
            this.directionTypeTableAdapter1.ClearBeforeFill = true;
            // 
            // frmIllnessHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 602);
            this.Controls.Add(this.toolStripContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmIllnessHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "История болезни";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIllnessHistory_FormClosing);
            this.Load += new System.EventHandler(this.IllnessHistory_Load);
            tabPage14.ResumeLayout(false);
            tabPage14.PerformLayout();
            tabPage0.ResumeLayout(false);
            tabPage0.PerformLayout();
            this.groupButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBOut)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMKBAttend)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nmYear)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAbortion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualDaysFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMenstrualYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPregnancy)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridService)).EndInit();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabControlObservations.ResumeLayout(false);
            this.tabPageTherapist.ResumeLayout(false);
            this.tabPageTherapist.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPageDerma.ResumeLayout(false);
            this.tabPageDerma.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.directionTypeBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        //*** Закладка Общая
        //Диагноз с места отбора
        //Диагноз санатория при поступлении основной
        //Диагноз санатория при поступлении сопутствующие заболевания

        //*** Закладка Жалобы больного
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage1;
        //Закладка Итория заболевания
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage2;
        //Закладка Антрометрические данные
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage4;
        //Закладка Общий анамнез
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage3;
        //Закладка Объективные исследования
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage5;
        //Закладка Сердечнососудистые заболевания
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage6;
        //Закладка Органы дыхания
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage7;
        //Закладка Органы пищеварения
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage8;
        //Закладка Мочеполовая система
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage9;
        //Закладка Органы опоры и движения
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage10;
        //Закладка Нервная система
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage11;
        //Закладка Назначения
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage12;
        //Закладка Течение болезни
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage13;
        //Закладка Заключительный прием

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label21;
        [sec(new Groups[] { Groups.Admin, Groups.Registrar }, false)]
        public System.Windows.Forms.ComboBox cmbTypeIH;
        private System.Windows.Forms.Label label20;
        
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDisorder;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTrauma;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInherit;
        private System.Windows.Forms.ComboBox cmbWorkForIH;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtGenecologyHarmfulness;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.ComboBox cmbMaelsFormIH;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbMealsIH;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmbHousingConditionIH;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox chkPregnancy;
        private System.Windows.Forms.CheckBox chkMenstrual;
        private System.Windows.Forms.CheckBox chkBirth;
        private System.Windows.Forms.ComboBox cmbMenstrualFormIH;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chkAbortion;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cmbSkinIH;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cmbMeals2IH;
        private System.Windows.Forms.ComboBox cmbFigureIH;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtFigure;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cmbMucuosIH;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbMuscularIH;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox txtMeals;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cmbPulse1IH;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmbHeartToneIH;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox cmbHeartNoiseIH;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cmbArteryPulseIH;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox cmbTongueIH;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox cmbMendalinsIH;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cmbToothsIH;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ComboBox cmbLiver1IH;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox cmbSpleenIH;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox cmbUrinationIH;
        private System.Windows.Forms.TextBox txtSupportMotion;
        private System.Windows.Forms.TextBox txtNervose;
        private System.Windows.Forms.ListBox lbObserv;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAddObservation;
        private System.Windows.Forms.ToolStripButton tsbDeleteObservation;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.DataGridView gridService;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.ComboBox cmbUnhealthyIH;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ComboBox cmbFamilyStatusIH;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbIntensificationIH;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox cmbSensationIH;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox cmbResultIH;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox txtOtherDinamic;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnOk;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem историяБолезниToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem книжкаНазначенияToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkInherit;
        private System.Windows.Forms.CheckBox chkTrauma;
        private System.Windows.Forms.CheckBox chkDisorder;
        private System.Windows.Forms.CheckBox chkGenecologyHarmfulness;
        private System.Windows.Forms.ComboBox cmbGlandIH;
        private System.Windows.Forms.CheckBox chkSupportMotion;
        private System.Windows.Forms.CheckBox chkNoPeculiarity;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.ComboBox cmbStomachNormal1IH;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.ComboBox cmbStomachNormal2IH;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cmbHeartTone2IH;
        private System.Windows.Forms.ComboBox cmbHeartTone1IH;
        private System.Windows.Forms.ComboBox cmbPulse2IH;
        private System.Windows.Forms.ComboBox cmbHeartNoise2IH;
        private System.Windows.Forms.ComboBox cmbHeartNoise1IH;
        private System.Windows.Forms.ComboBox cmbLiver4IH;
        private System.Windows.Forms.ComboBox cmbLiver3IH;
        private System.Windows.Forms.ComboBox cmbLiver2IH;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox26;
        private ExButton.NET.ExButton btnAddMedicine;
        private ExButton.NET.ExButton btnDelMedicine;
        private ExButton.NET.ExButton btnAddService;
        private ExButton.NET.ExButton btnDelService;
        private System.Windows.Forms.TextBox txtSeasonClose;
        private System.Windows.Forms.TextBox txtSeasonOpen;
        [sec(new Groups[] { Groups.Admin, Groups.Registrar }, false)]
        public System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox lblPrice;
        private System.Windows.Forms.TextBox lblMedPrice;
        private System.Windows.Forms.TextBox lblServicePrice;
        private System.Windows.Forms.Label label83;
        [sec(new Groups[] { Groups.Admin, Groups.Registrar }, false)]
        public System.Windows.Forms.TextBox textTypeNbr;
        [sec(new Groups[] { Groups.Admin, Groups.Registrar }, false)]
        public System.Windows.Forms.NumericUpDown nmYear;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBox27;
        [sec(new Groups[] { Groups.Admin, Groups.Manager }, false)]
        public System.Windows.Forms.Label lblDoctor;
        [sec(new Groups[] { Groups.Admin, Groups.Manager}, false)]
        public System.Windows.Forms.ComboBox cmbDoctor;
        [sec(new Groups[] { Groups.Admin, Groups.Registrar }, false)]
        public Medsyst.Class.Core.RegularTextBox txtIHNbr;
        private System.Windows.Forms.Label lblFIO;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colFulfil;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCharacter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCountMed;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountDivide;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRecept;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.ComboBox cmbStool2;
        private System.Windows.Forms.ComboBox cmbStool1;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ComboBox cmbStomachNormalIH;
        private System.Windows.Forms.TextBox txtStomachNormal;
        private System.Windows.Forms.ToolStripMenuItem амбулаторнаяКартаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkOtherDinamic;
        private System.Windows.Forms.NumericUpDown numPregnancy;
        private System.Windows.Forms.NumericUpDown numMenstrualYear;
        private System.Windows.Forms.NumericUpDown numMenstrualDaysFrom;
        private System.Windows.Forms.NumericUpDown numMenstrualDays;
        private System.Windows.Forms.NumericUpDown numBirth;
        private System.Windows.Forms.NumericUpDown numAbortion;
        private ExButton.NET.ExButton exButton3;
        private ExButton.NET.ExButton btnAddMKBOutMain;
        private ExButton.NET.ExButton btnDeleteMKBOut;
        private System.Windows.Forms.DataGridView dgMKBOut;
        private ExButton.NET.ExButton exButton1;
        private ExButton.NET.ExButton exButton2;
        private System.Windows.Forms.DataGridView dgMKBMain;
        private ExButton.NET.ExButton exButton4;
        private System.Windows.Forms.DataGridView dgMKBAttend;
        private ExButton.NET.ExButton exButton5;
        private ExButton.NET.ExButton exButton6;
        private ExButton.NET.ExButton exButton7;
        private ExButton.NET.ExButton exButton8;
        private ExButton.NET.ExButton exButton9;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor }, false)]
        public System.Windows.Forms.GroupBox groupButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ToolStripMenuItem обратныйТалонToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbMakeTemplate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMKBName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private MedsystDataSet medsystDataSet;
        private System.Windows.Forms.BindingSource directionTypeBindingSource1;
        private Medsyst.DataSet.MedsystDataSetTableAdapters.DirectionTypeTableAdapter directionTypeTableAdapter1;
        private System.Windows.Forms.TabControl tabControlObservations;
        private System.Windows.Forms.TabPage tabPageTherapist;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox cmbStoolOSV;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox cmbUrinationOSV;
        private System.Windows.Forms.ComboBox cmbNoiseHeart3OSV;
        private System.Windows.Forms.ComboBox cmbNoiseHeart2OSV;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox cmbNoiseHeart1OSV;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtStomachNormalOSV;
        private System.Windows.Forms.ComboBox cmbStomachNormalOSV;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.ComboBox cmbStomachNormal1OSV;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbCrepitationOSV;
        private System.Windows.Forms.CheckBox chkComplaint;
        private System.Windows.Forms.ComboBox cmbStateOSV;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox cmbTongueOSV;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox cmbToneHeartOSV;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox cmbBreathingOSV;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtComplaint;
        private System.Windows.Forms.TabPage tabPageDerma;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cmbProcessIs;
        private System.Windows.Forms.ComboBox cmbHair;
        private System.Windows.Forms.ComboBox cmbNail;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.ComboBox cmbSkin;
        private System.Windows.Forms.TextBox txtAnamnes;
        private System.Windows.Forms.TextBox txtComplaint2;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.ComboBox cmbLymphNode;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox txtHair;
        private System.Windows.Forms.TextBox txtNail;
        private System.Windows.Forms.TextBox txtSkin;
        private System.Windows.Forms.TextBox txtLymphNode;
        private System.Windows.Forms.TextBox txtProcessLocalized;
        private System.Windows.Forms.TextBox txtDiagnosis;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.ListBox lbObservDerma;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox txtRecomendations;

    }
}