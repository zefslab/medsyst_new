﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Data.Common;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmContact : Form
    {
        PersonContact contact = new PersonContact(true);

        public frmContact()
        {
            InitializeComponent();
        }

        private void toolbtnReady_Click(object sender, EventArgs e)
        {
            toolbtnApply_Click(this, e);
            if (toolbtnReady.Tag == null) return;
            if ((int)toolbtnReady.Tag != 1) return;
            Tag = new Command(CommandModes.Return, contact.PersonContactID);
            cmd = Tag as Command;
            DialogResult = DialogResult.OK;
            if (IsMdiChild) Close();
        }

        private void frmContact_Load(object sender, EventArgs e)
        {
            contact.FormBinding("contact", this);
            if (cmd.Mode != CommandModes.New)
            {
                contact.PersonID = cmd.Id;
                contact.Load();
                contact.WriteToForm();
            };
        }

        private void toolbtnApply_Click(object sender, EventArgs e)
        {
            if ((txtLastName.Text == "") || (txtName.Text == "") || (txtSurName.Text == ""))
            {
                MessageBox.Show("Проверьте правильность заполнения обязательных полей", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            toolbtnReady.Tag = 1;
            contact.ReadFromForm();
            if (cmd.Mode == CommandModes.New)
            {
                contact.Insert();
                contact.PersonID = contact.Identity("Person");
            }
            else
            {
                contact.Update();
            }
        }  
    }
}
