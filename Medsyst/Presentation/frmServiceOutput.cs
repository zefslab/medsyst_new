﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Forms;
using Medsyst.Data.Entity;


namespace Medsyst
{
    public partial class frmServiceOutput : Form
    {
      //Расходные документы  
        ///<summary>Историй болезней</summary>
        IllnessHistoryShortList histories = new IllnessHistoryShortList();
        ///<summary>Список назначенных услуг</summary>
        ServiceOutputList srvs;
        /// <summary>Список сезонов </summary>
        Seasons seasons = new Seasons();
        /// <summary>Список типов расходных документов</summary>
        DocumentTypeList DTOlist = new DocumentTypeList();
        /// <summary> Тип расходного документа представленный в виде индексного списочного значения</summary>
        //int doctype = 4; //По умолчанию устанавливается тип расходного документа - Истории болезней

        public frmServiceOutput()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmServiceOutput_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "medsystDataSet.PersonPatientCategory". При необходимости она может быть перемещена или удалена.
            this.personPatientCategoryTableAdapter.Connection.ConnectionString = DB.CS;
            this.personPatientCategoryTableAdapter.Fill(this.medsystDataSet.PersonPatientCategory);
            gridSrvs.AutoGenerateColumns = false;
            gridOutDocuments.AutoGenerateColumns = false;
            //Сначала настраиваются фильтры всех расходных документов на значения по умолчанию 
            FilterIllnessHistory(); //Настраивается фильр истории волезней настройками по умолчанию
            //Затем запускается главный механизм отображения расходных документов
            //с учетом установок фильров
            Main();
        }
        
        /// <summary>
        /// Определяет какая закладка является текущей и сопоставляет с ней тип расходного документа
        /// Устанавливаются значения по умолчанию для фильтра
        /// Загружается соответсвующий список расходных документов
        /// Автор: Заплатин 18.10.2010
        /// </summary>
        private void Main()
        {
          //Анализируется какая закладка активна в настоящее время
            if (tabDocOutType.SelectedIndex == 0)//Если открыта закладка Истории болезни
            {
                LoadHistories();//Настраивается элемент вывода списка расходных документов
            }
            else
            {//Дополнить структуру др. вариантами выбора
                // А пока...
                gridOutDocuments.DataSource = null;// ... просто очищается элемент grid
            }
            
            LoadSrvs();//Загрузка услуг назначенных по выбранным расходным документам

        }

        /// <summary>
        /// Загружается список историй болезней с учетом значений фильтра по умолчанию
        /// </summary>
        void LoadHistories()
        {
            gridOutDocuments.Columns["colDocID"].HeaderText = "ID";
            gridOutDocuments.Columns["colDocID"].DataPropertyName = "DocumentID";
            if (p.trace == true) gridOutDocuments.Columns["colDocID"].Visible = true;

            gridOutDocuments.Columns["colNum"].HeaderText = "№";
            gridOutDocuments.Columns["colNum"].DataPropertyName = "Season";

            gridOutDocuments.Columns["colDesc"].HeaderText = "ФИО";
            gridOutDocuments.Columns["colDesc"].DataPropertyName = "FullName";

            gridOutDocuments.Columns["colDate"].Visible = false;//подумать и может быть совсем убрать эти неиспользуемые поля. ЗЕФ
            gridOutDocuments.Columns["colDoctor"].Visible = true;//востанавливает видимость поля после перехода из др. закладок расходных документов

            gridOutDocuments.Columns["colSum"].DataPropertyName = "SummMedicament";

            gridOutDocuments.DataSource = null;
            //Проанализировать значения фильтра и если они установлены верно то загрузить список расходных документов
            if (cmbSeason.SelectedValue == null || cmbCategoryPacient.SelectedValue == null)
            {//Не все фильтрующие элементы управления установлены
                MessageBox.Show("Один или более параметров фильтра не установлены. Заполните фильтр и повторите запрос на фильтрацию.",
                    "Ошибка в фильтре", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            histories.Load((int)cmbSeason.SelectedValue, (int)cmbCategoryPacient.SelectedValue);//Загрузка с учетом фильтра

            Season season = new Season { SeasonID = (int)cmbSeason.SelectedValue };
            season.Load();

            var test = histories.Select(h => new DocDto
            {
                DocumentID = h.DocumentID,
                Season = h.NbrIH + "/" + season.SeasonNbr,
                FullName = h.FullName,
                DoctorName = h.DoctorName,
                SummMedicament = h.SummMedicament
            }).ToList();
            gridOutDocuments.DataSource = test;
        }

        /// <summary>
        /// Настройка фильтра Истории болезни и установка стартовых значений элементов фильтра
        /// </summary>
        private void FilterIllnessHistory() 
        {
              //Считывание настроек системы
              Settings set = new Settings(true);
              set.Load();
              numYear.Value = new decimal(new int[] { set.CurrentYear, 0, 0, 0 });//Устанавливается текущий год в качестве стартового значения фильтра
              LoadSeasonsIH((int)numYear.Value);//Загружается список сезонов 
        }
        /// <summary>
        //// Загрузка списка сезонов для фильтра историй болезней
        /// </summary>
        /// <param name="year"></param>
        private void LoadSeasonsIH(int year)
        {
            seasons.Load(year);//Загрузка сезонов по установленному фильтру поля года
            cmbSeason.DataSource = null; //Обнуление источника данных
            cmbSeason.DataSource = seasons;//Назначение загруженных сезонов в качестве источника данных комбобоксу   

            Settings set = new Settings(true);
            set.Load();//Загружаются настройки системы
            if (year == set.CurrentYear)//если установленный год является текущим
            {
                cmbSeason.SelectedValue = set.CurrentSeason; //Устанавливается текущий сезон в качестве выделенного в списке.
            }
        } 


        /// <summary>
        /// Нажатие на кнопку "Установить фильтр"
        /// Автор: Заплатин Е.Ф. 04.10.2010
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            //Предварительно нужно протестировать все элементы фильтров, чтобы они были проинициированы
            // Так например не всегда выставляется сезон, если выбрать год, в котором не заведено ни одного сезона
            // В таких случаях следует выдавать пустые списки расходныъ документов
            // или выдавать сообщения о незаполненных элементах списков
            Main();
        }
        /// <summary>
        /// Изменение значения года влечет за собой изменение наполнения списка сезонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numYear_ValueChanged(object sender, EventArgs e)
        {
            //После изменения значения счетчика года необходимо обновить список сезонов
            LoadSeasonsIH((int)numYear.Value);
        }
        /// <summary>
        /// Переход по закладкам и вывод списка расходных документов в зависимости от выбранной закладки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabDocOutType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Main();//Главный механизм отображения расходных документов

        }

        /// <summary>
        /// Загрузка назначенных услуг в соответсвии с установленным фильтром и количеством выделенных документов
        /// Заплатин Е.Ф.
        /// 31.07.2011
        /// 12.07.2012, ЗЕФ. Оптимизирована процедура загрузки услуг и организована сортировка по алфавиту
        /// </summary>
        void LoadSrvs()
        {
            //Анализируется выделение строк и их количество в таблице документов
            if (gridOutDocuments.SelectedRows == null & gridOutDocuments.SelectedRows.Count <= 0) return;
            //выделена одна или более записей расходных документов для отображения списка услуг

            srvs = new ServiceOutputList();
            //Собираются в массив идентификаторы выделеных расходных документов для передачи в процедуру загрузки 
            int[] docs = new int[gridOutDocuments.SelectedRows.Count];
            int i = 0;
            foreach (DataGridViewRow row in gridOutDocuments.SelectedRows)
            {
                docs[i] = (int)row.Cells["colDocID"].Value;
                i++;
            }
            if (rbShowAll.Checked)  
                //Выбран фильтрующий критерий - "Загружать все  услуги" 
                srvs.Load(docs);
            else 
                //Указан какой либо другой из ограничивающих критериев?
                srvs.Load(docs, rbShowFulfil.Checked);
            
            gridSrvs.DataSource = null;
            gridSrvs.DataSource = srvs;
        }
        /// <summary>
        /// Отметка и снятие чекбокса услуги
        /// Автор: Заплатин Е.Ф.
        /// Дата: 05.12.2010
        /// </summary>
        private void gridSrvs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridSrvs.SelectedRows == null || gridSrvs.SelectedRows.Count <= 0) return;
            if (gridSrvs.Columns[e.ColumnIndex].Name != "colFulfil") return;//Если щелчек мыши был не по полю назначения
          //Отмечается услуга как выполненная
            ServiceOutput so = new ServiceOutput();//новый экзкмпляр
            so.ServiceOutputID = (int)gridSrvs.SelectedRows[0].Cells["colSrvOutID"].Value;
            so.Load();//Загрузка сведений об оказанной услуге
            so.Implement(); //!!! Применение оказанной услуги
            LoadSrvs();//Перезагрузка списка услуг
            gridSrvs.Refresh(); //Обновление списка услуг
        }

        private void показатьИдентификаторыДокументовToolStripMenuItem_Click(object sender, EventArgs e)
        {

            gridOutDocuments.Columns["colDocID"].Visible = toolStripShowId.Checked;

        }

        private void gridOutDocuments_SelectionChanged(object sender, EventArgs e)
        {
            LoadSrvs();
        }

        private void rbShowAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadSrvs();
        }

        private void rbShowFulfil_CheckedChanged(object sender, EventArgs e)
        {
            LoadSrvs();
        }

        private void rbShowNotFulfil_CheckedChanged(object sender, EventArgs e)
        {
            LoadSrvs();
        }
        private class DocDto
        {
            public int DocumentID { get; set; }
            public string Season { get; set; }
            public string FullName { get; set; }
            public string DoctorName { get; set; }
            public decimal SummMedicament { get; set; }
        }

       
    }
}
