﻿using System;
using System.Linq;
using System.Windows.Forms;
using Medsyst.Class.Core;
using Medsyst.Class;
using Medsyst.Dao.Base;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Entity;
using Medsyst.Data.Filter;
using Medsyst.Services.Bl;

namespace Medsyst
{
    public partial class frmProvider : Form
    {
        Provider provider = new Provider(true);
        //27.03.10
        //Чирков Е.О.
        DBOList<FirmForm> fforms = new DBOList<FirmForm>();
        Cities cities = new Cities();
        Address addr = new Address(true);
        BankDetails details = new BankDetails();
        DBOList<PersonContact> contacts = new DBOList<PersonContact>();
        ReceiptOrderCrudService _receiptOrderCrudService = new ReceiptOrderCrudService();

        public frmProvider()
        {
            InitializeComponent();
        }

        //27.03.10
        //Чирков Е.О.
        private void frmProvider_Load(object sender, EventArgs e)
        {
            fforms.Load();
            cbFirmForm.ValueMember = "FormID";
            cbFirmForm.DisplayMember = "FullName";
            cbFirmForm.DataSource = fforms;

            cities.Load();
            cbCity.ValueMember = "CityID";
            cbCity.DisplayMember = "CityN";
            cbCity.DataSource = cities;

            provider.FormBinding("provider", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                provider.FirmID = cmd.Id;
                provider.Load();
                provider.WriteToForm();
            }

            addr.FormBinding("addr", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                addr.AddressID = provider.AdressID;
                addr.Load();
                addr.WriteToForm();
            }

            details.FormBinding("details", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                details.BankDetailsID = provider.BankDetailsID;
                details.Load();
                details.WriteToForm();
            }

            FirmContacts cts = new FirmContacts();
            cts.dbWhere = "FirmID=" + provider.FirmID;
            cts.Load();
            foreach (FirmContact fc in cts)
            {
                PersonContact pc = new PersonContact(true);
                pc.PersonID = fc.PersonID;
                pc.Load();
                contacts.Add(pc);
            }

            dgvContacts.AutoGenerateColumns = false;
            dgvContacts.DataSource = contacts;

            if (cmd.Mode == CommandModes.Edit)
            {
                dgvOrders.AutoGenerateColumns = false;
                //При загрузке имеет значение лишь параметр с идентификатором провайдера, остальные указаны формально и не имеют никакого значения. ЗЕФ
                var _orders = _receiptOrderCrudService.Get(new ReceiptOrderFilter
                {
                    ProviderId = provider.FirmID
                }).Select(x => new
                {
                    AccountD = x.Date,
                    Nbr = x.Number,
                    PriceRO = x.Summ
                }).ToList();
                
                dgvOrders.DataSource = _orders;

            }

        }
        
        private void btnOrderAdd_Click(object sender, EventArgs e)
        {
            frmReceiptOrder childForm = new frmReceiptOrder();
            //childForm.MdiParent = this;
            childForm.Show();

        }

        private void btnOrderGo_Click(object sender, EventArgs e)
        {
            frmReceiptOrder childForm = new frmReceiptOrder();
            //childForm.MdiParent = this;
            childForm.Show();

        }

        private void btnContactAdd_Click(object sender, EventArgs e)
        {
            frmContact frm = new frmContact();
            frm.Tag = new Command(CommandModes.New);
            if (frm.ShowDialog(this) != DialogResult.OK) return;
            int id = frm.cmd.Id;
            PersonContact pc = new PersonContact(true);
            pc.PersonID = id;
            pc.Load();
            contacts.Add(pc);

            FirmContact fc = new FirmContact(true);
            fc.FirmID = provider.FirmID;
            fc.PersonID = pc.PersonID;
            fc.Insert();

            dgvContacts.DataSource = null;
            dgvContacts.AutoGenerateColumns = false;
            dgvContacts.DataSource = contacts;
        }

        private void btnContactEdit_Click(object sender, EventArgs e)
        {
            if (dgvContacts.SelectedRows.Count == 0) return;
            int i = dgvContacts.SelectedRows[0].Index;
            if (i >= contacts.Count) return;
            frmContact frm = new frmContact();
            frm.Tag = new Command(CommandModes.Edit,contacts[i].PersonID);
            frm.cmd = frm.Tag as Command;
            if (frm.ShowDialog(this) != DialogResult.OK) return;

            PersonContact pc = new PersonContact(true);
            pc.PersonID = frm.cmd.Id;
            pc.Load();
            contacts[i] = pc;

            dgvContacts.DataSource = null;
            dgvContacts.AutoGenerateColumns = false;
            dgvContacts.DataSource = contacts;
        }


        // Кнопка "Готово"
        private void toolbtnReady_Click(object sender, EventArgs e)
        {
            provider.ReadFromForm();
            addr.ReadFromForm();
            details.ReadFromForm();
            
            if (cmd.Mode == CommandModes.Edit)
            {
                addr.Update();
                details.Update();
                provider.Update();
            }
            else
            {
                addr.Insert();
                details.Insert();
                provider.AdressID = addr.Identity("Address");
                provider.BankDetailsID = details.Identity("BankDetails");
                provider.Insert();
                Tag = new Command(CommandModes.Return, provider.FirmID);
            }
            Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvContacts.SelectedRows.Count == 0) return;
            int i = dgvContacts.SelectedRows[0].Index;
            if (i >= contacts.Count) return;

            FirmContacts fcs = new FirmContacts();
            fcs.dbWhere = "(FirmID=" + provider.FirmID + ") AND (PersonID=" + contacts[i].PersonID + ")";
            fcs.Load();
            if (fcs.Count > 0)
            {
                foreach (FirmContact fc in fcs)
                {
                    fc.Delete();
                }
            }
            contacts[i].Delete();

            contacts.RemoveAt(i);
            dgvContacts.DataSource = null;
            dgvContacts.AutoGenerateColumns = false;
            dgvContacts.DataSource = contacts;
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {

        }
        /// <summary>
        /// Выбрана закладка в форме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage4)
            {
                
            }
        }
    }
}
