﻿using Medsyst.DataSet;

namespace Medsyst
{
    partial class frmDemandByMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDemandByMaterial));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.personPatientCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.btnRestoreMedecine = new ExButton.NET.ExButton();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.lblDefectiveSheet = new System.Windows.Forms.Label();
            this.rbtnNotBudget = new System.Windows.Forms.RadioButton();
            this.rbtnIsBudget = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnAdd_Medicine = new ExButton.NET.ExButton();
            this.btnDel_Medicine = new ExButton.NET.ExButton();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.cmbMembers = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSumm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNbr = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tbtnSign = new System.Windows.Forms.ToolStripButton();
            this.tbtnSave = new System.Windows.Forms.ToolStripButton();
            this.tbtnPrint = new System.Windows.Forms.ToolStripButton();
            this.tbtnDifect = new System.Windows.Forms.ToolStripButton();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.personPatientCategoryTableAdapter = new Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter();
            this.medsystDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colMedicamentOutputID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMeasures = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 126;
            this.label3.Text = "Затребовал (ФИО)";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label9);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmbCategory);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.btnRestoreMedecine);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.chkShowDeleted);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.lblDefectiveSheet);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.rbtnNotBudget);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.rbtnIsBudget);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label7);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dtpDate);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.btnAdd_Medicine);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.btnDel_Medicine);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label8);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmbSeason);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmbMembers);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label6);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtSumm);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label5);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtComment);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.gridMeds);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label4);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label3);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label2);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtNbr);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(687, 541);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(687, 566);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(325, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 13);
            this.label9.TabIndex = 153;
            this.label9.Text = "для категории пациентов";
            // 
            // cmbCategory
            // 
            this.cmbCategory.DataSource = this.personPatientCategoryBindingSource;
            this.cmbCategory.DisplayMember = "CategoryN";
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(467, 61);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(212, 21);
            this.cmbCategory.TabIndex = 152;
            this.cmbCategory.Tag = "demand.CategoryID";
            this.cmbCategory.ValueMember = "CategoryID";
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // personPatientCategoryBindingSource
            // 
            this.personPatientCategoryBindingSource.DataMember = "PersonPatientCategory";
            this.personPatientCategoryBindingSource.DataSource = this.medsystDataSet;
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnRestoreMedecine
            // 
            this.btnRestoreMedecine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestoreMedecine.Image = null;
            this.btnRestoreMedecine.Location = new System.Drawing.Point(446, 103);
            this.btnRestoreMedecine.Name = "btnRestoreMedecine";
            this.btnRestoreMedecine.Size = new System.Drawing.Size(81, 23);
            this.btnRestoreMedecine.TabIndex = 151;
            this.btnRestoreMedecine.Text = "Восстановить";
            this.btnRestoreMedecine.Visible = false;
            this.btnRestoreMedecine.Click += new System.EventHandler(this.btnRestoreMedecine_Click);
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(550, 84);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkShowDeleted.Size = new System.Drawing.Size(133, 17);
            this.chkShowDeleted.TabIndex = 150;
            this.chkShowDeleted.Text = "Показать удаленные";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // lblDefectiveSheet
            // 
            this.lblDefectiveSheet.AutoSize = true;
            this.lblDefectiveSheet.Location = new System.Drawing.Point(6, 84);
            this.lblDefectiveSheet.Name = "lblDefectiveSheet";
            this.lblDefectiveSheet.Size = new System.Drawing.Size(141, 13);
            this.lblDefectiveSheet.TabIndex = 149;
            this.lblDefectiveSheet.Text = "Дефектную ведомость от ";
            this.lblDefectiveSheet.Visible = false;
            // 
            // rbtnNotBudget
            // 
            this.rbtnNotBudget.AutoSize = true;
            this.rbtnNotBudget.Location = new System.Drawing.Point(163, 64);
            this.rbtnNotBudget.Name = "rbtnNotBudget";
            this.rbtnNotBudget.Size = new System.Drawing.Size(102, 17);
            this.rbtnNotBudget.TabIndex = 146;
            this.rbtnNotBudget.Tag = "";
            this.rbtnNotBudget.Text = "внебюджетные";
            this.rbtnNotBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnIsBudget
            // 
            this.rbtnIsBudget.AutoSize = true;
            this.rbtnIsBudget.Checked = true;
            this.rbtnIsBudget.Location = new System.Drawing.Point(73, 64);
            this.rbtnIsBudget.Name = "rbtnIsBudget";
            this.rbtnIsBudget.Size = new System.Drawing.Size(84, 17);
            this.rbtnIsBudget.TabIndex = 145;
            this.rbtnIsBudget.TabStop = true;
            this.rbtnIsBudget.Tag = "demand.IsBudget";
            this.rbtnIsBudget.Text = "бюджетные";
            this.rbtnIsBudget.UseVisualStyleBackColor = true;
            this.rbtnIsBudget.CheckedChanged += new System.EventHandler(this.rbtnIsBudget_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 144;
            this.label7.Text = "Средства";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(187, 5);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(145, 20);
            this.dtpDate.TabIndex = 143;
            this.dtpDate.Tag = "demand.RegisterD";
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // btnAdd_Medicine
            // 
            this.btnAdd_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd_Medicine.Image")));
            this.btnAdd_Medicine.Location = new System.Drawing.Point(524, 103);
            this.btnAdd_Medicine.Name = "btnAdd_Medicine";
            this.btnAdd_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnAdd_Medicine.TabIndex = 140;
            this.btnAdd_Medicine.Text = "Добавить";
            this.btnAdd_Medicine.Click += new System.EventHandler(this.btnAdd_Medicine_Click);
            // 
            // btnDel_Medicine
            // 
            this.btnDel_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnDel_Medicine.Image")));
            this.btnDel_Medicine.Location = new System.Drawing.Point(602, 103);
            this.btnDel_Medicine.Name = "btnDel_Medicine";
            this.btnDel_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnDel_Medicine.TabIndex = 139;
            this.btnDel_Medicine.Text = "Удалить";
            this.btnDel_Medicine.Click += new System.EventHandler(this.btnDel_Medicine_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(335, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 16);
            this.label8.TabIndex = 138;
            this.label8.Text = "Сезон";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "FullNbr";
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(384, 5);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(85, 21);
            this.cmbSeason.TabIndex = 137;
            this.cmbSeason.Tag = "demand.SeasonID";
            this.cmbSeason.ValueMember = "SeasonID";
            this.cmbSeason.SelectedIndexChanged += new System.EventHandler(this.cmbSeason_SelectedIndexChanged);
            // 
            // cmbMembers
            // 
            this.cmbMembers.DisplayMember = "Fullname";
            this.cmbMembers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMembers.FormattingEnabled = true;
            this.cmbMembers.Location = new System.Drawing.Point(118, 34);
            this.cmbMembers.Name = "cmbMembers";
            this.cmbMembers.Size = new System.Drawing.Size(351, 21);
            this.cmbMembers.TabIndex = 133;
            this.cmbMembers.Tag = "demand.EmployeeID";
            this.cmbMembers.ValueMember = "PersonID";
            this.cmbMembers.SelectedIndexChanged += new System.EventHandler(this.cmbMembers_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 426);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 13);
            this.label6.TabIndex = 131;
            this.label6.Text = "Примечание к требованию";
            // 
            // txtSumm
            // 
            this.txtSumm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSumm.Location = new System.Drawing.Point(558, 404);
            this.txtSumm.Name = "txtSumm";
            this.txtSumm.ReadOnly = true;
            this.txtSumm.Size = new System.Drawing.Size(125, 20);
            this.txtSumm.TabIndex = 130;
            this.txtSumm.Tag = "demand.SummMedicament";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(201, 407);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(351, 13);
            this.label5.TabIndex = 129;
            this.label5.Text = "Сумма стоимости медикаментов израсходованных по требованию:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 16);
            this.label1.TabIndex = 122;
            this.label1.Text = "№";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(7, 442);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(676, 96);
            this.txtComment.TabIndex = 132;
            this.txtComment.Tag = "demand.Comment";
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // gridMeds
            // 
            this.gridMeds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMedicamentOutputID,
            this.dataGridViewTextBoxColumn2,
            this.colMeasures,
            this.colCount,
            this.colDeleted});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridMeds.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridMeds.Location = new System.Drawing.Point(7, 129);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridMeds.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(676, 269);
            this.gridMeds.TabIndex = 127;
            this.gridMeds.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellEndEdit);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 128;
            this.label4.Text = "Лекарственные средства";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(162, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 16);
            this.label2.TabIndex = 124;
            this.label2.Text = "от";
            // 
            // txtNbr
            // 
            this.txtNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNbr.Location = new System.Drawing.Point(28, 3);
            this.txtNbr.Name = "txtNbr";
            this.txtNbr.Size = new System.Drawing.Size(118, 22);
            this.txtNbr.TabIndex = 123;
            this.txtNbr.Tag = "demand.Nbr";
            this.txtNbr.TextChanged += new System.EventHandler(this.txtNbr_TextChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnSign,
            this.tbtnSave,
            this.tbtnPrint,
            this.tbtnDifect,
            this.tbtnNew});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(459, 25);
            this.toolStrip1.TabIndex = 101;
            // 
            // tbtnSign
            // 
            this.tbtnSign.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSign.Image")));
            this.tbtnSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.tbtnSign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSign.Name = "tbtnSign";
            this.tbtnSign.Size = new System.Drawing.Size(86, 22);
            this.tbtnSign.Text = "Подписать";
            this.tbtnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // tbtnSave
            // 
            this.tbtnSave.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSave.Image")));
            this.tbtnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.tbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSave.Name = "tbtnSave";
            this.tbtnSave.Size = new System.Drawing.Size(85, 22);
            this.tbtnSave.Text = "Сохранить";
            this.tbtnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbtnPrint
            // 
            this.tbtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPrint.Image")));
            this.tbtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPrint.Name = "tbtnPrint";
            this.tbtnPrint.Size = new System.Drawing.Size(66, 22);
            this.tbtnPrint.Text = "Печать";
            this.tbtnPrint.Click += new System.EventHandler(this.tbtnPrint_Click);
            // 
            // tbtnDifect
            // 
            this.tbtnDifect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDifect.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDifect.Image")));
            this.tbtnDifect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDifect.Name = "tbtnDifect";
            this.tbtnDifect.Size = new System.Drawing.Size(81, 22);
            this.tbtnDifect.Text = "Дефектовать";
            this.tbtnDifect.Click += new System.EventHandler(this.tbtnDifect_Click);
            // 
            // tbtnNew
            // 
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(129, 22);
            this.tbtnNew.Text = "Новое требование";
            this.tbtnNew.ToolTipText = "Новое требование";
            // 
            // personPatientCategoryTableAdapter
            // 
            this.personPatientCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // medsystDataSetBindingSource
            // 
            this.medsystDataSetBindingSource.DataSource = this.medsystDataSet;
            this.medsystDataSetBindingSource.Position = 0;
            // 
            // colMedicamentOutputID
            // 
            this.colMedicamentOutputID.DataPropertyName = "Id";
            this.colMedicamentOutputID.HeaderText = "Код";
            this.colMedicamentOutputID.Name = "colMedicamentOutputID";
            this.colMedicamentOutputID.ReadOnly = true;
            this.colMedicamentOutputID.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.FillWeight = 300F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Наименование";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // colMeasures
            // 
            this.colMeasures.DataPropertyName = "MeasureID";
            this.colMeasures.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colMeasures.FillWeight = 50F;
            this.colMeasures.HeaderText = "Ед. изм.";
            this.colMeasures.Name = "colMeasures";
            this.colMeasures.ReadOnly = true;
            this.colMeasures.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colMeasures.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colMeasures.Width = 50;
            // 
            // colCount
            // 
            this.colCount.DataPropertyName = "CountOutput";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colCount.DefaultCellStyle = dataGridViewCellStyle2;
            this.colCount.HeaderText = "Кол-во выписано";
            this.colCount.Name = "colCount";
            this.colCount.Width = 65;
            // 
            // colDeleted
            // 
            this.colDeleted.DataPropertyName = "Deleted";
            this.colDeleted.HeaderText = "Удален";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDeleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colDeleted.Visible = false;
            this.colDeleted.Width = 50;
            // 
            // frmDemandByMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 566);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmDemandByMaterial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Требование на расходные материалы для лечебных кабинетов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDemandByMaterial_FormClosing);
            this.Load += new System.EventHandler(this.frmDemandByMaterial_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personPatientCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private ExButton.NET.ExButton btnAdd_Medicine;
        private ExButton.NET.ExButton btnDel_Medicine;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.ComboBox cmbMembers;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSumm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNbr;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tbtnPrint;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.DateTimePicker dtpDate;
        [sec(new Groups[] { Groups.Admin, Groups.Manager }, false)]
        public System.Windows.Forms.ToolStripButton tbtnSign;
        private System.Windows.Forms.ToolStripButton tbtnSave;
        private System.Windows.Forms.RadioButton rbtnNotBudget;
        private System.Windows.Forms.RadioButton rbtnIsBudget;
        private System.Windows.Forms.Label label7;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Stock }, false)]
        public System.Windows.Forms.ToolStripButton tbtnDifect;
        private System.Windows.Forms.Label lblDefectiveSheet;
        [sec(new Groups[] { Groups.Admin, Groups.Manager}, true)]
        public System.Windows.Forms.CheckBox chkShowDeleted;
        private ExButton.NET.ExButton btnRestoreMedecine;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbCategory;
        private MedsystDataSet medsystDataSet;
        private System.Windows.Forms.BindingSource personPatientCategoryBindingSource;
        private Medsyst.DataSet.MedsystDataSetTableAdapters.PersonPatientCategoryTableAdapter personPatientCategoryTableAdapter;
        private System.Windows.Forms.BindingSource medsystDataSetBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedicamentOutputID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn colMeasures;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCount;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
    }
}