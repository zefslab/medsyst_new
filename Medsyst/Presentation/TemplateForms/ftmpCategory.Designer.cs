﻿namespace Medsyst
{
    partial class ftmpCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ftmpCategory));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.treeCat = new System.Windows.Forms.TreeView();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.exButton2 = new ExButton.NET.ExButton();
            this.exButton1 = new ExButton.NET.ExButton();
            this.groupButtons = new System.Windows.Forms.GroupBox();
            this.btnDelete = new ExButton.NET.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.btnAdd = new ExButton.NET.ExButton();
            this.exButton4 = new ExButton.NET.ExButton();
            this.btnRestore = new ExButton.NET.ExButton();
            this.exButton3 = new ExButton.NET.ExButton();
            this.btnRename = new ExButton.NET.ExButton();
            this.btnPast = new ExButton.NET.ExButton();
            this.btnCut = new ExButton.NET.ExButton();
            this.contextMenuAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьВКореньДереваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupButtons.SuspendLayout();
            this.contextMenuAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(70, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Visible = false;
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(58, 22);
            this.toolStripButton7.Text = "Выбрать";
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(729, 402);
            // 
            // treeCat
            // 
            this.treeCat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeCat.HideSelection = false;
            this.treeCat.Location = new System.Drawing.Point(0, 0);
            this.treeCat.Name = "treeCat";
            this.treeCat.Size = new System.Drawing.Size(516, 494);
            this.treeCat.TabIndex = 2;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.exButton2);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.exButton1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.groupButtons);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.treeCat);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(642, 522);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(642, 522);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // exButton2
            // 
            this.exButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exButton2.Image = null;
            this.exButton2.Location = new System.Drawing.Point(77, 496);
            this.exButton2.Name = "exButton2";
            this.exButton2.Size = new System.Drawing.Size(74, 23);
            this.exButton2.TabIndex = 60;
            this.exButton2.Text = "Развернуть";
            this.exButton2.Click += new System.EventHandler(this.exButton2_Click);
            // 
            // exButton1
            // 
            this.exButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exButton1.Image = null;
            this.exButton1.Location = new System.Drawing.Point(3, 496);
            this.exButton1.Name = "exButton1";
            this.exButton1.Size = new System.Drawing.Size(68, 23);
            this.exButton1.TabIndex = 59;
            this.exButton1.Text = "Свернуть";
            this.exButton1.Click += new System.EventHandler(this.exButton1_Click);
            // 
            // groupButtons
            // 
            this.groupButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupButtons.Controls.Add(this.btnDelete);
            this.groupButtons.Controls.Add(this.label1);
            this.groupButtons.Controls.Add(this.chkShowDeleted);
            this.groupButtons.Controls.Add(this.btnAdd);
            this.groupButtons.Controls.Add(this.exButton4);
            this.groupButtons.Controls.Add(this.btnRestore);
            this.groupButtons.Controls.Add(this.exButton3);
            this.groupButtons.Controls.Add(this.btnRename);
            this.groupButtons.Controls.Add(this.btnPast);
            this.groupButtons.Controls.Add(this.btnCut);
            this.groupButtons.Location = new System.Drawing.Point(522, 12);
            this.groupButtons.Name = "groupButtons";
            this.groupButtons.Size = new System.Drawing.Size(108, 303);
            this.groupButtons.TabIndex = 59;
            this.groupButtons.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(12, 37);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(93, 23);
            this.btnDelete.TabIndex = 50;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "удаленные";
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(11, 233);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(75, 17);
            this.chkShowDeleted.TabIndex = 56;
            this.chkShowDeleted.Text = "Показать";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.IsSplitButton = true;
            this.btnAdd.Location = new System.Drawing.Point(11, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(91, 23);
            this.btnAdd.TabIndex = 51;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.SplitButtonClick += new System.EventHandler(this.btnAdd_SplitButtonClick);
            this.btnAdd.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // exButton4
            // 
            this.exButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton4.Enabled = false;
            this.exButton4.Image = ((System.Drawing.Image)(resources.GetObject("exButton4.Image")));
            this.exButton4.Location = new System.Drawing.Point(14, 186);
            this.exButton4.Name = "exButton4";
            this.exButton4.Size = new System.Drawing.Size(91, 23);
            this.exButton4.TabIndex = 54;
            this.exButton4.Text = "Вниз";
            // 
            // btnRestore
            // 
            this.btnRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestore.Enabled = false;
            this.btnRestore.Image = null;
            this.btnRestore.Location = new System.Drawing.Point(11, 270);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(91, 23);
            this.btnRestore.TabIndex = 58;
            this.btnRestore.Text = "Восстановить";
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // exButton3
            // 
            this.exButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exButton3.Enabled = false;
            this.exButton3.Image = ((System.Drawing.Image)(resources.GetObject("exButton3.Image")));
            this.exButton3.Location = new System.Drawing.Point(14, 157);
            this.exButton3.Name = "exButton3";
            this.exButton3.Size = new System.Drawing.Size(91, 23);
            this.exButton3.TabIndex = 53;
            this.exButton3.Text = "Вверх";
            // 
            // btnRename
            // 
            this.btnRename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRename.Image = null;
            this.btnRename.Location = new System.Drawing.Point(12, 124);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(91, 23);
            this.btnRename.TabIndex = 55;
            this.btnRename.Text = "Переименовать";
            this.btnRename.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // btnPast
            // 
            this.btnPast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPast.Image = ((System.Drawing.Image)(resources.GetObject("btnPast.Image")));
            this.btnPast.Location = new System.Drawing.Point(12, 95);
            this.btnPast.Name = "btnPast";
            this.btnPast.Size = new System.Drawing.Size(91, 23);
            this.btnPast.TabIndex = 52;
            this.btnPast.Text = "Вставить";
            this.btnPast.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // btnCut
            // 
            this.btnCut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCut.Image = ((System.Drawing.Image)(resources.GetObject("btnCut.Image")));
            this.btnCut.Location = new System.Drawing.Point(12, 66);
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(91, 23);
            this.btnCut.TabIndex = 1;
            this.btnCut.Text = "Вырезать";
            this.btnCut.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // contextMenuAdd
            // 
            this.contextMenuAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьВКореньДереваToolStripMenuItem});
            this.contextMenuAdd.Name = "contextMenuAdd";
            this.contextMenuAdd.Size = new System.Drawing.Size(218, 26);
            // 
            // добавитьВКореньДереваToolStripMenuItem
            // 
            this.добавитьВКореньДереваToolStripMenuItem.Name = "добавитьВКореньДереваToolStripMenuItem";
            this.добавитьВКореньДереваToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.добавитьВКореньДереваToolStripMenuItem.Text = "Добавить в корень дерева";
            this.добавитьВКореньДереваToolStripMenuItem.Click += new System.EventHandler(this.btnAddToRoot_Click);
            // 
            // ftmpCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 522);
            this.Controls.Add(this.toolStripContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ftmpCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Категории";
            this.Load += new System.EventHandler(this.ftmpCategory_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupButtons.ResumeLayout(false);
            this.groupButtons.PerformLayout();
            this.contextMenuAdd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        public System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        public System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.ToolStripButton toolStripButton7;
        public System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        public System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        public System.Windows.Forms.ToolStripContentPanel ContentPanel;
        public System.Windows.Forms.TreeView treeCat;
        public System.Windows.Forms.ToolStripContainer toolStripContainer1;
        public System.Windows.Forms.ContextMenuStrip contextMenuAdd;
        public System.Windows.Forms.ToolStripMenuItem добавитьВКореньДереваToolStripMenuItem;
        public ExButton.NET.ExButton btnRestore;
        public System.Windows.Forms.Label label1;
        public ExButton.NET.ExButton btnAdd;
        public ExButton.NET.ExButton btnDelete;
        public ExButton.NET.ExButton exButton4;
        public System.Windows.Forms.CheckBox chkShowDeleted;
        public ExButton.NET.ExButton exButton3;
        public ExButton.NET.ExButton btnPast;
        public ExButton.NET.ExButton btnRename;
        public ExButton.NET.ExButton btnCut;
        public System.Windows.Forms.GroupBox groupButtons;
        public ExButton.NET.ExButton exButton2;
        public ExButton.NET.ExButton exButton1;

    }
}