﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
/* Что нужно сделать чтобы реализовать форму шаблона?
 * 1. Создать интерфейсф для сущностей, которые будут использовать эту форму. 
 *    В интерфейсах нужжно объявить все методы и свойства задействованные формой шаблоном
 * 2. Реализовать интерфейсы для каждой сущности. Т.е. инициализировать все свойства и реализовать все методы.
 * 3. Реализовать все функции в форме-шаблоне
 * 4. Создать форму на основе формы шаблона 
 */
using Medsyst.Dao.Base;

namespace Medsyst
{
    ///<summary>Функциональный шаблон формы категорий</summary>
    public partial class ftmpCategoryList: Form
    {
        ///<summary>Объект списка категорий</summary>
        public IDBOList Categories;
        ///<summary>Объект категории</summary>
        public IDBO Category;

        public ftmpCategoryList()
        {
            InitializeComponent();
        }

        private void ftmpCategory_Load(object sender, EventArgs e)
        {
             dgCategory.AutoGenerateColumns = false;
            if (cmd.Mode == Class.Core.CommandModes.Select)
            {//Форма открыта для выбора
                groupButtons.Enabled = false; //При использовании формы для выбора делать кнопки недоступными
            }
        }
 
        ///<summary>Загрузка списка инициализация элементов управления с учетом установленного фильтра "Показать удаленные"
        ///Запускается в реализованной на основе этого шаблона форме при запуске после 
        ///определения Categories и Category
        ///Заплатин Е.Ф.
        ///08.07.2011</summary>
        public void LoadCategory()
        {
            Categories.ShowDeleted = chkShowDeleted.Checked;//Считывание настроек фильтра
            //Загрузка категорий пациентов
            dgCategory.DataSource = null;
            Categories.Load();
            dgCategory.DataSource = Categories; //Указание комбобоксу в качестве источника данных заполненной коллекции

        }

        /// <summary> Удаление категории
        /// Заплатин Е.Ф.
        /// 07.07.2011
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Следует реализовать
        }
        //Добавление категории 
        private void btnAdd_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Text = "Введите название категории:";
            if (frm.Show(this) == DialogResult.OK)
            {
                IDBO m = Category;//Не совсем понятно, зачем это нужно делать? ЗЕФ.
                m.Name = frm.Value;
                m.Insert();
                LoadCategory();
                //Categories.Add(m.Name);
                //Categories.Load();
            }
        }
        // Переименование категории
        private void btnRename_Click(object sender, EventArgs e)
        {
            if (dgCategory.SelectedRows == null) return;
            InputBox frm = new InputBox();
            frm.Text = "Введите новое название категории:";
            frm.Value = dgCategory.SelectedRows[0].Cells["colCategoryN"].Value.ToString();
            if (frm.Show(this) == DialogResult.OK)
            {
                if (frm.Value.Trim() == "") return;
                IDBO m = Category;
                m.ID = int.Parse(dgCategory.SelectedRows[0].Cells["colCategoryID"].Value.ToString());
                m.Load();
                m.Name = frm.Value;
                m.Update();
                dgCategory.SelectedRows[0].Cells["colCategoryN"].Value = m.Name;
                Categories.Load();
            }
        }
        /// <summary>Отображение/скрытие удаленных категорий</summary>
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            btnRestore.Enabled = chkShowDeleted.Checked;//Открывается/закрывается доступ к кнопке восстановления при включеном/выключеном фильтре показа удаленных узлов
            LoadCategory();//Перезагрузка дерева
        }
        /// <summary>Восстановление категории, отмеченной как удаленная
        /// Заплатин Е.Ф.
        /// 08.07.2011
        /// </summary>
        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (dgCategory.SelectedRows == null) return;//Не выделена ни одна категория
            Categories.Restore((int)dgCategory.SelectedRows[0].Cells["colCategoryID"].Value);
            LoadCategory();
        }
        /// <summary>Отображение коментария к выделеному элементу
        /// Заплатин Е.Ф.
        /// 12.05.2013
        /// </summary>
        private void dgCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IDBO m = Category;
            m.ID = int.Parse(dgCategory.SelectedRows[0].Cells["colCategoryID"].Value.ToString());
            m.Load();
            txtComment.Text = m.comment;
        }
        /// <summary>Выбор категории
        /// Заплатин Е.Ф.
        /// 12.05.2013
        /// </summary>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            Tag = new Command(CommandModes.Return, int.Parse(dgCategory.SelectedRows[0].Cells["colCategoryID"].Value.ToString()));
            Close();

        }
      }
}
