﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;

namespace Medsyst
{
    ///<summary>Функциональный шаблон формы категорий</summary>
    public partial class ftmpCategory: Form
    {
        ///<summary>Объект дерева категорий</summary>
        public IDBOTree Categories;
        ///<summary>Объект категории</summary>
        public ITreeItem Category;
        ///<summary>Идентификатор вырезанной категории</summary>
        private int CutID = 0;

        public ftmpCategory()
        {
            InitializeComponent();
        }
        
        ///<summary>Загрузка дерева и инициализация элементов управления с учетом установленного фильтра "Показать удаленные"
        ///Заплатин Е.Ф.
        ///08.07.2011</summary>
        public void LoadCategory()
        {
            Categories.ShowDeleted = chkShowDeleted.Checked;//Считывание настроек фильтра
            Categories.Load();//Загрузка коллекции категорий с учетом фильтра
            Categories.FillTree(treeCat);
            treeCat.ExpandAll();
        }

        /// <summary> Удаление категории
        /// Заплатин Е.Ф.
        /// 07.07.2011
        /// </summary>
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;//Не выделена ни одна категория
            if (Categories.DeleteFromTable(treeCat.SelectedNode)) // Попытка удаления категории
                LoadCategory();//Перезагрузка  дерева с учетом фильтра 

        }

        // Добавить в категорию
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Text = "Введите название категории:";
            if (frm.Show(this) == DialogResult.OK)
            {
                ITreeItem m = Category;
                m.Text = frm.Value;
                m.ParentID = int.Parse(treeCat.SelectedNode.Name);
                m.Insert();
                Categories.AddNode(treeCat, treeCat.SelectedNode, m.ID, m.Text);
                Categories.Load();
            }
        }

        //Добавление категории в корень дерева (дублирует в большинстве кода обычную операцию добавления
        //что не совсем красиво с точки зрения программирования)
        private void btnAddToRoot_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Text = "Введите название категории:";
            if (frm.Show(this) == DialogResult.OK)
            {
                ITreeItem m = Category;
                m.Text = frm.Value;
                m.ParentID = 0;
                m.Insert();
                Categories.AddNode(treeCat, null, m.ID, m.Text);
                Categories.Load();
            }
        }


        // Переименование категории
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;
            InputBox frm = new InputBox();
            frm.Text = "Введите новое название категории:";
            frm.Value = treeCat.SelectedNode.Text;
            if (frm.Show(this) == DialogResult.OK)
            {
                if (frm.Value.Trim() == "") return;
                ITreeItem m = Category;
                m.ID = int.Parse(treeCat.SelectedNode.Name);
                m.Load();
                m.Text = frm.Value;
                m.Update();
                treeCat.SelectedNode.Text = m.Text;
                Categories.Load();
            }
        }

        // Вырезать
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;
            CutID = int.Parse(treeCat.SelectedNode.Name);
            btnCut.Enabled = false;
            btnPast.Enabled = true;
            treeCat.SelectedNode.Remove();
        }

        // Вставить
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null || CutID<=0) return;

            ITreeItem m = Category;
            m.ID = CutID;
            m.Load();
            m.ParentID = int.Parse(treeCat.SelectedNode.Name);
            m.Update();

            btnCut.Enabled = true;
            btnPast.Enabled = false;

            Categories.Load();
            Categories.FillTree(treeCat);
            treeCat.ExpandAll();
        }
     
        //Отображение меню комбо кнопки добавления новой категории
        private void btnAdd_SplitButtonClick(object sender, EventArgs e)
        {
            Point p = new Point(btnAdd.Left, btnAdd.Bottom);
            contextMenuAdd.Show(this, p);
        }
        /// <summary>Отображение/скрытие удаленных категорий
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            btnRestore.Enabled = chkShowDeleted.Checked;//Открывается/закрывается доступ к кнопке восстановления при включеном/выключеном фильтре показа удаленных узлов
            LoadCategory();//Перезагрузка дерева
        }
        /// <summary>Восстановление категории, отмеченной как удаленная
        /// Заплатин Е.Ф.
        /// 08.07.2011
        /// </summary>
        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;//Не выделена ни одна категория
            Categories.Restore(treeCat.SelectedNode);
            LoadCategory();
        }

        private void ftmpCategory_Load(object sender, EventArgs e)
        {
            if (cmd.Mode == Class.Core.CommandModes.Select)
            {//Форма открыта для выбора
                groupButtons.Enabled = false; //При использовании формы для выбора делать кнопки недоступными
            }
        }
        /// <summary>Сворачивание всех ветвей дерева
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// </summary>
        private void exButton1_Click(object sender, EventArgs e)
        {
            treeCat.CollapseAll();
        }
        /// <summary>Разворачивание всех ветвей дерева
        /// Заплатин Е.Ф.
        /// 25.01.2012
        /// </summary>
        private void exButton2_Click(object sender, EventArgs e)
        {
            treeCat.ExpandAll();
        }

      }
}
