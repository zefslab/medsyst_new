﻿namespace Medsyst
{
    partial class rptIllnessHistoryList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmbFacult = new System.Windows.Forms.ComboBox();
            this.chkFacult = new System.Windows.Forms.CheckBox();
            this.rbtnWorkers = new System.Windows.Forms.RadioButton();
            this.rbtnStudents = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExecutReport = new System.Windows.Forms.Button();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.illnessHistoryList1 = new Medsyst.Reports.IllnessHistoryList();
            this.studentByFacult1 = new Medsyst.Reports.StudentByFacult();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmbFacult);
            this.splitContainer1.Panel1.Controls.Add(this.chkFacult);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnWorkers);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnStudents);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeason);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.btnExecutReport);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.crystalReportViewer1);
            this.splitContainer1.Size = new System.Drawing.Size(886, 604);
            this.splitContainer1.SplitterDistance = 180;
            this.splitContainer1.TabIndex = 0;
            // 
            // cmbFacult
            // 
            this.cmbFacult.DisplayMember = "ShortName";
            this.cmbFacult.Enabled = false;
            this.cmbFacult.FormattingEnabled = true;
            this.cmbFacult.Location = new System.Drawing.Point(60, 127);
            this.cmbFacult.Name = "cmbFacult";
            this.cmbFacult.Size = new System.Drawing.Size(108, 21);
            this.cmbFacult.TabIndex = 95;
            this.cmbFacult.ValueMember = "FacultyID";
            // 
            // chkFacult
            // 
            this.chkFacult.AutoSize = true;
            this.chkFacult.Location = new System.Drawing.Point(10, 103);
            this.chkFacult.Name = "chkFacult";
            this.chkFacult.Size = new System.Drawing.Size(109, 17);
            this.chkFacult.TabIndex = 94;
            this.chkFacult.Text = " по факультетно";
            this.chkFacult.UseVisualStyleBackColor = true;
            this.chkFacult.CheckedChanged += new System.EventHandler(this.chkFacult_CheckedChanged);
            // 
            // rbtnWorkers
            // 
            this.rbtnWorkers.AutoSize = true;
            this.rbtnWorkers.Location = new System.Drawing.Point(60, 80);
            this.rbtnWorkers.Name = "rbtnWorkers";
            this.rbtnWorkers.Size = new System.Drawing.Size(84, 17);
            this.rbtnWorkers.TabIndex = 93;
            this.rbtnWorkers.Text = "Сотрудники";
            this.rbtnWorkers.UseVisualStyleBackColor = true;
            // 
            // rbtnStudents
            // 
            this.rbtnStudents.AutoSize = true;
            this.rbtnStudents.Checked = true;
            this.rbtnStudents.Location = new System.Drawing.Point(60, 57);
            this.rbtnStudents.Name = "rbtnStudents";
            this.rbtnStudents.Size = new System.Drawing.Size(73, 17);
            this.rbtnStudents.TabIndex = 92;
            this.rbtnStudents.TabStop = true;
            this.rbtnStudents.Text = "Студенты";
            this.rbtnStudents.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 91;
            this.label4.Text = "Пациенты";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "FullNbr";
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(59, 10);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(108, 21);
            this.cmbSeason.TabIndex = 87;
            this.cmbSeason.ValueMember = "SeasonId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 86;
            this.label3.Text = "Сезон";
            // 
            // btnExecutReport
            // 
            this.btnExecutReport.Location = new System.Drawing.Point(10, 154);
            this.btnExecutReport.Name = "btnExecutReport";
            this.btnExecutReport.Size = new System.Drawing.Size(155, 23);
            this.btnExecutReport.TabIndex = 85;
            this.btnExecutReport.Text = "Гененрировать отчет";
            this.btnExecutReport.UseVisualStyleBackColor = true;
            this.btnExecutReport.Click += new System.EventHandler(this.btnExecutReport_Click);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ShowGotoPageButton = false;
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.ShowParameterPanelButton = false;
            this.crystalReportViewer1.ShowTextSearchButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(702, 604);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // rptIllnessHistoryList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 604);
            this.Controls.Add(this.splitContainer1);
            this.Name = "rptIllnessHistoryList";
            this.Text = "rptIllnessHistoryList";
            this.Load += new System.EventHandler(this.rptIllnessHistoryList_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RadioButton rbtnWorkers;
        private System.Windows.Forms.RadioButton rbtnStudents;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExecutReport;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private Reports.IllnessHistoryList illnessHistoryList1;
        private System.Windows.Forms.ComboBox cmbFacult;
        private System.Windows.Forms.CheckBox chkFacult;
        private Reports.StudentByFacult studentByFacult1;
    }
}