﻿namespace Medsyst
{
    partial class frmIllnessHistoryList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIllnessHistoryList));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgDiagOut = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgDiagMain = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgDiagAttand = new System.Windows.Forms.DataGridView();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.chlbCategory = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkDoctor = new System.Windows.Forms.CheckBox();
            this.cmbYear2 = new System.Windows.Forms.ComboBox();
            this.chkSeasons = new System.Windows.Forms.CheckBox();
            this.cmbYear1 = new System.Windows.Forms.ComboBox();
            this.cmbDoctor = new System.Windows.Forms.ComboBox();
            this.chkYear = new System.Windows.Forms.CheckBox();
            this.dgSeasons = new System.Windows.Forms.DataGridView();
            this.colChk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeasonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rbSexF = new System.Windows.Forms.RadioButton();
            this.rbSexM = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSex = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFilterCancel = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.udPageCount = new System.Windows.Forms.NumericUpDown();
            this.txtCostSite = new System.Windows.Forms.TextBox();
            this.btnPageEnd = new System.Windows.Forms.Button();
            this.txtSumm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPagePrev = new System.Windows.Forms.Button();
            this.btnPageNext = new System.Windows.Forms.Button();
            this.lblPageCount = new System.Windows.Forms.Label();
            this.udPage = new System.Windows.Forms.NumericUpDown();
            this.lblCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPageBegin = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.gridIHL = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIHNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBirthD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRegisterD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSumm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbView = new System.Windows.Forms.ToolStripButton();
            this.ПечатьMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.книжкаНазначенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.историяБолезниToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обратныйТалонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ЖурналРегистрацииПациентовMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.СписокСтудентовПоФакулMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tstFind = new System.Windows.Forms.ToolStripTextBox();
            this.tsbFind = new System.Windows.Forms.ToolStripButton();
            this.tsbCreateTemplate = new System.Windows.Forms.ToolStripButton();
            this.toolStripAdmin = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuItemShowIdIh = new System.Windows.Forms.ToolStripMenuItem();
            this.cachedStudentByFacult1 = new Medsyst.Reports.CachedStudentByFacult();
            this.удалитьИсториюБолезниToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagAttand)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSeasons)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIHL)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(721, 468);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(640, 468);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(559, 468);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(478, 468);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.dgDiagOut);
            this.panel2.Controls.Add(this.dgDiagMain);
            this.panel2.Controls.Add(this.dgDiagAttand);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(266, 560);
            this.panel2.TabIndex = 6;
            // 
            // dgDiagOut
            // 
            this.dgDiagOut.AllowUserToAddRows = false;
            this.dgDiagOut.AllowUserToDeleteRows = false;
            this.dgDiagOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDiagOut.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgDiagOut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDiagOut.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDiagOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDiagOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDiagOut.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgDiagOut.Enabled = false;
            this.dgDiagOut.GridColor = System.Drawing.SystemColors.Window;
            this.dgDiagOut.Location = new System.Drawing.Point(3, 17);
            this.dgDiagOut.MultiSelect = false;
            this.dgDiagOut.Name = "dgDiagOut";
            this.dgDiagOut.RowHeadersVisible = false;
            this.dgDiagOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiagOut.Size = new System.Drawing.Size(257, 143);
            this.dgDiagOut.TabIndex = 30;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "МКБ Код";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 500;
            // 
            // dgDiagMain
            // 
            this.dgDiagMain.AllowUserToAddRows = false;
            this.dgDiagMain.AllowUserToDeleteRows = false;
            this.dgDiagMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDiagMain.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgDiagMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDiagMain.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgDiagMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDiagMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDiagMain.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgDiagMain.Enabled = false;
            this.dgDiagMain.GridColor = System.Drawing.SystemColors.Window;
            this.dgDiagMain.Location = new System.Drawing.Point(3, 355);
            this.dgDiagMain.MultiSelect = false;
            this.dgDiagMain.Name = "dgDiagMain";
            this.dgDiagMain.RowHeadersVisible = false;
            this.dgDiagMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiagMain.Size = new System.Drawing.Size(257, 143);
            this.dgDiagMain.TabIndex = 29;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn1.HeaderText = "МКБ Код";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 500;
            // 
            // dgDiagAttand
            // 
            this.dgDiagAttand.AllowUserToAddRows = false;
            this.dgDiagAttand.AllowUserToDeleteRows = false;
            this.dgDiagAttand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDiagAttand.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgDiagAttand.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDiagAttand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgDiagAttand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDiagAttand.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCode});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDiagAttand.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgDiagAttand.Enabled = false;
            this.dgDiagAttand.GridColor = System.Drawing.SystemColors.Window;
            this.dgDiagAttand.Location = new System.Drawing.Point(3, 187);
            this.dgDiagAttand.MultiSelect = false;
            this.dgDiagAttand.Name = "dgDiagAttand";
            this.dgDiagAttand.RowHeadersVisible = false;
            this.dgDiagAttand.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiagAttand.Size = new System.Drawing.Size(257, 143);
            this.dgDiagAttand.TabIndex = 28;
            // 
            // colCode
            // 
            this.colCode.DataPropertyName = "Name";
            this.colCode.HeaderText = "МКБ Код";
            this.colCode.Name = "colCode";
            this.colCode.ReadOnly = true;
            this.colCode.Width = 500;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Диагноз с места отбора";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Диагноз санатория при поступлении";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 339);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Диагноз санатория при выписке";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-31, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Краткие сведения о пациенте";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer5);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1020, 560);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1020, 585);
            this.toolStripContainer1.TabIndex = 6;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.splitContainer7);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(1020, 560);
            this.splitContainer5.SplitterDistance = 214;
            this.splitContainer5.TabIndex = 0;
            // 
            // splitContainer7
            // 
            this.splitContainer7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer7.IsSplitterFixed = true;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.chlbCategory);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer7.Size = new System.Drawing.Size(214, 543);
            this.splitContainer7.SplitterDistance = 115;
            this.splitContainer7.TabIndex = 0;
            // 
            // chlbCategory
            // 
            this.chlbCategory.CheckOnClick = true;
            this.chlbCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chlbCategory.FormattingEnabled = true;
            this.chlbCategory.Location = new System.Drawing.Point(0, 0);
            this.chlbCategory.Name = "chlbCategory";
            this.chlbCategory.Size = new System.Drawing.Size(210, 111);
            this.chlbCategory.TabIndex = 2;
            this.chlbCategory.SelectedIndexChanged += new System.EventHandler(this.chlbCategory_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkDoctor);
            this.groupBox1.Controls.Add(this.cmbYear2);
            this.groupBox1.Controls.Add(this.chkSeasons);
            this.groupBox1.Controls.Add(this.cmbYear1);
            this.groupBox1.Controls.Add(this.cmbDoctor);
            this.groupBox1.Controls.Add(this.chkYear);
            this.groupBox1.Controls.Add(this.dgSeasons);
            this.groupBox1.Controls.Add(this.rbSexF);
            this.groupBox1.Controls.Add(this.rbSexM);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chkSex);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnFilterCancel);
            this.groupBox1.Controls.Add(this.btnFilter);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 412);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // chkDoctor
            // 
            this.chkDoctor.AutoSize = true;
            this.chkDoctor.Location = new System.Drawing.Point(6, 16);
            this.chkDoctor.Name = "chkDoctor";
            this.chkDoctor.Size = new System.Drawing.Size(74, 17);
            this.chkDoctor.TabIndex = 29;
            this.chkDoctor.Text = "Терапевт";
            this.chkDoctor.UseVisualStyleBackColor = true;
            this.chkDoctor.CheckedChanged += new System.EventHandler(this.chkDoctor_CheckedChanged);
            // 
            // cmbYear2
            // 
            this.cmbYear2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear2.Enabled = false;
            this.cmbYear2.FormattingEnabled = true;
            this.cmbYear2.Location = new System.Drawing.Point(114, 321);
            this.cmbYear2.Name = "cmbYear2";
            this.cmbYear2.Size = new System.Drawing.Size(58, 21);
            this.cmbYear2.TabIndex = 24;
            // 
            // chkSeasons
            // 
            this.chkSeasons.AutoSize = true;
            this.chkSeasons.Location = new System.Drawing.Point(6, 60);
            this.chkSeasons.Name = "chkSeasons";
            this.chkSeasons.Size = new System.Drawing.Size(65, 17);
            this.chkSeasons.TabIndex = 16;
            this.chkSeasons.Text = "Сезоны";
            this.chkSeasons.UseVisualStyleBackColor = true;
            this.chkSeasons.CheckedChanged += new System.EventHandler(this.chkSeasons_CheckedChanged);
            // 
            // cmbYear1
            // 
            this.cmbYear1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear1.Enabled = false;
            this.cmbYear1.FormattingEnabled = true;
            this.cmbYear1.Location = new System.Drawing.Point(41, 321);
            this.cmbYear1.Name = "cmbYear1";
            this.cmbYear1.Size = new System.Drawing.Size(58, 21);
            this.cmbYear1.TabIndex = 24;
            // 
            // cmbDoctor
            // 
            this.cmbDoctor.DisplayMember = "Fullname";
            this.cmbDoctor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoctor.Enabled = false;
            this.cmbDoctor.FormattingEnabled = true;
            this.cmbDoctor.Location = new System.Drawing.Point(41, 34);
            this.cmbDoctor.Name = "cmbDoctor";
            this.cmbDoctor.Size = new System.Drawing.Size(152, 21);
            this.cmbDoctor.TabIndex = 28;
            this.cmbDoctor.ValueMember = "DoctorID";
            this.cmbDoctor.SelectedIndexChanged += new System.EventHandler(this.cmbDoctor_SelectedIndexChanged);
            // 
            // chkYear
            // 
            this.chkYear.AutoSize = true;
            this.chkYear.Location = new System.Drawing.Point(15, 300);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(97, 17);
            this.chkYear.TabIndex = 19;
            this.chkYear.Text = "Год рождения";
            this.chkYear.UseVisualStyleBackColor = true;
            this.chkYear.CheckedChanged += new System.EventHandler(this.chkYear_CheckedChanged);
            // 
            // dgSeasons
            // 
            this.dgSeasons.AllowUserToAddRows = false;
            this.dgSeasons.AllowUserToDeleteRows = false;
            this.dgSeasons.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgSeasons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSeasons.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgSeasons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSeasons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChk,
            this.colYear,
            this.colNbr,
            this.colSeasonID});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSeasons.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgSeasons.Enabled = false;
            this.dgSeasons.GridColor = System.Drawing.SystemColors.Window;
            this.dgSeasons.Location = new System.Drawing.Point(7, 79);
            this.dgSeasons.MultiSelect = false;
            this.dgSeasons.Name = "dgSeasons";
            this.dgSeasons.RowHeadersVisible = false;
            this.dgSeasons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSeasons.Size = new System.Drawing.Size(179, 170);
            this.dgSeasons.TabIndex = 27;
            // 
            // colChk
            // 
            this.colChk.DataPropertyName = "Chk";
            this.colChk.FalseValue = "false";
            this.colChk.HeaderText = "";
            this.colChk.Name = "colChk";
            this.colChk.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colChk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colChk.TrueValue = "true";
            this.colChk.Width = 30;
            // 
            // colYear
            // 
            this.colYear.DataPropertyName = "Year";
            this.colYear.HeaderText = "Год";
            this.colYear.Name = "colYear";
            this.colYear.ReadOnly = true;
            this.colYear.Width = 45;
            // 
            // colNbr
            // 
            this.colNbr.DataPropertyName = "SeasonNbr";
            this.colNbr.HeaderText = "№";
            this.colNbr.Name = "colNbr";
            this.colNbr.ReadOnly = true;
            this.colNbr.Width = 45;
            // 
            // colSeasonID
            // 
            this.colSeasonID.DataPropertyName = "SeasonID";
            this.colSeasonID.HeaderText = "Код";
            this.colSeasonID.Name = "colSeasonID";
            this.colSeasonID.Visible = false;
            // 
            // rbSexF
            // 
            this.rbSexF.AutoSize = true;
            this.rbSexF.Enabled = false;
            this.rbSexF.Location = new System.Drawing.Point(114, 281);
            this.rbSexF.Name = "rbSexF";
            this.rbSexF.Size = new System.Drawing.Size(69, 17);
            this.rbSexF.TabIndex = 18;
            this.rbSexF.TabStop = true;
            this.rbSexF.Text = "женский";
            this.rbSexF.UseVisualStyleBackColor = true;
            // 
            // rbSexM
            // 
            this.rbSexM.AutoSize = true;
            this.rbSexM.Enabled = false;
            this.rbSexM.Location = new System.Drawing.Point(115, 258);
            this.rbSexM.Name = "rbSexM";
            this.rbSexM.Size = new System.Drawing.Size(70, 17);
            this.rbSexM.TabIndex = 17;
            this.rbSexM.TabStop = true;
            this.rbSexM.Text = "мужской";
            this.rbSexM.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 324);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "от";
            // 
            // chkSex
            // 
            this.chkSex.AutoSize = true;
            this.chkSex.Location = new System.Drawing.Point(15, 260);
            this.chkSex.Name = "chkSex";
            this.chkSex.Size = new System.Drawing.Size(96, 17);
            this.chkSex.TabIndex = 16;
            this.chkSex.Text = "Пол пациента";
            this.chkSex.UseVisualStyleBackColor = true;
            this.chkSex.CheckedChanged += new System.EventHandler(this.chkSex_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "до";
            // 
            // btnFilterCancel
            // 
            this.btnFilterCancel.Location = new System.Drawing.Point(22, 380);
            this.btnFilterCancel.Name = "btnFilterCancel";
            this.btnFilterCancel.Size = new System.Drawing.Size(152, 23);
            this.btnFilterCancel.TabIndex = 15;
            this.btnFilterCancel.Text = "Отменить фильтр";
            this.btnFilterCancel.UseVisualStyleBackColor = true;
            this.btnFilterCancel.Click += new System.EventHandler(this.btnFilterCancel_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilter.Location = new System.Drawing.Point(21, 351);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(153, 23);
            this.btnFilter.TabIndex = 14;
            this.btnFilter.Text = "Применить фильтр";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.AutoScroll = true;
            this.splitContainer6.Panel1.Controls.Add(this.panel1);
            this.splitContainer6.Panel1.Controls.Add(this.gridIHL);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.panel2);
            this.splitContainer6.Panel2.Controls.Add(this.label7);
            this.splitContainer6.Size = new System.Drawing.Size(802, 560);
            this.splitContainer6.SplitterDistance = 532;
            this.splitContainer6.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.udPageCount);
            this.panel1.Controls.Add(this.txtCostSite);
            this.panel1.Controls.Add(this.btnPageEnd);
            this.panel1.Controls.Add(this.txtSumm);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btnPagePrev);
            this.panel1.Controls.Add(this.btnPageNext);
            this.panel1.Controls.Add(this.lblPageCount);
            this.panel1.Controls.Add(this.udPage);
            this.panel1.Controls.Add(this.lblCount);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnPageBegin);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 515);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 45);
            this.panel1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Стоимость койкодня:";
            // 
            // udPageCount
            // 
            this.udPageCount.Location = new System.Drawing.Point(182, 3);
            this.udPageCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udPageCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udPageCount.Name = "udPageCount";
            this.udPageCount.Size = new System.Drawing.Size(44, 20);
            this.udPageCount.TabIndex = 29;
            this.udPageCount.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.udPageCount.Visible = false;
            // 
            // txtCostSite
            // 
            this.txtCostSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCostSite.Location = new System.Drawing.Point(138, 25);
            this.txtCostSite.Name = "txtCostSite";
            this.txtCostSite.ReadOnly = true;
            this.txtCostSite.Size = new System.Drawing.Size(88, 20);
            this.txtCostSite.TabIndex = 5;
            this.txtCostSite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPageEnd
            // 
            this.btnPageEnd.Location = new System.Drawing.Point(149, 3);
            this.btnPageEnd.Name = "btnPageEnd";
            this.btnPageEnd.Size = new System.Drawing.Size(27, 20);
            this.btnPageEnd.TabIndex = 28;
            this.btnPageEnd.Text = "->|";
            this.btnPageEnd.UseVisualStyleBackColor = true;
            this.btnPageEnd.Visible = false;
            this.btnPageEnd.Click += new System.EventHandler(this.btnPageEnd_Click);
            // 
            // txtSumm
            // 
            this.txtSumm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSumm.Location = new System.Drawing.Point(429, 25);
            this.txtSumm.Name = "txtSumm";
            this.txtSumm.ReadOnly = true;
            this.txtSumm.Size = new System.Drawing.Size(100, 20);
            this.txtSumm.TabIndex = 3;
            this.txtSumm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(272, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Итого по историям болезней:";
            // 
            // btnPagePrev
            // 
            this.btnPagePrev.Location = new System.Drawing.Point(28, 3);
            this.btnPagePrev.Name = "btnPagePrev";
            this.btnPagePrev.Size = new System.Drawing.Size(30, 20);
            this.btnPagePrev.TabIndex = 28;
            this.btnPagePrev.Text = "<<";
            this.btnPagePrev.UseVisualStyleBackColor = true;
            this.btnPagePrev.Visible = false;
            this.btnPagePrev.Click += new System.EventHandler(this.btnPagePrev_Click);
            // 
            // btnPageNext
            // 
            this.btnPageNext.Location = new System.Drawing.Point(120, 3);
            this.btnPageNext.Name = "btnPageNext";
            this.btnPageNext.Size = new System.Drawing.Size(30, 20);
            this.btnPageNext.TabIndex = 28;
            this.btnPageNext.Text = ">>";
            this.btnPageNext.UseVisualStyleBackColor = true;
            this.btnPageNext.Visible = false;
            this.btnPageNext.Click += new System.EventHandler(this.btnPageNext_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.AutoSize = true;
            this.lblPageCount.Location = new System.Drawing.Point(291, 3);
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Size = new System.Drawing.Size(13, 13);
            this.lblPageCount.TabIndex = 26;
            this.lblPageCount.Text = "0";
            this.lblPageCount.Visible = false;
            // 
            // udPage
            // 
            this.udPage.Location = new System.Drawing.Point(64, 3);
            this.udPage.Name = "udPage";
            this.udPage.Size = new System.Drawing.Size(58, 20);
            this.udPage.TabIndex = 27;
            this.udPage.Visible = false;
            // 
            // lblCount
            // 
            this.lblCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(516, 8);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(13, 13);
            this.lblCount.TabIndex = 23;
            this.lblCount.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(233, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Cтраниц:";
            this.label6.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(358, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Кол-во историй болезней:";
            // 
            // btnPageBegin
            // 
            this.btnPageBegin.Location = new System.Drawing.Point(2, 3);
            this.btnPageBegin.Name = "btnPageBegin";
            this.btnPageBegin.Size = new System.Drawing.Size(27, 20);
            this.btnPageBegin.TabIndex = 28;
            this.btnPageBegin.Text = "|<-";
            this.btnPageBegin.UseVisualStyleBackColor = true;
            this.btnPageBegin.Visible = false;
            this.btnPageBegin.Click += new System.EventHandler(this.btnPageBegin_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 25;
            // 
            // gridIHL
            // 
            this.gridIHL.AllowUserToAddRows = false;
            this.gridIHL.AllowUserToDeleteRows = false;
            this.gridIHL.AllowUserToOrderColumns = true;
            this.gridIHL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridIHL.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridIHL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridIHL.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colIHNbr,
            this.colName,
            this.colDoctor,
            this.colBirthD,
            this.colRegisterD,
            this.colSumm});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridIHL.DefaultCellStyle = dataGridViewCellStyle12;
            this.gridIHL.Location = new System.Drawing.Point(0, 1);
            this.gridIHL.MultiSelect = false;
            this.gridIHL.Name = "gridIHL";
            this.gridIHL.ReadOnly = true;
            this.gridIHL.RowHeadersVisible = false;
            this.gridIHL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridIHL.Size = new System.Drawing.Size(530, 514);
            this.gridIHL.TabIndex = 1;
            this.gridIHL.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridIHL_ColumnHeaderMouseClick);
            this.gridIHL.SelectionChanged += new System.EventHandler(this.gridIHL_SelectionChanged);
            // 
            // colId
            // 
            this.colId.DataPropertyName = "DocumentID";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            this.colId.Width = 60;
            // 
            // colIHNbr
            // 
            this.colIHNbr.DataPropertyName = "ihNbr";
            this.colIHNbr.HeaderText = "№ ИБ";
            this.colIHNbr.Name = "colIHNbr";
            this.colIHNbr.ReadOnly = true;
            this.colIHNbr.Width = 50;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Fullname";
            this.colName.HeaderText = "ФИО";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 225;
            // 
            // colDoctor
            // 
            this.colDoctor.DataPropertyName = "DoctorName";
            this.colDoctor.HeaderText = "Терапевт";
            this.colDoctor.Name = "colDoctor";
            this.colDoctor.ReadOnly = true;
            this.colDoctor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDoctor.Width = 60;
            // 
            // colBirthD
            // 
            this.colBirthD.DataPropertyName = "BirthD";
            dataGridViewCellStyle10.Format = "d";
            dataGridViewCellStyle10.NullValue = null;
            this.colBirthD.DefaultCellStyle = dataGridViewCellStyle10;
            this.colBirthD.HeaderText = "Дата рожд.";
            this.colBirthD.Name = "colBirthD";
            this.colBirthD.ReadOnly = true;
            this.colBirthD.Width = 80;
            // 
            // colRegisterD
            // 
            this.colRegisterD.DataPropertyName = "RegisterD";
            this.colRegisterD.FillWeight = 120F;
            this.colRegisterD.HeaderText = "Дата регистрации";
            this.colRegisterD.Name = "colRegisterD";
            this.colRegisterD.ReadOnly = true;
            this.colRegisterD.Visible = false;
            this.colRegisterD.Width = 120;
            // 
            // colSumm
            // 
            this.colSumm.DataPropertyName = "Summ";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle11.Format = "C2";
            dataGridViewCellStyle11.NullValue = null;
            this.colSumm.DefaultCellStyle = dataGridViewCellStyle11;
            this.colSumm.HeaderText = "Сумма";
            this.colSumm.Name = "colSumm";
            this.colSumm.ReadOnly = true;
            this.colSumm.Width = 90;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbView,
            this.ПечатьMenuItem,
            this.tstFind,
            this.tsbFind,
            this.tsbCreateTemplate,
            this.toolStripAdmin});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(513, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbView
            // 
            this.tsbView.Image = ((System.Drawing.Image)(resources.GetObject("tsbView.Image")));
            this.tsbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbView.Name = "tsbView";
            this.tsbView.Size = new System.Drawing.Size(74, 22);
            this.tsbView.Text = "Перейти";
            this.tsbView.Click += new System.EventHandler(this.tsbView_Click);
            // 
            // ПечатьMenuItem
            // 
            this.ПечатьMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.книжкаНазначенияToolStripMenuItem,
            this.историяБолезниToolStripMenuItem,
            this.обратныйТалонToolStripMenuItem,
            this.ЖурналРегистрацииПациентовMenuItem,
            this.СписокСтудентовПоФакулMenuItem});
            this.ПечатьMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ПечатьMenuItem.Image")));
            this.ПечатьMenuItem.Name = "ПечатьMenuItem";
            this.ПечатьMenuItem.Size = new System.Drawing.Size(74, 25);
            this.ПечатьMenuItem.Text = "Печать";
            // 
            // книжкаНазначенияToolStripMenuItem
            // 
            this.книжкаНазначенияToolStripMenuItem.Name = "книжкаНазначенияToolStripMenuItem";
            this.книжкаНазначенияToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.книжкаНазначенияToolStripMenuItem.Text = "Книжка назначения";
            this.книжкаНазначенияToolStripMenuItem.Click += new System.EventHandler(this.книжкаНазначенияToolStripMenuItem_Click);
            // 
            // историяБолезниToolStripMenuItem
            // 
            this.историяБолезниToolStripMenuItem.Name = "историяБолезниToolStripMenuItem";
            this.историяБолезниToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.историяБолезниToolStripMenuItem.Text = "История болезни";
            this.историяБолезниToolStripMenuItem.Click += new System.EventHandler(this.историяБолезниToolStripMenuItem_Click);
            // 
            // обратныйТалонToolStripMenuItem
            // 
            this.обратныйТалонToolStripMenuItem.Name = "обратныйТалонToolStripMenuItem";
            this.обратныйТалонToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.обратныйТалонToolStripMenuItem.Text = "Обратный талон";
            this.обратныйТалонToolStripMenuItem.Click += new System.EventHandler(this.обратныйТалонToolStripMenuItem_Click);
            // 
            // ЖурналРегистрацииПациентовMenuItem
            // 
            this.ЖурналРегистрацииПациентовMenuItem.Name = "ЖурналРегистрацииПациентовMenuItem";
            this.ЖурналРегистрацииПациентовMenuItem.Size = new System.Drawing.Size(266, 22);
            this.ЖурналРегистрацииПациентовMenuItem.Text = "Журнал регистрации пациентов";
            this.ЖурналРегистрацииПациентовMenuItem.Click += new System.EventHandler(this.отчетПоИсториямБолезнейToolStripMenuItem_Click);
            // 
            // СписокСтудентовПоФакулMenuItem
            // 
            this.СписокСтудентовПоФакулMenuItem.Name = "СписокСтудентовПоФакулMenuItem";
            this.СписокСтудентовПоФакулMenuItem.Size = new System.Drawing.Size(266, 22);
            this.СписокСтудентовПоФакулMenuItem.Text = "Список студентов по факультетам ";
            this.СписокСтудентовПоФакулMenuItem.Click += new System.EventHandler(this.списокСтудентовПоToolStripMenuItem_Click);
            // 
            // tstFind
            // 
            this.tstFind.Name = "tstFind";
            this.tstFind.Size = new System.Drawing.Size(100, 25);
            this.tstFind.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tstFind_KeyUp);
            // 
            // tsbFind
            // 
            this.tsbFind.Image = ((System.Drawing.Image)(resources.GetObject("tsbFind.Image")));
            this.tsbFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFind.Name = "tsbFind";
            this.tsbFind.Size = new System.Drawing.Size(61, 22);
            this.tsbFind.Text = "Найти";
            this.tsbFind.Click += new System.EventHandler(this.tsbFind_Click);
            // 
            // tsbCreateTemplate
            // 
            this.tsbCreateTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbCreateTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCreateTemplate.Name = "tsbCreateTemplate";
            this.tsbCreateTemplate.Size = new System.Drawing.Size(102, 22);
            this.tsbCreateTemplate.Text = "Создать шаблон";
            this.tsbCreateTemplate.Click += new System.EventHandler(this.tsbCreateTemplate_Click);
            // 
            // toolStripAdmin
            // 
            this.toolStripAdmin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAdmin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemShowIdIh,
            this.удалитьИсториюБолезниToolStripMenuItem});
            this.toolStripAdmin.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAdmin.Image")));
            this.toolStripAdmin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAdmin.Name = "toolStripAdmin";
            this.toolStripAdmin.Size = new System.Drawing.Size(57, 22);
            this.toolStripAdmin.Text = "Админ";
            // 
            // menuItemShowIdIh
            // 
            this.menuItemShowIdIh.CheckOnClick = true;
            this.menuItemShowIdIh.Name = "menuItemShowIdIh";
            this.menuItemShowIdIh.Size = new System.Drawing.Size(270, 22);
            this.menuItemShowIdIh.Text = "Показать идентификаторы историй";
            this.menuItemShowIdIh.Click += new System.EventHandler(this.menuItemShowIdIh_Click);
            // 
            // удалитьИсториюБолезниToolStripMenuItem
            // 
            this.удалитьИсториюБолезниToolStripMenuItem.Name = "удалитьИсториюБолезниToolStripMenuItem";
            this.удалитьИсториюБолезниToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.удалитьИсториюБолезниToolStripMenuItem.Text = "Удалить историю болезни";
            this.удалитьИсториюБолезниToolStripMenuItem.Click += new System.EventHandler(this.deleteIHToolStripMenuItem_Click);
            // 
            // frmIllnessHistoryList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1020, 585);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "frmIllnessHistoryList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список историй болезней";
            this.Load += new System.EventHandler(this.frmIllnessHistoryList_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagAttand)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            this.splitContainer7.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSeasons)).EndInit();
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.Panel2.PerformLayout();
            this.splitContainer6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIHL)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripTextBox tstFind;
        private System.Windows.Forms.ToolStripButton tsbFind;
        private System.Windows.Forms.DataGridView gridIHL;
        private System.Windows.Forms.RadioButton rbSexF;
        private System.Windows.Forms.RadioButton rbSexM;
        private System.Windows.Forms.CheckBox chkSex;
        private System.Windows.Forms.Button btnFilterCancel;
        private System.Windows.Forms.CheckBox chkYear;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbYear1;
        private System.Windows.Forms.ComboBox cmbYear2;
        private System.Windows.Forms.Label lblPageCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown udPage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPageNext;
        private System.Windows.Forms.Button btnPagePrev;
        private System.Windows.Forms.Button btnPageEnd;
        private System.Windows.Forms.Button btnPageBegin;
        private System.Windows.Forms.NumericUpDown udPageCount;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor }, Hide = true)]
        public System.Windows.Forms.ToolStripButton tsbView;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSumm;
        private System.Windows.Forms.DataGridView dgSeasons;
        private System.Windows.Forms.CheckBox chkSeasons;
        private System.Windows.Forms.ComboBox cmbDoctor;
        private System.Windows.Forms.CheckBox chkDoctor;
        private System.Windows.Forms.ToolStripMenuItem ПечатьMenuItem;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor }, true)]
        public System.Windows.Forms.ToolStripMenuItem историяБолезниToolStripMenuItem;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor }, true)]
        public System.Windows.Forms.ToolStripMenuItem книжкаНазначенияToolStripMenuItem;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar, Groups.Stock }, true)]
        public System.Windows.Forms.ToolStripMenuItem ЖурналРегистрацииПациентовMenuItem;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar }, true)]
        public System.Windows.Forms.ToolStripMenuItem СписокСтудентовПоФакулMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCostSite;
        private System.Windows.Forms.GroupBox groupBox1;
        [sec(new Groups[] { Groups.Admin, Groups.Doctor }, Hide = true)]
        public System.Windows.Forms.ToolStripButton tsbCreateTemplate;
        private Medsyst.Reports.CachedStudentByFacult cachedStudentByFacult1;
        private System.Windows.Forms.DataGridView dgDiagAttand;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridView dgDiagOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgDiagMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colChk;
        private System.Windows.Forms.DataGridViewTextBoxColumn colYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeasonID;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor }, true)]
        public System.Windows.Forms.ToolStripMenuItem обратныйТалонToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox chlbCategory;
        [sec(new Groups[] { Groups.Admin, Groups.Manager}, Hide = true)]
        private System.Windows.Forms.ToolStripDropDownButton toolStripAdmin;
        private System.Windows.Forms.ToolStripMenuItem menuItemShowIdIh;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIHNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDoctor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBirthD;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRegisterD;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumm;
        private System.Windows.Forms.ToolStripMenuItem удалитьИсториюБолезниToolStripMenuItem;
    }
}