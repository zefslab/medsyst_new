﻿namespace Medsyst
{
    partial class frmRegistrature
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrature));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnChange = new ExButton.NET.ExButton();
            this.btnDelete = new ExButton.NET.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAdd = new ExButton.NET.ExButton();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.chlbCategory = new System.Windows.Forms.CheckedListBox();
            this.cmbYear2 = new System.Windows.Forms.ComboBox();
            this.cmbYear1 = new System.Windows.Forms.ComboBox();
            this.chkBirthDay = new System.Windows.Forms.CheckBox();
            this.chkAdge = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.chkSex = new System.Windows.Forms.CheckBox();
            this.btnFilterCancel = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.udPageCount = new System.Windows.Forms.NumericUpDown();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.lblPageCount = new System.Windows.Forms.Label();
            this.udPage = new System.Windows.Forms.NumericUpDown();
            this.lblCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.gridRegistrature = new System.Windows.Forms.DataGridView();
            this.colPatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSurName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBirthDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.btnFind = new System.Windows.Forms.ToolStripButton();
            this.panel2.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistrature)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(721, 468);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(640, 468);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(559, 468);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(478, 468);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.btnChange);
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.txtComment);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(169, 462);
            this.panel2.TabIndex = 6;
            // 
            // btnChange
            // 
            this.btnChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChange.Image = ((System.Drawing.Image)(resources.GetObject("btnChange.Image")));
            this.btnChange.Location = new System.Drawing.Point(3, 32);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(89, 23);
            this.btnChange.TabIndex = 55;
            this.btnChange.Text = "Изменить";
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(3, 61);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 23);
            this.btnDelete.TabIndex = 56;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Примечание";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(3, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(89, 23);
            this.btnAdd.TabIndex = 57;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(6, 120);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(160, 146);
            this.txtComment.TabIndex = 2;
            this.txtComment.Tag = "AmbCard.Comment";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-31, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Краткие сведения о пациенте";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer5);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(863, 466);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(863, 491);
            this.toolStripContainer1.TabIndex = 6;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // splitContainer5
            // 
            this.splitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.splitContainer7);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(863, 466);
            this.splitContainer5.SplitterDistance = 185;
            this.splitContainer5.TabIndex = 0;
            // 
            // splitContainer7
            // 
            this.splitContainer7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.chlbCategory);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.cmbYear2);
            this.splitContainer7.Panel2.Controls.Add(this.cmbYear1);
            this.splitContainer7.Panel2.Controls.Add(this.chkBirthDay);
            this.splitContainer7.Panel2.Controls.Add(this.chkAdge);
            this.splitContainer7.Panel2.Controls.Add(this.radioButton2);
            this.splitContainer7.Panel2.Controls.Add(this.radioButton1);
            this.splitContainer7.Panel2.Controls.Add(this.chkSex);
            this.splitContainer7.Panel2.Controls.Add(this.btnFilterCancel);
            this.splitContainer7.Panel2.Controls.Add(this.btnFilter);
            this.splitContainer7.Panel2.Controls.Add(this.dateTimePicker2);
            this.splitContainer7.Panel2.Controls.Add(this.label3);
            this.splitContainer7.Panel2.Controls.Add(this.label5);
            this.splitContainer7.Panel2.Controls.Add(this.label4);
            this.splitContainer7.Panel2.Controls.Add(this.label2);
            this.splitContainer7.Panel2.Controls.Add(this.dateTimePicker1);
            this.splitContainer7.Size = new System.Drawing.Size(185, 466);
            this.splitContainer7.SplitterDistance = 108;
            this.splitContainer7.TabIndex = 0;
            // 
            // chlbCategory
            // 
            this.chlbCategory.CheckOnClick = true;
            this.chlbCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chlbCategory.FormattingEnabled = true;
            this.chlbCategory.Location = new System.Drawing.Point(0, 0);
            this.chlbCategory.Name = "chlbCategory";
            this.chlbCategory.Size = new System.Drawing.Size(181, 104);
            this.chlbCategory.TabIndex = 1;
            this.chlbCategory.SelectedIndexChanged += new System.EventHandler(this.chlbCategory_SelectedIndexChanged);
            // 
            // cmbYear2
            // 
            this.cmbYear2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear2.Enabled = false;
            this.cmbYear2.FormattingEnabled = true;
            this.cmbYear2.Location = new System.Drawing.Point(115, 77);
            this.cmbYear2.Name = "cmbYear2";
            this.cmbYear2.Size = new System.Drawing.Size(58, 21);
            this.cmbYear2.TabIndex = 24;
            // 
            // cmbYear1
            // 
            this.cmbYear1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear1.Enabled = false;
            this.cmbYear1.FormattingEnabled = true;
            this.cmbYear1.Location = new System.Drawing.Point(42, 77);
            this.cmbYear1.Name = "cmbYear1";
            this.cmbYear1.Size = new System.Drawing.Size(58, 21);
            this.cmbYear1.TabIndex = 24;
            // 
            // chkBirthDay
            // 
            this.chkBirthDay.AutoSize = true;
            this.chkBirthDay.Location = new System.Drawing.Point(10, 101);
            this.chkBirthDay.Name = "chkBirthDay";
            this.chkBirthDay.Size = new System.Drawing.Size(119, 17);
            this.chkBirthDay.TabIndex = 20;
            this.chkBirthDay.Text = "Дата регистрации";
            this.chkBirthDay.UseVisualStyleBackColor = true;
            this.chkBirthDay.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // chkAdge
            // 
            this.chkAdge.AutoSize = true;
            this.chkAdge.Location = new System.Drawing.Point(10, 57);
            this.chkAdge.Name = "chkAdge";
            this.chkAdge.Size = new System.Drawing.Size(68, 17);
            this.chkAdge.TabIndex = 19;
            this.chkAdge.Text = "Возраст";
            this.chkAdge.UseVisualStyleBackColor = true;
            this.chkAdge.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(94, 39);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(69, 17);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "женский";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Enabled = false;
            this.radioButton1.Location = new System.Drawing.Point(94, 16);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(70, 17);
            this.radioButton1.TabIndex = 17;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "мужской";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // chkSex
            // 
            this.chkSex.AutoSize = true;
            this.chkSex.Location = new System.Drawing.Point(10, 17);
            this.chkSex.Name = "chkSex";
            this.chkSex.Size = new System.Drawing.Size(46, 17);
            this.chkSex.TabIndex = 16;
            this.chkSex.Text = "Пол";
            this.chkSex.UseVisualStyleBackColor = true;
            this.chkSex.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btnFilterCancel
            // 
            this.btnFilterCancel.Location = new System.Drawing.Point(21, 205);
            this.btnFilterCancel.Name = "btnFilterCancel";
            this.btnFilterCancel.Size = new System.Drawing.Size(152, 23);
            this.btnFilterCancel.TabIndex = 15;
            this.btnFilterCancel.Text = "Отменить фильтр";
            this.btnFilterCancel.UseVisualStyleBackColor = true;
            this.btnFilterCancel.Click += new System.EventHandler(this.btnFilterCancel_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnFilter.Location = new System.Drawing.Point(20, 176);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(153, 23);
            this.btnFilter.TabIndex = 14;
            this.btnFilter.Text = "Применить фильтр";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(37, 150);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker2.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "до";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "с";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "по";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "от";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(37, 124);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(136, 20);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // splitContainer6
            // 
            this.splitContainer6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.AutoScroll = true;
            this.splitContainer6.Panel1.Controls.Add(this.panel1);
            this.splitContainer6.Panel1.Controls.Add(this.gridRegistrature);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.panel2);
            this.splitContainer6.Panel2.Controls.Add(this.label7);
            this.splitContainer6.Size = new System.Drawing.Size(674, 466);
            this.splitContainer6.SplitterDistance = 497;
            this.splitContainer6.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.udPageCount);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.lblPageCount);
            this.panel1.Controls.Add(this.udPage);
            this.panel1.Controls.Add(this.lblCount);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 442);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(493, 20);
            this.panel1.TabIndex = 2;
            // 
            // udPageCount
            // 
            this.udPageCount.Location = new System.Drawing.Point(236, 2);
            this.udPageCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udPageCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udPageCount.Name = "udPageCount";
            this.udPageCount.Size = new System.Drawing.Size(44, 20);
            this.udPageCount.TabIndex = 29;
            this.udPageCount.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.udPageCount.ValueChanged += new System.EventHandler(this.udPageCount_ValueChanged);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(203, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(27, 20);
            this.button10.TabIndex = 28;
            this.button10.Text = "->|";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(82, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(30, 20);
            this.button8.TabIndex = 28;
            this.button8.Text = "<<";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(174, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(30, 20);
            this.button7.TabIndex = 28;
            this.button7.Text = ">>";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_2);
            // 
            // lblPageCount
            // 
            this.lblPageCount.AutoSize = true;
            this.lblPageCount.Location = new System.Drawing.Point(345, 2);
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Size = new System.Drawing.Size(13, 13);
            this.lblPageCount.TabIndex = 26;
            this.lblPageCount.Text = "0";
            // 
            // udPage
            // 
            this.udPage.Location = new System.Drawing.Point(118, 2);
            this.udPage.Name = "udPage";
            this.udPage.Size = new System.Drawing.Size(58, 20);
            this.udPage.TabIndex = 27;
            this.udPage.ValueChanged += new System.EventHandler(this.udPage_ValueChanged);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(444, 2);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(13, 13);
            this.lblCount.TabIndex = 23;
            this.lblCount.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Cтраниц:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(373, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Пациентов:";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(56, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(27, 20);
            this.button9.TabIndex = 28;
            this.button9.Text = "|<-";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Cтраница:";
            // 
            // gridRegistrature
            // 
            this.gridRegistrature.AllowUserToAddRows = false;
            this.gridRegistrature.AllowUserToDeleteRows = false;
            this.gridRegistrature.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridRegistrature.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRegistrature.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPatientID,
            this.colLastName,
            this.colName,
            this.colSurName,
            this.colBirthDay});
            this.gridRegistrature.Location = new System.Drawing.Point(0, 0);
            this.gridRegistrature.MultiSelect = false;
            this.gridRegistrature.Name = "gridRegistrature";
            this.gridRegistrature.ReadOnly = true;
            this.gridRegistrature.RowHeadersVisible = false;
            this.gridRegistrature.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridRegistrature.Size = new System.Drawing.Size(495, 441);
            this.gridRegistrature.TabIndex = 1;
            this.gridRegistrature.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridRegistrature_CellDoubleClick);
            this.gridRegistrature.SelectionChanged += new System.EventHandler(this.gridRegistrature_SelectionChanged);
            // 
            // colPatientID
            // 
            this.colPatientID.DataPropertyName = "PatientID";
            this.colPatientID.HeaderText = "PatientID";
            this.colPatientID.Name = "colPatientID";
            this.colPatientID.ReadOnly = true;
            this.colPatientID.Visible = false;
            // 
            // colLastName
            // 
            this.colLastName.DataPropertyName = "LastName";
            this.colLastName.HeaderText = "Фамилия";
            this.colLastName.Name = "colLastName";
            this.colLastName.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.HeaderText = "Имя";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colSurName
            // 
            this.colSurName.DataPropertyName = "SurName";
            this.colSurName.HeaderText = "Отечество";
            this.colSurName.Name = "colSurName";
            this.colSurName.ReadOnly = true;
            // 
            // colBirthDay
            // 
            this.colBirthDay.DataPropertyName = "BirthD";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.colBirthDay.DefaultCellStyle = dataGridViewCellStyle1;
            this.colBirthDay.HeaderText = "Дата рожд.";
            this.colBirthDay.Name = "colBirthDay";
            this.colBirthDay.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSearch,
            this.btnFind});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(175, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // txtSearch
            // 
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(100, 25);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnFind
            // 
            this.btnFind.Image = ((System.Drawing.Image)(resources.GetObject("btnFind.Image")));
            this.btnFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(61, 22);
            this.btnFind.Text = "Найти";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // frmRegistrature
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(863, 491);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "frmRegistrature";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистратура электронных медицинских карт пациентов";
            this.Load += new System.EventHandler(this.frmRegistrature_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            this.splitContainer7.Panel2.PerformLayout();
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.Panel2.PerformLayout();
            this.splitContainer6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistrature)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripTextBox txtSearch;
        private System.Windows.Forms.ToolStripButton btnFind;
        private System.Windows.Forms.DataGridView gridRegistrature;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.CheckBox chkSex;
        private System.Windows.Forms.Button btnFilterCancel;
        private System.Windows.Forms.CheckBox chkBirthDay;
        private System.Windows.Forms.CheckBox chkAdge;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbYear1;
        private System.Windows.Forms.ComboBox cmbYear2;
        private System.Windows.Forms.Label lblPageCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown udPage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.NumericUpDown udPageCount;
        private ExButton.NET.ExButton btnChange;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar }, false)]
        public ExButton.NET.ExButton btnDelete;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar }, false)]
        public ExButton.NET.ExButton btnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSurName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBirthDay;
        private System.Windows.Forms.CheckedListBox chlbCategory;
    }
}