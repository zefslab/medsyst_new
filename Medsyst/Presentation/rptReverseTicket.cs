﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.DataSet;
using Medsyst.DataSet.datReverseTicketTableAdapters;

using Medsyst.Class;

namespace Medsyst
{
    public partial class rptReverseTicket : Form
    {
        //Инициализация тайбладаптеров
        ReverseTicketTableAdapter ReverseTicket_TA = new ReverseTicketTableAdapter();
        PatientBookServiceTableAdapter PatientBookService_TA = new PatientBookServiceTableAdapter();
        PatientBookMedicamentTableAdapter  PatientBookMedicament_TA = new PatientBookMedicamentTableAdapter();
        IHDiagnosMKBAttendTableAdapter IHDiagnosMKBAttend_TA = new IHDiagnosMKBAttendTableAdapter();
        IHDiagnosMKBOutTableAdapter IHDiagnosMKBOut_TA = new IHDiagnosMKBOutTableAdapter();
        Settings set = new Settings(true);
        datReverseTicket ds = new datReverseTicket();
        
        public rptReverseTicket()
        {
            InitializeComponent();
        }

        private void rptReverseTicket_Load(object sender, EventArgs e)
        {
            int ih = cmd.Id;//История болезни

            //Инициализация строк подключения каждому адаптеру
            ReverseTicket_TA.Connection.ConnectionString = DB.CS;
            PatientBookService_TA.Connection.ConnectionString = DB.CS;
            PatientBookMedicament_TA.Connection.ConnectionString = DB.CS;
            IHDiagnosMKBAttend_TA.Connection.ConnectionString = DB.CS;
            IHDiagnosMKBOut_TA.Connection.ConnectionString = DB.CS;
            
            //Наполнение адаптеров 
            ReverseTicket_TA.Fill(ds.ReverseTicket, ih);
            PatientBookService_TA.Fill(ds.PatientBookService, ih);
            PatientBookMedicament_TA.Fill(ds.PatientBookMedicament, ih);
            IHDiagnosMKBAttend_TA.Fill(ds.IHDiagnosMKBAttend, 1,ih);
            IHDiagnosMKBOut_TA.Fill(ds.IHDiagnosMKBOut, 2, ih);

            reverseTicket.SetDataSource(ds); //передача отчету заполненного экземпляра набора данных - DS
            
            set.Load();//Загрузка настроек системы

            //Передача в отчет вспомогательных параметров для оформления  отчета
            reverseTicket.SetParameterValue("ГлавныйВрач", set.MainDoctor());
            reverseTicket.SetParameterValue("Санаторий", set.SanatoriumName());

            crystalReportViewer1.ReportSource = reverseTicket; //Элементу формы просмотра отчетов передается загруженный отчет

        }
    }
}
