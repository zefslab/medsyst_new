﻿namespace Medsyst
{
    partial class rptMedicamentChargesByRecieptOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkSclad = new System.Windows.Forms.CheckBox();
            this.gridCategoryGroups = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExecutReportByPeriod = new System.Windows.Forms.Button();
            this.rbtnNoBudget = new System.Windows.Forms.RadioButton();
            this.rbtnBudget = new System.Windows.Forms.RadioButton();
            this.dtpEndPeriod = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginPeriod = new System.Windows.Forms.DateTimePicker();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.medicamentChargesByRecieptOrder1 = new Medsyst.Reports.MedicamentChargesByRecieptOrder();
            this.colChk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCategoryGroups)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1199, 545);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1199, 576);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkSclad);
            this.splitContainer1.Panel1.Controls.Add(this.gridCategoryGroups);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.btnExecutReportByPeriod);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnNoBudget);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnBudget);
            this.splitContainer1.Panel1.Controls.Add(this.dtpEndPeriod);
            this.splitContainer1.Panel1.Controls.Add(this.dtpBeginPeriod);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.crystalReportViewer1);
            this.splitContainer1.Size = new System.Drawing.Size(1199, 545);
            this.splitContainer1.SplitterDistance = 278;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkSclad
            // 
            this.chkSclad.AutoSize = true;
            this.chkSclad.Enabled = false;
            this.chkSclad.Location = new System.Drawing.Point(16, 411);
            this.chkSclad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSclad.Name = "chkSclad";
            this.chkSclad.Size = new System.Drawing.Size(242, 21);
            this.chkSclad.TabIndex = 19;
            this.chkSclad.Text = "Показать количество на складе";
            this.chkSclad.UseVisualStyleBackColor = true;
            this.chkSclad.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // gridCategoryGroups
            // 
            this.gridCategoryGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridCategoryGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCategoryGroups.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChk,
            this.colGroup,
            this.colID});
            this.gridCategoryGroups.Location = new System.Drawing.Point(16, 156);
            this.gridCategoryGroups.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridCategoryGroups.MultiSelect = false;
            this.gridCategoryGroups.Name = "gridCategoryGroups";
            this.gridCategoryGroups.RowHeadersVisible = false;
            this.gridCategoryGroups.Size = new System.Drawing.Size(251, 185);
            this.gridCategoryGroups.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "с";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "Период";
            // 
            // btnExecutReportByPeriod
            // 
            this.btnExecutReportByPeriod.Location = new System.Drawing.Point(16, 364);
            this.btnExecutReportByPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExecutReportByPeriod.Name = "btnExecutReportByPeriod";
            this.btnExecutReportByPeriod.Size = new System.Drawing.Size(207, 28);
            this.btnExecutReportByPeriod.TabIndex = 14;
            this.btnExecutReportByPeriod.Text = "Гененрировать отчет";
            this.btnExecutReportByPeriod.UseVisualStyleBackColor = true;
            this.btnExecutReportByPeriod.Click += new System.EventHandler(this.btnExecutReportByPeriod_Click);
            // 
            // rbtnNoBudget
            // 
            this.rbtnNoBudget.AutoSize = true;
            this.rbtnNoBudget.Location = new System.Drawing.Point(112, 127);
            this.rbtnNoBudget.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnNoBudget.Name = "rbtnNoBudget";
            this.rbtnNoBudget.Size = new System.Drawing.Size(104, 21);
            this.rbtnNoBudget.TabIndex = 13;
            this.rbtnNoBudget.Text = "Внебюджет";
            this.rbtnNoBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnBudget
            // 
            this.rbtnBudget.AutoSize = true;
            this.rbtnBudget.Checked = true;
            this.rbtnBudget.Location = new System.Drawing.Point(16, 127);
            this.rbtnBudget.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnBudget.Name = "rbtnBudget";
            this.rbtnBudget.Size = new System.Drawing.Size(80, 21);
            this.rbtnBudget.TabIndex = 12;
            this.rbtnBudget.TabStop = true;
            this.rbtnBudget.Text = "Бюджет";
            this.rbtnBudget.UseVisualStyleBackColor = true;
            // 
            // dtpEndPeriod
            // 
            this.dtpEndPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEndPeriod.Location = new System.Drawing.Point(44, 80);
            this.dtpEndPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpEndPeriod.Name = "dtpEndPeriod";
            this.dtpEndPeriod.Size = new System.Drawing.Size(222, 22);
            this.dtpEndPeriod.TabIndex = 11;
            this.dtpEndPeriod.Value = new System.DateTime(2011, 2, 28, 23, 59, 0, 0);
            // 
            // dtpBeginPeriod
            // 
            this.dtpBeginPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpBeginPeriod.Location = new System.Drawing.Point(44, 44);
            this.dtpBeginPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpBeginPeriod.Name = "dtpBeginPeriod";
            this.dtpBeginPeriod.Size = new System.Drawing.Size(222, 22);
            this.dtpBeginPeriod.TabIndex = 10;
            this.dtpBeginPeriod.Value = new System.DateTime(2011, 1, 1, 17, 57, 0, 0);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(916, 545);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // colChk
            // 
            this.colChk.HeaderText = "";
            this.colChk.Name = "colChk";
            this.colChk.Width = 30;
            // 
            // colGroup
            // 
            this.colGroup.DataPropertyName = "GroupN";
            this.colGroup.HeaderText = "Группы категорий";
            this.colGroup.Name = "colGroup";
            this.colGroup.ReadOnly = true;
            this.colGroup.Width = 200;
            // 
            // colID
            // 
            this.colID.DataPropertyName = "GroupID";
            this.colID.HeaderText = "GroupID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            // 
            // rptMedicamentChargesByRecieptOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 576);
            this.Controls.Add(this.toolStripContainer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "rptMedicamentChargesByRecieptOrder";
            this.Text = "Расход медикаментов сгруппированых по приходным ордерам";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.rptMedicamentChargesByRecieptOrder_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCategoryGroups)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Reports.MedicamentChargesByRecieptOrder medicamentChargesByRecieptOrder1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExecutReportByPeriod;
        private System.Windows.Forms.RadioButton rbtnNoBudget;
        private System.Windows.Forms.RadioButton rbtnBudget;
        private System.Windows.Forms.DateTimePicker dtpEndPeriod;
        private System.Windows.Forms.DateTimePicker dtpBeginPeriod;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.DataGridView gridCategoryGroups;
        private System.Windows.Forms.CheckBox chkSclad;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colChk;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
    }
}