﻿namespace Medsyst
{
    partial class frmTaskList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaskList));
            this.dgTask = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSystemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colButton = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgEmployee = new System.Windows.Forms.DataGridView();
            this.colTaskEmployee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmployee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colNotice = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddEmployee = new ExButton.NET.ExButton();
            this.btnDeleteEmployee = new ExButton.NET.ExButton();
            this.btnAddTask = new ExButton.NET.ExButton();
            this.btnDelTask = new ExButton.NET.ExButton();
            this.btnEditTask = new ExButton.NET.ExButton();
            this.treeTask = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.dgTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // dgTask
            // 
            this.dgTask.AllowDrop = true;
            this.dgTask.AllowUserToAddRows = false;
            this.dgTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTask.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTask.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colName,
            this.colSystemName,
            this.colButton,
            this.colStatus});
            this.dgTask.Location = new System.Drawing.Point(9, 30);
            this.dgTask.MultiSelect = false;
            this.dgTask.Name = "dgTask";
            this.dgTask.RowHeadersVisible = false;
            this.dgTask.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTask.Size = new System.Drawing.Size(613, 148);
            this.dgTask.TabIndex = 0;
            this.dgTask.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTask_CellContentClick);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "TaskID";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            this.colID.Width = 30;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "TaskN";
            this.colName.HeaderText = "Задача";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colSystemName
            // 
            this.colSystemName.DataPropertyName = "TaskSystemN";
            this.colSystemName.HeaderText = "Системное имя";
            this.colSystemName.Name = "colSystemName";
            this.colSystemName.ReadOnly = true;
            // 
            // colButton
            // 
            this.colButton.DataPropertyName = "ButtonTaskComplet";
            this.colButton.HeaderText = "Кнопка завершения задачи";
            this.colButton.Name = "colButton";
            this.colButton.ReadOnly = true;
            this.colButton.Width = 150;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "StatusTask";
            this.colStatus.HeaderText = "Запись статуса выполнения задачи";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 300;
            // 
            // dgEmployee
            // 
            this.dgEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTaskEmployee,
            this.colEmployee,
            this.colNotice});
            this.dgEmployee.Location = new System.Drawing.Point(9, 353);
            this.dgEmployee.Name = "dgEmployee";
            this.dgEmployee.RowHeadersVisible = false;
            this.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEmployee.Size = new System.Drawing.Size(613, 111);
            this.dgEmployee.TabIndex = 1;
            this.dgEmployee.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEmployee_CellEndEdit);
            // 
            // colTaskEmployee
            // 
            this.colTaskEmployee.DataPropertyName = "TaskEmployeeID";
            this.colTaskEmployee.HeaderText = "ID";
            this.colTaskEmployee.Name = "colTaskEmployee";
            this.colTaskEmployee.ReadOnly = true;
            this.colTaskEmployee.Visible = false;
            // 
            // colEmployee
            // 
            this.colEmployee.DataPropertyName = "EmployeeID";
            this.colEmployee.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colEmployee.HeaderText = "Ответственный за исполнение";
            this.colEmployee.Name = "colEmployee";
            this.colEmployee.ReadOnly = true;
            this.colEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colEmployee.Width = 250;
            // 
            // colNotice
            // 
            this.colNotice.DataPropertyName = "NoticeID";
            this.colNotice.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colNotice.HeaderText = "Тип уведомления";
            this.colNotice.Name = "colNotice";
            this.colNotice.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colNotice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colNotice.Width = 250;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Процессы и задачи";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 337);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Исполнители";
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddEmployee.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEmployee.Image")));
            this.btnAddEmployee.Location = new System.Drawing.Point(630, 352);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(81, 23);
            this.btnAddEmployee.TabIndex = 53;
            this.btnAddEmployee.Text = "Добавить";
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnDeleteEmployee
            // 
            this.btnDeleteEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteEmployee.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteEmployee.Image")));
            this.btnDeleteEmployee.Location = new System.Drawing.Point(630, 381);
            this.btnDeleteEmployee.Name = "btnDeleteEmployee";
            this.btnDeleteEmployee.Size = new System.Drawing.Size(81, 23);
            this.btnDeleteEmployee.TabIndex = 52;
            this.btnDeleteEmployee.Text = "Удалить";
            // 
            // btnAddTask
            // 
            this.btnAddTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddTask.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTask.Image")));
            this.btnAddTask.Location = new System.Drawing.Point(630, 30);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(81, 23);
            this.btnAddTask.TabIndex = 51;
            this.btnAddTask.Text = "Добавить";
            this.btnAddTask.Click += new System.EventHandler(this.btnAdd_Medicine_Click);
            // 
            // btnDelTask
            // 
            this.btnDelTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelTask.Image = ((System.Drawing.Image)(resources.GetObject("btnDelTask.Image")));
            this.btnDelTask.Location = new System.Drawing.Point(630, 88);
            this.btnDelTask.Name = "btnDelTask";
            this.btnDelTask.Size = new System.Drawing.Size(81, 23);
            this.btnDelTask.TabIndex = 50;
            this.btnDelTask.Text = "Удалить";
            // 
            // btnEditTask
            // 
            this.btnEditTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditTask.Image = ((System.Drawing.Image)(resources.GetObject("btnEditTask.Image")));
            this.btnEditTask.Location = new System.Drawing.Point(630, 59);
            this.btnEditTask.Name = "btnEditTask";
            this.btnEditTask.Size = new System.Drawing.Size(81, 23);
            this.btnEditTask.TabIndex = 67;
            this.btnEditTask.Text = "Изменить";
            this.btnEditTask.Click += new System.EventHandler(this.exButton3_Click);
            // 
            // treeTask
            // 
            this.treeTask.Location = new System.Drawing.Point(9, 185);
            this.treeTask.Name = "treeTask";
            this.treeTask.Size = new System.Drawing.Size(613, 149);
            this.treeTask.TabIndex = 68;
            // 
            // frmTaskList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 478);
            this.Controls.Add(this.treeTask);
            this.Controls.Add(this.btnEditTask);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.btnDeleteEmployee);
            this.Controls.Add(this.btnAddTask);
            this.Controls.Add(this.btnDelTask);
            this.Controls.Add(this.dgEmployee);
            this.Controls.Add(this.dgTask);
            this.Name = "frmTaskList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задачи/исполнители/уведомления";
            this.Load += new System.EventHandler(this.frmTaskList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmployee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgTask;
        private System.Windows.Forms.DataGridView dgEmployee;
        private ExButton.NET.ExButton btnAddTask;
        private ExButton.NET.ExButton btnDelTask;
        private ExButton.NET.ExButton btnAddEmployee;
        private ExButton.NET.ExButton btnDeleteEmployee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public ExButton.NET.ExButton btnEditTask;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSystemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTaskEmployee;
        private System.Windows.Forms.DataGridViewComboBoxColumn colEmployee;
        private System.Windows.Forms.DataGridViewComboBoxColumn colNotice;
        private System.Windows.Forms.TreeView treeTask;
    }
}