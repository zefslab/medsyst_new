﻿namespace Medsyst
{
    partial class frmServiceCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeCat
            // 
            this.treeCat.LineColor = System.Drawing.Color.Black;
            this.treeCat.Size = new System.Drawing.Size(335, 554);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(453, 586);
            this.toolStripContainer1.Size = new System.Drawing.Size(453, 586);
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(11, 287);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(29, 266);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(11, 9);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(11, 38);
            // 
            // exButton4
            // 
            this.exButton4.Location = new System.Drawing.Point(11, 203);
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.Location = new System.Drawing.Point(11, 250);
            // 
            // exButton3
            // 
            this.exButton3.Location = new System.Drawing.Point(11, 174);
            // 
            // btnPast
            // 
            this.btnPast.Location = new System.Drawing.Point(11, 96);
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(11, 125);
            // 
            // btnCut
            // 
            this.btnCut.Location = new System.Drawing.Point(11, 67);
            // 
            // groupButtons
            // 
            this.groupButtons.Location = new System.Drawing.Point(341, 3);
            this.groupButtons.Size = new System.Drawing.Size(108, 318);
            // 
            // exButton2
            // 
            this.exButton2.Location = new System.Drawing.Point(77, 560);
            // 
            // exButton1
            // 
            this.exButton1.Location = new System.Drawing.Point(3, 560);
            // 
            // frmServiceCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 586);
            this.Name = "frmServiceCategory";
            this.Text = "Категории услуг";
            this.Load += new System.EventHandler(this.frmServiceCategory_Load);
            this.Controls.SetChildIndex(this.toolStripContainer1, 0);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupButtons.ResumeLayout(false);
            this.groupButtons.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion


    }
}