﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using System.Globalization;
using Medsyst.Dao.Repositories;
using Medsyst.Data.Dto.ReceiptOrder;
using Medsyst.Data.Entity;
using Medsyst.Data.Filter;
using Medsyst.Services.Bl;

namespace Medsyst
{
    public partial class frmReceiptOrderList : Form
    {
        ReceiptOrderCrudService _crudService = new ReceiptOrderCrudService();
        ProviderList providers = new ProviderList();

        IList<ReceiptOrderDto> _orders { get; set; } 
        
        

        public frmReceiptOrderList()
        {
            InitializeComponent();
        }
        // Загрузка формы
        private void frmRecieptOrders_Load(object sender, EventArgs e)
        {
            gridOrders.AutoGenerateColumns = false;

            providers.Load();
            cmbProviders.DataSource = null;
            cmbProviders.ValueMember = "Id";
            cmbProviders.DisplayMember = "Name";
            cmbProviders.DataSource = providers.Select(x=> new
            {
                Id = x.FirmID,
                Name = x.FirmN
            }).ToList();
            
            FilterApply();
        }

        // кнопка Перейти к приходному ордеру
        private void tsbGoto_Click(object sender, EventArgs e)
        {
            if (gridOrders.SelectedRows != null && gridOrders.SelectedRows.Count > 0)
            {
                frmReceiptOrder frm = new frmReceiptOrder();
                var cmd = new Command(CommandModes.Edit, _orders[gridOrders.SelectedRows[0].Index].Id);
                cmd.OnUpdate += FilterApply;
                frm.Tag = cmd;
                frm.MdiParent = MdiParent;
                frm.Show();
            }
        }

        // кнопка Добавить приходный ордер
        private void tsbAdd_Click(object sender, EventArgs e)
        {
            frmReceiptOrder frm = new frmReceiptOrder();
            var cmd1 = new Command(CommandModes.New);
            cmd1.OnUpdate += FilterApply;
            frm.MdiParent = MdiParent;
            frm.Tag = cmd1;
            frm.Show();
        }

       
        /// <summary> кнопка Применить фильтр
        /// Запалтин Е.Ф.
        /// 26.11.2011
        /// </summary>
        private void btnFilterApply_Click(object sender, EventArgs e)
        {
            FilterApply();
        }
        /// <summary>Применения фильтров для загрузки списка приходных ордеров
        /// Запалтин Е.Ф.
        /// </summary>
        private void FilterApply()
        {
            #region Подготовка фильтра
            var filter = new ReceiptOrderFilter()
            {
                ShowWithDeleted = chkShowDeleted.Checked
            };
            if (chkDate.Checked)
            {
                filter.BeginDate = dateTimePicker1.Value.Date;
                filter.EndDate = dateTimePicker2.Value.Date;
            }
            if (chkProviders.Checked)
            {
                filter.ProviderId = (int) cmbProviders.SelectedValue;
            }
            else
            {
                filter.ProviderId = null;
            }

            if (chkFinance.Checked)
            {
                filter.isBudget = rbIsBudget.Checked;
            }
            else
            {
                filter.isBudget = null;
            }
            #endregion
            
            gridOrders.DataSource = null;

            _orders = _crudService.Get(filter);

            gridOrders.DataSource = _orders;

            txtPrice.Text = _orders.Sum(x => x.Summ)
                                .ToString("C", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        /// <summary>Удаление приходного ордера
        /// Заплатин Е.Ф.
        /// 23.11.2011
        /// </summary>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (gridOrders.SelectedRows == null && gridOrders.SelectedRows.Count <= 0) return;

            ReceiptOrder r = new ReceiptOrder(true);
            r.DocumentID = _orders[gridOrders.SelectedRows[0].Index].Id;
            r.Load();
            if (r.Signed)
            {//Документ подписан поэтому его удаление невозможно
                MessageBox.Show("Невозможно удалить подписанный приходный ордер №" + r.Nbr,
                                "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if(MessageBox.Show(this, "Удаление приходного ордера","Вы хотите удалить приходный ордер?",
                    MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2)
                    == DialogResult.No)
                //Пользоватьель отказался от удаления    
                return;
                //Ползователь согласился с удалением
            if (!r.Delete(CommandDirective.MarkAsDeleted)) //Попытка удалить запись и если она окажется неудачной, то выодится сообщение об этом
                        MessageBox.Show("Не удалось удалить приходный ордер", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    FilterApply();//Загрузка списка приходных ордеров для их обновления

        }

        private void gridOrders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridOrders.SelectedRows != null && gridOrders.SelectedRows.Count > 0)
            {
                frmReceiptOrder frm = new frmReceiptOrder();
                var cmd = new Command(CommandModes.Edit, _orders[gridOrders.SelectedRows[0].Index].Id);
                cmd.OnUpdate += FilterApply;
                frm.Tag = cmd;
                frm.MdiParent = MdiParent;
                frm.Show();
                //LoadOrders();
            }
        }
        // кнопка Отменить фильтр
        private void btnFilterDelete_Click(object sender, EventArgs e)
        {
            chkProviders.Checked = chkDate.Checked = chkFinance.Checked = chkShowDeleted.Checked = false; 
            FilterApply();
        }

        // клик по чекбоксу фильтра даты
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker1.Enabled = dateTimePicker2.Enabled = chkDate.Checked;
        }

        private void chkProviders_CheckedChanged(object sender, EventArgs e)
        {
            cmbProviders.Enabled = chkProviders.Checked;
        }

        private void chkFinance_CheckedChanged(object sender, EventArgs e)
        {
            rbIsBudget.Enabled = rbIsNoBudget.Enabled = chkFinance.Checked;
        }
        /// <summary>Просмотр удаленных приходных ордеров
        /// Заплатин Е.Ф.
        /// 26.11.2011
        /// </summary>
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            FilterApply();
            colIsDeleted.Visible = tsbRestore.Visible = chkShowDeleted.Checked;
        }
        /// <summary>Восстановление удаленного приходного ордера
        /// Заплатин Е.Ф.
        /// 27.11.2011
        /// </summary>
        private void tsbRestore_Click(object sender, EventArgs e)
        {
            if (gridOrders.SelectedRows == null && gridOrders.SelectedRows.Count <= 0) return;

            //Операция применима только к удаленным приходным ордерам
            if (!(bool)gridOrders.SelectedRows[0].Cells["colDeleted"].Value) return;

            //Передать медикаменты на склад
            MedicamentInputList mis = new MedicamentInputList();
            mis.Load(_orders[gridOrders.SelectedRows[0].Index].Id);
            mis.Debit();
            
            //Снять отметку об удалении с приходного ордера
            //TODO перенести эту логику в сервис
            //_orders[gridOrders.SelectedRows[0].Index].isDeleted = false;
            //_orders[gridOrders.SelectedRows[0].Index].Update();
           
            FilterApply();
        }
    }
}
