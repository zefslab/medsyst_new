﻿namespace Medsyst
{
    partial class frmDemandByMedicament
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDemandByMedicament));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.rbtnNotBudget = new System.Windows.Forms.RadioButton();
            this.rbtnIsBudget = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnAddAllMedicine = new ExButton.NET.ExButton();
            this.btnDel_Medicine = new ExButton.NET.ExButton();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.cmbMembers = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSumm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.gridMeds = new System.Windows.Forms.DataGridView();
            this.colMedicamentByDemandID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMeasures = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCountResidue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNbr = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSign = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.tbPrint = new System.Windows.Forms.ToolStripButton();
            this.tbNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.rbtnNotBudget);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.rbtnIsBudget);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label7);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dtpDate);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.btnAddAllMedicine);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.btnDel_Medicine);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label8);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmbSeason);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.cmbMembers);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label6);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtSumm);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label5);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtComment);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.gridMeds);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label4);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label3);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.label2);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.txtNbr);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(595, 502);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(595, 527);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // rbtnNotBudget
            // 
            this.rbtnNotBudget.AutoSize = true;
            this.rbtnNotBudget.Location = new System.Drawing.Point(208, 61);
            this.rbtnNotBudget.Name = "rbtnNotBudget";
            this.rbtnNotBudget.Size = new System.Drawing.Size(102, 17);
            this.rbtnNotBudget.TabIndex = 149;
            this.rbtnNotBudget.Tag = "";
            this.rbtnNotBudget.Text = "внебюджетные";
            this.rbtnNotBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnIsBudget
            // 
            this.rbtnIsBudget.AutoSize = true;
            this.rbtnIsBudget.Checked = true;
            this.rbtnIsBudget.Location = new System.Drawing.Point(118, 61);
            this.rbtnIsBudget.Name = "rbtnIsBudget";
            this.rbtnIsBudget.Size = new System.Drawing.Size(84, 17);
            this.rbtnIsBudget.TabIndex = 148;
            this.rbtnIsBudget.TabStop = true;
            this.rbtnIsBudget.Tag = "demand.IsBudget";
            this.rbtnIsBudget.Text = "бюджетные";
            this.rbtnIsBudget.UseVisualStyleBackColor = true;
            this.rbtnIsBudget.CheckedChanged += new System.EventHandler(this.rbtnIsBudget_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 147;
            this.label7.Text = "Средства";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(184, 5);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(145, 20);
            this.dtpDate.TabIndex = 142;
            this.dtpDate.Tag = "demand.RegisterD";
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // btnAddAllMedicine
            // 
            this.btnAddAllMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAllMedicine.Image = null;
            this.btnAddAllMedicine.Location = new System.Drawing.Point(351, 88);
            this.btnAddAllMedicine.Name = "btnAddAllMedicine";
            this.btnAddAllMedicine.Size = new System.Drawing.Size(153, 23);
            this.btnAddAllMedicine.TabIndex = 141;
            this.btnAddAllMedicine.Text = "Добавить все назначенные";
            this.btnAddAllMedicine.Click += new System.EventHandler(this.btnAddAllMedicine_Click);
            // 
            // btnDel_Medicine
            // 
            this.btnDel_Medicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel_Medicine.Image = ((System.Drawing.Image)(resources.GetObject("btnDel_Medicine.Image")));
            this.btnDel_Medicine.Location = new System.Drawing.Point(510, 88);
            this.btnDel_Medicine.Name = "btnDel_Medicine";
            this.btnDel_Medicine.Size = new System.Drawing.Size(81, 23);
            this.btnDel_Medicine.TabIndex = 139;
            this.btnDel_Medicine.Text = "Удалить";
            this.btnDel_Medicine.Click += new System.EventHandler(this.btnDel_Medicine_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(335, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 16);
            this.label8.TabIndex = 138;
            this.label8.Text = "Сезон";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DisplayMember = "FullNbr";
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(384, 4);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(85, 21);
            this.cmbSeason.TabIndex = 137;
            this.cmbSeason.Tag = "demand.SeasonID";
            this.cmbSeason.ValueMember = "SeasonID";
            this.cmbSeason.SelectedIndexChanged += new System.EventHandler(this.cmbSeason_SelectedIndexChanged);
            // 
            // cmbMembers
            // 
            this.cmbMembers.DisplayMember = "Fullname";
            this.cmbMembers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMembers.FormattingEnabled = true;
            this.cmbMembers.Location = new System.Drawing.Point(118, 34);
            this.cmbMembers.Name = "cmbMembers";
            this.cmbMembers.Size = new System.Drawing.Size(351, 21);
            this.cmbMembers.TabIndex = 133;
            this.cmbMembers.Tag = "demand.EmployeeID";
            this.cmbMembers.ValueMember = "PersonID";
            this.cmbMembers.SelectedIndexChanged += new System.EventHandler(this.cmbMembers_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 387);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 13);
            this.label6.TabIndex = 131;
            this.label6.Text = "Примечание к требованию";
            // 
            // txtSumm
            // 
            this.txtSumm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSumm.Location = new System.Drawing.Point(466, 365);
            this.txtSumm.Name = "txtSumm";
            this.txtSumm.ReadOnly = true;
            this.txtSumm.Size = new System.Drawing.Size(125, 20);
            this.txtSumm.TabIndex = 130;
            this.txtSumm.Tag = "demand.SummMedicament";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(109, 368);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(351, 13);
            this.label5.TabIndex = 129;
            this.label5.Text = "Сумма стоимости медикаментов израсходованных по требованию:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 16);
            this.label1.TabIndex = 122;
            this.label1.Text = "№";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(7, 403);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(584, 96);
            this.txtComment.TabIndex = 132;
            this.txtComment.Tag = "demand.Comment";
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // gridMeds
            // 
            this.gridMeds.AllowUserToAddRows = false;
            this.gridMeds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMedicamentByDemandID,
            this.dataGridViewTextBoxColumn2,
            this.colMeasures,
            this.colCount,
            this.colCountResidue});
            this.gridMeds.Location = new System.Drawing.Point(7, 117);
            this.gridMeds.MultiSelect = false;
            this.gridMeds.Name = "gridMeds";
            this.gridMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridMeds.Size = new System.Drawing.Size(585, 242);
            this.gridMeds.TabIndex = 127;
            this.gridMeds.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMeds_CellEndEdit);
            // 
            // colMedicamentByDemandID
            // 
            this.colMedicamentByDemandID.DataPropertyName = "MedicamentByDemandID";
            this.colMedicamentByDemandID.HeaderText = "Код";
            this.colMedicamentByDemandID.Name = "colMedicamentByDemandID";
            this.colMedicamentByDemandID.ReadOnly = true;
            this.colMedicamentByDemandID.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MedicamentN";
            this.dataGridViewTextBoxColumn2.FillWeight = 300F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Наименование";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // colMeasures
            // 
            this.colMeasures.DataPropertyName = "MeasureID";
            this.colMeasures.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colMeasures.FillWeight = 50F;
            this.colMeasures.HeaderText = "Ед. изм.";
            this.colMeasures.Name = "colMeasures";
            this.colMeasures.ReadOnly = true;
            this.colMeasures.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colMeasures.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colMeasures.Width = 50;
            // 
            // colCount
            // 
            this.colCount.DataPropertyName = "CountGet";
            this.colCount.HeaderText = "Кол-во выписано";
            this.colCount.Name = "colCount";
            this.colCount.Width = 70;
            // 
            // colCountResidue
            // 
            this.colCountResidue.DataPropertyName = "CountResidue";
            this.colCountResidue.HeaderText = "Остаток";
            this.colCountResidue.Name = "colCountResidue";
            this.colCountResidue.ReadOnly = true;
            this.colCountResidue.Width = 70;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 128;
            this.label4.Text = "Лекарственные средства";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 126;
            this.label3.Text = "Затребовал (ФИО)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(160, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 16);
            this.label2.TabIndex = 124;
            this.label2.Text = "от";
            // 
            // txtNbr
            // 
            this.txtNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNbr.Location = new System.Drawing.Point(28, 3);
            this.txtNbr.Name = "txtNbr";
            this.txtNbr.Size = new System.Drawing.Size(118, 22);
            this.txtNbr.TabIndex = 123;
            this.txtNbr.Tag = "demand.Nbr";
            this.txtNbr.TextChanged += new System.EventHandler(this.txtNbr_TextChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSign,
            this.btnSave,
            this.tbPrint,
            this.tbNew});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(409, 25);
            this.toolStrip1.TabIndex = 101;
            // 
            // btnSign
            // 
            this.btnSign.Image = ((System.Drawing.Image)(resources.GetObject("btnSign.Image")));
            this.btnSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(86, 22);
            this.btnSign.Text = "Подписать";
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 22);
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPrint
            // 
            this.tbPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbPrint.Image")));
            this.tbPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbPrint.Name = "tbPrint";
            this.tbPrint.Size = new System.Drawing.Size(66, 22);
            this.tbPrint.Text = "Печать";
            this.tbPrint.Click += new System.EventHandler(this.tbPrint_Click);
            // 
            // tbNew
            // 
            this.tbNew.Image = ((System.Drawing.Image)(resources.GetObject("tbNew.Image")));
            this.tbNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbNew.Name = "tbNew";
            this.tbNew.Size = new System.Drawing.Size(129, 22);
            this.tbNew.Text = "Новое требование";
            this.tbNew.ToolTipText = "Новое требование";
            // 
            // frmDemandByMedicament
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 527);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmDemandByMedicament";
            this.Text = "Требование на медикаменты назначенные пациентам";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDemandByMedicament_FormClosing);
            this.Load += new System.EventHandler(this.frmDemandByMedicament_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeds)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.ComboBox cmbMembers;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSumm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.DataGridView gridMeds;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNbr;
        private System.Windows.Forms.ToolStrip toolStrip1;
        [sec(new Groups[] { Groups.Admin, Groups.Manager }, false)]
        public System.Windows.Forms.ToolStripButton btnSign;
        private System.Windows.Forms.ToolStripButton tbPrint;
        private System.Windows.Forms.ToolStripButton tbNew;
        private ExButton.NET.ExButton btnDel_Medicine;
        private ExButton.NET.ExButton btnAddAllMedicine;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMedicamentByDemandID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn colMeasures;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCountResidue;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.RadioButton rbtnNotBudget;
        private System.Windows.Forms.RadioButton rbtnIsBudget;
        private System.Windows.Forms.Label label7;
    }
}