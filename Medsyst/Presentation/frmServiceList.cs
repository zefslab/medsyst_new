﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmServiceList : Form
    {
        ///<summary>Список услуг</summary>
        ServiceList services = new ServiceList();
        ServiceOutputList excludeServices = new ServiceOutputList(); //Коллекция для сключения из списка уже выбранных услуг
        ///<summary>Список единиц измерения</summary>
        Measures measures = new Measures();
        ///<summary>Категории услуг</summary>
        ServiceCategoryList cats = new ServiceCategoryList();
        ///<summary>Текущая выбранная категория</summary>
        int CategoryID = 0;
        //Форматирование строк для отметки уже выбранных услуг
        DataGridViewCellStyle excludedElement = new DataGridViewCellStyle();
        //Форматирование строк для отметки услуг недоступных в связи с отсутсвием медикаментов
        DataGridViewCellStyle unavalebleElement = new DataGridViewCellStyle();
        CommandModes SelectMode; //Режим отображения списка услуг с отметкой небоступных
 

        public frmServiceList(Finance finance = Finance.Non, CommandModes selectMode = CommandModes.Select)
        {
            InitializeComponent();
            gridServices.AutoGenerateColumns = false; // отключение автоматической генерации колонок в таблице
            rbtnIsBudget.Checked = finance == Finance.Budget; //Устанавливается указатель источника финансирования
            SelectMode = selectMode;
        }

        /// <summary>Загрузка формы</summary>
        private void frmServices_Load(object sender, EventArgs e)
        {
            cmd = (Command)Tag;
            switch (cmd.Mode) //Управление видимостью кнопок выбора
            {
                case CommandModes.Select: //Единичный выбор
                    tsbSelectAndClose.Visible = true;
                    btnSelect.Visible = false;
                    break;
                case CommandModes.MultiSelect: // Множественный выбор
                    tsbSelectAndClose.Visible = btnSelect.Visible = true;
                    break;
                default: //Не используется для выбора
                    tsbSelectAndClose.Visible = btnSelect.Visible = false;
                    tsbtnClose.Visible = true;
                    break;
            }
            //Описание форматов для недоступных и исключенных из списка услуг
            excludedElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            unavalebleElement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            colAccessed.Visible = SelectMode == CommandModes.SelectWithUnavaleble; //Отображается столбик с доступностью, если форма открыта в режиме показа недоступных услуг
            measures.Load();
            colUnit.DisplayMember = "ShotName";
            colUnit.ValueMember = "MeasureID";
            colUnit.DataSource = measures;
            cats.Load();
            cats.FillTree(treeCat);
            treeCat.ExpandAll();
            if (cmd.obj != null)
            {//Передается коллекция объектов для исключения ее элементов из списка выбора
                excludeServices = (ServiceOutputList)cmd.obj;
            }
            LoadServices();
            Tag = null;//Очищается для того, чтобы в случае намеренного закрытия формы не возвращалось значение родительской форме
        }
 

        ///<summary>Загрузка списка услуг
        ///Заплатин Е.Ф.
        ///15.01.2012
        ///</summary>
        void LoadServices()
        {
            int curline = -1; // текущая выделенная строка
            if (gridServices.SelectedRows != null && gridServices.SelectedRows.Count > 0)
                curline = gridServices.SelectedRows[0].Index; // запоминаем индекс выделенной строки
            gridServices.DataSource = null; // Очистка элемета управления таблицы
            services = new ServiceList(); // Создание нового экземпляра списка, необходимо для избежания ошибки с вываливанием программы
            //Загрузка списка услуг с одновременным определением их доступности в зависимости от источника финансирования. ЗЕФ. 13.01.2012
            services.Load(CategoryID, (rbtnIsBudget.Checked ? Finance.Budget : Finance.ExtraBudget)); 
            gridServices.DataSource = services; // Соединение списка услуг с таблицей
            if (SelectMode == CommandModes.SelectWithUnavaleble)//Если требуется отмечать недоступные услуги
                foreach (DataGridViewRow row in gridServices.Rows)
                {
                    if (!(bool)row.Cells["colAccessed"].Value)
                        row.Cells["ServiceName"].Style = unavalebleElement;
                }
            if (excludeServices.Count > 0) //Имеется коллекция для отметки ее элементов в списке прейсурантных услуг как уже выбранных ранее
                ExcludeServices();
            //Выделение строки
            if (gridServices.Rows.Count >0)
                gridServices.Rows[0].Selected = false; // снятие автоматического выдления с первой строки
            if (gridServices.Rows.Count > curline && curline>=0)
                gridServices.Rows[curline].Selected = true;// установка выделения на нужную строку

        }

        /// <summary>Исключение из списка уже выбранных услуг
        /// Запалтин Е.Ф.
        /// 14.01.2012
        /// </summary>
        private void ExcludeServices()
        {
            foreach (DataGridViewRow row in gridServices.Rows)
            {
                //поиск услуги среди коллекции выбранных услуг
                ServiceOutput so = excludeServices.Find(delegate(ServiceOutput osrv) { return (osrv.ServiceID == (int)row.Cells["colServiceID"].Value); });
                if (so != null) //Нашлось вхождение услуги
                    row.Cells["ServiceName"].Style = excludedElement;//Нужно поменять стиль на недоступный
            }
        }
        ///<summary>Выделение категории услуг в дереве</summary>
        private void treeCat_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeCat.SelectedNode == null) return; // если категория не выделена то выход из функции
            CategoryID = int.Parse(treeCat.SelectedNode.Name); // Назначение текущей категории
            LoadServices();
        }

        ///<summary>Выбирается услуга для назначения ее пациенту
        /// </summary>
        private void btnSelect_Click(object sender,EventArgs e)
        {
            SelectService();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SelectService();
            Close();
        }
        /// <summary>
        /// Выбирается услуга
        /// </summary>
        private void SelectService()
        {
            //Проверка на выделение какой либо услуги в прейскуранте
            if (gridServices.SelectedRows == null || gridServices.SelectedRows.Count == 0)
            {//если селектор не установлен ни на одной услуге то выходим из процедуры
                MessageBox.Show("Небыло выбрано ни одной услуги. Повторите выбор, выделив предварительно услугу из списка.",
                                "Неверый выбор", MessageBoxButtons.OK, MessageBoxIcon.Stop);//предупредить пользователя о том, что выбор не произведен и только потом завершать процедуру
                return;
            }
            else //услуга выбрана из списка и можно производить ее назначение
            {
                Command с = new Command(CommandModes.Return, services[gridServices.SelectedRows[0].Index].ServiceID);
                //Через объектное свойство интерфейса возвращается выбранный источник  финансирования  
                //по которому нужно использовать медикаменты для оказания выбранной услуги
                с.obj = rbtnIsBudget.Checked?Finance.Budget:Finance.ExtraBudget; 
                //Передача вызвавшей форме интерфейса
                cmd.Form.Tag = с;
                cmd.OnUpdate();//Вызывается метод родительской формы для обновления ее содержимого
                //После выбора услуги нужно отметить ее как недоступную
                //Для этого в список уже выбранных услуг добавляется только что выбранная услуга
                ServiceOutput so = new ServiceOutput();
                so.ServiceID = services[gridServices.SelectedRows[0].Index].ServiceID;
                excludeServices.Add(so);
                //Повторно отмечаются уже выбранные услуги, как недоступные
                ExcludeServices();
            }
       }

        ///<summary>Кнопка вызова формы дерева категорий</summary>
        private void btnDelCategory_Click(object sender, EventArgs e)
        {
            frmServiceCategory frm = new frmServiceCategory();
            frm.ShowDialog();
            //Обновление дерева категорий услуг
            cats.Clear();
            cats.Load();
            cats.FillTree(treeCat);
            treeCat.ExpandAll();
        }

        ///<summary>Кнопка "Добавление категории"</summary>
        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Text = "Введите название категории:";
            if (frm.Show(this) == DialogResult.OK)
            {
                ServiceCategory m = new ServiceCategory();
                m.Text = frm.Value;
                m.ParentID = int.Parse(treeCat.SelectedNode.Name);
                m.Insert();
                cats.AddNode(treeCat, treeCat.SelectedNode, m.ID, m.Text);
                cats.Load();
            }
        }

        ///<summary>Кнопка "Добавить услугу"</summary>
        private void btnAdd_Medicine_Click(object sender, EventArgs e)
        {
            if (treeCat.SelectedNode == null) return;//Если не выделено ни одного элемента в дереве то выходит из функции
            frmService frm = new frmService();
            frm.Tag = new Command(CommandModes.New, int.Parse(treeCat.SelectedNode.Name));//Передача полю Tag объекта типа Command с параметрми режима и названия выбранной категории
            frm.ShowDialog(this);
            LoadServices();
        }

        ///<summary>Удаление услуги
        ///Заплатин Е.Ф.
        ///08.01.2012
        ///</summary>
        private void btnDelService_Click(object sender, EventArgs e)
        {
            if (gridServices.SelectedRows != null && gridServices.SelectedRows.Count > 0)
            {
                if (MessageBox.Show(this, "Вы действительно хотите удалить услугу из прейскуранта?", "Удаление услуги", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Услуга отмечается как удаленная. Так нужно делать потому, что возникают "дыры" в назначениях по которым была выписана удаляемая услуга
                    Service s = new Service();
                    s.ServiceID = services[gridServices.SelectedRows[0].Index].ServiceID;
                    if (s.Delete(CommandDirective.MarkAsDeleted))
                    {//Удаление прошло успешно
                        LoadServices();//Перезагрузка списка услуг
                    }
                }
            }
        }

        ///<summary>Кнопка "Изменить услугу"</summary>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (gridServices.SelectedRows != null && gridServices.SelectedRows.Count > 0)
            {
                int id = services[gridServices.SelectedRows[0].Index].ServiceID;
                frmService frm = new frmService();
                frm.Tag = new Command(CommandModes.Edit, id);
                frm.ShowDialog(this);
                LoadServices();
            }
        }

        ///<summary>Двойной клик по строке таблицы</summary>
        private void gridServices_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridServices.SelectedRows != null && gridServices.SelectedRows.Count > 0)
            {

                int id = services[gridServices.SelectedRows[0].Index].ServiceID;
                if ((cmd.Mode == CommandModes.Select)||(cmd.Mode == CommandModes.MultiSelect)) // если форма открыта в режиме выбора
                {
                    SelectService();//Выполняется метод выбора и назначения услуги
                }
                else // если форма открыта не в режиме выбора
                {
                    frmService frm = new frmService();
                    frm.Tag = new Command(CommandModes.Edit, id);
                    frm.ShowDialog(this);
                    LoadServices();
                }
            }
        }
        /// <summary>Обновление доступности услуг в зависимости от смены источника финансирования
        /// Запалтин Е.Ф.
        /// 13/01/2012
        /// </summary>
        private void rbtnIsBudget_CheckedChanged(object sender, EventArgs e)
        {
            LoadServices();
            rbtnNotBudget.Checked = !rbtnIsBudget.Checked;
        }
        /// <summary>Закрытие формы
        /// Запалтин Е.Ф.
        /// 14/01/2012
        /// </summary>
        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

  

    }
}
