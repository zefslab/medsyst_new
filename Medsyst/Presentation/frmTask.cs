﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmTask : Form
    {
        private Task task = new Task(true);

        public frmTask()
        {
            InitializeComponent();
        }
        ///<summary>Открытие формы
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void frmTask_Load(object sender, EventArgs e)
        {
            task.FormBinding("task", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                task.TaskID = cmd.Id;
                task.Load();
                task.WriteToForm();
            }
            
        }
        ///<summary>Сохранение задачи и закрытие формы
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void toolbtnSave_Click(object sender, EventArgs e)
        {
            task.ReadFromForm();
            if (cmd.Mode == CommandModes.New)
            {
                if (!task.Insert())
                    MessageBox.Show("Не удалось добавить задачу.");
            }
            else if (cmd.Mode == CommandModes.Edit)
            {
                task.Update();
            }
            Close();
        }
    }
}
