﻿namespace Medsyst
{
    partial class frmAmbulanceCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Общая");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Примечания");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("История болезни");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Платные услуги");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Направления");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Плательщик");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAmbulanceCard));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtNumber = new Medsyst.Class.Core.RegularTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtRegisterD = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDivision = new ExButton.NET.ExButton();
            this.lblDivision = new System.Windows.Forms.Label();
            this.cmbDivision = new System.Windows.Forms.ComboBox();
            this.btnAddGroup = new ExButton.NET.ExButton();
            this.btnDeleteGroup = new ExButton.NET.ExButton();
            this.lbGroup = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.btnPosAdd = new ExButton.NET.ExButton();
            this.btnPosDel = new ExButton.NET.ExButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbSexM = new System.Windows.Forms.RadioButton();
            this.rbSexF = new System.Windows.Forms.RadioButton();
            this.cmbPosition = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkPregnancy = new System.Windows.Forms.CheckBox();
            this.chkDonor = new System.Windows.Forms.CheckBox();
            this.chkKOV = new System.Windows.Forms.CheckBox();
            this.chkDisabiblity = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtClinicRegiter = new System.Windows.Forms.TextBox();
            this.txtDisabiblity = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.chkOrphan = new System.Windows.Forms.CheckBox();
            this.chkClinicRegiter = new System.Windows.Forms.CheckBox();
            this.cmbPatientCategory = new System.Windows.Forms.ComboBox();
            this.cmbFirm = new System.Windows.Forms.ComboBox();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblFirm = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtPhoneWork = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddCity = new ExButton.NET.ExButton();
            this.btnDelCity = new ExButton.NET.ExButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.gridAmbCard = new System.Windows.Forms.DataGridView();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolbtnAddInlless = new System.Windows.Forms.ToolStripButton();
            this.toolbtnGoToInlless = new System.Windows.Forms.ToolStripButton();
            this.toolbtnDeleteInlless = new System.Windows.Forms.ToolStripButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnAddMedicine = new ExButton.NET.ExButton();
            this.btnDelMedicine = new ExButton.NET.ExButton();
            this.btnAddService = new ExButton.NET.ExButton();
            this.btnDelService = new ExButton.NET.ExButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.colIhNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAmbCard)).BeginInit();
            this.toolStrip4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(782, 491);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(782, 518);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtNumber);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.textBox1);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.textBox2);
            this.splitContainer1.Panel1.Controls.Add(this.textBox3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.dtRegisterD);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(782, 491);
            this.splitContainer1.SplitterDistance = 66;
            this.splitContainer1.TabIndex = 2;
            // 
            // txtNumber
            // 
            this.txtNumber.Filter = Medsyst.Class.Core.FilterType.Integer;
            this.txtNumber.Location = new System.Drawing.Point(279, 5);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(98, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "AmbCard.Nbr";
            this.txtNumber.Text = "0";
            this.txtNumber.Value = 0D;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Фамилия";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(177, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Tag = "patient.LastName";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(380, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Отчество";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(195, 45);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(182, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Tag = "patient.Name";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(383, 45);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(208, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Tag = "patient.SurName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Имя";
            // 
            // dtRegisterD
            // 
            this.dtRegisterD.Location = new System.Drawing.Point(404, 5);
            this.dtRegisterD.Name = "dtRegisterD";
            this.dtRegisterD.Size = new System.Drawing.Size(133, 20);
            this.dtRegisterD.TabIndex = 2;
            this.dtRegisterD.Tag = "AmbCard.RegisterD";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(380, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "от";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(91, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Амбулаторная карта №";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer2.Size = new System.Drawing.Size(782, 421);
            this.splitContainer2.SplitterDistance = 127;
            this.splitContainer2.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode7.Name = "1";
            treeNode7.Text = "Общая";
            treeNode8.Name = "2";
            treeNode8.Text = "Примечания";
            treeNode9.Name = "3";
            treeNode9.Text = "История болезни";
            treeNode10.Name = "4";
            treeNode10.Text = "Платные услуги";
            treeNode11.Name = "5";
            treeNode11.Text = "Направления";
            treeNode12.Name = "6";
            treeNode12.Text = "Плательщик";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            this.treeView1.Size = new System.Drawing.Size(127, 421);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 421);
            this.tabControl1.TabIndex = 7;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(643, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общая";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDivision);
            this.groupBox1.Controls.Add(this.lblDivision);
            this.groupBox1.Controls.Add(this.cmbDivision);
            this.groupBox1.Controls.Add(this.btnAddGroup);
            this.groupBox1.Controls.Add(this.btnDeleteGroup);
            this.groupBox1.Controls.Add(this.lbGroup);
            this.groupBox1.Controls.Add(this.cmbGroup);
            this.groupBox1.Controls.Add(this.btnPosAdd);
            this.groupBox1.Controls.Add(this.btnPosDel);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.cmbPosition);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.cmbPatientCategory);
            this.groupBox1.Controls.Add(this.cmbFirm);
            this.groupBox1.Controls.Add(this.lblPosition);
            this.groupBox1.Controls.Add(this.lblFirm);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(584, 415);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Общая";
            // 
            // btnDivision
            // 
            this.btnDivision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDivision.Image = null;
            this.btnDivision.Location = new System.Drawing.Point(548, 86);
            this.btnDivision.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(27, 23);
            this.btnDivision.TabIndex = 76;
            this.btnDivision.Text = "...";
            this.btnDivision.Click += new System.EventHandler(this.exButton1_Click);
            // 
            // lblDivision
            // 
            this.lblDivision.AutoSize = true;
            this.lblDivision.Location = new System.Drawing.Point(296, 70);
            this.lblDivision.Name = "lblDivision";
            this.lblDivision.Size = new System.Drawing.Size(87, 13);
            this.lblDivision.TabIndex = 75;
            this.lblDivision.Text = "Подразделение";
            // 
            // cmbDivision
            // 
            this.cmbDivision.DisplayMember = "Name";
            this.cmbDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDivision.FormattingEnabled = true;
            this.cmbDivision.Location = new System.Drawing.Point(305, 86);
            this.cmbDivision.Name = "cmbDivision";
            this.cmbDivision.Size = new System.Drawing.Size(237, 21);
            this.cmbDivision.TabIndex = 74;
            this.cmbDivision.Tag = "patient.DivisionID";
            this.cmbDivision.ValueMember = "DivisionID";
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnAddGroup.Image")));
            this.btnAddGroup.Location = new System.Drawing.Point(515, 114);
            this.btnAddGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.Size = new System.Drawing.Size(27, 23);
            this.btnAddGroup.TabIndex = 73;
            this.btnAddGroup.Click += new System.EventHandler(this.btnAddGroup_Click);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGroup.Image")));
            this.btnDeleteGroup.Location = new System.Drawing.Point(548, 114);
            this.btnDeleteGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(27, 23);
            this.btnDeleteGroup.TabIndex = 72;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // lbGroup
            // 
            this.lbGroup.AutoSize = true;
            this.lbGroup.Location = new System.Drawing.Point(331, 117);
            this.lbGroup.Name = "lbGroup";
            this.lbGroup.Size = new System.Drawing.Size(42, 13);
            this.lbGroup.TabIndex = 71;
            this.lbGroup.Text = "Группа";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DisplayMember = "Name";
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(379, 114);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(130, 21);
            this.cmbGroup.TabIndex = 11;
            this.cmbGroup.Tag = "patient.StudentGroupID";
            this.cmbGroup.ValueMember = "GroupID";
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // btnPosAdd
            // 
            this.btnPosAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPosAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnPosAdd.Image")));
            this.btnPosAdd.Location = new System.Drawing.Point(265, 114);
            this.btnPosAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPosAdd.Name = "btnPosAdd";
            this.btnPosAdd.Size = new System.Drawing.Size(27, 23);
            this.btnPosAdd.TabIndex = 69;
            this.btnPosAdd.Click += new System.EventHandler(this.btnPosAdd_Click);
            // 
            // btnPosDel
            // 
            this.btnPosDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPosDel.Image = ((System.Drawing.Image)(resources.GetObject("btnPosDel.Image")));
            this.btnPosDel.Location = new System.Drawing.Point(298, 114);
            this.btnPosDel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPosDel.Name = "btnPosDel";
            this.btnPosDel.Size = new System.Drawing.Size(27, 23);
            this.btnPosDel.TabIndex = 68;
            this.btnPosDel.Click += new System.EventHandler(this.btnPosDel_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbSexM);
            this.groupBox5.Controls.Add(this.rbSexF);
            this.groupBox5.Location = new System.Drawing.Point(298, 9);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(83, 53);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Пол";
            // 
            // rbSexM
            // 
            this.rbSexM.AutoSize = true;
            this.rbSexM.Location = new System.Drawing.Point(6, 14);
            this.rbSexM.Name = "rbSexM";
            this.rbSexM.Size = new System.Drawing.Size(71, 17);
            this.rbSexM.TabIndex = 31;
            this.rbSexM.TabStop = true;
            this.rbSexM.Tag = "";
            this.rbSexM.Text = "Мужской";
            this.rbSexM.UseVisualStyleBackColor = true;
            // 
            // rbSexF
            // 
            this.rbSexF.AutoSize = true;
            this.rbSexF.Location = new System.Drawing.Point(6, 33);
            this.rbSexF.Name = "rbSexF";
            this.rbSexF.Size = new System.Drawing.Size(72, 17);
            this.rbSexF.TabIndex = 32;
            this.rbSexF.TabStop = true;
            this.rbSexF.Tag = "patient.Sex";
            this.rbSexF.Text = "Женский";
            this.rbSexF.UseVisualStyleBackColor = true;
            // 
            // cmbPosition
            // 
            this.cmbPosition.DisplayMember = "PositionN";
            this.cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPosition.FormattingEnabled = true;
            this.cmbPosition.Location = new System.Drawing.Point(95, 114);
            this.cmbPosition.Name = "cmbPosition";
            this.cmbPosition.Size = new System.Drawing.Size(168, 21);
            this.cmbPosition.TabIndex = 10;
            this.cmbPosition.Tag = "patient.PositionID";
            this.cmbPosition.ValueMember = "PositionID";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkPregnancy);
            this.groupBox4.Controls.Add(this.chkDonor);
            this.groupBox4.Controls.Add(this.chkKOV);
            this.groupBox4.Controls.Add(this.chkDisabiblity);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.txtClinicRegiter);
            this.groupBox4.Controls.Add(this.txtDisabiblity);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.chkOrphan);
            this.groupBox4.Controls.Add(this.chkClinicRegiter);
            this.groupBox4.Location = new System.Drawing.Point(6, 217);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(576, 107);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Студент";
            this.groupBox4.Visible = false;
            // 
            // chkPregnancy
            // 
            this.chkPregnancy.AutoSize = true;
            this.chkPregnancy.Location = new System.Drawing.Point(107, 87);
            this.chkPregnancy.Name = "chkPregnancy";
            this.chkPregnancy.Size = new System.Drawing.Size(100, 17);
            this.chkPregnancy.TabIndex = 34;
            this.chkPregnancy.Tag = "patient.Pregnancy";
            this.chkPregnancy.Text = "Беременность";
            this.chkPregnancy.UseVisualStyleBackColor = true;
            // 
            // chkDonor
            // 
            this.chkDonor.AutoSize = true;
            this.chkDonor.Location = new System.Drawing.Point(107, 64);
            this.chkDonor.Name = "chkDonor";
            this.chkDonor.Size = new System.Drawing.Size(59, 17);
            this.chkDonor.TabIndex = 33;
            this.chkDonor.Tag = "patient.Donor";
            this.chkDonor.Text = "Донор";
            this.chkDonor.UseVisualStyleBackColor = true;
            // 
            // chkKOV
            // 
            this.chkKOV.AutoSize = true;
            this.chkKOV.Location = new System.Drawing.Point(9, 87);
            this.chkKOV.Name = "chkKOV";
            this.chkKOV.Size = new System.Drawing.Size(48, 17);
            this.chkKOV.TabIndex = 32;
            this.chkKOV.Tag = "patient.KOV";
            this.chkKOV.Text = "КОВ";
            this.chkKOV.UseVisualStyleBackColor = true;
            // 
            // chkDisabiblity
            // 
            this.chkDisabiblity.AutoSize = true;
            this.chkDisabiblity.Location = new System.Drawing.Point(9, 41);
            this.chkDisabiblity.Name = "chkDisabiblity";
            this.chkDisabiblity.Size = new System.Drawing.Size(99, 17);
            this.chkDisabiblity.TabIndex = 19;
            this.chkDisabiblity.Tag = "patient.Disabiblity";
            this.chkDisabiblity.Text = "Инвалидность";
            this.chkDisabiblity.UseVisualStyleBackColor = true;
            this.chkDisabiblity.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(149, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(93, 13);
            this.label23.TabIndex = 28;
            this.label23.Text = "по  заболеванию";
            this.label23.Visible = false;
            // 
            // txtClinicRegiter
            // 
            this.txtClinicRegiter.Location = new System.Drawing.Point(248, 16);
            this.txtClinicRegiter.Name = "txtClinicRegiter";
            this.txtClinicRegiter.Size = new System.Drawing.Size(322, 20);
            this.txtClinicRegiter.TabIndex = 18;
            this.txtClinicRegiter.Tag = "patient.txtClinicRegiter";
            this.txtClinicRegiter.Visible = false;
            // 
            // txtDisabiblity
            // 
            this.txtDisabiblity.Location = new System.Drawing.Point(248, 39);
            this.txtDisabiblity.Name = "txtDisabiblity";
            this.txtDisabiblity.Size = new System.Drawing.Size(322, 20);
            this.txtDisabiblity.TabIndex = 20;
            this.txtDisabiblity.Tag = "patient.txtDisabiblity";
            this.txtDisabiblity.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(114, 42);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "по  заболеванию";
            this.label22.Visible = false;
            // 
            // chkOrphan
            // 
            this.chkOrphan.AutoSize = true;
            this.chkOrphan.Location = new System.Drawing.Point(9, 64);
            this.chkOrphan.Name = "chkOrphan";
            this.chkOrphan.Size = new System.Drawing.Size(62, 17);
            this.chkOrphan.TabIndex = 21;
            this.chkOrphan.Tag = "patient.Orphan";
            this.chkOrphan.Text = "Сирота";
            this.chkOrphan.UseVisualStyleBackColor = true;
            // 
            // chkClinicRegiter
            // 
            this.chkClinicRegiter.AutoSize = true;
            this.chkClinicRegiter.Location = new System.Drawing.Point(9, 18);
            this.chkClinicRegiter.Name = "chkClinicRegiter";
            this.chkClinicRegiter.Size = new System.Drawing.Size(134, 17);
            this.chkClinicRegiter.TabIndex = 17;
            this.chkClinicRegiter.Tag = "patient.ClinicRegiter";
            this.chkClinicRegiter.Text = "Состоит на \"Д\" учете";
            this.chkClinicRegiter.UseVisualStyleBackColor = true;
            this.chkClinicRegiter.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cmbPatientCategory
            // 
            this.cmbPatientCategory.DisplayMember = "CategoryN";
            this.cmbPatientCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPatientCategory.FormattingEnabled = true;
            this.cmbPatientCategory.Location = new System.Drawing.Point(96, 46);
            this.cmbPatientCategory.Name = "cmbPatientCategory";
            this.cmbPatientCategory.Size = new System.Drawing.Size(164, 21);
            this.cmbPatientCategory.TabIndex = 8;
            this.cmbPatientCategory.Tag = "patient.CategoryID";
            this.cmbPatientCategory.ValueMember = "CategoryID";
            // 
            // cmbFirm
            // 
            this.cmbFirm.DisplayMember = "FirmN";
            this.cmbFirm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFirm.FormattingEnabled = true;
            this.cmbFirm.Location = new System.Drawing.Point(15, 86);
            this.cmbFirm.Name = "cmbFirm";
            this.cmbFirm.Size = new System.Drawing.Size(278, 21);
            this.cmbFirm.TabIndex = 9;
            this.cmbFirm.Tag = "patient.FirmID";
            this.cmbFirm.ValueMember = "FirmID";
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(5, 117);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(65, 13);
            this.lblPosition.TabIndex = 16;
            this.lblPosition.Text = "Должность";
            // 
            // lblFirm
            // 
            this.lblFirm.AutoSize = true;
            this.lblFirm.Location = new System.Drawing.Point(6, 70);
            this.lblFirm.Name = "lblFirm";
            this.lblFirm.Size = new System.Drawing.Size(79, 13);
            this.lblFirm.TabIndex = 17;
            this.lblFirm.Text = "Место работы";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Дата рождения";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(98, 16);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(162, 20);
            this.dateTimePicker2.TabIndex = 6;
            this.dateTimePicker2.Tag = "patient.BirthD";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Категория";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtPhoneWork);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox12);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Location = new System.Drawing.Point(280, 144);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(302, 67);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Контактные данные";
            // 
            // txtPhoneWork
            // 
            this.txtPhoneWork.Location = new System.Drawing.Point(217, 14);
            this.txtPhoneWork.Name = "txtPhoneWork";
            this.txtPhoneWork.Size = new System.Drawing.Size(79, 20);
            this.txtPhoneWork.TabIndex = 15;
            this.txtPhoneWork.Tag = "patient.PhoneWork";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(158, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Тел. раб.";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(77, 37);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(219, 20);
            this.textBox12.TabIndex = 16;
            this.textBox12.Tag = "patient.PhoneMobile";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Тел. моб.";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(77, 13);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(75, 20);
            this.textBox11.TabIndex = 14;
            this.textBox11.Tag = "patient.PhoneHome";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Тел. дом.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbCity);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnAddCity);
            this.groupBox2.Controls.Add(this.btnDelCity);
            this.groupBox2.Location = new System.Drawing.Point(6, 144);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(268, 67);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Адрес";
            // 
            // cmbCity
            // 
            this.cmbCity.DisplayMember = "CityN";
            this.cmbCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(66, 13);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(126, 21);
            this.cmbCity.TabIndex = 12;
            this.cmbCity.Tag = "address.CityID";
            this.cmbCity.ValueMember = "CityID";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(66, 41);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(192, 20);
            this.textBox10.TabIndex = 13;
            this.textBox10.Tag = "address.Location";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 44);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Ул. дом";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Город";
            // 
            // btnAddCity
            // 
            this.btnAddCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCity.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCity.Image")));
            this.btnAddCity.Location = new System.Drawing.Point(198, 12);
            this.btnAddCity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddCity.Name = "btnAddCity";
            this.btnAddCity.Size = new System.Drawing.Size(27, 23);
            this.btnAddCity.TabIndex = 69;
            this.btnAddCity.Click += new System.EventHandler(this.btnAddCity_Click);
            // 
            // btnDelCity
            // 
            this.btnDelCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelCity.Image = ((System.Drawing.Image)(resources.GetObject("btnDelCity.Image")));
            this.btnDelCity.Location = new System.Drawing.Point(231, 12);
            this.btnDelCity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelCity.Name = "btnDelCity";
            this.btnDelCity.Size = new System.Drawing.Size(27, 23);
            this.btnDelCity.TabIndex = 68;
            this.btnDelCity.Click += new System.EventHandler(this.btnDelCity_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtComment);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(675, 399);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Примечания";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(4, 4);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(663, 386);
            this.txtComment.TabIndex = 0;
            this.txtComment.Tag = "AmbCard.Comment";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer3);
            this.tabPage3.Controls.Add(this.toolStrip4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(643, 395);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Истории болезней";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(3, 30);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.gridAmbCard);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.textBox15);
            this.splitContainer3.Panel2.Controls.Add(this.label18);
            this.splitContainer3.Panel2.Controls.Add(this.label19);
            this.splitContainer3.Panel2.Controls.Add(this.label17);
            this.splitContainer3.Panel2.Controls.Add(this.textBox14);
            this.splitContainer3.Panel2.Controls.Add(this.textBox7);
            this.splitContainer3.Size = new System.Drawing.Size(637, 362);
            this.splitContainer3.SplitterDistance = 402;
            this.splitContainer3.TabIndex = 5;
            // 
            // gridAmbCard
            // 
            this.gridAmbCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAmbCard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIhNbr,
            this.Column2,
            this.Column3});
            this.gridAmbCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAmbCard.Location = new System.Drawing.Point(0, 0);
            this.gridAmbCard.MultiSelect = false;
            this.gridAmbCard.Name = "gridAmbCard";
            this.gridAmbCard.ReadOnly = true;
            this.gridAmbCard.RowHeadersVisible = false;
            this.gridAmbCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridAmbCard.Size = new System.Drawing.Size(402, 362);
            this.gridAmbCard.TabIndex = 1;
            this.gridAmbCard.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAmbCard_CellContentDoubleClick);
            this.gridAmbCard.SelectionChanged += new System.EventHandler(this.gridAmbCard_SelectionChanged);
            // 
            // textBox15
            // 
            this.textBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox15.Location = new System.Drawing.Point(4, 248);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(222, 97);
            this.textBox15.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(195, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Диагноз санатория при поступлении";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 231);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(175, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Диагноз санатория при выписке";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(132, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Диагноз с места отбора";
            // 
            // textBox14
            // 
            this.textBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox14.Location = new System.Drawing.Point(4, 130);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(222, 97);
            this.textBox14.TabIndex = 3;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox7.Location = new System.Drawing.Point(4, 19);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(222, 91);
            this.textBox7.TabIndex = 1;
            // 
            // toolStrip4
            // 
            this.toolStrip4.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbtnAddInlless,
            this.toolbtnGoToInlless,
            this.toolbtnDeleteInlless});
            this.toolStrip4.Location = new System.Drawing.Point(3, 3);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(637, 27);
            this.toolStrip4.TabIndex = 4;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolbtnAddInlless
            // 
            this.toolbtnAddInlless.Image = ((System.Drawing.Image)(resources.GetObject("toolbtnAddInlless.Image")));
            this.toolbtnAddInlless.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnAddInlless.Name = "toolbtnAddInlless";
            this.toolbtnAddInlless.Size = new System.Drawing.Size(74, 24);
            this.toolbtnAddInlless.Text = "Создать";
            this.toolbtnAddInlless.Click += new System.EventHandler(this.toolStripButton13_Click);
            // 
            // toolbtnGoToInlless
            // 
            this.toolbtnGoToInlless.Image = ((System.Drawing.Image)(resources.GetObject("toolbtnGoToInlless.Image")));
            this.toolbtnGoToInlless.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnGoToInlless.Name = "toolbtnGoToInlless";
            this.toolbtnGoToInlless.Size = new System.Drawing.Size(78, 24);
            this.toolbtnGoToInlless.Text = "Перейти";
            this.toolbtnGoToInlless.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolbtnDeleteInlless
            // 
            this.toolbtnDeleteInlless.Image = ((System.Drawing.Image)(resources.GetObject("toolbtnDeleteInlless.Image")));
            this.toolbtnDeleteInlless.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnDeleteInlless.Name = "toolbtnDeleteInlless";
            this.toolbtnDeleteInlless.Size = new System.Drawing.Size(75, 24);
            this.toolbtnDeleteInlless.Text = "Удалить";
            this.toolbtnDeleteInlless.Click += new System.EventHandler(this.toolStripButton14_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnAddMedicine);
            this.tabPage4.Controls.Add(this.btnDelMedicine);
            this.tabPage4.Controls.Add(this.btnAddService);
            this.tabPage4.Controls.Add(this.btnDelService);
            this.tabPage4.Controls.Add(this.dataGridView2);
            this.tabPage4.Controls.Add(this.label74);
            this.tabPage4.Controls.Add(this.label73);
            this.tabPage4.Controls.Add(this.dataGridView1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage4.Size = new System.Drawing.Size(675, 399);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "Платные услуги";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnAddMedicine
            // 
            this.btnAddMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMedicine.Image")));
            this.btnAddMedicine.Location = new System.Drawing.Point(590, 203);
            this.btnAddMedicine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddMedicine.Name = "btnAddMedicine";
            this.btnAddMedicine.Size = new System.Drawing.Size(77, 23);
            this.btnAddMedicine.TabIndex = 69;
            this.btnAddMedicine.Text = "Добавить";
            // 
            // btnDelMedicine
            // 
            this.btnDelMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelMedicine.Image = ((System.Drawing.Image)(resources.GetObject("btnDelMedicine.Image")));
            this.btnDelMedicine.Location = new System.Drawing.Point(590, 232);
            this.btnDelMedicine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelMedicine.Name = "btnDelMedicine";
            this.btnDelMedicine.Size = new System.Drawing.Size(77, 23);
            this.btnDelMedicine.TabIndex = 68;
            this.btnDelMedicine.Text = "Удалить";
            // 
            // btnAddService
            // 
            this.btnAddService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddService.Image = ((System.Drawing.Image)(resources.GetObject("btnAddService.Image")));
            this.btnAddService.Location = new System.Drawing.Point(590, 17);
            this.btnAddService.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddService.Name = "btnAddService";
            this.btnAddService.Size = new System.Drawing.Size(77, 23);
            this.btnAddService.TabIndex = 67;
            this.btnAddService.Text = "Добавить";
            // 
            // btnDelService
            // 
            this.btnDelService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelService.Image = ((System.Drawing.Image)(resources.GetObject("btnDelService.Image")));
            this.btnDelService.Location = new System.Drawing.Point(590, 46);
            this.btnDelService.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelService.Name = "btnDelService";
            this.btnDelService.Size = new System.Drawing.Size(77, 23);
            this.btnDelService.TabIndex = 66;
            this.btnDelService.Text = "Удалить";
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGridView2.Location = new System.Drawing.Point(6, 203);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(578, 183);
            this.dataGridView2.TabIndex = 65;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "IHMedicament";
            this.dataGridViewTextBoxColumn1.FillWeight = 30F;
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MedicamentN";
            this.dataGridViewTextBoxColumn3.FillWeight = 270F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Наименование";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 270;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CountOutput";
            this.dataGridViewTextBoxColumn4.FillWeight = 30F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Кол-во";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 30;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MedicamentD";
            this.dataGridViewTextBoxColumn5.HeaderText = "Дата";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(8, 187);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(79, 13);
            this.label74.TabIndex = 64;
            this.label74.Text = "Медикаменты";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(5, 2);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(64, 13);
            this.label73.TabIndex = 63;
            this.label73.Text = "Процедуры";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn1,
            this.Column5,
            this.dataGridViewTextBoxColumn6,
            this.Column4});
            this.dataGridView1.Location = new System.Drawing.Point(6, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(578, 167);
            this.dataGridView1.TabIndex = 62;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 30F;
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 30;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.FillWeight = 150F;
            this.dataGridViewComboBoxColumn1.HeaderText = "Процедура";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 150;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Наименование";
            this.Column5.Name = "Column5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.FillWeight = 30F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Кол-во";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 30;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 90F;
            this.Column4.HeaderText = "Дата";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.textBox17);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(675, 399);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "Направления врача";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(4, 4);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(592, 389);
            this.textBox17.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage6.Size = new System.Drawing.Size(675, 399);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Плательщик";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolSave,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(146, 27);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolSave
            // 
            this.toolSave.Image = ((System.Drawing.Image)(resources.GetObject("toolSave.Image")));
            this.toolSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSave.Name = "toolSave";
            this.toolSave.Size = new System.Drawing.Size(69, 24);
            this.toolSave.Text = "Готово";
            this.toolSave.Click += new System.EventHandler(this.toolSave_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(65, 24);
            this.toolStripButton3.Text = "Новая";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(61, 22);
            this.toolStripButton2.Text = "Добавить";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(55, 22);
            this.toolStripButton7.Text = "Удалить";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(59, 22);
            this.toolStripButton8.Text = "Просмотр";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(61, 22);
            this.toolStripButton11.Text = "Добавить";
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(55, 22);
            this.toolStripButton12.Text = "Удалить";
            // 
            // colIhNbr
            // 
            this.colIhNbr.DataPropertyName = "ihNbr";
            this.colIhNbr.HeaderText = "№";
            this.colIhNbr.Name = "colIhNbr";
            this.colIhNbr.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "BeginD";
            this.Column2.HeaderText = "Дата нач.";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "EndD";
            this.Column3.HeaderText = "Дата оконч.";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // frmAmbulanceCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 518);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmAmbulanceCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Амбулаторная карта пациента";
            this.Load += new System.EventHandler(this.frmAmbulanceCard_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAmbCard)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        //Закладка ОБЩАЯ
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar })]
        public System.Windows.Forms.TabPage tabPage1;
        //Закладка ПРИМЕЧАНИЯ
        private System.Windows.Forms.TabPage tabPage2;
        //Закладка ИСТОРИИ БОЛЕЗНЕЙ
        private System.Windows.Forms.TabPage tabPage3;
        //Закладка ПЛАТНЫЕ УСЛУГИ
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage4;
        //Закладка НАПРАВЛЕНИЯ ВРАЧА
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Doctor })]
        public System.Windows.Forms.TabPage tabPage5;
        //Закладка ПЛАТЕЛЬЩИК
        private System.Windows.Forms.TabPage tabPage6;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar })]
        public System.Windows.Forms.DateTimePicker dtRegisterD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label1;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar })]
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar })]
        public System.Windows.Forms.TextBox textBox2;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar })]
        public System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolSave;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDisabiblity;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.Label lblFirm;
        private System.Windows.Forms.ComboBox cmbFirm;
        private System.Windows.Forms.TextBox txtPhoneWork;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbCity;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.DataGridView gridAmbCard;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ToolStrip toolStrip4;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar }, false)]
        public System.Windows.Forms.ToolStripButton toolbtnAddInlless;
        [sec(new Groups[] { Groups.Admin, Groups.Manager, Groups.Registrar }, false)]
        public System.Windows.Forms.ToolStripButton toolbtnDeleteInlless;
        private System.Windows.Forms.ComboBox cmbPatientCategory;
        private System.Windows.Forms.CheckBox chkDisabiblity;
        private System.Windows.Forms.CheckBox chkOrphan;
        private System.Windows.Forms.CheckBox chkClinicRegiter;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtClinicRegiter;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.ComboBox cmbPosition;
        private ExButton.NET.ExButton btnAddMedicine;
        private ExButton.NET.ExButton btnDelMedicine;
        private ExButton.NET.ExButton btnAddService;
        private ExButton.NET.ExButton btnDelService;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbSexM;
        private System.Windows.Forms.RadioButton rbSexF;
        private System.Windows.Forms.ToolStripButton toolbtnGoToInlless;
        private ExButton.NET.ExButton btnPosAdd;
        private ExButton.NET.ExButton btnPosDel;
        private ExButton.NET.ExButton btnAddGroup;
        private ExButton.NET.ExButton btnDeleteGroup;
        private System.Windows.Forms.Label lbGroup;
        private System.Windows.Forms.ComboBox cmbGroup;
        private ExButton.NET.ExButton btnAddCity;
        private ExButton.NET.ExButton btnDelCity;
        private System.Windows.Forms.TreeView treeView1;
        private Medsyst.Class.Core.RegularTextBox txtNumber;
        private System.Windows.Forms.CheckBox chkPregnancy;
        private System.Windows.Forms.CheckBox chkDonor;
        private System.Windows.Forms.CheckBox chkKOV;
        private ExButton.NET.ExButton btnDivision;
        private System.Windows.Forms.Label lblDivision;
        private System.Windows.Forms.ComboBox cmbDivision;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIhNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}