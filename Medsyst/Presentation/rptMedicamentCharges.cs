﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using Medsyst.Class;
using Medsyst.DataSet;
using Medsyst.DataSet.MedicamentChargesTableAdapters; //Необходимо для доступа к адаптерам набора данных



namespace Medsyst
{
    public partial class rptMedicamentCharges : Form
    {
        MCTableAdapter tableAdapter = new MCTableAdapter(); //Инициализируется адаптер с последующим присвоением занчений параметрам
        MedicamentCharges dataSet = new MedicamentCharges(); //создается экземпляр набора данных
        PatientCategoryList categoris = new PatientCategoryList();

        public rptMedicamentCharges()
        {
            InitializeComponent();
        }

        private void rptMedicamentCharges_Load(object sender, EventArgs e)
        {
            //Заполнение списков категорий пациентов 
            chlbCategory.Items.AddRange(categoris.ToArray());
            //Загружается отчет. Экзкмпляр отчета определен в файле rptMedicamentCharges.Desiner.cs
            rptMedicamentCharges1.Load(); 
            //Строка подключения
            tableAdapter.Connection.ConnectionString = DB.CS;
        } 
        
        private void button1_Click(object sender, EventArgs e)
        {
            chkCheckit.Enabled = true;
            ExecutReport();
        }
        /// <summary>Назначаются значания параметрам запроса и указывается запрос в качестве источника
        /// Заплатин Е.Ф.
        /// измененен 22.02.2013
        /// </summary>
        private void ExecutReport ()
        {
            //Спромотр выделеных категориий пациентов из списка. ЗЕФ. 22.02.2013
            //промотр выделеных категориий пациентов из списка. ЗЕФ. 22.02.2013
            String c = "";//Строка-список индексов обгалоченых категорий
            String cName = ""; //Строка-список наименований обгалоченых категорий
 
            foreach (int i in chlbCategory.CheckedIndices)
            {
                c += ", " + categoris[i].CategoryID.ToString();//Добавляется в список идентификаторов категорий
                cName += ", " + categoris[i].CategoryN;//Добавляется в список наименований категорий
            }
            if (c != "")
            {//Обгалочена хотя бы одна категория
                c = c.Remove(0, 2); //Удаление лишней запятой  спереди списка
                cName = cName.Remove(0, 2); //Удаление лишней запятой  спереди списка
            }
            else
                c = "0";//Не выделено ни одной категории



            //Заполнение данными таблицы набора данных с присвоением занчений параметрам 
            tableAdapter.Fill(dataSet.MC, dtpBeginPeriod.Value.ToString("dd.MM.yyyy"), //Изменил формат даты ЗЕФ с MM.dd.yyyy на dd.MM.yyyy
                              dtpEndPeriod.Value.ToString("dd.MM.yyyy"), 
                              (bool)rbtnBudget.Checked,
                              c); //Добавлена строка-список идентификаторов категорий пациентов 
            rptMedicamentCharges1.SetDataSource(dataSet); //вызываемый метод передает отчету заполненный экземпляр набора данных dataSet
            
            //Вспомогательные параметры для оформления заголовка отчета, указывающие даты начала и окончания отчетного периода
            rptMedicamentCharges1.SetParameterValue("DateBegin", dtpBeginPeriod.Value.ToString("dd.MM.yyyy"));
            rptMedicamentCharges1.SetParameterValue("DateEnd", dtpEndPeriod.Value.ToString("dd.MM.yyyy"));
            rptMedicamentCharges1.SetParameterValue("Средства", (bool)rbtnBudget.Checked);
            rptMedicamentCharges1.SetParameterValue("СтатьиРасходов", cName); 

            VeiwReport();
         }
        /// <summary>Настройка отображения отчета
        /// Заплатин Е.Ф.
        /// 12.07.2012
        /// </summary>
        private void VeiwReport()
        {
            rptMedicamentCharges1.ReportDefinition.ReportObjects["lblCheck"].ObjectFormat.EnableSuppress =
             rptMedicamentCharges1.ReportDefinition.ReportObjects["txtCheck"].ObjectFormat.EnableSuppress =
             rptMedicamentCharges1.ReportDefinition.ReportObjects["sumCheck"].ObjectFormat.EnableSuppress = !chkCheckit.Checked;
           
            crystalReportViewer.ReportSource = rptMedicamentCharges1; //Элементу формы просмотра отчетов передается загруженный отчет
             crystalReportViewer.Zoom(1);//Маштабирование предпросмотра отчета по ширине страницы
        }

       
        /// <summary>Регулирует отображение поля контрольной суммы в отчете
        /// Запалтин Е.Ф.
        /// 12.07.2012
        /// </summary>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            VeiwReport();
        }

    }
}
