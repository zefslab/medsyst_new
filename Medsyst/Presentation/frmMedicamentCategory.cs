﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;

namespace Medsyst
{
    public partial class frmMedicamentCategory : ftmpCategory
    {
        public frmMedicamentCategory()
        {
            InitializeComponent();
        }

        private void frmMedicamentCategory_Load(object sender, EventArgs e)
        {
            Categories = new MedicamentCategoryList(); // Присваиваем объект дерева категорий
            Category = new MedicamentCategory(); // Присваиваем объект категории
            LoadCategory(); // Загружаем базовую фунцию загрузки категорий
        }

    }
}
