﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    public partial class frmAmbulanceCard : Form
    {
        AmbulanceCard AmbCard = new AmbulanceCard(true);
        Patient patient = new Patient(true);
        PatientCategoryList pcl = new PatientCategoryList();
        DivisionList divisions = new DivisionList();
        PositionList positions = new PositionList();
        Data.Entity.PayerList firms = new Data.Entity.PayerList();
        Cities cities = new Cities();
        Address address = new Address(true);
        IllnessHistoryList histories = new IllnessHistoryList();
        FacultyList facults = new FacultyList();
        StudentGroupList sgroups = new StudentGroupList();
        
        public frmAmbulanceCard()
        {
            InitializeComponent();
        }
        private void frmAmbulanceCard_Load(object sender, EventArgs e)
        {
            //Загрузка списка категорий пациентов
            pcl.addnondefined = "не указана";
            pcl.Load();
            cmbPatientCategory.DataSource = pcl;

            facults.Load();
            gridAmbCard.AutoGenerateColumns = false; //Настройка грида со списком историй болезней
            //ЗЕФ:Важный элемент без которого элементы дерева не будут подсвечиваться при выделении закладок
            treeView1.HideSelection = false;
            //МЕВ: Привязка бизнес-объекта к форме 
            patient.FormBinding("patient", this);
            address = patient.Address;
            address.FormBinding("address", this);
            AmbCard.FormBinding("AmbCard", this);
          //Загрузка списка должностей
            positions.dbOrder = "PositionN";
            positions.addnondefined = "не указана";
            positions.Load();
            cmbPosition.DataSource = positions;
         
          //Загрузка списка мест работы
            firms.Load();
            cmbFirm.DataSource = firms;
          //Загрузка списка городов  
            cities.Load();
            cmbCity.DataSource = cities;
            //МЕВ: Индентификатор пациента передается из формы Registrature в форму AmbulaceCard
            //для идентификации пациента
            //Если форма открыта в режиме создания новой амбулаторной карты то присваивается номер последней + 1
            if (cmd.Mode == CommandModes.New)
            {
                cmbPatientCategory.SelectedValue = cmd.Id;
           
                //Присваивается номер последней + 1. В данной реализации не совсем правильное решение
                //Нужно ранжировать все амбулаторные карты по порядку номеров и брать последний. 
                //К нему и нужно прибавлять единицу
                txtNumber.Text = (AmbCard.GetCount("DocAmbulanceCard") + 1).ToString();
            }
            else  //форма открывается для редактирования или просмотра существующего пациента
            {
                //Передается идентификатор из родительской формы
                patient.PatientID = cmd.Id;
                //загружаются данные пациента
                patient.Load();
                LoadDivisions(patient.FirmID);
                patient.WriteToForm();
                address = patient.Address;
                address.WriteToForm();
                rbSexM.Checked = !patient.Sex;
                AmbCard.PatientID = patient.PatientID;
                AmbCard.Load();
                AmbCard.WriteToForm();
                LoadHistories();
                if (cmd.Mode == CommandModes.Veiw)
                {//форма открыта исключительно для просмотра, поэтому нужно исключить возможность внесения изменений в карту
                    toolStripContainer1.TopToolStripPanelVisible = false;
                }
            }

            ChangedIntervace();//В зависимости от категории пациента настраиваются некоторые управляющие элементы
            
            //Описания событий перенесены в эту часть, чтобы не срабатывать повторно при обновлении формы по технологии Марьенкова
            this.cmbFirm.SelectedIndexChanged += new System.EventHandler(this.cmbFirm_SelectedIndexChanged);
            this.cmbPatientCategory.SelectedIndexChanged += new System.EventHandler(this.cmbPatientCategory_SelectedIndexChanged);
            this.cmbPosition.SelectedIndexChanged += new System.EventHandler(this.cmbPosition_SelectedIndexChanged);

        }
        /// <summary>Загрузка подразделений
        /// Заплатин Е.Ф.
        /// 02.04.2013
        /// </summary>
        /// <param name="firmid">идентификатор фирмы</param>
        private void LoadDivisions(int firmid)
        {
            cmbDivision.DataSource = null;
            divisions.Load(firmid, CommandDirective.AddNonDefined, CommandDirective.NotLoadDeleted);
            cmbDivision.DataSource = divisions;
            cmbDivision.DisplayMember = "Name";//По непонятной причине свойство cmbDivision.DisplayMember сбрасывается после операции cmbDivision.DataSource = null;

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            EditHistory();
        }
        /// <summary>Добавление новой истории болезни</summary>
        /// Заплатин Е.Ф.
        /// 01.04.2011
        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            //TODO Проверить на предмет сохранения амулаторной карты и если она не сохранена, то сохранить
            //Эта проверка актуальна для только, что созданной амбулаторной карты
            frmIllnessHistory frm = new frmIllnessHistory();
            
            IllnessHistory ih = new IllnessHistory(true);
            //Загрузка шаблона истории болезни
            IllnessHistoryTemplateList ihts = new IllnessHistoryTemplateList();
            ihts.Load(patient.Sex);//Пока что загружаются все шаблоны с учетом пола пациента и выбирается один последний 
            if (ihts.Count > 0)
            {//Найден хотя бы один шаблон
                ih.DocumentID = ihts[0].DocumentID;
                ih.Load();
                //Инициализация свойств, которые не должны быть заполненными
                ih.DocumentID = ih.DocumentID_ = 0;//Если не обнулить, то наблюдения будут привязаны к новой истории болезни от и.б. шаблона
                ih.DoctorID = 0;
                ih.DiagOut = ih.DiagMain = ih.DiagAttend = ih.txtDoctorRecomendation = ih.txtResult = ih.Complaints = ih.HistiryOffIllness = 
                ih.txtHeightIn = ih.txtWeightIn = ih.txtDynamomentyIn = ih.txtSpirometriaIn =  ih.txtHeightOut = ih.txtWeightOut = 
                ih.txtDynamomentyOut = ih.txtSpirometriaOut = "";
                ih.DiagOutMKB = ih.DiagMainMKB = ih.DiagAttendMKB = 0;
                ih.SummMedicament = ih.SummService = 0; //Обнуление сумм назначеных медикаментов и услуг
            }
            else
            {//шаблонов для историй болезней нет

            }
            frm.ih = ih; //Присвоение свойств шаблона истории болезни вновь создаваемой истории болезни. Не красивый подход.
            
            cmd = new Command(CommandModes.New 
                                      ,AmbCard.PatientID //Т.к. создается новая история болезни, то передается идентификатор пациента
                                      ,this);
            cmd.OnUpdate = LoadHistories;
            frm.Tag = cmd;
            frm.Show();
        }

        private void treeView1_AfterSelect_1(object sender, TreeViewEventArgs e)
        {
            this.tabControl1.SelectTab(treeView1.SelectedNode.Index);
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            this.treeView1.SelectedNode = treeView1.Nodes.Find(e.TabPage.Name.Substring(7), false)[0];
        }

        //загрузка групп студентов
        void LoadStudentGroups()
        {
            if (cmbPosition.SelectedValue == null) return;//Если не указан факультет, то группы не загружаются
            int id = 0;
            try
            {
                id = (int)cmbPosition.SelectedValue;
            }
            catch (Exception)
            {
                return;
            }
            cmbGroup.DataSource = null;
            sgroups.Load(id);
            cmbGroup.DataSource = sgroups;
            cmbGroup.DisplayMember = "Name";
            cmbGroup.SelectedValue = patient.StudentGroupID;//Выделяется группа пациента
        }



        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClinicRegiter.Checked)
            {
                label23.Visible = true;
                txtClinicRegiter.Visible = true;}
            else {
                label23.Visible = false;
                txtClinicRegiter.Visible = false;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDisabiblity.Checked){
                label22.Visible = true;
                txtDisabiblity.Visible = true;}
            else{
                label22.Visible = false;
                txtDisabiblity.Visible = false;
            }

        }
       
        /// <summary>
        /// Кнопка "Готово" схраняет все введенные данные на форме в базу данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolSave_Click(object sender, EventArgs e)
        {
            if (cmd.Mode == CommandModes.New)//Если форма открыта в режиме добавления нового объекта
            {
                patient.ReadFromForm();
                address.ReadFromForm();
                AmbCard.ReadFromForm();
                patient.Address = address;
                //AmbCard.Load(); Закоментировал ЗЕФ 04.08.2011
                patient.Insert();
                AmbCard.PatientID = patient.PatientID;
                AmbCard.Insert();
                
            }
            else //если форма открыта в режиме редактирования существующего объекта
            {
                patient.ReadFromForm();
                address.ReadFromForm();
                AmbCard.ReadFromForm();
                patient.Address = address;
                patient.Update();
                AmbCard.Update();
            }
            cmd.UpdateParent();// Указание к обновлению списка вызывающей, родительской формы после изменения записи
            Close();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Tag = null;
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            txtNumber.Text = "0";
        }
        /// <summary>Заклужется коллекция истоий болезней для настоящей амбулаторной карты
        /// Заплатин Е.Ф. 
        /// 05.07.2011
        /// </summary>
        private void LoadHistories()
        {
            histories.Clear();
            histories.Load(AmbCard.DocumentID);
            gridAmbCard.DataSource = null;

            var seasons = new Seasons();
            seasons.Load();

            gridAmbCard.DataSource = histories.Join(seasons, h=> h.SeasonID, s=>s.SeasonID, (h,s)=> new
            {
                ihNbr = h.NbrIH + "/" + s.FullNbr,
                h.BeginD,
                h.EndD
            }).ToList();
        }
        /// <summary>Удаление истории болезни
        /// Заплатин Е.Ф.
        /// 05.07.2011
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            if (gridAmbCard.SelectedRows != null && gridAmbCard.SelectedRows.Count > 0)
            {
                IllnessHistory h = new IllnessHistory(true);
                h.DocumentID = histories[gridAmbCard.SelectedRows[0].Index].DocumentID;//получение идентификатора истории болезни из выделенного элемента списка
                //Проверка на наличие назначений сделанных по истории болезни
                if (h.CheckDestination())
                {
                    MessageBox.Show("По истории болезни сделаны назначения медикаментов и/или услуг. Удалить историю болезни не возможно. Сначала нужно удалить все сделанные назначения.",
                                    "Удаление истории болезни невозможно", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (MessageBox.Show(this, "Вы действительно хотите удалить историю болезни?", "Удаление истории болезни", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    h.Delete(CommandDirective.MarkAsDeleted);//Ставится отметка об удалении истории болезни
                    LoadHistories();
                }
            }

        }

        //26.04.10
        //Чирков Е.О.
        void EditHistory()
        {
            if (gridAmbCard.SelectedRows != null && gridAmbCard.SelectedRows.Count > 0)
            {
                frmIllnessHistory frm = new frmIllnessHistory();

                var cmd = new Command(CommandModes.Edit, histories[gridAmbCard.SelectedRows[0].Index].DocumentID, this);
                cmd.OnUpdate = LoadHistories;
                frm.Tag = cmd;
                //frm.IsNew = false;

                frm.ShowDialog(this);
                LoadHistories();
            }
        }

        private void gridAmbCard_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditHistory();
        }

        static string txtnum = "0";
        private void txtNumber_TextChanged(object sender, EventArgs e)
        {
            int l = txtNumber.SelectionStart;
            try
            {
                Convert.ToInt32(txtNumber.Text);
                txtnum = txtNumber.Text;
            }
            catch (FormatException)
            {
                txtNumber.Text = txtnum;
                txtNumber.SelectionStart = l;
            }
            
        }

        //11.04.10
        //Чирков Е.О.
        private void gridAmbCard_SelectionChanged(object sender, EventArgs e)
        {
            if (gridAmbCard.SelectedRows.Count <= 0) return;
            int i = gridAmbCard.SelectedRows[0].Index;
            textBox7.Text = histories[i].DiagOut;
            textBox14.Text = histories[i].DiagMain;
            textBox15.Text = histories[i].DiagAttend;
            //DiagOut //диагноз с места отбора
            //DiagMain //Диагноз основной при поступлении
            //DiagAttend //Диагноз сопутсвующий при поступлении
        }

        //26.04.10
        //Чирков Е.О.
        void AddFaculty()
        {
            InputBox frm = new InputBox();
            frm.Text = "Название факультета:";
            frm.Caption = "Добавить факультет";
            frm.Value = "";
            if (frm.Show(this) != DialogResult.OK) return;

            patient.StudentGroupID = Faculty.Add(frm.Value);
            
            cmbPosition.DataSource = null;
            facults.Load();
            cmbPosition.DataSource = facults;

            cmbPosition.DisplayMember = "Name";
            cmbPosition.ValueMember = "FacultyID";
            cmbPosition.SelectedValue = patient.StudentGroupID;
        }
        /// <summary>Выполняется двойная функция 
        /// в зависимости от принадлежности пациаетна к студенческой категории
        /// Заплатин Е.Ф.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPosAdd_Click(object sender, EventArgs e)
        {
            if ((int)cmbPatientCategory.SelectedValue == 1)
            {
                AddFaculty();
            }
            else
            {
                InputBox frm = new InputBox();
                frm.Text = "Название должности:";
                frm.Caption = "Добавить должность";
                frm.Value = "";
                if (frm.Show(this) != DialogResult.OK) return;
                Position pos = new Position(true);
                pos.PositionN = frm.Value;
                pos.Insert();
                pos.PositionID = pos.Identity("Position");

                cmbPosition.DataSource = null;
                positions = new PositionList();
                positions.Load();
                cmbPosition.DataSource = positions;
                cmbPosition.DisplayMember = "PositionN";
                cmbPosition.ValueMember = "PositionID";
                cmbPosition.SelectedValue = pos.PositionID;
                patient.PositionID = pos.PositionID;
            }
        }

        //26.04.10
        //Чирков Е.О.
        void DelFaculty()
        {
            if (DialogResult.No == MessageBox.Show(
                                                   "Удалить факультет/институт " +
                                                   cmbPosition.SelectedText + "?",
                                                   "Удаление факультета", MessageBoxButtons.YesNo)) return;

            Faculty fac = new Faculty();
            fac.FacultyID = (int)cmbPosition.SelectedValue;
            fac.Load();
            fac.Delete();
            patient.StudentGroupID = 0;

            cmbPosition.DataSource = null;
            facults = new FacultyList();
            facults.Load();
            cmbPosition.DataSource = facults;
            cmbPosition.DisplayMember = "Name";
            cmbPosition.ValueMember = "FacultyID";
        }

        private void btnPosDel_Click(object sender, EventArgs e)
        {
            if (cmbPosition.SelectedValue == null) return;

            if ((int)cmbPatientCategory.SelectedValue == 1)
            {
                DelFaculty();
            }
            else
            {
                if (DialogResult.No == MessageBox.Show(
                    "Удалить должность " +
                    cmbPosition.SelectedText +"?", 
                    "Удаление должности", MessageBoxButtons.YesNo)) return;
                Position pos = new Position(true);
                pos.PositionID = (int)cmbPosition.SelectedValue;
                pos.Load();
                pos.Delete();
                patient.PositionID = 0;

                cmbPosition.DataSource = null;
                positions = new PositionList();
                positions.Load();
                cmbPosition.DataSource = positions;
                cmbPosition.DisplayMember = "PositionN";
                cmbPosition.ValueMember = "PositionID";
            }
        }

       

        //26.04.10
        //Чикров Е.О.
        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            if (cmbPosition.SelectedValue == null)
            {
                MessageBox.Show("Выберите факультет", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            InputBox frm = new InputBox();
            frm.Text = "Название группы:";
            frm.Caption = "Добавить группу";
            frm.Value = "";
            if (frm.Show(this) != DialogResult.OK) return;
            //Добавление новой группы
            patient.StudentGroupID = StudentGroup.Add((int)cmbPosition.SelectedValue,
                                                       frm.Value);
            LoadStudentGroups();
        }

        //26.04.10
        //Чикров Е.О.
        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (cmbGroup.SelectedValue == null) return;
            if (DialogResult.No == MessageBox.Show(
                                                    "Удалить учебную группу " +
                                                    cmbGroup.SelectedText + "?",
                                                    "Удаление учебной группы", MessageBoxButtons.YesNo)) return;

            StudentGroup sg = new StudentGroup();
            sg.GroupID = (int)cmbGroup.SelectedValue;
            sg.Load();
            sg.Delete();
            LoadStudentGroups();
        }

        //27.06.10
        //Чирков Е.О.
        private void btnAddCity_Click(object sender, EventArgs e)
        {
            InputBox frm = new InputBox();
            frm.Text = "Название города:";
            frm.Caption = "Добавить город";
            if (frm.ShowDialog() != DialogResult.OK) return;
            if (frm.Value == "") return;
            //Проверка присутствия введенного названия в списке
            foreach (City cc in cities)
            {
                if (cc.CityN.ToLower() == frm.Value.ToLower())
                {
                    MessageBox.Show("Такой город уже есть", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbCity.SelectedValue = cc.CityID;
                    return;
                }
            }
            City c = new City(true);
            c.CityN = frm.Value;
            c.Insert();
            c.CityID = c.Identity("City");
            cities.Load();
            cmbCity.DataSource = null;
            cmbCity.DisplayMember = "CityN";
            cmbCity.ValueMember = "CityID";
            cmbCity.DataSource = cities;
            cmbCity.SelectedValue = c.CityID;
        }

        //27.06.10
        //Чирков Е.О.
        private void btnDelCity_Click(object sender, EventArgs e)
        {
            if (cmbCity.SelectedValue == null) return;
            if (DialogResult.No == MessageBox.Show(
                                        "Удалить город " +
                                        cmbCity.SelectedText + "?",
                                        "Удаление города", MessageBoxButtons.YesNo)) return;
            City c = new City();
            c.CityID = (int)cmbCity.SelectedValue;
            c.Load();
            c.Delete();
            cities.Load();
            cmbCity.DataSource = null;
            cmbCity.DisplayMember = "CityN";
            cmbCity.ValueMember = "CityID";
            cmbCity.DataSource = cities;
        }
        /// <summary>Выбор подразделения из формы списка подразделений
        /// Заплатин Е.Ф.
        /// 27.03.2013
        /// </summary>
        private void exButton1_Click(object sender, EventArgs e)
        {
            //передать форме идентификатор организации и заблокировать возможность изменения названия организации
            //разрешив только выбирать подразделение
            frmDivisionList frm = new frmDivisionList();
            Command cmd_div = new Command(CommandModes.Select, firms[cmbFirm.SelectedIndex].FirmID, this);
            cmd_div.type = typeof(Firm);//Передается тип идентификатора указанного в свойстве id интерфейса
            frm.Tag = cmd_div;
            DialogResult res = frm.ShowDialog();
            if (res != DialogResult.OK) return; //Выбор не произведен
            LoadDivisions((int)cmbFirm.SelectedValue);//Обновление списка в связи с возможным переименованием или добавлением подразделений
            //выделение выбранного подразделения в списке
            cmbDivision.SelectedValue = ((Command)frm.Tag).Id;
            

        }
        /// <summary>Смена списка подразделений после выбора новой фирмы
        /// Заплатин Е.Ф.
        /// 27.03.2013
        /// </summary>
        private void cmbFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDivisions((int)cmbFirm.SelectedValue);
        }
        /// <summary>В зависимости от выбранной категории пациента изменяется интерфейс формы. 
        /// 03.04.2013
        /// Заплатин Е.Ф.
        /// </summary>
        private void cmbPatientCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangedIntervace();
        }
        /// <summary>В зависимости от выбранной категории пациента изменяется интерфейс формы. 
        /// 26.04.10
        /// Чикров Е.О.
        /// </summary>
        private void ChangedIntervace()
        {
            if (cmbPatientCategory.SelectedValue == null) return; 
            bool IsStudent = (int)cmbPatientCategory.SelectedValue == 1;//Не очень хорошее решение привязанное к идентификатору категрии пациента
            //Настраивается видимость управляющих элементов в зависимости от категории пациента
            groupBox4.Visible = lbGroup.Visible = cmbGroup.Visible = btnAddGroup.Visible = btnDeleteGroup.Visible = IsStudent;

            if (IsStudent)  //ЗЕФ: Если пациент является студентом то меняются названия и назначения отдельных
            //управляющих элементов формы и загружаются новые значения
            {
                lblFirm.Text = "Место учебы";
                lblPosition.Text = "Факультет";

                cmbPosition.DataSource = facults;
                cmbPosition.DisplayMember = "Name";
                cmbPosition.ValueMember = "FacultyID";
                cmbPosition.Tag = null;

                StudentGroup sg = new StudentGroup();
                sg.GroupID = patient.StudentGroupID;
                sg.Load();
                cmbPosition.SelectedValue = sg.FacultyID;//Выделение факультета
                cmbGroup.Tag = "patient.StudentGroupID";
                LoadStudentGroups();//ЗЕФ. 03.01.2012 

                cmbDivision.Visible = btnDivision.Visible = lblDivision.Visible = false;//ЗЕФ. 09.04.2013
            }
            else
            {
                lblFirm.Text = "Место работы";
                lblPosition.Text = "Должность";

                cmbPosition.DataSource = null;
                cmbPosition.DataSource = positions; 
                cmbPosition.DisplayMember = "PositionN";
                cmbPosition.ValueMember = "PositionID";
                cmbPosition.Tag = "patient.PositionID";
                cmbPosition.SelectedValue = patient.PositionID;
                cmbDivision.Visible = btnDivision.Visible = lblDivision.Visible = true; //ЗЕФ. 09.04.2013
                cmbGroup.Tag = null;
            }
        }
        /// <summary>Обновление списка групп после смены факультета
        /// Заплатин Е.Ф.
        /// </summary>
        private void cmbPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
          if ((int)cmbPatientCategory.SelectedValue == 1) 
            LoadStudentGroups();
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
