﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Data.Entity;

namespace Medsyst
{
    /// <summary>
    /// Эта форма имеет двойное назначение:
    /// 1. Просмотр и управление складом медикаментов
    /// 2. Использование для выбора медикаментов чтобы сделать назначение или выписать расход
    /// </summary>
    public partial class frmSclad : Form
    {
        MedicamentOnScladList meds = new MedicamentOnScladList();
        /// <summary>Коллекция уже выбранных медикаментов для особой отметки ее в списке с целью исключения повторного выбора</summary>
        IList<int> excludeMedicineIds = new List<int>();
        MedicamentOnSclad medicament = new MedicamentOnSclad(true);
        MedicamentCategoryList cats = new MedicamentCategoryList();
        ReceiptOrder receiptorder = new ReceiptOrder(true); // Приходный ордер
        //Форматирование строк для отметки уже выбранных медикаментов
        DataGridViewCellStyle excludedElementStyle = new DataGridViewCellStyle();

        int CategoryID = 0;
        bool bFilter = false;//При загрузке формы фильтр игнорируется
        bool bSclad = true;//true - Полный склад для работы складского работника, false - урезанная версия склада для назначений докторами

        public frmSclad()
        {
            InitializeComponent();
        }
        /// <summary>Дополнительный конструктор призван ограничить выбор медикаментов по определенному источнику финансирования
        /// Заплатин Е.Ф.
        /// 03.09.2011
        /// </summary>
        /// <param name="onlyBudget">true- </param>
        public frmSclad(Finance f)
        {
            InitializeComponent();
            //Накладываются ограничения на представление медикаментов по определенному источник финансирования
            chkBudget.Checked = (f == Finance.Budget) || (f == Finance.Non);
            chkNoBudget.Checked = (f == Finance.ExtraBudget) || (f == Finance.Non);
            //chkBudget.Enabled = chkNoBudget.Enabled = (f == Finance.Non);
        }

        private void Sclad_Load(object sender, EventArgs e)
        {
            gridMeds.AutoGenerateColumns = false;

            switch (cmd.Mode) //Управление видимостью кнопок выбора
            {
                case CommandModes.Select: //Единичный выбор
                    tsbSelectAndClose.Visible = true;
                    btnSelect.Visible = false;
                    break;
                case CommandModes.MultiSelect: // Множественный выбор
                    tsbSelectAndClose.Visible = btnSelect.Visible = true;
                    break;
                default: //Не используется для выбора
                    tsbSelectAndClose.Visible = btnSelect.Visible = false;
                    tsbtnClose.Visible = true;
                    break;
            }
            
            //Описание стиля медикаментов, которые уже были выбраны и которые нет необходимости повторно выбирать
            excludedElementStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            receiptorder.FormBinding("ro", this);
            medicament.FormBinding("medicament",this);
            cats.Load();
            cats.FillTree(treeView1);
            treeView1.ExpandAll();
            if (cmd.obj != null)
            {//Передается коллекция идентификаторов складских медикаментов
             //для исключения ее элементов из списка выбора
                excludeMedicineIds = (IList<int>)cmd.obj;
            }
            LoadMeds();
            if (cmd.Mode == CommandModes.Select)
            {
                Column3.Visible = false;
                Column4.Visible = false;
            }
            if (cmd.Form is frmIllnessHistory)
            {
                chkZeroVisible.Visible = false;
                bSclad = false;
            }
        }

        /// <summary>Нажатие кнопки "Выбрать". Со склада выбирается лекарственное средство в расходные документы.
        /// Заплатин Е.Ф.
        /// 05.12.2012
        /// </summary>
        private void tsbSelect_Click(object sender, EventArgs e)
        {
            //Если не выделена хотябы одна запись списка
            if (gridMeds.SelectedRows.Count == 0) return;
            
            //В свойство вызывающей формы Tag передается интерфейс с  идентификатором выбранного медикамента
            cmd.Form.Tag = new Command(CommandModes.Return, meds[gridMeds.SelectedRows[0].Index].MedicamentOnScladID);
            cmd.UpdateParent();//Обновление родительской формы

            //После выбора медикамента нужно отметить его как недоступный
            //Для этого в список уже выбранных медикаментов добавляется только что выбранный
            excludeMedicineIds.Add(meds[gridMeds.SelectedRows[0].Index].MedicamentOnScladID);
            //Повторно отмечаются уже выбранные медикаменты, как недоступные
            ExcludeMedicines();
        }
        /// <summary>Выбрать медикамент и закрыть
        /// Заплатин Е.Ф.
        /// 05.12.2012
        /// </summary>
        private void tsbSelectAndClose_Click(object sender, EventArgs e)
        {
            tsbSelect_Click(sender, e);//Обработчик прерывания от нажатия на кнопку ВЫБРАТЬ
            Close();//Закрытие формы

        }
        /// <summary>Выбор медикамента двойным кликом мыши
        /// Заплатин Е.Ф.
        /// 05.12.2012
        /// </summary>
        private void gridMeds_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((cmd.Mode == CommandModes.Select)||(cmd.Mode == CommandModes.MultiSelect)) 
                tsbSelect_Click(sender, null);  //Обработчик прерывания от нажатия на кнопку ВЫБРАТЬ      
        }
       
        /// <summary>Исключение из списка уже выбранных услуг
        /// Запалтин Е.Ф.
        /// 15.01.2012
        /// </summary>
        private void ExcludeMedicines()
        {
            foreach (DataGridViewRow row in gridMeds.Rows)
            {
                if (excludeMedicineIds.Contains((int)row.Cells["colMedicamentOnScladID"].Value))
                {
                    //Нужно поменять стиль на недоступный
                    row.Cells["colMedicamentN"].Style = excludedElementStyle; 
                }
            }
        }
        /// <summary>
        /// Загрузка списка медикаментов без учета фильтра
        /// </summary>
        void LoadMeds()
        {
            meds = new MedicamentOnScladList();
            meds.dbOrder = "MedicamentN";
            gridMeds.AutoGenerateColumns = false;
            gridMeds.DataSource = null;
            if (bSclad)
                meds.LoadSclad(CategoryID, chkZeroVisible.Checked, chkBudget.Checked, chkNoBudget.Checked);
            else
                meds.Load(CategoryID, chkZeroVisible.Checked, chkBudget.Checked, chkNoBudget.Checked);
            gridMeds.DataSource = meds;
            ExcludeMedicines();
        }
        /// <summary>
        /// Загрузка списка медикаментов с учетом установок фильтра
        /// </summary>
        void LoadMeds(string price1,string price2)
        {
          //Проверка и преобразование фильтрующих критериев в числовые значения 
          decimal i, b;
          if (decimal.TryParse(price1, out i) & decimal.TryParse(price2, out b))
          {
            meds = new MedicamentOnScladList();
            
            gridMeds.DataSource = null;
            if (bSclad)
                meds.LoadSclad(CategoryID, i, b, chkZeroVisible.Checked, chkBudget.Checked, chkNoBudget.Checked);
            else
                meds.Load(CategoryID, i, b, chkZeroVisible.Checked, chkBudget.Checked, chkNoBudget.Checked);
            gridMeds.DataSource = meds;
          }
          else {
          //Сообщение о том что нужно проверить значения фильтров
              MessageBox.Show("Исправьте значения полей фильтра и повторите фильтрацию",
                               "Неверно установлен фильтр",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Stop);
          }
        }

        private void gridMeds_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            medicament.MedicamentOnScladID = meds[e.RowIndex].MedicamentOnScladID;//Уточняется идентификатор выбранного медикамента
            medicament.Load();//Загружаются сведения о выбранном медикаменте
            //Сбрасываются значения информационных полей приходного ордера, т.к. они обновляются по отдельной кнопке
            txtRONumber.Text = "";
            txtRODate.Text = "";
            medicament.WriteToForm();//Переносятся данные из свойств объекта в соответсвующие поля формы
        }

        // Кнопка "Применить фильтр"
        private void button5_Click(object sender, EventArgs e)
        {
            bFilter = true;//Устанавливает фильтр
            LoadMeds(textBox1.Text, textBox2.Text);
        }
        // Кнопка "Отменить фильтр"
        private void button1_Click(object sender, EventArgs e)
        {
            bFilter = false;//Отменяет фильтр
            LoadMeds();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode == null) return;
            CategoryID = int.Parse(treeView1.SelectedNode.Name);
            LoadMeds();
        }

        // Кнопка "перейим к приходному ордеру"
        private void btnGotoRecieptOrder_Click(object sender, EventArgs e)
        {
            if ((gridMeds.SelectedRows == null) || (gridMeds.SelectedRows.Count <= 0)) return;
            frmReceiptOrder frm = new frmReceiptOrder();
            frm.Tag = new Command(CommandModes.Edit, meds[gridMeds.SelectedRows[0].Index].InputDocumentID, this);
            frm.ShowDialog(this);
        }
        


        private void chkZeroVisible_CheckedChanged(object sender, EventArgs e)
        {
            if(bFilter)
                LoadMeds(textBox1.Text, textBox2.Text);
            else
                LoadMeds();
        }

        private void chkBudget_CheckedChanged(object sender, EventArgs e)
        {
            if (bFilter)
                LoadMeds(textBox1.Text, textBox2.Text);
            else
                LoadMeds();

        }

        private void chkNoBudget_CheckedChanged(object sender, EventArgs e)
        {
            if (bFilter)
                LoadMeds(textBox1.Text, textBox2.Text);
            else
                LoadMeds();

        }

        private void btnDetailsInput_Click(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows == null || gridMeds.SelectedRows.Count <= 0) return;
            receiptorder.DocumentID =  meds[gridMeds.SelectedRows[0].Index].InputDocumentID;
            receiptorder.Load();
            txtRONumber.Text = receiptorder.Nbr.ToString();
            txtRODate.Text = receiptorder.RegisterD.ToShortDateString();
            txtMedComment.Text = meds[gridMeds.SelectedRows[0].Index].CommentInput;
        }

        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }



 

  
    }
}
