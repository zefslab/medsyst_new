﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;
using Medsyst.Class.Forms;

namespace Medsyst
{
    public partial class frmMeasure : Form
    {
        Measures list = new Measures();

        public frmMeasure()
        {
            InitializeComponent();
        }

        ///<summary>Загрузка списка</summary>
        void LoadList()
        {
            gridList.AutoGenerateColumns = false;
            gridList.DataSource = null;
            list = new Measures();
            list.LoadSortByName();
            gridList.DataSource = list;
        }

        private void frmMeasure_Load(object sender, EventArgs e)
        {
            LoadList();
        }

        ///<summary>Кнопка "Добавить"</summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Measure o = new Measure();
            o.FullName = "Новая единица";
            o.Insert();
            LoadList();
        }

        ///<summary>Кнопка "Удалить"</summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridList.SelectedRows != null && gridList.SelectedRows.Count > 0)
            {
                list[gridList.SelectedRows[0].Index].Delete();
                LoadList();
            }
        }

        private void gridList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex == gridList.Columns["colFullName"].Index)
            {
                list[e.RowIndex].FullName = (string)gridList[e.ColumnIndex, e.RowIndex].Value;
                list[e.RowIndex].Update();
            }
            if (e.ColumnIndex == gridList.Columns["colShortName"].Index)
            {
                list[e.RowIndex].ShotName = (string)gridList[e.ColumnIndex, e.RowIndex].Value;
                list[e.RowIndex].Update();
            }
        }
    }
}
