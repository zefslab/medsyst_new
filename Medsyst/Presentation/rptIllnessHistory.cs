﻿using System;
using Medsyst.Class.Core;
using Medsyst.Class;
using CrystalDecisions.Shared; // Добавил ЗЕФ 09.09.2010
using Medsyst.DataSet;
using Medsyst.DataSet.datIllnessHistoryTableAdapters;

namespace Medsyst
{
    //ЗЕФ. Есть смысл проработать объединение двух форм отчетов книжки назначений и истории болезний 
    //в одну универсальную форму. Это типовые отчеты у которых даже передаваемые параметры одинаковые
    public partial class rptIllnessHistory : Form
    {


        public rptIllnessHistory()
        {
            InitializeComponent();
        }

        private void rptIllnessHistory_Load(object sender, EventArgs e)
        {
            cmd = (Command)Tag;//В интерфейсе передается идентификатор требования
            illnessHistory.Load(); //Загружается отчет
            datIllnessHistory DS_IH = new datIllnessHistory(); //Строго типизированный набор данных Истории болезни
            
            //Инициализация параметров хранимых процедур в адаптерах данных
            IHTableAdapter IHTA = new IHTableAdapter();//Адаптер истории болезни
            IHTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            IHTA.Fill(DS_IH.IH, cmd.Id);

            IHObservationTableAdapter ObservationTA = new IHObservationTableAdapter();//Наблюдения терапевта
            ObservationTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            ObservationTA.Fill(DS_IH.IHObservation, cmd.Id);

            IHObservationDermaTableAdapter ObservationDermaTA = new IHObservationDermaTableAdapter();//Наблюдения дерматоловенеролога
            ObservationDermaTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            ObservationDermaTA.Fill(DS_IH.IHObservationDerma, cmd.Id);

            PatientBookMedicamentTableAdapter MedicamentTA = new PatientBookMedicamentTableAdapter();//Назначенные медикаменты
            MedicamentTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            MedicamentTA.Fill(DS_IH.PatientBookMedicament, cmd.Id);

            PatientBookServiceTableAdapter ServiceTA = new PatientBookServiceTableAdapter();//Назначенные услуги
            ServiceTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            ServiceTA.Fill(DS_IH.PatientBookService, cmd.Id);

            IHDiagnosMKBOutTableAdapter MKBOutTA = new IHDiagnosMKBOutTableAdapter();//Диагнозы с места отбора
            MKBOutTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            MKBOutTA.Fill(DS_IH.IHDiagnosMKBOut, (int)MKBType.DiagOut, cmd.Id);

            IHDiagnosMKBAttendTableAdapter MKBAttendTA = new IHDiagnosMKBAttendTableAdapter();//Диагнозы при поступлении
            MKBAttendTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            MKBAttendTA.Fill(DS_IH.IHDiagnosMKBAttend, (int)MKBType.DiagAttend, cmd.Id);

            IHDiagnosMKBMainTableAdapter MKBMainTA = new IHDiagnosMKBMainTableAdapter();//Диагнозы заключительные
            MKBMainTA.Connection.ConnectionString = DB.CS;//Назначение строки подключения
            MKBMainTA.Fill(DS_IH.IHDiagnosMKBMain, (int)MKBType.DiagMain, cmd.Id);

            illnessHistory.SetDataSource(DS_IH); //Передача отчету экземпляра набора данных
            
            //SetParametrsValue(illnessHistory.ParameterFields);//Передача параметров отчета для загрузки их значений
            crystalReportViewer.ReportSource = illnessHistory; //Элементу формы просмотра отчетов передается загруженный отчет
        }

                    /// <summary>Установка значений всех параметров отчета
        /// Заплатин Е.Ф.
        /// </summary>
        private void SetParametrsValue(ParameterFields paramFilds)
        {
            foreach (ParameterField paramField in paramFilds)
            {
                //Осуществить поиск структуры в xml списке по наименованию параметра paramField.Name;
                //...

                //ParameterValues paramValues = new ParameterValues();//Создание коллекции значений параметра
                //paramField.CurrentValues = paramValues;//Присвоение коллекции значений параметра
                if (paramField.Name == "Param1")
                    paramField.DefaultValues.Add("Эксперимент удался!!!");//Добавление значения в коллекцию. Здесь нужно подставить значение полученное из xml списка
            }
        }
        //Пример добавления параметра взятый из справочника
        private ParameterFields AddParameter(string paramName,
          string paramValue, ParameterFields paramFields)
          {
          ParameterField paramField = new ParameterField();
          ParameterValues paramValues = new ParameterValues();
          paramField.AllowCustomValues = true;
          paramField.CurrentValues = paramValues;
          paramField.DefaultValueDisplayType = DefaultValueDisplayType.DescriptionAndValue;
          paramField.DefaultValues.Add(paramValue);
          paramField.EditMask = "----";
          paramField.EnableAllowMultipleValue = true;
          paramField.EnableNullValue = false;
          paramField.HasCurrentValue = false;
          paramField.Name = "New Parameter";
          paramField.ParameterValueType = ParameterValueKind.StringParameter;
          paramField.PromptingType = DiscreteOrRangeKind.RangeValue;
          paramField.PromptText = "Enter:";
          paramField.ReportName = "New Report";
          paramField.ReportParameterType = ParameterType.ReportParameter;
          paramFields.Add(paramField);
          return paramFields;
          }
        //Пример считывания значения параметра взятый из справочника
        private ParameterValue GetParamValue (string paramName)
        {
            ParameterFields paramFields = new ParameterFields();
            ParameterField paramField = new ParameterField();
            ParameterValues paramValues = new ParameterValues();
            ParameterValue paramValue;

            paramFields = crystalReportViewer.ParameterFieldInfo;
            paramField = paramFields[paramName];

            paramValues = paramField.CurrentValues;
            paramValue = paramValues[0];

            return paramValue;
        }

        private void illnessHistory_InitReport(object sender, EventArgs e)
        {

        }
     }
}
