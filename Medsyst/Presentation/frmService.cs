﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmService : Form
    {
        Service service = new Service(true);
        Measures measures = new Measures();
        ServiceCategory category = new ServiceCategory();
        MedicamentServiceList meds = new MedicamentServiceList();

        public frmService()
        {
            InitializeComponent();
        }

        private void frmService_Load(object sender, EventArgs e)
        {
            //Загрузка единиц измерения
            measures.Load();
            cmbMeasure.DataSource = measures;
            colMeasure.DisplayMember = "ShotName";
            colMeasure.ValueMember = "MeasureID";
            colMeasure.DataSource = measures;
            
            service.FormBinding("service", this);
            category.FormBinding("category", this);
            if (cmd.Mode == CommandModes.Edit)
            {
                service.ServiceID = cmd.Id;
                service.Load();
            }
            else if (cmd.Mode == CommandModes.New)
            {
                service.CategoryID = cmd.Id;
                service.ServiceN = "Новая услуга";
                service.Insert();
            }
            service.WriteToForm();
            category.ServiceCategoryID = service.CategoryID;
            category.Load();
            category.WriteToForm();
            LoadMeds();
            ReCalc();
        }

        ///<summary>Кнопка "Готово"</summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmd.Mode == CommandModes.Edit || cmd.Mode == CommandModes.New)
            {
                service.ReadFromForm();
                service.Update();
                cmd.Mode = CommandModes.Edit;
            }
            Close();
        }

        ///<summary>Событие закрытия формы</summary>
        private void frmService_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmd.Mode == CommandModes.New)
            {
                var dr = MessageBox.Show(this, "Сохранить добавленную услугу?", "Редактирование услуги", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    service.ReadFromForm();
                    service.Update();
                }
                else if (dr == DialogResult.No)
                {
                    service.Delete();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        ///<summary>Добавление медикамента в услугу
        ///
        ///</summary>
        private void btnAdd_Medicine_Click(object sender, EventArgs e)
        {
            frmMedicamentCatalog frm = new frmMedicamentCatalog();
            frm.Tag = new Command(CommandModes.Select);
            frm.ShowDialog(this);
            Command cmd = (Command)frm.Tag;
            if (cmd == null || cmd.Id <= 0) return;
            
            //Создание связи медикамента с услугой
            MedicamentService ms = new MedicamentService(true);
            ms.MedicamentID = cmd.Id;
            ms.ServiceID = service.ServiceID;
            if (!ms.Insert())
            {
                MessageBox.Show("Не удалось добавить медикамент в услугу");
                return;//ЗЕФ. 08.01.2012
            }
            LoadMeds();
            ReCalc();
        }
        
        ///<summary>Отметка лекарственного средства как удаленного
        ///Заплатин Е.Ф.
        ///Изменил 08.01.2012
        ///</summary>
        private void btnDelService_Click(object sender, EventArgs e)
        {
            bool res = true;
            if (gridMeds.SelectedRows != null && gridMeds.SelectedRows.Count > 0)
            {
                MedicamentService ms = new MedicamentService();
                ms = meds[gridMeds.SelectedRows[0].Index];
                if (ms.Deleted_)
                {//Медикамент уже был ранее отмечен как удаленный
                    if (DialogResult.Yes == MessageBox.Show("Удалить медикамент без возможности последующего восстановления?",
                                    "Удаление медикамента из услуги",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                       res = ms.Delete(CommandDirective.DeletePermanently);//Перманентное удаление без возможности восстановления
                }
                else
                {
                    res = ms.Delete(CommandDirective.MarkAsDeleted);//Отметка медикамента как удаленного с возможностью восстановления
                }
                if (res)
                {//Удаление или отметка об удалении прошла успешно. 
                    LoadMeds();
                }
            }
        }

        ///<summary>Загрузка медикаментов связанных с услугой</summary>
        private void LoadMeds()
        {
            gridMeds.AutoGenerateColumns = false;
            gridMeds.DataSource = null;
            meds = new MedicamentServiceList();
            bool showdeleted;
            if (chkShowDeleted.Checked)
                showdeleted = true;
            else
                showdeleted = false;
            meds.Load(service.ServiceID, showdeleted);//Загрузить медикаменты 
            gridMeds.DataSource = meds;
        }

        ///<summary>Редактирование количества медикаментов в таблице
        ///
        ///</summary>
        private void gridMeds_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            MedicamentService m = meds[e.RowIndex];
            if (e.ColumnIndex == gridMeds.Columns["colCount"].Index)
                m.Count = (int)gridMeds[e.ColumnIndex, e.RowIndex].Value;
            if (!m.Update())
                MessageBox.Show("Не удалось обновить медикамент из приходного ордера");
            else
            {
                gridMeds.Refresh();
                ReCalc();
            }
        }

        ///<summary>Расчет общей стоимости медикаментов</summary>
        void ReCalc()
        {
            decimal s = 0;
            for (int i = 0; i < meds.Count; i++)
            {
                s += meds[i].Cost * meds[i].Count;
            }
            textBox3.Text = s.ToString();
            Service srv = new Service();
            srv.ServiceID = service.ServiceID;
            if (srv.Load())
            {
                srv.PriceMed = s;
                service.PriceMed = s;
                if(!srv.Update())
                    MessageBox.Show("Не удалось сохранить стоимость медикаментов в услгу");
            }
            else
                MessageBox.Show("Не удалось сохранить стоимость медикаментов в услгу");
        }
        /// <summary>Отметка фильтра для показа/сокрытия удаленных медикаментов с последующей перезагрузкой медикаментов
        /// Заплатин Е.Ф.
        /// 11.07.2011
        /// </summary>
        private void chkShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            btnRestore.Enabled = chkShowDeleted.Checked;
            colDeleted.Visible = chkShowDeleted.Checked;
            LoadMeds();
        }
        /// <summary>Восстановление медикамента отмеченного как удаленный
        /// Заплатин Е.Ф.
        /// 11.07.2011
        /// </summary>
        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (gridMeds.SelectedRows != null && gridMeds.SelectedRows.Count > 0)
            {
                MedicamentService ms = new MedicamentService();
                ms = meds[gridMeds.SelectedRows[0].Index];
                ms.Deleted_ = false;
                if (ms.Update())
                {//Обновление по воссановления прошло успешно
                    LoadMeds();
                }
            }
        }
    }
}
