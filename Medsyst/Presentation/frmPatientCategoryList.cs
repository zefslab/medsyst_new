﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst;
using Medsyst.Class;

namespace Medsyst
{
    public partial class frmPatientCategoryList : ftmpCategoryList
    {

        DataGridViewComboBoxColumn colTypeID = new DataGridViewComboBoxColumn();
        DirectionTypeList dl = new DirectionTypeList();
        
        public frmPatientCategoryList()
        {
            InitializeComponent();
 
        }

        private void frmPatientCategories_Load(object sender, EventArgs e)
        {
         
            
            dgCategory.Columns.AddRange(new DataGridViewColumn[] { colTypeID });
            dl.addnondefined = "не определено";
            dl.Load();
            // Добавление дополнительного столбца с выпадающим списком типов направлений
            
            colTypeID.DataPropertyName = "TypeID";
            colTypeID.DisplayMember = "TypeN";
            colTypeID.DataSource = dl; // directionTypeBindingSource;
            colTypeID.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
            colTypeID.HeaderText = "Тип направления";
            colTypeID.Name = "colTypeID";
            colTypeID.Resizable = DataGridViewTriState.True;
            colTypeID.SortMode = DataGridViewColumnSortMode.Automatic;
            colTypeID.ValueMember = "TypeID";
            colTypeID.Width = 140;


            Categories = new PatientCategoryList(); // Присваиваем объект дерева категорий
            Category = new PatientCategory(); // Присваиваем объект категории
 
            LoadCategory(); // Загружаем базовую фунцию загрузки категорий



        }
    }
}
