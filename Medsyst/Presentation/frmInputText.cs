﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Medsyst
{
    public partial class frmInputText : Form
    {
        private bool _oneBtn = false;
        private int X = 0;
        [DefaultValue(false)][Browsable(true)]
        public bool OneButton {
            get
            {
                return _oneBtn;
            }
            set
            {
                if (_oneBtn == value) return;
                if (_oneBtn) //была одна, станет две
                {
                    //показать вторую
                    btnCancel.Visible = true;
                    //подвинуть первую
                    btnOK.Location = new Point(btnCancel.Left - btnOK.Width - X, btnOK.Top);
                }
                else //было две, станет одна
                {
                    //скрыть вторую
                    btnCancel.Visible = false;
                    //подвинуть первую
                    btnOK.Location = new Point(btnCancel.Right - btnOK.Width, btnOK.Top);
                }
                _oneBtn = value;
            }
        }
        public frmInputText()
        {
            InitializeComponent();
            X = btnCancel.Left - btnOK.Right;
        }
        public frmInputText(string label, string title , string text)
        {
            InitializeComponent();
            X = btnCancel.Left - btnOK.Right;
            Text = title;
            Value = text;
            LabelText = label;
        }

        public string LabelText
        {
            get { return labelText.Text; }
            set { labelText.Text = value; }
        }
        public string Value
        {
            get { return textBoxText.Text; }
            set { textBoxText.Text = value; }
        }
        public string OKText
        {
            get { return btnOK.Text; }
            set { btnOK.Text = value; }
        }
        public string CancelText
        {
            get { return btnCancel.Text; }
            set { btnCancel.Text = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
