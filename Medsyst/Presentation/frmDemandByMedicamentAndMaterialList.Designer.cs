﻿using Medsyst.DataSet;

namespace Medsyst
{
    partial class frmDemandByMedicamentAndMaterialList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDemandByMedicamentAndMaterialList));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkWithResidue = new System.Windows.Forms.CheckBox();
            this.chkSeason = new System.Windows.Forms.CheckBox();
            this.cmbSeasons = new System.Windows.Forms.ComboBox();
            this.chkDateEnd = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateEnd = new System.Windows.Forms.DateTimePicker();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.cmbEmployees = new System.Windows.Forms.ComboBox();
            this.chkProviders = new System.Windows.Forms.CheckBox();
            this.chkDateBegin = new System.Windows.Forms.CheckBox();
            this.btnFilterCancel = new System.Windows.Forms.Button();
            this.btnApplyFilter = new System.Windows.Forms.Button();
            this.dateBegin = new System.Windows.Forms.DateTimePicker();
            this.gridDemands = new System.Windows.Forms.DataGridView();
            this.colDemandID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colSumm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colSigned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIsDefected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAdd = new System.Windows.Forms.ToolStripButton();
            this.tsbGo = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbRecover = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.tsbSearch = new System.Windows.Forms.ToolStripButton();
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDemands)).BeginInit();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(937, 508);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(937, 533);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkWithResidue);
            this.splitContainer1.Panel1.Controls.Add(this.chkSeason);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeasons);
            this.splitContainer1.Panel1.Controls.Add(this.chkDateEnd);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dateEnd);
            this.splitContainer1.Panel1.Controls.Add(this.chkShowDeleted);
            this.splitContainer1.Panel1.Controls.Add(this.cmbEmployees);
            this.splitContainer1.Panel1.Controls.Add(this.chkProviders);
            this.splitContainer1.Panel1.Controls.Add(this.chkDateBegin);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilterCancel);
            this.splitContainer1.Panel1.Controls.Add(this.btnApplyFilter);
            this.splitContainer1.Panel1.Controls.Add(this.dateBegin);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridDemands);
            this.splitContainer1.Size = new System.Drawing.Size(937, 508);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkWithResidue
            // 
            this.chkWithResidue.AutoSize = true;
            this.chkWithResidue.Enabled = false;
            this.chkWithResidue.Location = new System.Drawing.Point(12, 180);
            this.chkWithResidue.Name = "chkWithResidue";
            this.chkWithResidue.Size = new System.Drawing.Size(156, 17);
            this.chkWithResidue.TabIndex = 39;
            this.chkWithResidue.Text = "С ненулевыми остатками";
            this.chkWithResidue.UseVisualStyleBackColor = true;
            // 
            // chkSeason
            // 
            this.chkSeason.AutoSize = true;
            this.chkSeason.Location = new System.Drawing.Point(12, 146);
            this.chkSeason.Name = "chkSeason";
            this.chkSeason.Size = new System.Drawing.Size(56, 17);
            this.chkSeason.TabIndex = 38;
            this.chkSeason.Text = "сезон";
            this.chkSeason.UseVisualStyleBackColor = true;
            this.chkSeason.CheckedChanged += new System.EventHandler(this.chkSeason_CheckedChanged);
            // 
            // cmbSeasons
            // 
            this.cmbSeasons.DisplayMember = "FullNbr";
            this.cmbSeasons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasons.Enabled = false;
            this.cmbSeasons.FormattingEnabled = true;
            this.cmbSeasons.Location = new System.Drawing.Point(68, 144);
            this.cmbSeasons.Name = "cmbSeasons";
            this.cmbSeasons.Size = new System.Drawing.Size(107, 21);
            this.cmbSeasons.TabIndex = 37;
            this.cmbSeasons.ValueMember = "SeasonID";
            // 
            // chkDateEnd
            // 
            this.chkDateEnd.AutoSize = true;
            this.chkDateEnd.Location = new System.Drawing.Point(12, 109);
            this.chkDateEnd.Name = "chkDateEnd";
            this.chkDateEnd.Size = new System.Drawing.Size(38, 17);
            this.chkDateEnd.TabIndex = 34;
            this.chkDateEnd.Text = "по";
            this.chkDateEnd.UseVisualStyleBackColor = true;
            this.chkDateEnd.CheckedChanged += new System.EventHandler(this.chkDateEnd_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Выписано";
            // 
            // dateEnd
            // 
            this.dateEnd.Enabled = false;
            this.dateEnd.Location = new System.Drawing.Point(55, 109);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Size = new System.Drawing.Size(120, 20);
            this.dateEnd.TabIndex = 31;
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(12, 203);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(147, 17);
            this.chkShowDeleted.TabIndex = 28;
            this.chkShowDeleted.Text = "Показывать удаленные";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // cmbEmployees
            // 
            this.cmbEmployees.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmployees.Enabled = false;
            this.cmbEmployees.FormattingEnabled = true;
            this.cmbEmployees.Location = new System.Drawing.Point(25, 31);
            this.cmbEmployees.Name = "cmbEmployees";
            this.cmbEmployees.Size = new System.Drawing.Size(150, 21);
            this.cmbEmployees.TabIndex = 27;
            // 
            // chkProviders
            // 
            this.chkProviders.AutoSize = true;
            this.chkProviders.Location = new System.Drawing.Point(12, 11);
            this.chkProviders.Name = "chkProviders";
            this.chkProviders.Size = new System.Drawing.Size(86, 17);
            this.chkProviders.TabIndex = 26;
            this.chkProviders.Text = "Затребовал";
            this.chkProviders.UseVisualStyleBackColor = true;
            this.chkProviders.CheckedChanged += new System.EventHandler(this.chkProviders_CheckedChanged);
            // 
            // chkDateBegin
            // 
            this.chkDateBegin.AutoSize = true;
            this.chkDateBegin.Location = new System.Drawing.Point(12, 87);
            this.chkDateBegin.Name = "chkDateBegin";
            this.chkDateBegin.Size = new System.Drawing.Size(32, 17);
            this.chkDateBegin.TabIndex = 25;
            this.chkDateBegin.Text = "с";
            this.chkDateBegin.UseVisualStyleBackColor = true;
            this.chkDateBegin.CheckedChanged += new System.EventHandler(this.chkDateBegin_CheckedChanged);
            // 
            // btnFilterCancel
            // 
            this.btnFilterCancel.Location = new System.Drawing.Point(12, 255);
            this.btnFilterCancel.Name = "btnFilterCancel";
            this.btnFilterCancel.Size = new System.Drawing.Size(163, 23);
            this.btnFilterCancel.TabIndex = 24;
            this.btnFilterCancel.Text = "Отменить фильтр";
            this.btnFilterCancel.UseVisualStyleBackColor = true;
            this.btnFilterCancel.Click += new System.EventHandler(this.btnFilterCancel_Click);
            // 
            // btnApplyFilter
            // 
            this.btnApplyFilter.Location = new System.Drawing.Point(12, 226);
            this.btnApplyFilter.Name = "btnApplyFilter";
            this.btnApplyFilter.Size = new System.Drawing.Size(163, 23);
            this.btnApplyFilter.TabIndex = 23;
            this.btnApplyFilter.Text = "Применить фильтр";
            this.btnApplyFilter.UseVisualStyleBackColor = true;
            this.btnApplyFilter.Click += new System.EventHandler(this.btnApplyFilter_Click);
            // 
            // dateBegin
            // 
            this.dateBegin.Enabled = false;
            this.dateBegin.Location = new System.Drawing.Point(55, 83);
            this.dateBegin.Name = "dateBegin";
            this.dateBegin.Size = new System.Drawing.Size(120, 20);
            this.dateBegin.TabIndex = 21;
            // 
            // gridDemands
            // 
            this.gridDemands.AllowUserToAddRows = false;
            this.gridDemands.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDemands.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDemands.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDemands.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDemandID,
            this.colDeleted,
            this.Column8,
            this.Column3,
            this.colName,
            this.colSumm,
            this.Column7,
            this.colSeason,
            this.colSigned,
            this.colIsDefected});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDemands.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridDemands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDemands.Location = new System.Drawing.Point(0, 0);
            this.gridDemands.MultiSelect = false;
            this.gridDemands.Name = "gridDemands";
            this.gridDemands.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDemands.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridDemands.RowHeadersVisible = false;
            this.gridDemands.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDemands.Size = new System.Drawing.Size(752, 508);
            this.gridDemands.TabIndex = 3;
            this.gridDemands.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDemands_CellDoubleClick);
            // 
            // colDemandID
            // 
            this.colDemandID.DataPropertyName = "DocumentID";
            this.colDemandID.HeaderText = "ID";
            this.colDemandID.Name = "colDemandID";
            this.colDemandID.ReadOnly = true;
            this.colDemandID.Visible = false;
            // 
            // colDeleted
            // 
            this.colDeleted.DataPropertyName = "Deleted";
            this.colDeleted.HeaderText = "Удален";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDeleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colDeleted.Visible = false;
            this.colDeleted.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Nbr";
            this.Column8.HeaderText = "Номер";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 50;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "RegisterD";
            this.Column3.HeaderText = "Дата";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 70;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "EmployeeID";
            this.colName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colName.HeaderText = "ФИО";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colName.Width = 120;
            // 
            // colSumm
            // 
            this.colSumm.DataPropertyName = "SummMedicament";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.colSumm.DefaultCellStyle = dataGridViewCellStyle2;
            this.colSumm.HeaderText = "Сумма";
            this.colSumm.Name = "colSumm";
            this.colSumm.ReadOnly = true;
            this.colSumm.Width = 80;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Comment";
            this.Column7.FillWeight = 300F;
            this.Column7.HeaderText = "Комментарий";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 180;
            // 
            // colSeason
            // 
            this.colSeason.DataPropertyName = "SeasonID";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colSeason.DefaultCellStyle = dataGridViewCellStyle3;
            this.colSeason.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colSeason.HeaderText = "Сезон";
            this.colSeason.Name = "colSeason";
            this.colSeason.ReadOnly = true;
            this.colSeason.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colSeason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colSeason.Width = 90;
            // 
            // colSigned
            // 
            this.colSigned.DataPropertyName = "Signed";
            this.colSigned.HeaderText = "Виза";
            this.colSigned.Name = "colSigned";
            this.colSigned.ReadOnly = true;
            this.colSigned.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colSigned.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colSigned.Width = 40;
            // 
            // colIsDefected
            // 
            this.colIsDefected.DataPropertyName = "IsDefected";
            this.colIsDefected.HeaderText = "Дефект.";
            this.colIsDefected.Name = "colIsDefected";
            this.colIsDefected.ReadOnly = true;
            this.colIsDefected.Visible = false;
            this.colIsDefected.Width = 60;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbGo,
            this.tsbDelete,
            this.tsbRecover,
            this.toolStripTextBox1,
            this.tsbSearch});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(399, 25);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAdd
            // 
            this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
            this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdd.Name = "tsbAdd";
            this.tsbAdd.Size = new System.Drawing.Size(79, 22);
            this.tsbAdd.Text = "Добавить";
            this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // tsbGo
            // 
            this.tsbGo.Image = ((System.Drawing.Image)(resources.GetObject("tsbGo.Image")));
            this.tsbGo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGo.Name = "tsbGo";
            this.tsbGo.Size = new System.Drawing.Size(74, 22);
            this.tsbGo.Text = "Перейти";
            this.tsbGo.Click += new System.EventHandler(this.tsbGo_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsbDelete.Image")));
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(71, 22);
            this.tsbDelete.Text = "Удалить";
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // tsbRecover
            // 
            this.tsbRecover.Enabled = false;
            this.tsbRecover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRecover.Name = "tsbRecover";
            this.tsbRecover.Size = new System.Drawing.Size(86, 22);
            this.tsbRecover.Text = "Восстановить";
            this.tsbRecover.Visible = false;
            this.tsbRecover.Click += new System.EventHandler(this.tsbRecover_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            // 
            // tsbSearch
            // 
            this.tsbSearch.Image = ((System.Drawing.Image)(resources.GetObject("tsbSearch.Image")));
            this.tsbSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearch.Name = "tsbSearch";
            this.tsbSearch.Size = new System.Drawing.Size(61, 22);
            this.tsbSearch.Text = "Найти";
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // frmDemandByMedicamentAndMaterialList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 533);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmDemandByMedicamentAndMaterialList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmDemandByMedicamentAndMaterialList";
            this.Load += new System.EventHandler(this.frmDemandByMedicamentAndMaterialList_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDemands)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox chkDateEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateEnd;
        public System.Windows.Forms.CheckBox chkShowDeleted;
        private System.Windows.Forms.ComboBox cmbEmployees;
        private System.Windows.Forms.CheckBox chkProviders;
        private System.Windows.Forms.CheckBox chkDateBegin;
        private System.Windows.Forms.Button btnFilterCancel;
        private System.Windows.Forms.Button btnApplyFilter;
        private System.Windows.Forms.DateTimePicker dateBegin;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAdd;
        private System.Windows.Forms.ToolStripButton tsbGo;
        public System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripButton tsbRecover;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton tsbSearch;
        private MedsystDataSet medsystDataSet;
        private System.Windows.Forms.CheckBox chkSeason;
        private System.Windows.Forms.ComboBox cmbSeasons;
        private System.Windows.Forms.DataGridView gridDemands;
        public System.Windows.Forms.CheckBox chkWithResidue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDemandID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewComboBoxColumn colSeason;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSigned;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colIsDefected;
    }
}