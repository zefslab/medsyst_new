﻿namespace Medsyst
{
    partial class frmDemandByServiceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDemandByServiceList));
            this.medsystDataSet = new Medsyst.DataSet.MedsystDataSet();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkWithResidue = new System.Windows.Forms.CheckBox();
            this.chkSeason = new System.Windows.Forms.CheckBox();
            this.cmbSeasons = new System.Windows.Forms.ComboBox();
            this.chkDateEnd = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateEnd = new System.Windows.Forms.DateTimePicker();
            this.chkServices = new System.Windows.Forms.CheckBox();
            this.cmbServices = new System.Windows.Forms.ComboBox();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.cmbEmployees = new System.Windows.Forms.ComboBox();
            this.chkProviders = new System.Windows.Forms.CheckBox();
            this.chkDateBegin = new System.Windows.Forms.CheckBox();
            this.btnFilterCancel = new System.Windows.Forms.Button();
            this.btnApplyFilter = new System.Windows.Forms.Button();
            this.dateBegin = new System.Windows.Forms.DateTimePicker();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gridDemands = new System.Windows.Forms.DataGridView();
            this.colDemandID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colService = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colSumm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colSign = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.rbtnNoBudget = new System.Windows.Forms.RadioButton();
            this.rbtnBudget = new System.Windows.Forms.RadioButton();
            this.btnServiceWOMedic = new System.Windows.Forms.Button();
            this.btnNewDemandOnService = new System.Windows.Forms.Button();
            this.gridServices = new System.Windows.Forms.DataGridView();
            this.colServiceID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAdd = new System.Windows.Forms.ToolStripButton();
            this.tsbGo = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbRecover = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.tsbSearch = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDemands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridServices)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // medsystDataSet
            // 
            this.medsystDataSet.DataSetName = "MedsystDataSet";
            this.medsystDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1020, 475);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1020, 500);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkWithResidue);
            this.splitContainer1.Panel1.Controls.Add(this.chkSeason);
            this.splitContainer1.Panel1.Controls.Add(this.cmbSeasons);
            this.splitContainer1.Panel1.Controls.Add(this.chkDateEnd);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dateEnd);
            this.splitContainer1.Panel1.Controls.Add(this.chkServices);
            this.splitContainer1.Panel1.Controls.Add(this.cmbServices);
            this.splitContainer1.Panel1.Controls.Add(this.chkShowDeleted);
            this.splitContainer1.Panel1.Controls.Add(this.cmbEmployees);
            this.splitContainer1.Panel1.Controls.Add(this.chkProviders);
            this.splitContainer1.Panel1.Controls.Add(this.chkDateBegin);
            this.splitContainer1.Panel1.Controls.Add(this.btnFilterCancel);
            this.splitContainer1.Panel1.Controls.Add(this.btnApplyFilter);
            this.splitContainer1.Panel1.Controls.Add(this.dateBegin);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 475);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkWithResidue
            // 
            this.chkWithResidue.AutoSize = true;
            this.chkWithResidue.Enabled = false;
            this.chkWithResidue.Location = new System.Drawing.Point(12, 235);
            this.chkWithResidue.Name = "chkWithResidue";
            this.chkWithResidue.Size = new System.Drawing.Size(156, 17);
            this.chkWithResidue.TabIndex = 37;
            this.chkWithResidue.Text = "С ненулевыми остатками";
            this.chkWithResidue.UseVisualStyleBackColor = true;
            // 
            // chkSeason
            // 
            this.chkSeason.AutoSize = true;
            this.chkSeason.Location = new System.Drawing.Point(12, 137);
            this.chkSeason.Name = "chkSeason";
            this.chkSeason.Size = new System.Drawing.Size(56, 17);
            this.chkSeason.TabIndex = 36;
            this.chkSeason.Text = "сезон";
            this.chkSeason.UseVisualStyleBackColor = true;
            this.chkSeason.CheckedChanged += new System.EventHandler(this.chkSeason_CheckedChanged);
            // 
            // cmbSeasons
            // 
            this.cmbSeasons.DisplayMember = "FullNbr";
            this.cmbSeasons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeasons.Enabled = false;
            this.cmbSeasons.FormattingEnabled = true;
            this.cmbSeasons.Location = new System.Drawing.Point(68, 135);
            this.cmbSeasons.Name = "cmbSeasons";
            this.cmbSeasons.Size = new System.Drawing.Size(107, 21);
            this.cmbSeasons.TabIndex = 35;
            this.cmbSeasons.ValueMember = "SeasonID";
            // 
            // chkDateEnd
            // 
            this.chkDateEnd.AutoSize = true;
            this.chkDateEnd.Location = new System.Drawing.Point(12, 109);
            this.chkDateEnd.Name = "chkDateEnd";
            this.chkDateEnd.Size = new System.Drawing.Size(38, 17);
            this.chkDateEnd.TabIndex = 34;
            this.chkDateEnd.Text = "по";
            this.chkDateEnd.UseVisualStyleBackColor = true;
            this.chkDateEnd.CheckedChanged += new System.EventHandler(this.chkDateEnd_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Выписано";
            // 
            // dateEnd
            // 
            this.dateEnd.Enabled = false;
            this.dateEnd.Location = new System.Drawing.Point(55, 109);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Size = new System.Drawing.Size(120, 20);
            this.dateEnd.TabIndex = 31;
            // 
            // chkServices
            // 
            this.chkServices.AutoSize = true;
            this.chkServices.Location = new System.Drawing.Point(12, 179);
            this.chkServices.Name = "chkServices";
            this.chkServices.Size = new System.Drawing.Size(62, 17);
            this.chkServices.TabIndex = 30;
            this.chkServices.Text = "Услуга";
            this.chkServices.UseVisualStyleBackColor = true;
            this.chkServices.CheckedChanged += new System.EventHandler(this.chkServices_CheckedChanged);
            // 
            // cmbServices
            // 
            this.cmbServices.DisplayMember = "ServiceN";
            this.cmbServices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbServices.Enabled = false;
            this.cmbServices.FormattingEnabled = true;
            this.cmbServices.Location = new System.Drawing.Point(25, 199);
            this.cmbServices.Name = "cmbServices";
            this.cmbServices.Size = new System.Drawing.Size(150, 21);
            this.cmbServices.TabIndex = 29;
            this.cmbServices.Tag = "";
            this.cmbServices.ValueMember = "ServiceID";
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(12, 270);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(147, 17);
            this.chkShowDeleted.TabIndex = 28;
            this.chkShowDeleted.Text = "Показывать удаленные";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.chkShowDeleted_CheckedChanged);
            // 
            // cmbEmployees
            // 
            this.cmbEmployees.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmployees.Enabled = false;
            this.cmbEmployees.FormattingEnabled = true;
            this.cmbEmployees.Location = new System.Drawing.Point(25, 31);
            this.cmbEmployees.Name = "cmbEmployees";
            this.cmbEmployees.Size = new System.Drawing.Size(150, 21);
            this.cmbEmployees.TabIndex = 27;
            // 
            // chkProviders
            // 
            this.chkProviders.AutoSize = true;
            this.chkProviders.Location = new System.Drawing.Point(12, 11);
            this.chkProviders.Name = "chkProviders";
            this.chkProviders.Size = new System.Drawing.Size(86, 17);
            this.chkProviders.TabIndex = 26;
            this.chkProviders.Text = "Затребовал";
            this.chkProviders.UseVisualStyleBackColor = true;
            this.chkProviders.CheckedChanged += new System.EventHandler(this.chkProviders_CheckedChanged);
            // 
            // chkDateBegin
            // 
            this.chkDateBegin.AutoSize = true;
            this.chkDateBegin.Location = new System.Drawing.Point(12, 87);
            this.chkDateBegin.Name = "chkDateBegin";
            this.chkDateBegin.Size = new System.Drawing.Size(32, 17);
            this.chkDateBegin.TabIndex = 25;
            this.chkDateBegin.Text = "с";
            this.chkDateBegin.UseVisualStyleBackColor = true;
            this.chkDateBegin.CheckedChanged += new System.EventHandler(this.chkDateBegin_CheckedChanged);
            // 
            // btnFilterCancel
            // 
            this.btnFilterCancel.Location = new System.Drawing.Point(12, 322);
            this.btnFilterCancel.Name = "btnFilterCancel";
            this.btnFilterCancel.Size = new System.Drawing.Size(163, 23);
            this.btnFilterCancel.TabIndex = 24;
            this.btnFilterCancel.Text = "Отменить фильтр";
            this.btnFilterCancel.UseVisualStyleBackColor = true;
            this.btnFilterCancel.Click += new System.EventHandler(this.btnFilterCancel_Click);
            // 
            // btnApplyFilter
            // 
            this.btnApplyFilter.Location = new System.Drawing.Point(12, 293);
            this.btnApplyFilter.Name = "btnApplyFilter";
            this.btnApplyFilter.Size = new System.Drawing.Size(163, 23);
            this.btnApplyFilter.TabIndex = 23;
            this.btnApplyFilter.Text = "Применить фильтр";
            this.btnApplyFilter.UseVisualStyleBackColor = true;
            this.btnApplyFilter.Click += new System.EventHandler(this.btnApplyFilter_Click);
            // 
            // dateBegin
            // 
            this.dateBegin.Enabled = false;
            this.dateBegin.Location = new System.Drawing.Point(55, 83);
            this.dateBegin.Name = "dateBegin";
            this.dateBegin.Size = new System.Drawing.Size(120, 20);
            this.dateBegin.TabIndex = 21;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gridDemands);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.rbtnNoBudget);
            this.splitContainer2.Panel2.Controls.Add(this.rbtnBudget);
            this.splitContainer2.Panel2.Controls.Add(this.btnServiceWOMedic);
            this.splitContainer2.Panel2.Controls.Add(this.btnNewDemandOnService);
            this.splitContainer2.Panel2.Controls.Add(this.gridServices);
            this.splitContainer2.Size = new System.Drawing.Size(835, 475);
            this.splitContainer2.SplitterDistance = 349;
            this.splitContainer2.TabIndex = 3;
            // 
            // gridDemands
            // 
            this.gridDemands.AllowUserToAddRows = false;
            this.gridDemands.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDemands.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDemands.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDemands.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDemandID,
            this.colDeleted,
            this.Column8,
            this.Column3,
            this.colService,
            this.colName,
            this.colSumm,
            this.Column7,
            this.colSeason,
            this.colSign});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDemands.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridDemands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDemands.Location = new System.Drawing.Point(0, 0);
            this.gridDemands.MultiSelect = false;
            this.gridDemands.Name = "gridDemands";
            this.gridDemands.ReadOnly = true;
            this.gridDemands.RowHeadersVisible = false;
            this.gridDemands.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDemands.Size = new System.Drawing.Size(835, 349);
            this.gridDemands.TabIndex = 2;
            this.gridDemands.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDemands_CellDoubleClick);
            // 
            // colDemandID
            // 
            this.colDemandID.DataPropertyName = "DocumentID";
            this.colDemandID.HeaderText = "ID";
            this.colDemandID.Name = "colDemandID";
            this.colDemandID.ReadOnly = true;
            this.colDemandID.Visible = false;
            // 
            // colDeleted
            // 
            this.colDeleted.DataPropertyName = "Deleted";
            this.colDeleted.HeaderText = "Удален";
            this.colDeleted.Name = "colDeleted";
            this.colDeleted.ReadOnly = true;
            this.colDeleted.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDeleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colDeleted.Visible = false;
            this.colDeleted.Width = 50;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Nbr";
            this.Column8.HeaderText = "Номер";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 45;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "RegisterD";
            this.Column3.HeaderText = "Дата";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // colService
            // 
            this.colService.DataPropertyName = "ServiceID";
            this.colService.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colService.HeaderText = "Услуга";
            this.colService.Name = "colService";
            this.colService.ReadOnly = true;
            this.colService.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colService.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colService.Width = 200;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "EmployeeID";
            this.colName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colName.HeaderText = "ФИО";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colName.Width = 110;
            // 
            // colSumm
            // 
            this.colSumm.DataPropertyName = "SummMedicament";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.colSumm.DefaultCellStyle = dataGridViewCellStyle2;
            this.colSumm.HeaderText = "Сумма";
            this.colSumm.Name = "colSumm";
            this.colSumm.ReadOnly = true;
            this.colSumm.Width = 80;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Comment";
            this.Column7.FillWeight = 300F;
            this.Column7.HeaderText = "Комментарий";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 180;
            // 
            // colSeason
            // 
            this.colSeason.DataPropertyName = "SeasonID";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.colSeason.DefaultCellStyle = dataGridViewCellStyle3;
            this.colSeason.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colSeason.HeaderText = "Сезон";
            this.colSeason.Name = "colSeason";
            this.colSeason.ReadOnly = true;
            this.colSeason.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colSeason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colSeason.Width = 60;
            // 
            // colSign
            // 
            this.colSign.DataPropertyName = "Signed";
            this.colSign.HeaderText = "Виза";
            this.colSign.Name = "colSign";
            this.colSign.ReadOnly = true;
            this.colSign.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colSign.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colSign.Width = 40;
            // 
            // rbtnNoBudget
            // 
            this.rbtnNoBudget.AutoSize = true;
            this.rbtnNoBudget.Location = new System.Drawing.Point(631, 28);
            this.rbtnNoBudget.Name = "rbtnNoBudget";
            this.rbtnNoBudget.Size = new System.Drawing.Size(83, 17);
            this.rbtnNoBudget.TabIndex = 8;
            this.rbtnNoBudget.Text = "Внебюджет";
            this.rbtnNoBudget.UseVisualStyleBackColor = true;
            // 
            // rbtnBudget
            // 
            this.rbtnBudget.AutoSize = true;
            this.rbtnBudget.Checked = true;
            this.rbtnBudget.Location = new System.Drawing.Point(631, 5);
            this.rbtnBudget.Name = "rbtnBudget";
            this.rbtnBudget.Size = new System.Drawing.Size(65, 17);
            this.rbtnBudget.TabIndex = 7;
            this.rbtnBudget.TabStop = true;
            this.rbtnBudget.Text = "Бюджет";
            this.rbtnBudget.UseVisualStyleBackColor = true;
            // 
            // btnServiceWOMedic
            // 
            this.btnServiceWOMedic.Location = new System.Drawing.Point(631, 50);
            this.btnServiceWOMedic.Name = "btnServiceWOMedic";
            this.btnServiceWOMedic.Size = new System.Drawing.Size(120, 30);
            this.btnServiceWOMedic.TabIndex = 5;
            this.btnServiceWOMedic.Text = "Обнаружить";
            this.btnServiceWOMedic.UseVisualStyleBackColor = true;
            this.btnServiceWOMedic.Click += new System.EventHandler(this.btnServiceWOMedic_Click);
            // 
            // btnNewDemandOnService
            // 
            this.btnNewDemandOnService.Location = new System.Drawing.Point(631, 82);
            this.btnNewDemandOnService.Name = "btnNewDemandOnService";
            this.btnNewDemandOnService.Size = new System.Drawing.Size(120, 34);
            this.btnNewDemandOnService.TabIndex = 4;
            this.btnNewDemandOnService.Text = "Создать требование для услуги";
            this.btnNewDemandOnService.UseVisualStyleBackColor = true;
            this.btnNewDemandOnService.Click += new System.EventHandler(this.btnNewDemandOnService_Click);
            // 
            // gridServices
            // 
            this.gridServices.AllowUserToAddRows = false;
            this.gridServices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridServices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridServices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colServiceID,
            this.colComment});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridServices.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridServices.Location = new System.Drawing.Point(0, 2);
            this.gridServices.MultiSelect = false;
            this.gridServices.Name = "gridServices";
            this.gridServices.ReadOnly = true;
            this.gridServices.RowHeadersVisible = false;
            this.gridServices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridServices.Size = new System.Drawing.Size(625, 117);
            this.gridServices.TabIndex = 3;
            // 
            // colServiceID
            // 
            this.colServiceID.DataPropertyName = "ServiceID";
            this.colServiceID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colServiceID.HeaderText = "Услуги не обеспеченные  медикаментами";
            this.colServiceID.Name = "colServiceID";
            this.colServiceID.ReadOnly = true;
            this.colServiceID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colServiceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colServiceID.Width = 300;
            // 
            // colComment
            // 
            this.colComment.DataPropertyName = "Comment";
            this.colComment.HeaderText = "Коментарий к услуге";
            this.colComment.Name = "colComment";
            this.colComment.ReadOnly = true;
            this.colComment.Width = 300;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbGo,
            this.tsbDelete,
            this.tsbRecover,
            this.toolStripTextBox1,
            this.tsbSearch});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(485, 25);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAdd
            // 
            this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
            this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdd.Name = "tsbAdd";
            this.tsbAdd.Size = new System.Drawing.Size(79, 22);
            this.tsbAdd.Text = "Добавить";
            this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // tsbGo
            // 
            this.tsbGo.Image = ((System.Drawing.Image)(resources.GetObject("tsbGo.Image")));
            this.tsbGo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGo.Name = "tsbGo";
            this.tsbGo.Size = new System.Drawing.Size(74, 22);
            this.tsbGo.Text = "Перейти";
            this.tsbGo.Click += new System.EventHandler(this.tsbGo_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsbDelete.Image")));
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(71, 22);
            this.tsbDelete.Text = "Удалить";
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // tsbRecover
            // 
            this.tsbRecover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRecover.Name = "tsbRecover";
            this.tsbRecover.Size = new System.Drawing.Size(86, 22);
            this.tsbRecover.Text = "Восстановить";
            this.tsbRecover.Click += new System.EventHandler(this.tsbRecover_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            // 
            // tsbSearch
            // 
            this.tsbSearch.Image = ((System.Drawing.Image)(resources.GetObject("tsbSearch.Image")));
            this.tsbSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearch.Name = "tsbSearch";
            this.tsbSearch.Size = new System.Drawing.Size(61, 22);
            this.tsbSearch.Text = "Найти";
            this.tsbSearch.Click += new System.EventHandler(this.tsbSearch_Click);
            // 
            // frmDemandByServiceList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 500);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "frmDemandByServiceList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.Text = "Требования на медикаменты реализуемые  через услуги";
            this.Load += new System.EventHandler(this.frmDemands_Load);
            ((System.ComponentModel.ISupportInitialize)(this.medsystDataSet)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDemands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridServices)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cmbEmployees;
        private System.Windows.Forms.CheckBox chkProviders;
        private System.Windows.Forms.CheckBox chkDateBegin;
        private System.Windows.Forms.Button btnFilterCancel;
        private System.Windows.Forms.Button btnApplyFilter;
        private System.Windows.Forms.DateTimePicker dateBegin;
        private System.Windows.Forms.DataGridView gridDemands;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAdd;
        private System.Windows.Forms.ToolStripButton tsbGo;
        private System.Windows.Forms.ToolStripButton tsbRecover;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton tsbSearch;
        private Medsyst.DataSet.MedsystDataSet medsystDataSet;
        [sec(new Groups[] { Groups.Admin, Groups.Stock, Groups.Manager })]
        public System.Windows.Forms.CheckBox chkShowDeleted;
        [sec(new Groups[] { Groups.Admin, Groups.Stock, Groups.Manager })]
        public System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.CheckBox chkServices;
        private System.Windows.Forms.ComboBox cmbServices;
        private System.Windows.Forms.CheckBox chkDateEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateEnd;
        private System.Windows.Forms.CheckBox chkSeason;
        private System.Windows.Forms.ComboBox cmbSeasons;
        public System.Windows.Forms.CheckBox chkWithResidue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDemandID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn colService;
        private System.Windows.Forms.DataGridViewComboBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewComboBoxColumn colSeason;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSign;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button btnNewDemandOnService;
        private System.Windows.Forms.DataGridView gridServices;
        private System.Windows.Forms.Button btnServiceWOMedic;
        private System.Windows.Forms.RadioButton rbtnNoBudget;
        private System.Windows.Forms.RadioButton rbtnBudget;
        private System.Windows.Forms.DataGridViewComboBoxColumn colServiceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colComment;
    }
}