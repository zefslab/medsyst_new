﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    public partial class frmTaskList : Form
    {
        /// <summary>Список задач. ЗЕФ. 04.07.2012</summary>
        private TaskList tl = new TaskList();
        /// <summary>Список сотрудников отвечающих за выполнение задачи. ЗЕФ. 04.07.2012</summary>
        private TaskEmployeeList tel = new TaskEmployeeList();
        /// <summary>Список сотрудников. ЗЕФ. 04.07.2012</summary>
        private MemberList ml = new MemberList();
        /// <summary>Список типов уведомлений. ЗЕФ. 04.07.2012</summary>
        private NoticeList nl = new NoticeList();


        public frmTaskList()
        {
            InitializeComponent();
        }
        ///<summary>Загрузка формы
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void frmTaskList_Load(object sender, EventArgs e)
        {
            dgTask.AutoGenerateColumns = false;
            dgEmployee.AutoGenerateColumns = false;
            //Запрет не отображение некоторых полей задач и кнопок редактирования задач, для обычных пользователей
            if (!Security.Access(Groups.Programmer))
            {
                colSystemName.Visible = colStatus.Visible = colButton.Visible = false;
                btnAddTask.Visible = btnDelTask.Visible = btnEditTask.Visible = false;
                colName.Width = 400;
            }
            ml.Load();
            colEmployee.DataSource = null;
            colEmployee.ValueMember = "PersonID_member";
            colEmployee.DisplayMember = "FIOBrif";
            colEmployee.DataSource = ml;
 
            nl.Load(CommandDirective.AddNonDefined);
            colNotice.DataSource = null;
            colNotice.ValueMember = "NoticeID";
            colNotice.DisplayMember = "NoticeN";
            colNotice.DataSource = nl;

            LoadTaskList();
            //LoadTaskEmployees();

        }
        ///<summary>Загрузка списка задач
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void LoadTaskList()
        {
            dgTask.DataSource = null;
            tl.Load();
            tl.FillTree(treeTask);
            dgTask.DataSource = tl;
        }
        ///<summary>Загрузка списка исполнителей задач
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void LoadTaskEmployees(Task task)
        {
            dgEmployee.DataSource = null;
            tel.Load(task);
            dgEmployee.DataSource = tel;
        }
        ///<summary>Добавление задачи
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void btnAdd_Medicine_Click(object sender, EventArgs e)
        {
            frmTask frm = new frmTask();
            frm.Tag = new Command(CommandModes.New);
            frm.ShowDialog(this);
            LoadTaskList();
        }
        ///<summary>Изменение задачи
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void exButton3_Click(object sender, EventArgs e)
        {
            if (dgTask.SelectedRows != null && dgTask.SelectedRows.Count > 0)
            {
                frmTask frm = new frmTask();
                frm.Tag = new Command(CommandModes.Edit, tl[dgTask.SelectedRows[0].Index].TaskID);
                frm.ShowDialog(this);
                LoadTaskList();
            }
        }
        ///<summary>Выбор исполнителя задачи
        ///Заплатин Е.Ф.
        ///04.07.2012</summary>
        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            if (dgTask.SelectedRows != null && dgTask.SelectedRows.Count > 0)
            {
                frmEmployeeList frm = new frmEmployeeList();
                Command emp_cmd = new Command(CommandModes.Select);
                frm.Tag = emp_cmd;
                frm.ShowDialog(this);
                emp_cmd = (Command)frm.Tag;
                if (emp_cmd.Mode != CommandModes.Return) return; //Выбор сотрудника не состоялся
                
                TaskEmployee te = new TaskEmployee(true);
                te.TaskID = tl[dgTask.SelectedRows[0].Index].TaskID;
                te.EmployeeID = emp_cmd.Id;
                te.Insert();
                LoadTaskEmployees(tl[dgTask.SelectedRows[0].Index]);
            }
        }

 
        private void dgEmployee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == dgEmployee.Columns["colNotice"].Index)
            {//Проверка на выделение хотя бы одной строки в списке сотрудников и редактирование колонки типа уведомления
                //Обновление записи сотрудника ответсвенного за выполнение задачи
                TaskEmployee te = new TaskEmployee();
                te.TaskEmployeeID = tel[e.RowIndex].TaskEmployeeID;
                te.Load();
                te.NoticeID = (int)dgEmployee.Rows[e.RowIndex].Cells["colNotice"].Value;
                te.Update();
            }
        }

        private void dgTask_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           if (dgTask.SelectedRows.Count > 0)
               LoadTaskEmployees(tl[dgTask.SelectedRows[0].Index]);

        }


      
    }
}
