﻿using Medsyst.Class;
using Medsyst.Class.Core;

namespace Medsyst
{
    partial class frmDivisionList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeDivision = new System.Windows.Forms.TreeView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolbtnSelect = new System.Windows.Forms.ToolStripButton();
            this.btnPaste = new ExButton.NET.ExButton();
            this.btnCut = new ExButton.NET.ExButton();
            this.btnAdd = new ExButton.NET.ExButton();
            this.btnEdit = new ExButton.NET.ExButton();
            this.btnDelete = new ExButton.NET.ExButton();
            this.menuAdd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.какПодузелToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.вКореньToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbFirmList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRestore = new ExButton.NET.ExButton();
            this.chkShowDeleted = new System.Windows.Forms.CheckBox();
            this.toolStrip1.SuspendLayout();
            this.menuAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeDivision
            // 
            this.treeDivision.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeDivision.HideSelection = false;
            this.treeDivision.Location = new System.Drawing.Point(12, 57);
            this.treeDivision.Name = "treeDivision";
            this.treeDivision.Size = new System.Drawing.Size(490, 287);
            this.treeDivision.TabIndex = 1;
            this.treeDivision.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeDivision_AfterSelect);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbtnSelect});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(602, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolbtnSelect
            // 
            this.toolbtnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnSelect.Name = "toolbtnSelect";
            this.toolbtnSelect.Size = new System.Drawing.Size(58, 22);
            this.toolbtnSelect.Text = "Выбрать";
            this.toolbtnSelect.Visible = false;
            this.toolbtnSelect.Click += new System.EventHandler(this.toolbtnSelect_Click);
            // 
            // btnPaste
            // 
            this.btnPaste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaste.Image = null;
            this.btnPaste.Location = new System.Drawing.Point(508, 226);
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.Size = new System.Drawing.Size(87, 23);
            this.btnPaste.TabIndex = 4;
            this.btnPaste.Text = "Вставить";
            this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
            // 
            // btnCut
            // 
            this.btnCut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCut.Image = null;
            this.btnCut.Location = new System.Drawing.Point(508, 197);
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(87, 23);
            this.btnCut.TabIndex = 3;
            this.btnCut.Text = "Вырезать";
            this.btnCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = null;
            this.btnAdd.IsSplitButton = true;
            this.btnAdd.Location = new System.Drawing.Point(508, 57);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.SplitButtonClick += new System.EventHandler(this.btnAdd_SplitButtonClick);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = null;
            this.btnEdit.Location = new System.Drawing.Point(508, 86);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(87, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = null;
            this.btnDelete.Location = new System.Drawing.Point(508, 115);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(87, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // menuAdd
            // 
            this.menuAdd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.какПодузелToolStripMenuItem1,
            this.вКореньToolStripMenuItem1});
            this.menuAdd.Name = "menuAdd";
            this.menuAdd.Size = new System.Drawing.Size(141, 48);
            // 
            // какПодузелToolStripMenuItem1
            // 
            this.какПодузелToolStripMenuItem1.Name = "какПодузелToolStripMenuItem1";
            this.какПодузелToolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
            this.какПодузелToolStripMenuItem1.Text = "Как подузел";
            this.какПодузелToolStripMenuItem1.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // вКореньToolStripMenuItem1
            // 
            this.вКореньToolStripMenuItem1.Name = "вКореньToolStripMenuItem1";
            this.вКореньToolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
            this.вКореньToolStripMenuItem1.Text = "В корень";
            this.вКореньToolStripMenuItem1.Click += new System.EventHandler(this.вКореньToolStripMenuItem_Click);
            // 
            // cmbFirmList
            // 
            this.cmbFirmList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbFirmList.DisplayMember = "FirmN";
            this.cmbFirmList.FormattingEnabled = true;
            this.cmbFirmList.Location = new System.Drawing.Point(92, 29);
            this.cmbFirmList.Name = "cmbFirmList";
            this.cmbFirmList.Size = new System.Drawing.Size(410, 21);
            this.cmbFirmList.TabIndex = 9;
            this.cmbFirmList.ValueMember = "FirmID";
            this.cmbFirmList.SelectedIndexChanged += new System.EventHandler(this.cmbFirmList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Организация";
            // 
            // btnRestore
            // 
            this.btnRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestore.Enabled = false;
            this.btnRestore.Image = null;
            this.btnRestore.Location = new System.Drawing.Point(508, 144);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(87, 23);
            this.btnRestore.TabIndex = 11;
            this.btnRestore.Text = "Восстановить";
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // chkShowDeleted
            // 
            this.chkShowDeleted.AutoSize = true;
            this.chkShowDeleted.Location = new System.Drawing.Point(13, 351);
            this.chkShowDeleted.Name = "chkShowDeleted";
            this.chkShowDeleted.Size = new System.Drawing.Size(133, 17);
            this.chkShowDeleted.TabIndex = 12;
            this.chkShowDeleted.Text = "Показать удаленные";
            this.chkShowDeleted.UseVisualStyleBackColor = true;
            this.chkShowDeleted.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // frmDivisionList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 382);
            this.Controls.Add(this.chkShowDeleted);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbFirmList);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnPaste);
            this.Controls.Add(this.treeDivision);
            this.Controls.Add(this.btnCut);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDivisionList";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подразделения";
            this.Load += new System.EventHandler(this.frmDivisionList_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuAdd.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeDivision;
        private System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.ToolStripButton toolbtnSelect;
        private ExButton.NET.ExButton btnCut;
        private ExButton.NET.ExButton btnPaste;
        private ExButton.NET.ExButton btnAdd;
        private ExButton.NET.ExButton btnEdit;
        private ExButton.NET.ExButton btnDelete;
        private System.Windows.Forms.ContextMenuStrip menuAdd;
        private System.Windows.Forms.ToolStripMenuItem какПодузелToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem вКореньToolStripMenuItem1;
        private System.Windows.Forms.ComboBox cmbFirmList;
        private System.Windows.Forms.Label label1;
        private ExButton.NET.ExButton btnRestore;
        private System.Windows.Forms.CheckBox chkShowDeleted;
    }
}